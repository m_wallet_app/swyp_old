package com.indy.helpers;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.IdlingPolicies;
import android.support.test.espresso.IdlingResource;

import com.indy.models.utils.MasterInputResponse;

import java.util.concurrent.TimeUnit;

/**
 * Created by mobile on 28/02/2017.
 */

public class CommonObjects {

    public static IdlingResource idlingResource;
    public static MasterInputResponse getBaseInput(){
        MasterInputResponse masterInputResponse = new MasterInputResponse();
//        masterInputResponse.setMsisdn("0512345678");
//        masterInputResponse.setOsVersion("1");
//        masterInputResponse.setToken("token");
//        masterInputResponse.setImsi("0512345678");
//        masterInputResponse.setAppVersion("1");
//        masterInputResponse.setChannel("channel");
//        masterInputResponse.setDeviceId("1232333232");
        return masterInputResponse;
    }
//    public static ViewAction waitFor(final long millis) {
//        return new ViewAction() {
//            @Override
//            public Matcher<View> getConstraints() {
//                return isRoot();
//            }
//
//            @Override
//            public String getDescription() {
//                return "Wait for " + millis + " milliseconds.";
//            }
//
//            @Override
//            public void perform(UiController uiController, final View view) {
//                uiController.loopMainThreadForAtLeast(millis);
//            }
//        };
//    }

    public static void waitFor(long waitingTime){
        IdlingPolicies.setMasterPolicyTimeout(waitingTime * 2, TimeUnit.MILLISECONDS);
        IdlingPolicies.setIdlingResourceTimeout(waitingTime * 2, TimeUnit.MILLISECONDS);

// Now we wait
        idlingResource = new ElapsedTimeIdlingResource(waitingTime);
        Espresso.registerIdlingResources(idlingResource);

// Stop and verify

    }

    public static void waitFinished(){

// Clean up
        Espresso.unregisterIdlingResources(idlingResource);
    }

}
