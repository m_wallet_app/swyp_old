package com.indy.modules.login;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.indy.R;
import com.indy.views.activites.SplashActivity;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class LoginTest {

    @Rule
    public ActivityTestRule<SplashActivity> mActivityTestRule = new ActivityTestRule<>(SplashActivity.class);

    public static void checkIfTutorialIsPresent() {
        try {

            ViewInteraction linearLayout = onView(
                    allOf(withId(R.id.skipBtn),
                            withParent(allOf(withId(R.id.titleLayout),
                                    withParent(withId(R.id.rl_root)))),
                            isDisplayed()));
            linearLayout.perform(click());
            //view is displayed logic
        } catch (Exception e) {
            //view not displayed logic

        }
    }

    public static void navigateToLogin() {
        try {

            ViewInteraction customButton = onView(
                    allOf(withId(R.id.loginBtn), withText("Login")));
            customButton.perform(scrollTo(), click());
        } catch (Exception ex) {

        }
    }

    public static void login() throws InterruptedException {
        checkIfTutorialIsPresent();
        navigateToLogin();
        try {
            ViewInteraction textInputEditText = onView(
                    allOf(withId(R.id.mobileNo),
                            withParent(allOf(withId(R.id.mobile_number_text_input_layout),
                                    withParent(withId(R.id.root_layout))))));

            textInputEditText.perform(scrollTo(), replaceText("0544481671"), closeSoftKeyboard());

            ViewInteraction textInputEditText2 = onView(
                    allOf(withId(R.id.password),
                            withParent(allOf(withId(R.id.password_text_input_layout),
                                    withParent(withId(R.id.root_layout))))));
            textInputEditText2.perform(scrollTo(), replaceText("Test@123"), closeSoftKeyboard());


            ViewInteraction customButton2 = onView(
                    allOf(withId(R.id.loginBtn), withText("Login"),
                            withParent(withId(R.id.root_layout))));
            customButton2.perform(scrollTo(), click());
            Thread.sleep(20000);
        }catch (Exception ex){

        }
    }

    @Test
    public void loginTest() throws InterruptedException {

        Thread.sleep(5000);

        login();

    }


    public static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
