package com.indy.modules.settings;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.indy.R;
import com.indy.helpers.FirstViewMatcher;
import com.indy.views.activites.SplashActivity;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.indy.modules.login.LoginTest.login;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class SignOutTest {

    @Rule
    public ActivityTestRule<SplashActivity> mActivityTestRule = new ActivityTestRule<>(SplashActivity.class);

    @Test
    public void signOut() throws InterruptedException {

        Thread.sleep(5000);

        navigateToSettings();

        ViewInteraction customTextView10 = onView(
                allOf(withId(R.id.tv_sign_out), withText("Sign out"), isDisplayed()));
        customTextView10.perform(click());
        Thread.sleep(5000);
        ViewInteraction textView2 = onView(
                allOf(withId(R.id.details), withText("Please enter your \nlogin details"),
                        childAtPosition(
                                allOf(withId(R.id.root_layout),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
                                                0)),
                                0),
                        isDisplayed()));
    }

    public static void navigateToSettings() throws InterruptedException {

        login();
        openTopMenu();
        ViewInteraction appCompatImageView = onView(
                withId(R.id.settingsID));
        appCompatImageView.perform( click());


        Thread.sleep(5000);
    }
    public static void openTopMenu(){
        ViewInteraction linearLayout2 = onView(
                allOf(withId(R.id.dragView), FirstViewMatcher.firstView())).perform(click());
        /*linearLayout2.perform(click());
        textView.check(matches(withText("U S A G E")));
        textView.perform(click());*/
    }

    public static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
