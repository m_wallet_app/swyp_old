package com.indy.modules.help;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.indy.R;
import com.indy.helpers.CommonObjects;
import com.indy.views.activites.SplashActivity;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class AViewPackagesTest {

    @Rule
    public ActivityTestRule<SplashActivity> mActivityTestRule = new ActivityTestRule<>(SplashActivity.class);

    @Test
    public void tutorialTest() throws InterruptedException {

//        CommonObjects.waitFor(14000);
        Thread.sleep(25000);
        ViewInteraction textView = onView(
                allOf(withText("View Packages"),
                        childAtPosition(
                                allOf(withId(R.id.viewPackagesId),
                                        childAtPosition(
                                                withId(R.id.ll_fourth),
                                                1)),
                                0),
                        isDisplayed()));
        textView.check(matches(isDisplayed()));
//        waitFinished();
        ViewInteraction button = onView(
                allOf(withId(R.id.loginBtn),
                        childAtPosition(
                                allOf(withId(R.id.ll_fourth),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
                                                3)),
                                0),
                        isDisplayed()));
        button.check(matches(isDisplayed()));

        ViewInteraction linearLayout = onView(
                allOf(withId(R.id.viewPackagesId),
                        withParent(withId(R.id.ll_fourth)),
                        isDisplayed()));
        linearLayout.perform(click());

        CommonObjects.waitFor(7000);

        ViewInteraction textView2 = onView(
                allOf(withId(R.id.tv_name), withText("1 GB"),
                        childAtPosition(
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
                                        0),
                                0),
                        isDisplayed()));
        textView2.check(matches(isDisplayed()));

        ViewInteraction textView6 = onView(
                allOf(withId(R.id.tv_category), withText("Share share share! A full speed 1GB package of Instagram and Snapchat only, to capture as many life moments as you can!"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.minutes_card_view),
                                        0),
                                2),
                        isDisplayed()));
        textView6.check(matches(isDisplayed()));

        ViewInteraction textView7 = onView(
                allOf(withId(R.id.tv_new_item), withText("Snapchat + Instagram"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.minutes_card_view),
                                        0),
                                1),
                        isDisplayed()));
        textView7.check(matches(isDisplayed()));

        ViewInteraction textView8 = onView(
                allOf(withId(R.id.tv_new_item), withText("Snapchat + Instagram"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.minutes_card_view),
                                        0),
                                1),
                        isDisplayed()));
        textView8.check(matches(isDisplayed()));

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.loginBtn_static), withText("Sign up"), isDisplayed()));
        appCompatButton.perform(click());

        pressBack();

        pressBack();

    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
