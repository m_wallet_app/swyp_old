package com.indy.modules.help;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.indy.R;
import com.indy.views.activites.SplashActivity;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class LiveChatTest {

    @Rule
    public ActivityTestRule<SplashActivity> mActivityTestRule = new ActivityTestRule<>(SplashActivity.class);


    public static void login() throws InterruptedException {
        FAQsTest.navigateToHelp();

        ViewInteraction linearLayout12 = onView(
                withId(R.id.ll_live_chat_help));
        linearLayout12.perform(scrollTo(), click());
        Thread.sleep(5000);
        pressBack();

        ViewInteraction linearLayout13 = onView(
                allOf(withId(R.id.ll_faq_help),
                        withParent(withId(R.id.card_view))));
        linearLayout13.perform(scrollTo(), click());
        Thread.sleep(5000);
        pressBack();

        ViewInteraction linearLayout14 = onView(
                withId(R.id.ll_find_us_help));
        linearLayout14.perform(scrollTo(), click());
        Thread.sleep(5000);
        ViewInteraction appCompatTextView4 = onView(
                allOf(withText("          SHOPS          "), isDisplayed()));
        appCompatTextView4.perform(click());
        Thread.sleep(5000);
        ViewInteraction appCompatTextView5 = onView(
                allOf(withText("WIFI"), isDisplayed()));
        appCompatTextView5.perform(click());
        Thread.sleep(5000);
        ViewInteraction customTextView8 = onView(
                allOf(withId(R.id.tv_logout_findus), withText(" Logout from all hotspots "), isDisplayed()));
        customTextView8.perform(click());
        Thread.sleep(3000);
        Thread.sleep(15000);
    }

    @Test
    public void liveChatTest() throws InterruptedException {

        Thread.sleep(5000);

        login();

//        ViewInteraction customTextView = onView(
//                allOf(withId(R.id.confirmBtn), withText("Add packages"),
//                        withParent(withId(R.id.ll_bottom_packages)),
//                        isDisplayed()));
//        customTextView.perform(click());
//
//        Thread.sleep(5000);
//        pressBack();
//
//        ViewInteraction customTextView2 = onView(
//                allOf(withId(R.id.tv_inviteFriends), withText("Invite friends"),
//                        withParent(withId(R.id.ll_bottom_packages)),
//                        isDisplayed()));
//        customTextView2.perform(click());
//        Thread.sleep(8000);
//        pressBack();
//
//        ViewInteraction customTextView3 = onView(
//                allOf(withId(R.id.confirmBtn), withText("Add packages"),
//                        withParent(withId(R.id.ll_bottom_packages)),
//                        isDisplayed()));
//        customTextView3.perform(click());
//        Thread.sleep(5000);
//        ViewInteraction linearLayout2 = onView(
//                allOf(withId(R.id.store_card_view),
//                        childAtPosition(
//                                allOf(withId(R.id.my_recycler_view),
//                                        childAtPosition(
//                                                IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
//                                                1)),
//                                0),
//                        isDisplayed()));
//        linearLayout2.check(matches(isDisplayed()));
//
//
//        pressBack();
//
//        ViewInteraction textView = onView(
//                allOf(withId(R.id.usageTxtID), withText("U S A G E"),
//                        childAtPosition(
//                                allOf(withId(R.id.lv_main_swip),
//                                        childAtPosition(
//                                                withId(R.id.dragView),
//                                                0)),
//                                0),
//                        isDisplayed()));
//        textView.check(matches(withText("U S A G E")));
//        textView.perform(click());
//
////        ViewInteraction customTextView4 = onView(
////                allOf(withId(R.id.usageID), withText("Usage")));
////        customTextView4.perform(scrollTo(), click());
////        Thread.sleep(5000);
////        textView = onView(
////                allOf(withId(R.id.usageTxtID), withText("U S A G E"),
////                        childAtPosition(
////                                allOf(withId(R.id.lv_main_swip),
////                                        childAtPosition(
////                                                withId(R.id.dragView),
////                                                0)),
////                                0),
////                        isDisplayed()));
//////        textView.check(matches(withText("U S A G E")));
////        textView.perform(click());
//
//        ViewInteraction customTextView5 = onView(
//                allOf(withId(R.id.storeID), withText("Store")));
//        customTextView5.perform(scrollTo(), click());
//        Thread.sleep(5000);
//
//        ViewInteraction linearLayout3 = onView(
//                allOf(withId(R.id.ll_recharge), isDisplayed()));
//        linearLayout3.perform(click());
//
//        ViewInteraction customButton3 = onView(
//                allOf(withId(R.id.btn_CreditCard), withText("Credit card"), isDisplayed()));
//        customButton3.perform(click());
//
//        ViewInteraction customEditText = onView(
//                allOf(withId(R.id.rechargenumberAmount),
//                        withParent(withId(R.id.rechargenumberTxtInput)),
//                        isDisplayed()));
//        customEditText.perform(replaceText("25"), closeSoftKeyboard());
//        ViewInteraction customButton4 = onView(
//                allOf(withId(R.id.btn_proceed), withText("Proceed"), isDisplayed()));
//        customButton4.perform(click());
//
//        Thread.sleep(5000);
//
//        pressBack();
//
//        pressBack();
//
//        ViewInteraction customButton5 = onView(
//                allOf(withId(R.id.btn_rechargeCard), withText("Recharge card"), isDisplayed()));
//        customButton5.perform(click());
//
//        pressBack();
//
//        pressBack();
//
//        ViewInteraction linearLayout4 = onView(
//                allOf(withId(R.id.ll_history), isDisplayed()));
//        linearLayout4.perform(click());
//        Thread.sleep(5000);
//
//        ViewInteraction linearLayout5 = onView(
//                allOf(childAtPosition(
//                        childAtPosition(
//                                withId(R.id.my_recycler_view),
//                                1),
//                        0),
//                        isDisplayed()));
//        linearLayout5.check(matches(isDisplayed()));
//
//        pressBack();
//
//        ViewInteraction linearLayout6 = onView(
//                allOf(withId(R.id.ll_transfer), isDisplayed()));
//        linearLayout6.perform(click());
//        Thread.sleep(5000);
//
//
//
//        pressBack();
//
//        pressBack();
//        textView.perform(click());
//
//        ViewInteraction customTextView6 = onView(
//                allOf(withId(R.id.rewardsID), withText("Rewards")));
//        customTextView6.perform(scrollTo(), click());
//        Thread.sleep(2000);
//
//        pressBack();
//
//
//        textView.perform(click());
//
//
////        ViewInteraction appCompatTextView = onView(
////                allOf(withId(R.id.shareID), withText("Share")));
////        appCompatTextView.perform(scrollTo(), click());
////        Thread.sleep(2000);
////        ViewInteraction linearLayout10 = onView(
////                allOf(IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
////                        childAtPosition(
////                                allOf(IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
////                                        childAtPosition(
////                                                IsInstanceOf.<View>instanceOf(android.widget.ScrollView.class),
////                                                0)),
////                                0),
////                        isDisplayed()));
////        linearLayout10.check(matches(isDisplayed()));
//
////        pressBack();
//
//        ViewInteraction appCompatTextView2 = onView(
//                allOf(withId(R.id.invitefriendID), withText("Invite friends")));
//        appCompatTextView2.perform(scrollTo(), click());
//        Thread.sleep(5000);
////        ViewInteraction customTextView7 = onView(
////                allOf(withId(R.id.btn_invite), withText("Invite")));
////        customTextView7.perform(scrollTo(), click());
//
////        ViewInteraction linearLayout11 = onView(
////                allOf(IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
////                        childAtPosition(
////                                allOf(IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
////                                        childAtPosition(
////                                                IsInstanceOf.<View>instanceOf(android.widget.ScrollView.class),
////                                                0)),
////                                0),
////                        isDisplayed()));
////        linearLayout11.check(matches(isDisplayed()));
//
//        pressBack();
////        pressBack();
//        ViewInteraction appCompatTextView3 = onView(
//                allOf(withId(R.id.getSupportID), withText("Get support")));
//        appCompatTextView3.perform(scrollTo(), click());
//
//        ViewInteraction linearLayout12 = onView(
//                withId(R.id.ll_live_chat_help));
//        linearLayout12.perform(scrollTo(), click());
//        Thread.sleep(5000);
//        pressBack();
//
//        ViewInteraction linearLayout13 = onView(
//                allOf(withId(R.id.ll_faq_help),
//                        withParent(withId(R.id.card_view))));
//        linearLayout13.perform(scrollTo(), click());
//        Thread.sleep(5000);
//        pressBack();
//
//        ViewInteraction linearLayout14 = onView(
//                withId(R.id.ll_find_us_help));
//        linearLayout14.perform(scrollTo(), click());
//        Thread.sleep(5000);
//        ViewInteraction appCompatTextView4 = onView(
//                allOf(withText("          SHOPS          "), isDisplayed()));
//        appCompatTextView4.perform(click());
//        Thread.sleep(5000);
//        ViewInteraction appCompatTextView5 = onView(
//                allOf(withText("WIFI"), isDisplayed()));
//        appCompatTextView5.perform(click());
//        Thread.sleep(5000);
//        ViewInteraction customTextView8 = onView(
//                allOf(withId(R.id.tv_logout_findus), withText(" Logout from all hotspots "), isDisplayed()));
//        customTextView8.perform(click());
//        Thread.sleep(3000);
//        pressBack();
//
//        pressBack();
//
//        pressBack();
//
//        pressBack();
//        Thread.sleep(6000);
//        textView.perform(click());
//
//        ViewInteraction appCompatImageView = onView(
//                withId(R.id.settingsID));
//        appCompatImageView.perform( click());
//
//        ViewInteraction cardView = onView(
//                allOf(withId(R.id.AccountDetails_Card), isDisplayed()));
//        cardView.perform(click());
//
//        ViewInteraction customTextView9 = onView(
//                allOf(withId(R.id.termsandconditionstv), withText("Terms and Conditions"),
//                        withParent(withId(R.id.termsandconditionsLv)),
//                        isDisplayed()));
//        customTextView9.perform(click());
//        Thread.sleep(5000);
//        pressBack();
//
//        pressBack();
//
//        ViewInteraction customTextView10 = onView(
//                allOf(withId(R.id.tv_sign_out), withText("Sign out"), isDisplayed()));
//        customTextView10.perform(click());
//        Thread.sleep(5000);
//        ViewInteraction textView2 = onView(
//                allOf(withId(R.id.details), withText("Please enter your \nlogin details"),
//                        childAtPosition(
//                                allOf(withId(R.id.root_layout),
//                                        childAtPosition(
//                                                IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
//                                                0)),
//                                0),
//                        isDisplayed()));
//        textView2.check(matches(withText("Please enter your \nlogin details")));

    }


    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
