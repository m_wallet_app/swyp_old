package com.indy.modules.help;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.indy.R;
import com.indy.views.activites.SplashActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class FindUsTest {

    @Rule
    public ActivityTestRule<SplashActivity> mActivityTestRule = new ActivityTestRule<>(SplashActivity.class);


    @Test
    public void findUsTest() throws InterruptedException {

        FAQsTest.navigateToHelp();

        ViewInteraction linearLayout14 = onView(
                withId(R.id.ll_find_us_help));
        linearLayout14.perform(scrollTo(), click());
        Thread.sleep(5000);
        ViewInteraction appCompatTextView4 = onView(
                allOf(withText("          SHOPS          "), isDisplayed()));
        appCompatTextView4.perform(click());
        Thread.sleep(5000);
        ViewInteraction appCompatTextView5 = onView(
                allOf(withText("WIFI"), isDisplayed()));
        appCompatTextView5.perform(click());
        Thread.sleep(5000);
        ViewInteraction customTextView8 = onView(
                allOf(withId(R.id.tv_logout_findus), withText(" Logout from all hotspots "), isDisplayed()));
        customTextView8.perform(click());
        Thread.sleep(2000);
        pressBack();

        Thread.sleep(1000);

    }


}