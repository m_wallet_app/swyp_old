package com.indy.modules.help;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.indy.R;
import com.indy.modules.login.LoginTest;
import com.indy.views.activites.SplashActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class FAQsTest {

    @Rule
    public ActivityTestRule<SplashActivity> mActivityTestRule = new ActivityTestRule<>(SplashActivity.class);

    public static void navigateToHelp() throws InterruptedException {
        Thread.sleep(5000);
        LoginTest.checkIfTutorialIsPresent();
        Thread.sleep(5000);
        LoginTest.navigateToLogin();

        ViewInteraction relativeLayout = onView(
                allOf(withId(R.id.helpLayout),
                        withParent(allOf(withId(R.id.headerLayoutID),
                                withParent(withId(R.id.root)))),
                        isDisplayed()));
        relativeLayout.perform(click());
    }

    @Test
    public void faqsTest() throws InterruptedException {


        navigateToHelp();

        ViewInteraction linearLayout13 = onView(
                allOf(withId(R.id.ll_faq_help),
                        withParent(withId(R.id.card_view))));
        linearLayout13.perform(scrollTo(), click());
        Thread.sleep(2000);
        pressBack();
        Thread.sleep(3000);
    }

}
