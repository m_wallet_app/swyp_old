package com.indy.new_membership;

import android.animation.Animator;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.Utils;
import com.indy.R;
import com.indy.adapters.MembershipChartAdapter;
import com.indy.customviews.MyProgressBar;
import com.indy.helpers.DateHelpers;
import com.indy.models.finalizeregisteration.AdditionalInfo;
import com.indy.models.packages.NewMemberShipPack;
import com.indy.models.packages.OfferList;
import com.indy.models.packages.PackageConsumption;
import com.indy.models.packages.UsagePackageList;
import com.indy.models.packageusage.GraphPointList;
import com.indy.models.packageusage.NewPackageUsageInfo;
import com.indy.models.packageusage.OfferInfo;
import com.indy.models.packageusage.PackageInfo;
import com.indy.models.packageusage.PackageUsageOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.services.NewPackageUsageService;
import com.indy.squad_number.SquadNumberActivity;
import com.indy.squad_number.listeners.UpdateSquadNumberListener;
import com.indy.squad_number.models.Squad;
import com.indy.squad_number.models.SquadNumber;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.MasterActivity;
import com.indy.views.fragments.usage.PackagesFragments;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class NewMembershipChartListActivity extends MasterActivity implements UpdateSquadNumberListener {

    private ImageView iv_packageIcon;
    private TextView data_package_name;
    private TextView data_nonstop_package_name;
    private TextView titletext, tv_dailyUsedSubtitle, tv_dailyEstimatedUsage, tv_dataUsedToday, tv_forecastUsage, tv_daysLeftInGraph, tv_offerPackageAmount;
    private TextView nonstop_titletext, tv_nonstop_dailyUsedSubtitle, tv_nonstop_dailyEstimatedUsage, tv_nonstop_dataUsedToday, tv_nonstop_forecastUsage, tv_nonstop_daysLeftInGraph, tv_nonstop_offerPackageAmount;
    private TextView startDate, endDate, tv_user_balance_amount, tv_balance_expire, tv_additional_subscription;
    private TextView nonstop_startDate, nonstop_endDate, tv_nonstop_user_balance_amount, tv_nonstop_balance_expire, tv_nonstop_additional_subscription;
    private TextView tv_squad_count, tv_squad_number, tv_squad_desc;
    private Button btn_squad_manage;
    private Button backImg, helpBtn;
    private NestedScrollView sv_main;
    private RecyclerView rv_chart_list;
    private LinearLayout ll_general_chart, ll_social_chart;
    private LinearLayout lv_progressBar;
    private LineChart mChart;
    private FrameLayout frameLayout;
    private MyProgressBar myProgressBar;
    private MyProgressBar my_nonstop_ProgressBar;
    private Typeface typeface;
    private UpdateSquadNumberListener updateSquadNumberListener;

    public static final int RESULT_DELETE_NUMBER = 1001;
    public static final int RESULT_CHANGE_NUMBER = 1002;
    public static final int RESULT_ADD_NUMBER = 1003;

    int xVal = 30;
    float yVal = 100;
    float minVal;
    float upperLimt = 0;
    float lowerLimit = 0f;
    private YAxis leftAxis;
    private LimitLine llXAxis;
    NewMemberShipPack newMemberShipPack;
    UsagePackageList socialPackgeListItem;
    UsagePackageList generalPackgeListItem;
    List<UsagePackageList> allPackgeList;
    SquadNumber squadNumber;
    ArrayList<OfferList> offerListSocial;
    ArrayList<OfferList> offerListGeneral;
    ArrayList<OfferList> offerListAll;
    ArrayList<OfferList> socialOfferListForServiceInput = new ArrayList<>();
    ArrayList<OfferList> generalOfferListForServiceInput = new ArrayList<>();
    ArrayList<OfferList> allOfferListForServiceInput = new ArrayList<>();
    ;
    private TextView expiresId;
    private PackageUsageOutput packageUsageOutput;
    private NewPackageUsageInfo packageUsageInput;
    ArrayList<Entry> values = new ArrayList<Entry>();

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        switch (resultCode) {


            case RESULT_ADD_NUMBER:
                Squad newSquad = new Squad();
                newSquad.setMobileNumber(data.getStringExtra("squadNumber"));
                ArrayList<Squad> squads = new ArrayList<>();
                squads.add(0, newSquad);
                squadNumber.setSquad(squads);
                newMemberShipPack.getSquadNumber().setSquad(squads);
                setSquadNumber(newMemberShipPack.getSquadNumber());
                if (PackagesFragments.instance != null) {
                    PackagesFragments.instance.onSetNewSquadNumber(squads);
                }
                break;

            case RESULT_CHANGE_NUMBER:
                newSquad = new Squad();
                newSquad.setMobileNumber(data.getStringExtra("squadNumber"));
                newMemberShipPack.getSquadNumber().getSquad().set(0, newSquad);
                setSquadNumber(newMemberShipPack.getSquadNumber());
                if (PackagesFragments.instance != null) {
                    PackagesFragments.instance.onSetNewSquadNumber(newMemberShipPack.getSquadNumber().getSquad());
                }
                break;

            case RESULT_DELETE_NUMBER:
                newMemberShipPack.getSquadNumber().getSquad().remove(0);
                setSquadNumber(newMemberShipPack.getSquadNumber());
                if (PackagesFragments.instance != null) {
                    PackagesFragments.instance.onDeleteSquadNumber();
                }
                break;

        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datapackage_chart_list_new_membership);
        initUI();
        onConsumeService();
    }

    @Override
    public void initUI() {
        super.initUI();
        try {
            ll_general_chart = (LinearLayout) findViewById(R.id.ll_general_chart);
            ll_social_chart = (LinearLayout) findViewById(R.id.ll_social_chart);
            tv_squad_count = (TextView) findViewById(R.id.tv_squad_count);
            tv_squad_number = (TextView) findViewById(R.id.tv_squad_number);
            tv_squad_desc = (TextView) findViewById(R.id.tv_squad_desc);
            btn_squad_manage = (Button) findViewById(R.id.btn_squad_manage);
            iv_packageIcon = (ImageView) findViewById(R.id.numberofdeals);
            backImg = (Button) findViewById(R.id.backImg);
            helpBtn = (Button) findViewById(R.id.helpBtn);
            helpBtn.setVisibility(View.INVISIBLE);
            rv_chart_list = (RecyclerView) findViewById(R.id.rv_chart_list);
            rv_chart_list.setLayoutManager(new LinearLayoutManager(this));
            sv_main = (NestedScrollView) findViewById(R.id.sv_main);
            lv_progressBar = (LinearLayout) findViewById(R.id.ll_progressBar);
            backImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    hideKeyBoard();
                    onBackPressed();
                }
            });
            lv_progressBar.setVisibility(View.VISIBLE);
            data_package_name = (TextView) findViewById(R.id.data_package_name);
            data_nonstop_package_name = (TextView) findViewById(R.id.data_nonstop_package_name);
            tv_dailyUsedSubtitle = (TextView) findViewById(R.id.tv_dailyEsimation);
            tv_nonstop_dailyUsedSubtitle = (TextView) findViewById(R.id.tv_nonstop_dailyEsimation);
            sv_main.setVisibility(View.GONE);
            tv_dailyEstimatedUsage = (TextView) findViewById(R.id.tv_mbused_daily);
            tv_nonstop_dailyEstimatedUsage = (TextView) findViewById(R.id.tv_nonstop_mbused_daily);
            tv_dataUsedToday = (TextView) findViewById(R.id.tv_mbused);
            tv_nonstop_dataUsedToday = (TextView) findViewById(R.id.tv_nonstop_mbused);
            tv_forecastUsage = (TextView) findViewById(R.id.tv_mbused_forecast);
            tv_nonstop_forecastUsage = (TextView) findViewById(R.id.tv_nonstop_mbused_forecast);

            tv_offerPackageAmount = (TextView) findViewById(R.id.data_package_value);
            tv_nonstop_offerPackageAmount = (TextView) findViewById(R.id.data_nonstop_package_value);
            tv_daysLeftInGraph = (TextView) findViewById(R.id.tv_expire_days_left);
            newMemberShipPack = getIntent().getParcelableExtra("newMemberShipPack");

            if (newMemberShipPack.getUsagePackageList() != null && newMemberShipPack.getUsagePackageList().size() > 0) {
                allPackgeList = newMemberShipPack.getUsagePackageList();
                for (int i = 0; i < allPackgeList.size(); i++) {
                    offerListAll = allPackgeList.get(i).getOfferList();
                    for (int j = 0; j < offerListAll.size(); j++) {
                        OfferList offerListItem = new OfferList();
                        offerListItem.setInOfferEndDate(offerListAll.get(j).getInOfferEndDate());
                        offerListItem.setInOfferStartDate(offerListAll.get(j).getInOfferStartDate());
                        offerListItem.setInOfferId(offerListAll.get(j).getInOfferId());
                        offerListItem.setPackageConsumption(offerListAll.get(j).getPackageConsumption());
                        offerListAll.get(j).setInOfferStartDate(parseDateWithDateSubtraction(offerListItem.getInOfferEndDate()));
                        allOfferListForServiceInput.add(offerListAll.get(j));
                        allPackgeList.get(i).setOfferList(offerListAll);
                    }
                }
            }

//            socialPackgeListItem = getIntent().getParcelableExtra("socialDataItem");
//            generalPackgeListItem = getIntent().getParcelableExtra("generalDataItem");
//            if (socialPackgeListItem != null) {
//                offerListSocial = socialPackgeListItem.getOfferList();
//
//                if (offerListSocial != null && offerListSocial.size() > 0) {
//                    for (int i = 0; i < offerListSocial.size(); i++) {
//                        OfferList offerListItem = new OfferList();
//                        offerListItem.setInOfferEndDate(offerListSocial.get(i).getInOfferEndDate());
//                        offerListItem.setInOfferStartDate(offerListSocial.get(i).getInOfferStartDate());
//                        offerListItem.setInOfferId(offerListSocial.get(i).getInOfferId());
//                        offerListItem.setPackageConsumption(offerListSocial.get(i).getPackageConsumption());
//                        socialOfferListForServiceInput.add(offerListItem);
//                    }
//                    offerListSocial.get(0).setInOfferStartDate(parseDateWithDateSubtraction(offerListSocial.get(0).getInOfferEndDate()));
//
//                }
//
//                socialPackgeListItem.setOfferList(offerListSocial);
//            } else {
//                ll_social_chart.setVisibility(View.GONE);
//            }
//            if (generalPackgeListItem != null) {
//                offerListGeneral = generalPackgeListItem.getOfferList();
//                if (offerListGeneral != null && offerListGeneral.size() > 0) {
//                    for (int i = 0; i < offerListGeneral.size(); i++) {
//                        OfferList offerListItem = new OfferList();
//                        offerListItem.setInOfferEndDate(offerListGeneral.get(i).getInOfferEndDate());
//                        offerListItem.setInOfferStartDate(offerListGeneral.get(i).getInOfferStartDate());
//                        offerListItem.setInOfferId(offerListGeneral.get(i).getInOfferId());
//                        offerListItem.setPackageConsumption(offerListGeneral.get(i).getPackageConsumption());
//                        generalOfferListForServiceInput.add(offerListItem);
//                    }
//                }
//                if (offerListGeneral != null && offerListGeneral.size() > 0) {
//                    offerListGeneral.get(0).setInOfferStartDate(parseDateWithDateSubtraction(offerListGeneral.get(0).getInOfferEndDate()));
//                }
//
//                generalPackgeListItem.setOfferList(offerListGeneral);
//            } else {
//                ll_general_chart.setVisibility(View.GONE);
//            }


            titletext = (TextView) findViewById(R.id.titleTxt);
            titletext.setText(R.string.membership_pack);

            mChart = (LineChart) findViewById(R.id.chart1);
            startDate = (TextView) findViewById(R.id.startDateID);
            nonstop_startDate = (TextView) findViewById(R.id.nonstop_startDateID);
            endDate = (TextView) findViewById(R.id.endDateId);
            nonstop_endDate = (TextView) findViewById(R.id.nonstop_endDateId);

            tv_user_balance_amount = (TextView) findViewById(R.id.tv_user_balance_amount);
            tv_balance_expire = (TextView) findViewById(R.id.tv_balance_expire);
            tv_additional_subscription = (TextView) findViewById(R.id.tv_additional_subscription);
            frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
            if (sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)) {
            } else {
            }

            // Only this line to be used instead of the whole commented block below
            tv_user_balance_amount.setText(getResources().getString(R.string.your_total_remaining_data_allowance_is));

//            if (currentLanguage.equals("en") && offerListSocial != null && offerListSocial.size() > 0 && offerListSocial.get(0).getPackageConsumption().getRemaining() != null && offerListGeneral != null && offerListGeneral.size() > 0 && offerListGeneral.get(0).getPackageConsumption().getRemaining() != null) {
//                tv_user_balance_amount.setLineSpacing(0, 1f);
//                String unitMB = ((offerListSocial.get(0).getPackageConsumption().getRemaining() + offerListGeneral.get(0).getPackageConsumption().getRemaining()) < 1000) ? "MB" : "GB";
//                tv_user_balance_amount.setText(getResources().getString(R.string.your_total_remaining_data_allowance_is) + " " + (offerListSocial.get(0).getPackageConsumption().getRemaining() + offerListGeneral.get(0).getPackageConsumption().getRemaining()) + " " + offerListSocial.get(0).getPackageConsumption().getConsumptionUnitEn());
//
//            } else if (currentLanguage.equals("ar") && offerListSocial != null && offerListSocial.size() > 0 && offerListSocial.get(0).getPackageConsumption().getRemaining() != null && offerListGeneral != null && offerListGeneral.size() > 0 && offerListGeneral.get(0).getPackageConsumption().getRemaining() != null) {
//                tv_user_balance_amount.setLineSpacing(10, 1.2f);
//                String unitMB = ((offerListSocial.get(0).getPackageConsumption().getRemaining() + offerListGeneral.get(0).getPackageConsumption().getRemaining()) < 1000) ? "MB" : "GB";
//                tv_user_balance_amount.setText(getResources().getString(R.string.your_total_remaining_data_allowance_is) + " " + (offerListSocial.get(0).getPackageConsumption().getRemaining() + offerListGeneral.get(0).getPackageConsumption().getRemaining()) + " " + offerListSocial.get(0).getPackageConsumption().getConsumptionUnitAr());
//
//            } else {
//                tv_user_balance_amount.setText(getResources().getString(R.string.get_remaining_balance) + " 0");
//            }


            tv_balance_expire.setText(" " + parseDateFormatChart(newMemberShipPack.getEndDate()) + " ");

            tv_additional_subscription.setText(getResources().getString(R.string.get_aditional_text));


            /*
            Below block for adapter

            if (currentLanguage.equals("en")) {
                typeface = Typeface.createFromAsset(getResources().getAssets(), "fonts/OpenSans-Regular.ttf");

                if (socialPackgeListItem != null && socialPackgeListItem.getDescriptionEn() != null)
                    data_nonstop_package_name.setText(socialPackgeListItem.getDescriptionEn());
                else
                    data_nonstop_package_name.setText("");

                if (generalPackgeListItem != null && generalPackgeListItem.getDescriptionEn() != null)
                    data_package_name.setText(generalPackgeListItem.getDescriptionEn());
                else
                    data_package_name.setText("");

            } else {
                typeface = Typeface.createFromAsset(getResources().getAssets(), "fonts/aktive_grotest_rg.ttf");

                if (socialPackgeListItem != null && socialPackgeListItem.getDescriptionAr() != null)
                    data_nonstop_package_name.setText(socialPackgeListItem.getDescriptionAr());
                else
                    data_nonstop_package_name.setText("");

                if (generalPackgeListItem != null && generalPackgeListItem.getDescriptionAr() != null)
                    data_package_name.setText(generalPackgeListItem.getDescriptionAr());
                else
                    data_package_name.setText("");
            }

*/
            myProgressBar = (MyProgressBar) findViewById(R.id.myProgressBar);
            my_nonstop_ProgressBar = (MyProgressBar) findViewById(R.id.my_nonstop_ProgressBar);

            squadNumber = newMemberShipPack.getSquadNumber();


            btn_squad_manage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent squadIntent = new Intent(NewMembershipChartListActivity.this, SquadNumberActivity.class);
                    squadIntent.putExtra("squadNumber", squadNumber);
                    startActivityForResult(squadIntent, 1000);
                }
            });
            setSquadNumber(squadNumber);

        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }
    }

    private void setSquadNumber(SquadNumber squadNumber) {
        if (squadNumber != null && squadNumber.getSquad() != null) {
            tv_squad_count.setText(squadNumber.getSquad().size() + "/" + squadNumber.getMaxSquadNumber());
            tv_squad_desc.setText(R.string.you_can_add_squad);
            btn_squad_manage.setText(R.string.manage);
        } else {
            tv_squad_count.setText("0/" + squadNumber.getMaxSquadNumber());
            tv_squad_desc.setText(R.string.make_unlimited_calls_to_swyp_buddy);
            btn_squad_manage.setText(R.string.add_buddy);
        }

        if (newMemberShipPack.getSquadNumber() != null && newMemberShipPack.getSquadNumber().getSquad() != null && newMemberShipPack.getSquadNumber().getSquad().size() > 0)
            tv_squad_number.setText(squadNumber.getSquad().get(0).getMobileNumber());
        else
            tv_squad_number.setText("");
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        try {
            addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
            lv_progressBar.setVisibility(View.VISIBLE);
            sv_main.setVisibility(View.GONE);
            frameLayout.setVisibility(View.VISIBLE);
            packageUsageInput = new NewPackageUsageInfo();
            packageUsageInput.setAppVersion(appVersion);
//        packageUsageInput.setToken(token);
            packageUsageInput.setOsVersion(osVersion);
            packageUsageInput.setAdditionalInfo(new ArrayList<AdditionalInfo>());
            packageUsageInput.setDeviceId(deviceId);
            packageUsageInput.setLang(currentLanguage);
            packageUsageInput.setChannel(channel);
            packageUsageInput.setMsisdn(sharedPrefrencesManger.getMobileNo());
            packageUsageInput.setAuthToken(authToken);

            List<PackageInfo> packageInfos = new ArrayList<>();
            PackageInfo packageInfo;
            List<OfferList> offerInfoList;
            List<OfferInfo> offerInfo;
            OfferInfo offerInfo1;

            for (int i = 0; i < allPackgeList.size(); i++) {
                packageInfo = new PackageInfo();
                packageInfo.setPackageId(allPackgeList.get(i).getPackageType() + "");
                offerInfoList = new ArrayList<OfferList>();
                offerInfoList.addAll(allPackgeList.get(i).getOfferList());
                offerInfo = new ArrayList<OfferInfo>();

                offerInfo1 = new OfferInfo();
                for (int j = 0; j < offerInfoList.size(); j++) {
                    offerInfo1.setStartDate(parseDateFormat(allOfferListForServiceInput.get(i).getInOfferStartDate()));
                    offerInfo1.setEndDate(parseDateFormat(allOfferListForServiceInput.get(i).getInOfferEndDate()));
                    offerInfo1.setOfferId(allOfferListForServiceInput.get(i).getInOfferId());
                    offerInfo.add(offerInfo1);
                }
                packageInfo.setOfferInfo(offerInfo);
                packageInfos.add(packageInfo);
            }

//            if (socialPackgeListItem != null) {
//                packageInfo = new PackageInfo();
//                packageInfo.setPackageId(socialPackgeListItem.getPackageType() + "");
//                offerInfoList = new ArrayList<OfferList>();
//                offerInfoList.addAll(offerListSocial);
//                offerInfo = new ArrayList<OfferInfo>();
//
//                offerInfo1 = new OfferInfo();
//                for (int i = 0; i < offerInfoList.size(); i++) {
//                    offerInfo1.setStartDate(parseDateFormat(socialOfferListForServiceInput.get(0).getInOfferStartDate()));
//                    offerInfo1.setEndDate(parseDateFormat(socialOfferListForServiceInput.get(0).getInOfferEndDate()));
//                    offerInfo1.setOfferId(socialOfferListForServiceInput.get(i).getInOfferId());
//                    offerInfo.add(offerInfo1);
//                }
//                packageInfo.setOfferInfo(offerInfo);
//                packageInfos.add(packageInfo);
//
//            }
//
//            if (generalPackgeListItem != null) {
//                packageInfo = new PackageInfo();
//                packageInfo.setPackageId(generalPackgeListItem.getPackageType() + "");
//                offerInfoList = new ArrayList<OfferList>();
//                offerInfoList.addAll(offerListGeneral);
//                offerInfo = new ArrayList<OfferInfo>();
//
//                offerInfo1 = new OfferInfo();
//                for (int i = 0; i < offerInfoList.size(); i++) {
//                    offerInfo1.setStartDate(parseDateFormat(generalOfferListForServiceInput.get(0).getInOfferStartDate()));
//                    offerInfo1.setEndDate(parseDateFormat(generalOfferListForServiceInput.get(0).getInOfferEndDate()));
//                    offerInfo1.setOfferId(generalOfferListForServiceInput.get(i).getInOfferId());
//                    offerInfo.add(offerInfo1);
//                }
//                packageInfo.setOfferInfo(offerInfo);
//                packageInfos.add(packageInfo);
//            }

            if (packageInfos.size() > 0) {
                packageUsageInput.setPackageInfo(packageInfos);
                new NewPackageUsageService(this, packageUsageInput);
            }
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            frameLayout.setVisibility(View.GONE);
            lv_progressBar.setVisibility(View.GONE);
            sv_main.setVisibility(View.VISIBLE);
            onBackPressed();
            if (responseModel != null && responseModel.getResultObj() != null) {
                packageUsageOutput = (PackageUsageOutput) responseModel.getResultObj();
                if (packageUsageOutput != null && packageUsageOutput.getUsageGraphList() != null && packageUsageOutput.getUsageGraphList().size() > 0 && packageUsageOutput.getUsageGraphList().get(0) != null) {
                    //    packageUsageOutput.getUsageGraphList().get(0).getGraphPointList().get(0).getXAxisValue();
                    //    Log.v("oooooooooops", packageUsageOutput.getUsageGraphList().get(0).getColor() + "");
                    values.clear();
                    sv_main.setVisibility(View.INVISIBLE);
                    ViewTreeObserver viewTreeObserver = sv_main.getViewTreeObserver();
                    if (viewTreeObserver.isAlive()) {
                        viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                            @Override
                            public void onGlobalLayout() {
                                circularRevealActivity();
                                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                                    sv_main.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                                } else {
                                    sv_main.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                                }
                            }
                        });
                    }
                    if (packageUsageOutput.getUsageGraphList() != null && packageUsageOutput.getUsageGraphList().size() > 0 && packageUsageOutput.getUsageGraphList().get(0) != null) {
//                        AssembleChart();

//                        setupProgressBarSocial();
//                        if (packageUsageOutput.getUsageGraphList().size() > 1)
//                            setupProgressBarGeneral();
                        MembershipChartAdapter membershipChartAdapter = new MembershipChartAdapter(allPackgeList, packageUsageOutput.getUsageGraphList(), this, currentLanguage);
                        rv_chart_list.setAdapter(membershipChartAdapter);

                    }
                } else {
                    frameLayout.setVisibility(View.VISIBLE);
                    onPackageFail();
                }
            }
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
//        super.onErrorListener(responseModel);
//        onBackPressed();
        try {
            frameLayout.setVisibility(View.GONE);
            lv_progressBar.setVisibility(View.GONE);
            sv_main.setVisibility(View.VISIBLE);
            onPackageFail();
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }
    }

    public void setupProgressBarSocial() {
//        rl_progressBarHeader.setVisibility(View.VISIBLE);
//        rl_dataHeader.setVisibility(View.GONE);
        mChart.setVisibility(View.GONE);
//        rl_progressBarLayout.setVisibility(View.VISIBLE);

        PackageConsumption packageConsumption;
//            if(PackagesListAdapter.usagePackageItem!=null && PackagesListAdapter.usagePackageItem.getOfferList()!=null)
        packageConsumption = socialPackgeListItem.getOfferList().get(0).getPackageConsumption();
        if (sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)) {
            data_nonstop_package_name.setText(socialPackgeListItem.getNameEn());
        } else {
            data_nonstop_package_name.setText(socialPackgeListItem.getNameAr());
        }
        my_nonstop_ProgressBar.setDataLimitText(packageConsumption.getRemaining() + " " + packageConsumption.getConsumptionUnitEn() +
                " " + getString(R.string.data_left));
        Double remainingAllowance = (packageConsumption.getConsumption() / packageConsumption.getQuota()) * 100;
//        myProgressBar.setMax(100);

        int finalProgress = 100 - (int) Math.ceil(remainingAllowance);
//        remainingAllowance.intValue();

        my_nonstop_ProgressBar.setProgress(finalProgress);

//        myProgressBar.setProgress(100 - 50);


        Double limitFromServiceValue = socialPackgeListItem.getOfferList().get(0).getPackageConsumption().getQuota();
        Double forecastedUsageValue = Double.parseDouble(packageUsageOutput.getUsageGraphList().get(0).getForecastedUsage());

        if (forecastedUsageValue > limitFromServiceValue) {
            tv_nonstop_forecastUsage.setTextColor(getResources().getColor(R.color.red_color));
        } else {
            tv_nonstop_forecastUsage.setTextColor(getResources().getColor(R.color.colorBlack));
        }

        tv_nonstop_forecastUsage.setText(daysDifference(socialPackgeListItem.getOfferList().get(0).getInOfferEndDate(), getString(R.string.days)));
//        tv_forecastUsage.setText(PackagesListAdapter.daysDifferenceForDetail(
//                MixedDataPackageGraphPagerActivity.userPackgeListItem.getOfferList().get(0).getInOfferEndDate()));//, getString(R.string.days_left)));

        if (sharedPrefrencesManger.getLanguage().equals("en")) {
            tv_nonstop_dataUsedToday.setText(packageUsageOutput.getUsageGraphList().get(0).getUsedToday() + " " + packageConsumption.getConsumptionUnitEn());
            tv_nonstop_dailyEstimatedUsage.setText(packageConsumption.getConsumption() + " " + packageConsumption.getConsumptionUnitEn());
//            tv_dailyUsedSubtitle.setText(getString(R.string.of) + " " + packageConsumption.getQuota()+packageConsumption.getConsumptionUnitEn() + " " + getString(R.string.used_lower_case));

            tv_nonstop_dailyUsedSubtitle.setText(getString(R.string.total_used));
//            tv_forecastUsage.setText(usageGraphListObject.getForecastedUsage() + " " + packageConsumption.getConsumptionUnitEn());
//            tv_forecastUsage.setText(tv_balance_expire.getText().toString());
//            tv_dailyEstimatedUsage.setText(usageGraphListObject.getDailyBudget() + " " + packageConsumption.getConsumptionUnitEn());
//            tv_offerName.setText(packageConsumption.getTextEn());
            tv_nonstop_offerPackageAmount.setText(packageConsumption.getQuota().toString() + " " + packageConsumption.getConsumptionUnitEn());
        } else {
            tv_nonstop_dataUsedToday.setText(packageUsageOutput.getUsageGraphList().get(0).getUsedToday() + " " + packageConsumption.getConsumptionUnitAr());
//            tv_forecastUsage.setText(usageGraphListObject.getForecastedUsage() + " " + packageConsumption.getConsumptionUnitAr());
            tv_nonstop_dailyEstimatedUsage.setText(getString(R.string.of) + " " + packageConsumption.getQuota() + packageConsumption.getConsumptionUnitAr() + " " + getString(R.string.used_lower_case));

            tv_nonstop_dailyEstimatedUsage.setText(packageUsageOutput.getUsageGraphList().get(0).getDailyBudget() + " " + packageConsumption.getConsumptionUnitAr());
//            tv_offerName.setText(packageConsumption.getTextAr());
            tv_nonstop_offerPackageAmount.setText(packageConsumption.getQuota().toString() + " " + packageConsumption.getConsumptionUnitAr());
        }
    }

    public void setupProgressBarGeneral() {
//        rl_progressBarHeader.setVisibility(View.VISIBLE);
//        rl_dataHeader.setVisibility(View.GONE);
        mChart.setVisibility(View.GONE);
//        rl_progressBarLayout.setVisibility(View.VISIBLE);

        PackageConsumption packageConsumption;
//            if(PackagesListAdapter.usagePackageItem!=null && PackagesListAdapter.usagePackageItem.getOfferList()!=null)
        if (generalPackgeListItem != null) {
            packageConsumption = generalPackgeListItem.getOfferList().get(0).getPackageConsumption();
            if (sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)) {
                data_package_name.setText(generalPackgeListItem.getNameEn());
            } else {
                data_package_name.setText(generalPackgeListItem.getNameAr());
            }
            myProgressBar.setDataLimitText(packageConsumption.getRemaining() + " " + packageConsumption.getConsumptionUnitEn() +
                    " " + getString(R.string.data_left));
            Double remainingAllowance = (packageConsumption.getConsumption() / packageConsumption.getQuota()) * 100;
//        myProgressBar.setMax(100);

            int finalProgress = 100 - (int) Math.ceil(remainingAllowance);
//        remainingAllowance.intValue();

            myProgressBar.setProgress(finalProgress);

//        myProgressBar.setProgress(100 - 50);


            Double limitFromServiceValue = generalPackgeListItem.getOfferList().get(0).getPackageConsumption().getQuota();
            Double forecastedUsageValue = Double.parseDouble(packageUsageOutput.getUsageGraphList().get(1).getForecastedUsage());

            if (forecastedUsageValue > limitFromServiceValue) {
                tv_forecastUsage.setTextColor(getResources().getColor(R.color.red_color));
            } else {
                tv_forecastUsage.setTextColor(getResources().getColor(R.color.colorBlack));
            }

            tv_forecastUsage.setText(daysDifference(generalPackgeListItem.getOfferList().get(0).getInOfferEndDate(), getString(R.string.days)));
//        tv_forecastUsage.setText(PackagesListAdapter.daysDifferenceForDetail(
//                MixedDataPackageGraphPagerActivity.userPackgeListItem.getOfferList().get(0).getInOfferEndDate()));//, getString(R.string.days_left)));

            if (sharedPrefrencesManger.getLanguage().equals("en")) {
                tv_dataUsedToday.setText(packageUsageOutput.getUsageGraphList().get(0).getUsedToday() + " " + packageConsumption.getConsumptionUnitEn());
                tv_dailyEstimatedUsage.setText(packageConsumption.getConsumption() + " " + packageConsumption.getConsumptionUnitEn());
//            tv_dailyUsedSubtitle.setText(getString(R.string.of) + " " + packageConsumption.getQuota()+packageConsumption.getConsumptionUnitEn() + " " + getString(R.string.used_lower_case));

                tv_dailyUsedSubtitle.setText(getString(R.string.total_used));
//            tv_forecastUsage.setText(usageGraphListObject.getForecastedUsage() + " " + packageConsumption.getConsumptionUnitEn());
//            tv_forecastUsage.setText(tv_balance_expire.getText().toString());
//            tv_dailyEstimatedUsage.setText(usageGraphListObject.getDailyBudget() + " " + packageConsumption.getConsumptionUnitEn());
//            tv_offerName.setText(packageConsumption.getTextEn());
                tv_offerPackageAmount.setText(packageConsumption.getQuota().toString() + " " + packageConsumption.getConsumptionUnitEn());
            } else {
                tv_dataUsedToday.setText(packageUsageOutput.getUsageGraphList().get(0).getUsedToday() + " " + packageConsumption.getConsumptionUnitAr());
//            tv_forecastUsage.setText(usageGraphListObject.getForecastedUsage() + " " + packageConsumption.getConsumptionUnitAr());
                tv_dailyEstimatedUsage.setText(getString(R.string.of) + " " + packageConsumption.getQuota() + packageConsumption.getConsumptionUnitAr() + " " + getString(R.string.used_lower_case));

                tv_dailyEstimatedUsage.setText(packageUsageOutput.getUsageGraphList().get(0).getDailyBudget() + " " + packageConsumption.getConsumptionUnitAr());
//            tv_offerName.setText(packageConsumption.getTextAr());
                tv_offerPackageAmount.setText(packageConsumption.getQuota().toString() + " " + packageConsumption.getConsumptionUnitAr());
            }
        }
    }

    public void onPackageFail() {

        if (packageUsageOutput != null && packageUsageOutput.getErrorMsgEn() != null) {
            if (currentLanguage.equals("en")) {
                showErrorFragment(packageUsageOutput.getErrorMsgEn());
            } else if (currentLanguage.equals("ar")) {
                showErrorFragment(packageUsageOutput.getErrorMsgAr());
            } else {
                showErrorFragment(getString(R.string.generice_error));
            }
        }

    }

    private void showErrorFragment(String error) {
        frameLayout.setVisibility(View.VISIBLE);

        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("finish", true);
        bundle.putString(ConstantUtils.errorString, error);
        errorFragment.setArguments(bundle);
        addFragmnet(errorFragment, R.id.frameLayout, true);
    }


    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    private void circularRevealActivity() {
        try {
            int cx = sv_main.getWidth() / 2;
            int cy = sv_main.getHeight() / 2;
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int width = size.x;
            int height = size.y;
            float finalRadius = Math.max(width + 200, height + 200);

            // create the animator for this view (the start radius is zero)
            Animator circularReveal = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                circularReveal = ViewAnimationUtils.createCircularReveal(sv_main, 0, height, 0, finalRadius);

                circularReveal.setDuration(1000);

                // make the view visible and start the animation
                sv_main.setVisibility(View.VISIBLE);
                circularReveal.start();
            } else {
                sv_main.setVisibility(View.VISIBLE);
            }
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }
    }

    private void setData(int count, float range) {

        try {

            List<GraphPointList> tempList = packageUsageOutput.getUsageGraphList().get(0).getGraphPointList();

            Collections.sort(tempList, new Comparator<GraphPointList>() {
                @Override
                public int compare(GraphPointList lhs, GraphPointList rhs) {
                    if (lhs.getYAxisValue() < rhs.getYAxisValue()) {
                        return -1;
                    } else {
                        return 1;
                    }
                }
            });

            if (packageUsageOutput != null && packageUsageOutput.getUsageGraphList() != null &&
                    packageUsageOutput.getUsageGraphList().size() > 0 && packageUsageOutput.getUsageGraphList().get(0) != null &&
                    packageUsageOutput.getUsageGraphList().get(0).getGraphPointList() != null &&
                    packageUsageOutput.getUsageGraphList().get(0).getGraphPointList().get(0) != null && (
                    packageUsageOutput.getUsageGraphList().get(0).getGraphPointList().get(0).getXAxisValue() > 0 ||
                            packageUsageOutput.getUsageGraphList().get(0).getGraphPointList().get(0).getYAxisValue() > 0)) {
                values = new ArrayList<Entry>();
                for (int i = 0; i < count; i++) {
                    values.add(new Entry(i, 0));
                }
                int xValForGraph = -1;
                int previousX = 0;
                for (int i = 0; i < packageUsageOutput.getUsageGraphList().get(0).getGraphPointList().size(); i++) {
//                if (i < 10) {
                    String valToFloat = tempList.get(i).getYAxisValue().toString();
                    float val = Float.parseFloat(valToFloat);
                    float xval;

//            if(i!=0){
//                if(val==0){
//                    xval = Float.parseFloat(tempList.get(i-1).getXAxisValue().toString());
//                    xValForGraph = Math.round(xval);
//
//                    values.set(xValForGraph,new Entry(xValForGraph, val));
//                }else{
                    xval = Float.parseFloat(tempList.get(i).getXAxisValue().toString());
                    xValForGraph = Math.round(xval);
                    if (xValForGraph >= 0 && xValForGraph <= 30) {
//                        if(xValForGraph==0)
//                            xValForGraph = 1;
//                        if(i>=1 && previousX==0&& xValForGraph>0){
//                            values.add(xValForGraph - 1, new Entry(xValForGraph - 0.001f, 0));
//
//                        }
                        values.set(xValForGraph, new Entry(xValForGraph, val));
                        previousX = xValForGraph;
                    }
//                }
//            }else{
//                xval = Float.parseFloat(tempList.get(i).getXAxisValue().toString());
//                xValForGraph = Math.round(xval);
//                values.set(xValForGraph,new Entry(xValForGraph, val));
//            }


                    upperLimt = (float) (tempList.get(i).getYAxisValue() + 5);
                    //   int abc = Integer.parseInt(Collections.max(tempList.get(i).getYAxisValue()/11));

//                } else {
//                    float val = (float) (tempList.get(i).getYAxisValue()/11);
//                    values.add(new Entry(i, val));
//
//                }


//            } else {
//                values.add(new Entry(i, 0));
//            }
                }
                if (xValForGraph >= 0 && xValForGraph <= 30) {

                    values.add(xValForGraph + 1, new Entry(xValForGraph + 0.001f, 0));
                }

            } else {
                values = new ArrayList<Entry>();
            }
            LineDataSet set1;

            if (mChart.getData() != null &&
                    mChart.getData().getDataSetCount() > 0) {
                set1 = (LineDataSet) mChart.getData().getDataSetByIndex(0);
                set1.setDrawValues(false);
                set1.setValues(values);

                set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                mChart.getData().notifyDataChanged();
                mChart.notifyDataSetChanged();
            } else {
                // create a dataset and give it a type
//            set1 = new LineDataSet(values, "10 days Remaining");
                set1 = new LineDataSet(values, "");
                // set the line to be drawn like this "- - - - - -"
//            set1.enableDashedLine(10f, 5f, 0f);
                set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                set1.enableDashedHighlightLine(10f, 5f, 0f);
                set1.setColor(getResources().getColor(R.color.orange_color));
                set1.setCircleColor(getResources().getColor(R.color.orange_color));
                set1.setLineWidth(1f);
                set1.setCircleRadius(3f);
                set1.setDrawCircleHole(false);
                set1.setValueTextSize(9f);
                set1.setDrawFilled(true);
                set1.setDrawValues(false);
                if (Utils.getSDKInt() >= 18) {
                    // fill drawable only supported on api level 18 and above
                    Drawable drawable = ContextCompat.getDrawable(this, R.drawable.fade_red);
                    set1.setFillDrawable(drawable);
                } else {
                    set1.setFillColor(getResources().getColor(R.color.graph_line_color));
                }

                ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
                dataSets.add(set1); // add the datasets

                // create a data object with the datasets
                LineData data = new LineData(dataSets);

                // set data
                mChart.setData(data);
            }
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }
    }


    public static String parseDateFormat(String date) {
        String returnString = "";
        SimpleDateFormat dateFormatter;
        try {
            Date formattedDate;
            //28/09/2016 19:13:20
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
            formattedDate = df.parse(date);
            dateFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
            return dateFormatter.format(formattedDate);
        } catch (Exception ex) {
            return returnString;

        }
    }


    public static String parseDateFormatChart(String date) {
        String returnString = "";
        SimpleDateFormat dateFormatter;
        try {
            Date formattedDate;
            //28/09/2016 19:13:20
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
            formattedDate = df.parse(date);
            dateFormatter = new SimpleDateFormat("dd MMM, HH:mm", Locale.US);
            return dateFormatter.format(formattedDate);
        } catch (Exception ex) {
            return returnString;

        }
    }

    public static String parseDateForStartEndDate(String date) {
        String returnString = "";
        SimpleDateFormat dateFormatter;
        try {
            Date formattedDate;
            //28/09/2016 19:13:20
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
            formattedDate = df.parse(date);
            dateFormatter = new SimpleDateFormat("dd MMM", Locale.US);
            return dateFormatter.format(formattedDate);
        } catch (Exception ex) {
            return returnString;

        }
    }


    public static String daysDifference(String date, String toAppendAtEnd) {
        Date dateToPass;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
        try {

            dateToPass = simpleDateFormat.parse(date);

        } catch (Exception ex) {
            dateToPass = null;
        }
        if (dateToPass != null) {
            long diffInMils = dateToPass.getTime() - DateHelpers.getCurrentDateFor().getTime();
            long daysDiff = TimeUnit.MILLISECONDS.toDays(diffInMils);
            return daysDiff + " " + toAppendAtEnd;
        } else {
            return toAppendAtEnd + " " + date;
        }
    }

    public static String parseDateWithDateSubtraction(String date) {
        String returnString = "";
        SimpleDateFormat dateFormatter;
        try {
            Date formattedDate;
            //28/09/2016 19:13:20
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

            formattedDate = df.parse(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(formattedDate);
            long timeOld = calendar.getTimeInMillis();
            long timeToSubtract = 2592000000L;

            long newTime = timeOld - timeToSubtract;
            Date dateAfterSubtraction = new Date(newTime);

            dateFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
            return dateFormatter.format(dateAfterSubtraction);
        } catch (Exception ex) {
            return returnString;

        }
    }

    public void updateSquadNumberListener(UpdateSquadNumberListener updateSquadNumberListener) {
        this.updateSquadNumberListener = updateSquadNumberListener;
    }

    @Override
    public void onSetNewSquadNumber(String squadNumber) {
        Squad newSquad = new Squad();
        newSquad.setMobileNumber(squadNumber);
        newMemberShipPack.getSquadNumber().getSquad().set(0, newSquad);
        updateSquadNumberListener.onSetNewSquadNumber(squadNumber);
    }

    @Override
    public void onDeleteSquadNumber() {
        newMemberShipPack.getSquadNumber().getSquad().remove(0);
        updateSquadNumberListener.onDeleteSquadNumber();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (PackagesFragments.instance != null) {
            PackagesFragments.instance.refresh();
        }
    }
}
