package com.indy.new_membership;

public interface NewMembershipSuccessListener {
    void onOKPressed();
}
