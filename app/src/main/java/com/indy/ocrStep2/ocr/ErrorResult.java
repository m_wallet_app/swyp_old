package com.indy.ocrStep2.ocr;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mobile on 10/08/2017.
 */

public class ErrorResult {


    @SerializedName("result")
    @Expose
    private String result;
    @SerializedName("errorType")
    @Expose
    private String errorType;
    @SerializedName("errorDescription")
    @Expose
    private Object errorDescription;


    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getErrorType() {
        return errorType;
    }

    public void setErrorType(String errorType) {
        this.errorType = errorType;
    }

    public Object getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(Object errorDescription) {
        this.errorDescription = errorDescription;
    }



}
