package com.indy.ocrStep2.ocr;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mobile on 10/08/2017.
 */

public class AppStatsResult {

    @SerializedName("sessionKey")
    @Expose
    private String sessionKey;
    @SerializedName("environmentId")
    @Expose
    private String environmentId;
    @SerializedName("instanceId")
    @Expose
    private String instanceId;
    @SerializedName("documentId")
    @Expose
    private String documentId;

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getEnvironmentId() {
        return environmentId;
    }

    public void setEnvironmentId(String environmentId) {
        this.environmentId = environmentId;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }


}
