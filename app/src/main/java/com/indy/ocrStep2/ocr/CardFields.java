package com.indy.ocrStep2.ocr;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mobile on 10/08/2017.
 */

public class CardFields {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("valid")
    @Expose
    private Boolean valid;
    @SerializedName("errorDescription")
    @Expose
    private String errorDescription;
    @SerializedName("left")
    @Expose
    private Integer left;
    @SerializedName("top")
    @Expose
    private Integer top;
    @SerializedName("height")
    @Expose
    private Integer height;
    @SerializedName("width")
    @Expose
    private Integer width;
    @SerializedName("pageIndex")
    @Expose
    private Integer pageIndex;
    @SerializedName("confidence")
    @Expose
    private Double confidence;
    @SerializedName("formattingFailed")
    @Expose
    private Boolean formattingFailed;
    @SerializedName("fieldAlternatives")
    @Expose
    private List<Object> fieldAlternatives = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean getValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public Integer getLeft() {
        return left;
    }

    public void setLeft(Integer left) {
        this.left = left;
    }

    public Integer getTop() {
        return top;
    }

    public void setTop(Integer top) {
        this.top = top;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    public Double getConfidence() {
        return confidence;
    }

    public void setConfidence(Double confidence) {
        this.confidence = confidence;
    }

    public Boolean getFormattingFailed() {
        return formattingFailed;
    }

    public void setFormattingFailed(Boolean formattingFailed) {
        this.formattingFailed = formattingFailed;
    }

    public List<Object> getFieldAlternatives() {
        return fieldAlternatives;
    }

    public void setFieldAlternatives(List<Object> fieldAlternatives) {
        this.fieldAlternatives = fieldAlternatives;
    }

}
