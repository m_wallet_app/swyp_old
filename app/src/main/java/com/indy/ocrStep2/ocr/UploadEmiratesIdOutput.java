package com.indy.ocrStep2.ocr;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

import java.util.List;

/**
 * Created by mobile on 10/08/2017.
 */

public class UploadEmiratesIdOutput extends MasterErrorResponse {


    @SerializedName("extractionClass")
    @Expose
    private String extractionClass;
    @SerializedName("classificationResult")
    @Expose
    private List<Object> classificationResult = null;
    @SerializedName("fields")
    @Expose
    private List<CardFields> fields = null;
    @SerializedName("sessionKey")
    @Expose
    private String sessionKey;
    @SerializedName("environmentId")
    @Expose
    private String environmentId;
    @SerializedName("instanceId")
    @Expose
    private String instanceId;
    @SerializedName("documentId")
    @Expose
    private String documentId;
    @SerializedName("result")
    @Expose
    private String result;
    @SerializedName("errorType")
    @Expose
    private String errorType;

    @SerializedName("errorResult")
    @Expose
    private ErrorResult errorResult;
    @SerializedName("appStatsResult")
    @Expose
    private AppStatsResult appStatsResult;
    private String responseMessage;
    private String responseCode;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getExtractionClass() {
        return extractionClass;
    }

    public void setExtractionClass(String extractionClass) {
        this.extractionClass = extractionClass;
    }

    public List<Object> getClassificationResult() {
        return classificationResult;
    }

    public void setClassificationResult(List<Object> classificationResult) {
        this.classificationResult = classificationResult;
    }

    public List<CardFields> getFields() {
        return fields;
    }

    public void setFields(List<CardFields> fields) {
        this.fields = fields;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getEnvironmentId() {
        return environmentId;
    }

    public void setEnvironmentId(String environmentId) {
        this.environmentId = environmentId;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getErrorType() {
        return errorType;
    }

    public void setErrorType(String errorType) {
        this.errorType = errorType;
    }

    public ErrorResult getErrorResult() {
        return errorResult;
    }

    public void setErrorResult(ErrorResult errorResult) {
        this.errorResult = errorResult;
    }

    public AppStatsResult getAppStatsResult() {
        return appStatsResult;
    }

    public void setAppStatsResult(AppStatsResult appStatsResult) {
        this.appStatsResult = appStatsResult;
    }

    public UploadEmiratesIdOutput() {
    }

//    @Override
//    public int describeContents() {
//        return 0;
//    }

//    @Override
//    public void writeToParcel(Parcel dest, int flags) {
////        super.writeToParcel(dest, flags);
//        dest.writeString(this.extractionClass);
//        dest.writeList(this.classificationResult);
//        dest.writeTypedList(this.fields);
//        dest.writeString(this.sessionKey);
//        dest.writeString(this.environmentId);
//        dest.writeString(this.instanceId);
//        dest.writeString(this.documentId);
//        dest.writeString(this.result);
//        dest.writeString(this.errorType);
//        dest.writeParcelable(this.errorResult, flags);
//        dest.writeParcelable(this.appStatsResult, flags);
//        dest.writeString(this.responseMessage);
//        dest.writeString(this.responseCode);
//        dest.writeString(this.responseMsg);
//        dest.writeString(this.errorCode);
//        dest.writeString(this.errorMsg);
//        dest.writeString(this.businessError);
//    }
//
//    protected UploadEmiratesIdOutput(Parcel in) {
//        super(in);
//        this.extractionClass = in.readString();
//        this.classificationResult = new ArrayList<Object>();
//        in.readList(this.classificationResult, Object.class.getClassLoader());
//        this.fields = in.createTypedArrayList(CardFields.CREATOR);
//        this.sessionKey = in.readString();
//        this.environmentId = in.readString();
//        this.instanceId = in.readString();
//        this.documentId = in.readString();
//        this.result = in.readString();
//        this.errorType = in.readString();
//        this.errorResult = in.readParcelable(ErrorResult.class.getClassLoader());
//        this.appStatsResult = in.readParcelable(AppStatsResult.class.getClassLoader());
//        this.responseMessage = in.readString();
//        this.responseCode = in.readString();
//        this.responseMsg = in.readString();
//        this.errorCode = in.readString();
//        this.errorMsg = in.readString();
//        this.businessError = in.readString();
//    }
//
//    public static final Creator<UploadEmiratesIdOutput> CREATOR = new Creator<UploadEmiratesIdOutput>() {
//        @Override
//        public UploadEmiratesIdOutput createFromParcel(Parcel source) {
//            return new UploadEmiratesIdOutput(source);
//        }
//
//        @Override
//        public UploadEmiratesIdOutput[] newArray(int size) {
//            return new UploadEmiratesIdOutput[size];
//        }
//    };
}