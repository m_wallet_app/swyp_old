package com.indy.squad_number.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.indy.R;
import com.indy.squad_number.listeners.OnSquadNumberAddConfirmationListener;
import com.indy.views.fragments.utils.MasterFragment;

import org.w3c.dom.Text;

public class SquadNumberConfirmationFragment extends MasterFragment {

    private View baseView;
    private Button btn_ok;
    private TextView tv_squad_no, btn_cancel;
    private TextView tv_title, tv_warningText;
    private String addSquadNo = "", responseMessage = "";
    private OnSquadNumberAddConfirmationListener onSquadNumberAddConfirmationListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        baseView = inflater.inflate(R.layout.fragment_add_squad_confirmation, null);
        return baseView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initViews();
        if(getArguments() != null && getArguments().getString("addSquadNo") != null && getArguments().getString("addSquadNo").length() > 0){
            addSquadNo = getArguments().getString("addSquadNo");
        }
        if(getArguments() != null && getArguments().getString("responseMessage") != null && getArguments().getString("responseMessage").length() > 0){
            responseMessage = getArguments().getString("responseMessage");
            tv_warningText.setText(responseMessage);
        }

        setData();
    }

    private void initViews(){
        tv_title = (TextView) baseView.findViewById(R.id.tv_title);
        tv_squad_no = (TextView) baseView.findViewById(R.id.tv_squad_no);
        tv_warningText = (TextView) baseView.findViewById(R.id.tv_warningText);
        btn_ok = baseView.findViewById(R.id.btn_ok);
        btn_cancel = baseView.findViewById(R.id.btn_cancel);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSquadNumberAddConfirmationListener.onConfirmed();
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSquadNumberAddConfirmationListener.onCancelled();
            }
        });
    }

    private void setData(){
//        tv_squad_no.setText(addSquadNo);
    }

    public void setOnSquadNumberAddConfirmationListener(OnSquadNumberAddConfirmationListener onSquadNumberAddConfirmationListener){
        this.onSquadNumberAddConfirmationListener = onSquadNumberAddConfirmationListener;
    }

}
