package com.indy.squad_number.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.indy.R;
import com.indy.squad_number.listeners.OnSquadNumberAddConfirmationListener;
import com.indy.squad_number.listeners.OnSquadNumberDeleteListener;
import com.indy.views.fragments.utils.MasterFragment;

public class SquadNumberDeleteFragment extends MasterFragment {

    private View baseView;
    private Button btn_ok, btn_cancel;
    private TextView tv_squad_no;
    private TextView tv_title;
    private String addSquadNo = "";
    private OnSquadNumberDeleteListener onSquadNumberDeleteListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        baseView = inflater.inflate(R.layout.fragment_delete_squad_confirmation, null);
        return baseView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initViews();
    }

    private void initViews(){
        tv_title = (TextView) baseView.findViewById(R.id.tv_title);
        tv_squad_no = (TextView) baseView.findViewById(R.id.tv_squad_no);
        btn_ok = baseView.findViewById(R.id.btn_ok);
        btn_cancel = baseView.findViewById(R.id.btn_cancel);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSquadNumberDeleteListener.onDelete();
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSquadNumberDeleteListener.onCancelled();
            }
        });
    }

    public void setOnSquadNumberAddConfirmationListener(OnSquadNumberDeleteListener onSquadNumberDeleteListener){
        this.onSquadNumberDeleteListener = onSquadNumberDeleteListener;
    }

}