package com.indy.squad_number;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.controls.BaseInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.new_membership.NewMembershipChartActivity;
import com.indy.services.AddSquadNumberService;
import com.indy.services.ChangeSquadNumberService;
import com.indy.services.DeleteSquadNumberService;
import com.indy.services.ValidateSquadNumberService;
import com.indy.squad_number.fragments.SquadNumberConfirmationFragment;
import com.indy.squad_number.fragments.SquadNumberDeleteFragment;
import com.indy.squad_number.listeners.OnSquadNumberAddConfirmationListener;
import com.indy.squad_number.listeners.OnSquadNumberDeleteListener;
import com.indy.squad_number.models.AddSquadNumberModel;
import com.indy.squad_number.models.Squad;
import com.indy.squad_number.models.SquadNumber;
import com.indy.squad_number.models.SquadNumberOutput;
import com.indy.squad_number.models.ValidateSquadNumberModel;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.MasterActivity;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;

import java.util.ArrayList;

public class SquadNumberActivity extends MasterActivity implements BaseInterface, OnSquadNumberAddConfirmationListener, OnSquadNumberDeleteListener {


    private TextInputLayout til_number;
    private TextView tv_squad_number;
    private TextView tv_add_squad_error, titleTxt;
    private EditText et_number;
    private Button bt_add_squad, backImg, helpBtn;
    private LinearLayout ll_squad_number_input;
    private Button bt_change_squad;
    private FrameLayout frame_content;
    private SquadNumber squadNumber;
    private String number_squad;
    private RelativeLayout headerLayoutID, backLayout, helpLayout;


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (data != null && data.getData() != null) {
                String phoneNo = null;
                Uri uri = data.getData();
                Cursor cursor = getContentResolver().query(uri, null, null, null, null);

                if (cursor.moveToFirst()) {
                    int phoneIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                    phoneNo = cursor.getString(phoneIndex);
                }

                cursor.close();

                et_number.setText(phoneNo);

            }
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_squad_number);
        squadNumber = getIntent().getExtras().getParcelable("squadNumber");
        initUI();

        if (squadNumber != null && squadNumber.getSquad() != null && squadNumber.getSquad().size() > 0) {
            setData(squadNumber.getSquad());
        } else {
            ll_squad_number_input.setVisibility(View.VISIBLE);
            tv_squad_number.setVisibility(View.GONE);
            setAddBtn("Add");
        }
        initToolbar();
        setupToolbar();
        enableNextBtn(false);
    }

    private void setData(ArrayList<Squad> squad) {
        if (squad.size() > 0) {
            ll_squad_number_input.setVisibility(View.GONE);
            tv_squad_number.setVisibility(View.VISIBLE);
            tv_squad_number.setText(squad.get(0).getMobileNumber());
            setChangeButton();
        } else {
            ll_squad_number_input.setVisibility(View.VISIBLE);
            tv_squad_number.setVisibility(View.GONE);
        }

    }

    @Override
    public void initUI() {
        super.initUI();
        til_number = findViewById(R.id.til_dbt_mobile_number);
        et_number = findViewById(R.id.et_dbt_mobile_number);
        tv_squad_number = findViewById(R.id.tv_squad_number);
        tv_add_squad_error = findViewById(R.id.tv_add_squad_error);
        bt_add_squad = findViewById(R.id.bt_add_squad);
        bt_change_squad = findViewById(R.id.bt_change_squad);
        frame_content = findViewById(R.id.frame_content);
        ll_squad_number_input = findViewById(R.id.ll_squad_number_input);

        bt_add_squad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                hideKeyBoard();
                if (isValid()) {
                    validateSquadNumber();
//                    if (v.getTag().toString().equalsIgnoreCase("Add"))
//                        verifyAndAddNumber();
//                    else
//                        validateSquadNumber();
                } else
                    et_number.clearFocus();

            }
        });

        bt_change_squad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAddBtn("Change");
                ll_squad_number_input.setVisibility(View.VISIBLE);
                tv_squad_number.setVisibility(View.GONE);
            }
        });
        tv_squad_number.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_END_ENGLISH = 2;
                final int DRAWABLE_END_ARABIC = 0;
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentLanguage.equals("en")) {
                        if (event.getRawX() >= (tv_squad_number.getRight() - tv_squad_number.getCompoundDrawables()[DRAWABLE_END_ENGLISH].getBounds().width())) {
                            // your action here
                            showDeleteConfirmation();
                            return true;
                        }
                    } else {
                        if (event.getX() <= (tv_squad_number.getCompoundDrawables()[DRAWABLE_END_ARABIC].getBounds().width())) {
                            // your action here
                            showDeleteConfirmation();
                            return true;
                        }
                    }
                } else if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    return true;
                }
                return false;
            }
        });
        et_number.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                final int DRAWABLE_END_ENGLISH = 2;
                final int DRAWABLE_END_ARABIC = 0;
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentLanguage.equals("en")) {
                        if (event.getRawX() >= (et_number.getRight() - et_number.getCompoundDrawables()[DRAWABLE_END_ENGLISH].getBounds().width())) {
                            // your action here
                            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                            startActivityForResult(intent, 1);
                            return true;
                        }
                    } else {
                        if (event.getX() <= (et_number.getCompoundDrawables()[DRAWABLE_END_ARABIC].getBounds().width())) {
                            // your action here
                            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                            startActivityForResult(intent, 1);
                            return true;
                        }
                    }
                }
                return false;
            }
        });

        et_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                isValid();
                if (et_number.getText().toString().length() == 0) {
                    til_number.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        et_number.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                } else {
                    isValid();
                }
            }
        });

    }

    private void initToolbar() {

        backImg = (Button) findViewById(R.id.backImg);
        backLayout = (RelativeLayout) findViewById(R.id.backLayout);
        helpLayout = (RelativeLayout) findViewById(R.id.helpLayout);
        headerLayoutID = (RelativeLayout) findViewById(R.id.headerLayoutID);
        helpBtn = (Button) findViewById(R.id.helpBtn);
        titleTxt = (TextView) findViewById(R.id.titleTxt);
    }

    private void setupToolbar() {
        titleTxt.setText("");
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard();
                onBackPressed();
//                Intent intent = new Intent(RegisterationActivity.this,TutorialActivity.class);
//                startActivity(intent);
            }
        });
//        helpBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(RegisterationActivity.this, HelpActivity.class);
//                startActivity(intent);
//            }
//        });
//        helpLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(RegisterationActivity.this, HelpActivity.class);
//                startActivity(intent);
//            }
//        });
        helpLayout.setVisibility(View.INVISIBLE);
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard();
                onBackPressed();
            }
        });
    }


    private void changeSquadNumber() {
        addFragmnet(new LoadingFragmnet(), R.id.frame_content, true);
        showFrame();
        AddSquadNumberModel addSquadNumberModel = new AddSquadNumberModel();
        addSquadNumberModel.setAppVersion(appVersion);
        addSquadNumberModel.setDeviceId(deviceId);
        addSquadNumberModel.setOsVersion(osVersion);
        addSquadNumberModel.setLang(currentLanguage);
        addSquadNumberModel.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        addSquadNumberModel.setToken(token);
        addSquadNumberModel.setChannel(channel);
        addSquadNumberModel.setImsi(sharedPrefrencesManger.getMobileNo());
        addSquadNumberModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        addSquadNumberModel.setAuthToken(authToken);
        if (squadNumber.getSquad() != null && squadNumber.getSquad().size() > 0 && squadNumber.getSquad().get(0) != null && squadNumber.getSquad().get(0).getMobileNumber() != null)
            addSquadNumberModel.setOldMobileNumber(squadNumber.getSquad().get(0).getMobileNumber());
        addSquadNumberModel.setNewMobileNumber(et_number.getText().toString());
        new ChangeSquadNumberService(this, addSquadNumberModel);

    }

    private void deleteSquadNumber() {
        addFragmnet(new LoadingFragmnet(), R.id.frame_content, true);
        showFrame();
        AddSquadNumberModel addSquadNumberModel = new AddSquadNumberModel();
        addSquadNumberModel.setAppVersion(appVersion);
        addSquadNumberModel.setDeviceId(deviceId);
        addSquadNumberModel.setOsVersion(osVersion);
        addSquadNumberModel.setLang(currentLanguage);
        addSquadNumberModel.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        addSquadNumberModel.setToken(token);
        addSquadNumberModel.setChannel(channel);
        addSquadNumberModel.setImsi(sharedPrefrencesManger.getMobileNo());
        addSquadNumberModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        addSquadNumberModel.setAuthToken(authToken);
        addSquadNumberModel.setMobileNumber(squadNumber.getSquad().get(0).getMobileNumber());
        new DeleteSquadNumberService(this, addSquadNumberModel);
    }

    private void showDeleteConfirmation() {
        SquadNumberDeleteFragment squadNumberDeleteFragment = new SquadNumberDeleteFragment();
        squadNumberDeleteFragment.setOnSquadNumberAddConfirmationListener(this);
        addFragmnet(squadNumberDeleteFragment, R.id.frame_content, true);
        showFrame();
    }

    private void showConfirmation(String responseMessage) {
        Bundle bundle = new Bundle();
        bundle.putString("addSquadNo", number_squad);
        bundle.putString("responseMessage", responseMessage);

        showFrame();
        SquadNumberConfirmationFragment squadNumberConfirmationFragment = new SquadNumberConfirmationFragment();
        squadNumberConfirmationFragment.setArguments(bundle);
        squadNumberConfirmationFragment.setOnSquadNumberAddConfirmationListener(this);
        addFragmnet(squadNumberConfirmationFragment, R.id.frame_content, true);
    }

    private void verifyAndAddNumber() {
        addFragmnet(new LoadingFragmnet(), R.id.frame_content, true);
        showFrame();
        AddSquadNumberModel addSquadNumberModel = new AddSquadNumberModel();
        addSquadNumberModel.setAppVersion(appVersion);
        addSquadNumberModel.setDeviceId(deviceId);
        addSquadNumberModel.setOsVersion(osVersion);
        addSquadNumberModel.setLang(currentLanguage);
        addSquadNumberModel.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        addSquadNumberModel.setToken(token);
        addSquadNumberModel.setChannel(channel);
        addSquadNumberModel.setImsi(sharedPrefrencesManger.getMobileNo());
        addSquadNumberModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        addSquadNumberModel.setAuthToken(authToken);
        addSquadNumberModel.setMobileNumber(number_squad);
        new AddSquadNumberService(this, addSquadNumberModel);

    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();

    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        if (responseModel.getResultObj() != null) {
            hideFrame();

            int j = 0;
            for (int i = 0; i < getSupportFragmentManager().getFragments().size(); i++) {

                if ((getSupportFragmentManager().getFragments().get(i) instanceof LoadingFragmnet)) {
                    j++;
                }

            }
            Fragment[] loadingFragments = new Fragment[j];
            for (int i = 0; i < j; i++) {
                loadingFragments[i] = new LoadingFragmnet();
            }
            removeFragment(loadingFragments);

            if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.validate_squad_number_service)) {
                ValidateSquadNumberModel transferDataResponseModel = (ValidateSquadNumberModel) responseModel.getResultObj();
                if (transferDataResponseModel.getStatus()) {
                    showConfirmation(transferDataResponseModel.getResponseMsg());
//                    onAddSuccess();
                } else {
//                    onAddSuccess();
                    if (transferDataResponseModel != null && transferDataResponseModel.getErrorMsgEn() != null) {
                        if (currentLanguage.equals("en")) {
                            showErrorFragment(transferDataResponseModel.getErrorMsgEn());
                        } else {

                            showErrorFragment(transferDataResponseModel.getErrorMsgAr());
                        }
                    } else {
                        showErrorFragment(getString(R.string.generice_error));
                    }
                }
            }

            if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.add_squad_number_service)) {
                SquadNumberOutput transferDataResponseModel = (SquadNumberOutput) responseModel.getResultObj();
                if (transferDataResponseModel.getStatus()) {
                    onAddSuccess();
                } else {
//                    onAddSuccess();
                    if (transferDataResponseModel != null && transferDataResponseModel.getErrorMsgEn() != null) {
                        if (currentLanguage.equals("en")) {
                            showErrorFragment(transferDataResponseModel.getErrorMsgEn());
                        } else {

                            showErrorFragment(transferDataResponseModel.getErrorMsgAr());
                        }
                    } else {
                        showErrorFragment(getString(R.string.generice_error));
                    }
                }
            }

            if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.delete_squad_number_service)) {
                SquadNumberOutput transferDataResponseModel = (SquadNumberOutput) responseModel.getResultObj();
                if (transferDataResponseModel.getStatus()) {
                    onDeleteSuccess();
                } else {
//                    onDeleteSuccess();
                    if (transferDataResponseModel != null && transferDataResponseModel.getErrorMsgEn() != null) {
                        if (currentLanguage.equals("en")) {
                            showErrorFragment(transferDataResponseModel.getErrorMsgEn());
                        } else {
                            showErrorFragment(transferDataResponseModel.getErrorMsgAr());
                        }
                    } else {
                        showErrorFragment(getString(R.string.generice_error));
                    }
                }
            }


            if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.change_squad_number_service)) {
                SquadNumberOutput transferDataResponseModel = (SquadNumberOutput) responseModel.getResultObj();
                if (transferDataResponseModel.getStatus()) {
                    onChangeSuccess();
                } else {
//                    onChangeSuccess();
                    if (transferDataResponseModel != null && transferDataResponseModel.getErrorMsgEn() != null) {
                        if (currentLanguage.equals("en")) {
                            showErrorFragment(transferDataResponseModel.getErrorMsgEn());
                        } else {

                            showErrorFragment(transferDataResponseModel.getErrorMsgAr());
                        }
                    } else {
                        showErrorFragment(getString(R.string.generice_error));
                    }
                }
            }
        }
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        try {
            MasterErrorResponse masterErrorResponse = (MasterErrorResponse) responseModel.getResultObj();
            if (masterErrorResponse != null && masterErrorResponse.getErrorMsgEn() != null) {
                if (currentLanguage.equals("en")) {
                    showErrorFragment(masterErrorResponse.getErrorMsgEn());
                } else {

                    showErrorFragment(masterErrorResponse.getErrorMsgEn());
                }
            } else {
                showErrorFragment(getString(R.string.generice_error));
            }
        } catch (Exception e) {
            e.printStackTrace();
//            onTransferSuccess();
            showErrorFragment(getString(R.string.generice_error));
        }
    }


    private void onAddSuccess() {
        Intent addIntent = new Intent();
        addIntent.putExtra("squadNumber", et_number.getText().toString());
        ll_squad_number_input.setVisibility(View.GONE);
        tv_squad_number.setVisibility(View.VISIBLE);
        tv_squad_number.setText(et_number.getText().toString());
        setChangeButton();
        Squad sqd = new Squad();
        sqd.setMobileNumber(number_squad);
        ArrayList<Squad> squads = new ArrayList<>();
        squads.add(0, sqd);
        if (squadNumber == null){
            squadNumber = new SquadNumber();
        }
        squadNumber.setSquad(squads);
        setResult(NewMembershipChartActivity.RESULT_ADD_NUMBER, addIntent);
//        finish();
    }

    private void onChangeSuccess() {
        Intent changeIntent = new Intent();
        changeIntent.putExtra("squadNumber", et_number.getText().toString());
        ll_squad_number_input.setVisibility(View.GONE);
        tv_squad_number.setVisibility(View.VISIBLE);
        tv_squad_number.setText(et_number.getText().toString());
        setChangeButton();
        if (squadNumber.getSquad().size() > 0)
            squadNumber.getSquad().get(0).setMobileNumber(number_squad);
        setResult(NewMembershipChartActivity.RESULT_CHANGE_NUMBER, changeIntent);
//        finish();
    }

    private void onDeleteSuccess() {
        et_number.setText("");
        ll_squad_number_input.setVisibility(View.VISIBLE);
        tv_squad_number.setVisibility(View.GONE);
        tv_squad_number.setText("");
        setAddBtn("Change");
        squadNumber.getSquad().remove(0);
        setResult(NewMembershipChartActivity.RESULT_DELETE_NUMBER);
    }

    private void displayError(String error) {
        tv_add_squad_error.setVisibility(View.VISIBLE);
        tv_add_squad_error.setText(error);
    }

    private void showErrorFragment(String error) {
        showFrame();
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, error);
        errorFragment.setArguments(bundle);
        replaceFragmnet(errorFragment, R.id.frame_content, true);
    }

    @Override
    public void onConfirmed() {
        hideFrame();
        onBackPressed();
        verifyAndAddNumber();
//        changeSquadNumber();
    }

    @Override
    public void onDelete() {
        hideFrame();
        onBackPressed();
        deleteSquadNumber();
    }

    @Override
    public void onCancelled() {
        hideFrame();
        onBackPressed();
    }


    private void showFrame() {
        frame_content.setVisibility(View.VISIBLE);
    }

    private void hideFrame() {
        frame_content.setVisibility(View.GONE);
    }


    private boolean isValid() {
        boolean retValue = true;


        if (et_number.getText().toString().length() < 9) {
            retValue = false;
            if (et_number.getText().toString().length() > 0) {
                til_number.setError(getString(R.string.ouch_number_does_not_seem_valid));
                til_number.requestFocus();
            }
        } else {
            String stringToCheck = et_number.getText().toString().replace(" ", "");
            stringToCheck = "+9710" + stringToCheck;
            String noStr = stringToCheck.substring(0, 2);
            if (!noStr.equalsIgnoreCase("05") && !stringToCheck.substring(0, 4).contains("+971") && !stringToCheck.substring(0, 5).contains("00971")) {

                retValue = false;
                if (et_number.getText().toString().length() > 0) {
                    til_number.setError(getString(R.string.ouch_number_does_not_seem_valid));
                }
            } else {
                number_squad = "0" + et_number.getText().toString();
                removeTextInputLayoutError(til_number);
            }
        }

        enableNextBtn(retValue);
        return retValue;
    }

    private void enableNextBtn(boolean enable) {
        bt_add_squad.setEnabled(enable);
        if (!enable)
            bt_add_squad.setAlpha(.5f);
        else
            bt_add_squad.setAlpha(1f);
    }

    private void setAddBtn(String tag) {
        bt_add_squad.setVisibility(View.VISIBLE);
        bt_change_squad.setVisibility(View.GONE);
        if (tag == null || tag.length() == 0)
            bt_add_squad.setTag("Add");
        else
            bt_add_squad.setTag(tag);
    }

    private void setChangeButton() {
        bt_add_squad.setVisibility(View.GONE);
        bt_change_squad.setVisibility(View.VISIBLE);
        bt_add_squad.setTag("Change");
    }

    private void validateSquadNumber() {
        addFragmnet(new LoadingFragmnet(), R.id.frame_content, true);
        showFrame();
        AddSquadNumberModel addSquadNumberModel = new AddSquadNumberModel();
        addSquadNumberModel.setAppVersion(appVersion);
        addSquadNumberModel.setDeviceId(deviceId);
        addSquadNumberModel.setOsVersion(osVersion);
        addSquadNumberModel.setLang(currentLanguage);
        addSquadNumberModel.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        addSquadNumberModel.setToken(token);
        addSquadNumberModel.setChannel(channel);
        addSquadNumberModel.setImsi(sharedPrefrencesManger.getMobileNo());
        addSquadNumberModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        addSquadNumberModel.setAuthToken(authToken);
        addSquadNumberModel.setMobileNumber(number_squad);
        new ValidateSquadNumberService(this, addSquadNumberModel);
    }

}
