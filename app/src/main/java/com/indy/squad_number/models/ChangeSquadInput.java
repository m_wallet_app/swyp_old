package com.indy.squad_number.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterInputResponse;

public class ChangeSquadInput extends MasterInputResponse implements Parcelable {


    @SerializedName("oldMobileNumber")
    @Expose
    private String oldMobileNumber;

    @SerializedName("newMobileNumber")
    @Expose
    private String newMobileNumber;

    public String getOldMobileNumber() {
        return oldMobileNumber;
    }

    public void setOldMobileNumber(String oldMobileNumber) {
        this.oldMobileNumber = oldMobileNumber;
    }

    public String getNewMobileNumber() {
        return newMobileNumber;
    }

    public void setNewMobileNumber(String newMobileNumber) {
        this.newMobileNumber = newMobileNumber;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.oldMobileNumber);
        dest.writeString(this.newMobileNumber);
    }

    public ChangeSquadInput() {
    }

    protected ChangeSquadInput(Parcel in) {
        this.oldMobileNumber = in.readString();
        this.newMobileNumber = in.readString();
    }

    public static final Parcelable.Creator<ChangeSquadInput> CREATOR = new Parcelable.Creator<ChangeSquadInput>() {
        @Override
        public ChangeSquadInput createFromParcel(Parcel source) {
            return new ChangeSquadInput(source);
        }

        @Override
        public ChangeSquadInput[] newArray(int size) {
            return new ChangeSquadInput[size];
        }
    };
}