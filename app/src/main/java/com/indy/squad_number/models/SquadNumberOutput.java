package com.indy.squad_number.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

public class SquadNumberOutput extends MasterErrorResponse {
    @SerializedName("status")
    @Expose
    private Boolean status;

    public void setStatus(Boolean updated) {
        this.status = updated;
    }

    public Boolean getStatus() {
        return status;
    }
}
