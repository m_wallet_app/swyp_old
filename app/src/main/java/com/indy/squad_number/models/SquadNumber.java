package com.indy.squad_number.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SquadNumber implements Parcelable {

    @SerializedName("maxSquadNumber")
    @Expose
    private String maxSquadNumber;
    @SerializedName("squad")
    @Expose
    private ArrayList<Squad> squad = new ArrayList<>();

    public String getMaxSquadNumber() {
        return maxSquadNumber;
    }

    public void setMaxSquadNumber(String maxSquadNumber) {
        this.maxSquadNumber = maxSquadNumber;
    }

    public ArrayList<Squad> getSquad() {
        return squad;
    }

    public void setSquad(ArrayList<Squad> squad) {
        this.squad = squad;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.maxSquadNumber);
        dest.writeList(this.squad);
    }

    public SquadNumber() {
    }

    protected SquadNumber(Parcel in) {
        this.maxSquadNumber = in.readString();
        this.squad = new ArrayList<Squad>();
        in.readList(this.squad, Squad.class.getClassLoader());
    }

    public static final Creator<SquadNumber> CREATOR = new Creator<SquadNumber>() {
        @Override
        public SquadNumber createFromParcel(Parcel source) {
            return new SquadNumber(source);
        }

        @Override
        public SquadNumber[] newArray(int size) {
            return new SquadNumber[size];
        }
    };
}