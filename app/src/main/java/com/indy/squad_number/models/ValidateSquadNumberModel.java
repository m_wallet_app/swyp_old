package com.indy.squad_number.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

public class ValidateSquadNumberModel extends MasterErrorResponse {
    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @SerializedName("responseCode")
    @Expose
    public String responseCode;
    @SerializedName("responseMsg")
    @Expose
    public String responseMsg;
    @SerializedName("status")
    @Expose
    public Boolean status;

}
