package com.indy.squad_number.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterInputResponse;

public class AddSquadNumberModel extends MasterInputResponse {

    @SerializedName("oldMobileNumber")
    @Expose
    private String oldMobileNumber;

    @SerializedName("newMobileNumber")
    @Expose
    private String newMobileNumber;

    @SerializedName("mobileNumber")
    @Expose
    private String mobileNumber;

    public String getOldMobileNumber() {
        return oldMobileNumber;
    }

    public void setOldMobileNumber(String oldMobileNumber) {
        this.oldMobileNumber = oldMobileNumber;
    }

    public String getNewMobileNumber() {
        return newMobileNumber;
    }

    public void setNewMobileNumber(String newMobileNumber) {
        this.newMobileNumber = newMobileNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

}
