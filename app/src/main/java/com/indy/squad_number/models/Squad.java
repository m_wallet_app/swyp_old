package com.indy.squad_number.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Squad implements Parcelable {

    @SerializedName("mobileNumber")
    @Expose
    private String mobileNumber;

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mobileNumber);
    }

    public Squad() {
    }

    protected Squad(Parcel in) {
        this.mobileNumber = in.readString();
    }

    public static final Creator<Squad> CREATOR = new Creator<Squad>() {
        @Override
        public Squad createFromParcel(Parcel source) {
            return new Squad(source);
        }

        @Override
        public Squad[] newArray(int size) {
            return new Squad[size];
        }
    };
}