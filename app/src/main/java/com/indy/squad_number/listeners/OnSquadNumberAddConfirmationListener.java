package com.indy.squad_number.listeners;

public interface OnSquadNumberAddConfirmationListener {

    void onConfirmed();
    void onCancelled();

}
