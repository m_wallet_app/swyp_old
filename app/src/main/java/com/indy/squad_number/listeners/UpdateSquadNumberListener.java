package com.indy.squad_number.listeners;

public interface UpdateSquadNumberListener {

    void onSetNewSquadNumber(String squadNumber);
    void onDeleteSquadNumber();

}
