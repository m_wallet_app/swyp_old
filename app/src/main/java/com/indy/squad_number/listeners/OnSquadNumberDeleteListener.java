package com.indy.squad_number.listeners;

public interface OnSquadNumberDeleteListener {

    void onDelete();
    void onCancelled();

}
