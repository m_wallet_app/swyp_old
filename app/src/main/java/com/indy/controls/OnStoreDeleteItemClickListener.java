package com.indy.controls;

import com.indy.models.bundles.BundleList;

/**
 * Created by emad on 10/18/16.
 */

public interface OnStoreDeleteItemClickListener {
    void onDeleteStoreItem(BundleList bundleList, int position);
}
