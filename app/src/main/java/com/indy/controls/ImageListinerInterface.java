package com.indy.controls;

import com.indy.models.utils.ImageItem;

import java.util.ArrayList;


/**
 * Created by Eabdelhamid on 11/17/2015.
 */
public interface ImageListinerInterface {
    void onImageSet(ImageItem imageItem);

    void onDeleteImage(ArrayList<ImageItem> imageItems, int imgPosition);
}
