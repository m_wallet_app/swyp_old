package com.indy.controls;

/**
 * Created by emad on 9/22/16.
 */

public interface OnDeleteSentTransferRequestListener {
    public void onSelectedRequest(int position);
}
