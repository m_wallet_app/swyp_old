package com.indy.controls;


import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by Emad on 13/08/2015.
 */
public interface BaseInterface {
    void onSucessListener(BaseResponseModel responseModel);

    void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse);

    void onErrorListener(BaseResponseModel responseModel);

    void initUI();

    void onConsumeService();

}
