package com.indy.controls;


import com.indy.views.fragments.gamification.models.getnotificationslist.NotificationModel;

/**
 * Created by mobile on 21/01/2018.
 */

public interface OnNotificationClickInterface {
    void onNotificationClick(NotificationModel notificationModel, int index);

}
