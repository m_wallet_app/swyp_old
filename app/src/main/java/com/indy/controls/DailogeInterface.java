package com.indy.controls;

/**
 * Created by Eabdelhamid on 2/9/2016.
 */
public interface DailogeInterface {
    void onAgreeListener(String alertType);

    void onCancelListener(String alertType);
}
