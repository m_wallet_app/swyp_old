package com.indy.controls;

/**
 * Created by emad on 9/22/16.
 */

public interface OnAcceptReceivedRequestListener {
    public void onAcceptReceivedRequest(int index,boolean isAmountEdited, String amount);
}
