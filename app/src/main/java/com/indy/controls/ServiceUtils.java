package com.indy.controls;

/**
 * Created by emad on 8/11/16.
 */
public interface ServiceUtils {
    String profileStatusServiceType = "PROFILESTATUSSERVICETYPE";
    String loginService = "LOGINSERVICE";
    String balanceServiceType = "BALANCESERVICETYPE";
    String packageServiceType = "PACKAGESERVICETYPE";
    String packageListType = "PACKAGELISTTYPE";
    String refreshBalance = "REFRESHBALANCE";
    String nearestLocationType = "NEARESTLOCATIONTTYPE";
    String inilizeRequestServiceType = "INILIZEREQUESTSERVICETYPE";
    String faqsService = "FAQSSERVICE";
    String lockNumberService = "LOCKNUMBERSERVICE";
    String isEligable = "ISELIGIBLE";
    String unLockNumberService = "UNLOCKNUMBERSERVICE";
    String msisdnService = "MSISDENSERVICE";
    String payPerUseService = "PAYPERUSESERVICE";
    String isLogin = "isLogin";

    String requestCredit = "REQUESTCREDIT";
    String getPendingTransfersList = "GETPENDINGTRANSFERSLIST";
    String transferCredit = "TRANSFERCREDIT";
    String sendReminder = "SENDREMINDER";
    String processRequest = "PROCESSREQUEST";
    String bundleStore = "BUNDLESTORE";
    String purchaseBundle = "PURCHASEBUNDLE";
    String updateProfile = "UPDATEPROFILE";
    String UploadImageInput = "UPLOADIMAGEINPUT";
    String VoucherCode = "VOUCHERCODE";
    String RedeemVoucher = "REDEEMVOUCHER";
    String rechargeCard = "RECHARGECARD";
    String rechargeCreditCard = "RECHARGECREDITCARD";
    String logoutService = "logout";
    String changePreferredNumber = "changePreferredNumber";
    String getUserMessage = "GETUSERMESSAGE";
    String benefitsText = "BENEFITSTEXT";
    String getCategories = "GETCATEGORIES";

    String isPreLogin = "ISPRELOGIN";
    String getShipmentStatus = "GETSHIPMENTSTATUS";
    String optInGamification = "optInGamification";
    String getEventsListService = "getEventsListService";
    String getEventDetailsService = "getEventDetailsService";
    String getEntryDetailsService = "getEntryDetailsService";
    String likeUnlikeEntryService = "likeUnlikeEntryService";
    String EventsFilter = "EventsFilter";
    String getWinnerListService = "getWinnersListService";
    String getCheckinLocationsService = "getCheckinsLocationService";
    String doCheckInService = "doCheckInService";
    String GET_BADGES_LIST = "GET_BADGES_LIST";
    String GET_IMAGES_FILTERS = "GET_IMAGES_FILTERS";
    String UPLOAD_EVENT_IMAGE = "UPLOAD_EVENT_IMAGE";
    String SOCIAL_SHARE = "SOCIAL_SHARE";
    String GET_RAFFLES_LIST = "GET_RAFFLES_LIST";

    String VIEW_LUCKY_DEAL = "VIEW_LUCKY_DEAL";
    String GET_BADGES_DETAILS = "GET_BADGES_DETAILS";
    String GET_NOTIFICATION_POPUP_DETAILS = "GET_NOTIFICATION_POPUP_DETAILS";
    String GET_RAFFLE_DETAILS = "GET_RAFFLE_DETAILS";
    String GET_USER_NOTIFICATION_SETTINGS = "GET_USER_NOTIFICATION_SETTINGS";
    String UPDATE_USER_NOTIFICATION_SETTING = "UPDATE_USER_NOTIFICATION_SETTING";
    String REGISTER_EVENT_INVITE_FRIEND = "REGISTER_EVENT_INVITE_FRIEND";
    String REGISTER_PURCHASE_STORE_SUCCESS = "REGISTER_PURCHASE_STORE_SUCCESS";
    String REGISTER_RECHARGE_SUCCESS = "REGISTER_RECHARGE_SUCCESS";
    String REGISTER_SHARE_EVENT = "REGISTER_SHARE_EVENT";

    String UPDATE_LANGUAGE = "UPDATE_LANGUAGE";
    String updateProfileGamification = "updateProfileGamification";
    String getRewardForSharingService = "getRewardForSharingService";

    String GET_CURRENT_EVENTS = "GET_CURRENT_EVENTS";
    String GET_PREVIOUS_EVENTS = "GET_PREVIOUS_EVENTS";
    String SEARCH_MSISDN = "SEARCH_MSISDN";
    String isVATEnabled = "IS_VAT_ENABLED";
   String GET_NOTIFICATIONS_LIST = "GET_NOTIFICATIONS_LIST";
    String GET_NOTIFICATION_DETAILS = "GET_NOTIFICATION_DETAILS";
    String LOG_USER_EVENT = "log_user_event";
    String new_get_registration_id_service = "new_get_registration_id_service";
    String VERIFY_PROMO_CODE = "verify_promo_code";
    String emirates_id_verify_new = "emirates_id_verify_new";
    String accept_terms_conditions_service = "accept_terms_conditions_service";
    String get_esim_eligibility_service = "get_esim_eligibility_service";

    String check_msisdn_service = "check_msisdn_service";
    String emirates_id_verify_old = "emirates_id_verify_old";
    String CHECK_ELIGIBILITY_FOR_MIGRATION = "CHECK_ELIGIBILITY_FOR_MIGRATION";
    String finalize_registration = "finalize_registration";
    String dataTransferInfo = "dataTransferInfo";
    String transferDataRequest = "transferDataRequest";
    String getAvailableData = "getAvailableData";

    String add_squad_number_service = "add_squad_number_service";
    String validate_squad_number_service = "validate_squad_number_service";
    String delete_squad_number_service = "delete_squad_number_service";
    String change_squad_number_service = "change_squad_number_service";
    String purchase_new_membership_service = "change_squad_number_service";

    String log_landing_page_service = "log_landing_page_service";
    String log_promo_code_page_service = "log_promo_code_page_service";
    String log_eid_details_service = "log_eid_details_service";
    String log_contact_details_service = "log_contact_details_service";
    String log_onboarding_details_service = "log_onboarding_details_service";
}
