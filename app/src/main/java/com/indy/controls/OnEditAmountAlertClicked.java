package com.indy.controls;

/**
 * Created by emad on 9/22/16.
 */

public interface OnEditAmountAlertClicked {
    public void onEditRequestBeforeSending(int index);
}
