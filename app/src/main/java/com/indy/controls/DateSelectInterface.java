package com.indy.controls;

/**
 * Created by emad on 8/8/16.
 */
public interface DateSelectInterface {
    void onDateSelect(String day,String month,String year);
}
