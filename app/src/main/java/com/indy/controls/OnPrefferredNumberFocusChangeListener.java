package com.indy.controls;

/**
 * Created by emad on 9/22/16.
 */

public interface OnPrefferredNumberFocusChangeListener {
    public void onFocusChanged(int index,String updatedNumber);
}
