package com.indy.controls;

/**
 * Created by emad on 9/22/16.
 */

public interface OnDeleteReceivedRequestListener {
    public void onDeleteReceivedRequest(int index);
}
