package com.indy.controls;

/**
 * Created by Tohamy on 2/28/2018.
 */

public interface AdjustEvents {
    String CLICKS_SIGN_UP_OR_SKIP = "cdlk9q";
    String CLICKS_GET_SIM = "5lamvw";
    String NUMBER_SELECTION_COMPLETE = "xxi6s2";
    String EID_PRECESS_INITIATED = "jvc9ur";
    String EID_PRECESS_COMPLETE = "v5j05c";
    String ALREADY_SWYP_CUSTOMER = "o4ylec";
    String AGE_ELIGIBILITY_SUCCESS = "gzjef3";
    String AGE_ELIGIBILITY_FAILURE = "rppfon";
    String PAYMENT_SELECTION_COMPLETE = "jfk7wc";
    String DELIVERY_SELECTION_INITIALIZED = "cg1ers";
    String DELIVERY_SELECTION_COMPLETE = "i2j6cj";
    String REVIEW_ORDER_SUMMARY = "80249r";
    String CREDIT_CARD_PAYMENT_INITIALIZED = "blxle1";
    String TRANSACTION_FAILED = "nqit13";
    String ORDER_COMPLETE_CREDIT_CARD = "2fnbmy";
    String ORDER_COMPLETE_COD = "mkr4k7";
    String ORDER_COMPLETE = "ct3m75";
    String NEAREST_STORE_LOCATION = "ts7r4u";
    String ORDER_NEXT = "7j5pro";
    String TnC_CONFIRM = "85mjvl";
    String MANUAL_EID = "y5vbek";
    String EMAIL_PHONE = "83b51q";
    String ADJUST_CC = "blxle1";
    String ADJUST_COD = "drorzn";
    String ADJUST_CHOOSE_CITY =  "j64ni0";
    String ADJUST_ADDRESS_MAP =  "yz6wv1";
}
