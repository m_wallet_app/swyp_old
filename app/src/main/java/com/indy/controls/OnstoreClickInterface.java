package com.indy.controls;

import com.indy.models.bundles.BundleList;

/**
 * Created by emad on 10/18/16.
 */

public interface OnstoreClickInterface {
    void onItemClick(BundleList bundleList, int index);
}
