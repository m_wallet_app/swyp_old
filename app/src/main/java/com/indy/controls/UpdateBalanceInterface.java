package com.indy.controls;

/**
 * Created by emad on 8/8/16.
 */
public interface UpdateBalanceInterface {
    void onBalanceUpdate(String updatedBalance);
}
