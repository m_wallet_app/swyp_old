package com.indy.controls;

import com.indy.BuildConfig;
import com.indy.dbt.models.GetAvailableDataResponseModel;
import com.indy.dbt.models.GetDataTransferInfoModel;
import com.indy.dbt.models.TransferDataRequestModel;
import com.indy.dbt.models.TransferDataResponseModel;
import com.indy.models.ChangePreferredNumberInputModel;
import com.indy.models.ForgotPassword.ForgotPasswordInputModel;
import com.indy.models.ForgotPassword.ForgotPasswordOutputModel;
import com.indy.models.LogEventInputModel;
import com.indy.models.MigrateEtisaltNumber.MigrateEtisalatNumerOutput;
import com.indy.models.acceptDeleteTransferProcesses.AcceptDeleteTransferProcessesInput;
import com.indy.models.acceptDeleteTransferProcesses.AcceptDeleteTransferProcessesOutput;
import com.indy.models.acceptTermsConditionsService.AccpetTermsConditionsServiceInput;
import com.indy.models.autorenewalOff.AutoRenewalInputModel;
import com.indy.models.balance.BalanceInputModel;
import com.indy.models.balance.BalanceResponseModel;
import com.indy.models.benefits.BenefitsOutputModel;
import com.indy.models.benefits.NewBenefitsModel;
import com.indy.models.bundles.BundleListOutPut;
import com.indy.models.changepassword.ChangePasswordInput;
import com.indy.models.changepassword.ChangePasswordOutput;
import com.indy.models.checkmisidenoperator.CheckMisidenOperatorOutput;
import com.indy.models.createnewaccount.CreateNewAccountInput;
import com.indy.models.createnewaccount.CreateNewAccountNewResponse;
import com.indy.models.createnewaccount.CreateNewAccountOutput;
import com.indy.models.createprofilestatus.CreateProfileStatusInput;
import com.indy.models.createprofilestatus.CreateProfileStatusoutput;
import com.indy.models.emiratesIDVerificationService.EmiratesIDVerificationInputModel;
import com.indy.models.emiratesIdVerification.VerifyEmiratesIdInput;
import com.indy.models.emiratesIdVerification.VerifyEmiratesIdOutput;
import com.indy.models.emiratesIdVerification.VerifyEmiratesIdOutputString;
import com.indy.models.esimActivation.ActivateESIMResponseModel;
import com.indy.models.esimActivation.CheckESIMEligibiltyResponseModel;
import com.indy.models.faqs.FaqsInputModel;
import com.indy.models.faqs.FaqsResponseModel;
import com.indy.models.finalizeregisteration.FinalizeRegisterationRequestInput;
import com.indy.models.finalizeregisteration.FinalizeRegisterationRequestOutput;
import com.indy.models.getPendingTransfersList.requestCreditFromFriend.GetPendingTransferListOutput;
import com.indy.models.getPendingTransfersList.requestCreditFromFriend.GetPendingTransfersListInput;
import com.indy.models.getShipmentStatus.GetShipmentStatusInput;
import com.indy.models.getShipmentStatus.GetShipmentStatusOutput;
import com.indy.models.getStaticBundles.StaticBundleOutput;
import com.indy.models.getTokenForPassword.GetPreLoginOutput;
import com.indy.models.getUserMessage.GetSuccessMessageOutputModel;
import com.indy.models.getUserMessage.GetUserMessageInputModel;
import com.indy.models.getUserMessage.GetUserMessageOutputModel;
import com.indy.models.history.HistoryDataOutput;
import com.indy.models.inilizeregisteration.InilizeRegisterationInput;
import com.indy.models.inilizeregisteration.InilizeRegisteratonOutput;
import com.indy.models.isEligable.IsEligableOutputModel;
import com.indy.models.isEligable.IsEligiableInputModel;
import com.indy.models.locaknumber.LockMobileNumberInput;
import com.indy.models.locaknumber.LockMobileNumberOutput;
import com.indy.models.location.LocationInputModel;
import com.indy.models.location.LocationModelResponse;
import com.indy.models.login.LoginInputModel;
import com.indy.models.login.LoginOutputModel;
import com.indy.models.logout.LogoutOutput;
import com.indy.models.msisdn.MsisdenListInput;
import com.indy.models.msisdn.MsisdenListResponse;
import com.indy.models.onboarding_push_notifications.GetOnBoardingDetailsRequestModel;
import com.indy.models.onboarding_push_notifications.GetOnBoardingDetailsResponseModel;
import com.indy.models.onboarding_push_notifications.InitializeServiceRequestModel;
import com.indy.models.onboarding_push_notifications.LandingPageRequestModel;
import com.indy.models.onboarding_push_notifications.LandingPageResponseModel;
import com.indy.models.onboarding_push_notifications.LogContactDetailsRequestModel;
import com.indy.models.onboarding_push_notifications.LogEIDDetailsRequestModel;
import com.indy.models.onboarding_push_notifications.LogPromoCodeDetailsRequestModel;
import com.indy.models.onboarding_push_notifications.OnboardingLogEventBaseResponse;
import com.indy.models.packages.UsagePackagesoutput;
import com.indy.models.packageusage.NewPackageUsageInfo;
import com.indy.models.packageusage.PackageUsageInput;
import com.indy.models.packageusage.PackageUsageOutput;
import com.indy.models.payperuse.PayPeruseOutput;
import com.indy.models.profile.EditProfileInput;
import com.indy.models.profile.EditProfileOutput;
import com.indy.models.profilestatus.NewProileStautsOutput;
import com.indy.models.profilestatus.ProfileStatusInput;
import com.indy.models.purchaseStoreBundle.PurchaseStoreInputResponse;
import com.indy.models.purchaseStoreBundle.PurchaseStoreOutput;
import com.indy.models.recharge.RechargeInput;
import com.indy.models.recharge.RechargeOutput;
import com.indy.models.rechargeCreditCard.CreditCardPaymentRequestInput;
import com.indy.models.rechargeCreditCard.RechargeCreditCardRequestOutput;
import com.indy.models.rechargeList.RechargeListOutput;
import com.indy.models.redeemVoucher.RedeemVoucherInput;
import com.indy.models.redeemVoucher.RedeemVoucherOutput;
import com.indy.models.requestCreditFromFriend.RequestCreditInput;
import com.indy.models.requestCreditFromFriend.RequestCreditOutput;
import com.indy.models.rewards.AvilableRewardsoutput;
import com.indy.models.rewards.GetCategoriesListInput;
import com.indy.models.rewards.GetCategoriesOutput;
import com.indy.models.rewards.UsedRewardsoutput;
import com.indy.models.search_msisdn_model.SearchMsisdnInputModel;
import com.indy.models.sendReminder.SendReminderInput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.squad_number.models.AddSquadNumberModel;
import com.indy.squad_number.models.ChangeSquadInput;
import com.indy.squad_number.models.Squad;
import com.indy.squad_number.models.SquadNumberOutput;
import com.indy.models.transferCredit.TransferCreditOutput;
import com.indy.models.unsubscribe.UnsubscribeHotspotOutput;
import com.indy.models.updateLanguage.UpdateLanguageInput;
import com.indy.models.updateLanguage.UpdateLanguageOutput;
import com.indy.models.uploadimages.UploadImageOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.models.utils.MasterInputResponse;
import com.indy.models.validateinvitioncode.ValidateInvitaionCodeInput;
import com.indy.models.validateinvitioncode.ValidateInvitaionCodeOutput;
import com.indy.models.validateotp.ValidateOtpInput;
import com.indy.models.validateotp.ValidateOtpOutput;
import com.indy.models.verfiymsisdnusingeid.VerfyMsisidnUsingEidInput;
import com.indy.models.verfiymsisdnusingeid.VerfyMsisidnUsingEidOutput;
import com.indy.models.verifyPromoCode.VerifyPromoCodeInputModel;
import com.indy.models.verifyPromoCode.VerifyPromoCodeOutputResponse;
import com.indy.models.voucherCode.vouchercodeInput;
import com.indy.models.voucherCode.vouchercodeOutput;
import com.indy.ocrStep2.ocr.UploadEmiratesIdOutput;
import com.indy.squad_number.models.ValidateSquadNumberModel;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.gamification.models.GetNotificationSettingsOutput;
import com.indy.views.fragments.gamification.models.UpdateUserNotificationSettingOutputResponse;
import com.indy.views.fragments.gamification.models.UpdateUserNotificationSettingsInput;
import com.indy.views.fragments.gamification.models.eventfilters.EventFilerInput;
import com.indy.views.fragments.gamification.models.eventfilters.EventFilterOutput;
import com.indy.views.fragments.gamification.models.getbadgesdetails.GetBadgeDetailsInputModel;
import com.indy.views.fragments.gamification.models.getbadgesdetails.GetBadgeDetailsOutputModel;
import com.indy.views.fragments.gamification.models.getbadgeslist.GetBadgesListInputModel;
import com.indy.views.fragments.gamification.models.getbadgeslist.GetBadgesListOutputModel;
import com.indy.views.fragments.gamification.models.getimagefilters.GetImageFiltersInput;
import com.indy.views.fragments.gamification.models.getimagefilters.GetImageFiltersOutput;
import com.indy.views.fragments.gamification.models.getnotificationdetails.GetNotificationDetailsInput;
import com.indy.views.fragments.gamification.models.getnotificationdetails.GetNotificationDetailsOutput;
import com.indy.views.fragments.gamification.models.getnotificationpopupdetails.GetNotificationPopupDetailsInput;
import com.indy.views.fragments.gamification.models.getnotificationpopupdetails.GetNotificationPopupDetailsOutput;
import com.indy.views.fragments.gamification.models.getnotificationslist.GetNotificationsListInput;
import com.indy.views.fragments.gamification.models.getnotificationslist.GetNotificationsListOutput;
import com.indy.views.fragments.gamification.models.getrafflesdetails.GetRafflesDetailsInput;
import com.indy.views.fragments.gamification.models.getrafflesdetails.GetRafflesDetailsOutputResponse;
import com.indy.views.fragments.gamification.models.getraffleslist.GetRafflesListInput;
import com.indy.views.fragments.gamification.models.getraffleslist.GetRafflesListOutput;
import com.indy.views.fragments.gamification.models.serviceInputOutput.DoCheckInInputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.DoCheckInOutputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GamificationBaseInputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetCheckinLocationsOutputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetEntryDetailsInputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetEntryDetailsOutputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetEventDetailInputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetEventDetailOutputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetEventsListInputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetEventsListOutputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetRewardForSharingInputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetRewardForSharingOutputResponse;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetWinnersListInputResponse;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetWinnersListOutputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.LikeUnlikeEntryInputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.LikeUnlikeEntryOutputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.LogShareEventInputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.OptInGamificationInputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.OptInGamificationOutputResponse;
import com.indy.views.fragments.gamification.models.serviceInputOutput.SuccessOutputResponse;
import com.indy.views.fragments.gamification.models.serviceInputOutput.UpdateProfileGamificationInputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.logPurchaseBundleEvent.LogPurchaseBundleEventInputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.rechargeBalance.LogRechargeBalanceEventInputModel;
import com.indy.views.fragments.gamification.models.socialshare.SocialShareInput;
import com.indy.views.fragments.gamification.models.socialshare.SocialShareOutput;
import com.indy.views.fragments.gamification.models.uploadeventimage.UploadEventImageOutput;
import com.indy.views.fragments.gamification.models.viewluckydealservice.ViewLuckyDealInput;
import com.indy.views.fragments.gamification.models.viewluckydealservice.ViewLuckyDealOutput;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

/**
 * This class contains all service URL's and model classes names. It also contains production base urls.
 */
public interface MyEndPointsInterface {

//    String imgBaseUrl = "https://uat-swypapp.etisalat.ae";              //uat url 2 (new)
//    String baseUrl = "https://uat-swypapp.etisalat.ae/Indy/rest/";    //uat url 2 (new)

    String imgBaseUrl = BuildConfig.imgBaseUrl;
    String baseUrl = BuildConfig.baseUrl;


//     public String imgBaseUrl = "https://sit-swyp.etisalat.ae";              //SIT  url
//     public String baseUrl = "https://sit-swyp.etisalat.`ae/Indy/rest/";    //SIT  url
//
//     public String imgBaseUrl = "https://uat-swyp.etisalat.ae";              //UAT URL
//     public String baseUrl = "https://uat-swyp.etisalat.ae/Indy/rest/";    //UAT URL

//     public String imgBaseUrl = " https://regtest-swyp.etisalat.ae";              //regression testing URL
//     public String baseUrl = " https://regtest-swyp.etisalat.ae/Indy/rest/";    //regression testing URL

//     public String imgBaseUrl = " https://preprodtest-swyp.etisalat.ae";              //pre prodcution URL
//     public String baseUrl = " https://preprodtest-swyp.etisalat.ae/Indy/rest/";    //pre prodcution URL


    @POST("allowance/balance")
    Call<BalanceResponseModel> getUserBalance(@Body BalanceInputModel balanceInputModel);

//    @POST("payment/init")
//    Call<PaymentOutputMode
// l> inilizePayment(@Body PaymentInputModel paymentModelInput);

    @POST("payment/init")
    Call<RechargeCreditCardRequestOutput> initializeCreditCardRechargePayment(@Body CreditCardPaymentRequestInput paymentModelInput);

    @POST("profile/register/complete")
    Call<FinalizeRegisterationRequestOutput> finalizeRegisteration(@Body FinalizeRegisterationRequestInput finalizeRegisterationRequestInput);

    @POST("allowance/package")
    Call<UsagePackagesoutput> getPackesList(@Body MasterInputResponse masterInputResponse);

    @POST("faf/package")
    Call<UsagePackagesoutput> getNewPackesList(@Body MasterInputResponse masterInputResponse);

    @POST("gis/shop/location")
    Call<LocationModelResponse> getNearestLocationService(@Body LocationInputModel locationInputModel);

    @POST("allowance/usage")
    Call<PayPeruseOutput> payPerUseService(@Body MasterInputResponse masterInputResponse);

    @POST("content/resource")
    Call<FaqsResponseModel> getFaqsService(@Body FaqsInputModel faqsInputModel);

    @POST("reservation/msisdn/list")
    Call<MsisdenListResponse> getMsisdnService(@Body MsisdenListInput msisdenListInput);

    @POST("profile/status")
    Call<NewProileStautsOutput> getProfileStatusService(@Body ProfileStatusInput profileStatusInput);

    @POST("profile/update")
    Call<EditProfileOutput> updateProfile(@Body EditProfileInput profileStatusInput);

    @POST("reservation/msisdn/reserve")
    Call<LockMobileNumberOutput> lockNumber(@Body LockMobileNumberInput lockMobileNumberInput);

    @POST("reservation/msisdn/unlock")
    Call<LockMobileNumberOutput> UnlockNumber(@Body LockMobileNumberInput lockMobileNumberInput);

    @POST("profile/login")
    Call<LoginOutputModel> loginService(@Body LoginInputModel loginInputModel);

    @POST("profile/password/forget")
    Call<ForgotPasswordOutputModel> forgotPasswordService(@Body ForgotPasswordInputModel forgotPasswordInputModel);

    @POST("profile/register/credentials")
    Call<CreateNewAccountOutput> setUserCredials(@Body CreateNewAccountInput changePasswordInput);

    @POST("profile/create/credentials")
    Call<CreateNewAccountNewResponse> createNewAccount(@Body CreateNewAccountInput changePasswordInput);

    @POST("profile/register/init")
    Call<InilizeRegisteratonOutput> inilizeRegisterationRequest(@Body InilizeRegisterationInput inilizeRegisterationInput);

    @POST("profile/invitation")
    Call<ValidateInvitaionCodeOutput> validateInvitationCodeService(@Body ValidateInvitaionCodeInput validateInvitaionCodeInput);

    @POST("migration/msisdn/operator")
    Call<CheckMisidenOperatorOutput> checkMisidenOperator(@Body MasterInputResponse masterInputResponse);

    @POST("migration/msisdn/verify")
    Call<VerfyMsisidnUsingEidOutput> verfiyMsisdnUsingEid(@Body VerfyMsisidnUsingEidInput verfyMsisidnUsingEidInput);

    @POST("migration/migrate")
    Call<MigrateEtisalatNumerOutput> migrationService(@Body MasterInputResponse masterInputResponse);

    @Multipart
    @POST("profile/picture/update")
    Call<UploadImageOutput> uploadImg(@Part("description") RequestBody description, @Part MultipartBody.Part filePart, @PartMap() Map<String, RequestBody> partMap);

    @POST("profile/password/change")
    Call<ChangePasswordOutput> changePasswordService(@Body ChangePasswordInput changePasswordInput);

    @POST("rewards/available")
    Call<AvilableRewardsoutput> getAvilableRewards(@Body MasterInputResponse masterInputResponse);

    @POST("rewards/used")
    Call<UsedRewardsoutput> getUsedRewards(@Body MasterInputResponse masterInputResponse);

    @POST("allowance/package/usage")
    Call<PackageUsageOutput> getPackageUsage(@Body PackageUsageInput masterInputResponse);

    @POST("faf/usage")
    Call<PackageUsageOutput> getNewPackageUsage(@Body NewPackageUsageInfo masterInputResponse);

    @POST("store/bundle/list")
    Call<StaticBundleOutput> getStaticPackagesList(@Body MasterInputResponse masterInputResponse);

    @POST("profile/create")
    Call<CreateProfileStatusoutput> createProfileStatus(@Body CreateProfileStatusInput masterInputResponse);

    @POST("profile/register/eligibility")
    Call<VerifyEmiratesIdOutput> verifyEmiratesIfForNewOrder(@Body VerifyEmiratesIdInput verifyEmiratesIdOutput);

    @POST("services/wifi/unsubscribe")
    Call<UnsubscribeHotspotOutput> unsubscribeHotspot(@Body MasterInputResponse masterInputResponse);

    @POST("services/transfer/request")
    Call<RequestCreditOutput> requestCredit(@Body RequestCreditInput requestCreditInput);

    @POST("services/transfer")
    Call<TransferCreditOutput> transferCredit(@Body RequestCreditInput requestCreditInput);

    @POST("services/transfer/list")
    Call<GetPendingTransferListOutput> getPendingList(@Body GetPendingTransfersListInput pendingTransfersListInput);

    @POST("services/transfer/pending/reminder")
    Call<TransferCreditOutput> sendReminder(@Body SendReminderInput sendReminderInput);

    @POST("services/transfer/pending/process")
    Call<AcceptDeleteTransferProcessesOutput> acceptDeleteTransferRequests(@Body AcceptDeleteTransferProcessesInput acceptDeleteTransferProcessesInput);

    @POST("store/bundle/list")
    Call<BundleListOutPut> getBundlesList(@Body MasterInputResponse masterInputResponse);

    @POST("store/recharge")
    Call<RechargeOutput> rechargeCard(@Body RechargeInput rechargeInput);

    @POST("profile/logout")
    Call<LogoutOutput> logoutUser(@Body MasterInputResponse logoutInput);

    @POST("store/recharge/list")
    Call<RechargeListOutput> rechargeList(@Body MasterInputResponse rechargeListInput);

    @POST("store/transaction/history")
    Call<HistoryDataOutput> historyList(@Body MasterInputResponse rechargeListInput);

    @POST("store/bundle/purchase")
    Call<PurchaseStoreOutput> purchaseStoreItems(@Body PurchaseStoreInputResponse purchaseStoreInputResponse);

    @POST("store/purchase/membership")
    Call<MasterErrorResponse> purchaseNewMembership(@Body MasterInputResponse purchaseStoreInputResponse);

    @POST("services/transfer/eligible")
    Call<IsEligableOutputModel> isEligible(@Body IsEligiableInputModel isEligiableInputModel);

    @POST("rewards/redeem")
    Call<RedeemVoucherOutput> RedeemVoucher(@Body RedeemVoucherInput redeemVoucherInput);

    @POST("rewards/voucher/code")
    Call<vouchercodeOutput> VoucherCode(@Body vouchercodeInput vouchercodeInput);

    @POST("store/bundle/update/prefnum")
    Call<PurchaseStoreOutput> changePreferredNumber(@Body ChangePreferredNumberInputModel changePreferredNumberInputModel);

    @POST("store/bundle/unsubscribe")
    Call<PurchaseStoreOutput> unsubSubscribe(@Body AutoRenewalInputModel changePreferredNumberInputModel);


    @POST("notification/otp/validate")
    Call<ValidateOtpOutput> validateOtp(@Body ValidateOtpInput validateOtpInput);

    @POST("content/message")
    Call<GetUserMessageOutputModel> getUserMessage(@Body GetUserMessageInputModel getUserMessageInputModel);

    @POST("content/membership/benefits")
    Call<BenefitsOutputModel> getBenefits(@Body MasterInputResponse masterInputResponse);

    @POST("content/membership/benefits-new")
    Call<NewBenefitsModel> getNewBenefits(@Body MasterInputResponse masterInputResponse);

    @POST("profile/prelogin")
    Call<GetPreLoginOutput> getPreLogin(@Body MasterInputResponse masterInputResponse);


    @POST("profile/update/language")
    Call<UpdateLanguageOutput> updateLanguage(@Body UpdateLanguageInput masterInputResponse);

    @POST("rewards/categories")
    Call<GetCategoriesOutput> getRewardsCategories(@Body GetCategoriesListInput masterInputResponse);

    @POST("profile/simshipmentstatus")
    Call<GetShipmentStatusOutput> getShipmentStatus(@Body GetShipmentStatusInput getShipmentStatusInput);

    @POST("gamification/user/optin")
    Call<OptInGamificationOutputResponse> optInGamification(@Body OptInGamificationInputModel optInGamificationInputModel);

    @POST("gamification/indyevents/list")
    Call<GetEventsListOutputModel> getEventsList(@Body GetEventsListInputModel getEventsListInputModel);

    @POST("gamification/indyevents/list/previous")
    Call<GetEventsListOutputModel> getPreviousEventsList(@Body GetEventsListInputModel getEventsListInputModel);


    @POST("gamification/indyevents/entry/details")
    Call<GetEntryDetailsOutputModel> getEntryDetails(@Body GetEntryDetailsInputModel getEntryDetailsInputModel);

    @POST("gamification/indyevents/entry/like")
    Call<LikeUnlikeEntryOutputModel> likeUnlikeEntry(@Body LikeUnlikeEntryInputModel likeUnlikeEntryInputModel);

    @POST("gamification/uicontrols/indyeventsdetail/sortoptions")
    Call<EventFilterOutput> getEventFilter(@Body EventFilerInput eventFilerInput);

    @POST("gamification/indyevents/details/winners")
    Call<GetWinnersListOutputModel> getEventWinnersList(@Body GetWinnersListInputResponse getWinnersListInputResponse);

    @POST("gamification/location/list")
    Call<GetCheckinLocationsOutputModel> getCheckinLocationsList(@Body DoCheckInInputModel gamificationBaseInputModel);

    @POST("gamification/location/checkin")
    Call<DoCheckInOutputModel> doCheckIn(@Body DoCheckInInputModel doCheckInOutputModel);

    @POST("gamification/badges/list")
    Call<GetBadgesListOutputModel> getBadgesList(@Body GetBadgesListInputModel getBadgesListInputModel);

    @POST("gamification/indyevents/uploadentry/filter")
    Call<GetImageFiltersOutput> getImageFilters(@Body GetImageFiltersInput getImageFiltersInput);

    @Multipart
    @POST("gamification/indyevents/uploadentry")
    Call<UploadEventImageOutput> uploadEventImage(@Part("description") RequestBody description, @Part MultipartBody.Part filePart, @PartMap() Map<String, RequestBody> partMap);

    @POST("gamification/social/share")
    Call<SocialShareOutput> socialShare(@Body SocialShareInput socialShareInput);

    @POST("gamification/raffles/list")
    Call<GetRafflesListOutput> getRafflesList(@Body GetRafflesListInput getRafflesListInput);

    @POST("gamification/location/checkin/viewluckydeal")
    Call<ViewLuckyDealOutput> viewLuckyDeal(@Body ViewLuckyDealInput viewLuckyDealInput);

    @POST("gamification/badges/details")
    Call<GetBadgeDetailsOutputModel> getBadgeDetails(@Body GetBadgeDetailsInputModel getBadgeDetailsInputModel);

    @POST("gamification/notifications/popup/details")
    Call<GetNotificationPopupDetailsOutput> getNotificationPopupDetails(@Body GetNotificationPopupDetailsInput getNotificationPopupDetailsInput);

    @POST("gamification/raffles/winners")
    Call<GetRafflesDetailsOutputResponse> getRaffleDetails(@Body GetRafflesDetailsInput getRafflesDetailsInput);

    @POST("profile/preferences/notifications/update")
    Call<UpdateUserNotificationSettingOutputResponse> updateUserNotificationSetting(@Body UpdateUserNotificationSettingsInput updateUserNotificationSettingsInput);

    @POST("profile/preferences/notifications")
    Call<GetNotificationSettingsOutput> getUserNotificationSetting(@Body MasterInputResponse masterInputResponse);

    @POST("gamification/user/invitefriend")
    Call<SuccessOutputResponse> registerInviteFriendEvent(@Body GamificationBaseInputModel gamificationBaseInputModel);

    @POST("gamification/actions/bundle/purchase")
    Call<SuccessOutputResponse> logPurchaseBundleEvent(@Body LogPurchaseBundleEventInputModel logPurchaseBundleEventInputModel);

    @POST("gamification/actions/rechargebalance")
    Call<SuccessOutputResponse> logRechargeBalanceEvent(@Body LogRechargeBalanceEventInputModel logRechargeBalanceEventInputModel);

    @POST("gamification/user/update")
    Call<SuccessOutputResponse> updateProfileGamification(@Body UpdateProfileGamificationInputModel updateProfileGamificationInputModel);

    @POST("gamification/actions/reward")
    Call<GetRewardForSharingOutputResponse> getRewardForShare(@Body GetRewardForSharingInputModel updateProfileGamificationInputModel);

    @POST("gamification/indyevents/details")
    Call<GetEventDetailOutputModel> getEventDetails(@Body GetEventDetailInputModel getEventDetailInputModel);

    @Multipart
    @POST("validate/emirates-id")
//    @Multipart
//    @POST("re-registration/ocr/proccess-emirate-id")
    Call<UploadEmiratesIdOutput> uploadEmiratesId(@Part MultipartBody.Part filePartFront, @Part MultipartBody.Part filePartBack, @PartMap() Map<String, RequestBody> partMap);

    @POST("gamification/notifications/list")
    Call<GetNotificationsListOutput> getNotificationsList(@Body GetNotificationsListInput getNotificationsListInput);


    @POST("gamification/notifications/details")
    Call<GetNotificationDetailsOutput> getNotificationDetails(@Body GetNotificationDetailsInput getNotificationDetailsInput);

    @POST("reservation/msisdn/search")
    Call<MsisdenListResponse> searchNumber(@Body SearchMsisdnInputModel searchMsisdnInputModel);
//    @POST("events/list/active")
//    Call<EventsOutputResponse> getCurrentEvents(@Body MasterInputResponse masterInputResponse);
//
//     @POST("events/list/previous")
//     Call<EventsOutputResponse> getPreviousEvents(@Body MasterInputResponse masterInputResponse);


    @POST("applogging/log")
    Call<GetSuccessMessageOutputModel> logAppEvent(@Body LogEventInputModel logEventInputModel);

    @POST("gamification/actions/add")
    Call<SuccessOutputResponse> registerShareEvent(@Body LogShareEventInputModel logShareEventInputModel);

    @POST("profile/register/initialize")
    Call<InilizeRegisteratonOutput> newGetRegistrationID(@Body InitializeServiceRequestModel masterInputResponse);

    @POST("profile/promocode/verify")
    Call<VerifyPromoCodeOutputResponse> verifyPromoCode(@Body VerifyPromoCodeInputModel verifyPromoCodeInputModel);

    //new registration id eligibility service
    @POST("profile/registration/eligibility")
    Call<VerifyEmiratesIdOutput> getEmiratesIDEligibilityNew(@Body EmiratesIDVerificationInputModel emiratesIDVerificationInputModel);

    //new registration id eligibility service
    @POST("profile/registration/acceptTermsAndConditions")
    Call<VerifyEmiratesIdOutputString> acceptTermsConditionsService(@Body AccpetTermsConditionsServiceInput accpetTermsConditionsServiceInput);

    @POST("esim/check-esim-eligibility-for-account")
    Call<CheckESIMEligibiltyResponseModel> getESIMEligibilityService(@Body MasterInputResponse masterInputResponse);

    @POST("esim/activate-esim-for-account")
    Call<ActivateESIMResponseModel> activateESIMService(@Body MasterInputResponse masterInputResponse);

    @POST("balance-transfer/get-available-data")
    Call<GetAvailableDataResponseModel> getAvailableData(@Body MasterInputResponse masterInputResponse);

    @POST("balance-transfer/get-data-transfer-info")
    Call<GetDataTransferInfoModel> getDataTransferInfo(@Body MasterInputResponse masterInputResponse);

    @POST("balance-transfer/data-transfer")
    Call<TransferDataResponseModel> transferData(@Body TransferDataRequestModel masterInputResponse);

    @POST("faf/add")
    Call<SquadNumberOutput> addSquadNumberService(@Body AddSquadNumberModel masterInputResponse);

    @POST("faf/delete")
    Call<SquadNumberOutput> deleteSquadNumberService(@Body AddSquadNumberModel masterInputResponse);

    @POST("faf/update")
    Call<SquadNumberOutput> changeSquadNumberService(@Body AddSquadNumberModel masterInputResponse);

    @POST("faf/validate")
    Call<ValidateSquadNumberModel> validateSquadNumberService(@Body AddSquadNumberModel masterInputResponse);

    @POST("profile/landing-page")
    Call<LandingPageResponseModel> logLandingPage(@Body LandingPageRequestModel masterInputResponse);

    @POST("profile/promo-code-details")
    Call<OnboardingLogEventBaseResponse> logPromoCodePage(@Body LogPromoCodeDetailsRequestModel masterInputResponse);

    @POST("profile/eid-details")
    Call<OnboardingLogEventBaseResponse> logEIDDetailsPage(@Body LogEIDDetailsRequestModel masterInputResponse);

    @POST("profile/contact-details")
    Call<OnboardingLogEventBaseResponse> logContactDetailsPage(@Body LogContactDetailsRequestModel masterInputResponse);

    @POST("profile/getDetails")
    Call<GetOnBoardingDetailsResponseModel> getOnboardingDetailsService(@Body GetOnBoardingDetailsRequestModel masterInputResponse);







}

