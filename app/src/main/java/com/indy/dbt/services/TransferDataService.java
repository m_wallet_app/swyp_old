package com.indy.dbt.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.dbt.models.GetDataTransferInfoModel;
import com.indy.dbt.models.TransferDataRequestModel;
import com.indy.dbt.models.TransferDataResponseModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.models.utils.MasterInputResponse;
import com.indy.services.BaseServiceManger;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to get user's subscribed packages.
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>Uses base request</p>
 * <p><b>Response</b></p>
 * <p>usagePackageList: Returns list of user packages with name, expiry date, current remaining usage etc.
 * </p>
 */
public class TransferDataService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private TransferDataRequestModel masterInputResponse;

    public TransferDataService(BaseInterface baseInterface, TransferDataRequestModel masterInputResponse) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.masterInputResponse = masterInputResponse;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<TransferDataResponseModel> call = apiService.transferData(masterInputResponse);
        call.enqueue(new Callback<TransferDataResponseModel>() {
            @Override
            public void onResponse(Call<TransferDataResponseModel> call, Response<TransferDataResponseModel> response) {
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.transferDataRequest);
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (response.body() != null && response.body().getErrorMsgEn() != null) {
                        String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<TransferDataResponseModel> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.transferDataRequest);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
