package com.indy.dbt.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.dbt.models.GetDataTransferInfoModel;
import com.indy.models.packages.UsagePackagesoutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterInputResponse;
import com.indy.services.BaseServiceManger;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to get user's subscribed packages.
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>Uses base request</p>
 * <p><b>Response</b></p>
 * <p>usagePackageList: Returns list of user packages with name, expiry date, current remaining usage etc.
 * </p>
 */
public class GetDataTransferInfoService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private MasterInputResponse masterInputResponse;

    public GetDataTransferInfoService(BaseInterface baseInterface, MasterInputResponse masterInputResponse) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.masterInputResponse = masterInputResponse;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<GetDataTransferInfoModel> call = apiService.getDataTransferInfo(masterInputResponse);
        call.enqueue(new Callback<GetDataTransferInfoModel>() {
            @Override
            public void onResponse(Call<GetDataTransferInfoModel> call, Response<GetDataTransferInfoModel> response) {
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.dataTransferInfo);
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                  if (response.body()!=null && response.body().getErrorMsgEn() != null) {                      String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<GetDataTransferInfoModel> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.dataTransferInfo);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
