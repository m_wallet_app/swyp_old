package com.indy.dbt.fragments;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.indy.R;
import com.indy.controls.ServiceUtils;
import com.indy.dbt.activities.DBTPagerActivity;
import com.indy.dbt.adapters.RechargeAmountAdapter;
import com.indy.dbt.listeners.OnBalanceUpdatedListener;
import com.indy.dbt.listeners.OnDataTransferConfirmedListener;
import com.indy.dbt.listeners.OnRecyclerViewItemClickListener;
import com.indy.dbt.models.DataAmountModel;
import com.indy.dbt.models.GetAvailableDataResponseModel;
import com.indy.dbt.models.GetDataTransferInfoModel;
import com.indy.dbt.models.TransferDataRequestModel;
import com.indy.dbt.models.TransferDataResponseModel;
import com.indy.dbt.services.GetAvailableDataService;
import com.indy.dbt.services.GetDataTransferInfoService;
import com.indy.dbt.services.TransferDataService;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.models.utils.MasterInputResponse;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.indy.views.fragments.utils.MasterFragment;

import java.text.DecimalFormat;
import java.util.ArrayList;

import retrofit2.converter.gson.GsonConverterFactory;

public class LCTMobileTransferFragment extends MasterFragment implements OnRecyclerViewItemClickListener, OnDataTransferConfirmedListener {

    private View baseView;
    private RecyclerView rv_ic_amount;
    private TextView tv_dbt_available_data, tv_dbt_data_unit, tv_dbt_remaining_transfers, tv_service_fee, tv_dbt_vat_amount, tv_total_amount;
    private EditText et_number, et_amount;
    private TextInputLayout til_number, til_amount;
    private RechargeAmountAdapter dataTransferAmountAdapter;
    private Button btn_next;
    private String amount;
    private String number_to;

    private double availableData;
    private int remainingTransfers;
    private double availableBalance;
    private ArrayList<DataAmountModel> listamount;
    private GetDataTransferInfoModel getDataTransferInfoModel;
    private GetAvailableDataResponseModel getAvailableDataResponseModel;
    private MasterInputResponse balanceInputModel;
    private double serviceFeePer100 = 3;
    private double data = 0, serviceFee = 0, vatAmount = 0, totalAmount = 0;
    private OnBalanceUpdatedListener onBalanceUpdatedListener;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        baseView = inflater.inflate(R.layout.fragment_lct_mobile_transfer, null, false);
        return baseView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI();
        getAvailableData();
        getDataTransferInfo();
//        onMockDataSuccess();
    }

    public void setOnBalanceUpdatedListener(OnBalanceUpdatedListener onBalanceUpdatedListener) {
        this.onBalanceUpdatedListener = onBalanceUpdatedListener;
    }

    private void getAvailableData() {
        ((DBTPagerActivity) getActivity()).addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        ((DBTPagerActivity) getActivity()).showFrame();
        balanceInputModel = new MasterInputResponse();
        balanceInputModel.setAppVersion(appVersion);
        balanceInputModel.setToken(token);
        balanceInputModel.setOsVersion(osVersion);
        balanceInputModel.setChannel(channel);
        balanceInputModel.setDeviceId(deviceId);
        balanceInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        balanceInputModel.setLang(currentLanguage);
        balanceInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
        balanceInputModel.setAuthToken(authToken);
        new GetAvailableDataService(this, balanceInputModel);
    }

    private void getDataTransferInfo() {

        ((DBTPagerActivity) getActivity()).addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        ((DBTPagerActivity) getActivity()).showFrame();
        balanceInputModel = new MasterInputResponse();
        balanceInputModel.setAppVersion(appVersion);
        balanceInputModel.setToken(token);
        balanceInputModel.setOsVersion(osVersion);
        balanceInputModel.setChannel(channel);
        balanceInputModel.setDeviceId(deviceId);
        balanceInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        balanceInputModel.setLang(currentLanguage);
        balanceInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
        balanceInputModel.setAuthToken(authToken);
        new GetDataTransferInfoService(this, balanceInputModel);
    }

    private void requestDataTransfer() {

        ((DBTPagerActivity) getActivity()).addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        ((DBTPagerActivity) getActivity()).showFrame();
        TransferDataRequestModel transferDataRequestModel = new TransferDataRequestModel();
        transferDataRequestModel.setAppVersion(appVersion);
        transferDataRequestModel.setToken(token);
        transferDataRequestModel.setOsVersion(osVersion);
        transferDataRequestModel.setChannel(channel);
        transferDataRequestModel.setDeviceId(deviceId);
        transferDataRequestModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        transferDataRequestModel.setLang(currentLanguage);
        transferDataRequestModel.setImsi(sharedPrefrencesManger.getMobileNo());
        transferDataRequestModel.setAuthToken(authToken);
        transferDataRequestModel.setAmount(Integer.parseInt(amount));
        transferDataRequestModel.setReceiverAccountNumber(number_to);
        transferDataRequestModel.setSenderAccountNumber(sharedPrefrencesManger.getMobileNo());
        new TransferDataService(this, transferDataRequestModel);
    }

    @Override
    public void initUI() {
        super.initUI();
        rv_ic_amount = baseView.findViewById(R.id.rv_amounts);
        til_number = baseView.findViewById(R.id.til_dbt_mobile_number);
        til_amount = baseView.findViewById(R.id.til_dbt_amount);
        et_number = baseView.findViewById(R.id.et_dbt_mobile_number);
        et_amount = baseView.findViewById(R.id.et_dbt_amount);

        tv_dbt_available_data = baseView.findViewById(R.id.tv_dbt_available_data);
        tv_dbt_data_unit = baseView.findViewById(R.id.tv_dbt_data_unit);
        tv_dbt_remaining_transfers = baseView.findViewById(R.id.tv_dbt_remaining_transfers);
        tv_service_fee = baseView.findViewById(R.id.tv_service_fee);
        tv_dbt_vat_amount = baseView.findViewById(R.id.tv_dbt_vat_amount);
        tv_total_amount = baseView.findViewById(R.id.tv_total_amount);

        btn_next = baseView.findViewById(R.id.nxtBtn);
        et_number.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                final int DRAWABLE_END_ENGLISH = 2;
                final int DRAWABLE_END_ARABIC = 0;
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentLanguage.equals("en")) {
                        if (event.getRawX() >= (et_number.getRight() - et_number.getCompoundDrawables()[DRAWABLE_END_ENGLISH].getBounds().width())) {
                            // your action here
                            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                            startActivityForResult(intent, 1);
                            return true;
                        }
                    } else {
                        if (event.getX() <= (et_number.getCompoundDrawables()[DRAWABLE_END_ARABIC].getBounds().width())) {
                            // your action here
                            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                            startActivityForResult(intent, 1);
                            return true;
                        }
                    }
                }
                return false;
            }
        });


        et_number.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {

                } else {
                    isValid();
                }
            }
        });

        et_amount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {

                } else {
                    Integer amountRound = Integer.parseInt(et_amount.getText().toString());
                    if (amountRound > 100) {
                        amountRound = amountRound - (amountRound % 100);
                        et_amount.setText(amountRound + "");
                        setAmounts(et_amount.getText().toString());
                    }
                    isValid();
                }
            }
        });

        et_amount.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                switch (actionId) {

                    case EditorInfo.IME_ACTION_DONE:
                        Integer amountRound = Integer.parseInt(et_amount.getText().toString());
                        if (amountRound > 100) {
                            amountRound = amountRound - (amountRound % 100);
                            et_amount.setText(amountRound + "");
                            setAmounts(v.getText().toString());
                        }
                        isValid();
                        break;
                }


                return false;
            }
        });

        et_amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String enteredAmount = s.toString();
                if (enteredAmount != null && enteredAmount.length() > 0) {
                    setAmounts(enteredAmount);
                    isValid();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyBoard();
                if (isValid()) {
                    Bundle bundle = new Bundle();
                    bundle.putString(DataTransferConfirmationFragment.KEY_DATA_MB, amount);
                    bundle.putString(DataTransferConfirmationFragment.KEY_MSISDN, number_to);
                    bundle.putString(DataTransferConfirmationFragment.KEY_AMOUNT_CHARGES, "" + totalAmount);
                    DataTransferConfirmationFragment dataTransferConfirmationFragment = new DataTransferConfirmationFragment();
                    dataTransferConfirmationFragment.setArguments(bundle);
                    dataTransferConfirmationFragment.setOnDataTransferConfirmedListener(LCTMobileTransferFragment.this);
                    ((DBTPagerActivity) getActivity()).showFrame();
                    ((DBTPagerActivity) getActivity()).addFragmnet(dataTransferConfirmationFragment, R.id.frameLayout, true);
                }
            }
        });

        disableNextBtn();


    }

    private void initializeInfo(GetDataTransferInfoModel getDataTransferInfoModel) {

        tv_dbt_remaining_transfers.setText("" + getDataTransferInfoModel.getAttemptsLeft());

        if (getDataTransferInfoModel.getAttemptsLeft() == null || getDataTransferInfoModel.getAttemptsLeft() <= 0) {
            et_number.setEnabled(false);
            et_amount.setEnabled(false);
            rv_ic_amount.setEnabled(false);
            rv_ic_amount.setClickable(false);
        }

        availableData = getDataTransferInfoModel.getMaximumData();
        remainingTransfers = getDataTransferInfoModel.getAttemptsLeft();
        availableBalance = DBTPagerActivity.availableBalance;
    }

    private void setAmounts(String dataAmount) {
        if (dataAmount != null && dataAmount.length() > 0) {
            data = Integer.parseInt(dataAmount);
            serviceFee = ((data / 100) * serviceFeePer100);
            serviceFee = Math.round(serviceFee * 100D) / 100D;
            vatAmount = ((serviceFee / 100) * 5);
            vatAmount = Math.round(vatAmount * 100D) / 100D;
            totalAmount = serviceFee + vatAmount;
            totalAmount = Math.round(totalAmount * 100D) / 100D;
            tv_service_fee.setText("" + serviceFee);
            tv_dbt_vat_amount.setText("" + vatAmount);
            tv_total_amount.setText("" + totalAmount);
        }

    }

    private void setRechargeAmmountList() {

        listamount = new ArrayList<>();
        if (getDataTransferInfoModel.getOptions() != null && getDataTransferInfoModel.getOptions().size() > 0) {
            for (int i = 0; i < getDataTransferInfoModel.getOptions().size(); i++) {
                listamount.add(new DataAmountModel(i, getDataTransferInfoModel.getOptions().get(i), false));
            }
        }
        rv_ic_amount.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, true));
        dataTransferAmountAdapter = new RechargeAmountAdapter((DBTPagerActivity) getActivity(), listamount, this);
        rv_ic_amount.setAdapter(dataTransferAmountAdapter);
        if (remainingTransfers <= 0)
            dataTransferAmountAdapter.setOnItemClickListener(null);
        else
            dataTransferAmountAdapter.setOnItemClickListener(this);
        dataTransferAmountAdapter.notifyDataSetChanged();
    }


    private boolean itemSelected = false;


    private boolean isItemSelected(boolean value) {
        itemSelected = value;
        return itemSelected;
    }

    private boolean isValid() {
        boolean retValue = true;

        if (remainingTransfers <= 0) {
            return false;
        }
        if (et_number.getText().toString().length() < 10) {
            retValue = false;
            if (et_number.getText().toString().length() > 0) {
                til_number.setError(getString(R.string.ouch_number_does_not_seem_valid));
                til_number.requestFocus();
            }
        } else {
            String stringToCheck = et_number.getText().toString().replace(" ", "");
            String noStr = stringToCheck.substring(0, 2);
            if (!noStr.equalsIgnoreCase("05") && !stringToCheck.substring(0, 4).contains("+971") && !stringToCheck.substring(0, 5).contains("00971")) {

                retValue = false;
                if (et_number.getText().toString().length() > 0) {
                    til_number.setError(getString(R.string.ouch_number_does_not_seem_valid));
                }
            } else {
                number_to = et_number.getText().toString();
                removeTextInputLayoutError(til_number);
            }
        }
        amount = et_amount.getText().toString();
        amount = amount.replace(" ", "");
        try {
            if (amount.length() > 0 &&
                    Integer.parseInt(amount) >= 100 &&
                    Integer.parseInt(amount) <= availableData &&
                    totalAmount < availableBalance) {
                removeTextInputLayoutError(til_amount);
            } else {
                retValue = false;
                if (amount.length() > 0 && Integer.parseInt(amount) < 100)
                    setTextInputLayoutError(til_amount, getString(R.string.data_amount_not_correct));
                else if (amount.length() > 0 && Integer.parseInt(amount) > availableData) {
                    setTextInputLayoutError(til_amount, getString(R.string.data_amount_not_available));
                } else if (totalAmount > availableBalance) {
                    setTextInputLayoutError(til_amount, getString(R.string.insufficent_balance));
                }
            }
        } catch (NumberFormatException ex) {
            retValue = false;
            if (amount.length() > 0)
                setTextInputLayoutError(til_amount, getString(R.string.data_amount_not_correct));
        }


        if (retValue) {
            enableNextBtn();
        } else {
            disableNextBtn();
        }
        return retValue;
    }

    @Override
    public void onItemClick(View view, int position) {
        DataAmountModel dataAmountModel = listamount.get(position);
        if (dataAmountModel != null) {
            et_amount.setText("" + dataAmountModel.getAmount());
        }
        isItemSelected(true);
        dataTransferAmountAdapter.notifyDataSetChanged();
    }


    private void enableNextBtn() {
        btn_next.setAlpha(1f);
        btn_next.setClickable(true);
    }

    private void disableNextBtn() {
        btn_next.setAlpha(.2f);
        btn_next.setClickable(false);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (data != null && data.getData() != null) {
                String phoneNo = null;
                Uri uri = data.getData();
                Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);

                if (cursor.moveToFirst()) {
                    int phoneIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                    phoneNo = cursor.getString(phoneIndex);
                }

                cursor.close();

                et_number.setText(phoneNo);

            }
        }
    }

//    private void onMockDataSuccess() {
//
//        getDataTransferInfoModel = new Gson().fromJson(ConstantUtils.DATA_TRANSFER_GET_INFO, GetDataTransferInfoModel.class);
////                getDataTransferInfoModel = (GetDataTransferInfoModel) responseModel.getResultObj();
//        if (getDataTransferInfoModel != null) {
//            initializeInfo(getDataTransferInfoModel);
//            setRechargeAmmountList();
//        } else {
//
//        }
//
//    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        if (responseModel.getResultObj() != null) {
            if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.dataTransferInfo)) {
                ((DBTPagerActivity) getActivity()).hideFrame();
                int j = 0;
                for (int i = 0; i < ((DBTPagerActivity) getActivity()).getSupportFragmentManager().getFragments().size(); i++) {

                    if (((DBTPagerActivity) getActivity()).getSupportFragmentManager().getFragments().get(i) instanceof LoadingFragmnet) {
                        j++;
                    }

                }
                Fragment[] loadingFragments = new Fragment[j];
                for (int i = 0; i < j; i++) {
                    loadingFragments[i] = new LoadingFragmnet();
                }
                ((DBTPagerActivity) getActivity()).removeFragment(loadingFragments);
                getDataTransferInfoModel = (GetDataTransferInfoModel) responseModel.getResultObj();
                if (getDataTransferInfoModel != null) {
                    initializeInfo(getDataTransferInfoModel);
                    setRechargeAmmountList();
                } else {
                    if (getDataTransferInfoModel != null && getDataTransferInfoModel.getErrorMsgEn() != null) {
                        if (currentLanguage.equals("en")) {
                            showErrorFragment(getDataTransferInfoModel.getErrorMsgEn());
                        } else {

                            showErrorFragment(getDataTransferInfoModel.getErrorMsgEn());
                        }
                    } else {
                        showErrorFragment(getString(R.string.generice_error));
                    }
                }
            } else if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.getAvailableData)) {
                ((DBTPagerActivity) getActivity()).hideFrame();
                int j = 0;
                for (int i = 0; i < ((DBTPagerActivity) getActivity()).getSupportFragmentManager().getFragments().size(); i++) {

                    if (((DBTPagerActivity) getActivity()).getSupportFragmentManager().getFragments().get(i) instanceof LoadingFragmnet) {
                        j++;
                    }

                }
                Fragment[] loadingFragments = new Fragment[j];
                for (int i = 0; i < j; i++) {
                    loadingFragments[i] = new LoadingFragmnet();
                }
                ((DBTPagerActivity) getActivity()).removeFragment(loadingFragments);
                getAvailableDataResponseModel = (GetAvailableDataResponseModel) responseModel.getResultObj();
                if (getAvailableDataResponseModel != null) {
                    availableData = getAvailableDataResponseModel.getAvailableData();
                    if (getAvailableDataResponseModel.getAvailableData() >= 1000) {
                        tv_dbt_available_data.setText(new DecimalFormat("##.#").format(getAvailableDataResponseModel.getAvailableData() / 1000));
//                        tv_dbt_available_data.setText("" + (double) Math.round((getAvailableDataResponseModel.getAvailableData() / 1000) * 100) / 100);
                        tv_dbt_data_unit.setText("GB");
                    } else {
                        tv_dbt_available_data.setText(new DecimalFormat("##.#").format(getAvailableDataResponseModel.getAvailableData()));
//                        tv_dbt_available_data.setText("" + (double) Math.round((getAvailableDataResponseModel.getAvailableData()) * 100) / 100);
                        tv_dbt_data_unit.setText("MB");
                    }
                } else {
                    if (getAvailableDataResponseModel != null && getAvailableDataResponseModel.getErrorMsgEn() != null) {
                        if (currentLanguage.equals("en")) {
                            showErrorFragment(getAvailableDataResponseModel.getErrorMsgEn());
                        } else {

                            showErrorFragment(getAvailableDataResponseModel.getErrorMsgEn());
                        }
                    } else {
                        showErrorFragment(getString(R.string.generice_error));
                    }
                }
            } else if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.transferDataRequest)) {
                ((DBTPagerActivity) getActivity()).hideFrame();
                int j = 0;
                for (int i = 0; i < ((DBTPagerActivity) getActivity()).getSupportFragmentManager().getFragments().size(); i++) {

                    if (((DBTPagerActivity) getActivity()).getSupportFragmentManager().getFragments().get(i) instanceof LoadingFragmnet) {
                        j++;
                    }

                }
                Fragment[] loadingFragments = new Fragment[j];
                for (int i = 0; i < j; i++) {
                    loadingFragments[i] = new LoadingFragmnet();
                }
                ((DBTPagerActivity) getActivity()).removeFragment(loadingFragments);
                TransferDataResponseModel transferDataResponseModel = (TransferDataResponseModel) responseModel.getResultObj();
                if (transferDataResponseModel.isRewarded()) {
                    onTransferSuccess();
                } else {
                    if (transferDataResponseModel != null && transferDataResponseModel.getResponseMsg() != null) {
                        if (currentLanguage.equals("en")) {
                            showErrorFragment(transferDataResponseModel.getResponseMsg());
                        } else {
                            showErrorFragment(transferDataResponseModel.getResponseMsg());
                        }
                    } else {
                        showErrorFragment(getString(R.string.generice_error));
                    }
                }
            }
        }
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        try {
            MasterErrorResponse masterErrorResponse = (MasterErrorResponse) responseModel.getResultObj();
            if (masterErrorResponse != null && masterErrorResponse.getErrorMsgEn() != null) {
                if (currentLanguage.equals("en")) {
                    showErrorFragment(masterErrorResponse.getErrorMsgEn());
                } else {

                    showErrorFragment(masterErrorResponse.getErrorMsgEn());
                }
            } else {
                showErrorFragment(getString(R.string.generice_error));
            }
        } catch (Exception e) {
            e.printStackTrace();
//            onTransferSuccess();
            showErrorFragment(getString(R.string.generice_error));
        }
    }


    private void onTransferSuccess() {
        Bundle bundle = new Bundle();
        bundle.putString(DataTransferConfirmationFragment.KEY_DATA_MB, amount);
        bundle.putString(DataTransferConfirmationFragment.KEY_MSISDN, number_to);
        DataTransferSuccessfulFragment dataTransferConfirmation = new DataTransferSuccessfulFragment();
        dataTransferConfirmation.setArguments(bundle);
        dataTransferConfirmation.setOnDataTransferConfirmedListener(LCTMobileTransferFragment.this);
        ((DBTPagerActivity) getActivity()).showFrame();
        ((DBTPagerActivity) getActivity()).addFragmnet(dataTransferConfirmation, R.id.frameLayout, true);
    }

    @Override
    public void onCancelled() {
        ((DBTPagerActivity) getActivity()).onBackPressed();
        ((DBTPagerActivity) getActivity()).hideFrame();
    }

    @Override
    public void onConfirmed() {
        ((DBTPagerActivity) getActivity()).onBackPressed();
        ((DBTPagerActivity) getActivity()).hideFrame();
        requestDataTransfer();
    }

    @Override
    public void onOk() {
        ((DBTPagerActivity) getActivity()).onBackPressed();
        ((DBTPagerActivity) getActivity()).hideFrame();
        if (this.onBalanceUpdatedListener != null) {
            this.onBalanceUpdatedListener.onBalanceUpdated();
        }
        ((DBTPagerActivity) getActivity()).finish();
    }

    private void showErrorFragment(String error) {
        ((DBTPagerActivity) getActivity()).showFrame();
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, error);
        bundle.putBoolean(ErrorFragment.KEY_DBT_LISTENER, true);
        errorFragment.setArguments(bundle);
        errorFragment.setOnDataTransferConfirmedListener(this);
        ((DBTPagerActivity) getActivity()).replaceFragmnet(errorFragment, R.id.frameLayout, true);
    }
}
