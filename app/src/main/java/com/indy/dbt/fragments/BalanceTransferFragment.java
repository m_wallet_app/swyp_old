package com.indy.dbt.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indy.R;
import com.indy.dbt.activities.AlertsActivity;
import com.indy.dbt.activities.DBTPagerActivity;
import com.indy.dbt.activities.RequestActivity;
import com.indy.dbt.activities.TransferActivity;
import com.indy.dbt.listeners.OnRequestSuccessfulListener;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.store.request.AlertsFragment;
import com.indy.views.fragments.store.request.RequestFragment;
import com.indy.views.fragments.store.request.TransferFragment;
import com.indy.views.fragments.utils.MasterFragment;

public class BalanceTransferFragment extends MasterFragment implements View.OnClickListener, OnRequestSuccessfulListener {

    private View baseView;

    CardView cv_dbt_balance_request;
    CardView cv_dbt_balance_send;
    CardView cv_dbt_balance_alerts;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("onActivityResult", "onActivityResult");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        baseView = inflater.inflate(R.layout.fragment_balance_transfer, null, false);
        return baseView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI();
    }

     public void initUI(){
        super.initUI();
        cv_dbt_balance_alerts = baseView.findViewById(R.id.cv_dbt_balance_alerts);
        cv_dbt_balance_request = baseView.findViewById(R.id.cv_dbt_balance_request);
        cv_dbt_balance_send = baseView.findViewById(R.id.cv_dbt_balance_send);
        cv_dbt_balance_alerts.setOnClickListener(this);
        cv_dbt_balance_request.setOnClickListener(this);
        cv_dbt_balance_send.setOnClickListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((DBTPagerActivity) getActivity()).setOnRequestSuccessfulListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.cv_dbt_balance_alerts:

                startActivity(new Intent(getActivity(), AlertsActivity.class));
//                ((DBTPagerActivity) getActivity()).addFragmnet(new AlertsFragment(), R.id.frameLayout, true);
//                ((DBTPagerActivity) getActivity()).showFrame();
                CommonMethods.logFirebaseEvent(mFirebaseAnalytics, ConstantUtils.TAGGING_transfer_view_alerts);
                break;

            case R.id.cv_dbt_balance_request:

                ((DBTPagerActivity)getActivity()).startActivityForResult(new Intent(getActivity(), RequestActivity.class),  DBTPagerActivity.REQUEST_CODE_FROM_REQUEST);
//                ((DBTPagerActivity) getActivity()).addFragmnet(new RequestFragment(), R.id.frameLayout, true);
//                ((DBTPagerActivity) getActivity()).showFrame();
                CommonMethods.logFirebaseEvent(mFirebaseAnalytics, ConstantUtils.TAGGING_transfer_request_tab);
                break;

            case R.id.cv_dbt_balance_send:

                ((DBTPagerActivity)getActivity()).startActivityForResult(new Intent(getActivity(), TransferActivity.class), DBTPagerActivity.REQUEST_CODE_FROM_TRANSFER);
//                ((DBTPagerActivity) getActivity()).addFragmnet(new TransferFragment(), R.id.frameLayout, true);
//                ((DBTPagerActivity) getActivity()).showFrame();
                CommonMethods.logFirebaseEvent(mFirebaseAnalytics, ConstantUtils.TAGGING_transfer_tab);
                break;



        }

    }

    @Override
    public void onRequestSuccessful() {
        startActivity(new Intent(getActivity(), AlertsActivity.class));
//                ((DBTPagerActivity) getActivity()).addFragmnet(new AlertsFragment(), R.id.frameLayout, true);
//                ((DBTPagerActivity) getActivity()).showFrame();
        CommonMethods.logFirebaseEvent(mFirebaseAnalytics, ConstantUtils.TAGGING_transfer_view_alerts);
    }
}



