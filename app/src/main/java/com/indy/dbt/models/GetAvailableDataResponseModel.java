package com.indy.dbt.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

public class GetAvailableDataResponseModel extends MasterErrorResponse {

    @SerializedName("availableData")
    @Expose
    private Double availableData;

    public Double getAvailableData() {
        return availableData;
    }

    public void setAvailableData(Double availableData) {
        this.availableData = availableData;
    }
}
