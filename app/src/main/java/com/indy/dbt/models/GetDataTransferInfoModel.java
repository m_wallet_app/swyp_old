package com.indy.dbt.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

import java.util.List;

public class GetDataTransferInfoModel extends MasterErrorResponse {

    @SerializedName("chargesPerMB")
    @Expose
    private Double chargesPerMB;
    @SerializedName("minimumData")
    @Expose
    private Integer minimumData;
    @SerializedName("maximumData")
    @Expose
    private Integer maximumData;
    @SerializedName("attemptsLeft")
    @Expose
    private Integer attemptsLeft;
    @SerializedName("daysLeft")
    @Expose
    private Integer daysLeft;
    @SerializedName("options")
    @Expose
    private List<Integer> options = null;



    public Double getChargesPerMB() {
        return chargesPerMB;
    }

    public void setChargesPerMB(Double chargesPerMB) {
        this.chargesPerMB = chargesPerMB;
    }

    public Integer getMinimumData() {
        return minimumData;
    }

    public void setMinimumData(Integer minimumData) {
        this.minimumData = minimumData;
    }

    public Integer getMaximumData() {
        return maximumData;
    }

    public void setMaximumData(Integer maximumData) {
        this.maximumData = maximumData;
    }

    public Integer getAttemptsLeft() {
        return attemptsLeft;
    }

    public void setAttemptsLeft(Integer attemptsLeft) {
        this.attemptsLeft = attemptsLeft;
    }

    public Integer getDaysLeft() {
        return daysLeft;
    }

    public void setDaysLeft(Integer daysLeft) {
        this.daysLeft = daysLeft;
    }

    public List<Integer> getOptions() {
        return options;
    }

    public void setOptions(List<Integer> options) {
        this.options = options;
    }

}
