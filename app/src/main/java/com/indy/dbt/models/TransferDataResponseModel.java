package com.indy.dbt.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

public class TransferDataResponseModel extends MasterErrorResponse {

    @SerializedName("isRewarded")
    @Expose
    private boolean isRewarded;


    @SerializedName("responseMsg")
    @Expose
    private String responseMsg;


    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public boolean isRewarded() {
        return isRewarded;
    }

    public void setRewarded(boolean rewarded) {
        isRewarded = rewarded;
    }
}
