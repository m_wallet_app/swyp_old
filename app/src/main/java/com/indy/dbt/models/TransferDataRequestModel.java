package com.indy.dbt.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterInputResponse;

public class TransferDataRequestModel extends MasterInputResponse {


    @SerializedName("senderAccountNumber")
    @Expose
    private String senderAccountNumber;
    @SerializedName("receiverAccountNumber")
    @Expose
    private String receiverAccountNumber;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("otp")
    @Expose
    private String otp;
    @SerializedName("deliveryType")
    @Expose
    private Integer deliveryType;
    @SerializedName("accountCategory")
    @Expose
    private String accountCategory;

    public String getSenderAccountNumber() {
        return senderAccountNumber;
    }

    public void setSenderAccountNumber(String senderAccountNumber) {
        this.senderAccountNumber = senderAccountNumber;
    }

    public String getReceiverAccountNumber() {
        return receiverAccountNumber;
    }

    public void setReceiverAccountNumber(String receiverAccountNumber) {
        this.receiverAccountNumber = receiverAccountNumber;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public Integer getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(Integer deliveryType) {
        this.deliveryType = deliveryType;
    }

    public String getAccountCategory() {
        return accountCategory;
    }

    public void setAccountCategory(String accountCategory) {
        this.accountCategory = accountCategory;
    }

}
