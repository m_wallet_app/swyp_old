package com.indy.dbt.models;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by mohammadabid.hussain on 5/22/2017.
 */

public class DataAmountModel  {

    public DataAmountModel(Integer amountId, Integer amount, boolean isSelected) {
        this.amountId = amountId;
        this.amount = amount;
        this.isSelected = isSelected;
    }

    private Integer amountId;
    private Integer amount;
    private boolean isSelected;

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getAmountId() {
        return amountId;
    }

    public void setAmountId(Integer amountId) {
        this.amountId = amountId;
    }


    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

}
