package com.indy.dbt.activities;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.adapters.ReceivedRequestsAdapter;
import com.indy.adapters.SentRequestsAdapter;
import com.indy.controls.OnAcceptReceivedRequestListener;
import com.indy.controls.OnDeleteReceivedRequestListener;
import com.indy.controls.OnDeleteSentTransferRequestListener;
import com.indy.controls.OnSendReminderRequestListener;
import com.indy.controls.ServiceUtils;
import com.indy.customviews.CustomTextView;
import com.indy.models.acceptDeleteTransferProcesses.AcceptDeleteTransferProcessesInput;
import com.indy.models.acceptDeleteTransferProcesses.AcceptDeleteTransferProcessesOutput;
import com.indy.models.balance.BalanceInputModel;
import com.indy.models.balance.BalanceResponseModel;
import com.indy.models.getPendingTransfersList.requestCreditFromFriend.GetPendingTransferListOutput;
import com.indy.models.getPendingTransfersList.requestCreditFromFriend.GetPendingTransfersListInput;
import com.indy.models.getPendingTransfersList.requestCreditFromFriend.TransacitonList;
import com.indy.models.isEligable.IsEligableOutputModel;
import com.indy.models.isEligable.IsEligiableInputModel;
import com.indy.models.requestCreditFromFriend.RequestCreditInput;
import com.indy.models.requestCreditFromFriend.RequestCreditOutput;
import com.indy.models.sendReminder.SendReminderInput;
import com.indy.models.transferCredit.TransferCreditOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.services.GetUserBalanceService;
import com.indy.services.RequestCreditService;
import com.indy.services.SendReminderService;
import com.indy.services.TransferCreditService;
import com.indy.services.isEligableService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.HelpActivity;
import com.indy.views.activites.MasterActivity;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RequestActivity extends MasterActivity implements OnDeleteSentTransferRequestListener,
        OnAcceptReceivedRequestListener, OnDeleteReceivedRequestListener, OnSendReminderRequestListener {

    View view;
    LinearLayout ll_lowBalance, ll_sentRequests, ll_receivedRequests, ll_confirmationMessage, ll_mainRequest,
            ll_successMessage;
    RelativeLayout root_layout;
    Button btn_next, btn_ok, btn_okSuccesful;
    EditText et_number, et_amount;
    TextView tv_titleConfirmation, tv_textConfirmation, btn_cancel, tv_titleSuccess, tv_textSuccess, tv_reminder;
    //        ImageView iv_contacts;
    String serviceType = "";
    SentRequestsAdapter mSentRequestAdapter;
    ReceivedRequestsAdapter mReceivedRequestsAdapter;
    TextInputLayout til_number, til_amount;
    public static boolean refreshList = false;

    private TextView headerTxt;
    public static TextView tv_lastUpdated;
    public static TextView titleTxt;
    private Button backImg, helpImg;
    public LinearLayout ll_topLayout;
    FrameLayout frameLayout;
    BalanceInputModel balanceInputModel;
    BalanceResponseModel balanceResponseModel;

    public final static int request_permission_for_contacts = 10;
    List<TransacitonList> mSentRequests, mReceivedRequests;
    RecyclerView rv_sentRequests, rv_receivedRequests;
    RequestCreditInput requestCreditInput;
    RequestCreditOutput requestCreditOutput;
    GetPendingTransfersListInput transfersListInput;
    GetPendingTransferListOutput transferListOutput;
    SendReminderInput sendReminderInput;
    TransferCreditOutput transferCreditOutput, acceptReceivedRequestOutput;
    AcceptDeleteTransferProcessesInput transferProcessesInput;
    AcceptDeleteTransferProcessesOutput transferProcessesOutput;

    IsEligiableInputModel isEligiableInputModel;
    IsEligableOutputModel isEligiableOutputModel;
    int selectedIndex = -1, selectedMode = -1;
    double currentBalance = 15;
    String actionType;
    public static int MINIMUM_BALANCE_LIMIT = 10;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dbt_request);
        initUI();
        initObjects();
        getBalanceService();
    }

    @Override
    public void initUI() {
        super.initUI();
        try {
            //For Header
            titleTxt = (TextView) findViewById(R.id.tv_title_findus);
            headerTxt = (TextView) findViewById(R.id.titleTxt);
            headerTxt.setText(getString(R.string.transfer_spaced));
            backImg = (Button) findViewById(R.id.backImg);
            backImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });
            helpImg = (Button) findViewById(R.id.helpBtn);
            helpImg.setVisibility(View.INVISIBLE);
            helpImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(RequestActivity.this, HelpActivity.class));
                }
            });
            frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
            frameLayout.setVisibility(View.GONE);
            ll_topLayout = (LinearLayout) findViewById(R.id.ll_top_layout);
            tv_lastUpdated = (TextView) findViewById(R.id.tv_last_updated);
            tv_lastUpdated.setText(getString(R.string.last_updated) + " " + sharedPrefrencesManger.getLastUpdatedBalance() +
                    " " + getString(R.string.min_ago));
//        initTabLayout();
            onConsumeService();

            ll_lowBalance = (LinearLayout) findViewById(R.id.ll_low_balance_warning);
            ll_sentRequests = (LinearLayout) findViewById(R.id.ll_sent_requests);
            ll_receivedRequests = (LinearLayout) findViewById(R.id.ll_received_requests);
            btn_next = (Button) findViewById(R.id.nextBtn);
            rv_sentRequests = (RecyclerView) findViewById(R.id.rv_sent_requests);
            rv_receivedRequests = (RecyclerView) findViewById(R.id.rv_received_requests);
            ll_confirmationMessage = (LinearLayout) findViewById(R.id.ll_confirmation_message);
            ll_successMessage = (LinearLayout) findViewById(R.id.ll_successful_transaction);
            ll_mainRequest = (LinearLayout) findViewById(R.id.ll_main_request);
            root_layout = (RelativeLayout) findViewById(R.id.root_layout);
            til_number = (TextInputLayout) findViewById(R.id.mobileNoInputLayout);
            til_amount = (TextInputLayout) findViewById(R.id.amountInputLayout);
            tv_titleConfirmation = (TextView) findViewById(R.id.tv_title);
            tv_textConfirmation = (TextView) findViewById(R.id.tv_warningText);
            if (sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)) {
                tv_textConfirmation.setLineSpacing(0, 1f);
            } else {
                tv_textConfirmation.setLineSpacing(10, 1.2f);
            }
            setHeaderTitle(R.string.request);
            et_number = (EditText) findViewById(R.id.mobile_number);
            et_amount = (EditText) findViewById(R.id.amount);
            btn_ok = (Button) findViewById(R.id.btn_ok);
            btn_okSuccesful = (Button) findViewById(R.id.btn_ok_success);
            btn_cancel = (CustomTextView) findViewById(R.id.btn_cancel);
            tv_reminder = (TextView) findViewById(R.id.tv_send_reminder);
            btn_next = (Button) findViewById(R.id.nxtBtn);


            rv_sentRequests.setLayoutManager(new LinearLayoutManager(this));
            rv_receivedRequests.setLayoutManager(new LinearLayoutManager(this));
            tv_titleSuccess = (TextView) findViewById(R.id.tv_title_success);
            tv_textSuccess = (TextView) findViewById(R.id.tv_warningText_success);
            ll_confirmationMessage.setVisibility(View.GONE);
            ll_mainRequest.setVisibility(View.VISIBLE);
            ll_successMessage.setVisibility(View.GONE);
//        iv_contacts = (ImageView) findViewById(R.id.iv_contacts);
            et_number.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    try {
                        final int DRAWABLE_END_ENGLISH = 2;
                        final int DRAWABLE_END_ARABIC = 0;
                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            if (currentLanguage.equals("en")) {
                                if (event.getRawX() >= (et_number.getRight() - et_number.getCompoundDrawables()[DRAWABLE_END_ENGLISH].getBounds().width())) {
                                    // your action here
                                    Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                                    startActivityForResult(intent, 1);
                                    return true;
                                }
                            } else {
                                if (event.getX() <= (et_number.getCompoundDrawables()[DRAWABLE_END_ARABIC].getBounds().width())) {
                                    // your action here
                                    Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                                    startActivityForResult(intent, 1);
                                    return true;
                                }
                            }
                        }
                    } catch (Exception ex) {
                        CommonMethods.logException(ex);
                    }
                    return false;
                }
            });
//        iv_contacts.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                ((DBTPagerActivity) getActivity()).showFrame();
////                ((DBTPagerActivity) getActivity()).replaceFragmnet(new ContactListFragmnt(),R.id.frameLayout,false);7
////                startActivityForResult(new Intent(getActivity(), ContactListFragmnt.class),1001);
//                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
//                startActivityForResult(intent, 1);
//
//            }
//        });

            btn_next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (isValid()) {
                        selectedMode = 2;
                        serviceType = ServiceUtils.requestCredit;
                        showConfirmationView(selectedMode, -1);
                        hideKeyBoard();

                    }

                }
            });
          et_amount.clearFocus();
          et_number.clearFocus();
//        serviceType = ServiceUtils.getPendingTransfersList;
//        onConsumeService();
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }
    }

    public boolean isValid() {
        boolean retValue = true;

        if (et_number.getText().toString().length() < 10) {
            retValue = false;
            if (et_number.getText().toString().length() > 0)
                til_number.setError(getString(R.string.ouch_number_does_not_seem_valid));
        } else {
            String stringToCheck = et_number.getText().toString().replace(" ", "");
            String noStr = stringToCheck.substring(0, 2);
            if (!noStr.equalsIgnoreCase("05") && !stringToCheck.substring(0, 4).contains("+971") && !stringToCheck.substring(0, 5).contains("00971")) {
                retValue = false;
                if (et_number.getText().toString().length() > 0)
                    til_number.setError(getString(R.string.ouch_number_does_not_seem_valid));
            } else {
                removeTextInputLayoutError(til_number);
            }
        }
        String amount = et_amount.getText().toString();
        amount = amount.replace("AED", "");
        amount = amount.replace(" ", "");
        try {
            if (amount.length() > 0 && Integer.parseInt(amount) >= 2 && Integer.parseInt(amount) <= 150) {
                removeTextInputLayoutError(til_amount);
            } else {
                retValue = false;
                if (et_amount.getText().toString().length() > 0)
                    setTextInputLayoutError(til_amount, getString(R.string.invalid_number));
            }
        } catch (NumberFormatException ex) {
            retValue = false;
            if (et_amount.getText().toString().length() > 0)
                setTextInputLayoutError(til_amount, getString(R.string.invalid_number));
        }

        if (retValue) {
            enableNextBtn();
        } else {
            disableNextBtn();
        }

        return retValue;
    }

    private void initObjects() {
        disableNextBtn();
        mSentRequests = new ArrayList<TransacitonList>();
        mReceivedRequests = new ArrayList<TransacitonList>();

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                processConfirmation();
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_transfer_confirm_request_sending);

            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedIndex = -1;
                selectedMode = -1;
                hideConfirmationMesage();
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_transfer_cancel_request);
            }
        });

        btn_okSuccesful.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideConfirmationMesage();
                if (selectedMode == 1) {
                    onConsumeService();
                }
            }
        });
//        et_number.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//                String tempNumber = "";
//                tempNumber = et_number.getText().toString();
//                tempNumber = tempNumber.replace("+971", "0");
//                tempNumber = tempNumber.replace("00971", "0");
//                tempNumber = tempNumber.replace(" ", "");
//                tempNumber = tempNumber.trim();
//                if (tempNumber.length() > 0 && isValid()) {
//                    enableNextBtn();
//                } else {
//                    disableNextBtn();
//                }
//            }
//
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//                // TODO Auto-generated method stub
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//                // TODO Auto-generated method stub
//            }
//        });
//
//        et_amount.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//                String amount = et_amount.getText().toString();
//                amount = amount.replace("AED", "");
//                amount = amount.replace(" ", "");
//                try {
//                    if (amount.length() > 0 && Integer.parseInt(amount) >= 2 && Integer.parseInt(amount) <= 150) {
//                        if (isValid()) {
//                            enableNextBtn();
//                        }
//
//                    } else {
//                        disableNextBtn();
//                        setTextInputLayoutError(til_amount, getString(R.string.invalid_number));
//
//                    }
//                } catch (NumberFormatException ex) {
//                    disableNextBtn();
//                    setTextInputLayoutError(til_amount, getString(R.string.invalid_number));
//
//                }
//            }
//
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//                // TODO Auto-generated method stub
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//                // TODO Auto-generated method stub
//            }
//        });
        et_number.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {

                } else {
                    isValid();
                }
            }
        });

        et_number.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    isValid();
                    hideKeyBoard();
                    return true;
                }
                return true;
            }
        });

        et_amount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {

                } else {
                    isValid();
                }
            }
        });

        et_amount.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    isValid();
                    hideKeyBoard();
                    return true;
                }
                return true;
            }
        });

        removeTextInputLayoutError(til_amount);
        hideKeyBoard();

    }

    private void setupScreen() {

        if (mSentRequests.size() == 0) {
            ll_sentRequests.setVisibility(View.GONE);
        } else {
            ll_sentRequests.setVisibility(View.VISIBLE);
        }
        if (mReceivedRequests.size() == 0) {
            ll_receivedRequests.setVisibility(View.GONE);
        } else {
            ll_receivedRequests.setVisibility(View.VISIBLE);
        }

        if (currentBalance < MINIMUM_BALANCE_LIMIT) {
            ll_lowBalance.setVisibility(View.VISIBLE);
        } else {
            ll_lowBalance.setVisibility(View.GONE);
        }

//        if(rv_receivedRequests.)
    }
    //dummy list

//    public void deleteSentRequest() {
//        mSentRequestAdapter.remove(selectedIndex);
//        setRecyclerViewHeight(rv_sentRequests);
//        setupScreen();
//        selectedIndex = -1;
//        selectedMode = -1;
//    }
//
//    public void acceptReceivedRequest() {
//
//        showSuccessfulView(selectedMode, selectedIndex);
//        mReceivedRequestsAdapter.remove(selectedIndex);
//        setupScreen();
//        setRecyclerViewHeight(rv_receivedRequests);
//        selectedMode = -1;
////        selectedIndex = -1;
//
//    }

//    public void deleteReceivedRequest() {
//        mReceivedRequestsAdapter.remove(selectedIndex);
//        setRecyclerViewHeight(rv_receivedRequests);
//        setupScreen();
////        selectedIndex = -1;
//        selectedMode = -1;
//    }

    public void resetServiceParameters() {
        selectedIndex = -1;
        selectedMode = -1;
    }

//    public void addSentRequest() {
////        mSentRequests.add()
//        if (mSentRequests != null) {
//            TransacitonList mTempModel = new TransacitonList();
//
//            mTempModel.setMsisdn(et_number.getText().toString());
//            String amount = et_amount.getText().toString();
//            amount = amount.replace("AED", "");
//            amount = amount.replace(" ", "");
//            mTempModel.setAmount(amount);
//            mTempModel.setDate("" + parseDate(new Date()));// 16 Oct 2016");
//            mTempModel.setRequestId("");
//            mSentRequestAdapter.add(mTempModel);
//            setRecyclerViewHeight(rv_sentRequests);
//            selectedMode = -1;
//
//        }
//    }

    public String parseDate(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
        try {

            return dateFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "-";
    }

    public void setRecyclerViewHeight(RecyclerView recyclerView) {

        int viewHeight = (int) getResources().getDimension(R.dimen.x120) * (recyclerView.getAdapter().getItemCount());
        viewHeight += recyclerView.getAdapter().getItemCount();
        recyclerView.getLayoutParams().height = viewHeight;

    }

    public void showConfirmationView(int mode, int index) {
        hideTopLayout();
        ll_mainRequest.setVisibility(View.GONE);
        ll_confirmationMessage.setVisibility(View.VISIBLE);
        ll_successMessage.setVisibility(View.GONE);
        String detail;
        switch (mode) {

            case 0:
                //Delete sent request
                detail = getString(R.string.you_will_delete_the_request_of) + " " + mSentRequests.get(index).getAmount() +
                        " " + getString(R.string.aed_you_sent_to_this_number) + "\n" + mSentRequests.get(index).getMsisdn();
                tv_titleConfirmation.setText(getString(R.string.delete_credit_request));
                tv_textConfirmation.setText(detail);
                break;

            case 1:
                //accept received request
                detail = getString(R.string.you_will_send) + " " + isEligiableOutputModel.getAmount() +
                        " " + getString(R.string.aed_from_your_credit_to) + " " + mReceivedRequests.get(index).getMsisdn();
                tv_titleConfirmation.setText(getString(R.string.accept_credit_request));
                tv_textConfirmation.setText(detail);
                break;

            case 2:
                //request credit from friend
                String amount = et_amount.getText().toString();
                amount = amount.replace("AED", "");
                amount.replace(" ", "");
                if (sharedPrefrencesManger.isVATEnabled()) {
                    detail = getString(R.string.you_are_about_to_request) + " " + et_amount.getText().toString().trim() +
                            " " + getString(R.string.aed_from) + ": " + et_number.getText().toString().trim() + "\n\n" + getString(R.string.five_percent_vat_will_be_paid_by_sender);
                } else {
                    detail = getString(R.string.you_are_about_to_request) + " " + et_amount.getText().toString().trim() +
                            " " + getString(R.string.aed_from) + ": " + et_number.getText().toString().trim();// + "\n\n" + getString(R.string.five_percent_vat_will_be_paid_by_sender);

                }
                tv_titleConfirmation.setText(getString(R.string.request_credit));
                tv_textConfirmation.setText(detail);

                break;
            case 3:
                //Delete received request
                detail = getString(R.string.you_will_not_send_any_credit_to) + "\n" + mReceivedRequests.get(index).getMsisdn();
                tv_titleConfirmation.setText(getString(R.string.decline_received_credit_request));
                tv_textConfirmation.setText(detail);
                break;

            default:
                break;
        }
    }

    public void showSuccessfulView(int mode, int index) {
        hideTopLayout();
        ll_mainRequest.setVisibility(View.GONE);
        ll_confirmationMessage.setVisibility(View.GONE);
        ll_successMessage.setVisibility(View.VISIBLE);
        try {
            root_layout.setBackgroundResource(R.drawable.success_background_mdpi);
        } catch (Exception ex) {

        } catch (OutOfMemoryError oom) {
        }
        String detail;
        switch (mode) {

            case 0:
                //reminder case
                detail = getString(R.string.reminder_success_message_first_line) + " " + mSentRequests.get(index).getMsisdn() +
                        " " + getString(R.string.to_accept_your_request);
                tv_titleSuccess.setText(getString(R.string.notice));
                tv_textSuccess.setText(detail);
                break;

            case 1:
                //accept received request
                detail = getString(R.string.you_transferred) + " " + mReceivedRequests.get(index).getAmount() +
                        " " + getString(R.string.aed_to) + " " + mReceivedRequests.get(index).getMsisdn() + ". " +
                        getString(R.string.with_the_transfer_fee) + " " + isEligiableOutputModel.getTotalAmount() +
                        getString(R.string.aed_was_deducted_from_your_balance);
                tv_titleSuccess.setText(getString(R.string.awesome));
                tv_textSuccess.setText(detail);
                break;

            default:
                break;
        }
    }

    public void processConfirmation() {
        switch (selectedMode) {

            case 0:
                //Delete sent request
                onConsumeService();
                break;

            case 1:
                onConsumeService();
//                acceptReceivedRequest();
                //accept received request

                break;

            case 2:
//                addSentRequest();
                //request credit from friend

                onConsumeService();


                break;
            case 3:
                //Delete received request
                onConsumeService();
//                deleteReceivedRequest();
                break;

            default:
                break;

        }
        hideConfirmationMesage();
    }


    private void hideConfirmationMesage() {
        showTopLayout();
        root_layout.setBackgroundResource(0);
        ll_confirmationMessage.setVisibility(View.GONE);
        ll_successMessage.setVisibility(View.GONE);
        ll_mainRequest.setVisibility(View.VISIBLE);
    }


    private void acceptRequest() {

        serviceType = ServiceUtils.processRequest;
        actionType = "3";
        showConfirmationView(1, selectedIndex);
    }

    boolean isEdited;

    @Override
    public void onAcceptReceivedRequest(int index, boolean isEdited, String amount) {
        selectedIndex = index;
        selectedMode = 1;
        serviceType = ServiceUtils.isEligable;
        this.isEdited = isEdited;
        onConsumeService();


    }

    @Override
    public void onSelectedRequest(int position) {
        selectedIndex = position;
        selectedMode = 0;
        serviceType = ServiceUtils.processRequest;
        actionType = "1";
        showConfirmationView(0, position);

    }

    @Override
    public void onDeleteReceivedRequest(int index) {
        selectedIndex = index;
        selectedMode = 3;
        serviceType = ServiceUtils.processRequest;
        actionType = "2";
        showConfirmationView(3, index);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (data != null && data.getData() != null) {//cancel action
                String phoneNo = null;
                Uri uri = data.getData();
                Cursor cursor = getContentResolver().query(uri, null, null, null, null);
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_transfer_select_number);

                if (cursor.moveToFirst()) {
                    int phoneIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                    phoneNo = cursor.getString(phoneIndex);
                }

                cursor.close();
                et_number.setText(phoneNo);
            }
        }
    }

    @Override
    public void onSendReminderRequestListener(int index) {
        serviceType = ServiceUtils.sendReminder;
        selectedIndex = index;
        onConsumeService();
//        selectedMode = 0;

    }

//    addFragment(new LoadingFragmnet(),R.id.frameLayout,true);

    private void showErrorFragment(String error) {
        showFrame();
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, error);
        errorFragment.setArguments(bundle);
        replaceFragmnet(errorFragment, R.id.frameLayout, true);
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();

        addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        showFrame();
        if (serviceType.equals(ServiceUtils.requestCredit)) {
            requestCreditInput = new RequestCreditInput();
            requestCreditInput.setImsi(sharedPrefrencesManger.getMobileNo());
            requestCreditInput.setOsVersion(osVersion);
            requestCreditInput.setDeviceId(deviceId);
            requestCreditInput.setAppVersion(appVersion);
            String numberToSend = et_number.getText().toString();
            numberToSend = numberToSend.replace(" ", "");
            numberToSend = numberToSend.replace("+971", "0");
            numberToSend = numberToSend.replace("00971", "0");
            String amountToSend = et_amount.getText().toString();
            amountToSend = amountToSend.replace("AED", "");
            amountToSend = amountToSend.replace(" ", "");
            requestCreditInput.setAmount(amountToSend);
            requestCreditInput.setTargetMsisdn(numberToSend);
            requestCreditInput.setChannel(channel);
            requestCreditInput.setLang(currentLanguage);
            requestCreditInput.setToken(token);
            requestCreditInput.setMsisdn(sharedPrefrencesManger.getMobileNo());
            requestCreditInput.setAuthToken(authToken);
            new RequestCreditService(this, requestCreditInput);
        } else if (serviceType.equals(ServiceUtils.getPendingTransfersList)) {
            transfersListInput = new GetPendingTransfersListInput();
            transfersListInput.setImsi(sharedPrefrencesManger.getMobileNo());
            transfersListInput.setOsVersion(osVersion);
            transfersListInput.setDeviceId(deviceId);
            transfersListInput.setAppVersion(appVersion);
//            transfersListInput.setBeneficiaryMsisdn(et_number.getText().toString());
//            transfersListInput.setTransferType("1");
            transfersListInput.setChannel(channel);
            transfersListInput.setLang(currentLanguage);
            transfersListInput.setToken(token);
            transfersListInput.setMsisdn(sharedPrefrencesManger.getMobileNo());
            transfersListInput.setAuthToken(authToken);
        } else if (serviceType.equals(ServiceUtils.sendReminder)) {
            sendReminderInput = new SendReminderInput();
            sendReminderInput.setImsi(sharedPrefrencesManger.getMobileNo());
            sendReminderInput.setOsVersion(osVersion);
            sendReminderInput.setDeviceId(deviceId);
            sendReminderInput.setAppVersion(appVersion);
            sendReminderInput.setRequestId(mSentRequests.get(selectedIndex).getRequestId());
            sendReminderInput.setChannel(channel);
            sendReminderInput.setLang(currentLanguage);
            sendReminderInput.setToken(token);
            sendReminderInput.setMsisdn(sharedPrefrencesManger.getMobileNo());
            sendReminderInput.setAuthToken(authToken);
            new SendReminderService(this, sendReminderInput);
        } else if (serviceType.equals(ServiceUtils.processRequest)) {

            transferProcessesInput = new AcceptDeleteTransferProcessesInput();
            if (actionType.equals("1")) {
                transferProcessesInput.setRequestId(mSentRequests.get(selectedIndex).getRequestId());
            } else {
                transferProcessesInput.setRequestId(mReceivedRequests.get(selectedIndex).getRequestId());
            }

            transferProcessesInput.setToken(token);
            transferProcessesInput.setLang(currentLanguage);
            transferProcessesInput.setAppVersion(appVersion);
            transferProcessesInput.setOsVersion(osVersion);
            transferProcessesInput.setDeviceId(deviceId);
            transferProcessesInput.setChannel(channel);
            transferProcessesInput.setMsisdn(sharedPrefrencesManger.getMobileNo());
            transferProcessesInput.setActionType(actionType);
            transferProcessesInput.setTargetMsisdn(et_number.getText().toString());
            transferProcessesInput.setAuthToken(authToken);


        } else if (serviceType.equals(ServiceUtils.transferCredit)) {

            RequestCreditInput requestCreditInput = new RequestCreditInput();
            requestCreditInput.setImsi(sharedPrefrencesManger.getMobileNo());
            requestCreditInput.setOsVersion(osVersion);
            requestCreditInput.setDeviceId(deviceId);
            requestCreditInput.setAppVersion(appVersion);
            String numberToSend = et_number.getText().toString();
            numberToSend = numberToSend.replace(" ", "");
            numberToSend = numberToSend.replace("+971", "0");

            String amountToSend = et_amount.getText().toString();
            amountToSend = amountToSend.replace("AED", "");
            amountToSend = amountToSend.replace(" ", "");
            requestCreditInput.setAmount(isEligiableOutputModel.getAmount());
            requestCreditInput.setTargetMsisdn(mReceivedRequests.get(selectedIndex).getMsisdn());
            requestCreditInput.setChannel(channel);
            requestCreditInput.setLang(currentLanguage);
            requestCreditInput.setToken(token);
            requestCreditInput.setMsisdn(sharedPrefrencesManger.getMobileNo());
            requestCreditInput.setAuthToken(authToken);
            new TransferCreditService(this, requestCreditInput);
        } else if (serviceType.equals(ServiceUtils.isEligable)) {

            isEligiableInputModel = new IsEligiableInputModel();
            isEligiableInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
            isEligiableInputModel.setLang(currentLanguage);
            isEligiableInputModel.setDeviceId(deviceId);
            isEligiableInputModel.setOsVersion(osVersion);
            isEligiableInputModel.setAppVersion(appVersion);
            isEligiableInputModel.setChannel(channel);
            isEligiableInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
            isEligiableInputModel.setAmount(mReceivedRequests.get(selectedIndex).getAmount());
            isEligiableInputModel.setTargetMsisdn(mReceivedRequests.get(selectedIndex).getMsisdn());
            isEligiableInputModel.setToken(token);
            isEligiableInputModel.setAuthToken(authToken);
            new isEligableService(this, isEligiableInputModel);
        }



    }

    private void getBalanceService(){
        addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        showFrame();
        balanceInputModel = new BalanceInputModel();
        balanceInputModel.setAppVersion(appVersion);
        balanceInputModel.setToken(token);
        balanceInputModel.setOsVersion(osVersion);
        balanceInputModel.setChannel(channel);
        balanceInputModel.setDeviceId(deviceId);
        balanceInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        balanceInputModel.setBillingPeriod(CommonMethods.getBillingPeriod());
        balanceInputModel.setLang(currentLanguage);
        balanceInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
        balanceInputModel.setAuthToken(authToken);
        new GetUserBalanceService(this, balanceInputModel, this);
    }

    public void setupTransactionListFromService() {
//        mSentRequests = new ArrayList<TransacitonList>();
//        mReceivedRequests = new ArrayList<TransacitonList>();
//
//        for (int i = 0; i < transferListOutput.getTransacitonList().length; i++) {
//            if (transferListOutput.getTransacitonList()[i].getTransferType().equals("credit")) {
//                mSentRequests.add(transferListOutput.getTransacitonList()[i]);
//            } else if (transferListOutput.getTransacitonList()[i].getTransferType().equals("debit")) {
//                mReceivedRequests.add(transferListOutput.getTransacitonList()[i]);
//            }
//        }
//
//        mSentRequestAdapter = new SentRequestsAdapter(mSentRequests, getContext(), this, this);
//        rv_sentRequests.setAdapter(mSentRequestAdapter);
//        setRecyclerViewHeight(rv_sentRequests);
//        rv_sentRequests.addItemDecoration(new DividerItemDecoration(getResources().getDrawable(R.drawable.background_line_grey), false, false, 1));
//
//        mReceivedRequestsAdapter = new ReceivedRequestsAdapter(mReceivedRequests, getContext(), this, this);
//        rv_receivedRequests.setAdapter(mReceivedRequestsAdapter);
//        rv_receivedRequests.addItemDecoration(new DividerItemDecoration(getResources().getDrawable(R.drawable.background_line_grey), false, false, 1));
//
//        setRecyclerViewHeight(rv_receivedRequests);
//        setupScreen();


    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        onBackPressed();
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            if (this != null) {
                onBackPressed();
                if (responseModel != null && responseModel.getResultObj() != null) {
                    hideFrame();
                    int j = 0;
                    for (int i = 0; i < getSupportFragmentManager().getFragments().size(); i++) {

                        if (getSupportFragmentManager().getFragments().get(i) instanceof LoadingFragmnet) {
                            j++;
                        }

                    }
                    Fragment[] loadingFragments = new Fragment[j];
                    for (int i = 0; i < j; i++) {
                        loadingFragments[i] = new LoadingFragmnet();
                    }
                    this.removeFragment(loadingFragments);
                    if(responseModel.getServiceType().equals(ServiceUtils.balanceServiceType)){
                        balanceResponseModel = (BalanceResponseModel) responseModel.getResultObj();
                        if (balanceResponseModel != null) {
                            if (balanceResponseModel.getErrorCode() != null) {
                                showErrorFragment(balanceResponseModel.getErrorMsgEn());
                            } else {
                                titleTxt.setText(getString(R.string.your_balance_is_aed) + " " + balanceResponseModel.getAmount()+ " " + getString(R.string.aed_arabic_only));
                                tv_lastUpdated.setText(getString(R.string.last_updated) + " " + sharedPrefrencesManger.getLastUpdatedBalance() +
                                        " " + getString(R.string.min_ago));
                                CommonMethods.updateBalanceAndLastUpdated(getString(R.string.your_balance_is_aed) + " " + balanceResponseModel.getAmount()+ " " + getString(R.string.aed_arabic_only)
                                        , getString(R.string.last_updated) + " " + sharedPrefrencesManger.getLastUpdatedBalance() +
                                                " " + getString(R.string.min_ago));
                            }
                        }
                    }
                    else if (responseModel.getServiceType().equals(ServiceUtils.requestCredit)) {
                        requestCreditOutput = (RequestCreditOutput) responseModel.getResultObj();
                        if (requestCreditOutput != null && requestCreditOutput.getRequested() != null && requestCreditOutput.getRequested().equals("true")) {
                            CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_transfer_request_user_for_credit);

//                addSentRequest();
                            et_number.setText("");
                            et_amount.setText("");
                            removeTextInputLayoutError(til_amount);
                            removeTextInputLayoutError(til_number);
                            DBTPagerActivity.viewPager.setCurrentItem(0, true);
                            DBTPagerActivity.tabLayout.getTabAt(0).select();
                            setResult(RESULT_OK);
                            this.finish();

                        } else {
                            if (requestCreditOutput != null && requestCreditOutput.getErrorMsgEn() != null) {
                                if (currentLanguage.equalsIgnoreCase("en"))
                                    showErrorFragment(requestCreditOutput.getErrorMsgEn());
                                else
                                    showErrorFragment(requestCreditOutput.getErrorMsgAr());

                            } else {
                                showErrorFragment(getString(R.string.generice_error));
                            }
                        }
                    } else if (responseModel.getServiceType().equals(ServiceUtils.getPendingTransfersList)) {
                        transferListOutput = (GetPendingTransferListOutput) responseModel.getResultObj();
//            if (transferListOutput != null) {
//                Toast.makeText(getActivity(), transferListOutput.toString() + "\n" + transferListOutput.getTransacitonList(), Toast.LENGTH_SHORT).show();
//
//                Toast.makeText(getActivity(), transferListOutput.toString(), Toast.LENGTH_LONG).show();
//            } else {
//                Toast.makeText(getActivity(), "Transfer list output is null", Toast.LENGTH_SHORT).show();
//
//            }
                        if (transferListOutput != null && transferListOutput.getTransacitonList() != null &&
                                transferListOutput.getTransacitonList().length > 0) {
//                            setupTransactionListFromService();
                            ll_lowBalance.setVisibility(View.GONE);

                        } else if (transferListOutput != null && transferListOutput.getTransacitonList() != null
                                && transferListOutput.getTransacitonList().length == 0) {
//                            setupTransactionListFromService();
                            ll_lowBalance.setVisibility(View.VISIBLE);
                        } else {
                            ll_lowBalance.setVisibility(View.VISIBLE);
                            if (requestCreditOutput != null && requestCreditOutput.getErrorMsgEn() != null) {
                                if (currentLanguage.equals("en")) {
                                    showErrorFragment(requestCreditOutput.getErrorMsgEn());
                                } else {

                                    showErrorFragment(requestCreditOutput.getErrorMsgEn());
                                }
                            } else {
                                showErrorFragment(getString(R.string.generice_error));
                            }
                        }

                    } else if (responseModel.getServiceType().equals(ServiceUtils.sendReminder)) {
                        transferCreditOutput = (TransferCreditOutput) responseModel.getResultObj();
                        if (transferCreditOutput != null && transferCreditOutput.getSent() != null &&
                                transferCreditOutput.getSent().equals("true")) {
                            showSuccessfulView(0, selectedIndex);
                            selectedIndex = -1;
                            selectedMode = -1;
                        } else {
                            if (transferCreditOutput != null && transferCreditOutput.getErrorMsgEn() != null) {
                                showErrorFragment(transferCreditOutput.getErrorMsgEn());
                            } else {
                                showErrorFragment("Error");
                            }
                        }

                    } else if (responseModel.getServiceType().equals(ServiceUtils.processRequest)) {
                        transferProcessesOutput = (AcceptDeleteTransferProcessesOutput) responseModel.getResultObj();
                        if (transferProcessesOutput != null && transferProcessesOutput.getProcessed() != null
                                && transferProcessesOutput.getProcessed().equals("true")) {
                            if (actionType.equals("1")) {
                                //reject request
                                serviceType = ServiceUtils.getPendingTransfersList;
                                onConsumeService();
                            } else if (actionType.equals("2")) {

                                //delete request
                                serviceType = ServiceUtils.getPendingTransfersList;
                                onConsumeService();

                                ;
                            } else if (actionType.equals("3")) {
                                serviceType = ServiceUtils.getPendingTransfersList;
                                showSuccessfulView(1, selectedIndex);
                            }
                        } else {
                            showErrorFragment(transferProcessesOutput.getErrorMsgEn());
                        }
                    } else if (responseModel.getServiceType().equals(ServiceUtils.isEligable)) {
                        isEligiableOutputModel = (IsEligableOutputModel) responseModel.getResultObj();
                        if (isEligiableOutputModel != null && isEligiableOutputModel.getEligible()) {
                            acceptRequest();
                        } else {
                            if (currentLanguage.equalsIgnoreCase(ConstantUtils.lang_english)) {
                                if (isEligiableOutputModel != null && isEligiableOutputModel.getErrorMsgEn() != null) {
                                    showErrorFragment(isEligiableOutputModel.getErrorMsgEn());
                                } else {
                                    showErrorFragment("You are not eligible for this service.");
                                }
                            } else {
                                if (isEligiableOutputModel != null && isEligiableOutputModel.getErrorMsgAr() != null) {
                                    showErrorFragment(isEligiableOutputModel.getErrorMsgAr());
                                } else {
                                    showErrorFragment("You are not eligible for this service.");
                                }
                            }

                        }
                    } else if (responseModel.getServiceType().equals(ServiceUtils.transferCredit)) {

                    }
                }
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        try {
            resetServiceParameters();
            hideFrame();
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
//        showErrorFragment("Error. In error listener.");
    }

    private void enableNextBtn() {
        btn_next.setAlpha(1f);
        btn_next.setClickable(true);
        til_number.setError(null);
    }

    private void disableNextBtn() {
        btn_next.setAlpha(.5f);
        btn_next.setClickable(false);
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public void setHeaderTitle(int stringId){
        headerTxt.setText(getString(stringId));
    }

    public void hideTopLayout() {
        ll_topLayout.setVisibility(View.GONE);
        frameLayout.setVisibility(View.GONE);
    }

    public void showTopLayout() {
        ll_topLayout.setVisibility(View.VISIBLE);
        frameLayout.setVisibility(View.VISIBLE);
    }

    public void showFrame() {
        frameLayout.setVisibility(View.VISIBLE);
    }

    public void hideFrame() {
        frameLayout.setVisibility(View.GONE);
    }

}