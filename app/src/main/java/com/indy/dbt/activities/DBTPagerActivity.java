package com.indy.dbt.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.customviews.CustomTabLayout;
import com.indy.dbt.adapters.TransferDBTAdapter;
import com.indy.dbt.listeners.OnBalanceUpdatedListener;
import com.indy.dbt.listeners.OnRequestSuccessfulListener;
import com.indy.models.balance.BalanceInputModel;
import com.indy.models.balance.BalanceResponseModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.services.GetUserBalanceService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.HelpActivity;
import com.indy.views.activites.MasterActivity;
import com.indy.views.fragments.store.request.TransferFragment;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;

public class DBTPagerActivity extends MasterActivity implements OnBalanceUpdatedListener {

    private boolean isBalance = true, isData = false;
    public static final int REQUEST_CODE_FROM_TRANSFER = 999;
    public static final int REQUEST_CODE_FROM_REQUEST= 888;
    public static double availableBalance = 0;
    public static ViewPager viewPager;
    private TextView headerTxt;
    public static TextView tv_lastUpdated;
    public static TextView titleTxt;
    private Button backImg, helpImg;
    public LinearLayout ll_topLayout;
    FrameLayout frameLayout;
    BalanceInputModel balanceInputModel;
    BalanceResponseModel balanceResponseModel;
    public final static int request_permission_for_contacts = 10;
    public static CustomTabLayout tabLayout;
    private OnRequestSuccessfulListener onRequestSuccessfulListener;


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_FROM_TRANSFER && resultCode == RESULT_OK) {
            try {
                onConsumeService();
                CommonMethods.createAppRatingDialog(DBTPagerActivity.this, mFirebaseAnalytics);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        if (requestCode == REQUEST_CODE_FROM_REQUEST && resultCode == RESULT_OK) {
            try {
                onRequestSuccessfulListener.onRequestSuccessful();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_dbt_pager);
        initUI();
    }

    @Override
    public void initUI() {
        super.initUI();
        titleTxt = (TextView) findViewById(R.id.tv_title_findus);
        headerTxt = (TextView) findViewById(R.id.titleTxt);
        headerTxt.setText(getString(R.string.transfer));
        backImg = (Button) findViewById(R.id.backImg);
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        helpImg = (Button) findViewById(R.id.helpBtn);
        helpImg.setVisibility(View.INVISIBLE);
        helpImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DBTPagerActivity.this, HelpActivity.class));
            }
        });
        frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
        frameLayout.setVisibility(View.GONE);
        ll_topLayout = (LinearLayout) findViewById(R.id.ll_top_layout);
        tv_lastUpdated = (TextView) findViewById(R.id.tv_last_updated);
        tv_lastUpdated.setText(getString(R.string.last_updated) + " " + sharedPrefrencesManger.getLastUpdatedBalance() +
                " " + getString(R.string.min_ago));
        initTabLayout();
        onConsumeService();

    }

    private void initTabLayout() {
        tabLayout = (CustomTabLayout) findViewById(R.id.tab_layout);
        tabLayout.setSharedPrefrencesManger(sharedPrefrencesManger);
        tabLayout.addTab(tabLayout.newTab().setText((getString(R.string.balance))));
        tabLayout.addTab(tabLayout.newTab().setText((getString(R.string.data))));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        viewPager = (ViewPager) findViewById(R.id.pager);
        final TransferDBTAdapter adapter = new TransferDBTAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        adapter.setActivity(this);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(0, true);
        viewPager.setSelected(true);
        viewPager.setFocusable(true);
        tabLayout.getTabAt(0).select();
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
//        titleTxt.setText(getString(R.string.locate_our_shop_now));
        tabLayout.setFillViewport(true);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                tabLayout.setSelectedTabIndicatorColor(Color.WHITE);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void showErrorFragment(String error) {
        frameLayout.setVisibility(View.VISIBLE);
//        frameLayout.bringToFront();
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, error);
        errorFragment.setArguments(bundle);
        addFragmnet(errorFragment, R.id.frameLayout, true);
    }

    public void hideTopLayout() {
        ll_topLayout.setVisibility(View.GONE);
        frameLayout.setVisibility(View.GONE);
    }

    public void showTopLayout() {
        ll_topLayout.setVisibility(View.VISIBLE);
        frameLayout.setVisibility(View.VISIBLE);
    }

    public void showFrame() {
        frameLayout.setVisibility(View.VISIBLE);
    }

    public void hideFrame() {
        frameLayout.setVisibility(View.GONE);
    }


    public void checkPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.READ_CONTACTS}, request_permission_for_contacts);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (tv_lastUpdated != null)
            tv_lastUpdated.setText(getString(R.string.last_updated) + " " + sharedPrefrencesManger.getLastUpdatedBalance() +
                    " " + getString(R.string.min_ago));
        if (sharedPrefrencesManger.getNavigationIndex().length() > 0 || sharedPrefrencesManger.getNavigationBadgeIndex().length() > 0) {
            finish();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == request_permission_for_contacts) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                // permission was granted, yay! Do the
                // contacts-related task you need to do.

            } else {

                // permission denied, boo! Disable the
                // functionality that depends on this permission.
            }
        }

    }


    @Override
    public void onConsumeService() {
        super.onConsumeService();
        addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        showFrame();
        balanceInputModel = new BalanceInputModel();
        balanceInputModel.setAppVersion(appVersion);
        balanceInputModel.setToken(token);
        balanceInputModel.setOsVersion(osVersion);
        balanceInputModel.setChannel(channel);
        balanceInputModel.setDeviceId(deviceId);
        balanceInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        balanceInputModel.setBillingPeriod(CommonMethods.getBillingPeriod());
        balanceInputModel.setLang(currentLanguage);
        balanceInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
        balanceInputModel.setAuthToken(authToken);
        new GetUserBalanceService(this, balanceInputModel, this);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        balanceResponseModel = (BalanceResponseModel) responseModel.getResultObj();
        if (balanceResponseModel != null) {
            if (balanceResponseModel.getErrorCode() != null) {
                showErrorFragment(balanceResponseModel.getErrorMsgEn());
            } else {
                availableBalance = balanceResponseModel.getAmount();
                titleTxt.setText(getString(R.string.your_balance_is_aed) + " " + balanceResponseModel.getAmount() + " " + getString(R.string.aed_arabic_only)  );
                tv_lastUpdated.setText(getString(R.string.last_updated) + " " + sharedPrefrencesManger.getLastUpdatedBalance() +
                        " " + getString(R.string.min_ago));
                CommonMethods.updateBalanceAndLastUpdated(getString(R.string.your_balance_is_aed) + " " + balanceResponseModel.getAmount()+ " " + getString(R.string.aed_arabic_only)
                        , getString(R.string.last_updated) + " " + sharedPrefrencesManger.getLastUpdatedBalance() +
                                " " + getString(R.string.min_ago));
            }
        }
//        if (TransferFragment.isFromTransfer) {
//            onBackPressed();
//            TransferFragment.isFromTransfer = false;
//        }
//        if (sharedPrefrencesManger.getUsedActivity().equals("100")) {
//
//        } else {
//            initTabLayout();
//        }
        hideFrame();
//        sharedPrefrencesManger.setUsedActivity("");
    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
//        if (sharedPrefrencesManger.getUsedActivity().equals("100")) {
//
//        } else {
//            initTabLayout();
//        }
//        sharedPrefrencesManger.setUsedActivity("");
    }

    @Override
    public void onBackPressed() {
        try {
            super.onBackPressed();
            if (frameLayout.getVisibility() == View.VISIBLE) {
                hideFrame();
                setHeaderTitle(R.string.transfer_spaced);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        try {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                View v = getCurrentFocus();
                if (v instanceof EditText) {
                    Rect outRect = new Rect();
                    v.getGlobalVisibleRect(outRect);
                    if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                        v.clearFocus();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    }
                }
            }
            return super.dispatchTouchEvent(event);
        } catch (Exception ex) {
            CommonMethods.logException(ex);
            return true;
        }
    }

    public void setHeaderTitle(int stringId) {
        headerTxt.setText(getString(stringId));
    }

    @Override
    public void onBalanceUpdated() {
        balanceInputModel = new BalanceInputModel();
        balanceInputModel.setAppVersion(appVersion);
        balanceInputModel.setToken(token);
        balanceInputModel.setOsVersion(osVersion);
        balanceInputModel.setChannel(channel);
        balanceInputModel.setDeviceId(deviceId);
        balanceInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        balanceInputModel.setBillingPeriod(CommonMethods.getBillingPeriod());
        balanceInputModel.setLang(currentLanguage);
        balanceInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
        balanceInputModel.setAuthToken(authToken);
        new GetUserBalanceService(this, balanceInputModel, this);
    }

    public void setOnRequestSuccessfulListener(OnRequestSuccessfulListener onRequestSuccessfulListener) {
        this.onRequestSuccessfulListener = onRequestSuccessfulListener;
    }
}

