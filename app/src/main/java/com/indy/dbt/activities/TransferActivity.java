package com.indy.dbt.activities;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.controls.ServiceUtils;
import com.indy.customviews.CustomTextView;
import com.indy.models.balance.BalanceInputModel;
import com.indy.models.balance.BalanceResponseModel;
import com.indy.models.isEligable.IsEligableOutputModel;
import com.indy.models.isEligable.IsEligiableInputModel;
import com.indy.models.requestCreditFromFriend.RequestCreditInput;
import com.indy.models.transferCredit.TransferCreditOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.services.GetUserBalanceService;
import com.indy.services.TransferCreditService;
import com.indy.services.isEligableService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.utils.MaterialCheckBox;
import com.indy.views.activites.HelpActivity;
import com.indy.views.activites.MasterActivity;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.indy.views.fragments.utils.MasterFragment;

public class TransferActivity extends MasterActivity {

    View view;
    LinearLayout ll_confirmationMessage, ll_mainRequest,
            ll_successMessage;
    RelativeLayout root_layout;
    Button btn_next, btn_ok, btn_okSuccesful;
    EditText et_number, et_amount;
    TextView tv_titleConfirmation, tv_textConfirmation, btn_cancel, tv_titleSuccess, tv_textSuccess, tv_fees_applied;
    //    ImageView iv_contacts;
    TransferCreditOutput transferCreditOutput;
    TextInputLayout til_number, til_amount;
    IsEligiableInputModel isEligiableInputModel;
    IsEligableOutputModel isEligiableOutputModel;
    String serviceType = "";
    public static boolean isFromTransfer = false;
    private TextView headerTxt;
    public static TextView tv_lastUpdated;
    public static TextView titleTxt;
    private Button backImg, helpImg;
    public LinearLayout ll_topLayout;
    FrameLayout frameLayout;
    BalanceInputModel balanceInputModel;
    BalanceResponseModel balanceResponseModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dbt_transfer);
        initUI();
        initObjects();
        getBalanceService();
    }

    @Override
    public void initUI() {
        super.initUI();

        titleTxt = (TextView) findViewById(R.id.tv_title_findus);
        headerTxt = (TextView) findViewById(R.id.titleTxt);
        headerTxt.setText(getString(R.string.transfer_spaced));
        backImg = (Button) findViewById(R.id.backImg);
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        helpImg = (Button) findViewById(R.id.helpBtn);
        helpImg.setVisibility(View.INVISIBLE);
        helpImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TransferActivity.this, HelpActivity.class));
            }
        });
        frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
        frameLayout.setVisibility(View.GONE);
        ll_topLayout = (LinearLayout) findViewById(R.id.ll_top_layout);
        tv_lastUpdated = (TextView) findViewById(R.id.tv_last_updated);
        tv_lastUpdated.setText(getString(R.string.last_updated) + " " + sharedPrefrencesManger.getLastUpdatedBalance() +
                " " + getString(R.string.min_ago));

        btn_next = (Button) findViewById(R.id.nxtBtn);
        ll_confirmationMessage = (LinearLayout) findViewById(R.id.ll_confirmation_message);
        ll_successMessage = (LinearLayout) findViewById(R.id.ll_successful_transaction);
        ll_mainRequest = (LinearLayout) findViewById(R.id.ll_main_request);
        tv_titleConfirmation = (TextView) findViewById(R.id.tv_title);
        tv_textConfirmation = (TextView) findViewById(R.id.tv_warningText);
        et_number = (EditText) findViewById(R.id.mobile_number);
        et_amount = (EditText) findViewById(R.id.amount);
        btn_ok = (Button) findViewById(R.id.btn_ok);
        btn_okSuccesful = (Button) findViewById(R.id.btn_ok_success);
        btn_cancel = (CustomTextView) findViewById(R.id.btn_cancel);
        til_number = (TextInputLayout) findViewById(R.id.mobileNoInputLayout);
        til_amount = (TextInputLayout) findViewById(R.id.amountInputLayout);

        tv_titleSuccess = (TextView) findViewById(R.id.tv_title_success);
        tv_textSuccess = (TextView) findViewById(R.id.tv_warningText_success);

        root_layout = (RelativeLayout) findViewById(R.id.root_layout);
        tv_fees_applied = (TextView) findViewById(R.id.tv_fees_applied);
        ll_confirmationMessage.setVisibility(View.GONE);
        ll_mainRequest.setVisibility(View.VISIBLE);
        ll_successMessage.setVisibility(View.GONE);


        setHeaderTitle(R.string.send);

        if (sharedPrefrencesManger.isVATEnabled()) {
            tv_fees_applied.setText(getString(R.string.five_5_percent_transfer_fee_applies_and_vat));
        } else {
            tv_fees_applied.setText(getString(R.string.five_5_percent_transfer_fee_applies));

        }

//        iv_contacts = (ImageView) view.findViewById(R.id.iv_contacts);
//        iv_contacts.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                ((DBTPagerActivity) getActivity()).showFrame();
////                ((DBTPagerActivity) getActivity()).replaceFragmnet(new ContactListFragmnt(),R.id.frameLayout,false);7
////                startActivityForResult(new Intent(getActivity(), ContactListFragmnt.class),1001);
//                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
//                startActivityForResult(intent, 1);
//
//            }
//        });

        et_number.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                final int DRAWABLE_END_ENGLISH = 2;
                final int DRAWABLE_END_ARABIC = 0;
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentLanguage.equals("en")) {
                        if (event.getRawX() >= (et_number.getRight() - et_number.getCompoundDrawables()[DRAWABLE_END_ENGLISH].getBounds().width())) {
                            // your action here
                            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                            startActivityForResult(intent, 1);
                            return true;
                        }
                    } else {
                        if (event.getX() <= (et_number.getCompoundDrawables()[DRAWABLE_END_ARABIC].getBounds().width())) {
                            // your action here
                            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                            startActivityForResult(intent, 1);
                            return true;
                        }
                    }
                }
                return false;
            }
        });


        et_number.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {

                } else {
                    isValid();
                }
            }
        });

        et_number.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    isValid();
                    hideKeyBoard();
                    return true;
                }
                return true;
            }
        });
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyBoard();
                if (isValid()) {

                    serviceType = ServiceUtils.isEligable;
                    onConsumeService();
                }
            }
        });

        disableNextBtn();
    }

    private void initObjects() {

//        setupScreen();

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                serviceType = ServiceUtils.transferCredit;

                onConsumeService();
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_transfer_confirm_transfer_sending);


            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_transfer_cancel_sending_balance);


                hideConfirmationMesage();
            }
        });

        btn_okSuccesful.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                hideConfirmationMesage();
                et_amount.setText("");
                et_number.setText("");
                removeTextInputLayoutError(til_amount);
                removeTextInputLayoutError(til_number);
                setResult(RESULT_OK);
                TransferActivity.this.finish();
            }
        });

//        et_number.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                String tempNumber = "";
//                tempNumber = s.toString();
//                tempNumber = tempNumber.replace("+971", "0");
//                tempNumber = tempNumber.replace(" ", "");
//                tempNumber = tempNumber.replace("00971","0");
//                tempNumber = tempNumber.trim();
//                if (tempNumber.length() > 0 && isValid()) {
//                    enableNextBtn();
//                } else {
//                    disableNextBtn();
//                }
//            }
//
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//                // TODO Auto-generated method stub
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//                // TODO Auto-generated method stub
//            }
//        });
//
//        et_amount.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
////                if(et_number.getText().toString().trim().length()>0 && et_amount.getText().toString().trim().length()>0){
////                    enableNextBtn();
////                }else{
////                    disableNextBtn();
////                }
//
//                String amount = et_amount.getText().toString();
//                amount = amount.replace("AED", "");
//                amount = amount.replace(" ", "");
//                try {
//                    if (amount.length() > 0 && Integer.parseInt(amount) >= 2 && Integer.parseInt(amount) <= 150) {
//
//                        removeTextInputLayoutError(til_amount);
//                        if(isValid()){
//                            enableNextBtn();
//                        }
//                    } else {
//                        disableNextBtn();
//                        setTextInputLayoutError(til_amount, getString(R.string.invalid_number));
//                    }
//                } catch (NumberFormatException ex) {
//                    disableNextBtn();
//                    setTextInputLayoutError(til_amount, getString(R.string.invalid_number));
//
//                }
//
//            }
//
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//                // TODO Auto-generated method stub
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//                // TODO Auto-generated method stub
//            }
//        });

        et_amount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {

                } else {
                    isValid();
                }
            }
        });

        et_amount.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    isValid();
                    hideKeyBoard();
                    return true;
                }
                return true;
            }
        });
        MaterialCheckBox materialCheckBox = (MaterialCheckBox) findViewById(R.id.fl_tick_animation);
        ImageView iv_tick = (ImageView) findViewById(R.id.iv_tick);
        CommonMethods.drawCircle(materialCheckBox, iv_tick);
        hideKeyBoard(root_layout);
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        hideKeyBoard();
        addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        showFrame();

        if (serviceType.equals(ServiceUtils.transferCredit)) {
            RequestCreditInput requestCreditInput = new RequestCreditInput();
            requestCreditInput.setImsi(sharedPrefrencesManger.getMobileNo());
            requestCreditInput.setOsVersion(osVersion);
            requestCreditInput.setDeviceId(deviceId);
            requestCreditInput.setAppVersion(appVersion);
            String numberToSend = et_number.getText().toString();
            numberToSend = numberToSend.replace(" ", "");
            numberToSend = numberToSend.replace("+971", "0");
            numberToSend = numberToSend.replace("00971", "0");
            String amountToSend = et_amount.getText().toString();
            amountToSend = amountToSend.replace("AED", "");
            amountToSend = amountToSend.replace(" ", "");
            requestCreditInput.setAmount(amountToSend);

            //using dummy numbers
            requestCreditInput.setTargetMsisdn(numberToSend);
//            requestCreditInput.setTargetMsisdn(numberToSend);
            requestCreditInput.setChannel(channel);
            requestCreditInput.setLang(currentLanguage);
            requestCreditInput.setToken(token);
            //using dummy numbers
            requestCreditInput.setMsisdn(sharedPrefrencesManger.getMobileNo());
            requestCreditInput.setAuthToken(authToken);


            removeTextInputLayoutError(til_amount);
            removeTextInputLayoutError(til_number);
            //requestCreditInput.setMsisdn(sharedPrefrencesManger.getMobileNo());

            new TransferCreditService(this, requestCreditInput);
        } else if (serviceType.equals(ServiceUtils.isEligable)) {
            isEligiableInputModel = new IsEligiableInputModel();
            isEligiableInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
            isEligiableInputModel.setLang(currentLanguage);
            isEligiableInputModel.setDeviceId(deviceId);
            isEligiableInputModel.setOsVersion(osVersion);
            isEligiableInputModel.setAppVersion(appVersion);
            isEligiableInputModel.setChannel(channel);
            isEligiableInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
            String numberToSend = et_number.getText().toString();
            numberToSend = numberToSend.replace(" ", "");
            numberToSend = numberToSend.replace("+971", "0");

            String amountToSend = et_amount.getText().toString();
            amountToSend = amountToSend.replace("AED", "");
            amountToSend = amountToSend.replace(" ", "");
            isEligiableInputModel.setAmount(amountToSend);
            isEligiableInputModel.setTargetMsisdn(numberToSend);
            isEligiableInputModel.setToken(token);
            isEligiableInputModel.setAuthToken(authToken);
            new isEligableService(this, isEligiableInputModel);
        }
    }

    private void getBalanceService() {
        addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        showFrame();
        balanceInputModel = new BalanceInputModel();
        balanceInputModel.setAppVersion(appVersion);
        balanceInputModel.setToken(token);
        balanceInputModel.setOsVersion(osVersion);
        balanceInputModel.setChannel(channel);
        balanceInputModel.setDeviceId(deviceId);
        balanceInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        balanceInputModel.setBillingPeriod(CommonMethods.getBillingPeriod());
        balanceInputModel.setLang(currentLanguage);
        balanceInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
        balanceInputModel.setAuthToken(authToken);
        new GetUserBalanceService(this, balanceInputModel, this);
    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        onBackPressed();
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        onBackPressed();
        if (responseModel != null && responseModel.getResultObj() != null) {
            hideFrame();
            int j = 0;
            for (int i = 0; i < getSupportFragmentManager().getFragments().size(); i++) {

                if (getSupportFragmentManager().getFragments().get(i) instanceof LoadingFragmnet) {
                    j++;
                }

            }
            Fragment[] loadingFragments = new Fragment[j];
            for (int i = 0; i < j; i++) {
                loadingFragments[i] = new LoadingFragmnet();
            }
            this.removeFragment(loadingFragments);
            if (responseModel.getServiceType().equals(ServiceUtils.balanceServiceType)) {
                balanceResponseModel = (BalanceResponseModel) responseModel.getResultObj();
                if (balanceResponseModel != null) {
                    if (balanceResponseModel.getErrorCode() != null) {
                        showErrorFragment(balanceResponseModel.getErrorMsgEn());
                    } else {
                        titleTxt.setText(getString(R.string.your_balance_is_aed) + " " + balanceResponseModel.getAmount()+ " " + getString(R.string.aed_arabic_only));
                        tv_lastUpdated.setText(getString(R.string.last_updated) + " " + sharedPrefrencesManger.getLastUpdatedBalance() +
                                " " + getString(R.string.min_ago));
                        CommonMethods.updateBalanceAndLastUpdated(getString(R.string.your_balance_is_aed) + " " + balanceResponseModel.getAmount()+ " " + getString(R.string.aed_arabic_only)
                                , getString(R.string.last_updated) + " " + sharedPrefrencesManger.getLastUpdatedBalance() +
                                        " " + getString(R.string.min_ago));
                    }
                }
            }
            if (responseModel.getServiceType().equals(ServiceUtils.transferCredit)) {
                transferCreditOutput = (TransferCreditOutput) responseModel.getResultObj();
                if (transferCreditOutput != null && transferCreditOutput.getSent() != null && transferCreditOutput.getSent().equals("true")) {// && transferCreditOutput.getSent().equals("true")) {
                    CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_transfer_request_user_for_transfer);

                    showSuccessfulView();
//                addSentRequest();
                } else {
                    if (transferCreditOutput != null && transferCreditOutput.getErrorMsgEn() != null) {
                        if (sharedPrefrencesManger.getLanguage().equals("en")) {
                            showErrorFragment(transferCreditOutput.getErrorMsgEn());
                        } else {
                            showErrorFragment(transferCreditOutput.getErrorMsgAr());
                        }
                    } else {
                        showErrorFragment(getString(R.string.generice_error));
                    }
                }
            } else if (responseModel.getServiceType().equals(ServiceUtils.isEligable)) {
                isEligiableOutputModel = (IsEligableOutputModel) responseModel.getResultObj();
                if (isEligiableOutputModel != null && isEligiableOutputModel.getEligible()) {
                    showConfirmationView();

                } else {
                    CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_transfer_eligibility_failure);

                    if (isEligiableOutputModel != null && isEligiableOutputModel.getErrorMsgEn() != null) {
                        if (sharedPrefrencesManger.getLanguage().equals("en")) {
                            showErrorFragment(isEligiableOutputModel.getErrorMsgEn());
                        } else {
                            showErrorFragment(isEligiableOutputModel.getErrorMsgAr());

                        }
                    } else {
                        showErrorFragment(getString(R.string.generice_error));
                    }
                }
            }
        }
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        onBackPressed();
//        ((DBTPagerActivity) getActivity()).hideFrame();
    }

    public void addSentRequest() {
//        mSentRequestsList.add()

//        showSuccessfulView(selectedMode,selectedIndex);
//        selectedMode = -1;
    }

    public void showConfirmationView() {
        hideTopLayout();
        ll_mainRequest.setVisibility(View.GONE);
        ll_confirmationMessage.setVisibility(View.VISIBLE);
        ll_successMessage.setVisibility(View.GONE);
        String detail;

        //Delete sent request
        String amount = et_amount.getText().toString();
        amount = amount.replace("AED", "");
        amount.replace(" ", "");
        detail = getString(R.string.you_are_about_to_transfer) + " " + isEligiableOutputModel.getAmount() +
                " " + getString(R.string.aed_from_your_balance) + " " + et_number.getText().toString() + ". ";

        if (sharedPrefrencesManger.isVATEnabled()) {
            detail += "\n\n" + getString(R.string.you_ll_pay_) + " " + isEligiableOutputModel.getTotalAmount() + " " +
                    getString(R.string.with_the_transfer_fee_and_vat);
        } else {
            detail += "\n\n" + getString(R.string.you_ll_pay_) + " " + isEligiableOutputModel.getTotalAmount() + " " +
                    getString(R.string.with_the_transfer_fee);
        }
        tv_titleConfirmation.setText(getString(R.string.transfer_credit_to_friends));
        tv_textConfirmation.setText(detail);


    }

    public void showSuccessfulView() {
        hideTopLayout();
        ll_mainRequest.setVisibility(View.GONE);
        ll_confirmationMessage.setVisibility(View.GONE);
        ll_successMessage.setVisibility(View.VISIBLE);
        try {
            root_layout.setBackgroundResource(R.drawable.bg_confirmation_screen);
        } catch (Exception ex) {

        } catch (OutOfMemoryError er) {
            root_layout.setBackgroundResource(R.drawable.bg_confirmation_screen);

        }
        String detail;

        //reminder case
        String amount = et_amount.getText().toString();
        amount = amount.replace("AED", "");
        amount.replace(" ", "");

        if (sharedPrefrencesManger.isVATEnabled()) {
            if (sharedPrefrencesManger.getLanguage().equals("en")) {
                detail = getString(R.string.you_treated) + " " + et_number.getText().toString()
                        + " " + getString(R.string.to_aed) + " " + isEligiableOutputModel.getAmount() + ".";

                detail += " " + getString(R.string.with_the_transfer_fee_we_have_deducted_aed_and_vat) + " " + isEligiableOutputModel.getTotalAmount() +
                        " " + getString(R.string.from_your_account_make_sure_your_friend_hits_you_back_and_vat);
            } else {
                detail = getString(R.string.you_treated) + " " + isEligiableOutputModel.getAmount()
                        + " " + getString(R.string.to_aed) + " " + et_number.getText().toString() + ".";

                detail += " " + getString(R.string.with_the_transfer_fee_we_have_deducted_aed_and_vat) + " " + isEligiableOutputModel.getTotalAmount() +
                        " " + getString(R.string.from_your_account_make_sure_your_friend_hits_you_back_and_vat);
            }
        } else {
            if (sharedPrefrencesManger.getLanguage().equals("en")) {
                detail = getString(R.string.you_treated) + " " + et_number.getText().toString()
                        + " " + getString(R.string.to_aed) + " " + isEligiableOutputModel.getAmount() + ".";

                detail += " " + getString(R.string.with_the_transfer_fee_we_have_deducted_aed) + " " + isEligiableOutputModel.getTotalAmount() +
                        " " + getString(R.string.from_your_account_make_sure_your_friend_hits_you_back);
            } else {
                detail = getString(R.string.you_treated) + " " + isEligiableOutputModel.getAmount()
                        + " " + getString(R.string.to_aed) + " " + et_number.getText().toString() + ".";

                detail += " " + getString(R.string.with_the_transfer_fee_we_have_deducted_aed) + " " + isEligiableOutputModel.getTotalAmount() +
                        " " + getString(R.string.from_your_account_make_sure_your_friend_hits_you_back);
            }
        }
        tv_titleSuccess.setText(getString(R.string.transfer_succesful_simple));
        tv_textSuccess.setText(detail);


    }

    private void hideConfirmationMesage() {
        showTopLayout();
        root_layout.setBackgroundResource(0);
        root_layout.setBackgroundResource(0);
        ll_confirmationMessage.setVisibility(View.GONE);
        ll_successMessage.setVisibility(View.GONE);
        ll_mainRequest.setVisibility(View.VISIBLE);

    }

    public boolean isValid() {
        boolean retValue = true;

        if (et_number.getText().toString().length() < 10) {
            retValue = false;
            if (et_number.getText().toString().length() > 0)
                til_number.setError(getString(R.string.ouch_number_does_not_seem_valid));
        } else {
            String stringToCheck = et_number.getText().toString().replace(" ", "");
            String noStr = stringToCheck.substring(0, 2);
            if (!noStr.equalsIgnoreCase("05") && !stringToCheck.substring(0, 4).contains("+971") && !stringToCheck.substring(0, 5).contains("00971")) {

                retValue = false;
                if (et_number.getText().toString().length() > 0)
                    til_number.setError(getString(R.string.ouch_number_does_not_seem_valid));
            } else {
                removeTextInputLayoutError(til_number);
            }
        }
        String amount = et_amount.getText().toString();
        amount = amount.replace("AED", "");
        amount = amount.replace(" ", "");
        try {
            if (amount.length() > 0 && Integer.parseInt(amount) >= 2 && Integer.parseInt(amount) <= 150) {
                removeTextInputLayoutError(til_amount);
            } else {
                retValue = false;
                if (amount.length() > 0)
                    setTextInputLayoutError(til_amount, getString(R.string.invalid_number));
            }
        } catch (NumberFormatException ex) {
            retValue = false;
            if (amount.length() > 0)
                setTextInputLayoutError(til_amount, getString(R.string.invalid_number));
        }

        if (retValue) {
            enableNextBtn();
        } else {
            disableNextBtn();
        }
        return retValue;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (data != null && data.getData() != null) {
                String phoneNo = null;
                Uri uri = data.getData();
                Cursor cursor = getContentResolver().query(uri, null, null, null, null);

                if (cursor.moveToFirst()) {
                    int phoneIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                    phoneNo = cursor.getString(phoneIndex);
                }

                cursor.close();

                et_number.setText(phoneNo);

            }
        }
    }

    private void showErrorFragment(String error) {
        showFrame();
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, error);
        errorFragment.setArguments(bundle);
        replaceFragmnet(errorFragment, R.id.frameLayout, true);
    }

    private void enableNextBtn() {
        btn_next.setAlpha(1f);
        btn_next.setClickable(true);
    }

    private void disableNextBtn() {
        btn_next.setAlpha(.5f);
        btn_next.setClickable(false);
    }


    public void setHeaderTitle(int stringId) {
        headerTxt.setText(getString(stringId));
    }

    public void hideTopLayout() {
        ll_topLayout.setVisibility(View.GONE);
        frameLayout.setVisibility(View.GONE);
    }

    public void showTopLayout() {
        ll_topLayout.setVisibility(View.VISIBLE);
        frameLayout.setVisibility(View.VISIBLE);
    }

    public void showFrame() {
        frameLayout.setVisibility(View.VISIBLE);
    }

    public void hideFrame() {
        frameLayout.setVisibility(View.GONE);
    }

}
