package com.indy.dbt.listeners;

public interface OnRequestSuccessfulListener {

    void onRequestSuccessful();

}
