package com.indy.dbt.listeners;

public interface OnBalanceUpdatedListener {

    void onBalanceUpdated();

}
