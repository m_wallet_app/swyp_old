package com.indy.dbt.listeners;

public interface OnDataTransferConfirmedListener {

    void onCancelled();
    void onConfirmed();
    void onOk();

}
