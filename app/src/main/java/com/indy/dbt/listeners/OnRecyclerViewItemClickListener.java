package com.indy.dbt.listeners;

import android.view.View;

public interface OnRecyclerViewItemClickListener {
    void onItemClick(View view, int position);
}
