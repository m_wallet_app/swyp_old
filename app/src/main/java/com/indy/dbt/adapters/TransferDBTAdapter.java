package com.indy.dbt.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.indy.dbt.activities.DBTPagerActivity;
import com.indy.dbt.fragments.BalanceTransferFragment;
import com.indy.dbt.fragments.DataTransferFragment;
import com.indy.dbt.fragments.LCTMobileTransferFragment;
import com.indy.views.fragments.store.request.AlertsFragment;
import com.indy.views.fragments.store.request.RequestFragment;
import com.indy.views.fragments.store.request.TransferFragment;

public class TransferDBTAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    DBTPagerActivity DBTPagerActivity;

    public TransferDBTAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    public void setActivity(DBTPagerActivity activity){
        this.DBTPagerActivity = activity;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                BalanceTransferFragment requestFragment = new BalanceTransferFragment();
                return requestFragment;
            case 1:
                LCTMobileTransferFragment requestFragment2 = new LCTMobileTransferFragment();
                requestFragment2.setOnBalanceUpdatedListener(DBTPagerActivity);
                return requestFragment2;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}

