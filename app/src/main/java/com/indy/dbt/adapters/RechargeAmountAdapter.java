package com.indy.dbt.adapters;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.dbt.activities.DBTPagerActivity;
import com.indy.dbt.listeners.OnRecyclerViewItemClickListener;
import com.indy.dbt.models.DataAmountModel;

import java.util.List;

/**
 * Created by Amir Jehangir on 7/26/2017.
 */

public class RechargeAmountAdapter extends RecyclerView.Adapter<RechargeAmountAdapter.ViewHolder> {

    private final DBTPagerActivity mActivity;
    private List<DataAmountModel> newsItems;
    private OnRecyclerViewItemClickListener onItemClickListener;
    private Typeface typeface;


    public RechargeAmountAdapter(DBTPagerActivity mActivity, List<DataAmountModel> items, OnRecyclerViewItemClickListener onItemClickListener) {
        this.mActivity = mActivity;
        this.newsItems = items;
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public RechargeAmountAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater mInflater = LayoutInflater.from(parent.getContext());
        final View sView = mInflater.inflate(R.layout.item_recharge_amount, parent, false);
//        prefHelper = new SharedPreferencesManager();
//        prefHelper.setContext(mActivity);
        return new RechargeAmountAdapter.ViewHolder(sView);
    }

    @Override
    public void onBindViewHolder(final RechargeAmountAdapter.ViewHolder holder, int position) {

        if (newsItems.get(position) != null) {
            if (newsItems.get(position).getAmount() > 999) {
                holder.label_mparking_itemnumber.setText(String.valueOf(newsItems.get(position).getAmount() / 1000));
                holder.label_mparking_itemtype.setText("GB");
            } else {
                holder.label_mparking_itemnumber.setText(String.valueOf(newsItems.get(position).getAmount()));
                holder.label_mparking_itemtype.setText("MB");
            }
        }


        if (newsItems.get(position).isSelected()) {
            holder.ll_parent_region.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.title_usage_pack));
            holder.label_mparking_itemnumber.setTextColor(ContextCompat.getColor(mActivity, R.color.color_white));
            holder.label_mparking_itemtype.setTextColor(ContextCompat.getColor(mActivity, R.color.color_white));
        } else {
            holder.ll_parent_region.setBackgroundColor(ContextCompat.getColor(mActivity, android.R.color.white));
            holder.label_mparking_itemnumber.setTextColor(ContextCompat.getColor(mActivity, R.color.black_all_36));
            holder.label_mparking_itemtype.setTextColor(ContextCompat.getColor(mActivity, R.color.grey_black));
        }

    }


    @Override
    public int getItemCount() {
        return newsItems.size();
    }


//    public void setData(ArrayList<NewsListModel> propertyInfoArrayList) {
//        this.newsItems = (ArrayList) propertyInfoArrayList;
//        notifyDataSetChanged();
//    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView label_mparking_itemnumber, label_mparking_itemtype;
        LinearLayout ll_parent_region;

        public ViewHolder(View view) {
            super(view);
            //  ll_parent_region = (LinearLayout)view.findViewById(R.id.ll_parent_region);
            label_mparking_itemnumber = (TextView) view.findViewById(R.id.tv_amount);
            label_mparking_itemtype = (TextView) view.findViewById(R.id.tv_unit);
            ll_parent_region = (LinearLayout) view.findViewById(R.id.ll_amount_parent);
            ll_parent_region.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {

            if (onItemClickListener != null) {
                setSelected(getLayoutPosition());
                onItemClickListener.onItemClick(view, getLayoutPosition());
            }

        }
    }

    public void setOnItemClickListener(final OnRecyclerViewItemClickListener mItemClickListener) {

        this.onItemClickListener = mItemClickListener;
    }


    private void setSelected(int position) {

        for (int i = 0; i < newsItems.size(); i++) {

            if (i != position) {
                newsItems.get(i).setSelected(false);
            } else {
                newsItems.get(position).setSelected(true);
            }
        }
        notifyDataSetChanged();
    }

    public void setAllUnSelected() {
        for (int i = 0; i < newsItems.size(); i++) {
            newsItems.get(i).setSelected(false);
        }
        notifyDataSetChanged();
    }


}



