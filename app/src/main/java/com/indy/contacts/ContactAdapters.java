package com.indy.contacts;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.indy.R;

public class ContactAdapters extends ArrayAdapter<String> {
    String[] name;
    String[] num_time;
    Context context;
    ViewGroup parent1;
    TextView textView;
    View single_row;

    public ContactAdapters(Activity context, String[] text, String[] number_time) {
        super(context, R.layout.contact_item, text);
        // TODO Auto-generated constructor stub
        this.name = text;
        this.num_time = number_time;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        try {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            single_row = inflater.inflate(R.layout.contact_item, null,
                    true);
            textView = (TextView) single_row.findViewById(R.id.Itemname);
            ImageView imageView = (ImageView) single_row.findViewById(R.id.icon);
            imageView.setVisibility(View.INVISIBLE);
            textView.setText(name[position]);
            parent1 = parent;
            if (name[position].equalsIgnoreCase(num_time[position])) {
                textView.setTypeface(null, Typeface.BOLD);
                textView.setBackgroundColor(context.getResources().getColor(R.color.green_usage));
                textView.setTextColor(context.getResources().getColor(android.R.color.black));
            }

            TextView textPNumber = (TextView) single_row.findViewById(R.id.Number);
            textPNumber.setText(num_time[position]);
            textPNumber.setVisibility(View.GONE);

        } catch (Exception e) {
//            Toast.makeText(parent1.getContext(), "Error  : " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }


        return single_row;
    }
}
