package com.indy.contacts;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.ThumbnailUtils;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.contacts.async_task_thread_pool.AsyncTaskEx;
import com.indy.contacts.async_task_thread_pool.AsyncTaskThreadPool;

/**
 * Created by emad on 7/27/16.
 */
public class ContactDetailActivity extends AppCompatActivity {
    CollapsingToolbarLayout toolbarLayout;
    ImageView imageView, defaultImageView;
    public AsyncTaskEx<Void, Void, Bitmap> updateTask;
    LinearLayout ll_contact_number;
    public final AsyncTaskThreadPool mAsyncTaskThreadPool = new AsyncTaskThreadPool(1, 2, 10);
    private TextView contactType, contactNo;
    RelativeLayout dialLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_detail_activity);
        final Contact contact = getIntent().getExtras().getParcelable("contact");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        imageView = (ImageView) findViewById(R.id.imageView);
        defaultImageView = (ImageView) findViewById(R.id.defaultView);
        contactType = (TextView) findViewById(R.id.contactType);
        contactNo = (TextView) findViewById(R.id.contactNo);
        dialLayout = (RelativeLayout) findViewById(R.id.dialLayout);
        toolbar.setTitleTextColor((Color.parseColor("#FFFFFF")));
        toolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        setCollapsingToolbarLayoutTitle(contact.getDisplayName());
        contactNo.setText(getPhoneType(Integer.valueOf(contact.getNumber())));
        contactType.setText(contact.getType());
        ll_contact_number = (LinearLayout) findViewById(R.id.ll_contact_number);

        boolean hasPhoto = !TextUtils.isEmpty(contact.photoId);
        if (hasPhoto) {
            defaultImageView.setVisibility(View.GONE);
            imageView.setVisibility(View.VISIBLE);

            updateTask = new AsyncTaskEx<Void, Void, Bitmap>() {

                @Override
                public Bitmap doInBackground(final Void... params) {
                    if (isCancelled())
                        return null;
                    final Bitmap b = ContactImageUtil.loadContactPhotoThumbnail(ContactDetailActivity.this, contact.getThumbId(), getLargestScreenDimension());
                    if (b != null)
                        return ThumbnailUtils.extractThumbnail(b, getLargestScreenDimension(),
                                getLargestScreenDimension());
                    return null;
                }

                @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onPostExecute(final Bitmap result) {
                    super.onPostExecute(result);
                    if (result == null)
                        return;
//                    Drawable drawable = (Drawable)new BitmapDrawable(result);
//                    toolbarLayout.setBackground(drawable);
                    imageView.setImageBitmap(result);
                }
            };
            mAsyncTaskThreadPool.executeAsyncTask(updateTask);
        } else {
            imageView.setVisibility(View.GONE);
            defaultImageView.setVisibility(View.VISIBLE);
        }
        dialLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                dial(contact.getTransferType());
            }
        });
        dialLayout.setVisibility(View.GONE);
        setSupportActionBar(toolbar);
        ll_contact_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent resultIntent = new Intent();
                resultIntent.putExtra("contact_number",contactType.getText().toString());
                setResult(Activity.RESULT_OK,resultIntent);
                finish();
            }
        });
    }

    private int getLargestScreenDimension() {
        // Gets a DisplayMetrics object, which is used to retrieve the display's pixel height and
        // width
        final DisplayMetrics displayMetrics = new DisplayMetrics();

        // Retrieves a displayMetrics object for the device's default display
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        final int height = displayMetrics.heightPixels;
        final int width = displayMetrics.widthPixels;

        // Returns the larger of the two values
        return height > width ? height : width;
    }

    private void setCollapsingToolbarLayoutTitle(String title) {
        toolbarLayout.setTitle(title);
        toolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
        toolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
        toolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBarPlus1);
        toolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBarPlus1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private String getPhoneType(int type) {
        String phoneType = "";
        switch (type) {
            case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                phoneType = "Home";
                break;
            case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                phoneType = "Mobile";
                break;
            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                phoneType = "Work";
                break;
            case ContactsContract.CommonDataKinds.Phone.TYPE_FAX_HOME:
                phoneType = "Home Fax";
                break;
            case ContactsContract.CommonDataKinds.Phone.TYPE_FAX_WORK:
                phoneType = "Work Fax";
                break;
            default:
                phoneType = "Other";
                break;
        }
        return phoneType;

    }

//    private void dial(String phoneNo) {
//        String phoneStr = "*145*" + phoneNo + Uri.encode("#");
//        Log.v("phone str", phoneStr);
//        Intent intent = new Intent(Intent.ACTION_CALL);
//        intent.setData(Uri.parse("tel:" + phoneStr));
//        startActivity(intent);
//    }
}
