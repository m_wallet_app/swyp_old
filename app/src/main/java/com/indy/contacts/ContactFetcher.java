package com.indy.contacts;

import android.app.Activity;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by emad on 7/25/16.
 */
public class ContactFetcher {
    Activity mActivity;

    public ContactFetcher(Activity activity) {
        this.mActivity = activity;
    }

    public Map listContactsByName(String name) {
        Map searchcontactMap = new HashMap();
        String ret = null;
        String selection = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " like'%" + name + "%'";
        String[] projection = new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME};
        Cursor cursor = mActivity.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                projection, selection, null, null);

        if (cursor.moveToFirst()) {

            do {
                String contactId =
                        cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                String searchname = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                searchcontactMap.put(searchname, contactId);
            } while (cursor.moveToNext());

            cursor.close();
        }

        return searchcontactMap;
    }

    public List<String[]> fillContactList(Map<String, String> map) {
        List<String[]> contactData = new ArrayList<>();
        String[] cNames;
        String[] cNumbers;
        Integer[] cid2;
        int i = 0;
        Object[] keys = map.keySet().toArray();
        Arrays.sort(keys);
        String temp = "%";
        ArrayList<String> nameArray = new ArrayList<String>();
        ArrayList<String> numberArray = new ArrayList<String>();
        for (Object key : keys) {
            try {
                if (!map.get(key).toString().equals("No Results"))
                    Log.v("temp", "temp is"+ temp +" ");
                {
                    if (!temp.equalsIgnoreCase(String.valueOf(map.get(key).toString().charAt(0)))) {
                        temp = String.valueOf(map.get(key).toString().charAt(0)).toUpperCase();
                        nameArray.add(temp);
                        numberArray.add(temp);
                        i++;
                        nameArray.add(map.get(key).toString());
                        numberArray.add(" ");
                        i++;
                    } else {
                        nameArray.add(map.get(key).toString().toLowerCase());
                        numberArray.add(" ");
                        i++;
                    }
                }
            } catch (Exception e) {
                Log.v("Madhes Exception ", e.toString());
                throw e;

            }
        }

        try {

            cNames = new String[nameArray.size()];
            cNumbers = new String[nameArray.size()];
            cid2 = new Integer[nameArray.size()];

            for (int j = 0; j < nameArray.size(); j++) {
                cNames[j] = nameArray.get(j);
                cNumbers[j] = numberArray.get(j);
                cid2[j] = j;
            }
            contactData.add(cNames);
            contactData.add(cNumbers);
//            contactData.add(cid2);

        } catch (Exception e) {
            Log.v("Madhes Exception ", e.toString());
            // Deal with e as you please.
            //e may be any type of exception at all.
            throw e;

        }
        return contactData;
    }
//
//        ccadapter = new ContactCustomListViewAdapter(this, cid2, cNames, cNumbers);
//        lv = (ListView) findViewById(R.id.contactlist);
//        lv.setAdapter(ccadapter);
//        listClick = (ListView) findViewById(R.id.contactlist);
//        listClick.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
//
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//                Object listItem = listClick.getItemAtPosition(position);
//                Map<String, String> detailDisplay = slectedUserNumbers(listItem.toString());
//                if (detailDisplay.size() > 0) {
//                    HashMap<String, String> detailDisplaycopy = new HashMap<String, String>(detailDisplay);
//                    Intent intent = new Intent(Contactslist.this, contactsReceive.class);
//                    intent.putExtra("map", detailDisplaycopy);
//                    startActivity(intent);
//                }
//            }
//
//
//        });

}
