package com.indy.contacts;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by emad on 7/27/16.
 */
public class Contact implements Parcelable {
    public long contactId;
    public Uri contactUri;
    public String displayName;
    public String photoId;
    public String thumbId;
    public String number;
    public String type;

    public Contact() {
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNumber() {
        return number;
    }

    public String getType() {
        return type;
    }

    public void setThumbId(String thumbId) {
        this.thumbId = thumbId;
    }

    public String getThumbId() {
        return thumbId;
    }

    protected Contact(Parcel in) {
        contactId = in.readLong();
        contactUri = in.readParcelable(Uri.class.getClassLoader());
        displayName = in.readString();
        photoId = in.readString();
        thumbId = in.readString();
        number = in.readString();
        type = in.readString();
    }

    public static final Creator<Contact> CREATOR = new Creator<Contact>() {
        @Override
        public Contact createFromParcel(Parcel in) {
            return new Contact(in);
        }

        @Override
        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };

    public void setContactId(long contactId) {
        this.contactId = contactId;
    }

    public void setContactUri(Uri contactUri) {
        this.contactUri = contactUri;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setPhotoId(String photoId) {
        this.photoId = photoId;
    }

    public long getContactId() {
        return contactId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getPhotoId() {
        return photoId;
    }

    public Uri getContactUri() {
        return contactUri;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(contactId);
        dest.writeParcelable(contactUri, flags);
        dest.writeString(displayName);
        dest.writeString(photoId);
        dest.writeString(thumbId);
        dest.writeString(type);
        dest.writeString(number);
    }
}
