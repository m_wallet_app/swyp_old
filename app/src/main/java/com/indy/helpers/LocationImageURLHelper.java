package com.indy.helpers;

import android.content.Context;
import android.location.Location;

/**
 * Created by emad on 8/16/16.
 */
public class LocationImageURLHelper {
    private Context mContext;

    public LocationImageURLHelper(Context context) {
        this.mContext = context;
    }

    public static String getImageURL(Location latLng) {

        String url = "http://maps.google.com/maps/api/staticmap?center=" + latLng.getLatitude() + "," + latLng.getLongitude() + "&4=25&size=4000x2000&markers=color:blue|" + latLng.getLatitude() + "," + latLng.getLongitude() + "&sensor=false";
        return url;
    }
}
