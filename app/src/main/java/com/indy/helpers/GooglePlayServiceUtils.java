package com.indy.helpers;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.indy.gcm.QuickstartPreferences;
import com.indy.utils.SharedPrefrencesManger;

/**
 * Created by emad on 8/21/16.
 */
public class GooglePlayServiceUtils {
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    Activity mActivity;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private boolean isReceiverRegistered;
    boolean sentToken;
    SharedPreferences sharedPreferences;
    SharedPrefrencesManger sharedPrefrencesManger;

    public GooglePlayServiceUtils(Activity activity) {
        this.mActivity = activity;
        sharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(activity);

        sharedPrefrencesManger = new SharedPrefrencesManger(activity);
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
//                mRegistrationProgressBar.setVisibility(ProgressBar.GONE);

                sentToken = sharedPreferences
                        .getBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false);
                if (sentToken) {
                    Log.v("token_status", sentToken + " ");
                } else {
                    Log.v("token_status", sentToken + " ");
                }
            }
        };
    }

    public boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(mActivity);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(mActivity, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i("Playservice error", "This device is not supported.");
//                finish();
            }
            return false;
        }
        return true;
    }

    public void startIntentService() {
        if (sharedPrefrencesManger.getToken() == null
                || sharedPrefrencesManger.getToken().isEmpty()) {
            GooglePlayServiceUtils googlePlayServiceUtils = new GooglePlayServiceUtils(mActivity);
            if (googlePlayServiceUtils.checkPlayServices()) {
                // Start IntentService to register this application with GCM.
//                Intent intent = new Intent(mActivity, RegistrationIntentService.class);
//                mActivity.startService(intent);
            }
        }
    }

    public void registerReceiver() {
        if (sentToken) {
            if (!isReceiverRegistered) {
                LocalBroadcastManager.getInstance(mActivity).registerReceiver(mRegistrationBroadcastReceiver,
                        new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));
                isReceiverRegistered = true;
            }
        }
    }

    public void stopReceiver() {
        LocalBroadcastManager.getInstance(mActivity).unregisterReceiver(mRegistrationBroadcastReceiver);
        isReceiverRegistered = false;

    }
}
