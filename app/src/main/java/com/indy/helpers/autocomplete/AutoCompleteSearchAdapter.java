package com.indy.helpers.autocomplete;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.indy.R;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;


public class AutoCompleteSearchAdapter extends ArrayAdapter<Search> {

    private ArrayList<Search> items;
    private ArrayList<Search> itemsAll;
    private ArrayList<Search> suggestions;
    private int viewResourceId;
    OnItemClickListener onItemClickListener;
    private ArrayList<String> itemsToSearch;

    public AutoCompleteSearchAdapter(Context context, int viewResourceId, ArrayList<Search> items) {
        super(context, viewResourceId, items);
        this.items = items;
        this.itemsAll = (ArrayList<Search>) items.clone();
        this.suggestions = new ArrayList<Search>();
        this.viewResourceId = viewResourceId;
        this.itemsToSearch = new ArrayList<String> ();
        for(int i=0;i<items.size();i++){
            this.itemsToSearch.add(items.get(i).getName().toLowerCase());
        }
    }

    @SuppressLint("NewApi")
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(viewResourceId, null);
        }
        final Search search = items.get(position);
        if (search != null) {
            TextView nameLabel = (TextView) v.findViewById(R.id.searchNameLabel);
            if (nameLabel != null) {

                nameLabel.setCompoundDrawablePadding(10);
                nameLabel.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                nameLabel.setText(search.getName());

            }
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClick(v, position, new Search(search.getName(), search.getLatitude(), search.getLongitude()));
                }
            });
        }


        return v;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            String str = ((Search) (resultValue)).getName();
            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();

                for (int i=0;i<itemsToSearch.size();i++) {

                    if (itemsToSearch.get(i).contains(constraint.toString().toLowerCase())) {
                        suggestions.add(itemsAll.get(i));
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<Search> filteredList = (ArrayList<Search>) results.values;
            if (results != null && results.count > 0) {
                clear();
                try {
                    addAll(filteredList);
//                    for (Search c : filteredList) {
//
//                        Search temp = new Search(c.getName(), c.getLatitude(), c.getLongitude());
//                        add(temp);
//
//                    }
                } catch (ConcurrentModificationException ex) {

                ex.printStackTrace();
//                for (Search c : filteredList) {
//                    add(c);
//
                }

                notifyDataSetChanged();
            }
        }
    };
//
//    @Override
//    public void onClick(View view) {
//        if (onItemClickListener != null) {
////            onItemClickListener.onItemClick(view, 0,new Search("sds"));
//        }
//    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position, Search search);

    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.onItemClickListener = mItemClickListener;
    }
}
