package com.indy.helpers.autocomplete;

/**
 * Created by hadi.mehmood on 12/23/2015.
 */
public class Search {

    String name;
    String titleEn;
    String titleAr;
    String id;

    Double latitude;
    Double longitude;

    public Search(String name){
        this.name = name;
    }

    public Search(String name, Double lat, Double lon) {
        this.name = name;
        this.latitude = lat;
        this.longitude = lon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Search(String name, String titleEn, String titleAr) {
        this.name = name;
        this.titleEn = titleEn;
        this.titleAr = titleAr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitleEn() {
        return titleEn;
    }

    public void setTitleEn(String titleEn) {
        this.titleEn = titleEn;
    }

    public String getTitleAr() {
        return titleAr;
    }

    public void setTitleAr(String titleAr) {
        this.titleAr = titleAr;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
