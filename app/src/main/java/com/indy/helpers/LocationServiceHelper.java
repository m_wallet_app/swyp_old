package com.indy.helpers;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import com.indy.controls.BaseInterface;
import com.indy.models.locations.LocationModel;
import com.indy.models.utils.BaseResponseModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by emad on 8/16/16.
 */
public class LocationServiceHelper {
    private LocationModel _locationModel;
    private Location _location;
    private List<String> addressList = null;
    private Context _context;
    BaseResponseModel mBaseResponseModel;
    BaseInterface mBaseInterface;
    private StringBuilder cityAddress = new StringBuilder();
    private String serviceType = "LocationService";

    public LocationServiceHelper(LocationModel locationModel, BaseInterface baseInterface) {
        this._locationModel = locationModel;
        this.mBaseResponseModel = new BaseResponseModel();
        this.mBaseInterface = baseInterface;
        new getAddressInfo().execute();
    }

    private List<String> setAddress() {
        List<String> addressList = new ArrayList<>();
        String address = null;
        try {
            Geocoder geocoder;
            List<Address> addresses = null;
            if (_locationModel != null) {
                geocoder = new Geocoder(_locationModel.getActivity(), new Locale("EN"));
                addresses = geocoder.getFromLocation(_locationModel.getLocation().getLatitude(), _locationModel.getLocation().getLongitude(), 1);
            } else {
                geocoder = new Geocoder(_context, new Locale("EN"));
                addresses = geocoder.getFromLocation(_location.getLatitude(), _location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            }
            address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
//            String knownName = addresses.get(0).getFeatureName();
            Log.v("citycity", address + " " + city + " " + state + " " + country + " " + postalCode + " ");
            addressList.add(address);
            addressList.add(city);
        } catch (Exception e) {
            e.getStackTrace();
        }
        return addressList;
    }


    private class getAddressInfo extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            addressList = setAddress();
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            if (addressList != null) {
                if (addressList.size() > 0)
                    for (int index = 0; index < addressList.size(); index++) {
                        if (addressList.get(index) != null && !addressList.get(index).equalsIgnoreCase(null))
                            cityAddress.append(addressList.get(index));
                        cityAddress.append(" ");
                    }
            } else {
                cityAddress = null;
            }
            mBaseResponseModel.setServiceType(serviceType);
            mBaseResponseModel.setResultObj(cityAddress);
            mBaseInterface.onSucessListener(mBaseResponseModel);
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }


    public LocationModel get_locationModel() {
        return _locationModel;
    }

    public List<String> getAddress() {
        return addressList;
    }
}
