package com.indy.helpers;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by emad on 9/5/16.
 */
public class Fonts {

        public enum FontTypes {
            bold,
            light,
            medium,
            opensans,
            opensanslight,
            allerRegular,
            openSansRegular
        }

        public static Typeface getFont(Context context, int resIdFontName) {
            return Typeface.createFromAsset(context.getAssets(), getFontPath(context, resIdFontName));
        }

        public static String getFontPath(Context context, int resIdFontName) {
        return String.format("fonts/%s", context.getString(resIdFontName));
    }

    public static Typeface getBoldFont(Context context) {
        return AppFont.getInstance(context).get_boldTypeFace();
    }

    public static Typeface getLightFont(Context context) {
        return AppFont.getInstance(context).get_lightTypeFace();
    }

    public static Typeface getMediumFont(Context context) {
        return AppFont.getInstance(context).get_mediumTypeFace();
    }

    public static Typeface getOpenSansSemiBold(Context context) {
        return AppFont.getInstance(context).get_open_sans_TypeFace();
    }
    public static Typeface getOpenSansLight(Context context) {
        return AppFont.getInstance(context).get_open_sans_Typeface_Light();
    }

    public static Typeface getAllerRegular(Context context){
        return AppFont.getInstance(context).get_aller_regular();
    }
    public static Typeface getOpenSansRegular(Context context){
        return AppFont.getInstance(context).get_open_sans_regular();
    }

    public static Typeface getFont(Context context, FontTypes fontType) {

        switch (fontType) {
            case bold:
                return getBoldFont(context);

            case light:
                return getLightFont(context);
            case medium:
                return getMediumFont(context);
            case opensans:
                return getOpenSansSemiBold(context);
            case opensanslight:
                return getOpenSansLight(context);

            case allerRegular:
                return getAllerRegular(context);

            case openSansRegular:
                return getOpenSansRegular(context);

            default:
                return getOpenSansRegular(context);

        }


    }
}

