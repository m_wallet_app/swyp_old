package com.indy.helpers;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.indy.R;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.SwpeMainActivity;
import com.indy.views.fragments.gamification.challenges.BadgeUnlockedActivity;
import com.indy.views.fragments.gamification.events.SuccessfulShareActivity;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

/**
 * Created by hadi.mehmood on 12/4/2016.
 */
public class IndyFireBaseNotificationReceiver extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";
    public static int reqCode = 10001;
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //Displaying data in log
        //It is optional
        if(remoteMessage!=null)
        Log.d(TAG, "From: " + remoteMessage.getFrom());
//        if(remoteMessage.getNotification()!=null)
        Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());
        Log.d(TAG, "Notification Message Data: " + remoteMessage.getData());

        //Calling method to generate notification
        if (remoteMessage.getData() != null && remoteMessage.getData().size() > 0) {
            String id = remoteMessage.getData().get(ConstantUtils.notification_type);
            if(id!=null) {
                switch (id) {

                    case "1":

                        Intent badgeIntent = new Intent(getApplicationContext(), BadgeUnlockedActivity.class);
                        badgeIntent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                        badgeIntent.putExtra(ConstantUtils.KEY_NOTIFICATION_ID_FOR_SERVICE, remoteMessage.getData().get(ConstantUtils.NOTIFICATION_ID_FROM_PUSH));
                        badgeIntent.putExtra(ConstantUtils.notification_type, remoteMessage.getData().get(ConstantUtils.notification_type));

                        startActivity(badgeIntent);
                        break;
                    case "2":
                        Intent shareIntent = new Intent(getApplicationContext(), SuccessfulShareActivity.class);
                        shareIntent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                        shareIntent.putExtra(ConstantUtils.KEY_NOTIFICATION_ID_FOR_SERVICE, remoteMessage.getData().get(ConstantUtils.NOTIFICATION_ID_FROM_PUSH));
                        startActivity(shareIntent);
                        break;
                    case "3":

                        Intent badgeIntent2 = new Intent(getApplicationContext(), BadgeUnlockedActivity.class);
                        badgeIntent2.setFlags(FLAG_ACTIVITY_NEW_TASK);
                        badgeIntent2.putExtra(ConstantUtils.KEY_NOTIFICATION_ID_FOR_SERVICE, remoteMessage.getData().get(ConstantUtils.NOTIFICATION_ID_FROM_PUSH));
                        badgeIntent2.putExtra(ConstantUtils.notification_type, remoteMessage.getData().get(ConstantUtils.notification_type));
                        startActivity(badgeIntent2);
                        break;



                    default:
                        sendNotification(remoteMessage);
                        break;

                }
            }
//            if(remoteMessage.getData().get("id").equalsIgnoreCase("3")) {
//                Intent intent = new Intent(getApplicationContext(), BadgeUnlockedActivity.class);
//                intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
//                intent.putExtra(ConstantUtils.KEY_NOTIFICATION_ID_FOR_SERVICE, remoteMessage.getData().get("notificationId"));
//                startActivity(intent);
//            }

        } else {

            sendNotification(remoteMessage);
        }
    }

    //This method is only generating push notification
    //It is same as we did in earlier posts
    public NotificationCompat.Builder notificationBuilder;
    public Uri defaultSoundUri;
    public  PendingIntent pendingIntent;
    private void sendNotification(final RemoteMessage message) {
        Intent intent = new Intent(this, SwpeMainActivity.class);
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.notification_type,message.getData().get(ConstantUtils.notification_type));
        bundle.putString(ConstantUtils.KEY_NOTIFICATION_ID,message.getData().get(ConstantUtils.KEY_NOTIFICATION_ID));
        bundle.putString(ConstantUtils.KEY_ITEM_ID,message.getData().get(ConstantUtils.KEY_ITEM_ID));

        intent.putExtras(bundle);
//        intent.addFlags(Intent.FLAG_);
        intent.addFlags(
                Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NEW_TASK);
        try {

//            if(SwpeMainActivity.swpeMainActivityInstance!=null || RegisterationActivity.regInstance!=null){
//
//            }else{
//
//            }
            int requestCodeToSend = reqCode ++;
            pendingIntent = PendingIntent.getActivity(this, requestCodeToSend /* Request code */, intent,
                    PendingIntent.FLAG_ONE_SHOT);

            defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            ImageLoader.getInstance().displayImage(message.getData().get(ConstantUtils.media_url), new ImageView(getApplicationContext()), new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {

                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                    notificationBuilder = new NotificationCompat.Builder(getApplicationContext())
                            .setSmallIcon(R.drawable.ic_launcher)
//                            .setContentTitle(getString(R.string.swyp_name))
                            .setContentTitle(message.getData().get("title"))
                            .setContentText(message.getData().get("body"))
                            .setSmallIcon(R.drawable.app_icon)

//                .setDefaults(NotificationCompat.DEFAULT_VIBRATE)
//                .setDefaults(NotificationCompat.DEFAULT_SOUND)
                            .setAutoCancel(true)
                            .setSound(defaultSoundUri)
                            .setContentIntent(pendingIntent);

                    NotificationManager notificationManager =
                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    Notification notificationCompat = notificationBuilder.build();
                    notificationCompat.flags |= Notification.FLAG_AUTO_CANCEL;
                    notificationManager.notify(0 /* ID of notification */, notificationCompat);
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    notificationBuilder = new NotificationCompat.Builder(getApplicationContext())
                            .setContentTitle(message.getData().get("title"))
                            .setContentText(message.getData().get("body"))
                            .setSmallIcon(R.drawable.app_icon)

//                .setDefaults(NotificationCompat.DEFAULT_VIBRATE)
//                .setDefaults(NotificationCompat.DEFAULT_SOUND)
                            .setAutoCancel(true)
                            .setSound(defaultSoundUri)
                            .setContentIntent(pendingIntent)
                            .setLargeIcon(loadedImage)
                            .setStyle(new NotificationCompat.BigPictureStyle()
                                    .bigPicture(loadedImage));
                    NotificationManager notificationManager =
                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    Notification notificationCompat = notificationBuilder.build();
                    notificationCompat.flags |= Notification.FLAG_AUTO_CANCEL;
                    notificationManager.notify(0 /* ID of notification */, notificationCompat);

                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {

                }
            });


        } catch (Exception ex) {

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.ic_launcher)
                    .setContentTitle(message.getData().get("title"))
                    .setContentText(message.getData().get("body"))
                    .setSmallIcon(R.drawable.app_icon)

//                .setDefaults(NotificationCompat.DEFAULT_VIBRATE)
//                .setDefaults(NotificationCompat.DEFAULT_SOUND)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(message.getNotification().getBody()));
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            Notification notificationCompat = notificationBuilder.build();
            notificationCompat.flags |= Notification.FLAG_AUTO_CANCEL;
            notificationManager.notify(0 /* ID of notification */, notificationCompat);
        }

    }

}
