package com.indy.helpers;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by emad on 8/3/16.
 */
public class DateHelpers {
    SimpleDateFormat simpleDateFormat;
    long elapsedYears;

    public DateHelpers() {
        simpleDateFormat =
                new SimpleDateFormat("dd/M/yyyy");
    }

    public long validateDates(String date_1, String date_2) {
        try {

            Date date1 = simpleDateFormat.parse(date_1);
            Date date2 = simpleDateFormat.parse(date_2);
            long different = date1.getTime() - date2.getTime();
            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;
            long daysInMilli = hoursInMilli * 24;
            long monthInMilli = daysInMilli * 30;
            long yearsInMilli = monthInMilli * 12;
            long elapsedDays = different / daysInMilli;

            elapsedYears = different / yearsInMilli;

            different = different % daysInMilli;

            long elapsedHours = different / hoursInMilli;
            different = different % hoursInMilli;

            long elapsedMinutes = different / minutesInMilli;
            different = different % minutesInMilli;

            long elapsedSeconds = different / secondsInMilli;

            Log.d(
                    "years%n",

                    elapsedYears + "");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return elapsedYears;
    }

    public String getCurrentDate() {
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("dd/M/yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public static Date getCurrentDateFor() {
        return Calendar.getInstance().getTime();
    }

    public static double getDiffYears(Date first, Date last) {
        Calendar a = getCalendar(first);
        Calendar b = getCalendar(last);
        double diff = b.get(Calendar.YEAR) - a.get(Calendar.YEAR);
        if (diff == 29) {
            return diff;
        }
        if (a.get(Calendar.MONTH) > b.get(Calendar.MONTH) ||
                (a.get(Calendar.MONTH) == b.get(Calendar.MONTH) && a.get(Calendar.DATE) > b.get(Calendar.DATE))) {
            diff--;
        } else {
            diff++;
        }
        return diff;
    }

    public static Calendar getCalendar(Date date) {
        Calendar cal = Calendar.getInstance(Locale.US);
        cal.setTime(date);
        return cal;
    }

    // EEE MMM dd HH:mm:ss T yyyy
    // dd MMMM, HH:mm
    public static String getExpirtyDate(String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss 'GST' yyyy", Locale.US);
        try {
            Date dateValue = dateFormat.parse(date);
            dateFormat = new SimpleDateFormat("dd MMM, HH:mm", Locale.US);
            return dateFormat.format(dateValue);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String lastUpdated(String longValue) {
        try {
            long time = Long.parseLong(longValue);
            Date dateToParse = new Date();
            dateToParse.setTime(time);
            Date date2 = new Date();

            long secondsDifference = date2.getTime() - dateToParse.getTime();
            int finalValue = (int) secondsDifference / 1000;
            int minutes = finalValue / 60;
            int seconds = finalValue % 60;
            if (minutes == 0 && seconds == 0)
                return "0";
            else
                return minutes + ":" + seconds;
        } catch (Exception ex) {
            return "0:00";
        }

    }
}
