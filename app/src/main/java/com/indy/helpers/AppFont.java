package com.indy.helpers;

import android.content.Context;
import android.graphics.Typeface;

import com.indy.R;


/**
 * Created by emad on 9/5/16.
 */
public class AppFont {

    public static AppFont _appFont = null;

    private Typeface _boldTypeFace = null;
    private Typeface _lightTypeFace = null;
    private Typeface _mediumTypeFace = null;
    private Typeface _open_sans_TypeFace = null;
    private Typeface _open_sans_TypeFace_light = null;
    private Typeface _aller_regular = null;
    private Typeface _open_sans_regular = null;
    private AppFont(Context context) {
        loadFont(context);
    }

    public static AppFont getInstance(Context context) {
        if (_appFont == null) {
            _appFont = new AppFont(context);
        }
        return _appFont;
    }

    public Typeface getTypeFace() {
        return _lightTypeFace;
    }

    public static AppFont get_appFont() {
        return _appFont;
    }

    public Typeface get_boldTypeFace() {
        return _boldTypeFace;
    }

    public Typeface get_lightTypeFace() {
        return _lightTypeFace;
    }

    public Typeface get_mediumTypeFace() {
        return _mediumTypeFace;
    }

    public Typeface get_open_sans_TypeFace() {
        return _open_sans_TypeFace;
    }

    public Typeface get_open_sans_Typeface_Light(){
        return _open_sans_TypeFace_light;
    }

    public Typeface get_aller_regular(){
        return _aller_regular;
    }

    public Typeface get_open_sans_regular(){
        return _open_sans_regular;
    }
    public void loadFont(Context context) {
        _lightTypeFace = Typeface.createFromAsset(context.getAssets(), String.format("fonts/%s", context.getString(R.string.open_sans_TypeFace_light)));
        _mediumTypeFace = Typeface.createFromAsset(context.getAssets(), String.format("fonts/%s", context.getString(R.string.font_light)));
        _boldTypeFace = Typeface.createFromAsset(context.getAssets(), String.format("fonts/%s", context.getString(R.string.font_bold)));
        _open_sans_TypeFace = Typeface.createFromAsset(context.getAssets(), String.format("fonts/%s", context.getString(R.string.open_sans_TypeFace)));
        _open_sans_TypeFace_light = Typeface.createFromAsset(context.getAssets(), String.format("fonts/%s", context.getString(R.string.open_sans_TypeFace_light)));
        _aller_regular = Typeface.createFromAsset(context.getAssets(),String.format("fonts/%s",context.getString(R.string.aller_regular)));
        _open_sans_regular = Typeface.createFromAsset(context.getAssets(),String.format("fonts/%s",context.getString(R.string.open_sans_regular)));
    }
}

