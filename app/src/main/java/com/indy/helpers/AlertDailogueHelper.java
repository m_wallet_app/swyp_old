package com.indy.helpers;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.controls.DailogeInterface;

/**
 * Created by emad on 8/17/16.
 */
public class AlertDailogueHelper {

    static DailogeInterface _dailogeInterface;
    static Dialog dialog;

    public static void showDaiogue(final Activity mContext, DailogeInterface dailogeInterface, String title, String msg, String ok, String cancel, final String alertType) {
        _dailogeInterface = dailogeInterface;
        dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_layout);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        //region divider
        int divierId = dialog.getContext().getResources()
                .getIdentifier("android:id/titleDivider", null, null);
        View divider = dialog.findViewById(divierId);
        if (divider != null) {
            divider.setBackgroundColor((Color.TRANSPARENT));
        }
        //endregion

        TextView okTxt = (TextView) dialog.findViewById(R.id.okBtn);
        TextView cancelTxt = (TextView) dialog.findViewById(R.id.cancelBtn);
        TextView titleTxt = (TextView) dialog.findViewById(R.id.title);
        TextView descTxt = (TextView) dialog.findViewById(R.id.mesg);

        titleTxt.setText(title);
        descTxt.setText(msg);
        okTxt.setText(ok);
        cancelTxt.setText(cancel);
        okTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                _dailogeInterface.onAgreeListener(alertType);
            }
        });
        cancelTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                _dailogeInterface.onCancelListener(alertType);
            }
        });
        //  doAnimation();
        if (dialog != null) {
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            if (cancel.length() == 0) {
            }
            dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK &&
                            event.getAction() == KeyEvent.ACTION_UP &&
                            !event.isCanceled()) {
                        dialog.cancel();
                        mContext.onBackPressed();
                        return true;
                    }
                    return false;
                }
            });

        }
        dialog.show();

    }

    //................................
//    public static void showErrorMsg(final Activity mContext, DailogeInterface dailogeInterface, String title, String ok, final String alertType) {
//        _dailogeInterface = dailogeInterface;
//        dialog = new Dialog(mContext);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.error_layout);
//        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//
//        //region divider
//        int divierId = dialog.getContext().getResources()
//                .getIdentifier("android:id/titleDivider", null, null);
//        View divider = dialog.findViewById(divierId);
//        if (divider != null) {
//            divider.setBackgroundColor((Color.TRANSPARENT));
//        }
//        //endregion
//
//        TextView okTxt = (TextView) dialog.findViewById(R.id.okTxt);
//        TextView titleTxt = (TextView) dialog.findViewById(R.id.errortxt);
//        titleTxt.setText(title);
//        okTxt.setText(ok);
//        okTxt.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//                _dailogeInterface.onAgreeListener(alertType);
//            }
//        });
//        //  doAnimation();
//        if (dialog != null) {
//            dialog.setCanceledOnTouchOutside(false);
//            dialog.setCancelable(false);
//            dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
//                @Override
//                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
//                    if (keyCode == KeyEvent.KEYCODE_BACK &&
//                            event.getAction() == KeyEvent.ACTION_UP &&
//                            !event.isCanceled()) {
//                        dialog.cancel();
//                        mContext.onBackPressed();
//                        return true;
//                    }
//                    return false;
//                }
//            });
//
//        }
//        dialog.show();
//
//    }
}
