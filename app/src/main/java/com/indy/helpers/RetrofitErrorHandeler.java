package com.indy.helpers;

import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.indy.utils.CommonMethods;
import com.indy.views.activites.ConnectionErrorActivity;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;


/**
 * Created by emad on 9/20/16.
 */

public class RetrofitErrorHandeler {
    public static RetrofitErrorHandeler retrofitErrorHandeler;
    Context mContext;

    public void handleError(Throwable throwable, Context context) {
        this.mContext = context;

        if (throwable == null) {
            onConnectionError(0);
            return;
        } else if (throwable instanceof SocketTimeoutException) {
            onConnectionError(0);
        } else if (throwable instanceof NetworkErrorException ||
                throwable instanceof ConnectException ||
                throwable instanceof UnknownHostException) {
            onConnectionError(1);
        }
//        FirebaseCrash.report(throwable);

//        if (throwable instanceof NetworkErrorException
//                || throwable instanceof SocketTimeoutException
//                || throwable instanceof ConnectException
//                || throwable instanceof UnknownHostException
//               ) {
//            onConnectionError(0);
//        }else if(throwable ==null){
//            onConnectionError(1);
//        }
    }

    private void onConnectionError(int mode) {
//        new SweetAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE)
//                .setTitleText(mContext.getString(R.string.connection_error))
//                .setContentText("")
//                .show();
        try {
            if(mContext==null)
                return;
            Intent intent = new Intent(mContext, ConnectionErrorActivity.class);
            Bundle bundle = new Bundle();
            intent.putExtra("isNetworkError",mode);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            mContext.startActivity(intent);
        } catch (Exception ex) {
            CommonMethods.logException(ex);

        }
    }

    public RetrofitErrorHandeler() {
        retrofitErrorHandeler = this;
    }


}

