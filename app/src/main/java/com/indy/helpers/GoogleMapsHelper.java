package com.indy.helpers;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.net.Uri;
import android.support.annotation.DrawableRes;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.indy.R;
import com.indy.utils.CommonMethods;
import com.indy.views.activites.NearestStoreActivity;

/**
 * Created by emad on 8/18/16.
 */
public class GoogleMapsHelper {
    private Context mContext;
    private NearestStoreActivity mapsActivity;
    //    Bitmap etisaltLogo;
    int height = 150;
    int width = 150;
    String distanceDouble;

    public GoogleMapsHelper(Context context) {
        this.mContext = context;

        mapsActivity = NearestStoreActivity.instance;

//        etisaltLogo = loadBitmapFromView(R.drawable.logo_mobile);
    }

    public void onGoogleMpasOpen(double latitude, double longitude, String address) {
//        String label = "";
        try {
            String uriBegin = "geo:" + latitude + "," + longitude;
            String query = latitude + "," + longitude + "(" + address + ")";
            String encodedQuery = Uri.encode(query);
            String uriString = uriBegin + "?q=" + encodedQuery + "&z=16";
            Log.v("uri ", uriString);
            Uri uri = Uri.parse(uriString);
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(intent);
        }catch (ActivityNotFoundException ex){
            //showToast("Please install the latest version of google maps. If you have already installed it, please make sure it is not disabled it from the applications menu.!");
        } catch (Exception ex){
            CommonMethods.logException(ex);
        }
    }

    public void drawaMapPins() {
        mapsActivity.mMap.clear();
        LatLng fLatLng = null;
        String distance = null;
        Marker tempMarker = null;
        Marker temp = null;
        for (int i = 0; i < mapsActivity.mLocationModelResponse.getNearestLocationList().size(); i++) {
            fLatLng = new LatLng(mapsActivity.mLocationModelResponse.getNearestLocationList().get(i).getGeoLocation().getLatitude(),
                    mapsActivity.mLocationModelResponse.getNearestLocationList().get(i).getGeoLocation().getLongitude());
            LatLng mLatLng = new LatLng(mapsActivity.mLocationModelResponse.getNearestLocationList().get(i).getGeoLocation().getLatitude(),
                    mapsActivity.mLocationModelResponse.getNearestLocationList().get(i).getGeoLocation().getLongitude());
            mapsActivity.marker = new MarkerOptions().position(mLatLng);
//            mapsActivity.marker.icon(BitmapDescriptorFactory.fromBitmap(etisaltLogo));
            if (i == 0)
                temp = mapsActivity.mMap.addMarker(
                        mapsActivity.marker.position(mLatLng)
                                .title(mapsActivity.mLocationModelResponse.getNearestLocationList().get(i).getAddressEn())
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.location_icon_fill)));
            else
                temp = mapsActivity.mMap.addMarker(
                        mapsActivity.marker.position(mLatLng)
                                .title(mapsActivity.mLocationModelResponse.getNearestLocationList().get(i).getAddressEn())
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.location_orange)));

//            if(i==0){
//                temp.showInfoWindow();
//            }
//            if(i==0) {
//                tempMarker = temp;
//            }
            NearestStoreActivity.mMarkerList.put(temp.getId(), i);
        }
        fLatLng = new LatLng(mapsActivity.mLocationModelResponse.getNearestLocationList().get(0).getGeoLocation().getLatitude(),
                mapsActivity.mLocationModelResponse.getNearestLocationList().get(0).getGeoLocation().getLongitude());

        distance = mapsActivity.mLocationModelResponse.getNearestLocationList().get(0).getDistance().toString();
        if (distance != null) {
            distanceDouble = String.format("%.2f", Double.valueOf(distance));
        }
        mapsActivity.locationName = mapsActivity.mLocationModelResponse.getNearestLocationList().get(0).getAddressEn();
        mapsActivity.distanceId.setText(mapsActivity.getString(R.string.nearest_shop) + " " + distanceDouble + "  " + mapsActivity.getString(R.string.away));
        mapsActivity.mLatitude = mapsActivity.mLocationModelResponse.getNearestLocationList().get(0).getGeoLocation().getLatitude();
        mapsActivity.mLongitude = mapsActivity.mLocationModelResponse.getNearestLocationList().get(0).getGeoLocation().getLongitude();
        mapsActivity.locationNameId.setText(mapsActivity.locationName);
        mapsActivity.mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(fLatLng,
                13));
        mapsActivity.mMap.setOnMarkerClickListener(mapsActivity);
        mapsActivity.mMap.setOnInfoWindowClickListener(mapsActivity);
        if (tempMarker != null) {
            tempMarker.showInfoWindow();
        }
    }

    public Bitmap loadBitmapFromView(int resource) {
        BitmapDrawable bitmapdrawble = (BitmapDrawable) mapsActivity.getResources().getDrawable(resource);
        Bitmap b = bitmapdrawble.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
        return smallMarker;
    }

    public static Bitmap getBitmapFromDrawable(Context context, @DrawableRes int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);

        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        } else if (drawable instanceof VectorDrawable || drawable instanceof VectorDrawableCompat) {
            Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);

            return bitmap;
        } else {
            throw new IllegalArgumentException("unsupported drawable type");
        }
    }
}
