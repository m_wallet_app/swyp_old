package com.indy.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.indy.R;
import com.indy.helpers.Fonts;


/**
 * Created by Amir Jehangir on 9/5/16.
 */
public class CustomAnimatedTextView extends AppCompatTextView {
    int textStyle = 0;

    public CustomAnimatedTextView(Context context) {
        super(context);
    }

    public CustomAnimatedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CustomAnimatedTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        Typeface face = Fonts.getLightFont(getContext());
        setTypeface(face);
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.FontStyle);
        textStyle = typedArray.getInt(R.styleable.FontStyle_fontType, 0);
        if (textStyle == 0) {//Light
            setTypeface(Fonts.getLightFont(getContext()));
        } else if (textStyle == 1) {//medium
            setTypeface(Fonts.getMediumFont(getContext()));
        } else if (textStyle == 2) {//bold
            setTypeface(Fonts.getBoldFont(getContext()));
//            setText(CharacterUtils.getArabicNumbers((getText().toString())));
        } else if (textStyle == 3) {//opensans semi bold
            setTypeface(Fonts.getOpenSansSemiBold(getContext()));
//            setText(CharacterUtils.getArabicNumbers((getText().toString())));

        } else if (textStyle == 4) {//opensans semi bold
            setTypeface(Fonts.getOpenSansLight(getContext()));
//            setText(CharacterUtils.getArabicNumbers((getText().toString())));

        } else {
            setTypeface(Fonts.getLightFont(getContext()));
        }
        startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.custom_animation_label));
    }

}
