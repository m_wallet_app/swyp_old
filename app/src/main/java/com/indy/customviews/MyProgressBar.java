package com.indy.customviews;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RadialGradient;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.ProgressBar;

import com.indy.R;

import java.util.Arrays;

//public class MyProgressBar extends ProgressBar {
//
//    Paint p = new Paint();
//    int progress;
//    String dataLimitText = "";
//
//    public String getDataLimitText() {
//        return dataLimitText;
//    }
//
//    public void setDataLimitText(String dataLimitText) {
//        this.dataLimitText = dataLimitText;
//    }
//
//    public MyProgressBar(Context context) {
//        super(context);
//        p.setColor(Color.GREEN);
//    }
//
//    // Resizing Behavior
//    public enum ResizingBehavior {
//        AspectFit, //!< The content is proportionally resized to fit into the target rectangle.
//        AspectFill, //!< The content is proportionally resized to completely fill the target rectangle.
//        Stretch, //!< The content is stretched to match the entire target rectangle.
//        Center, //!< The content is centered in the target rectangle, but it is NOT resized.
//    }
//
//    public MyProgressBar(Context context, AttributeSet attrs) {
//        super(context, attrs);
//        this.p = p;
//        this.progress = progress;
//    }
//
//    public MyProgressBar(Context context, AttributeSet attrs, int progress) {
//        super(context, attrs);
//        this.p = p;
//        this.progress = progress;
//    }
//
//    @Override
//    protected synchronized void onDraw(Canvas canvas) {
////        canvas.drawLine(0, 0, getWidth(), 0, p);
////        canvas.drawLine(0, 0, 0, getHeight(), p);
////        canvas.drawArc(new RectF(0, 0, getWidth(),  getHeight()), 40,40, true, p);
//        /**
//         * Whatever drawing is needed for your Layout
//         * But make sure you use relative lenghts and heights
//         * So you can reuse your Widget.
//         * Also use the Progress Variable to draw the filled part
//         * corresponding to getMax()
//         */
//
//
//        Paint paint = CacheForCanvas1.paint;
//
//        if (CacheForCanvas1.gradient == null)
//            CacheForCanvas1.gradient = new PaintCodeGradient(new int[]{getContext().getResources().getColor(R.color.gradient_start),
//                    PaintCodeColor.colorByBlendingColors(getContext().getResources().getColor(R.color.gradient_start),
//                            0.5f, getContext().getResources().getColor(R.color.gradient_end))
//                    , getContext().getResources().getColor(R.color.gradient_end),Color.LTGRAY}, new float[]{0f, 0.2f, 0.999f,1.1f});
//        PaintCodeGradient gradient = CacheForCanvas1.gradient;
//        // Resize to Target Frame
////        canvas.save();
////        RectF resizedFrame = CacheForCanvas1.resizedFrame;
//////        MyProgressBar.resizingBehaviorApply(resizing, CacheForCanvas1.originalFrame, targetFrame, resizedFrame);
////        canvas.translate(resizedFrame.left, resizedFrame.top);
////        canvas.scale(resizedFrame.width() / 466f, resizedFrame.height() / 156f);
//
//        float width = 0, height = 0;
////        width = getWidth();
////        height = getHeight();
//
//        width =getWidth();
//
//        height = width/3.661290322580645f;
////        151.31277533f;
//
//
//        RectF bezierRect = CacheForCanvas1.bezierRect;
//
//        bezierRect.set(150f, 0f, width-150, 180);
//        Path bezierPath = CacheForCanvas1.bezierPath;
//        bezierPath.reset();
//        bezierPath.moveTo(+0.01f, 0.4705882352941176f * height);
//        bezierPath.cubicTo(0.0024669603524229f * width, 0.1333870967741935f * height, 0.0817621145374449f * width, 0.1232258064516129f * height,
//                0.0817621145374449f * width, 0.1232258064516129f * height);
//        bezierPath.cubicTo(0.0817621145374449f * width, 0.1232258064516129f * height, 0.884625550660793f * width, -0.0182258064516129f*height,
//                0.9351101321585903f * width, 0.0019354838709677f * height);
//        bezierPath.cubicTo(0.9855947136563877f * width, 0.022258064516129f * height, 1f * width, 0.2748387096774194f * height,
//                1f * width, 0.2748387096774194f * height);
//        bezierPath.lineTo(1f * width, 0.6993548387096774f * height);
//        bezierPath.cubicTo(1f * width, 0.6993548387096774f * height, 0.9903964757709251f * width, 0.8812903225806452f * height,
//                0.9351101321585903f * width, 0.972258064516129f * height);
//
//        bezierPath.cubicTo(0.8798237885462555f * width, 1.063225806451613f * height, 0.0817621145374449f * width, 0.9014516129032258f * height,
//                0.0817621145374449f * width, 0.9014516129032258f * height);
//        bezierPath.cubicTo(0.0817621145374449f * width, 0.9014516129032258f * height, -0.0023348017621145f * width, 0.9014516129032258f * height,
//                000.00004405286343612335f * width, 0.5174193548387097f * height);
//        bezierPath.close();
//
//        paint.reset();
//        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
//        paint.setStyle(Paint.Style.FILL);
////        paint.setShader(CacheForCanvas1.bezierPathGradient.get(gradient, 0f, 0f, width, height/2f));
//        canvas.drawPath(bezierPath, paint);
//        bezierPath.close();
//
//        paint.reset();
//        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
//        paint.setStyle(Paint.Style.FILL);
//        paint.setColor(Color.LTGRAY);
//        canvas.drawPath(bezierPath, paint);
//
////        canvas.restore();
//
//        paint.reset();
//        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
//        paint.setStyle(Paint.Style.FILL);
//        paint.setColor(Color.WHITE);
//        canvas.drawPath(bezierPath, paint);
//
//        paint.reset();
//        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
//        paint.setStrokeWidth(1f);
//        paint.setStrokeMiter(10f);
//        canvas.save();
//        paint.setStyle(Paint.Style.STROKE);
//        paint.setColor(Color.BLACK);
//        canvas.drawPath(bezierPath, paint);
//
//        double totalWidth = 247 - 19.99;
//        double desiredPoint = (progress / 100f) * width;
////        desiredPoint += 20;
//        RectF bezierRect2 = CacheForCanvas1.bezierRect;
//        bezierRect.set(150f, 0f, width-150, 180);
//        Path bezierPath2 = CacheForCanvas1.bezierPath;
//        bezierPath2.reset();
//        bezierPath2.moveTo(0.01f, 0.4705882352941176f * height);
//        bezierPath2.cubicTo(0.0024669603524229f * width, 0.1333870967741935f * height, 0.0817621145374449f * width, 0.1232258064516129f * height,
//                0.0817621145374449f * width, 0.1232258064516129f * height);
//        bezierPath2.cubicTo(0.0817621145374449f * width, 0.1232258064516129f * height, 0.884625550660793f * width, -0.0182258064516129f*height,
//                0.9351101321585903f * width, 0.0019354838709677f * height);
//        bezierPath2.cubicTo(0.9855947136563877f * width, 0.022258064516129f * height, 1f * width, 0.2748387096774194f * height,
//                1f * width, 0.2748387096774194f * height);
//        bezierPath2.lineTo(1f * width, 0.6993548387096774f * height);
//        bezierPath2.cubicTo(1f * width, 0.6993548387096774f * height, 0.9903964757709251f * width, 0.8812903225806452f * height,
//                0.9351101321585903f * width, 0.972258064516129f * height);
//
//        bezierPath2.cubicTo(0.8798237885462555f * width, 1.063225806451613f * height, 0.0817621145374449f * width, 0.9014516129032258f * height,
//                0.0817621145374449f * width, 0.9014516129032258f * height);
//        bezierPath2.cubicTo(0.0817621145374449f * width, 0.9014516129032258f * height, -0.0023348017621145f * width, 0.9014516129032258f * height,
//                000.00004405286343612335f * width, 0.5174193548387097f * height);
//        bezierPath2.close();
//        paint.reset();
//        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
//        paint.setStyle(Paint.Style.FILL);
//
//        if(progress!=0) {
//            paint.setShader(CacheForCanvas1.bezierPathGradient.get(gradient, 0f, height / 2, (float) desiredPoint, height / 2));
//            canvas.drawPath(bezierPath2, paint);
//        }
//        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),"fonts/OpenSans-Semibold.ttf");
//        if(progress!=100) {
//            p.setColor(getContext().getResources().getColor(R.color.line_color));
//
//            canvas.drawLine((float) desiredPoint, -40, (float) desiredPoint, height + 20, p);
//            Paint paintText = new Paint();
//
//            paintText.setColor(getContext().getResources().getColor(R.color.text_color));
//
//            paintText.setTextSize(25);
//            paintText.setTypeface(tf);
//            paintText.setFlags(Paint.ANTI_ALIAS_FLAG);
//
//            if(progress<10) {
//                canvas.drawText(dataLimitText, (float) desiredPoint, height + 50, paintText);
//            }else{
//                if(progress>90){
//                    canvas.drawText(dataLimitText, (float) desiredPoint -250, height + 50, paintText);
//
//                }else if(progress>80){
//                    canvas.drawText(dataLimitText, (float) desiredPoint -180, height + 50, paintText);
//                }else{
//                    canvas.drawText(dataLimitText, (float) desiredPoint -30, height + 50, paintText);
//
//                }
//
//            }
//        }else{
//            Paint paintText = new Paint();
//            paintText.setColor(Color.WHITE);
//            paintText.setTextSize(30);
//            paintText.setTypeface(tf);
//            paintText.setFlags(Paint.ANTI_ALIAS_FLAG);
//            canvas.drawText(dataLimitText, 25, height/1.8f, paintText);
//        }
////        Paint paintText = new Paint();
////        paint.setColor(Color.WHITE);
////        paint.setStyle(Paint.Style.FILL);
////        canvas.drawPaint(paint);
////
//
////        canvas.restore();
//
//
////        RectF bezierRect2 = new RectF();
//
////        paint.reset();
////        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
////        paint.setStyle(Paint.Style.FILL);
////        paint.setColor(Color.RED);
////
////        double percentageValue = 435.5 * (progress/100);
////        bezierRect2.set(20.42f, 20.5f, (float)percentageValue, 121.5f);
////        canvas.drawRect(bezierRect2,paint);
////        canvas.restore();
////
////        canvas.restore();
//
//
//    }
//
//    static class PaintCodeColor extends Color {
//        private static float[] ColorToHSV(int originalColor) {
//            float hsv[] = new float[3];
//            RGBToHSV(red(originalColor), green(originalColor), blue(originalColor), hsv);
//            return hsv;
//        }
//
//        public static int colorByChangingHue(int originalColor, float newHue) {
//            float hsv[] = ColorToHSV(originalColor);
//            hsv[0] = newHue;
//            return HSVToColor(alpha(originalColor), hsv);
//        }
//
//        public static int colorByChangingSaturation(int originalColor, float newSaturation) {
//            float hsv[] = ColorToHSV(originalColor);
//            hsv[1] = newSaturation;
//            return HSVToColor(alpha(originalColor), hsv);
//        }
//
//        public static int colorByChangingValue(int originalColor, float newValue) {
//            float hsv[] = ColorToHSV(originalColor);
//            hsv[2] = newValue;
//            return HSVToColor(alpha(originalColor), hsv);
//        }
//
//        public static float hue(int color) {
//            return ColorToHSV(color)[0];
//        }
//
//        public static float saturation(int color) {
//            return ColorToHSV(color)[1];
//        }
//
//        public static float brightness(int color) {
//            return ColorToHSV(color)[2];
//        }
//
//        public static int colorByChangingAlpha(int color, int newAlpha) {
//            return argb(newAlpha, red(color), green(color), blue(color));
//        }
//
//        public static int colorByBlendingColors(int c1, float ratio, int c2) {
//            return argb((int) ((1f - ratio) * alpha(c1) + ratio * alpha(c2)),
//                    (int) ((1f - ratio) * red(c1) + ratio * red(c2)),
//                    (int) ((1f - ratio) * green(c1) + ratio * green(c2)),
//                    (int) ((1f - ratio) * blue(c1) + ratio * blue(c2)));
//        }
//
//        public static int colorByApplyingHighlight(int color, float ratio) {
//            return colorByBlendingColors(color, ratio, colorByChangingAlpha(WHITE, alpha(color)));
//        }
//
//        public static int colorByApplyingShadow(int color, float ratio) {
//            return colorByBlendingColors(color, ratio, colorByChangingAlpha(BLACK, alpha(color)));
//        }
//    }
//
//    @Override
//    public int getProgress() {
//        return progress;
//    }
//
//    protected void onValueChanged() {
//        /**
//         * Update your progress Variable and draw again
//         */
//        progress = getProgress();
//        this.invalidate();
//    }
//    protected void onValueChanged(int progressNew) {
//        /**
//         * Update your progress Variable and draw again
//         */
//        progress = getProgress();
//        this.invalidate();
//    }
//    @Override
//    public synchronized void setMax(int max) {
//        onValueChanged();
//        super.setMax(max);
//    }
//
//    @Override
//    public synchronized void setProgress(int progress) {
//        this.progress = progress;
//        super.setProgress(progress);
//        onValueChanged();
//
//    }
//
//    @Override
//    public synchronized void setSecondaryProgress(int secondaryProgress) {
//        onValueChanged();
//        super.setSecondaryProgress(secondaryProgress);
//    }
//
//    private static class CacheForCanvas1 {
//        private static Paint paint = new Paint();
//        private static PaintCodeGradient gradient = null;
//
//        private static RectF originalFrame = new RectF(0f, 0f, 466f, 156f);
//        private static RectF resizedFrame = new RectF();
//        private static RectF bezierRect = new RectF();
//        private static Path bezierPath = new Path();
//        private static PaintCodeLinearGradient bezierPathGradient = new PaintCodeLinearGradient();
//    }
//
//    public static class PaintCodeLinearGradient {
//        private LinearGradient shader;
//        private PaintCodeGradient paintCodeGradient;
//        private float x0, y0, x1, y1;
//
//        LinearGradient get(PaintCodeGradient paintCodeGradient, float x0, float y0, float x1, float y1) {
//            if (this.shader == null || this.x0 != x0 || this.y0 != y0 || this.x1 != x1 || this.y1 != y1 || !this.paintCodeGradient.equals(paintCodeGradient)) {
//                this.x0 = x0;
//                this.y0 = y0;
//                this.x1 = x1;
//                this.y1 = y1;
//                this.paintCodeGradient = paintCodeGradient;
//                this.shader = paintCodeGradient.linearGradient(x0, y0, x1, y1);
//            }
//            return this.shader;
//        }
//    }
//
//
//    // Resizing Behavior
//    public static void resizingBehaviorApply(ResizingBehavior behavior, RectF rect, RectF target, RectF result) {
//        if (rect.equals(target) || target == null) {
//            result.set(rect);
//            return;
//        }
//
//        if (behavior == ResizingBehavior.Stretch) {
//            result.set(target);
//            return;
//        }
//
//        float xRatio = Math.abs(target.width() / rect.width());
//        float yRatio = Math.abs(target.height() / rect.height());
//        float scale = 0f;
//
//        switch (behavior) {
//            case AspectFit: {
//                scale = Math.min(xRatio, yRatio);
//                break;
//            }
//            case AspectFill: {
//                scale = Math.max(xRatio, yRatio);
//                break;
//            }
//            case Center: {
//                scale = 1f;
//                break;
//            }
//        }
//
//        float newWidth = Math.abs(rect.width() * scale);
//        float newHeight = Math.abs(rect.height() * scale);
//        result.set(target.centerX() - newWidth / 2,
//                target.centerY() - newHeight / 2,
//                target.centerX() + newWidth / 2,
//                target.centerY() + newHeight / 2);
//    }
//
//    class PaintCodeGradient {
//        private int[] colors;
//        private float[] positions;
//
//        public PaintCodeGradient(int[] colors, float[] positions) {
//            if (positions == null) {
//                int steps = colors.length;
//                positions = new float[steps];
//                for (int i = 0; i < steps; i++)
//                    positions[i] = (float) i / (steps - 1);
//            }
//
//            this.colors = colors;
//            this.positions = positions;
//        }
//
//        public LinearGradient linearGradient(float x0, float y0, float x1, float y1) {
//            return new LinearGradient(x0, y0, x1, y1, this.colors, this.positions, Shader.TileMode.CLAMP);
//        }
//
//        public RadialGradient radialGradient(float startX, float startY, float startRadius, float endX, float endY, float endRadius) {
//            int steps = this.colors.length;
//            float[] positions = new float[steps];
//
//            if (startRadius > endRadius) {
//                float ratio = endRadius / startRadius;
//                int[] colors = new int[steps];
//
//                for (int i = 0; i < steps; i++) {
//                    colors[i] = this.colors[steps - i - 1];
//                    positions[i] = (1 - this.positions[steps - i - 1]) * (1 - ratio) + ratio;
//                }
//
//                return new RadialGradient(endX, endY, startRadius, colors, positions, Shader.TileMode.CLAMP);
//            } else {
//                float ratio = startRadius / endRadius;
//
//                for (int i = 0; i < steps; i++) {
//                    positions[i] = this.positions[i] * (1 - ratio) + ratio;
//                }
//
//                return new RadialGradient(startX, startY, endRadius, this.colors, positions, Shader.TileMode.CLAMP);
//            }
//        }
//
//        @Override
//        public boolean equals(Object obj) {
//            if (!(obj instanceof PaintCodeGradient))
//                return false;
//            PaintCodeGradient other = (PaintCodeGradient) obj;
//            return Arrays.equals(this.colors, other.colors) && Arrays.equals(this.positions, other.positions);
//        }
//    }
//}

public class MyProgressBar extends ProgressBar {

    Paint p = new Paint();
    int progress;
    double throttleProgress;
    String dataLimitText = "";
    String throttleLimitText = "";
    String fullSpeedText = "";
    String nonStopText = "";
    boolean isNonStop = false;

    public String getDataLimitText() {
        return dataLimitText;
    }

    public void setDataLimitText(String dataLimitText) {
        this.dataLimitText = dataLimitText;
    }

    public void setThrottleLimitText(String throttleLimitText) {
        this.throttleLimitText = throttleLimitText;
    }

    public void setFullSpeedText(String fullSpeedText) {
        this.fullSpeedText = fullSpeedText;
    }

    public void setNonStopText(String nonStopText) {
        this.nonStopText = nonStopText;
    }

    public void setNonStop(boolean isNonStop) {
        this.isNonStop = isNonStop;
    }

    public void setThrottleProgress(String throttleProgress) {
        this.throttleProgress = Double.parseDouble(throttleProgress) * 100;
    }


    public MyProgressBar(Context context) {
        super(context);
        p.setColor(Color.GREEN);
    }

    // Resizing Behavior
    public enum ResizingBehavior {
        AspectFit, //!< The content is proportionally resized to fit into the target rectangle.
        AspectFill, //!< The content is proportionally resized to completely fill the target rectangle.
        Stretch, //!< The content is stretched to match the entire target rectangle.
        Center, //!< The content is centered in the target rectangle, but it is NOT resized.
    }

    public MyProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.p = p;
        this.progress = progress;
    }

    public MyProgressBar(Context context, AttributeSet attrs, int progress) {
        super(context, attrs);
        this.p = p;
        this.progress = progress;
    }

    @Override
    protected synchronized void onDraw(Canvas canvas) {
//        canvas.drawLine(0, 0, getWidth(), 0, p);
//        canvas.drawLine(0, 0, 0, getHeight(), p);
//        canvas.drawArc(new RectF(0, 0, getWidth(),  getHeight()), 40,40, true, p);
        /**
         * Whatever drawing is needed for your Layout
         * But make sure you use relative lenghts and heights
         * So you can reuse your Widget.
         * Also use the Progress Variable to draw the filled part
         * corresponding to getMax()
         */


        Paint paint = CacheForCanvas1.paint;

        if (CacheForCanvas1.gradient == null)
            CacheForCanvas1.gradient = new PaintCodeGradient(new int[]{getContext().getResources().getColor(R.color.gradient_start),
                    PaintCodeColor.colorByBlendingColors(getContext().getResources().getColor(R.color.gradient_start),
                            0.5f, getContext().getResources().getColor(R.color.gradient_end))
                    , getContext().getResources().getColor(R.color.gradient_end), Color.LTGRAY}, new float[]{0f, 0.2f, 0.999f, 1.1f});
        PaintCodeGradient gradient = CacheForCanvas1.gradient;


        float width = 0, height = 0;

        width = getWidth();

        height = width / 3.661290322580645f;

//        height = width / 5f;

        double totalWidth = 247 - 19.99;

        double desiredPoint = 0;
        if (isNonStop)
            desiredPoint = (throttleProgress / 100f) * width;
        else
            desiredPoint = (progress / 100f) * width;

        double fullProgress = ((width - desiredPoint) / 100f) * progress;
        fullProgress += desiredPoint;

        if(fullProgress == desiredPoint)
            fullProgress += 0.1;

//        desiredPoint += 20;


        RectF bezierRect2 = CacheForCanvas1.bezierRect;
        bezierRect2.set(0f, 0f, width, height + 100);
        Path bezierPath2 = CacheForCanvas1.bezierPath;
        bezierPath2.reset();
        bezierPath2.moveTo(0.01f, 0.4705882352941176f * height);
        bezierPath2.cubicTo(0.0024669603524229f * width, 0.1333870967741935f * height, 0.0817621145374449f * width, 0.1232258064516129f * height,
                0.0817621145374449f * width, 0.1232258064516129f * height);
        bezierPath2.cubicTo(0.0817621145374449f * width, 0.1232258064516129f * height, 0.884625550660793f * width, -0.0182258064516129f * height,
                0.9351101321585903f * width, 0.0019354838709677f * height);
        bezierPath2.cubicTo(0.9855947136563877f * width, 0.022258064516129f * height, 1f * width, 0.2748387096774194f * height,
                1f * width, 0.2748387096774194f * height);
        bezierPath2.lineTo(1f * width, 0.6993548387096774f * height);
        bezierPath2.cubicTo(1f * width, 0.6993548387096774f * height, 0.9903964757709251f * width, 0.8812903225806452f * height,
                0.9351101321585903f * width, 0.972258064516129f * height);

        bezierPath2.cubicTo(0.8798237885462555f * width, 1.063225806451613f * height, 0.0817621145374449f * width, 0.9014516129032258f * height,
                0.0817621145374449f * width, 0.9014516129032258f * height);
        bezierPath2.cubicTo(0.0817621145374449f * width, 0.9014516129032258f * height, -0.0023348017621145f * width, 0.9014516129032258f * height,
                000.00004405286343612335f * width, 0.5174193548387097f * height);
        bezierPath2.close();

        paint.reset();
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.LTGRAY);
        canvas.drawPath(bezierPath2, paint);

        paint.reset();
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.FILL);


        if (progress != 0) {
            if (!isNonStop) {
                paint.setShader(CacheForCanvas1.bezierPathGradient.get(gradient, 0, height / 2, (float) desiredPoint, height / 2));
                canvas.drawPath(bezierPath2, paint);
            } else {
                paint.reset();
                paint.setFlags(Paint.ANTI_ALIAS_FLAG);
                paint.setStyle(Paint.Style.FILL);
                paint.setShader(CacheForCanvas1.bezierPathGradient.get(gradient, (float) desiredPoint, height / 2, (float) fullProgress, height / 2));
                canvas.drawPath(bezierPath2, paint);
            }
        }

        else{
            if (isNonStop) {
                paint.reset();
                paint.setFlags(Paint.ANTI_ALIAS_FLAG);
                paint.setStyle(Paint.Style.FILL);
                paint.setShader(CacheForCanvas1.bezierPathGradient.get(gradient, (float) desiredPoint, height / 2, (float) fullProgress, height / 2));
                canvas.drawPath(bezierPath2, paint);
            }
        }

        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/OpenSans-Semibold.ttf");

        if (!isNonStop) {

            if (progress != 100) {

                p.setColor(getResources().getColor(R.color.line_color));
                p.setStrokeWidth(3);
                canvas.drawLine((float) desiredPoint, -40, (float) desiredPoint, height + 20, p);
                Paint paintText = new Paint();

                paintText.setColor(getContext().getResources().getColor(R.color.text_color));

                paintText.setTextSize(25);
                paintText.setTypeface(tf);
                paintText.setFlags(Paint.ANTI_ALIAS_FLAG);

                if (progress < 10) {
                    canvas.drawText(dataLimitText, (float) desiredPoint, height + 50, paintText);
                } else {
                    if (progress > 90) {
                        canvas.drawText(dataLimitText, (float) desiredPoint - 250, height + 50, paintText);
                    } else if (progress > 80) {
                        canvas.drawText(dataLimitText, (float) desiredPoint - 180, height + 50, paintText);
                    } else {
                        canvas.drawText(dataLimitText, (float) desiredPoint - 30, height + 50, paintText);
                    }
                }
            } else {
                Paint paintText = new Paint();
                paintText.setColor(Color.WHITE);
                paintText.setTextSize(30);
                paintText.setTypeface(tf);
                paintText.setFlags(Paint.ANTI_ALIAS_FLAG);
                canvas.drawText(dataLimitText, 25, height / 1.8f, paintText);
            }
        } else {

            p.setColor(Color.WHITE);
            p.setStrokeWidth(5);
            canvas.drawLine((float) desiredPoint, -40, (float) desiredPoint, height + 20, p);

            float throttleTextPosition = (float) desiredPoint / 2;
            float fullSpeedTextPosition = (float) ((width + desiredPoint) / 2);

            Paint bottomPaintText = new Paint();
            bottomPaintText.setColor(Color.GRAY);
            bottomPaintText.setTextSize(40);
            bottomPaintText.setTypeface(tf);
            bottomPaintText.setFlags(Paint.ANTI_ALIAS_FLAG);

            Paint nonStopPaintText = new Paint();
            nonStopPaintText.setColor(Color.WHITE);
            nonStopPaintText.setTextSize(50);
            bottomPaintText.setTypeface(tf);
            nonStopPaintText.setFlags(Paint.ANTI_ALIAS_FLAG);

            float nonStopTextWidth = nonStopPaintText.measureText(nonStopText);
            float throttleTextWidth = bottomPaintText.measureText(throttleLimitText);
            float fullSpeedTextWidth = bottomPaintText.measureText(fullSpeedText);

            canvas.drawText(throttleLimitText, throttleTextPosition - (throttleTextWidth / 2f), height + 50f, bottomPaintText);
            canvas.drawText(fullSpeedText, fullSpeedTextPosition - (fullSpeedTextWidth / 2f), height + 50f, bottomPaintText);
            canvas.drawText(nonStopText, throttleTextPosition - (nonStopTextWidth / 2f), height / 2f - ((nonStopPaintText.descent() + nonStopPaintText.ascent()) / 2f), nonStopPaintText);

        }
    }

    static class PaintCodeColor extends Color {
        private static float[] ColorToHSV(int originalColor) {
            float hsv[] = new float[3];
            RGBToHSV(red(originalColor), green(originalColor), blue(originalColor), hsv);
            return hsv;
        }

        public static int colorByChangingHue(int originalColor, float newHue) {
            float hsv[] = ColorToHSV(originalColor);
            hsv[0] = newHue;
            return HSVToColor(alpha(originalColor), hsv);
        }

        public static int colorByChangingSaturation(int originalColor, float newSaturation) {
            float hsv[] = ColorToHSV(originalColor);
            hsv[1] = newSaturation;
            return HSVToColor(alpha(originalColor), hsv);
        }

        public static int colorByChangingValue(int originalColor, float newValue) {
            float hsv[] = ColorToHSV(originalColor);
            hsv[2] = newValue;
            return HSVToColor(alpha(originalColor), hsv);
        }

        public static float hue(int color) {
            return ColorToHSV(color)[0];
        }

        public static float saturation(int color) {
            return ColorToHSV(color)[1];
        }

        public static float brightness(int color) {
            return ColorToHSV(color)[2];
        }

        public static int colorByChangingAlpha(int color, int newAlpha) {
            return argb(newAlpha, red(color), green(color), blue(color));
        }

        public static int colorByBlendingColors(int c1, float ratio, int c2) {
            return argb((int) ((1f - ratio) * alpha(c1) + ratio * alpha(c2)),
                    (int) ((1f - ratio) * red(c1) + ratio * red(c2)),
                    (int) ((1f - ratio) * green(c1) + ratio * green(c2)),
                    (int) ((1f - ratio) * blue(c1) + ratio * blue(c2)));
        }

        public static int colorByApplyingHighlight(int color, float ratio) {
            return colorByBlendingColors(color, ratio, colorByChangingAlpha(WHITE, alpha(color)));
        }

        public static int colorByApplyingShadow(int color, float ratio) {
            return colorByBlendingColors(color, ratio, colorByChangingAlpha(BLACK, alpha(color)));
        }
    }

    @Override
    public int getProgress() {
        return progress;
    }

    protected void onValueChanged() {
        /**
         * Update your progress Variable and draw again
         */
        progress = getProgress();
        this.invalidate();
    }

    protected void onValueChanged(int progressNew) {
        /**
         * Update your progress Variable and draw again
         */
        progress = getProgress();
        this.invalidate();
    }

    @Override
    public synchronized void setMax(int max) {
        onValueChanged();
        super.setMax(max);
    }

    @Override
    public synchronized void setProgress(int progress) {
        this.progress = progress;
        super.setProgress(progress);
        onValueChanged();

    }

    @Override
    public synchronized void setSecondaryProgress(int secondaryProgress) {
        onValueChanged();
        super.setSecondaryProgress(secondaryProgress);
    }

    private static class CacheForCanvas1 {
        private static Paint paint = new Paint();
        private static PaintCodeGradient gradient = null;

        private static RectF originalFrame = new RectF(0f, 0f, 466f, 156f);
        private static RectF resizedFrame = new RectF();
        private static RectF bezierRect = new RectF();
        private static Path bezierPath = new Path();
        private static PaintCodeLinearGradient bezierPathGradient = new PaintCodeLinearGradient();
    }

    public static class PaintCodeLinearGradient {
        private LinearGradient shader;
        private PaintCodeGradient paintCodeGradient;
        private float x0, y0, x1, y1;

        LinearGradient get(PaintCodeGradient paintCodeGradient, float x0, float y0, float x1, float y1) {
            if (this.shader == null || this.x0 != x0 || this.y0 != y0 || this.x1 != x1 || this.y1 != y1 || !this.paintCodeGradient.equals(paintCodeGradient)) {
                this.x0 = x0;
                this.y0 = y0;
                this.x1 = x1;
                this.y1 = y1;
                this.paintCodeGradient = paintCodeGradient;
                this.shader = paintCodeGradient.linearGradient(x0, y0, x1, y1);
            }
            return this.shader;
        }
    }


    // Resizing Behavior
    public static void resizingBehaviorApply(ResizingBehavior behavior, RectF rect, RectF target, RectF result) {
        if (rect.equals(target) || target == null) {
            result.set(rect);
            return;
        }

        if (behavior == ResizingBehavior.Stretch) {
            result.set(target);
            return;
        }

        float xRatio = Math.abs(target.width() / rect.width());
        float yRatio = Math.abs(target.height() / rect.height());
        float scale = 0f;

        switch (behavior) {
            case AspectFit: {
                scale = Math.min(xRatio, yRatio);
                break;
            }
            case AspectFill: {
                scale = Math.max(xRatio, yRatio);
                break;
            }
            case Center: {
                scale = 1f;
                break;
            }
        }

        float newWidth = Math.abs(rect.width() * scale);
        float newHeight = Math.abs(rect.height() * scale);
        result.set(target.centerX() - newWidth / 2,
                target.centerY() - newHeight / 2,
                target.centerX() + newWidth / 2,
                target.centerY() + newHeight / 2);
    }

    class PaintCodeGradient {
        private int[] colors;
        private float[] positions;

        public PaintCodeGradient(int[] colors, float[] positions) {
            if (positions == null) {
                int steps = colors.length;
                positions = new float[steps];
                for (int i = 0; i < steps; i++)
                    positions[i] = (float) i / (steps - 1);
            }

            this.colors = colors;
            this.positions = positions;
        }

        public LinearGradient linearGradient(float x0, float y0, float x1, float y1) {
            return new LinearGradient(x0, y0, x1, y1, this.colors, this.positions, Shader.TileMode.CLAMP);
        }

        public RadialGradient radialGradient(float startX, float startY, float startRadius, float endX, float endY, float endRadius) {
            int steps = this.colors.length;
            float[] positions = new float[steps];

            if (startRadius > endRadius) {
                float ratio = endRadius / startRadius;
                int[] colors = new int[steps];

                for (int i = 0; i < steps; i++) {
                    colors[i] = this.colors[steps - i - 1];
                    positions[i] = (1 - this.positions[steps - i - 1]) * (1 - ratio) + ratio;
                }

                return new RadialGradient(endX, endY, startRadius, colors, positions, Shader.TileMode.CLAMP);
            } else {
                float ratio = startRadius / endRadius;

                for (int i = 0; i < steps; i++) {
                    positions[i] = this.positions[i] * (1 - ratio) + ratio;
                }

                return new RadialGradient(startX, startY, endRadius, this.colors, positions, Shader.TileMode.CLAMP);
            }
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof PaintCodeGradient))
                return false;
            PaintCodeGradient other = (PaintCodeGradient) obj;
            return Arrays.equals(this.colors, other.colors) && Arrays.equals(this.positions, other.positions);
        }
    }
}