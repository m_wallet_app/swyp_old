package com.indy.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Dimension;
import android.support.design.widget.TabLayout;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.indy.utils.ConstantUtils;
import com.indy.utils.SharedPrefrencesManger;

/**
 * Created by emad on 8/9/16.
 */
public class CustomTabLayout extends TabLayout {
    private Typeface mTypeface;
    private SharedPrefrencesManger sharedPrefrencesManger;
    public CustomTabLayout(Context context) {
        super(context);
        init();
    }

    public CustomTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SharedPrefrencesManger getSharedPrefrencesManger() {
        return sharedPrefrencesManger;
    }

    public void setSharedPrefrencesManger(SharedPrefrencesManger sharedPrefrencesManger) {
        this.sharedPrefrencesManger = sharedPrefrencesManger;
    }

    public CustomTabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    int size;
    private void init() {

    }

    @Override
    public void addTab(Tab tab) {
        super.addTab(tab);

        if(sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)) {
            mTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/OpenSans-Semibold.ttf");
            size = 18;
        }else{
            mTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/aktive_grotesk_rg.ttf");
            size = 16;
        }

        ViewGroup mainView = (ViewGroup) getChildAt(0);
        ViewGroup tabView = (ViewGroup) mainView.getChildAt(tab.getPosition());

        int tabChildCount = tabView.getChildCount();

        for (int i = 0; i < tabChildCount; i++) {
            View tabViewChild = tabView.getChildAt(i);
//            tabViewChild.setBackgroundColor(Color.WHITE);
            if (tabViewChild instanceof TextView) {
                ((TextView) tabViewChild).setTypeface(mTypeface, Typeface.NORMAL);
                ((TextView) tabViewChild).setTextSize(Dimension.SP,size);
//                ((TextView) tabViewChild).setTextSize(0, 150);
//                ((TextView) tabViewChild).setTextSize(0,150);
            }
        }
    }

}
