package com.indy.customviews.SlideToUnlock;

/**
 * Created by Amir.jehangir on 11/7/2016.
 */
public interface SlideButtonListener {
    public void handleSlide();
}
