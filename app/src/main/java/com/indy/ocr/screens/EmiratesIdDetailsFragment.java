package com.indy.ocr.screens;


import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.controls.AdjustEvents;
import com.indy.controls.ServiceUtils;
import com.indy.helpers.DateHelpers;
import com.indy.models.emiratesIDVerificationService.EmiratesIDVerificationInputModel;
import com.indy.models.emiratesIdVerification.VerifyEmiratesIdInput;
import com.indy.models.emiratesIdVerification.VerifyEmiratesIdOutput;
import com.indy.models.onboarding_push_notifications.LogEIDDetailsRequestModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.verfiymsisdnusingeid.VerfyMsisidnUsingEidInput;
import com.indy.models.verfiymsisdnusingeid.VerfyMsisidnUsingEidOutput;
import com.indy.ocr.scanning.EmiratesIdDataObject;
import com.indy.services.LogEIDDetailsService;
import com.indy.services.VerfiyMsisdByEidService;
import com.indy.services.VerifyEmiratesIdOCRNewService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.buynewline.exisitingnumber.CoorporateNumberMigrationFragment;
import com.indy.views.fragments.buynewline.exisitingnumber.EligableNumberMigrationFragment;
import com.indy.views.fragments.buynewline.exisitingnumber.EmiratesIDVerificationFragment;
import com.indy.views.fragments.buynewline.exisitingnumber.InvalidEIDFrgament;
import com.indy.views.fragments.buynewline.exisitingnumber.NumberMigrationFragment;
import com.indy.views.fragments.buynewline.exisitingnumber.PendingPaymentFragment;
import com.indy.views.fragments.buynewline.exisitingnumber.RegisteredNonEmirateIDDocumentFragment;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.indy.views.fragments.utils.MasterFragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class EmiratesIdDetailsFragment extends MasterFragment {

    private static final int NO_OF_DIGITS = 15;
    public static String dateOfBirth = "";
    public static String fullName;
    public EmiratesIdDataObject emiratesIdDataObject;
    View rootView;
    EditText et_name, et_nationality, et_expiryDate, et_EIDNumber, et_arabicName;
    Button btnConfirm;
    TextView et_dob;
    String emiratesIdFromOCR = "";
    TextInputLayout til_name, til_emiratesID;
    DateHelpers dateUtils;
    private VerifyEmiratesIdInput verifyEmiratesIdInput;
    private VerifyEmiratesIdOutput verifyEmiratesIdOutput;
    private VerfyMsisidnUsingEidOutput verfyMsisidnUsingEidOutput;
    private int yearInt, monthInt, dayInt = 0;
    private int status = 0;
    private double diffYears = 0;

    public EmiratesIdDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_emirates_id_details, container, false);
        initUI();
        CommonMethods.logFirebaseEvent(mFirebaseAnalytics, ConstantUtils.TAGGING_enter_details);
        return rootView;
    }

    @Override
    public void initUI() {
        super.initUI();

        et_dob = (TextView) rootView.findViewById(R.id.et_date_of_birth);
        et_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCalendar();
            }
        });
        et_EIDNumber = (EditText) rootView.findViewById(R.id.et_id_number);
        et_expiryDate = (EditText) rootView.findViewById(R.id.et_expiry_date);
        et_name = (EditText) rootView.findViewById(R.id.et_name);
        et_nationality = (EditText) rootView.findViewById(R.id.et_nationality);
        et_arabicName = (EditText) rootView.findViewById(R.id.et_arabic_name);
        btnConfirm = (Button) rootView.findViewById(R.id.btn_confirm);
        til_name = (TextInputLayout) rootView.findViewById(R.id.fullNameTxtInput);
        til_emiratesID = (TextInputLayout) rootView.findViewById(R.id.eid_txt_input);
        dateUtils = new DateHelpers();
        et_EIDNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().length() > 0) {
                    if (s.toString().trim().length() == NO_OF_DIGITS) {
                        til_emiratesID.setError(null);
                        if (isValid()) {
                            enableNextBtn();
                        } else {
                            disableNextBtn();
                        }
                    } else {
                        til_emiratesID.setError(getString(R.string.eid_invalid_error));
                    }
                } else {
                    til_emiratesID.setError(getString(R.string.eid_invalid_error));
                    disableNextBtn();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        et_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length() > 0) {
                    til_name.setError(null);
                    if (isValid()) {
                        enableNextBtn();
                    } else {
                        disableNextBtn();
                    }
                } else {
                    til_name.setError(getString(R.string.please_enter_name));
                    disableNextBtn();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        if (getArguments() != null && getArguments().getParcelable(ConstantUtils.EMIRATES_ID) != null) {
            emiratesIdDataObject = getArguments().getParcelable(ConstantUtils.EMIRATES_ID);
            if (emiratesIdDataObject != null) {
                et_name.setText(emiratesIdDataObject.getFullName());
                if (dateOfBirth.isEmpty() &&
                        emiratesIdDataObject.getDateOfBirth() != null &&
                        !emiratesIdDataObject.getDateOfBirth().trim().isEmpty()) {
                }
                dateOfBirth = formatDateOfBirth(emiratesIdDataObject.getDateOfBirth());
                et_dob.setText(dateOfBirth);
                et_EIDNumber.setText(emiratesIdDataObject.getEmiratesIdNumber());
                et_nationality.setText(emiratesIdDataObject.getNationality());
                et_expiryDate.setText(emiratesIdDataObject.getExpiryDate());
                et_arabicName.setText(emiratesIdDataObject.getFullNameArabic());// + " " + emiratesIdDataObject.getLastNameLocal());
                enableNextBtn();

            } else {
                disableNextBtn();
            }

        }
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (emiratesIdDataObject != null) {
                    if (et_name.getText().toString().isEmpty() || et_dob.getText().toString().isEmpty()) {
                        Toast.makeText(getActivity(), "Please enter details.", Toast.LENGTH_LONG);
                    } else {
                        onConsumeService();

                    }
//                    TermsAndConditionsFragment termsAndConditionsFragment = new TermsAndConditionsFragment();
//                    Bundle bundle = new Bundle();
//                    bundle.putString(ConstantUtils.userName, emiratesIdDataObject.getFullName());
//                    bundle.putString(ConstantUtils.email, emailString);
//                    bundle.putString(ConstantUtils.birthdate, emiratesIdDataObject.getDateOfBirth());
//
//                    termsAndConditionsFragment.setArguments(bundle);
//                    regActivity.replaceFragmnet(termsAndConditionsFragment,R.id.frameLayout,true);
                }
            }
        });
        if (isValid()) {
            enableNextBtn();
        } else {
            disableNextBtn();
        }
    }

    private void enableNextBtn() {
        btnConfirm.setAlpha(ConstantUtils.ALPHA_BUTTON_ENABLED);
        btnConfirm.setClickable(true);
        btnConfirm.setEnabled(true);
    }

    private void disableNextBtn() {
        btnConfirm.setAlpha(.5f);
        btnConfirm.setClickable(false);
        btnConfirm.setEnabled(false);
    }

    @Override
    public void onResume() {
        et_dob.setText(dateOfBirth);
        super.onResume();
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        showLoadingFragment();
//        progressID.setVisibility(View.VISIBLE);
        EmiratesIDVerificationInputModel emiratesIDVerificationInputModel;

        emiratesIDVerificationInputModel = new EmiratesIDVerificationInputModel();

        emiratesIDVerificationInputModel.setLang(currentLanguage);
        emiratesIDVerificationInputModel.setAppVersion(appVersion);
        emiratesIDVerificationInputModel.setOsVersion(osVersion);
        emiratesIDVerificationInputModel.setToken(token);
        emiratesIDVerificationInputModel.setChannel(channel);
        emiratesIDVerificationInputModel.setDeviceId(token);
        emiratesIDVerificationInputModel.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        emiratesIdFromOCR = et_EIDNumber.getText().toString();
        String part1 = emiratesIdFromOCR.substring(0, 3);
        String part2 = emiratesIdFromOCR.substring(3, 7);
        String part3 = emiratesIdFromOCR.substring(7, 14);
        String part4 = emiratesIdFromOCR.substring(14, 15);
        part1 += "-";
        part2 += "-";
        part3 += "-";

        emiratesIdFromOCR = part1 + part2 + part3 + part4;
        dateOfBirth = et_dob.getText().toString();
        dateOfBirth = formatDateOfBirth(dateOfBirth);
        et_dob.setText(dateOfBirth);
        emiratesIDVerificationInputModel.setEmiratesId(emiratesIdFromOCR);
        new VerifyEmiratesIdOCRNewService(this, emiratesIDVerificationInputModel);

    }

    public boolean isValid() {

        return et_name.getText().toString().length() >= 1 && et_EIDNumber.getText().toString().length() == NO_OF_DIGITS && et_dob.getText().toString().length() > 0;
    }

    public String formatDateOfBirth(String date) {

        String returnString = "";
        SimpleDateFormat dateFormatter;
        try {
            Date formattedDate;
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            formattedDate = df.parse(date);
            dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
            return dateFormatter.format(formattedDate);
        } catch (Exception ex) {
            return returnString;

        }
    }

    private void onEmiratesIDVerificationFail() {
        logEvent(AdjustEvents.EID_PRECESS_COMPLETE, new Bundle());
//        emiratesID = getCode(true);
//        Bundle bundle2 = new Bundle();
//
//        bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_delivery_info_eid_entered);
//        bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_delivery_info_eid_entered);
//        mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_delivery_info_eid_entered, bundle2);
//        regActivity.replaceFragmnet(new OrderSummaryFragment(), R.id.frameLayout, true);
        CommonMethods.logFirebaseEvent(mFirebaseAnalytics, ConstantUtils.TAGGING_onboarding_error_eid_failure);
        if (verifyEmiratesIdOutput.getErrorMsgEn() != null) {
            if (currentLanguage.equalsIgnoreCase("en"))
                showErrorFragment(verifyEmiratesIdOutput.getErrorMsgEn());
            else
                showErrorFragment(verifyEmiratesIdOutput.getErrorMsgAr());
        } else {
            showErrorFragment(getString(R.string.generice_error));
        }


    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            if (isVisible()) {
                if (responseModel != null && responseModel.getServiceType() != null && responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.LOG_USER_EVENT)) {
                } else {
                    regActivity.onBackPressed();
                    if (isVisible()) {
                        if (responseModel != null && responseModel.getResultObj() != null &&
                                responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.emirates_id_verify_new)) {
                            verifyEmiratesIdOutput = (VerifyEmiratesIdOutput) responseModel.getResultObj();
                            if (verifyEmiratesIdOutput != null) {
                                if (verifyEmiratesIdOutput.getStatus() == null)
                                    onEmiratesIDVerificationFail();
                                else
                                    onEmiratesIDVerificationSuccess();
                            }
                        } else if (responseModel != null && responseModel.getResultObj() != null &&
                                responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.CHECK_ELIGIBILITY_FOR_MIGRATION)) {
                            verfyMsisidnUsingEidOutput = (VerfyMsisidnUsingEidOutput) responseModel.getResultObj();
                            if (verfyMsisidnUsingEidOutput != null)
                                if (verfyMsisidnUsingEidOutput.getErrorCode() != null)
                                    onNumberMigrationFail();
                                else
                                    onNumberMigrationSucess();
                        }
                    }
                }

            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

    }

    private void showLoadingFragment() {
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.loadingString, getString(R.string.verifying_eid));
        LoadingFragmnet loadingFragmnet = new LoadingFragmnet();
        loadingFragmnet.setArguments(bundle);
        regActivity.addFragmnet(loadingFragmnet, R.id.loadingFragmeLayout, true);
    }

    private void onNumberMigrationSucess() {
        try {
            status = verfyMsisidnUsingEidOutput.getStatus();
            status = 1;
            switch (status) {
                case 1: // eligable
                    onEligable();
                    break;
                case 2: // not eligable due to un paid amount
                    onNotEligableDueToPaidAmount();
                    break;
                case 3: // Not eligible due to migrate to Indy
                    onNotEligableDueToMigrateIndy();
                    break;
                case 4: //Failure
                    Bundle bundle2 = new Bundle();

                    bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_migration_invalid_eid_number);
                    bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_migration_invalid_eid_number);
                    mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_migration_invalid_eid_number, bundle2);
                    onInValidEidResponse();
                    break;
                case 6:
                    onAlreadyCustomerResponse();
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void onAlreadyCustomerResponse() {


        regActivity.replaceFragmnet(new RegisteredNonEmirateIDDocumentFragment(), R.id.frameLayout, true);

    }

    private void onInValidEidResponse() {
        InvalidEIDFrgament invalidEIDFrgament = new InvalidEIDFrgament();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.eidNO, EmiratesIDVerificationFragment.emiratesID);
        bundle.putString(ConstantUtils.mobileNo, NumberMigrationFragment.migrationMobileNumber);
        invalidEIDFrgament.setArguments(bundle);
        regActivity.replaceFragmnet(invalidEIDFrgament, R.id.frameLayout, true);

    }

    private void onNotEligableDueToMigrateIndy() {
        Fragment corporateNumberMigrationFragment = new CoorporateNumberMigrationFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.mobileNo, NumberMigrationFragment.migrationMobileNumber);
        corporateNumberMigrationFragment.setArguments(bundle);
        regActivity.replaceFragmnet(corporateNumberMigrationFragment, R.id.frameLayout, true);

    }

    private void onNotEligableDueToPaidAmount() {
        Bundle bundle = new Bundle();
        bundle.putString("AMOUNT", String.valueOf(verfyMsisidnUsingEidOutput.getAmount()));
        Fragment pendingFragmmnet = new PendingPaymentFragment();
        pendingFragmmnet.setArguments(bundle);
        regActivity.replaceFragmnet(pendingFragmmnet, R.id.frameLayout, true);
    }

    private void onEligable() {
        Bundle bundle2 = new Bundle();

        bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_migration_valid_eid_entered);
        bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_migration_valid_eid_entered);
        mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_migration_valid_eid_entered, bundle2);
        EligableNumberMigrationFragment eligableNumberMigrationFragment = new EligableNumberMigrationFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.mobileNo, NumberMigrationFragment.migrationMobileNumber);
        eligableNumberMigrationFragment.setArguments(bundle);
        regActivity.replaceFragmnet(eligableNumberMigrationFragment, R.id.frameLayout, true);
    }

    private void onNumberMigrationFail() {
        if (currentLanguage.equalsIgnoreCase("en"))
            showErrorFragment(verfyMsisidnUsingEidOutput.getErrorMsgEn());
        else
            showErrorFragment(verfyMsisidnUsingEidOutput.getErrorMsgAr());

//        ((RegisterationActivity) getActivity()).replaceFragmnet(new RegisteredNonEmirateIDDocumentFragment(), R.id.frameLayout, true);

    }

    private void onEmiratesIDVerificationSuccess() {
        logEvent(AdjustEvents.EID_PRECESS_COMPLETE, new Bundle());
        status = verifyEmiratesIdOutput.getStatus();
        EmiratesIDVerificationFragment.emiratesID = emiratesIdFromOCR;
        fullName = et_name.getText().toString();
        switch (status) {

            case 1: // eligable
//                onEligable();
                logEvent(AdjustEvents.AGE_ELIGIBILITY_SUCCESS, new Bundle());


                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_confirm_details);
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_delivery_info_eid_entered);
                logEvent(AdjustEvents.REVIEW_ORDER_SUMMARY, new Bundle());
                if (sharedPrefrencesManger.isFromMigration()) {
                    verifyMigrationEligibility();
                } else {
                    LogEIDDetailsRequestModel logEIDDetailsRequestModel = new LogEIDDetailsRequestModel();
                    if (emiratesIdDataObject != null && emiratesIdDataObject.getDateOfBirth() != null && !emiratesIdDataObject.getDateOfBirth().isEmpty())
                        logEIDDetailsRequestModel.setBirthDate(formatDateOfBirth(emiratesIdDataObject.getDateOfBirth()));
                    logEIDDetailsRequestModel.setEmiratesid(emiratesIdDataObject.getEmiratesIdNumber());
                    logEIDDetailsRequestModel.setFullName(emiratesIdDataObject.getFullName());
                    logEIDDetailsRequestModel.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
                    new LogEIDDetailsService(this, logEIDDetailsRequestModel);
                    EmailAddressFragment emailAddressFragment = new EmailAddressFragment();
                    Bundle bundle = new Bundle();
                    if (emiratesIdDataObject != null && emiratesIdDataObject.getDateOfBirth() != null && emiratesIdDataObject.getDateOfBirth().length() > 0) {
                        bundle.putString(ConstantUtils.birthdate, formatDateOfBirth(emiratesIdDataObject.getDateOfBirth()));
                    }
                    emailAddressFragment.setArguments(bundle);
                    regActivity.replaceFragmnet(emailAddressFragment, R.id.frameLayout, true);
                }
                break;
            case 6:
                logEvent(AdjustEvents.ALREADY_SWYP_CUSTOMER, new Bundle());
                CommonMethods.logFirebaseEvent(mFirebaseAnalytics, ConstantUtils.TAGGING_onboarding_error_existing_user);

//                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_onboarding_invalid_eid_number);
                regActivity.replaceFragmnet(new RegisteredNonEmirateIDDocumentFragment(), R.id.frameLayout, true);
                break;
            default:
                logEvent(AdjustEvents.AGE_ELIGIBILITY_FAILURE, new Bundle());
//                CommonMethods.logFirebaseEvent(mFirebaseAnalytics,ConstantUtils.TAGGING_onboarding_error_existing_user);
                CommonMethods.logFirebaseEvent(mFirebaseAnalytics, ConstantUtils.TAGGING_onboarding_error_eid_failure);

                if (verifyEmiratesIdOutput.getErrorMsgEn() != null) {
                    if (currentLanguage.equalsIgnoreCase("en"))
                        showErrorFragment(verifyEmiratesIdOutput.getErrorMsgEn());
                    else
                        showErrorFragment(verifyEmiratesIdOutput.getErrorMsgAr());
                } else {
                    showErrorFragment(getString(R.string.generice_error));
                }
                break;
        }
    }

    private void verifyMigrationEligibility() {
        showLoadingFragment();
        VerfyMsisidnUsingEidInput verfyMsisidnUsingEidInput;

        verfyMsisidnUsingEidInput = new VerfyMsisidnUsingEidInput();
        verfyMsisidnUsingEidInput.setAppVersion(appVersion);
        verfyMsisidnUsingEidInput.setToken(token);
        verfyMsisidnUsingEidInput.setOsVersion(osVersion);
        verfyMsisidnUsingEidInput.setChannel(channel);
        verfyMsisidnUsingEidInput.setDeviceId(deviceId);
        verfyMsisidnUsingEidInput.setMsisdn(NumberMigrationFragment.migrationMobileNumber);
        verfyMsisidnUsingEidInput.setEmiratesId(EmiratesIDVerificationFragment.emiratesID);
        verfyMsisidnUsingEidInput.setLang(currentLanguage);
        verfyMsisidnUsingEidInput.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        new VerfiyMsisdByEidService(this, verfyMsisidnUsingEidInput);

    }

    private void showErrorFragment(String error) {
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, error);
        ErrorFragment errorFragment = new ErrorFragment();
        errorFragment.setArguments(bundle);
        regActivity.replaceFragmnet(errorFragment, R.id.frameLayout, true);
    }

    public void showCalendar() {
        try {
            final Dialog dialog = new Dialog(getActivity());
            dialog.setContentView(R.layout.testing);

            final DatePicker datePicker = (DatePicker) dialog.findViewById(R.id.datePicker);
            Calendar cal1 = Calendar.getInstance();
            cal1 = Calendar.getInstance();

            cal1.setTimeInMillis(new Date().getTime());
            cal1.add(Calendar.YEAR, -18);
//        cal1.add(Calendar.DAY_OF_MONTH,-1);
            datePicker.setMaxDate(cal1.getTimeInMillis());
            cal1.setTime(new Date());
            cal1.add(Calendar.DAY_OF_MONTH, 1);
            cal1.add(Calendar.YEAR, -30);
//            datePicker.setMinDate(cal1.getTimeInMillis());

            Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
            // if button is clicked, close the custom dialog
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int day = datePicker.getDayOfMonth();
                    int month = datePicker.getMonth();
                    int year = datePicker.getYear();
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(year, month, day);
                    SimpleDateFormat dateFormatter;

                    onDateSelect(day + "", month + "", year + "");
//                long diffYears = dateHelpers.validateDates(dateUtils.getCurrentDate(), getSelectedDate());
                    dialog.dismiss();

                }
            });

            Button dialogCancelButton = (Button) dialog.findViewById(R.id.dialogButtonCancel);
            dialogCancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.cancel();
                }
            });

            dialog.show();
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }
    }

    public void onDateSelect(String day, String month, String year) {
        try {

            int monthValTemp = Integer.parseInt(month);
            monthValTemp += 1;
        } catch (Exception ex) {
        }
        Calendar cal1 = Calendar.getInstance();
        yearInt = Integer.valueOf(year);
        monthInt = Integer.valueOf(month);
        dayInt = Integer.valueOf(day);
        cal1.set(yearInt, monthInt, dayInt);

        Date date = cal1.getTime();
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        et_dob.setText(dateFormatter.format(date));
        dateOfBirth = dateFormatter.format(date);
        emiratesIdDataObject.setDateOfBirth(year + "-" + month + "-" + day);
        if (isValid()) {
            enableNextBtn();
        } else {
            disableNextBtn();
        }
    }

}
