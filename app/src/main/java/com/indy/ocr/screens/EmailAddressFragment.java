package com.indy.ocr.screens;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.controls.AdjustEvents;
import com.indy.helpers.ValidationHelper;
import com.indy.models.onboarding_push_notifications.LogContactDetailsRequestModel;
import com.indy.models.utils.MasterInputResponse;
import com.indy.utils.CommonMethods;
import com.indy.services.LogContactDetailsService;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.buynewline.Registeration4Fragment;
import com.indy.views.fragments.buynewline.TermsAndConditionsFragment;
import com.indy.views.fragments.utils.MasterFragment;

/**
 * Created by emad on 9/7/16.
 */
public class EmailAddressFragment extends MasterFragment {
    private View view;
    private Button nextBtn;
    private EditText mobileNumber, email;
    public static String mobileNoStr, emailStr, dob;
    private TextInputLayout mobileNoTxtInput, emailTxtInput;
    private CharSequence emailCharSeq, fullNameCharSeq;
    private boolean fromNotification = false;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_email_number, container, false);
        initUI();
        if (getArguments() != null) {
            dob = getArguments().getString(ConstantUtils.birthdate);
            if(getArguments().containsKey(ConstantUtils.KEY_FROM_NOTIFICATION)){
                fromNotification = true;
                email.setText(EmailAddressFragment.emailStr);
                mobileNumber.setText(EmailAddressFragment.mobileNoStr);
            }
        }
        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_let_us_know_enter_details);

        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
        regActivity.showHeaderLayout();
        regActivity.setHeaderTitle("");
        nextBtn = (Button) view.findViewById(R.id.nextBtn);
        mobileNumber = (EditText) view.findViewById(R.id.mobileNo);
        email = (EditText) view.findViewById(R.id.email);
        emailTxtInput = (TextInputLayout) view.findViewById(R.id.emailTxtInput);

        mobileNoTxtInput = (TextInputLayout) view.findViewById(R.id.mobileNoInputLayout);
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()) {
                    logEvent(AdjustEvents.EMAIL_PHONE, new Bundle());
                    CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_let_us_know_details_successful);
                    hideKeyBoard();
                    Bundle bundle2 = new Bundle();

                    bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_name_email_entered);
                    bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_name_email_entered);
                    mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_name_email_entered, bundle2);

                    TermsAndConditionsFragment termsAndConditionsFragment = new TermsAndConditionsFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString(ConstantUtils.userName, mobileNumber.getText().toString());
                    bundle.putString(ConstantUtils.email, email.getText().toString());
                    bundle.putString(ConstantUtils.birthdate, dob);
                    mobileNoStr = mobileNumber.getText().toString();
                    emailStr = email.getText().toString();
                    termsAndConditionsFragment.setArguments(bundle);
                    regActivity.replaceFragmnet(termsAndConditionsFragment, R.id.frameLayout, true);
                }
            }
        });

        disableNextBtn();
        changeNextBtnState();
    }

    private boolean isValid() {
        boolean isVaild = true;


        if (mobileNumber.getText().toString().trim().length() == 0) {
            setTextInputLayoutError(mobileNoTxtInput, getString(R.string.enter_full_name));
            isVaild = false;
        } else {
            removeTextInputLayoutError(mobileNoTxtInput);
        }
        if (email.getText().toString().trim().length() == 0) {
            setTextInputLayoutError(emailTxtInput, getString(R.string.enter_email));
            isVaild = false;

        } else {
            if ((validationHelper.isEmailValid(email.getText().toString().trim()))) {
                removeTextInputLayoutError(emailTxtInput);
            } else {
                setTextInputLayoutError(emailTxtInput, getString(R.string.enter_valid_email));
                isVaild = false;
                Bundle bundle2 = new Bundle();

                bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_incorrect_email_entererd);
                bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_incorrect_email_entererd);
                mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_incorrect_email_entererd, bundle2);
            }
        }
        return isVaild;
    }

    public void changeNextBtnState() {
        mobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
                fullNameCharSeq = s;
                if (mobileNumber != null)
                    if (s.length() == 0 || mobileNumber.length() == 0)
                        disableNextBtn();
                    else
                        enableNextBtn();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
            }
        });
        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
                emailCharSeq = s;
                if (fullNameCharSeq != null)
                    if (s.length() == 0 || fullNameCharSeq.length() == 0)
                        disableNextBtn();
                    else
                        enableNextBtn();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
            }
        });
    }

    private void enableNextBtn() {
        nextBtn.setAlpha(ConstantUtils.ALPHA_BUTTON_ENABLED);
        nextBtn.setClickable(true);
    }

    private void disableNextBtn() {
        nextBtn.setAlpha(ConstantUtils.ALPHA_BUTTON_DISABLED);
        nextBtn.setClickable(false);
    }


}
