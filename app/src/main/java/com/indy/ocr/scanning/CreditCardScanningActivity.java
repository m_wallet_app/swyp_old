package com.indy.ocr.scanning;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat.OnRequestPermissionsResultCallback;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.indy.R;
import com.indy.utils.ConstantUtils;
import com.kofax.kmc.ken.engines.ImageProcessor;
import com.kofax.kmc.ken.engines.data.Image;
import com.kofax.kmc.ken.engines.data.ImagePerfectionProfile;
import com.kofax.kmc.kui.uicontrols.CameraInitializationFailedEvent;
import com.kofax.kmc.kui.uicontrols.CameraInitializationFailedListener;
import com.kofax.kmc.kui.uicontrols.ImageCaptureView;
import com.kofax.kmc.kui.uicontrols.ImageCapturedEvent;
import com.kofax.kmc.kui.uicontrols.ImageCapturedListener;
import com.kofax.kmc.kui.uicontrols.ImgReviewEditCntrl;
import com.kofax.kmc.kui.uicontrols.captureanimations.CaptureExperience;
import com.kofax.kmc.kui.uicontrols.data.Flash;
import com.kofax.kmc.kut.utilities.AppContextProvider;
import com.kofax.kmc.kut.utilities.Licensing;
import com.kofax.kmc.kut.utilities.error.KmcException;
import com.kofax.mobile.sdk.capture.CaptureActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CreditCardScanningActivity
        extends Activity
        implements OnRequestPermissionsResultCallback, ImageProcessor.ImageOutListener {
    private static final String TAG = CaptureActivity.class.getSimpleName();
    private static final String[] PERMISSIONS = {
            Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private static int RESULT_LOAD_IMAGE_GALLERY = 1;


    // Image processing settings
    //private static final String PREFIX_OPT_PRECROPPED = "_DeviceType_0_DoNoPageDetection_";
    private static final String PREFIX_OPT_UNCROPPED = "_DeviceType_2_Do90DegreeRotation_4_DoCropCorrection__DoSkewCorrectionPage_";
    private static final String PROFILE_OPT_EXTRACT = "_LoadInlineSetting_[CSkewDetect.document_smaller_dim.double=2.125]\n" +
            "_LoadInlineSetting_[CSkewDetect.document_larger_dim.double=3.375]\n" +
            "_LoadInlineSetting_[CSkewDetect.do_scale_to_dpi.Bool=1]\n" +
            "_LoadInlineSetting_[CSkewDetect.scaled_image_dpi.Int=300]\n" +
            "_LoadInlineSetting_[CSkewDetect.correct_illumination.Bool=0]\n" +
            "_LoadInlineSetting_[CSkewDetect.Remove_Shadows=0]\n" +
            "_LoadInlineSetting_[CSkewDetect.color_stats_error_sum_thr_white_bkg=36]\n" +
            "_LoadInlineSetting_[CSkewDetect.color_stats_error_sum_thr_black_bkg=36]\n" +
            "_LoadInlineSetting_[CSkewDetect.step_white_bkg=1]\n" +
            "_LoadInlineSetting_[CSkewDetect.step_black_bkg=1]\n" +
            "_LoadInlineSetting_[CSkewDetect.bright_window=255]\n" +
            "_LoadInlineSetting_[CSkewDetect.side_angle_diff_thr.double=90.0]\n" +
            "_LoadInlineSetting_[EdgeCleanup.enable=0]\n" +
            "_LoadInlineSetting_[CZoneExtract.PerformRecognition=1]\n" +
            "_LoadInlineSetting_[CZoneExtract.Enable=1]\n" +
            "_LoadInlineSetting_[CZoneExtract.UseFieldType=2]\n" +
            "_LoadInlineSetting_[CZoneExtract.NumZones=2]\n" +
            "_LoadInlineSetting_[CZoneExtract.AspectRatio.Double=1.584112]\n" +
            "_LoadInlineSetting_[CZoneExtract.LineLabels.00.00.String=CardNumber]\n" +
            "_LoadSetting_<Property Name=\"CZoneExtract.LineLabelsTemplate.00.00.String\" Value=\"1:9999[0-9 ]\" />\n" +
            "_LoadInlineSetting_[CZoneExtract.Max_bbX1.00.Int=543]\n" +
            "_LoadInlineSetting_[CZoneExtract.Max_bbY1.00.Int=5000]\n" +
            "_LoadInlineSetting_[CZoneExtract.Max_bbX2.00.Int=9500]\n" +
            "_LoadInlineSetting_[CZoneExtract.Max_bbY2.00.Int=6954]\n" +
            "_LoadInlineSetting_[CZoneExtract.LineHeight.00.00.Int=961]\n" +
            "_LoadInlineSetting_[CZoneExtract.LineWidth.00.00.Int=8396]\n" +
            "_LoadInlineSetting_[CZoneExtract.LineMedianX1.00.00.Int=1058]\n" +
            "_LoadInlineSetting_[CZoneExtract.LineMedianX2.00.00.Int=9010]\n" +
            "_LoadInlineSetting_[CZoneExtract.LineMedianY1.00.00.Int=5504]\n" +
            "_LoadInlineSetting_[CZoneExtract.LineMedianY2.00.00.Int=6478]\n" +
            "_LoadInlineSetting_[CZoneExtract.Binarization_Method.00.Int=3]\n" +
            "_LoadInlineSetting_[CZoneExtract.LineLabels.01.00.String=ExpirationDate]\n" +
            "_LoadSetting_<Property Name=\"CZoneExtract.LineLabelsTemplate.01.00.String\" Value=\"1:9999[0-9 /-*]\" />\n" +
            "_LoadInlineSetting_[CZoneExtract.ValidExpYearStart.Int=1980]\n" +
            "_LoadInlineSetting_[CZoneExtract.ValidExpYearStop.Int=2027]\n" +
            "_LoadInlineSetting_[CZoneExtract.CC_CheckDigitMatch_TryTwoWeakest=1]\n" +
            "_LoadInlineSetting_[CZoneExtract.CC_CheckDigitMatch_TryThirdChoice=1]";

    private final PermissionsManager mPermissionsManager = new PermissionsManager(this);

    private ImageCaptureView mImageCaptureView;
    private ImgReviewEditCntrl mImgReviewEditCntrl;
    private View reviewVIew;
    private TextView text;
    private CaptureExperience mExperience;
    private ImageButton mForceCapture;
    private ImageButton mBrowse;
    private ImageProcessor imageProcessor;
    private ProgressDialog dialog;
    private final ImageCapturedListener mImageCapturedListener = new ImageCapturedListener() {
        @Override
        public void onImageCaptured(final ImageCapturedEvent event) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    reviewImage(event.getImage());
                    processImage(event.getImage(), false);
                }
            });
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Initializing the SDK
        AppContextProvider.setContext(getApplicationContext());
        Licensing.setMobileSDKLicense(ConstantUtils.KOFAX_LICENSE);
        if (mPermissionsManager.isGranted(PERMISSIONS)) {
            setUp();
        } else {
            mPermissionsManager.request(PERMISSIONS);
        }

        dialog = new ProgressDialog(this);
        dialog.setMessage("Wait...");
        dialog.setIndeterminate(true);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] results) {
        if (mPermissionsManager.isGranted(PERMISSIONS)) {
            setUp();
        } else {
            new AlertDialog.Builder(this)
                    .setMessage("Please give me permissions!")
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .setCancelable(false)
                    .show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Getting image from gallery
        if (resultCode == RESULT_OK && requestCode == RESULT_LOAD_IMAGE_GALLERY && data != null) {
            Uri selectedImage = data.getData();
            AppContextProvider.setContext(getApplicationContext());
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            final Image image = new Image();
            image.setImageFilePath(picturePath);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    reviewImage(image);
                    processImage(image, true);
                }
            });
        }
    }

    private void setUp() {
        setContentView(R.layout.activity_capture);

        mImageCaptureView = (ImageCaptureView) findViewById(R.id.view_capture);
        mImgReviewEditCntrl = (ImgReviewEditCntrl) findViewById(R.id.view_review);
        reviewVIew = findViewById(R.id.view_review_view);
        text = (TextView) findViewById(R.id.text);
        mForceCapture = (ImageButton) findViewById(R.id.btn_force_capture);
        mBrowse = (ImageButton) findViewById(R.id.btn_browse);
        imageProcessor = new ImageProcessor();

        // Set flash mode to OFF. We do not recommend using torch or flash during capturing CC
        mImageCaptureView.addCameraInitializationFailedListener(new CameraInitializationFailedListener() {
            @Override
            public void onCameraInitializationFailed(CameraInitializationFailedEvent event) {
                mImageCaptureView.setFlash(Flash.OFF);
            }
        });

        // Setting up the experience
        mExperience = CaptureExperienceFactory.createExperience(mImageCaptureView, this);

        // Listening for the image captured event
        mExperience.addOnImageCapturedListener(mImageCapturedListener);

        mForceCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Forces an image capture
                mImageCaptureView.forceTakePicture();
            }
        });

        mBrowse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Select an image from gallery
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE_GALLERY);
            }
        });

        mExperience.takePictureContinually();
    }

    private void processImage(Image image, boolean isFromGallery) {

        // Use the image processing string profile - assume uncropped image
        ImagePerfectionProfile ipp = new ImagePerfectionProfile();
        ipp.setIpOperations(PREFIX_OPT_UNCROPPED + PROFILE_OPT_EXTRACT);

        ipp.setUseTargetFrameCrop(
                isFromGallery ?
                        ImagePerfectionProfile.UseTargetFrameCrop.OFF :
                        ImagePerfectionProfile.UseTargetFrameCrop.ON
        );

        imageProcessor.setImagePerfectionProfile(ipp);

        // Set the image perfection profile of the image processor
        imageProcessor.addImageOutEventListener(this);
        try {
            imageProcessor.processImage(image);
        } catch (KmcException e) {
            Log.e(TAG, "Exception", e);
            Toast.makeText(this, "Unable to process the captured image", Toast.LENGTH_LONG).show();
        }
        dialog.show();
    }

    @Override
    public void imageOut(ImageProcessor.ImageOutEvent event) {
        // Handle response from the image processor.
        imageProcessor.removeImageOutEventListener(this);
        Image image = mImgReviewEditCntrl.getImage();
        if (image != null) {
            image.imageClearBitmap();
        }

        Image outputImage = event.getImage();
        if (outputImage == null) {
            Toast.makeText(this, "Unable to process captured image", Toast.LENGTH_LONG).show();
            dialog.dismiss();

            return;
        }

        try {
            mImgReviewEditCntrl.setImage(outputImage);
        } catch (KmcException e) {
            Log.e(TAG, "Exception", e);
            Toast.makeText(this, "Unable to show the captured image", Toast.LENGTH_LONG).show();
        }

        try {
            String outputData = outputImage.getImageMetaData();
            JSONObject metadata = new JSONObject(outputData);
            JSONArray lines = metadata.getJSONObject("Front Side").getJSONObject("Text Lines").getJSONArray("Lines");

            String cardNumber = "";
            String expirationDate = "";

            for (int i = 0; i < lines.length(); i++) {
                if ("CardNumber".equals(lines.getJSONObject(i).getString("Label"))) {
                    cardNumber = lines.getJSONObject(i).getString("OCR Data");
                }
                if ("ExpirationDate".equals(lines.getJSONObject(i).getString("Label"))) {
                    expirationDate = lines.getJSONObject(i).getString("OCR Data");
                }
            }

            final StringBuilder sb = new StringBuilder();
            sb.append("\n\n\n");
            sb.append("Card number: ").append(cardNumber).append("\n");
            sb.append("Expiration date: ").append(expirationDate).append("\n");

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    text.setText(sb.toString());
                }
            });

        } catch (JSONException e) {
            Log.e(TAG, "Exception", e);
            Toast.makeText(this, "Unable to parse extracted data", Toast.LENGTH_LONG).show();
        } finally {
            dialog.dismiss();
        }
    }

    private void reviewImage(Image image) {
        mImageCaptureView.setVisibility(View.GONE);
        reviewVIew.setVisibility(View.VISIBLE);
        mForceCapture.setVisibility(View.GONE);
        mBrowse.setVisibility(View.GONE);
        try {
            mImgReviewEditCntrl.setImage(image);
        } catch (KmcException e) {
            Log.e(TAG, "Exception", e);
            Toast.makeText(this, "Unable to show the captured image", Toast.LENGTH_LONG).show();
        }
    }

    private boolean finishReview() {
        if (reviewVIew.getVisibility() != View.VISIBLE) {
            return false;
        }
        reviewVIew.setVisibility(View.GONE);
        text.setText("");
        mImageCaptureView.setVisibility(View.VISIBLE);
        mForceCapture.setVisibility(View.VISIBLE);
        mBrowse.setVisibility(View.VISIBLE);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (finishReview()) {
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mExperience != null) {
            mExperience.removeOnImageCapturedListener(mImageCapturedListener);
            mExperience.destroy();
        }
    }
}