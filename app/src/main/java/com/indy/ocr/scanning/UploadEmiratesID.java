package com.indy.ocr.scanning;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.ocrStep2.ocr.UploadEmiratesIdOutput;
import com.kofax.kmc.ken.engines.data.Image;
import com.kofax.kmc.kut.utilities.error.KmcException;

import java.io.File;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.indy.utils.ConstantUtils.token;

/**

 */

public class UploadEmiratesID {

    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    Image mImgPathFront, imgPathBack;
    Context context;
    public static final String MULTIPART_FORM_DATA = "multipart/form-data";

    String mRegID, mtoken, mDeviceID, mAuthToken, language, imsi, channel, osVersion, appVersion;
    File fileForDelete;

//    UploadImageInput mUploadImageInput;

    public UploadEmiratesID(BaseInterface baseInterface, Image imgPathFront, Image imgPathBack, Context mContext
    ) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.context = mContext;
        this.mImgPathFront = imgPathFront;
        this.mtoken = token;
        this.imgPathBack = imgPathBack;
        this.fileForDelete = fileForDelete;
        this.imsi = imsi;
        this.channel = channel;
        this.osVersion = osVersion;
        this.appVersion = appVersion;
//        this.mUploadImageInput = uploadImageInput;
        uploadFile(imgPathFront, imgPathBack);
    }

    private void uploadFile(Image fileUriFront, Image fileUriBack) {

        Log.v("regID", mRegID + "");

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(interceptor);
        OkHttpClient client = httpClient.readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build();
//        ttps://prod-myetisalatapp.etisalat.ae/DigitalApp/rest/re-registration/ocr/proccess-emirate-id

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MyEndPointsInterface.baseUrl).addConverterFactory(GsonConverterFactory.create())
                .client(client).build();
        MyEndPointsInterface apiService = retrofit.create(MyEndPointsInterface.class);
        /*File file = new File(fileUriFront.getImageFilePath());


        File file1 = null;
        try {
            // Assume block needs to be inside a Try/Catch block.
            String path = Environment.getExternalStorageDirectory().toString();
            OutputStream fOut = null;
            Integer counter = 0;
            file1 = new File(path, "eid2"+counter+".jpg"); // the File to save , append increasing numeric counter to prevent files from getting overwritten.
            fOut = new FileOutputStream(file1);

            Bitmap pictureBitmap = fileUriFront.getImageBitmap(); // obtaining the Bitmap
            pictureBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut); // saving the Bitmap to a file compressed as a JPEG with 85% compression rate
            fOut.flush(); // Not really required
            fOut.close(); // do not forget to close the stream
            RequestBody requestTestFile = RequestBody.create(MediaType.parse("image/jpeg"), file1);
            MultipartBody.Part body =
                    MultipartBody.Part.createFormData("imageFront", "imageFront", requestTestFile);


        } catch (Exception e) {
            e.printStackTrace();
        }*/


        Bitmap bitmap;
        RequestBody requestTestFile = RequestBody.create(MediaType.parse("image/jpeg"), convertImageToByteArray(fileUriFront));

//        RequestBody requestFile =
//                RequestBody.create(MediaType.parse("multipart/form-data"), requestTestFile);
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("imageFront", "imageFront", requestTestFile);

//        File fileBack = new File(fileUriBack.getImageFilePath());

//        if (fileUriBack != null) {
//            file = FileUtils.getFile(context, fileUriBack);
//        } else {
//            file = fileForDelete;
//        }

        File file2 = null;
        MultipartBody.Part bodyBack = null;
        try {
            // Assume block needs to be inside a Try/Catch block.
            /*String path = Environment.getExternalStorageDirectory().toString();
            OutputStream fOut = null;
            Integer counter = 0;
            file2 = new File(path, "eid3"+1+".jpg"); // the File to save , append increasing numeric counter to prevent files from getting overwritten.
            fOut = new FileOutputStream(file2);

            Bitmap pictureBitmap = fileUriBack.getImageBitmap(); // obtaining the Bitmap
            pictureBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut); // saving the Bitmap to a file compressed as a JPEG with 85% compression rate
            fOut.flush(); // Not really required
            fOut.close(); // do not forget to close the stream*/
            RequestBody requestTestFile2 = RequestBody.create(MediaType.parse("image/jpeg"), convertImageToByteArray(fileUriBack));
            bodyBack =
                    MultipartBody.Part.createFormData("imageBack", "imageBack", requestTestFile2);


        } catch (Exception e) {
            e.printStackTrace();
        }
//        RequestBody requestFile2 =
//                RequestBody.create(MediaType.parse("multipart/form-data"), fileBack);
        FileInputStream frontImgStream = null;
        FileInputStream backImageStream = null;


        HashMap<String, RequestBody> params = new HashMap<String, RequestBody>();
//        params.put("processImage",RequestBody.create(MediaType.parse(MULTIPART_FORM_DATA), "true"));
        params.put("format", RequestBody.create(MediaType.parse(MULTIPART_FORM_DATA), "jpeg"));
        Call<UploadEmiratesIdOutput> call = apiService.uploadEmiratesId(body,bodyBack, params);
        call.enqueue(new Callback<UploadEmiratesIdOutput>() {
            @Override
            public void onResponse(Call<UploadEmiratesIdOutput> call,
                                   Response<UploadEmiratesIdOutput> response) {
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.UploadImageInput);
//                if (response.code() == Constants.unAuthorizeToken) {
//                    try {
//                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                } else {
//                    try {
//                        if (response.body().getErrorResult() != null) {
//                            String urlForTag = call.request().url().toString();
//                            urlForTag = urlForTag.replace("\"https://uat-swypapp.etisalat.ae/Indy/rest/\"", "");
//                            urlForTag = urlForTag.replace("/", "_");
//                           // CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
//                        }
//                    } catch (Exception ex) {
//                        ex.printStackTrace();
//                    }
                mBaseInterface.onSucessListener(mBaseResponseModel);
                // }
            }

            @Override
            public void onFailure(Call<UploadEmiratesIdOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.UploadImageInput);

                mBaseInterface.onSucessListener(mBaseResponseModel);

            }
        });
    }

    protected synchronized byte[] convertImageToByteArray(Image imageObj) {
        // Delete the file so we can save it as a file buffer
        if (imageObj.getImageFileRep() == Image.ImageFileRep.FILE_STORED) {
            try {
                imageObj.imageReadFromFile();
                imageObj.imageDeleteFile();
            } catch (Exception e) {
                Log.e("SCRATCHCARD", "Exception", e);
                Toast.makeText(context, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

        // Set the MIME type of this Image object to TIFF
        imageObj.setImageMimeType(Image.ImageMimeType.MIMETYPE_JPEG);
        if (imageObj.getImageDPI() == null || imageObj.getImageDPI() < 96) {
            // setting the output DPI of the image
            imageObj.setImageDPI(500);
        }

        try {
            // Writing the bitmap into the internal imageFileBuffer
            imageObj.imageWriteToFileBuffer();
            imageObj.imageClearBitmap();
        } catch (Exception e) {
            Log.e("SCRATCHCARD", "Exception", e);
            Toast.makeText(context, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        // Getting buffer and clear internal one
        ByteBuffer bb = imageObj.getImageFileBuffer();
        byte[] processedFileBytes = new byte[bb.capacity()];
        bb.get(processedFileBytes);
        try {
            imageObj.imageClearFileBuffer();
        } catch (KmcException e) {
            Log.e("SCRATCHCARD", "Exception", e);
            Toast.makeText(context, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return processedFileBytes;
    }

    private RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(
                MediaType.parse(MULTIPART_FORM_DATA), descriptionString);
    }
    public MasterErrorResponse getResponseModel(String reponse) {
        MasterErrorResponse responseStr = null;
        Gson gson = new GsonBuilder().create();
        try {
            responseStr = gson.fromJson(reponse, MasterErrorResponse.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseStr;
    }
}


