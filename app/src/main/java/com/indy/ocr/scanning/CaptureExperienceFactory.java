package com.indy.ocr.scanning;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.kofax.kmc.ken.engines.data.DocumentDetectionSettings;
import com.kofax.kmc.kui.uicontrols.ImageCaptureView;
import com.kofax.kmc.kui.uicontrols.captureanimations.CaptureExperience;
import com.kofax.kmc.kui.uicontrols.captureanimations.CaptureMessage;
import com.kofax.kmc.kui.uicontrols.captureanimations.DocumentCaptureExperience;
import com.kofax.kmc.kui.uicontrols.captureanimations.DocumentCaptureExperienceCriteriaHolder;

import static android.content.Context.SENSOR_SERVICE;
//import com.kofax.kmc.kui.uicontrols.captureanimations.FixedAspectRatioCaptureExperience;
//import com.kofax.kmc.kui.uicontrols.captureanimations.FixedAspectRatioExperienceCriteriaHolder;


public class CaptureExperienceFactory {

    private static Context mContext;

    public static CaptureExperience createExperience(ImageCaptureView captureView,Context mContext) {
        CaptureExperienceFactory.mContext = mContext;
        return getGenericExperience(captureView);
    }

    //Generic experience set up for credit cards
    private static CaptureExperience getGenericExperience(ImageCaptureView captureView) {
        DocumentCaptureExperienceCriteriaHolder criteria = new DocumentCaptureExperienceCriteriaHolder();
        DocumentDetectionSettings detectionSettings = criteria.getDetectionSettings();
        detectionSettings.setTargetFrameAspectRatio(2.125f / 3.375);
        criteria.setPitchThresholdEnabled(false);
        criteria.setRollThresholdEnabled(false);
//        if (!sensorsAreAvailable(mContext)) {
//
//            criteria.setStabilityThresholdEnabled(false);
//
//
//        } else {

            criteria.setStabilityThresholdEnabled(true);
            criteria.setStabilityThreshold(50);

//        }
        DocumentCaptureExperience experience = new DocumentCaptureExperience(captureView, criteria);
        setGuidancePortrait(experience);

        return experience;
    }

    private static void setGuidancePortrait(DocumentCaptureExperience experience) {
        // Customizing the appearance for portrait mode
        CaptureMessage instructionMessage = experience.getUserInstructionMessage();
        instructionMessage.setOrientation(CaptureMessage.KUIMessageOrientation.PORTRAIT);
        experience.setUserInstructionMessage(instructionMessage);

        CaptureMessage centerMessage = experience.getCenterMessage();
        centerMessage.setOrientation(CaptureMessage.KUIMessageOrientation.PORTRAIT);
        experience.setCenterMessage(centerMessage);

        CaptureMessage zoomInMessage = experience.getZoomInMessage();
        zoomInMessage.setOrientation(CaptureMessage.KUIMessageOrientation.PORTRAIT);
        experience.setZoomInMessage(zoomInMessage);

        CaptureMessage zoomOutMessage = experience.getZoomOutMessage();
        zoomOutMessage.setOrientation(CaptureMessage.KUIMessageOrientation.PORTRAIT);
        experience.setZoomOutMessage(zoomOutMessage);

        CaptureMessage holdParallelMessage = experience.getHoldParallelMessage();
        holdParallelMessage.setOrientation(CaptureMessage.KUIMessageOrientation.PORTRAIT);
        experience.setHoldParallelMessage(holdParallelMessage);

        CaptureMessage rotateMessage = experience.getRotateMessage();
        rotateMessage.setOrientation(CaptureMessage.KUIMessageOrientation.PORTRAIT);
        experience.setRotateMessage(rotateMessage);

        CaptureMessage steadyMessage = experience.getHoldSteadyMessage();
        steadyMessage.setOrientation(CaptureMessage.KUIMessageOrientation.PORTRAIT);
        experience.setHoldSteadyMessage(steadyMessage);

        CaptureMessage capturedMessage = experience.getCapturedMessage();
        capturedMessage.setOrientation(CaptureMessage.KUIMessageOrientation.PORTRAIT);
        experience.setCapturedMessage(capturedMessage);

        CaptureMessage dismissMessage = experience.getTutorialDismissMessage();
        dismissMessage.setOrientation(CaptureMessage.KUIMessageOrientation.PORTRAIT);
        experience.setTutorialDismissMessage(dismissMessage);
    }
    private static boolean sensorsAreAvailable(Context context) {


        boolean sensorsAreAvailable = false;


        SensorEventListener listener = new SensorEventListener() {


            @Override


            public void onSensorChanged(SensorEvent event) {
            }


            @Override


            public void onAccuracyChanged(Sensor sensor, int accuracy) {
            }


        };


        SensorManager sensorManager = (SensorManager) context.getSystemService(SENSOR_SERVICE);


        Sensor rotationSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);


        if (rotationSensor != null && sensorManager.registerListener(listener, rotationSensor, SensorManager.SENSOR_DELAY_FASTEST)) {


            sensorsAreAvailable = true;


            sensorManager.unregisterListener(listener, rotationSensor);


        } else {


            rotationSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);


            Sensor geomagneticSensor = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);


            if (rotationSensor != null && sensorManager.registerListener(listener, rotationSensor, SensorManager.SENSOR_DELAY_FASTEST)


                    && geomagneticSensor != null && sensorManager.registerListener(listener, geomagneticSensor, SensorManager.SENSOR_DELAY_UI)) {


                sensorsAreAvailable = true;


                sensorManager.unregisterListener(listener, rotationSensor);


                sensorManager.unregisterListener(listener, geomagneticSensor);


            }


        }


        return sensorsAreAvailable;


    }
    //================================================================================
    // FarExperience - only SDK 3.3+
    //================================================================================
//    public static CaptureExperience createExperience(ImageCaptureView captureView) {
//        return getFarExperience(captureView);
//    }
//
//    //FarExperience should be set up for card capture by default
//    private static CaptureExperience getFarExperience(ImageCaptureView captureView) {
//        return new FixedAspectRatioCaptureExperience(captureView, new FixedAspectRatioExperienceCriteriaHolder());
//    }
}
