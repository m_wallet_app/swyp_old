package com.indy.ocr.scanning;

import com.kofax.kmc.ken.engines.data.Image;

/**
 * Created by Rupali on 24/07/17.
 */

public class Constants {

  public static final int CAPTURE_FRONT = 1;
  public static final int CAPTURE_BACK = 2;

  public static Image front_image = null;
  public static Image back_image = null;

  public static final String SIDE = "side";

}
