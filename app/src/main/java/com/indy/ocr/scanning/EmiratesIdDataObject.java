package com.indy.ocr.scanning;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mobile on 24/08/2017.
 */

public class EmiratesIdDataObject implements Parcelable{

    String fullName;
    String emiratesIdNumber;
    String cardNumber;
    String city;
    String dateOfBirth;
    String nationality;
    String gender;
    String lastNameLocal;
    String firstNameLocal;
    String fullNameLocal;
    String fullNameArabic;
    private String expiryDate;
    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmiratesIdNumber() {
        return emiratesIdNumber;
    }

    public void setEmiratesIdNumber(String emiratesIdNumber) {
        this.emiratesIdNumber = emiratesIdNumber;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }


    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(fullName);
        parcel.writeString(emiratesIdNumber);
        parcel.writeString(cardNumber);
        parcel.writeString(dateOfBirth);
        parcel.writeString(nationality);
        parcel.writeString(gender);
        parcel.writeString(expiryDate);
        parcel.writeString(fullNameArabic);
    }

    private EmiratesIdDataObject(Parcel in){
        fullName = in.readString();
        emiratesIdNumber = in.readString();
        cardNumber = in.readString();
        dateOfBirth = in.readString();
        nationality = in.readString();
        gender = in.readString();
        expiryDate = in.readString();
        fullNameArabic = in.readString();
    }

    public EmiratesIdDataObject() {
    }

    public String getLastNameLocal() {
        return lastNameLocal;
    }

    public void setLastNameLocal(String lastNameLocal) {
        this.lastNameLocal = lastNameLocal;
    }

    public String getFirstNameLocal() {
        return firstNameLocal;
    }

    public void setFirstNameLocal(String firstNameLocal) {
        this.firstNameLocal = firstNameLocal;
    }

    public String getFullNameArabic() {
        return fullNameArabic;
    }

    public void setFullNameArabic(String fullNameArabic) {
        this.fullNameArabic = fullNameArabic;
    }

    public String getFullNameLocal() {
        return fullNameLocal;
    }

    public void setFullNameLocal(String fullNameLocal) {
        this.fullNameLocal = fullNameLocal;
    }
}
