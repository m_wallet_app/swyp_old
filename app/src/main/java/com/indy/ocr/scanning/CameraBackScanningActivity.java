package com.indy.ocr.scanning;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.indy.R;
import com.kofax.kmc.ken.engines.ImageProcessor;
import com.kofax.kmc.ken.engines.data.DocumentDetectionSettings;
import com.kofax.kmc.ken.engines.data.Image;
import com.kofax.kmc.ken.engines.data.ImagePerfectionProfile;
import com.kofax.kmc.kui.uicontrols.ImageCaptureView;
import com.kofax.kmc.kui.uicontrols.ImageCapturedEvent;
import com.kofax.kmc.kui.uicontrols.ImageCapturedListener;
import com.kofax.kmc.kui.uicontrols.ImgReviewEditCntrl;
import com.kofax.kmc.kui.uicontrols.captureanimations.CaptureMessage;
import com.kofax.kmc.kui.uicontrols.captureanimations.DocumentCaptureExperience;
import com.kofax.kmc.kui.uicontrols.captureanimations.DocumentCaptureExperienceCriteriaHolder;
import com.kofax.kmc.kut.utilities.error.KmcException;

public class CameraBackScanningActivity extends AppCompatActivity implements ImageCapturedListener, ImageProcessor.ImageOutListener {

    private ImageCaptureView mImageCaptureView;
    private ImgReviewEditCntrl mImgReviewEditCntrl;
    private DocumentCaptureExperience mExperience;
    private ImageButton mForceCapture;
    private Image returnImage;
    private Intent intent;
    private static final String[] PERMISSIONS = {
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };

    private final PermissionsManager mPermissionsManager = new PermissionsManager(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        intent = getIntent();

        mImageCaptureView = (ImageCaptureView) findViewById(R.id.view_capture);

        mImageCaptureView.setUseVideoFrame(false);

        mExperience = new DocumentCaptureExperience(mImageCaptureView);
        DocumentCaptureExperienceCriteriaHolder criteriaHolder = new DocumentCaptureExperienceCriteriaHolder();

//        criteriaHolder.setDetectionSettings(createDocumentDetectionSettings());
        criteriaHolder.setPitchThresholdEnabled(false);
        criteriaHolder.setRollThresholdEnabled(false);
        if (!sensorsAreAvailable(this)) {
            criteriaHolder.setStabilityThresholdEnabled(false);
            criteriaHolder.setStabilityThreshold(50);
        } else {
            criteriaHolder.setStabilityThresholdEnabled(true);
            criteriaHolder.setStabilityThreshold(50);
        }

        criteriaHolder.setFocusEnabled(true);
        criteriaHolder.setRefocusEnabled(true);
        criteriaHolder.setOrientationEnabled(true);

        mExperience.setGuidanceFrameColor(getResources().getColor(R.color.red_color));
        mExperience.setSteadyGuidanceFrameColor(getResources().getColor(R.color.red_color));
        mExperience.setCaptureCriteria(criteriaHolder);
        mExperience.addOnImageCapturedListener(CameraBackScanningActivity.this);

        ((ImageButton) findViewById(R.id.btn_force_capture)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mImageCaptureView.forceTakePicture();
            }
        });

        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Semibold.ttf");

        CaptureMessage captureMessage = new CaptureMessage();
        captureMessage.setTextColor(getResources().getColor(R.color.color_white));
        captureMessage.setTextSize(16);
        captureMessage.setTypeface(tf);
        if (intent.getIntExtra(Constants.SIDE, 0) == Constants.CAPTURE_FRONT) {
            captureMessage.setMessage(getString(R.string.emirate_id_scan_front_message));

            mExperience.setUserInstructionMessage(captureMessage);

        } else if (intent.getIntExtra(Constants.SIDE, 0) == Constants.CAPTURE_BACK) {
            captureMessage.setMessage(getString(R.string.emirate_id_scan_back_message));
            mExperience.setUserInstructionMessage(captureMessage);
        }
        ((Button) findViewById(R.id.bt_enter_manually)).setVisibility(View.GONE);
        requestPermissionsForScanning();

    }

    private void requestPermissionsForScanning() {

        if (!mPermissionsManager.isGranted(PERMISSIONS)) {
            try {
                boolean isNeverAllowed = false;
                if (!shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                    isNeverAllowed = true;
                } else if (shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    isNeverAllowed = true;
                } else if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    isNeverAllowed = true;
                }

                if (isNeverAllowed) {
                    //create popup to explain you cannot go further

                    finish();
                } else {
                    mPermissionsManager.request(PERMISSIONS);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            mExperience.takePicture();
        }
    }

    @Override
    public void imageOut(ImageProcessor.ImageOutEvent event) {
        if (event != null) {
            if (event.getImage() != null) {
                if (intent.getIntExtra(Constants.SIDE, 0) == Constants.CAPTURE_BACK) {
                    Constants.back_image = event.getImage();
                    Constants.back_image.setImageMimeType(Image.ImageMimeType.MIMETYPE_TIFF);
                    if (Constants.back_image != null) {
                        setResult(100);
                    } else {
                        setResult(200);
                    }

                    finish();
                }
            }

        } else {
            onBackPressed();
        }


    }

    @Override
    public void onImageCaptured(ImageCapturedEvent imageCapturedEvent) {
        processImage(imageCapturedEvent.getImage());
        if (intent.getIntExtra(Constants.SIDE, 0) == Constants.CAPTURE_FRONT) {

        } else if (intent.getIntExtra(Constants.SIDE, 0) == Constants.CAPTURE_BACK) {

        }
    }


    private final ImageCapturedListener mImageCapturedListener = new ImageCapturedListener() {
        @Override
        public void onImageCaptured(final ImageCapturedEvent event) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //reviewImage(event.getImage());
                    //cleanup image here

                }
            });
        }
    };

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }

    private DocumentDetectionSettings createDocumentDetectionSettings() {
        DocumentDetectionSettings settings = new DocumentDetectionSettings();
        settings.setEdgeDetection(DocumentDetectionSettings.DocumentEdgeDetection.ISG);
        settings.setLongEdgeThreshold(.9);
        settings.setShortEdgeThreshold(.9);
        settings.setTargetFramePaddingPercent(5);
        settings.setTargetFrameAspectRatio(.62);


        settings.setMinFillFraction(0.7);
        settings.setMaxFillFraction(1.1);

        return settings;
    }

    private void processImage(Image image) {
        ImageProcessor imageProcessor = new ImageProcessor();
        ImagePerfectionProfile profile = new ImagePerfectionProfile();

//        //changed according to kofax
//        profile.setIpOperations("_DeviceType_2_DoBinarization__DoCropCorrection__DoSkewCorrectionAlt__Do90DegreeRotation_4_LoadInlineSetting_[CSkewDetect.convert_to_gray.Bool=1]_LoadInlineSetting_[CSkewDetect.scale_image_down.Bool=1]_LoadInlineSetting_[CSkewDetect.scale_down_factor.Int=80]_LoadInlineSetting_[CSkewDetect.document_size.Int=2]_LoadInlineSetting_[CSkewDetect.correct_illumination.Bool=0]_DoScaleImageToDPI_300");
        //original
        profile.setIpOperations("DeviceType_2__Do90DegreeRotation_4__DoCropCorrection__DoScaleImageToDPI_500_DoSkewCorrectionPage__DocDimLarge_3.375_DocDimSmall_2.125_LoadSetting_<Property Name=\"CSkewDetect.correct_illumination.Bool\" Value=\"0\"/>");
        profile.setUseTargetFrameCrop(ImagePerfectionProfile.UseTargetFrameCrop.ON);
        //changed according to kofax 2
//        profile.setIpOperations("_DeviceType_2_DoCropCorrection__DoSkewCorrectionAlt__Do90DegreeRotation_4_LoadInlineSetting_[CSkewDetect.scale_image_down.Bool=1]_LoadInlineSetting_[CSkewDetect.scale_down_factor.Int=80]_LoadInlineSetting_[CSkewDetect.document_size.Int=2]_LoadInlineSetting_[CSkewDetect.correct_illumination.Bool=0]_DoScaleImageToDPI_300");
//        String iPOperatiosnString  = "_DeviceType_0_DoNoPageDetection_\n" +
//                "_LoadInlineSetting_[CSkewDetect.document_smaller_dim.double=2.125]\n" +
//                "_LoadInlineSetting_[CSkewDetect.document_larger_dim.double=3.375]\n" +
//                "_LoadInlineSetting_[CSkewDetect.do_scale_to_dpi.Bool=1]\n" +
//                "_LoadInlineSetting_[CSkewDetect.scaled_image_dpi.Int=300]\n" +
//                "_LoadInlineSetting_[CSkewDetect.correct_illumination.Bool=0]\n" +
//                "_LoadInlineSetting_[CSkewDetect.Remove_Shadows=0]\n" +
//                "_LoadInlineSetting_[CSkewDetect.color_stats_error_sum_thr_white_bkg=36]\n" +
//                "_LoadInlineSetting_[CSkewDetect.color_stats_error_sum_thr_black_bkg=36]\n" +
//                "_LoadInlineSetting_[CSkewDetect.step_white_bkg=1]\n" +
//                "_LoadInlineSetting_[CSkewDetect.step_black_bkg=1]\n" +
//                "_LoadInlineSetting_[CSkewDetect.bright_window=255]\n" +
//                "_LoadInlineSetting_[CSkewDetect.side_angle_diff_thr.double=90.0]\n" +
//                "_LoadInlineSetting_[EdgeCleanup.enable=0]\n" +
//                "_LoadInlineSetting_[CZoneExtract.PerformRecognition=1]\n" +
//                "_LoadInlineSetting_[CZoneExtract.Enable=1]\n" +
//                "_LoadInlineSetting_[CZoneExtract.UseFieldType=2]\n" +
//                "_LoadInlineSetting_[CZoneExtract.NumZones=2]\n" +
//                "_LoadInlineSetting_[CZoneExtract.AspectRatio.Double=1.584112]\n" +
//                "_LoadInlineSetting_[CZoneExtract.LineLabels.00.00.String=CardNumber]\n" +
//                "_LoadSetting_<Property Name=\"CZoneExtract.LineLabelsTemplate.00.00.String\" Value=\"1:9999[0-9 ]\" />\n" +
//                "_LoadInlineSetting_[CZoneExtract.Max_bbX1.00.Int=543]\n" +
//                "_LoadInlineSetting_[CZoneExtract.Max_bbY1.00.Int=5000]\n" +
//                "_LoadInlineSetting_[CZoneExtract.Max_bbX2.00.Int=9500]\n" +
//                "_LoadInlineSetting_[CZoneExtract.Max_bbY2.00.Int=6954]\n" +
//                "_LoadInlineSetting_[CZoneExtract.LineHeight.00.00.Int=961]\n" +
//                "_LoadInlineSetting_[CZoneExtract.LineWidth.00.00.Int=8396]\n" +
//                "_LoadInlineSetting_[CZoneExtract.LineMedianX1.00.00.Int=1058]\n" +
//                "_LoadInlineSetting_[CZoneExtract.LineMedianX2.00.00.Int=9010]\n" +
//                "_LoadInlineSetting_[CZoneExtract.LineMedianY1.00.00.Int=5504]\n" +
//                "_LoadInlineSetting_[CZoneExtract.LineMedianY2.00.00.Int=6478]\n" +
//                "_LoadInlineSetting_[CZoneExtract.Binarization_Method.00.Int=3]\n" +
//                "_LoadInlineSetting_[CZoneExtract.LineLabels.01.00.String=ExpirationDate]\n" +
//                "_LoadSetting_<Property Name=\"CZoneExtract.LineLabelsTemplate.01.00.String\" Value=\"1:9999[0-9 /-*]\" />\n" +
//                "_LoadInlineSetting_[CZoneExtract.ValidExpYearStart.Int=1980]\n" +
//                "_LoadInlineSetting_[CZoneExtract.ValidExpYearStop.Int=2027]\n" +
//                "_LoadInlineSetting_[CZoneExtract.CC_CheckDigitMatch_TryTwoWeakest=1]\n" +
//                "_LoadInlineSetting_[CZoneExtract.CC_CheckDigitMatch_TryThirdChoice=1]";
//        profile.setIpOperations(iPOperatiosnString);
        // Set the image perfection profile of the image processor
        imageProcessor.setImagePerfectionProfile(profile);
        imageProcessor.addImageOutEventListener(this);
        try {
            imageProcessor.processImage(image);
        } catch (KmcException e) {
            Log.e("IDCapture", "Exception", e);
            Toast.makeText(this, "Unable to process the captured image", Toast.LENGTH_LONG).show();
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] results) {
        if (!mPermissionsManager.isGranted(PERMISSIONS)) {
            requestPermissionsForScanning();
        } else {

            mExperience.takePicture();
//            new AlertDialog.Builder(this)
//                    .setMessage("capture documents")
//                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            finish();
//                        }
//                    })
//                    .setCancelable(false)
//                    .show();
        }
    }


    private void sendRequest() {

        Constants.front_image.setImageDPI(500);
        Constants.back_image.setImageDPI(500);
        Toast.makeText(this, "Received both images", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 100:
                if (resultCode == 100)
                    sendRequest();
                else {
                    Toast.makeText(this, "error in capture", Toast.LENGTH_LONG).show();
                    finish();
                }
                break;
        }
    }

    private boolean sensorsAreAvailable(Context context) {

        boolean sensorsAreAvailable = false;
        SensorEventListener listener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {
            }
        };
        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        Sensor rotationSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        if (rotationSensor != null && sensorManager.registerListener(listener, rotationSensor, SensorManager.SENSOR_DELAY_FASTEST)) {
            sensorsAreAvailable = true;
            sensorManager.unregisterListener(listener, rotationSensor);
        } else {
            rotationSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            Sensor geomagneticSensor = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
            if (rotationSensor != null && sensorManager.registerListener(listener, rotationSensor, SensorManager.SENSOR_DELAY_FASTEST)
                    && geomagneticSensor != null && sensorManager.registerListener(listener, geomagneticSensor, SensorManager.SENSOR_DELAY_UI)) {
                sensorsAreAvailable = true;
                sensorManager.unregisterListener(listener, rotationSensor);
                sensorManager.unregisterListener(listener, geomagneticSensor);
            }
        }
        return sensorsAreAvailable;


    }

}
