package com.indy.ocr.scanning;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.indy.R;
import com.indy.utils.CommonMethods;
import com.indy.views.activites.MasterActivity;
import com.kofax.kmc.ken.engines.ImageProcessor;
import com.kofax.kmc.ken.engines.data.DocumentDetectionSettings;
import com.kofax.kmc.ken.engines.data.Image;
import com.kofax.kmc.ken.engines.data.ImagePerfectionProfile;
import com.kofax.kmc.kui.uicontrols.ImageCaptureView;
import com.kofax.kmc.kui.uicontrols.ImageCapturedEvent;
import com.kofax.kmc.kui.uicontrols.ImageCapturedListener;
import com.kofax.kmc.kui.uicontrols.ImgReviewEditCntrl;
import com.kofax.kmc.kui.uicontrols.captureanimations.CaptureMessage;
import com.kofax.kmc.kui.uicontrols.captureanimations.DocumentCaptureExperience;
import com.kofax.kmc.kui.uicontrols.captureanimations.DocumentCaptureExperienceCriteriaHolder;
import com.kofax.kmc.kut.utilities.error.KmcException;

import static com.indy.utils.ConstantUtils.TAGGING_id_scan_back;
import static com.indy.utils.ConstantUtils.TAGGING_id_scan_front;

public class CameraActivity extends MasterActivity implements ImageCapturedListener, ImageProcessor.ImageOutListener {

    private ImageCaptureView mImageCaptureView;
    private ImgReviewEditCntrl mImgReviewEditCntrl;
    private DocumentCaptureExperience mExperience;
    private ImageButton mForceCapture;
    private Button bt_enterDetailsManually;
    private Image returnImage;
    private Intent intent;
    private static final String[] PERMISSIONS = {
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };

    private final PermissionsManager mPermissionsManager = new PermissionsManager(this);
    private TextView title;
    private DocumentCaptureExperienceCriteriaHolder criteriaHolder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        intent = getIntent();
        title = (TextView) findViewById(R.id.titleTxt);
        title.setText(R.string.id_scan);
        mImageCaptureView = (ImageCaptureView) findViewById(R.id.view_capture);

        mImageCaptureView.setUseVideoFrame(false);

        mExperience = new DocumentCaptureExperience(mImageCaptureView);

        criteriaHolder = new DocumentCaptureExperienceCriteriaHolder();
        criteriaHolder.setDetectionSettings(createDocumentDetectionSettings());
        criteriaHolder.setPitchThresholdEnabled(false);
        criteriaHolder.setRollThresholdEnabled(false);
        criteriaHolder.setStabilityThreshold(50);
        criteriaHolder.setFocusEnabled(true);
        criteriaHolder.setRefocusEnabled(true);
        criteriaHolder.setOrientationEnabled(true);
        if (!sensorsAreAvailable(this)) {
            criteriaHolder.setHoldSteadyDelay(0.3);
            criteriaHolder.setStabilityThresholdEnabled(false);
        } else {
            criteriaHolder.setStabilityThresholdEnabled(true);
        }
//        criteriaHolder.setDetectionSettings(createDocumentDetectionSettings());
//        criteriaHolder.setPitchThresholdEnabled(true);
//        criteriaHolder.setRollThresholdEnabled(true);

        //criteriaHolder.setRollThreshold(15); //new code
//        if (!sensorsAreAvailable(this)) {
//
//            criteriaHolder.setStabilityThresholdEnabled(false);
//
//
//        } else {
//
//            criteriaHolder.setStabilityThresholdEnabled(true);
//            criteriaHolder.setStabilityThreshold(50);
//
//        }
//        criteriaHolder.setFocusEnabled(true);
//        criteriaHolder.setPitchThreshold(15);
//        criteriaHolder.setRollThreshold(15);
//        criteriaHolder.setOrientationEnabled(true);
        //  criteriaHolder.setFocusEnabled(true);


        mExperience.setGuidanceFrameColor(getResources().getColor(R.color.red_color));
        mExperience.setSteadyGuidanceFrameColor(getResources().getColor(R.color.red_color));

        mExperience.setCaptureCriteria(criteriaHolder);
        mExperience.addOnImageCapturedListener(CameraActivity.this);

        ((ImageButton) findViewById(R.id.btn_force_capture)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mImageCaptureView.forceTakePicture();
            }
        });

        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Semibold.ttf");

        CaptureMessage captureMessage = new CaptureMessage();
        captureMessage.setTextColor(getResources().getColor(R.color.color_white));
        captureMessage.setTextSize(16);
        captureMessage.setTypeface(tf);
        if (intent.getIntExtra(Constants.SIDE, 0) == Constants.CAPTURE_FRONT) {
            captureMessage.setMessage(getString(R.string.emirate_id_scan_front_message));

            mExperience.setUserInstructionMessage(captureMessage);


        } else if (intent.getIntExtra(Constants.SIDE, 0) == Constants.CAPTURE_BACK) {
            captureMessage.setMessage(getString(R.string.emirate_id_scan_back_message));
            mExperience.setUserInstructionMessage(captureMessage);
        }
        bt_enterDetailsManually = (Button) findViewById(R.id.bt_enter_manually);
        bt_enterDetailsManually.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                    replaceFragmnet(new ManualEmiratesIDVerificationFragment(),R.id.frameLayout,true);
                setResult(120);
                finish();
            }
        });
        requestPermissionsForScanning();

    }

    private void requestPermissionsForScanning() {

        if (!mPermissionsManager.isGranted(PERMISSIONS)) {
            try {
//                boolean isNeverAllowed = false;
//                if (!shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
//                    isNeverAllowed = true;
//                } else if (shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {
//                    isNeverAllowed = true;
//                } else if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
//                    isNeverAllowed = true;
//                }
//
//                if (isNeverAllowed) {
//                    //create popup to explain you cannot go further
//
//                    finish();
//                } else {
                mPermissionsManager.request(PERMISSIONS);
//                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            mExperience.takePicture();
        }
    }

    private boolean sensorsAreAvailable(Context context) {

        boolean sensorsAreAvailable = false;
        SensorEventListener listener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {
            }
        };
        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        Sensor rotationSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        if (rotationSensor != null && sensorManager.registerListener(listener, rotationSensor, SensorManager.SENSOR_DELAY_FASTEST)) {
            sensorsAreAvailable = true;
            sensorManager.unregisterListener(listener, rotationSensor);
        } else {
            rotationSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            Sensor geomagneticSensor = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
            if (rotationSensor != null && sensorManager.registerListener(listener, rotationSensor, SensorManager.SENSOR_DELAY_FASTEST)
                    && geomagneticSensor != null && sensorManager.registerListener(listener, geomagneticSensor, SensorManager.SENSOR_DELAY_UI)) {
                sensorsAreAvailable = true;
                sensorManager.unregisterListener(listener, rotationSensor);
                sensorManager.unregisterListener(listener, geomagneticSensor);
            }
        }
        return sensorsAreAvailable;
    }

    @Override
    public void imageOut(ImageProcessor.ImageOutEvent event) {
        if (event != null) {
            if (event.getImage() != null) {

                if (event.getImage() != null) {
                    if (intent.getIntExtra(Constants.SIDE, 0) == Constants.CAPTURE_FRONT) {
                        Constants.front_image = event.getImage();
                        Constants.front_image.setImageDPI(500);
                        Constants.front_image.setImageMimeType(Image.ImageMimeType.MIMETYPE_JPEG);
                        setResult(100);
                        finish();
                    } else if (intent.getIntExtra(Constants.SIDE, 0) == Constants.CAPTURE_BACK) {
                        Constants.back_image = event.getImage();
                        Constants.back_image.setImageDPI(500);
                        Constants.back_image.setImageMimeType(Image.ImageMimeType.MIMETYPE_JPEG);

                        if (Constants.back_image != null) {
                            setResult(100);
                        } else {
                            setResult(RESULT_CANCELED);
                        }
                        finish();
                    }
                }
            } else {
                onBackPressed();
            }
        }

    }

    @Override
    public void onImageCaptured(ImageCapturedEvent imageCapturedEvent) {
        processImage(imageCapturedEvent.getImage());
        if (getmFirebaseAnalytics() != null) {
            Log.d("getmFirebaseAnalytics()", "Not NULL");
            if (intent.getIntExtra(Constants.SIDE, 0) == Constants.CAPTURE_FRONT) {
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), TAGGING_id_scan_front);
            } else if (intent.getIntExtra(Constants.SIDE, 0) == Constants.CAPTURE_BACK) {
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), TAGGING_id_scan_back);
            }
        }
        else{
            Log.d("getmFirebaseAnalytics()", "is NULL");
        }
    }


    private final ImageCapturedListener mImageCapturedListener = new ImageCapturedListener() {
        @Override
        public void onImageCaptured(final ImageCapturedEvent event) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //reviewImage(event.getImage());
                    //cleanup image here

                }
            });
        }
    };

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }

    private DocumentDetectionSettings createDocumentDetectionSettings() {
        DocumentDetectionSettings settings = new DocumentDetectionSettings();
        settings.setEdgeDetection(DocumentDetectionSettings.DocumentEdgeDetection.ISG);
        settings.setLongEdgeThreshold(.9);
        settings.setShortEdgeThreshold(.9);
        settings.setTargetFramePaddingPercent(5);
        settings.setTargetFrameAspectRatio(.62);


        settings.setMinFillFraction(0.7);
        settings.setMaxFillFraction(1.1);

        return settings;
    }

    private void processImage(Image image) {
        ImageProcessor imageProcessor = new ImageProcessor();
        ImagePerfectionProfile profile = new ImagePerfectionProfile();

        profile.setIpOperations("_DeviceType_2__Do90DegreeRotation_4__DoCropCorrection__DoScaleImageToDPI_300_DoSkewCorrectionPage__DocDimLarge_3.375_DocDimSmall_2.125_LoadSetting_<Property Name=\"CSkewDetect.correct_illumination.Bool\" Value=\"0\" />");
        profile.setUseTargetFrameCrop(ImagePerfectionProfile.UseTargetFrameCrop.ON);

        // Set the image perfection profile of the image processor
        imageProcessor.setImagePerfectionProfile(profile);
        imageProcessor.addImageOutEventListener(this);

        try {
//            imageProcessor.addAnalysisCompleteEventListener(this);
            imageProcessor.processImage(image);
        } catch (KmcException e) {
            Log.e("IDCapture", "Exception", e);
            Toast.makeText(this, "Unable to process the captured image", Toast.LENGTH_LONG).show();
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] results) {
        if (!mPermissionsManager.isGranted(PERMISSIONS)) {
            requestPermissionsForScanning();
        } else {
            criteriaHolder = new DocumentCaptureExperienceCriteriaHolder();
            criteriaHolder.setDetectionSettings(createDocumentDetectionSettings());
            criteriaHolder.setPitchThresholdEnabled(false);
            criteriaHolder.setRollThresholdEnabled(false);
            criteriaHolder.setStabilityThreshold(50);
            criteriaHolder.setFocusEnabled(true);
            criteriaHolder.setRefocusEnabled(true);
            criteriaHolder.setOrientationEnabled(true);
            if (!sensorsAreAvailable(this)) {
                criteriaHolder.setStabilityThresholdEnabled(false);
            } else {
                criteriaHolder.setStabilityThresholdEnabled(true);
            }
            mExperience.takePicture();
//            new AlertDialog.Builder(this)
//                    .setMessage("capture documents")
//                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            finish();
//                        }
//                    })
//                    .setCancelable(false)
//                    .show();
        }
    }


    private void sendRequest() {

        Constants.front_image.setImageDPI(500);
        Constants.back_image.setImageDPI(500);

        Constants.front_image.setImageMimeType(Image.ImageMimeType.MIMETYPE_JPEG);
        Constants.back_image.setImageMimeType(Image.ImageMimeType.MIMETYPE_JPEG);
//        Toast.makeText(this, "Received both images", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 100:
                if (resultCode == 100)
                    sendRequest();
                else {
//                    Toast.makeText(this, "error in capture", Toast.LENGTH_LONG).show();
                    finish();
                }
                break;
        }
    }


}
