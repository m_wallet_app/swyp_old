package com.indy.views.activites;

import android.content.Intent;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.adapters.IntroFragmentAdapter;
import com.indy.adapters.StaticPackagesListAdapter;
import com.indy.controls.AdjustEvents;
import com.indy.controls.ServiceUtils;
import com.indy.models.getStaticBundles.BundleList;
import com.indy.models.getStaticBundles.StaticBundleOutput;
import com.indy.models.packages.UsagePackageList;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterInputResponse;
import com.indy.services.GetStaticPackageListService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.utils.SharedPrefrencesManger;
import com.indy.views.fragments.tutorial.FiristTutorialFragmnet;
import com.indy.views.fragments.tutorial.FourthTutorialFragmnet;
import com.indy.views.fragments.tutorial.SecondTutorialFragmnet;
import com.indy.views.fragments.tutorial.ThirdTutorialFragmnet;
import com.indy.views.fragments.tutorial.ViewPagerParallax;
import com.indy.views.fragments.tutorial.adapterViewPager;
import com.indy.views.fragments.utils.ErrorFragment;
import com.viewpagerindicator.PageIndicator;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.indy.utils.ConstantUtils.TAGGING_view_package_signup_button;

/**
 * Created by emad on 8/28/16.
 */
public class TutorialActivity extends MasterActivity {
    public LinearLayout skipBtn;
    IntroFragmentAdapter mAdapter;
    ViewPager mPager;
    PageIndicator mIndicator;

    public static Button bt_signUp;
    public static TextView tv_title, tv_content;
    public static LinearLayout ll_fourth, tv_viewPackages;

    private ViewPager myViewPager;
    private ImageView bagID, keyIcon, coinID, scaleID, packagesId;
    private ImageView fisritBullet, seconfBullet, thirdbullet, fourtBullet;
    public static List<ImageView> imageViews = new ArrayList<>();
    private TextView[] dots;
    private int[] layouts;
    static int currentPage;
    private LinearLayout dotsLayout;
    public static Timer timer;
    public static TimerTask timerTask;
    public static Handler handler = null;
    public static Runnable update;
    FrameLayout frameLayout;
    public static int timerTime = 5000;
    Runnable runnable;
    int count = 0;
    Handler mHandler = new Handler();
    public Thread background;
    public boolean shouldContinue = true;
    private boolean isInTouchMode;
    private RelativeLayout titleLayout, rl_root;
    private String currentLanguage;
    private FragmentTransaction transaction;
    public static TutorialActivity tutorialActivityInstance;
    public long currentThreadId = 0;


    //------------PACKAGES BOTTOMSHEET-----//////////
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mGridLayoutManger;
    private List<BundleList> usagePackageList;
    private MasterInputResponse masterInputResponse;
    private StaticBundleOutput usagePackagesoutput;
    private StaticPackagesListAdapter packagesListAdapter;
    private UsagePackageList emptyUsagePackageListlItem;
    private Button signupBtn;
    TutorialActivity tutorialActivity;
    private Button continueBtn;
    private LinearLayout viewPackagesBtn;
    public static BottomSheetBehavior behavior;
    private Button bt_buyNow;
    private TextView tv_swyp_description;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // remove title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        setContentView(R.layout.activty_tutorial);
        handler = new Handler();
        dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);
        titleLayout = (RelativeLayout) findViewById(R.id.titleLayout);
        rl_root = (RelativeLayout) findViewById(R.id.rl_root);
//        signupBtn = (Button) findViewById(R.id.btn_rechargeCard);
//        dotsLayout.bringToFront();
        bt_buyNow = (Button) findViewById(R.id.btn_rechargeCard);
        initUI();
//        initUi();
        initParallex();
        tutorialActivityInstance = this;
        initStaticPackagesUI();
//        myViewPager.setOnPageChangeListener(null);

        //-----------------BOTTOM SHEET DIALOGUE ---------------//

        continueBtn = (Button) findViewById(R.id.loginBtn);

        viewPackagesBtn = (LinearLayout) findViewById(R.id.viewPackagesId);
        viewPackagesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shouldContinue = false;
                rl_root.setVisibility(View.GONE);
                showBottomSheetSetup();
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                Bundle bundle = new Bundle();

                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_view_packages_tutorial);
                bundle.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_view_packages_tutorial);
                mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_view_packages_tutorial, bundle);
                onConsumeService();
            }
        });

        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                logEvent(AdjustEvents.CLICKS_SIGN_UP_OR_SKIP, new Bundle());
                sharedPrefrencesManger.setIsFiristTime(false);
                onSkip();

                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), TAGGING_view_package_signup_button);

            }
        });

        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinateLayoutID);
        View bottomSheet = coordinatorLayout.findViewById(R.id.bottom_sheet_static_packages_view);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    rl_root.setVisibility(View.VISIBLE);

                    hideBottomSheetSetup();
                } else if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                    showBottomSheetSetup();
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });

        playIntroVideo();

        bt_buyNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPrefrencesManger.setIsFiristTime(false);
                onClickBuyNow();
            }
        });
        //-----------------BOTTOM SHEET DIALOGUE ENDS--------------//
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

    }


    private void playIntroVideo() {
        try {
            Uri video;
            if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP && android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                video = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.onboarding_video);

            } else {
                video = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.onboarding_video);
            }

            final VideoView videoHolder = (VideoView) findViewById(R.id.myvideoview);
            Log.v("video path : ", video + " ");
            videoHolder.setVideoURI(video);
            videoHolder.setFitsSystemWindows(true);
            videoHolder.setActivated(true);
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int width = size.x;
            int height = size.y;
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) videoHolder.getLayoutParams();
            params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            params.width = width;
            params.height = height;
//            params.addRule(RelativeLayout.LEFT_OF, R.id.id_to_be_left_of);
            videoHolder.setLayoutParams(params);
//            videoHolder.setLayoutParams(new RelativeLayout.LayoutParams(width, height));

            getWindow().setFormat(PixelFormat.TRANSLUCENT);
            View decorView = getWindow().getDecorView();
            // Hide the status bar.
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
            videoHolder.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
//                    mp.setVolume(0, 0);
//                    finish();
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int width = size.x;
                    int height = size.y;
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) videoHolder.getLayoutParams();
                    params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
                    params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    params.width = width;
                    params.height = height;
                    videoHolder.setLayoutParams(params);
                    videoHolder.start();

                    getWindow().setFormat(PixelFormat.TRANSLUCENT);
                    View decorView = getWindow().getDecorView();
                    // Hide the status bar.
                    int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
                    decorView.setSystemUiVisibility(uiOptions);
                }

            });


            videoHolder.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
//                    mp.setLooping(true);
                    getWindow().setFormat(PixelFormat.TRANSLUCENT);
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int width = size.x;
                    int height = size.y;
                    videoHolder.setLayoutParams(new RelativeLayout.LayoutParams(width, height));
                    videoHolder.start();
                    getWindow().setFormat(PixelFormat.TRANSLUCENT);
                    View decorView = getWindow().getDecorView();
                    // Hide the status bar.
                    int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
                    decorView.setSystemUiVisibility(uiOptions);
//                    mp.setVolume(0, 0);

                }
            });


        } catch (Exception ex) {
            // jump();
        }
    }

    //    @Override
    public void initUI() {
        super.initUI();
        skipBtn = (LinearLayout) findViewById(R.id.skipBtn);
        fisritBullet = (ImageView) findViewById(R.id.fBullet);
        seconfBullet = (ImageView) findViewById(R.id.sBullet);
        thirdbullet = (ImageView) findViewById(R.id.thirdBullet);
        fourtBullet = (ImageView) findViewById(R.id.fourthBullet);
        ll_fourth = (LinearLayout) findViewById(R.id.ll_fourth);
        tv_content = (TextView) findViewById(R.id.tv_contentTxt);
        tv_swyp_description = (TextView) findViewById(R.id.tv_swyp_description);
        if (sharedPrefrencesManger.isVATEnabled()) {
            Double price = Double.parseDouble(sharedPrefrencesManger.getAfterLoginPrice());
            tv_swyp_description.setText(String.format(getString(R.string.is_the_mobile_service_for_new_generation), String.valueOf(price + (price * 0.05))));

        } else {
            tv_swyp_description.setText(String.format(getString(R.string.is_the_mobile_service_for_new_generation), sharedPrefrencesManger.getAfterLoginPrice()));
        }
        tv_title = (TextView) findViewById(R.id.tv_titleTxt);
        tv_viewPackages = (LinearLayout) findViewById(R.id.viewPackagesId);
        currentLanguage = sharedPrefrencesManger.getLanguage();
        imageViews.add(fisritBullet);
        imageViews.add(seconfBullet);
        imageViews.add(thirdbullet);
        imageViews.add(fourtBullet);

        ll_fourth.setVisibility(View.INVISIBLE);
        bt_signUp = (Button) findViewById(R.id.loginBtn);
        titleLayout.bringToFront();
        shouldContinue = true;
        isInTouchMode = false;
        skipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                finish();
                onSkip();
                Bundle bundle = new Bundle();


                if (tv_title.getText().toString().equalsIgnoreCase(getString(R.string.enjoy_str))) {
                    bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_skip_tutorial_1);
                    bundle.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_skip_tutorial_1);
                    mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_skip_tutorial_1, bundle);
                } else if (tv_title.getText().toString().equalsIgnoreCase(getString(R.string.enjoy_rewards))) {
                    bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_skip_tutorial_2);
                    bundle.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_skip_tutorial_2);
                    mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_skip_tutorial_2, bundle);
                } else if (tv_title.getText().toString().equalsIgnoreCase(getString(R.string.track_your_usage))) {
                    bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_skip_tutorial_3);
                    bundle.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_skip_tutorial_3);
                    mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_skip_tutorial_3, bundle);
                }
                sharedPrefrencesManger.setIsFiristTime(false);
            }
        });
        frameLayout = (FrameLayout) findViewById(R.id.frame_layout_root_Tutorial);

        //   LocalizationUtils.switchLocalizaion(currentLanguage,this);
    }

    private void initParallex() {
        //declare XML views...

        List<Fragment> fragments = new ArrayList<>();
        myViewPager = (ViewPager) findViewById(R.id.pager);
        //x myViewPager.setBackground(getResources().getDrawable(R.drawable.swyp_bg_new));
//        myViewPager.setBackgroundResource(R.drawable.swyp_bg_new);
//        myViewPager.setBackgroundAsset(R.drawable.swyp_bg_new);
//        myViewPager.setMaxPages(3);
//        myViewPager.setPagingEnabled(true);
//        myViewPager.isParallaxEnabled();

//sharedPrefrencesManger.getLanguage().contentEquals("en")
        //   myViewPager.setBackground(getResources().getDrawable(R.drawable.swyp_bg_new));
        if (currentLanguage.equals("en")) {
            fragments.add(new FiristTutorialFragmnet());
            fragments.add(new SecondTutorialFragmnet());
            fragments.add(new ThirdTutorialFragmnet());
            fragments.add(new FourthTutorialFragmnet());
            layouts = new int[]{0, 1, 2, 3};
        } else {
            fragments.add(new FourthTutorialFragmnet());
            fragments.add(new ThirdTutorialFragmnet());
            fragments.add(new SecondTutorialFragmnet());
            fragments.add(new FiristTutorialFragmnet());
//            layouts = new int[]{fragments.indexOf(0), fragments.indexOf(1), fragments.indexOf(2),fragments.indexOf(3) };
//            fragments.add(new FiristTutorialFragmnet());
//            fragments.add(new SecondTutorialFragmnet());
//            fragments.add(new ThirdTutorialFragmnet());
//            fragments.add(new FourthTutorialFragmnet());
            layouts = new int[]{0, 1, 2, 3};
        }
        ViewPagerParallax.OnPageChangeListener viewPagerPageChangeListener_ = new ViewPagerParallax.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (position == 0) {
                    frameLayout.setVisibility(View.GONE);
                    ll_fourth.setVisibility(View.INVISIBLE);
                    tv_content.setText(getString(R.string.enjoy_str_txt) + "\n");
                    tv_title.setText(getString(R.string.enjoy_str));
                } else if (position == 1) {
                    frameLayout.setVisibility(View.GONE);
                    ll_fourth.setVisibility(View.INVISIBLE);
                    tv_content.setText(getString(R.string.enjoy_rewards_txt) + "\n");
                    tv_title.setText(getString(R.string.enjoy_rewards));
                } else if (position == 2) {
                    ll_fourth.setVisibility(View.INVISIBLE);
                    frameLayout.setVisibility(View.GONE);
                    tv_content.setText(getString(R.string.track_your_usage_str));
                    tv_title.setText(getString(R.string.track_your_usage));
                } else if (position == 3) {
                    ll_fourth.setVisibility(View.VISIBLE);
                    frameLayout.setVisibility(View.GONE);
                    tv_content.setText(getString(R.string.enjoy_packages_str));
                    tv_title.setText(getString(R.string.enjoy_packages));
                }

//            if(position == myViewPager.getCurrentItem()){
//
//            }


            }

            @Override
            public void onPageSelected(int position) {


                addBottomDots(position);
                currentPage = position;

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        };

        myViewPager.addOnPageChangeListener(viewPagerPageChangeListener_);
        myViewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                isInTouchMode = true;
                return false;
            }
        });

        myViewPager.setAdapter(new adapterViewPager(getSupportFragmentManager(), fragments, this));  //set ViewPager adapter

        if (currentLanguage.equals("en")) {
            addBottomDots(0);
        } else {
            addBottomDots(3);
        }
//        if (currentLanguage.equals("en")) {
//            ParallaxPageTransformer pageTransformer = new ParallaxPageTransformer()
//                    .addViewToParallax(new ParallaxPageTransformer.ParallaxTransformInformation(R.id.bagID, 0.6f, 0.4f))
//                    .addViewToParallax(new ParallaxPageTransformer.ParallaxTransformInformation(R.id.keyIcon, 0.8f, 0.6f))
//                    .addViewToParallax(new ParallaxPageTransformer.ParallaxTransformInformation(R.id.coinID, 1f, 0.8f))
//                    .addViewToParallax(new ParallaxPageTransformer.ParallaxTransformInformation(R.id.scaleID, 1.5f, 1.0f))
//                    .addViewToParallax(new ParallaxPageTransformer.ParallaxTransformInformation(R.id.packagesId, 1.5f, 1.0f));
//            myViewPager.setPageTransformer(true, pageTransformer); //set page transformer
//            myViewPager.setCurrentItem(0);
//            addBottomDots(0);
//            //  pageTransformer.transformPage(myViewPager,-4);
//        } else {
//            ParallaxPageTransformer pageTransformer = new ParallaxPageTransformer()
//            .addViewToParallax(new ParallaxPageTransformer.ParallaxTransformInformation(R.id.packagesId, 1.5f, 1.0f))
//            .addViewToParallax(new ParallaxPageTransformer.ParallaxTransformInformation(R.id.scaleID, 1.5f, 1.0f))
//            .addViewToParallax(new ParallaxPageTransformer.ParallaxTransformInformation(R.id.coinID, 1f, 0.8f))
//            .addViewToParallax(new ParallaxPageTransformer.ParallaxTransformInformation(R.id.keyIcon, 0.8f, 0.6f))
//            .addViewToParallax(new ParallaxPageTransformer.ParallaxTransformInformation(R.id.bagID, 0.6f, 0.4f));
//            ParallaxPageTransformer pageTransformer = new ParallaxPageTransformer()
//                    .addViewToParallax(new ParallaxPageTransformer.ParallaxTransformInformation(R.id.bagID, 0.6f, 0.4f))
//                    .addViewToParallax(new ParallaxPageTransformer.ParallaxTransformInformation(R.id.keyIcon, 0.8f, 0.6f))
//                    .addViewToParallax(new ParallaxPageTransformer.ParallaxTransformInformation(R.id.coinID, 1f, 0.8f))
//                    .addViewToParallax(new ParallaxPageTransformer.ParallaxTransformInformation(R.id.scaleID, 1.5f, 1.0f))
//                    .addViewToParallax(new ParallaxPageTransformer.ParallaxTransformInformation(R.id.packagesId, 1.5f, 1.0f));
//            myViewPager.setPageTransformer(true, pageTransformer); //set page transformer
//            myViewPager.setCurrentItem(3);
//
//            //  myViewPager.setOffscreenPageLimit(3);
//            addBottomDots(3);
//        }

        SetAutoHide();
    }

    private void SetAutoHide() {

        if (background == null) {
            background = new Thread(new Runnable() {
                public void run() {
                    while (true) {
                        try {
//                        Log.v("running", shouldContinue + " ");
                            Thread.sleep(timerTime);
//                        Log.v("thread_time", Thread.activeCount() + " ");
                            if (shouldContinue && !isInTouchMode) {
                                mHandler.post(new Runnable() {

                                    @Override
                                    public void run() {


                                        Log.v("pages", "Time :");
                                        if (currentLanguage.equals("en")) {
                                            if (currentPage == 4 - 1) {
                                                //  Toast.makeText(TutorialActivity.this,"Loop Reset Timet ",Toast.LENGTH_SHORT).show();
                                                timerTime = 5000;
                                                currentPage = 0;
                                                myViewPager.setCurrentItem(currentPage, true);
                                                Log.v("Inner pages", "Time prit:");
                                            } else {
                                                //  Toast.makeText(TutorialActivity.this,"Loop Time",Toast.LENGTH_SHORT).show();
                                                myViewPager.setCurrentItem(++currentPage, true);
                                                timerTime = 5000;
                                            }
                                        } else {
                                            if (currentPage == 0) {
                                                currentPage = 4 - 1;
                                                myViewPager.setCurrentItem(currentPage, true);
                                                timerTime = 5000;
                                            } else {
                                                myViewPager.setCurrentItem(--currentPage, true);
                                                timerTime = 5000;
                                            }
                                        }
                                    }
                                });
                            }
                            isInTouchMode = false;
                        } catch (Exception e) {
                        }
                    }
                }
            });
            background.start();

        }


    }

    public void onSkip() {
//        Intent intent = new Intent(TutorialActivity.this, TermsConditionsSplashActivity.class);
//        intent.putExtra("fragIndex", "-1");
//        startActivity(intent);
//        finish();
        sharedPrefrencesManger.setIsTermsAccepted(true);
        Intent intent = new Intent(TutorialActivity.this, RegisterationActivity.class);
        intent.putExtra("fragIndex", "-1");//
        startActivity(intent);
        finish();
    }

    public void onClickBuyNow() {
//        Intent intent = new Intent(TutorialActivity.this, TermsConditionsSplashActivity.class);
//        intent.putExtra("fragIndex", "101");
//        startActivity(intent);
//        finish();
        sharedPrefrencesManger.setIsTermsAccepted(true);
        Intent intent = new Intent(TutorialActivity.this, RegisterationActivity.class);
        intent.putExtra("fragIndex", "1011");
        startActivity(intent);
        finish();
    }

    public void changeBulletColor(int selectedIndx) {
        for (int i = 0; i < imageViews.size(); i++) {
            if (i == selectedIndx)
                imageViews.get(i).setBackgroundDrawable(getApplicationContext().getResources().getDrawable(R.drawable.rounded_bullets_red));
            else
                imageViews.get(i).setBackgroundDrawable(getApplicationContext().getResources().getDrawable(R.drawable.rounded_bullets_orange));
        }
    }

    private void addBottomDots(int currentPage) {
        dots = new TextView[layouts.length];

        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);
    }

    public void showBottomSheetSetup() {
        dotsLayout.setVisibility(View.GONE);
        skipBtn.setVisibility(View.INVISIBLE);
        if (timer != null) {
            timer.cancel();
            if (timerTask != null)
                timerTask.cancel();
        }

        Animation animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 60);
        animation.setFillAfter(true);
        animation.setFillBefore(true);
        animation.setFillEnabled(true);
        animation.setStartOffset(300);
        animation.setDuration(300);
        signupBtn.startAnimation(animation);
    }

    public void hideBottomSheetSetup() {
        dotsLayout.setVisibility(View.VISIBLE);
        skipBtn.setVisibility(View.VISIBLE);
        resetTimer();
    }

    public void resetTimer() {
        shouldContinue = true;
        //  timerTime = 5000;
        //        if (timer != null){
//            if(timerTask != null)
//                timer.cancel();
//            timerTask.cancel();
//        }
//
//        timer = new Timer();
//        timerTask = new TimerTask() {
//
//            @Override
//            public void run() {
//                handler.post(update);
//            }
//        };
//        timer.schedule(timerTask, 100, timerTime);
//        //  handler.removeCallbacks(update);
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        if (behavior != null) {
            if (frameLayout.getVisibility() == View.VISIBLE) {
                frameLayout.setVisibility(View.GONE);
            } else if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                hideBottomSheetSetup();
            } else {
                finish();
                //  super.onBackPressed();
                // this.onBackPressed();
            }
        } else {
            super.onBackPressed();
        }
    }

    public void replaceFragmnet(Fragment fragment, int container, boolean addTobackStack) {
        transaction = getSupportFragmentManager().beginTransaction();
        if (!addTobackStack) {   // if fragment must add to back stack
            transaction.replace(container, fragment,
                    "").commit();
        } else {
            transaction.replace(container, fragment,
                    "").addToBackStack(null).commit();
        }
    }


    //BOTTOM SHEET AND PACKAGES VIEW

    private void initStaticPackagesUI() {
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mGridLayoutManger = new LinearLayoutManager(this);
        mGridLayoutManger.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mGridLayoutManger);
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {

            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);

//                int position = parent.getChildAdapterPosition(view);
//                if(position!=(usagePackageList.size()-1)){
//                    outRect.set(40,20,40,20);
//                }else{
//
//                }
            }
        });
        signupBtn = (Button) findViewById(R.id.loginBtn_static);

        signupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tutorialActivity != null) {
                    tutorialActivity.shouldContinue = false;
                }
                Intent intent = new Intent(TutorialActivity.this, TermsConditionsSplashActivity.class);
                startActivity(intent);
//                getActivity().finish();
            }
        });
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
//        getActivity().onBackPressed();
        try {
            if (responseModel != null && responseModel.getResultObj() != null) {
                if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.packageServiceType)) {
                    getPackagesList(responseModel);
                }
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    private void getPackagesList(BaseResponseModel responseModel) {
        usagePackagesoutput = (StaticBundleOutput) responseModel.getResultObj();
        if (usagePackagesoutput != null) {
            if (usagePackagesoutput.getErrorCode() != null) {
                onGetPackagesError();
            } else
                onGetPackageSucess();
        }
    }


    private void onGetPackageSucess() {
        usagePackageList = usagePackagesoutput.getBundleList();
        if (usagePackageList != null)
            if (usagePackageList.size() > 0) {
//                BundleList bundleList = new BundleList();
//                bundleList.setBundleName("dummy element");
//                bundleList.setNameEn("dummy element");
//                usagePackageList.add(bundleList);
                packagesListAdapter = new StaticPackagesListAdapter(usagePackageList, this);

                mRecyclerView.setAdapter(packagesListAdapter);
            }
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
//        getActivity().onBackPressed();
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
//        ((TutorialActivity) getActivity()).addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        masterInputResponse = new MasterInputResponse();
        masterInputResponse.setAppVersion(appVersion);
        masterInputResponse.setToken(token);
        masterInputResponse.setOsVersion(osVersion);
        masterInputResponse.setChannel(channel);

        masterInputResponse.setDeviceId(deviceId);
        masterInputResponse.setMsisdn("0505555555");
        new GetStaticPackageListService(this, masterInputResponse);

    }

    private void onGetPackagesError() {
        showErrorFragment(usagePackagesoutput.getErrorMsgEn());
    }

    public void showErrorFragment(String error) {
        frameLayout.setVisibility(View.VISIBLE);
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, error);
        errorFragment.setArguments(bundle);
        replaceFragmnet(errorFragment, R.id.frame_layout_root_Tutorial, true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        background.interrupt();
    }

    @Override
    protected void onResume() {
        super.onResume();
        handler = new Handler();
        dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);
        titleLayout = (RelativeLayout) findViewById(R.id.titleLayout);
//        dotsLayout.bringToFront();
        initUI();
        initParallex();
        tutorialActivityInstance = this;
        if (currentLanguage.equals("en"))
            currentPage = 3;
        else
            currentPage = 0;


        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }
}
