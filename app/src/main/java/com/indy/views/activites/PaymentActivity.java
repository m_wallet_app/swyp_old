package com.indy.views.activites;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.BuildConfig;
import com.indy.R;
import com.indy.controls.AdjustEvents;
import com.indy.controls.ServiceUtils;
import com.indy.models.finalizeregisteration.AdditionalInfo;
import com.indy.models.finalizeregisteration.DeliveryInfo;
import com.indy.models.finalizeregisteration.FinalizeRegisterationRequestInput;
import com.indy.models.finalizeregisteration.FinalizeRegisterationRequestOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.ocr.screens.EmailAddressFragment;
import com.indy.services.FinalizeRegisteraionRequestService;
import com.indy.services.LogApplicationFlowService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.buynewline.DeliveryDetailsAddress;
import com.indy.views.fragments.buynewline.DeliveryDetailsAreaFragment;
import com.indy.views.fragments.buynewline.NameDOBFragment;
import com.indy.views.fragments.buynewline.NewNumberFragment;
import com.indy.views.fragments.buynewline.OrderFragment;
import com.indy.views.fragments.buynewline.OrderSummaryFragment;
import com.indy.views.fragments.buynewline.PaymentFailsFragment;
import com.indy.views.fragments.buynewline.PaymentSucessfulllyFragment;
import com.indy.views.fragments.buynewline.exisitingnumber.EmiratesIDVerificationFragment;
import com.indy.views.fragments.buynewline.exisitingnumber.NumberTransferFragment;
import com.indy.views.fragments.buynewline.exisitingnumber.PromoCodeVerificationSuccessfulFragment;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.indy.views.activites.RegisterationActivity.logEventInputModel;

/**
 * Created by emad on 8/16/16.
 */
public class PaymentActivity extends MasterActivity {

    private WebView paymentWebView;
    //    private ProgressBar progressID;
    private StringBuilder stringBuilder = new StringBuilder();
    private String params = null;
    private String sParams = null;
    private FinalizeRegisterationRequestInput finalizeRegisterationRequestInput;
    private FinalizeRegisterationRequestOutput finalizeRegisterationRequestOutput;
    Button backImg;
    private FinalizeRegisteraionRequestService finalizeRegisteraionRequestService;
    private boolean isUrlLoadedSucessfully;
    private FrameLayout loadingFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        initUI();
    }

    @Override
    public void initUI() {
        super.initUI();
        paymentWebView = (WebView) findViewById(R.id.paymentWebView);
//        progressID = (ProgressBar) findViewById(R.id.progressID);
//        progressID.setVisibility(View.VISIBLE);
        TextView headerText = (TextView) findViewById(R.id.titleID);
        headerText.setText(getString(R.string.payment_spaced));
        replaceFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        loadingFragment = (FrameLayout) findViewById(R.id.loadingFragment);
        backImg = (Button) findViewById(R.id.backImg);
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        loadingFragment.setVisibility(View.GONE);
        WebSettings settings = paymentWebView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setAllowContentAccess(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setLoadWithOverviewMode(true);
//        settings.setAllowFileAccess(true);
//        settings.setAllowUniversalAccessFromFileURLs(true);
//        settings.setNeedInitialFocus(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        paymentWebView.addJavascriptInterface(new LoadListener(), "HTMLOUT");
        if (Build.VERSION.SDK_INT >= 21) {
            settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }

        /*
        dialoge is appear in case errors this is css dialoge
        setWebChromeClient is used for enable css dialoge.
         */
        paymentWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                //Required functionality here
                return super.onJsAlert(view, url, message, result);
            }
        });
        onConsumeService();
        isUrlLoadedSucessfully = false;
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
//        progressID.setVisibility(View.GONE);
        try {
            loadingFragment.setVisibility(View.GONE);
            if (responseModel != null && responseModel.getServiceType() != null && responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.LOG_USER_EVENT)) {

            } else {
                onBackPressed();
                if (responseModel != null && responseModel.getResultObj() != null) {
                    finalizeRegisterationRequestOutput = (FinalizeRegisterationRequestOutput) responseModel.getResultObj();
                    if (finalizeRegisterationRequestOutput != null) {
                        if (finalizeRegisterationRequestOutput.getErrorCode() != null)
                            onFinalizeRegisterationError();
                        else
                            onFinalizeRegisterationSucess();
                    }
                }
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

    }

    private void onFinalizeRegisterationSucess() {
        if (finalizeRegisterationRequestOutput != null) {

            Bundle bundle2 = new Bundle();

            bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_delivery_info_success);
            bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_delivery_info_success);
            mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_delivery_info_success, bundle2);
            if (finalizeRegisterationRequestOutput.getTransactionId() != null && !finalizeRegisterationRequestOutput.getTransactionId().isEmpty()) {
                sharedPrefrencesManger.setTransactionID(finalizeRegisterationRequestOutput.getTransactionId());
                if (!NewNumberFragment.msisdnNumber.equalsIgnoreCase("-1")) {
                    sharedPrefrencesManger.setMobileNo(NewNumberFragment.msisdnNumber);
                } else {
                    sharedPrefrencesManger.setMobileNo(NumberTransferFragment.mobileNoTxt);
                }
            }
            if (finalizeRegisterationRequestOutput.getAdditionalInfo().size() > 0) {
                for (AdditionalInfo additionalInfo : finalizeRegisterationRequestOutput.getAdditionalInfo()) {
                    stringBuilder.append(additionalInfo.getName());
                    stringBuilder.append("=");
                    stringBuilder.append(additionalInfo.getValue());
                    stringBuilder.append("&");
                }
                params = stringBuilder.toString();
                if (params.toString().length() > 0)
                    sParams = params.substring(0, params.length() - 1);
                loadURL(finalizeRegisterationRequestOutput.getPaymentGateWayUrl(), sParams);

            }
        }
    }

    private void onFinalizeRegisterationError() {
//        showErrorDalioge(finalizeRegisterationRequestOutput.getErrorMsgEn());
        Bundle bundle2 = new Bundle();

        bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_delivery_info_failure);
        bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_delivery_info_failure);
        mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_delivery_info_failure, bundle2);
        if (finalizeRegisterationRequestOutput != null && finalizeRegisterationRequestOutput.getErrorMsgEn() != null) {
            if (currentLanguage.equals("en")) {
                showErrorFragment(finalizeRegisterationRequestOutput.getErrorMsgEn());
            } else {
                showErrorFragment(finalizeRegisterationRequestOutput.getErrorMsgAr());
            }
        } else {
            showErrorFragment(getString(R.string.error_));
        }
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        //784-1992-6040730-1
//        progressID.setVisibility(View.VISIBLE);
        loadingFragment.setVisibility(View.VISIBLE);
        finalizeRegisterationRequestInput = new FinalizeRegisterationRequestInput();
        finalizeRegisterationRequestInput.setLang(currentLanguage);
        finalizeRegisterationRequestInput.setAppVersion(appVersion);
        finalizeRegisterationRequestInput.setOsVersion(osVersion);
        finalizeRegisterationRequestInput.setToken(sharedPrefrencesManger.getToken());
        finalizeRegisterationRequestInput.setChannel(channel);
        finalizeRegisterationRequestInput.setDeviceId(deviceId);
        finalizeRegisterationRequestInput.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        if (!NewNumberFragment.msisdnNumber.equalsIgnoreCase("-1")) {
            finalizeRegisterationRequestInput.setMsisdn(NewNumberFragment.msisdnNumber);
        } else {
            finalizeRegisterationRequestInput.setMsisdn(NumberTransferFragment.mobileNoTxt);
        }
        finalizeRegisterationRequestInput.setAmount(String.valueOf(OrderSummaryFragment.totalAmount));
        finalizeRegisterationRequestInput.setShippingMethod(OrderFragment.shippingMethodStr);
        if (!NewNumberFragment.msisdnNumber.equalsIgnoreCase("-1")) {
            finalizeRegisterationRequestInput.setOrderType("registrationOrder");//in migration the value will be migrationOrder
        } else {
            finalizeRegisterationRequestInput.setOrderType("migrationOrder");//in migration the value will be migrationOrder
        }
        if (!NewNumberFragment.msisdnNumber.equalsIgnoreCase("-1")) {
            finalizeRegisterationRequestInput.setEmiratesId(EmiratesIDVerificationFragment.emiratesID);
        } else {
            finalizeRegisterationRequestInput.setEmiratesId(NameDOBFragment.emiratedID);
        }
        finalizeRegisterationRequestInput.setPackageType(OrderFragment.orderTypeStr);
        if (PromoCodeVerificationSuccessfulFragment.promoCodeType == 1) {
            finalizeRegisterationRequestInput.setPackageType("4");

        } else if (PromoCodeVerificationSuccessfulFragment.promoCodeType == 2) {
            finalizeRegisterationRequestInput.setPackageType("3");
        }
        DeliveryInfo deliveryInfo = new DeliveryInfo();
        deliveryInfo.setEmirate(DeliveryDetailsAreaFragment.emirateStr);
        deliveryInfo.setCityCode(DeliveryDetailsAreaFragment.cityCodeStr);
        deliveryInfo.setArea(DeliveryDetailsAreaFragment.areaStr);
        deliveryInfo.setContactNumber(EmailAddressFragment.mobileNoStr);
        deliveryInfo.setAddressLine1(DeliveryDetailsAddress.addressLine2);
        deliveryInfo.setAddressLine2(DeliveryDetailsAddress.addressLine2);
        finalizeRegisterationRequestInput.setDeliveryInfo(deliveryInfo);
        if (OrderFragment.promoCode != null && OrderFragment.promoCode.length() > 0) {
            finalizeRegisterationRequestInput.setPromoCode(OrderFragment.promoCode);
        }
        finalizeRegisteraionRequestService = new FinalizeRegisteraionRequestService(this, finalizeRegisterationRequestInput);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        try {

            super.onErrorListener(responseModel);
//        progressID.setVisibility(View.GONE);
            loadingFragment.setVisibility(View.GONE);
//        onBackPressed();
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    @JavascriptInterface
    public void loadURL(String paymnetURL, String postData) {
//        progressID.setVisibility(View.GONE);

        paymentWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView wView, String url) {

                if (!BuildConfig.FLAVOR.equalsIgnoreCase("prod")) {
                    HashMap<String, String> customHeader = new HashMap();
                    customHeader.put("Content-Type", "application/json");
                    customHeader.put("custom_header", "pre_prod");
                    wView.loadUrl(url, customHeader);
                    return true;
                }
                return false;

            }

//            @Override
//            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
//                super.onReceivedHttpError(view, request, errorResponse);
//                if (!isUrlLoadedSucessfully) {
//                    view.loadUrl("javascript:window.HTMLOUT.processHTML('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
//
//                }
//            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                view.loadUrl("javascript:window.HTMLOUT.processHTML('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
            }

        });


        try {
            byte[] bytes = postData.toString().getBytes("UTF-8");
            paymentWebView.postUrl(paymnetURL, bytes);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showErrorFragment(String error) {
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, error);
        ErrorFragment errorFragment = new ErrorFragment();
        errorFragment.setArguments(bundle);
        replaceFragmnet(errorFragment, R.id.frameLayout, true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finalizeRegisteraionRequestService.onWeServiceCallCanceled();
    }

    private void onPaymnetSucess() {
        CommonMethods.logFirebaseEvent(mFirebaseAnalytics, ConstantUtils.TAGGING_order_placed_successfully);
        logEvent(AdjustEvents.ORDER_COMPLETE_CREDIT_CARD, new Bundle());
        logEvent(AdjustEvents.ORDER_COMPLETE, new Bundle());
        CommonMethods.logCompletedRegistrationEvent(ConstantUtils.ACCOUNT_NUMBER);
        NewNumberFragment.isTimerActive = false;
        PaymentSucessfulllyFragment paymentSucessfulllyFragment = new PaymentSucessfulllyFragment();
        logEvent();
        sharedPrefrencesManger.setLastStatus("UPL");
        replaceFragmnet(paymentSucessfulllyFragment, R.id.frameLayout, true);
    }

    private void logEvent() {

        logEventInputModel.setActionTransactionId(sharedPrefrencesManger.getTempAppId());
        logEventInputModel.setScreenId(ConstantUtils.SCREEN_ID_1006);
        logEventInputModel.setPay("pay");
        logEventInputModel.setProgressStatus(ConstantUtils.APP_LOG_STATUS_SUCCESSFUL);
        new LogApplicationFlowService(this, logEventInputModel);
    }

    private void onPaymentFailure() {
        CommonMethods.logFirebaseEvent(mFirebaseAnalytics, ConstantUtils.TAGGING_onboarding_error_payment_fail);
        logEvent(AdjustEvents.TRANSACTION_FAILED, new Bundle());

        logEventInputModel.setScreenId(ConstantUtils.SCREEN_ID_1006);
        logEventInputModel.setProgressStatus(ConstantUtils.APP_LOG_STATUS_SUCCESSFUL);
        new LogApplicationFlowService(this, logEventInputModel);
        replaceFragmnet(new PaymentFailsFragment(), R.id.frameLayout, true);
    }

    private void removeLoadingFragment() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loadingFragment.setVisibility(View.GONE);
            }
        });
    }

    private void getPaymentHeaders(final String paymentUrl) {
        loadingFragment.setVisibility(View.VISIBLE);
        LoadingFragmnet loadingFragmnet = new LoadingFragmnet();
        Bundle bundle = new Bundle();
        bundle.putString("fromRedeem", "fromRedeem");
        loadingFragmnet.setArguments(bundle);
        replaceFragmnet(loadingFragmnet, R.id.frameLayout, true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                URL url = null;
                try {
                    url = new URL(paymentUrl);
                    URLConnection conn = null;
                    try {
                        conn = url.openConnection();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Map<String, List<String>> headerFields = conn.getHeaderFields();

                    Set<String> headerFieldsSet = headerFields.keySet();
                    Iterator<String> hearerFieldsIter = headerFieldsSet.iterator();
                    removeLoadingFragment();
                    while (hearerFieldsIter.hasNext()) {

                        String headerFieldKey = hearerFieldsIter.next();
                        List<String> headerFieldValue = headerFields.get(headerFieldKey);

                        StringBuilder sb = new StringBuilder();
                        for (String value : headerFieldValue) {
                            sb.append(value);
                            sb.append("");
                        }
//                        System.out.println(headerFieldKey + "=" + sb.toString());
                        if (headerFieldKey != null) {
//                            removeLoadingFragment();
                            if (headerFieldKey.equalsIgnoreCase(ConstantUtils.indy_status_tag)) {
                                if (sb.toString().equalsIgnoreCase(ConstantUtils.indy_status_failure)) {
                                    onPaymentFailure();
                                } else {

                                    onPaymnetSucess();
                                }
                            }
                        }
                    }
                } catch (MalformedURLException e) {
                }
            }
        }).start();
    }

    @Override
    public void onBackPressed() {
        try {
            if (getSupportFragmentManager() != null && getSupportFragmentManager().getFragments() != null
                    && getSupportFragmentManager().getFragments().size() > 0) {
                for (int i = getSupportFragmentManager().getFragments().size() - 1; i < getSupportFragmentManager().getFragments().size(); i++) {
                    if (getSupportFragmentManager().getFragments().get(i) instanceof PaymentSucessfulllyFragment) {
                        finish();
//                    Intent intent = new Intent(this,RegisterationActivity.class);
                        RegisterationActivity.regInstance.finish();
//                    intent.putExtra("fragIndex","4");
//                    startActivity(intent);

                    }
                }
            }
            super.onBackPressed();
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }
    }

    class LoadListener {
        @JavascriptInterface
        public void processHTML(String html) {
            Log.d("HTML", html);
            // code here
            if (html.toLowerCase().contains(ConstantUtils.success_string)) {
                onPaymnetSucess();
            } else if (html.toLowerCase().contains(ConstantUtils.failure_string)) {
                onPaymentFailure();
            } else {
                return;
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();

        if (NewNumberFragment.isTimerActive && CommonMethods.isTimerUp(sharedPrefrencesManger.getTimerValue())) {
            finish();
        }
    }

}
