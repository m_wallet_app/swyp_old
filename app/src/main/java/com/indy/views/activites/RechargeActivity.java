package com.indy.views.activites;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.models.balance.BalanceInputModel;
import com.indy.models.balance.BalanceResponseModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.services.GetUserBalanceService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.store.recharge.LcoateNearestKiosk;
import com.indy.views.fragments.store.recharge.RechargeCardFragment;
import com.indy.views.fragments.store.recharge.RechargeCreditCardFragment;
import com.indy.views.fragments.utils.ErrorFragment;

/**
 * Created by Amir.jehangir on 10/14/2016.
 */
public class RechargeActivity extends MasterActivity implements Animation.AnimationListener {

    // test commit
    public TextView titleView, selectstore, tv_headerText;
    public static TextView tv_lastUpdated;
    Button btn_RechargeCard, btn_CreditCard;
    Animation animFadein;
    int Counter = 0;
    private Button backImg, helpBtn;
    public static TextView user_balance_amount;
    BalanceInputModel balanceInputModel;
    BalanceResponseModel balanceResponseModel;
    public LinearLayout ll_topLayout;
    FrameLayout frameLayout;
    public static boolean isCharged = false;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recharge);
        animFadein = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in);
        animFadein.setAnimationListener(this);
        //   animFadein.setStartOffset(1000);

//        AnimatorSet s = new AnimatorSet();
//        s.play(animFadein).with(animFadein);
        initUI();
        onConsumeService();


    }
//  

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            if(responseModel!=null && responseModel.getResultObj()!=null) {
                balanceResponseModel = (BalanceResponseModel) responseModel.getResultObj();
                if (balanceResponseModel != null) {
                    if (balanceResponseModel.getErrorCode() != null) {
//                showErrorFragment(balanceResponseModel.getErrorMsgEn());
                        user_balance_amount.setText(getString(R.string.your_balance_default_val));

                    } else {
                        user_balance_amount.setText(getString(R.string.your_balance_is_aed) + " " + balanceResponseModel.getAmount()+ " " + getString(R.string.aed_arabic_only));
                        tv_lastUpdated.setText(getString(R.string.last_updated) + " " + sharedPrefrencesManger.getLastUpdatedBalance() +
                                " " + getString(R.string.min_ago));

                        CommonMethods.updateBalanceAndLastUpdated(getString(R.string.your_balance_is_aed) + " " + balanceResponseModel.getAmount()+ " " + getString(R.string.aed_arabic_only)
                                , getString(R.string.last_updated) + " " + sharedPrefrencesManger.getLastUpdatedBalance() +
                                        " " + getString(R.string.min_ago));
                    }
                }
                if (sharedPrefrencesManger.getUsedActivity().equals("100")) {

                } else {

                }
                sharedPrefrencesManger.setUsedActivity("");
            }
        }catch (Exception ex){

        }
    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        if (sharedPrefrencesManger.getUsedActivity().equals("100")) {

        } else {

        }
        sharedPrefrencesManger.setUsedActivity("");
    }

    @Override
    public void onBackPressed() {

        if(frameLayout.getVisibility()==View.VISIBLE && getSupportFragmentManager().getBackStackEntryCount()<2){
            frameLayout.setVisibility(View.GONE);
            super.onBackPressed();
        }else{
            super.onBackPressed();
        }
    }

    private void showErrorFragment(String error) {
        frameLayout.setVisibility(View.VISIBLE);
//        frameLayout.bringToFront();
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, error);
        errorFragment.setArguments(bundle);
        addFragmnet(errorFragment, R.id.frameLayout, true);
    }

    public void hideTopLayout() {
        ll_topLayout.setVisibility(View.GONE);
        frameLayout.setVisibility(View.GONE);
    }

    public void showTopLayout() {
        ll_topLayout.setVisibility(View.VISIBLE);
        frameLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void initUI() {
        super.initUI();
        CommonMethods.setStatusBarTransparent(this);
        backImg = (Button) findViewById(R.id.backImg);
        helpBtn = (Button) findViewById(R.id.helpBtn);
        frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
        helpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RechargeActivity.this, HelpActivity.class);
                startActivity(intent);
            }
        });
        helpBtn.setVisibility(View.INVISIBLE);
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard();
                onBackPressed();
            }
        });
        tv_headerText = (TextView) findViewById(R.id.titleTxt);
        tv_headerText.setText(getString(R.string.recharge_title));
        selectstore = (TextView) findViewById(R.id.selectstore);
        selectstore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                displayNextFragment();


//                replaceFragmnet(new LcoateNearestKiosk(), R.id.frameLayout, true);
                startActivity(new Intent(RechargeActivity.this, LcoateNearestKiosk.class));
            }
        });
        btn_CreditCard = (Button) findViewById(R.id.btn_CreditCard);
        //   btn_CreditCard.setVisibility(View.GONE);

        btn_CreditCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_recharge_crerdit_card);

                CommonMethods.logAddedPaymentInfoEvent();
                displaycreditFragment();
            }
        });
        btn_RechargeCard = (Button) findViewById(R.id.btn_rechargeCard);
        // btn_RechargeCard.setVisibility(View.GONE);
        btn_RechargeCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_recharge_card_buton);
                CommonMethods.logAddedPaymentInfoEvent();
                displayrechargeCardFragment();
            }
        });

        frameLayout.setVisibility(View.GONE);
        user_balance_amount = (TextView) findViewById(R.id.user_balance_amount);
        tv_lastUpdated = (TextView) findViewById(R.id.tv_last_updated);
        tv_lastUpdated.setText(getString(R.string.last_updated) + " " + sharedPrefrencesManger.getLastUpdatedBalance() +
                " " + getString(R.string.min_ago));
        Bundle bundle = new Bundle();

        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "RechargeScreen");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "RechargeScreen");
        mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_RECHARGE, bundle);
        btn_CreditCard.startAnimation(animFadein);
        btn_RechargeCard.startAnimation(animFadein);
    }



    @Override
    public void onConsumeService() {
        super.onConsumeService();

        balanceInputModel = new BalanceInputModel();
        balanceInputModel.setAppVersion(appVersion);
        balanceInputModel.setToken(token);
        balanceInputModel.setOsVersion(osVersion);
        balanceInputModel.setChannel(channel);
        balanceInputModel.setDeviceId(deviceId);
        balanceInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        balanceInputModel.setBillingPeriod(CommonMethods.getBillingPeriod());
        balanceInputModel.setLang(currentLanguage);
        balanceInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
        balanceInputModel.setAuthToken(authToken);
        new GetUserBalanceService(this, balanceInputModel,this);
    }

    private void displaycreditFragment() {

        RechargeCreditCardFragment rechargeCreditCardFragment = new RechargeCreditCardFragment();
        Bundle bundle = new Bundle();
        if (balanceResponseModel != null) {
            if (balanceResponseModel.getErrorCode() == null) {
                bundle.putString("balance", balanceResponseModel.getAmount() + "");
            }
        }
        rechargeCreditCardFragment.setArguments(bundle);
        replaceFragmnet(rechargeCreditCardFragment, R.id.frameLayout, true);
        frameLayout.setVisibility(View.VISIBLE);
    }

    private void displayrechargeCardFragment() {
        frameLayout.setVisibility(View.VISIBLE);

        addFragmnet(new RechargeCardFragment(), R.id.frameLayout, true);
    }
//    private void displayHistoryFragment(){
//        replaceFragmnet(new HistoryActivity(), R.id.frameLayout, true);
//    }

    @Override
    public void onAnimationStart(Animation animation) {

//        for(int i=0; i<=10000; i++ ) {
//
//            if(i > 9999)
//            btn_RechargeCard.startAnimation(animFadein);
//        }
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        //   btn_CreditCard.setVisibility(View.VISIBLE);
        //  btn_RechargeCard.setVisibility(View.VISIBLE);

        //   btn_CreditCard.startAnimation(animFadein);

        if (animation == animFadein) {
//            Toast.makeText(getApplicationContext(), "Animation Stopped",
//                    Toast.LENGTH_SHORT);
//            if (Counter == 0) {
//                btn_RechargeCard.startAnimation(animFadein);
//                  Counter++;
//                animation.cancel();
//            }
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        if(tv_lastUpdated!=null)
        tv_lastUpdated.setText(getString(R.string.last_updated) + " " + sharedPrefrencesManger.getLastUpdatedBalance() +
                " " + getString(R.string.min_ago));

        if(isCharged){
            onBackPressed();
            isCharged = false;
        }
        if(sharedPrefrencesManger.getNavigationIndex().length() > 0 || sharedPrefrencesManger.getNavigationBadgeIndex().length() >0){
            finish();
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }
}
