package com.indy.views.activites;

import android.os.Bundle;
import android.webkit.WebView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.indy.R;

public class WebViewGifTestingActivity extends MasterActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view_gif_testing);
        initUI();
    }

    @Override
    public void initUI() {
        super.initUI();
        final WebView webView = (WebView) findViewById(R.id.webView);
        WebView ivgif = (WebView) findViewById(R.id.iv_gif);


        ImageView iv_one = (ImageView) findViewById(R.id.iv_one);
        ImageView iv_two = (ImageView) findViewById(R.id.iv_two);

//        Glide.with(this) // replace with 'this' if it's in activity
//                .load("https://uat-swypapp.etisalat.ae/Indy/assets/images/badges/Friends-&-Gems-TEST-675x459_100fr.gif")
//                .asGif()
//                .error(R.drawable.bitmap_default_image) // show error drawable if the image is not a gif
//                .into(iv_one)
//        ;

       Glide.with(this).asGif().
                    load("https://uat-swypapp.etisalat.ae/Indy/assets/images/badges/Friends-&-Gems-TEST-675x459_100fr.gif")
                    .into(iv_one)
                    .onLoadFailed(getResources().getDrawable(R.drawable.bitmap_default_image));


        Glide.with(this).asGif().
                load("https://uat-swypapp.etisalat.ae/Indy/assets/images/badges/Friends-&-Gems-TEST-472x321_100fr_70p.gif")
                .into(iv_two)

                .onLoadFailed(getResources().getDrawable(R.drawable.bitmap_default_image));

//
////        WebViewClient webViewClient = new WebViewClient();
//
//        webView.setWebViewClient(new WebViewClient() {
////            @Override
////            public boolean shouldOverrideUrlLoading(WebView wView, String url) {
////                Log.v("url", url + "emad" + wView.getUrl()
////                        + " ");
////                return false;
////            }
//
//            @Override
//            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
//                super.onReceivedHttpError(view, request, errorResponse);
//
//            }
//
//            @Override
//            public void onPageFinished(WebView view, String url) {
//                super.onPageFinished(view, url);
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        webView.loadUrl("https://uat-swypapp.etisalat.ae/Indy/assets/images/badges/Friends-&-Gems-TEST-675x459_100fr.gif");
//
//                    }
//                },3000);
////                view.loadUrl("javascript:window.HTMLOUT.processHTML('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
//            }
//
//        });
//        webView.loadUrl("https://uat-swypapp.etisalat.ae/Indy/assets/images/badges/Friends-&-Gems-TEST-675x459_100fr.gif");
//
//
//
//        ivgif.setWebViewClient(new WebViewClient() {
////            @Override
////            public boolean shouldOverrideUrlLoading(WebView wView, String url) {
////                Log.v("url", url + "emad" + wView.getUrl()
////                        + " ");
////                return false;
////            }
//
//            @Override
//            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
//                super.onReceivedHttpError(view, request, errorResponse);
//
//            }
//
//            @Override
//            public void onPageFinished(WebView view, String url) {
//                super.onPageFinished(view, url);
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        webView.loadUrl("https://uat-swypapp.etisalat.ae/Indy/assets/images/badges/Friends-&-Gems-TEST-472x321_100fr_70p.gif");
//
//                    }
//                },2500);
////                view.loadUrl("javascript:window.HTMLOUT.processHTML('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
//            }
//
//        });
//        ivgif.loadUrl("https://uat-swypapp.etisalat.ae/Indy/assets/images/badges/Friends-&-Gems-TEST-472x321_100fr_70p.gif");

    }
}
