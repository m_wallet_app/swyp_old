package com.indy.views.activites;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indy.R;
import com.indy.customviews.SlideToUnlock.SlideButton;
import com.indy.customviews.SlideToUnlock.SlideButtonListener;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.login.InviteCodeFragment;
import com.indy.views.fragments.newaccount.NewAccountCodeFragment;
import com.indy.views.fragments.newaccount.NewAccountFragment;
import com.indy.views.fragments.newaccount.SetPasswordFragment;
import com.indy.views.fragments.utils.MasterFragment;

/**
 * Created by Amir.jehangir on 11/7/2016.
 */

public class SwypeToReachUsFragmnet extends MasterFragment /*implements SeekBar.OnSeekBarChangeListener */ {

    private View view;
    SlideButton slideButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_swype_to_reach_us, container, false);
            initUI();
            ((RegisterationActivity) getActivity()).hideHeaderLayout();
        } else {
            if (view != null) {

            }
        }
        return view;

    }

    @Override
    public void initUI() {
        super.initUI();

        slideButton = ((SlideButton) view.findViewById(R.id.unlockButtonV));
//        slideButton.
//                setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
//                    @Override
//                    public void onProgressChanged(final SeekBar seekBar, final int i, boolean b) {
//                        Log.i("Progress", String.valueOf(i));
//                        if (seekBar.getProgress() >= 50) {
////                            ObjectAnimator animation = ObjectAnimator.ofInt(slideButton, "progress", 100);
////                            animation.setDuration(500); // 0.5 second
////                            animation.setInterpolator(new DecelerateInterpolator());
////                            animation.start();
//                            Runnable runnable = new Runnable() {
//                                @Override
//                                public void run() {
//                                    slideButton.setProgress(i + 1);
//                                    if (slideButton.getProgress() == 100) {
//
////                                handleSlide();
//                                    } else {
//                                        slideButton.postDelayed(this
//                                                , 20);
//                                    }
//                                }
//                            };
//                            slideButton.postDelayed(runnable
//                                    , 20);
//                        } else {
//                            slideButton.
//                                    setOnSeekBarChangeListener(null);
//                        }
//                    }
//
//                    @Override
//                    public void onStartTrackingTouch(SeekBar seekBar) {
//
//                    }
//
//                    @Override
//                    public void onStopTrackingTouch(SeekBar seekBar) {
//
//                    }
//                });
//        ((SlideButton) view.findViewById(R.id.unlockButtonV)).setProgress(55);
        ((SlideButton) view.findViewById(R.id.unlockButtonV)).setSlideButtonListener(new SlideButtonListener() {
            @Override
            public void handleSlide() {
//                System.out.println("Unlock Vertical");
//                Toast.makeText(getActivity(), "Unlock", Toast.LENGTH_SHORT).show();
//                startActivity(new Intent(getActivity(), RegisterationActivity.class));
//                onSkip();
//                ((RegisterationActivity) getActivity()).replaceFragmnet(new LoginFragment(), R.id.frameLayout, true);
                if (regActivity != null)
                    regActivity.finish();

                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_create_account_swyp_login);

                Intent intent = new Intent(getActivity(), SwpeMainActivity.class);
                getActivity().startActivity(intent);
            }
        });

    }

    private void onSkip() {
        Fragment[] myFragments = {new NewAccountFragment(),
                new NewAccountCodeFragment(),
                new SetPasswordFragment(),
                new InviteCodeFragment()
        };

        regActivity.removeFragment(myFragments);
    }
}
