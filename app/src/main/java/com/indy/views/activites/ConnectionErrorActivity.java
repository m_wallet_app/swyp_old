package com.indy.views.activites;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.indy.R;


/**
 * Created by emad on 10/8/16.
 */

public class ConnectionErrorActivity extends MasterActivity {
    private TextView titleTxtView, errorTxt;
    private Button okBtn;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_error);
        initUI();
    }

    @Override
    public void initUI() {
        super.initUI();
        titleTxtView = (TextView) findViewById(R.id.titleTxtView);
        errorTxt = (TextView) findViewById(R.id.errorTxt);

        if (getIntent().getIntExtra("isNetworkError", -1) == 0) {
            titleTxtView.setText(getString(R.string.error_txt));
            errorTxt.setText(getString(R.string.generice_error));
        } else if (getIntent().getIntExtra("isNetworkError", -1) == 1) {
            titleTxtView.setText(getString(R.string.error_txt));
            errorTxt.setText(getString(R.string.internet_connection_error));
        }

        ((Button) findViewById(R.id.cancelBtn)).setVisibility(View.INVISIBLE);
        okBtn = (Button) findViewById(R.id.okBtn);
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
