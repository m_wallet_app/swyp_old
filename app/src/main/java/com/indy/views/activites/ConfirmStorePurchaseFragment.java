package com.indy.views.activites;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.controls.ServiceUtils;
import com.indy.models.purchaseStoreBundle.OrderItems;
import com.indy.models.purchaseStoreBundle.PurchaseStoreInputResponse;
import com.indy.models.purchaseStoreBundle.PurchaseStoreOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.services.PurchaseStoreService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.utils.MaterialCheckBox;
import com.indy.views.fragments.gamification.models.serviceInputOutput.SuccessOutputResponse;
import com.indy.views.fragments.gamification.models.serviceInputOutput.logPurchaseBundleEvent.LogPurchaseBundleEventInputModel;
import com.indy.views.fragments.gamification.services.RegisterPurchaseBundleEventService;
import com.indy.views.fragments.store.mainstore.MyStoreFragment;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;

import java.util.ArrayList;
import java.util.List;


public class ConfirmStorePurchaseFragment extends MasterActivity {


    TextView tv_title_confirmation, tv_text_confirmation, tv_title_success, tv_text_success, btn_cancel;
    Button btn_okSuccess, btn_confirm;
    FrameLayout frameLayout;
    LinearLayout ll_confirmation, ll_success, ll_root;
    PurchaseStoreInputResponse purchaseStoreInputResponse;
    PurchaseStoreOutput purchaseStoreOutput;
    List<OrderItems> listOfItems;
    int countOfItems = 0;
    double totalAmount = 0;
    //    double userBalance = 0;
    boolean fromLotteryDeal = false;
    double userBalance = 0;
    String rewardId;
    int notificationResponseID;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_store_purchase_fragment);
        initUI();
        setListeners();

    }

    @Override
    public void initUI() {
        super.initUI();

        tv_title_confirmation = (TextView) findViewById(R.id.tv_title);
        tv_text_confirmation = (TextView) findViewById(R.id.tv_warningText);
        tv_title_success = (TextView) findViewById(R.id.tv_title_success);
        tv_text_success = (TextView) findViewById(R.id.tv_warningText_success);

        btn_cancel = (TextView) findViewById(R.id.btn_cancel);
        btn_okSuccess = (Button) findViewById(R.id.btn_ok_success);
        btn_confirm = (Button) findViewById(R.id.btn_ok);
        frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
        ll_confirmation = (LinearLayout) findViewById(R.id.ll_confirmation_message);
        ll_success = (LinearLayout) findViewById(R.id.ll_successful_transaction);
        ll_root = (LinearLayout) findViewById(R.id.ll_root);
        listOfItems = new ArrayList<OrderItems>();
        SuccessTickView successTickView = new SuccessTickView(getApplicationContext());
        successTickView = (SuccessTickView) findViewById(R.id.successView);

//        successTickView.startTickAnim();
        if (getIntent().getExtras() != null && getIntent().getExtras().getStringArrayList(ConstantUtils.itemsIdList) != null) {
            listOfItems = (List<OrderItems>) getIntent().getExtras().getSerializable(ConstantUtils.itemsIdList);
        }
        if (getIntent().getExtras() != null && getIntent().getExtras().getString("total") != null) {
            totalAmount = Double.parseDouble(getIntent().getExtras().getString("total"));
        }
        if (getIntent().getExtras() != null && getIntent().getExtras().getString(ConstantUtils.REWARD_ID) != null) {
            rewardId = getIntent().getExtras().getString(ConstantUtils.REWARD_ID);
        }
        if (getIntent().getExtras() != null) {
            fromLotteryDeal = getIntent().getExtras().getBoolean(ConstantUtils.FROM_LOTTERY_DEAL, false);
        }

        if (getIntent().getExtras() != null) {
            notificationResponseID = getIntent().getExtras().getInt(ConstantUtils.LUCKY_DEAL_ID, -1);
        }

        if (MyStoreFragment.selectedItems != null)
            for (int i = 0; i < MyStoreFragment.selectedItems.size(); i++) {
                countOfItems += MyStoreFragment.selectedItems.get(i).getCount();
            }

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(ConstantUtils.USER_BALANCE)) {
            userBalance = getIntent().getExtras().getDouble(ConstantUtils.USER_BALANCE);
        }
        if (userBalance >= totalAmount) {
            showConfirmationMesage();
        } else {
            String text = getString(R.string.you_do_not_have_sufficent_balance);
            String title = getString(R.string.sorry_payment_failed);
            showErrorFragment(title, text);
        }
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(ConstantUtils.USER_BALANCE)) {
            userBalance = getIntent().getExtras().getDouble(ConstantUtils.USER_BALANCE);
        }
        if (userBalance >= totalAmount) {
            showConfirmationMesage();
        } else {
            String text = getString(R.string.your_request_cannot_be_processed_because_you_dont_have_enough_balance);
            String title = getString(R.string.sorry_payment_failed);
            showErrorFragment(title, text);
        }
    }

    public void setListeners() {
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_store_cancel_final_purchase);

                finish();
            }
        });

        btn_okSuccess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                if (!fromLotteryDeal && MyStoreFragment.myStoreFragmentInstance != null)
                    MyStoreFragment.myStoreFragmentInstance.refreshBalance();
            }
        });

        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_store_final_purchase_confirmation);
                for (int i = 0; i < MyStoreFragment.selectedItems.size(); i++) {
                    CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_store_confirm_purchase_button + "_" + MyStoreFragment.selectedItems.get(i).getBundleName());

                }
                onConsumeService();
//                showSuccessMessage();
            }
        });

        if (totalAmount > MyStoreFragment.userBalance) {
            showBalanceErrorMessage();

        } else {
            showConfirmationMesage();
        }
    }

    private void showBalanceErrorMessage() {
        ll_confirmation.setVisibility(View.VISIBLE);
        ll_success.setVisibility(View.GONE);
        frameLayout.setVisibility(View.GONE);
        String text = getString(R.string.insufficent_balance_error);
        String title = getString(R.string.notice);
        tv_text_confirmation.setText(text);
        tv_title_confirmation.setText(title);
        btn_confirm.setText(getString(R.string.ok));
        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
//        if(getIntent().getExtras()!=null && getIntent().getExtras().containsKey(ConstantUtils.USER_BALANCE)){
//            userBalance = getIntent().getExtras().getDouble(ConstantUtils.USER_BALANCE);
//        }
//        if(userBalance>=totalAmount) {
//            showConfirmationMesage();
//        }else{
//            String text = getString(R.string.you_do_not_have_sufficent_balance);
//            String title = getString(R.string.sorry_payment_failed);
//            showErrorFragment(title, text);
//        }
    }

    private void showErrorFragment(String title, String errorText) {
        frameLayout.setVisibility(View.VISIBLE);
//        onBackPressed();
        ll_success.setVisibility(View.GONE);
        ll_confirmation.setVisibility(View.GONE);
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, errorText);
        if (title.length() > 0)
            bundle.putString(ConstantUtils.errorTitle, title);

        errorFragment.setArguments(bundle);

        replaceFragmnet(errorFragment, R.id.frameLayout, true);
    }

    private void showConfirmationMesage() {


        ll_confirmation.setVisibility(View.VISIBLE);
        ll_success.setVisibility(View.GONE);
        frameLayout.setVisibility(View.GONE);
        String text = getString(R.string.you_are_about_to_buy) + " " + countOfItems + " " + getString(R.string.items_total_of) + " " +
                totalAmount + " " + getString(R.string.aed) + " " + getString(R.string.will_be_decuted_from_your_balance) +
                " " + userBalance + " " + getString(R.string.aed) + "."
                // + total Balance left //
                + " " + /*getString(R.string.in_your_balance) +*/ getString(R.string.do_you_want_to_proceed);

        String title = getString(R.string.payment_confirmation);
        tv_text_confirmation.setText(text);
        tv_title_confirmation.setText(title);
    }

    private void showSuccessMessage() {
        if (MyStoreFragment.selectedItems != null) {
            MyStoreFragment.selectedItems.clear();
        }
        ll_confirmation.setVisibility(View.GONE);
        ll_success.setVisibility(View.VISIBLE);
        frameLayout.setVisibility(View.GONE);
        MaterialCheckBox materialCheckBox = (MaterialCheckBox) findViewById(R.id.fl_tick_animation);

        ImageView iv_tick = (ImageView) findViewById(R.id.iv_tick);
        CommonMethods.drawCircle(materialCheckBox, iv_tick);
        String text = getString(R.string.your_request_is_being_processed_and_you_will_soon_new);
        String title = getString(R.string.payment_succesful);
        tv_text_success.setText(text);
        tv_title_success.setText(title);
        ShoppingCartActivity.shoppingCartActivityInstance.finish();
        CommonMethods.createAppRatingDialog(this);
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();

        //show service here

        frameLayout.setVisibility(View.VISIBLE);
        ll_success.setVisibility(View.GONE);
        ll_confirmation.setVisibility(View.GONE);
        addFragmnet(new LoadingFragmnet(), R.id.frameLayout, false);
        PurchaseStoreInputResponse purchaseStoreInputResponse = new PurchaseStoreInputResponse();
        purchaseStoreInputResponse.setImsi(sharedPrefrencesManger.getMobileNo());
        purchaseStoreInputResponse.setLang(currentLanguage);
        purchaseStoreInputResponse.setMsisdn(sharedPrefrencesManger.getMobileNo());
        purchaseStoreInputResponse.setChannel(channel);
        purchaseStoreInputResponse.setAppVersion(appVersion);
        purchaseStoreInputResponse.setOsVersion(osVersion);
        purchaseStoreInputResponse.setToken(token);
        purchaseStoreInputResponse.setDeviceId(deviceId);
        purchaseStoreInputResponse.setLang(currentLanguage);
        purchaseStoreInputResponse.setAuthToken(authToken);
        purchaseStoreInputResponse.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        purchaseStoreInputResponse.setRewardId(rewardId);
        purchaseStoreInputResponse.setTalosUserId(sharedPrefrencesManger.getUserId());
        OrderItems[] listToSend = new OrderItems[listOfItems.size()];
        for (int i = 0; i < listOfItems.size(); i++) {
            listToSend[i] = listOfItems.get(i);
        }
        purchaseStoreInputResponse.setOrderItems(listToSend);
        new PurchaseStoreService(this, purchaseStoreInputResponse);

    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            frameLayout.setVisibility(View.GONE);
            switch (responseModel.getServiceType()) {
                case ServiceUtils.REGISTER_PURCHASE_STORE_SUCCESS:
                    if (responseModel != null && responseModel.getResultObj() != null && responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.REGISTER_PURCHASE_STORE_SUCCESS)) {
                        SuccessOutputResponse successOutputResponse = (SuccessOutputResponse) responseModel.getResultObj();

                    }
                    break;

                default:
                    if (responseModel != null && responseModel.getResultObj() != null) {
                        purchaseStoreOutput = (PurchaseStoreOutput) responseModel.getResultObj();

                        if (purchaseStoreOutput.getResponseCode() != null && purchaseStoreOutput.getResponseCode().equals("true")) {
                            onPurchaseSucess();
                        } else {
                            onPurchaseFaild();
                        }

                    } else {
                        // call no balance error here
                        String text = getString(R.string.your_request_cannot_be_processed_because_you_dont_have_enough_balance);
                        String title = getString(R.string.sorry_payment_failed);
                        showErrorFragment(title, text);
                    }
                    break;
            }

        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        onBackPressed();
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    private void onPurchaseSucess() {
        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_store_purchase_success);
        if (purchaseStoreOutput != null && purchaseStoreOutput.getErrorCode() == null) {
            showSuccessMessage();
            onRegisterEvent();
        } else {
            onPurchaseFaild();
        }
    }

    public void onRegisterEvent() {
        List<String> packageCodes = new ArrayList<>();
        for (int i = 0; i < listOfItems.size(); i++) {
            packageCodes.add(listOfItems.get(i).getOfferId());
        }

        LogPurchaseBundleEventInputModel gamificationBaseInputModel = new LogPurchaseBundleEventInputModel();
        gamificationBaseInputModel.setChannel(channel);
        gamificationBaseInputModel.setLang(currentLanguage);
        gamificationBaseInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        gamificationBaseInputModel.setAppVersion(appVersion);
        gamificationBaseInputModel.setAuthToken(authToken);
        gamificationBaseInputModel.setDeviceId(deviceId);
        gamificationBaseInputModel.setToken(token);
        gamificationBaseInputModel.setUserSessionId(sharedPrefrencesManger.getUserSessionId());
        gamificationBaseInputModel.setUserId(sharedPrefrencesManger.getUserId());
        gamificationBaseInputModel.setApplicationId(ConstantUtils.INDY_TALOS_ID);
        gamificationBaseInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
        gamificationBaseInputModel.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        gamificationBaseInputModel.setOsVersion(osVersion);
        gamificationBaseInputModel.setPackageCode(packageCodes);
        gamificationBaseInputModel.setRewardId(rewardId);
        if (notificationResponseID > 0) {
            gamificationBaseInputModel.setId(notificationResponseID);
        }
        new RegisterPurchaseBundleEventService(this, gamificationBaseInputModel);


    }

    private void onPurchaseFaild() {
        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_store_purchase_failure);
        String title = " ";
        String text;
        if (purchaseStoreOutput.getErrorMsgEn() != null) {
            if (currentLanguage.equalsIgnoreCase(ConstantUtils.lang_english))
                text = purchaseStoreOutput.getErrorMsgEn();
            else
                text = purchaseStoreOutput.getErrorMsgAr();
            if (purchaseStoreOutput.getErrorCode() != null && purchaseStoreOutput.getErrorCode().equals("3000020")) {
                title = getString(R.string.time_has_expired);
            }
            else{
                title = getString(R.string.error_txt);
            }
        } else {
            text = getString(R.string.generice_error);
        }
        showErrorFragment(getString(R.string.error_txt), text);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        try {
            frameLayout.setVisibility(View.GONE);
            switch (responseModel.getServiceType()) {

                case ServiceUtils.REGISTER_PURCHASE_STORE_SUCCESS:

                    break;

                default:
                    showErrorFragment(getString(R.string.error_txt), getString(R.string.generice_error));
                    break;

            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }
}
