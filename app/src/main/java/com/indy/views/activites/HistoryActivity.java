package com.indy.views.activites;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.adapters.RecyclerViewSectionAdapter;
import com.indy.models.history.HistoryDataModel;
import com.indy.models.history.HistoryDataOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.models.utils.MasterInputResponse;
import com.indy.services.HistoryListService;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Amir.jehangir on 10/18/2016.
 */
public class HistoryActivity extends MasterActivity {
    private Toolbar toolbar;


    List<HistoryDataModel> allSampleData;
    MasterInputResponse masterInputResponse;
    HistoryDataOutput historyDataOutput;
    //    private String status;
    RelativeLayout RvBackground;
    TextView title;
    private Button backImg, helpBtn;
    RecyclerView my_recycler_view;
    FrameLayout frameLayout;
    ProgressBar progressBar;
    LinearLayout ll_main,ll_emptyState;
    private RelativeLayout backLayout, helpLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history_fragment);

        initUI();
        onConsumeService();
        //  populateSampleData();

        allSampleData = new ArrayList<HistoryDataModel>();


        my_recycler_view = (RecyclerView) findViewById(R.id.my_recycler_view);
//        my_recycler_view.addItemDecoration(
//                new DividerItemDecoration(getResources().getDrawable(R.drawable.background_line_grey),false,false));
//getResources().getDrawable(R.drawable.background_line_grey),false,false)

//        RecyclerViewSectionAdapter adapter = new RecyclerViewSectionAdapter(allSampleData);
//
//
//        GridLayoutManager manager = new GridLayoutManager(this,
//                getResources().getInteger(R.integer.grid_span_1));
//
//
//     /*   GridLayoutManager manager = new GridLayoutManager(this,
//                getResources().getInteger(R.integer.grid_span_2));
//
//
//        GridLayoutManager manager = new GridLayoutManager(this,
//                getResources().getInteger(R.integer.grid_span_3));*/
//
//        my_recycler_view.setLayoutManager(manager);
//        adapter.setLayoutManager(manager);
//        my_recycler_view.setAdapter(adapter);


    }


    @Override
    public void initUI() {
        super.initUI();
        RvBackground = (RelativeLayout) findViewById(R.id.headerLayoutID);
        RvBackground.setBackground(getResources().getDrawable(R.drawable.header_reward_detail));
        title = (TextView) findViewById(R.id.titleTxt);
        title.setText(getResources().getString(R.string.history_paymentBilling));
        backImg = (Button) findViewById(R.id.backImg);
        helpBtn = (Button) findViewById(R.id.helpBtn);
        helpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HistoryActivity.this, HelpActivity.class);
                startActivity(intent);
            }
        });
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        helpBtn.setVisibility(View.INVISIBLE);
        frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
        frameLayout.setVisibility(View.GONE);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        ll_main = (LinearLayout) findViewById(R.id.ll_main);
        ll_emptyState = (LinearLayout) findViewById(R.id.ll_empty_state);
        ll_main.setVisibility(View.VISIBLE);
        ll_emptyState.setVisibility(View.GONE);

        title = (TextView) findViewById(R.id.titleTxt);
        title.setText(R.string.history_paymentBilling);
        backLayout = (RelativeLayout) findViewById(R.id.backLayout);
        helpLayout = (RelativeLayout) findViewById(R.id.helpLayout);

        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        helpLayout.setVisibility(View.INVISIBLE);
    }

    private void populateSampleData() {

        for (int i = 1; i <= 10; i++) {

            HistoryDataModel dm = new HistoryDataModel();

            dm.setHeaderTitle("April 201" + i);

            ArrayList<String> singleItem = new ArrayList<>();
            for (int j = 0; j <= 4; j++) {

                if (j == 0)
                    singleItem.add("Credit transfer ");
                else if (j == 1)
                    singleItem.add("Recharge ");
                else if (j == 2)
                    singleItem.add("Recharge ");
                else if (j == 3)
                    singleItem.add("Unlimited Snapchat ");
                else if (j == 4)
                    singleItem.add("Credit transfer ");
            }

            dm.setAllItemsInSection(singleItem);

            allSampleData.add(dm);

        }
    }


    @Override
    public void onConsumeService() {
        super.onConsumeService();
        addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        frameLayout.setVisibility(View.VISIBLE);
        masterInputResponse = new MasterInputResponse();
        masterInputResponse.setLang(currentLanguage);
        masterInputResponse.setAppVersion(appVersion);
        masterInputResponse.setOsVersion(osVersion);
        masterInputResponse.setToken(token);
        masterInputResponse.setChannel(channel);
        masterInputResponse.setDeviceId(deviceId);
        masterInputResponse.setMsisdn(sharedPrefrencesManger.getMobileNo());
        masterInputResponse.setAuthToken(authToken);
        masterInputResponse.setImsi(sharedPrefrencesManger.getMobileNo());
        new HistoryListService(this, masterInputResponse);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
//        if (isVisible()) {
//            //  rechargeActivity.onBackPressed();
//            if (isVisible()) {
        try {
            onBackPressed();
            frameLayout.setVisibility(View.GONE);
            if(responseModel!=null && responseModel.getResultObj()!=null) {
                historyDataOutput = (HistoryDataOutput) responseModel.getResultObj();
                if (historyDataOutput != null) {
                    if (historyDataOutput.getMonthlyTransList() != null) {
                        if (historyDataOutput.getMonthlyTransList().length > 0)
                            onHistoryListVerificationSuccess();
                        else{
                            ll_emptyState.setVisibility(View.VISIBLE);
                            ll_main.setVisibility(View.GONE);
                        }
                    }
                    //   onRechargeListIDVerificationFail();
                    else {
                        onHistoryListVerificationFail();

                    }
                    // onRechargeListIDVerificationSuccess();
                    //    }
                    //   }

                }
            }
        }catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        try {
            frameLayout.setVisibility(View.GONE);

            onBackPressed();
            onHistoryListVerificationFail();
        }catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
//        if(currentLanguage.equals("en")) {
//            showErrorDalioge(historyDataOutput.getErrorMsgEn()+"");
//        }else{
//            showErrorDalioge(historyDataOutput.getErrorMsgAr()+"");
//        }
        //  if (isVisible())
        //   Toast.makeText(this, "fail" + status, Toast.LENGTH_SHORT).show();
        //  regActivity.onBackPressed();

//        showErrorFragment("Error in service.");
    }


    private void onHistoryListVerificationFail() {
        //  Toast.makeText(this, "fail" + status, Toast.LENGTH_SHORT).show();
        ll_emptyState.setVisibility(View.VISIBLE);
        ll_main.setVisibility(View.GONE);
        if (historyDataOutput != null && historyDataOutput.getErrorCode() != null) {
            if (currentLanguage.equals("en")) {
                showErrorFragment(historyDataOutput.getErrorMsgEn() + "");
            } else {
                showErrorFragment(historyDataOutput.getErrorMsgAr() + "");
            }
        } else {
            showErrorFragment(getString(R.string.generice_error));
        }

    }

    public void showErrorFragment(String error) {
        frameLayout.setVisibility(View.VISIBLE);
        Fragment errorFragmnet = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, error);
        errorFragmnet.setArguments(bundle);
        replaceFragmnet(errorFragmnet, R.id.frameLayout, true);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (frameLayout.getVisibility() == View.VISIBLE) {
            frameLayout.setVisibility(View.GONE);
        }
    }

    private void onHistoryListVerificationSuccess() {
        if (historyDataOutput.getMonthlyTransList().length > 0) {

            try {
                for (int i = 0; i < historyDataOutput.getMonthlyTransList().length; i++) {

                    HistoryDataModel dm = new HistoryDataModel();
                    if (currentLanguage.equalsIgnoreCase("en")) {
                        dm.setHeaderTitle(historyDataOutput.getMonthlyTransList()[i].getMonthEn());
                    } else {
                        dm.setHeaderTitle(historyDataOutput.getMonthlyTransList()[i].getMonthAr());
                    }
                    dm.setBalance(historyDataOutput.getBalance());
                    dm.setMonthlyTransList(historyDataOutput.getMonthlyTransList());

                    ArrayList<String> singleItem = new ArrayList<>();
                    for (int j = 0; j < historyDataOutput.getMonthlyTransList()[i].getTransactionList().length; j++) {

                        if (currentLanguage != null && currentLanguage.equals("en")) {
                            singleItem.add(historyDataOutput.getMonthlyTransList()[i].getTransactionList()[j].getNameEn());
                        } else if (currentLanguage != null && currentLanguage.equals("ar")) {
                            singleItem.add(historyDataOutput.getMonthlyTransList()[i].getTransactionList()[j].getNameAr());

                        }

                    }

                    dm.setAllItemsInSection(singleItem);

                    allSampleData.add(dm);
                }
            } catch (Exception ex) {

            }
            RecyclerViewSectionAdapter adapter = new RecyclerViewSectionAdapter(allSampleData, getApplicationContext());


            GridLayoutManager manager = new GridLayoutManager(this,
                    getResources().getInteger(R.integer.grid_span_1));


            my_recycler_view.setLayoutManager(manager);
            adapter.setLayoutManager(manager);
            my_recycler_view.setAdapter(adapter);

        }


    }
}
