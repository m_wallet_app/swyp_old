package com.indy.views.activites;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.indy.R;

public class LivPopupActivity extends MasterActivity implements View.OnClickListener {

    private Button closeButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_liv_popup);
        initViews();
    }

    private void initViews() {
        closeButton = (Button) findViewById(R.id.closeBtn);
        closeButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.closeBtn:
                setResult(RESULT_OK);
                finish();
                break;

        }
    }
}
