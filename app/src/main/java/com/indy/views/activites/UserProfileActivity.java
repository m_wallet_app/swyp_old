package com.indy.views.activites;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.adapters.GenderSpinnerAdapter;
import com.indy.controls.BaseInterface;
import com.indy.controls.ImageListinerInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.login.UserProfile;
import com.indy.models.profile.EditProfileInput;
import com.indy.models.profile.EditProfileOutput;
import com.indy.models.uploadimages.UploadImageOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.ImageItem;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.services.EditProfileGamificationService;
import com.indy.services.EditProfileService;
import com.indy.services.UploadUserProfileImage;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.utils.SharedPrefrencesManger;
import com.indy.views.fragments.gamification.models.serviceInputOutput.UpdateProfileGamificationInputModel;
import com.indy.views.fragments.login.LoginFragment;
import com.indy.views.fragments.utils.ErrorFragment;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;


/**
 * Created by emad on 9/26/16.
 */

public class UserProfileActivity extends AttachmentActivity implements BaseInterface, ImageListinerInterface,
        AdapterView.OnItemSelectedListener {


    private BottomSheetBehavior behavior;
    private CoordinatorLayout coordinatorLayout;
    private ImageView img, iv_profileImage;
    private ImageItem imageItem;
    private Spinner selectGenderSpinner;
    private Button btn_cancel, helpBtn, backImg;
    private ProgressBar progressBar;
    private UploadImageOutput uploadImageOutput;
    private TextView tv_titleText;
    private CardView cardView;
    private EditText emailAddress, nickName;
    private TextView password;
    private UserProfile userProfile;
    private Context mContext;
    private TextView mobileNO;
    private TextView user_name;
    private LinearLayout savePhoto;
    private TextView iv_subscription_days;
    private TextView tv_membershipCycleText;
    EditProfileInput editProfileInput;
    private String[] fNickName;
    File fileForDelete = null;
    public SharedPrefrencesManger sharedPrefrencesManger;
    //    String serviceType ="abc";
    GenderSpinnerAdapter adapter;
    TextView btn_saveProfile;
    private EditProfileOutput editProfileOutput;
    public static UserProfileActivity userProfileActivityInstance;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        initUI();
        setListeners();


    }

    @Override
    public void initUI() {
        super.initUI();
        loadSpinnerItems();
        CommonMethods.setStatusBarTransparent(this);
        sharedPrefrencesManger = new SharedPrefrencesManger(this);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinateLayoutID);
        View bottomSheet = coordinatorLayout.findViewById(R.id.editemail_bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);
        img = (ImageView) findViewById(R.id.imgBg);
        progressBar = (ProgressBar) findViewById(R.id.progressID);
        selectGenderSpinner = (Spinner) findViewById(R.id.selectGenderSpinner);
        iv_profileImage = (ImageView) findViewById(R.id.iv_profile_image);
        btn_cancel = (Button) findViewById(R.id.cancelBtn);
        tv_titleText = (TextView) findViewById(R.id.titleTxt);
        backImg = (Button) findViewById(R.id.backImg);
        helpBtn = (Button) findViewById(R.id.helpBtn);
        mobileNO = (TextView) findViewById(R.id.mobileNO);
        user_name = (TextView) findViewById(R.id.user_name);
        savePhoto = (LinearLayout) findViewById(R.id.ll_save_photo);
        iv_subscription_days = (TextView) findViewById(R.id.iv_subscription_days);
        tv_membershipCycleText = (TextView) findViewById(R.id.tv_membership_cycle_text);
        tv_titleText.setText(getString(R.string.edit_profile));
        tv_titleText.setTextColor(Color.WHITE);
        this.mContext = getApplicationContext();
        emailAddress = (EditText) findViewById(R.id.emailAddress_user_profile);
        password = (TextView) findViewById(R.id.password_user_profile);
        nickName = (EditText) findViewById(R.id.nick_user_profile);
        btn_saveProfile = (TextView) findViewById(R.id.btn_saveProfile);

        userProfileActivityInstance = this;
        btn_saveProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //                serviceType = ServiceUtils.updateProfile;
                onConsumeService();
            }
        });
        //        if (sharedPrefrencesManger != null) {
        //            if (sharedPrefrencesManger.getUserProfileObject().getMembershipData().getStatus() != null) {
        //                if (sharedPrefrencesManger.getUserProfileObject().getMembershipData().getStatus() == ConstantUtils.inValidMembership) { // for invalid member ship
        //                    iv_subscription_days.setVisibility(View.VISIBLE);
        //                } else { // for valid member ship
        //                    iv_subscription_days.setVisibility(View.INVISIBLE);
        //                }
        //
        //            } else {
        //                iv_subscription_days.setVisibility(View.INVISIBLE);
        //            }
        //        } else {
        //            iv_subscription_days.setVisibility(View.INVISIBLE);
        //        }
        if (sharedPrefrencesManger != null)
            if (sharedPrefrencesManger.getUserProfileObject() != null) {
                if (sharedPrefrencesManger.getUserProfileObject().getMembershipData() != null) {
                    if (sharedPrefrencesManger.getUserProfileObject().getMembershipData().getStatus() == ConstantUtils.validMembership) {
                        iv_subscription_days.setText(/*DateHelpers.getExpirtyDate(*/" " + sharedPrefrencesManger.getUserProfileObject().getMembershipData().getExpiryDate()/*)*/);
                        tv_membershipCycleText.setVisibility(View.VISIBLE);
                    } else {
                        iv_subscription_days.setText(getString(R.string.no_valid_membership));
                        tv_membershipCycleText.setVisibility(View.GONE);
                    }
                }

            }
        cardView = (CardView) findViewById(R.id.currentPassord_Card);
//        password_user_profile

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onEditPassword();

            }
        });
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        helpBtn.setVisibility(View.GONE);

        password.setText("******");// dummy text
        password.setAlpha(1.0f);
//        password.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
////                onEditPassword();
//                return false;
//            }
//        });
        if (sharedPrefrencesManger != null)
            userProfile = sharedPrefrencesManger.getUserProfileObject();
        //        if (userProfile != null) {
        if (sharedPrefrencesManger.getUserProfileImage() != null && !sharedPrefrencesManger.getUserProfileImage().isEmpty()) {
            Picasso.with(mContext).load(MyEndPointsInterface.imgBaseUrl + sharedPrefrencesManger.getUserProfileImage()).
                    networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE).into(img);
            Picasso.with(mContext).
                    load(MyEndPointsInterface.imgBaseUrl + sharedPrefrencesManger.getUserProfileImage())
                    .networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE)
                    .
                            into(iv_profileImage);
        }
        mobileNO.setEnabled(false);
        if (sharedPrefrencesManger.getMobileNo() != null && !sharedPrefrencesManger.getMobileNo().isEmpty())
            mobileNO.setText(sharedPrefrencesManger.getMobileNo());

        if (sharedPrefrencesManger.getUserProfileObject() != null) {
            if (sharedPrefrencesManger.getUserProfileObject().getNickName() != null && !sharedPrefrencesManger.getUserProfileObject().getNickName().isEmpty()) {
//                if (sharedPrefrencesManger.getUserProfileObject().getNickName().contains(" ")) {
//                    fNickName = sharedPrefrencesManger.getUserProfileObject().getNickName().split(" ");
//                    user_name.setText(fNickName[0]);
//                } else {
                user_name.setText(sharedPrefrencesManger.getUserProfileObject().getNickName());
//                }
            } else {
                if (sharedPrefrencesManger.getUserProfileObject().getFullName() != null && !sharedPrefrencesManger.getUserProfileObject().getFullName().isEmpty()) {
//                    if (sharedPrefrencesManger.getUserProfileObject().getFullName().contains(" ")) {
//                        fNickName = sharedPrefrencesManger.getUserProfileObject().getFullName().split(" ");
//                        user_name.setText(fNickName[0]);
//                    } else {
                    user_name.setText(sharedPrefrencesManger.getUserProfileObject().getFullName());
//                    }
                }
            }
            //            if (sharedPrefrencesManger.getFullName().length() < 13) {
            //                user_name.setText(sharedPrefrencesManger.getFullName());
            //            } else {
            //                user_name.setText(sharedPrefrencesManger.getFullName().substring(0, 12) + "...");
            //            }
        }
        if(sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)){
            user_name.setLineSpacing(0,1f);
        }else{
            user_name.setLineSpacing(10,1.4f);
        }
        if (sharedPrefrencesManger.getNickName() != null && !sharedPrefrencesManger.getNickName().isEmpty())
            nickName.setText(sharedPrefrencesManger.getNickName());
        if (sharedPrefrencesManger.getEmail() != null && !sharedPrefrencesManger.getEmail().isEmpty())
            emailAddress.setText(sharedPrefrencesManger.getEmail());
        if (sharedPrefrencesManger.getGender() != null && !sharedPrefrencesManger.getGender().isEmpty()) {
            SetSpinnerItems(sharedPrefrencesManger.getGender());
            //            if(userProfile.getGender().equals("Male")){
            //                selectGenderSpinner.setSelection(selectGenderSpinner.getSelectedItemPosition());
            //            }else {
            //                selectGenderSpinner.setSelection(selectGenderSpinner.getSelectedItemPosition());
            //            }

        }

        nickName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(!b){
                    Bundle bundle2 = new Bundle();

                    bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_edit_profile_change_nickname);
                    bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_edit_profile_change_nickname);
                    mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_edit_profile_change_nickname, bundle2);
                }
            }
        });

        //        }

        //        serviceType = ServiceUtils.UploadImageInput;
        helpBtn.setVisibility(View.GONE);
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();

        progressBar.setVisibility(View.VISIBLE);
        editProfileInput = new EditProfileInput();
        editProfileInput.setRegistrationId(sharedPrefrencesManger.getRegisterationID());

        editProfileInput.setUserName(nickName.getText().toString());
        editProfileInput.setGender(selectGenderSpinner.getSelectedItem().toString());
        editProfileInput.setEmail(emailAddress.getText().toString());
        editProfileInput.setPreferedLanguage(sharedPrefrencesManger.getLanguage());
        editProfileInput.setLang(sharedPrefrencesManger.getLanguage());
        editProfileInput.setAppVersion(appVersion);
        editProfileInput.setOsVersion(osVersion);
        editProfileInput.setToken(token);
        editProfileInput.setChannel(channel);
        editProfileInput.setDeviceId(deviceId);
        editProfileInput.setMsisdn(sharedPrefrencesManger.getMobileNo());
        editProfileInput.setImsi(sharedPrefrencesManger.getMobileNo());
        editProfileInput.setAuthToken(authToken);

        new EditProfileService(this, editProfileInput);

    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            if (responseModel != null && responseModel.getResultObj() != null) {
                if ((responseModel.getServiceType().equals(ServiceUtils.updateProfile))) {


                    CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(),ConstantUtils.TAGGING_edit_profile_save);


//                    onBackPressed();
//                    progressBar.setVisibility(View.GONE);
                    editProfileOutput = (EditProfileOutput) responseModel.getResultObj();
                    if (editProfileOutput.getErrorCode() != null)
                        onUpdateUserProfileError();
                    else
                        onUpdateUserProfileSucess();
                } else if (responseModel.getServiceType().equals(ServiceUtils.UploadImageInput)) {
                    progressBar.setVisibility(View.GONE);
                    uploadImageOutput = (UploadImageOutput) responseModel.getResultObj();
                    if (uploadImageOutput != null)
                        if (uploadImageOutput.getErrorCode() != null)
                            onUploadImageError();
                        else
                            onUploadImageSucess();
                } else if(responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.updateProfileGamification)){
                    onBackPressed();
                    progressBar.setVisibility(View.GONE);
                    finish();
                }
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        onBackPressed();
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    private void onUpdateUserProfileSucess() {
        //        sharedPrefrencesManger.setPassword(password.getText().toString().trim());
        sharedPrefrencesManger.setRegisterationID(editProfileOutput.getUserProfile().getRegistrationId());
        sharedPrefrencesManger.setEmail(editProfileOutput.getUserProfile().getEmail());
        sharedPrefrencesManger.setNickName(editProfileOutput.getUserProfile().getNickName());
        sharedPrefrencesManger.setGender(editProfileOutput.getUserProfile().getGender());
        sharedPrefrencesManger.setUserProfileObj(editProfileOutput.getUserProfile());

        logTalosUpdateProfileEvent();

    }

    private void logTalosUpdateProfileEvent() {
        progressBar.setVisibility(View.VISIBLE);
        UpdateProfileGamificationInputModel updateProfileGamificationInputModel = new UpdateProfileGamificationInputModel();
        updateProfileGamificationInputModel.setRegistrationId(sharedPrefrencesManger.getRegisterationID());

        updateProfileGamificationInputModel.setUserName(nickName.getText().toString());
        updateProfileGamificationInputModel.setGender(selectGenderSpinner.getSelectedItem().toString());
        updateProfileGamificationInputModel.setEmail(emailAddress.getText().toString());
        updateProfileGamificationInputModel.setPreferedLanguage(sharedPrefrencesManger.getLanguage());
        updateProfileGamificationInputModel.setLang(sharedPrefrencesManger.getLanguage());
        updateProfileGamificationInputModel.setAppVersion(appVersion);
        updateProfileGamificationInputModel.setOsVersion(osVersion);
        updateProfileGamificationInputModel.setToken(token);
        updateProfileGamificationInputModel.setChannel(channel);
        updateProfileGamificationInputModel.setDeviceId(deviceId);
        updateProfileGamificationInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        updateProfileGamificationInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
        updateProfileGamificationInputModel.setAuthToken(authToken);
        updateProfileGamificationInputModel.setApplicationId(ConstantUtils.INDY_TALOS_ID);
        updateProfileGamificationInputModel.setUserId(sharedPrefrencesManger.getUserId());
        updateProfileGamificationInputModel.setUserSessionId(sharedPrefrencesManger.getUserSessionId());
        new EditProfileGamificationService(this, updateProfileGamificationInputModel);

    }

    private void onUpdateUserProfileError() {
        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(),ConstantUtils.TAGGING_edit_profile_save_failure);

        if (currentLanguage != null) {
            if (currentLanguage.equals("en"))
                showErrorFragment(editProfileOutput.getErrorMsgEn());
            else
                showErrorFragment(editProfileOutput.getErrorMsgAr());
        } else {
            showErrorFragment(editProfileOutput.getErrorMsgEn());
        }
    }

    private void onUploadImageError() {
        showErrorFragment(uploadImageOutput.getErrorMsgEn());
    }

    private void onUploadImageSucess() {

        if (uploadImageOutput.getUpdated()) {
            img.setImageBitmap(imageItem.getBitmap());
            iv_profileImage.setImageBitmap(imageItem.getBitmap());

            sharedPrefrencesManger.setUserProfileImage(uploadImageOutput.getProfilePicUrl());
            if (SwpeMainActivity.imgBg != null)
                SwpeMainActivity.imgBg.setImageBitmap(imageItem.getBitmap());
        } else {
            showErrorFragment(getString(R.string.upload_image_fail));
        }
    }

    private void showErrorFragment(String errorString) {
        Fragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, errorString);
        errorFragment.setArguments(bundle);
        replaceFragmnet(errorFragment, R.id.frameLayout, true);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        try {
            //        progressBar.setVisibility(View.GONE);
            if(responseModel!=null && responseModel.getResultObj()!=null) {
                if ((responseModel.getServiceType().equals(ServiceUtils.updateProfile))) {
                    onBackPressed();
                    progressBar.setVisibility(View.GONE);

                } else {
                    progressBar.setVisibility(View.GONE);
                }
            }
        }catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }


    public void setListeners() {
        hideKeyBoard();
        emailAddress.clearFocus();
        nickName.clearFocus();
        findViewById(R.id.cameraID).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disableSavePhoto();
                onCameraSelected();
            }
        });
        findViewById(R.id.takePhoto).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SplashActivity.service != null) {
                    SplashActivity.stopMyService();
                } else {
                    LoginFragment.stopMyService();
                }
                onTakePhoto();


            }
        });
        findViewById(R.id.chooseFromGallary).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SplashActivity.service != null) {
                    SplashActivity.stopMyService();
                } else {
                    LoginFragment.stopMyService();
                }
                onChooseFromGallary();

            }
        });
        findViewById(R.id.deletePhoto).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                onDeletePhoto();
                Bundle bundle2 = new Bundle();

                bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_edit_profile_change_picture_delete);
                bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_edit_profile_change_picture_delete);
                mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_edit_profile_change_picture_delete, bundle2);
            }
        });

        findViewById(R.id.ll_save_photo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                if (imageItem != null)
                    if (imageItem.getImgUrl() != null)
                        onSavePhoto();

            }
        });
        findViewById(R.id.cancelBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

            }
        });
    }

    private void onCameraSelected() {
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    private void onEditPassword() {

        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(),ConstantUtils.TAGGING_edit_profile_change_password);
        Intent intent = new Intent(this, ChangePasswordActivity.class);
        intent.putExtra(ConstantUtils.userProfile, userProfile);
        startActivity(intent);
    }

    private void onTakePhoto() {

        setImageListinerInterface(UserProfileActivity.this, 0);
        imgManage.getImg(PIC_FROM_CAMERA);
    }

    private void onDeletePhoto() {
        onDeleteImage(new ArrayList<ImageItem>(), 0);
    }


    private void onSavePhoto() {
        progressBar.setVisibility(View.VISIBLE);
        Bundle bundle2 = new Bundle();

        bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_edit_profile_change_picture_save);
        bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_edit_profile_change_picture_save);
        mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_edit_profile_change_picture_save, bundle2);
        //        serviceType = ServiceUtils.UploadImageInput;
        new UploadUserProfileImage(this, imageItem.getUri(), getApplicationContext(), sharedPrefrencesManger.getRegisterationID(),
                sharedPrefrencesManger.getUserProfileObject().getMsisdn(), sharedPrefrencesManger.getToken(),
                sharedPrefrencesManger.getDeviceId(), sharedPrefrencesManger.getAuthToken(), fileForDelete, channel,
                sharedPrefrencesManger.getUserProfileObject().getMsisdn(), sharedPrefrencesManger.getLanguage(), osVersion, appVersion);
    }

    private void onChooseFromGallary() {
        setImageListinerInterface(UserProfileActivity.this, 0);
        imgManage.getImg(PIC_FROM_Gallary);

    }

    @Override
    public void onImageSet(ImageItem imageItem) {
        if (imageItem != null) {
            enableSavePhoto();
            iv_profileImage.setImageBitmap(imageItem.getBitmap());
        } else {
            iv_profileImage.setImageResource(R.drawable.bitmap_default_image);
            //            iv_profileImage.setImageResource(R.drawable.bitmap_default_image);
        }
        if (SplashActivity.service != null) {
            SplashActivity.startMyService();
        } else if (LoginFragment.service != null) {
            LoginFragment.startMyService();
        }
        this.imageItem = imageItem;
    }

    @Override
    public void onDeleteImage(ArrayList<ImageItem> imageItems, int imgPosition) {
        img.setImageResource(R.drawable.bitmap_default_image);
        iv_profileImage.setImageResource(R.drawable.bitmap_default_image);
        progressBar.setVisibility(View.VISIBLE);
        //        serviceType = ServiceUtils.UploadImageInput;
        Uri imageUri = Uri.parse("android.resource://" + getPackageName() + "/" + "drawable/bitmap_default_image");
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
                R.drawable.bitmap_default_image);
        String path = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_MOVIES).getPath();
        fileForDelete = new File(path, "/" + "name.png");

        try {


            InputStream stream = getContentResolver().openInputStream(imageUri);

            OutputStream out = new FileOutputStream(fileForDelete);
            bitmap.compress(Bitmap.CompressFormat.PNG, 1, out);
            byte buf[] = new byte[1024];
            int len;
            while ((len = stream.read(buf)) > 0)
                out.write(buf, 0, len);
            out.close();
            stream.close();


        } catch (Exception e) {
            e.printStackTrace();
        }
        Uri imageUriDummy = null;
        imageItem = new ImageItem();
        imageItem.setUri(null);
        imageItem.setPosition(0);
        imageItem.setBitmap(bitmap);
        new UploadUserProfileImage(this, imageUriDummy,
                getApplicationContext(), sharedPrefrencesManger.getRegisterationID(),
                sharedPrefrencesManger.getUserProfileObject().getMsisdn(), sharedPrefrencesManger.getToken(),
                sharedPrefrencesManger.getDeviceId(), sharedPrefrencesManger.getAuthToken(), fileForDelete, channel,
                sharedPrefrencesManger.getUserProfileObject().getMsisdn(), sharedPrefrencesManger.getLanguage(), osVersion,
                appVersion);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if ((parent.getChildAt(0)) != null)
            ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        if ((parent.getChildAt(0)) != null)
            ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
    }

    private void loadSpinnerItems() {
        selectGenderSpinner = (Spinner) findViewById(R.id.selectGenderSpinner);
        adapter = new GenderSpinnerAdapter(this, R.layout.item_spinner);
        selectGenderSpinner.setAdapter(adapter);
    }


    private void SetSpinnerItems(String gender) {
        if (gender.equalsIgnoreCase(getString(R.string.male))) {
            selectGenderSpinner.setSelection(0);
        } else {
            selectGenderSpinner.setSelection(1);
        }
    }


    private void enableSavePhoto() {
        savePhoto.setAlpha(1f);
        savePhoto.setClickable(true);
    }

    private void disableSavePhoto() {
        savePhoto.setAlpha(.5f);
        savePhoto.setClickable(false);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

}
