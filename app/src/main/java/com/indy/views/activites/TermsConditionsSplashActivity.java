package com.indy.views.activites;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.AboutUs.TermsAndConditionsWelcome;

public class TermsConditionsSplashActivity extends MasterActivity {


    TextView tv_terms, tv_btn;
    String index;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_conditions_splash);
        initUI();
    }

    @Override
    public void initUI() {
        super.initUI();

        tv_terms = (TextView) findViewById(R.id.tv_terms);
        tv_btn = (TextView) findViewById(R.id.agree_btn);
        index = getIntent().getStringExtra("fragIndex");
        tv_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TermsConditionsSplashActivity.this, RegisterationActivity.class);
                if(index!=null && index.length()>0)
                intent.putExtra("fragIndex",index);

                startActivity(intent);
                sharedPrefrencesManger.setIsTermsAccepted(true);
                finish();
            }
        });

        tv_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TermsConditionsSplashActivity.this, TermsAndConditionsWelcome.class);
                startActivity(intent);
                Bundle bundle = new Bundle();

                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_welcome_agree_t_and_c);
                bundle.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_welcome_agree_t_and_c);
                mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_welcome_agree_t_and_c, bundle);
            }
        });

        Bundle bundle = new Bundle();

        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_welcome_terms_and_conditions);
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_welcome_terms_and_conditions);
        mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_welcome_terms_and_conditions, bundle);
    }
}
