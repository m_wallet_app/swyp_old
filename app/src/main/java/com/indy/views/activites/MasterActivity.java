package com.indy.views.activites;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.adjust.sdk.Adjust;
import com.adjust.sdk.AdjustEvent;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.controls.BaseInterface;
import com.indy.helpers.AppFont;
import com.indy.helpers.RetrofitErrorHandeler;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.utils.IndyApplication;
import com.indy.utils.SharedPrefrencesManger;

import java.util.List;
import java.util.Locale;
import java.util.Set;

import retrofit2.Call;

import static com.indy.views.activites.RegisterationActivity.isAnimationCustom;


/**
 * Created by emad on 8/2/16.
 */
public class MasterActivity extends AppCompatActivity implements BaseInterface {
    private FragmentTransaction transaction;
    private RetrofitErrorHandeler retrofitErrorHandeler;
    public String appVersion, token, osVersion, channel, deviceId, currentLanguage, authToken;
    protected SharedPrefrencesManger sharedPrefrencesManger;
    public Call mCall;
    public static boolean isAppWentToBg = true;

    public static boolean isWindowFocused = false;

    public static boolean isMenuOpened = false;

    public static boolean isBackPressed = false;
    protected static final String TAG = MasterActivity.class.getName();

    protected FirebaseAnalytics mFirebaseAnalytics;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPrefrencesManger = ((IndyApplication) getApplicationContext()).sharedPrefrencesManger;
        if(sharedPrefrencesManger!=null) {
            switchLocalizaion(sharedPrefrencesManger.getLanguage(), null);
        }

//        initUi();
//            initUI();
    }

    public FirebaseAnalytics getmFirebaseAnalytics() {
        return mFirebaseAnalytics;
    }

    public void setmFirebaseAnalytics(FirebaseAnalytics mFirebaseAnalytics) {
        this.mFirebaseAnalytics = mFirebaseAnalytics;
    }

    public void replaceFragmnet(Fragment fragment, int container, boolean addTobackStack) {
        try {
            transaction = getSupportFragmentManager().beginTransaction();
            FragmentManager fm = getSupportFragmentManager(); // didnot add fragment in back stack twice.
            if (isAnimationCustom) {
                transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.enter_from_right, R.anim.enter_from_right, R.anim.enter_from_right);
                isAnimationCustom = false;
            }
            for (int entry = 0; entry < fm.getBackStackEntryCount(); entry++) {
                try {
                    if (fm.getFragments().get(entry).getClass().equals(fragment.getClass())) {
                        removeExisitingFragment(fragment);
                    }
                }catch (Exception ex){
                        ex.printStackTrace();
                }
            }
            if (!addTobackStack) {   // if fragment must add to back stack
                transaction.replace(container, fragment,
                        "").commit();
            } else {
                transaction.replace(container, fragment,
                        "").addToBackStack(null).commit();
            }
            getCurrentFragment();
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }
    }

    public void addFragmnet(Fragment fragment, int container, boolean addTobackStack) {
        try {
            transaction = getSupportFragmentManager().beginTransaction();
            if (!addTobackStack) {   // if fragment must add to back stack
                transaction.add(container, fragment,
                        "").commit();
            } else {
                transaction.add(container, fragment,
                        "").addToBackStack(null).commit();
            }
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }
    }

    @Override
    public void initUI() {
        sharedPrefrencesManger = new SharedPrefrencesManger(this);
        appVersion = sharedPrefrencesManger.getAppVersion();
        token = sharedPrefrencesManger.getToken();
        osVersion = ConstantUtils.osVersion;
        channel = ConstantUtils.channel;
        currentLanguage = sharedPrefrencesManger.getLanguage();
        deviceId = sharedPrefrencesManger.getDeviceId();
        authToken = sharedPrefrencesManger.getAuthToken();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        Log.d("Token:", ""+sharedPrefrencesManger.getToken());
        CommonMethods.firebaseAnalytics = mFirebaseAnalytics;
        CommonMethods.setStatusBarTransparent(this);
    }

    public void removeFragment(Fragment[] myFrag) {
        try {
            for (Fragment fragment : myFrag) {
                FragmentManager manager = getSupportFragmentManager();
                FragmentTransaction trans = manager.beginTransaction();
                trans.remove(fragment);
                trans.commit();
                manager.popBackStack();
            }
        }catch (Exception ex){
            CommonMethods.logException(ex);
        }
    }

    public void removeExisitingFragment(Fragment myFrag) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction trans = manager.beginTransaction();
        trans.remove(myFrag);
        trans.commit();
        manager.popBackStack();
    }

    public void hideKeyBoard() {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void hideKeyBoard(View parentLayout) {
        if (parentLayout != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(parentLayout.getWindowToken(), 0);
        }
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {

    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        Intent intent = new Intent(MasterActivity.this, UnAuthorizedTokenActivity.class);
        if (masterErrorResponse != null) {

            if (masterErrorResponse.getErrorCode() != null) {

                if (currentLanguage != null) {
                    if (currentLanguage.equalsIgnoreCase("en"))
                        intent.putExtra("msg", masterErrorResponse.getErrorMsgEn());
                    else
                        intent.putExtra("msg", masterErrorResponse.getErrorMsgAr());

                } else {
                    intent.putExtra("msg", getString(R.string.generice_error));

                }

            }
        }
        startActivity(intent);
    }


    @Override
    public void onErrorListener(BaseResponseModel responseModel) {

        Throwable t = (Throwable) responseModel.getResultObj();
        retrofitErrorHandeler = new RetrofitErrorHandeler();
        retrofitErrorHandeler.handleError(t, getApplicationContext());
        if(RegisterationActivity.logEventInputModel!=null){
            if(responseModel.getResultObj()!=null){
                try {
                    MasterErrorResponse masterErrorResponse = (MasterErrorResponse) responseModel.getResultObj();
                    if (masterErrorResponse != null && masterErrorResponse.getErrorMsgEn() != null) {

                        RegisterationActivity.logEventInputModel.setErrorCode(masterErrorResponse.getErrorMsgEn());

                    }
                }catch (Exception ex){

                }
            }

        }
    }

//    public void initUi() {
//        sharedPrefrencesManger = new SharedPrefrencesManger(this);
//        appVersion = sharedPrefrencesManger.getAppVersion();
//        token = sharedPrefrencesManger.getToken();
//        osVersion = ConstantUtils.osVersion;
//        channel = ConstantUtils.channel;
//        deviceId = sharedPrefrencesManger.getDeviceId();
//        authToken = sharedPrefrencesManger.getAuthToken();
//        isAppWentToBg = true;
////        CommonMethods.setStatusBarTransparent(this);
//// ...
//
//
//// Obtain the FirebaseAnalytics instance.
//        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
//        CommonMethods.firebaseAnalytics = mFirebaseAnalytics;
//        Log.v("deviceID", deviceId + " ");
//        Log.v("tokenID", token + " ");
//    }

    @Override
    public void onConsumeService() {
        hideKeyBoard();
    }


    public void showErrorDalioge(String msg) {


    }
//    @Override
//    protected void onStop() {
//        super.onStop();
//        if(isApplicationBroughtToBackground(this)){
//            Toast.makeText(this, "Appp gone to background!", Toast.LENGTH_SHORT).show();
//        }
//    }

    public boolean isApplicationBroughtToBackground(
            final Activity activity) {
        ActivityManager activityManager = (ActivityManager) activity
                .getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = activityManager
                .getRunningTasks(1);

        // Check the top Activity against the list of Activities contained in //
        // the Application's package.
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            try {
                PackageInfo pi = activity.getPackageManager().getPackageInfo(
                        activity.getPackageName(),
                        PackageManager.GET_ACTIVITIES);
                for (ActivityInfo activityInfo : pi.activities) {
                    if (topActivity.getClassName().equals(activityInfo.name)) {
                        return false;
                    }
                }
            } catch (PackageManager.NameNotFoundException e) {
                return false;

                // Never happens.
            }
        }
        return true;
    }


    public void setTextInputLayoutError(TextInputLayout textInputLayoutError, String error) {
        textInputLayoutError.setError(error);
    }

    public void removeTextInputLayoutError(TextInputLayout textInputLayoutError) {
        textInputLayoutError.setError(null);
    }

    private Fragment getCurrentFragment() {


        return null;
    }

//    public static int isApplicationSentToBackground(final Context context) {
//        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
//        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
//        if (!tasks.isEmpty()) {
//            ComponentName topActivity = tasks.get(0).topActivity;
//            if (!topActivity.getPackageName().contains(context.getApplicationContext().getPackageName())
//            && !context.getPackageName().contains(context.getApplicationContext().getPackageName())) {
//                return 0;
//            }else if(topActivity.getPackageName().contains(context.getApplicationContext().getPackageName())
//                    && context.getPackageName().contains(context.getApplicationContext().getPackageName())){
//                return -1;
//            } else if(!topActivity.getPackageName().contains(context.getApplicationContext().getPackageName())
//                    && context.getPackageName().contains(context.getApplicationContext().getPackageName())){
//                return 1;
//            }
//        }
//        return -1;
//    }


    @Override
    public void onBackPressed() {
        try {
            super.onBackPressed();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void switchLocalizaion(String language, MasterActivity masterActivity) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        AppFont._appFont = null;
//        changeLocale(locale,this);

        Configuration config = new Configuration();
        config.locale = locale;
        getResources().updateConfiguration(config,
                getResources().getDisplayMetrics());

    }

//    private void changeLocale(@NonNull final Locale locale, Context context)
//    {
//        final Resources res = context.getResources();
//        final Configuration config = res.getConfiguration();
//
//        config.setLocale(locale);
//        final Context newContext = context.createConfigurationContext(config);
//        // What should I do with the newContext???
//        recreate();
////        super.attachBaseContext(newContext);
//    }


//    @Override
//    protected void attachBaseContext(Context newBase) {
//
//
//        if(Build.VERSION.SDK_INT>=25) {
//            Locale newLocale = null;
//            // .. create or get your new Locale object here.
//            sharedPrefrencesManger = new SharedPrefrencesManger(newBase);
//            if (sharedPrefrencesManger.getLanguage().equalsIgnoreCase("en")) {
//                newLocale = new Locale("en", "US");
//
//            } else if (sharedPrefrencesManger.getLanguage().equalsIgnoreCase("ar")) {
//                newLocale = new Locale("ar", "AE");
//            }
//
//            Context context = ContextWrapper.wrap(newBase, newLocale);
//            super.attachBaseContext(context);
//        }else{
//            super.attachBaseContext(newBase);
//        }
//
//
//    }

    /**
     * log event on Adjust
     *
     * @param eventTag event tag
     * @param bundle   bundle to set params
     */
    public void logEvent(String eventTag, Bundle bundle) {
        try {
            //Adjust Event
            AdjustEvent event = new AdjustEvent(eventTag);
            if (bundle != null && !bundle.isEmpty()) {
                Set<String> keys = bundle.keySet();
                for (String key : keys) {
                    if (bundle.getString(key) != null)
                        event.addPartnerParameter(key, bundle.getString(key));
                }
            }
            Adjust.trackEvent(event);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
