//package com.indy.views.activites;
//
//import android.app.Notification;
//import android.app.NotificationManager;
//import android.app.PendingIntent;
//import android.content.Context;
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.app.NotificationCompat;
//import android.view.View;
//import android.webkit.WebSettings;
//import android.webkit.WebView;
//import android.webkit.WebViewClient;
//import android.widget.Button;
//import android.widget.ProgressBar;
//
//import com.indy.MainActivity;
//import com.indy.R;
//import com.indy.models.utils.BaseResponseModel;
//
///**
// * Created by emad on 8/16/16.
// */
//public class PaymentToCompleteActivity extends MasterActivity {
//
//    private WebView paymentWebView;
//    private ProgressBar progressID;
//    Button pay, cancel;
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_tocomplete_payment);
//        initUI();
//    }
//
//    @Override
//    public void initUI() {
//        super.initUI();
//        paymentWebView = (WebView) findViewById(R.id.paymentWebView);
//        progressID = (ProgressBar) findViewById(R.id.progressID);
//        pay = (Button) findViewById(R.id.pay);
//        cancel = (Button) findViewById(R.id.cancel);
//        WebSettings settings = paymentWebView.getSettings();
//        settings.setJavaScriptEnabled(true);
//        paymentWebView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
//        loadPaymnetURL("");
////        new InilizePaymentService(this);
//        cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });
//        pay.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                generateLocatalNotification();
//                finish();
//                Intent intent = new Intent(PaymentToCompleteActivity.this, MainActivity.class);
//                startActivity(intent);
//                RegisterationActivity.regInstance.finish();
//
//            }
//        });
//    }
//
//    @Override
//    public void onSucessListener(BaseResponseModel responseModel) {
//        super.onSucessListener(responseModel);
////        PaymnetModelResponse paymnetModelResponse = (PaymnetModelResponse) responseModel.getResultObj();
////        if (paymnetModelResponse != null)
//        loadPaymnetURL("");
//    }
//
//    @Override
//    public void onErrorListener(BaseResponseModel responseModel) {
//        super.onErrorListener(responseModel);
//        progressID.setVisibility(View.GONE);
//    }
//
//    private void loadPaymnetURL(String paymnetURL) {
//        paymentWebView.setWebViewClient(new WebViewClient() {
//            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                view.loadUrl(url);
//                return true;
//            }
//
//            public void onPageFinished(WebView view, String url) {
//                progressID.setVisibility(View.GONE);
//            }
//
//            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//
//            }
//        });
//        paymentWebView.loadUrl("https://onlineservices.etisalat.ae/scp/registration/etisalatCustomerRegistration.jsp?qp_an=&qp_st=");
//    }
//
//    private void generateLocatalNotification() {
//        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
//        PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
//
//        NotificationCompat.Builder b = new NotificationCompat.Builder(getApplicationContext());
//
//        b.setAutoCancel(true)
//                .setDefaults(Notification.DEFAULT_ALL)
//                .setWhen(System.currentTimeMillis())
//                .setSmallIcon(R.drawable.ic_logo)
//                .setTicker("INDY")
//                .setContentTitle("Indy Mobile App")
//                .setContentText("Your Paymnet has been done sucessfully")
//                .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND)
//                .setContentIntent(contentIntent)
//                .setContentInfo("Info");
//
//
//        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
//        notificationManager.notify(1, b.build());
//    }
//}
