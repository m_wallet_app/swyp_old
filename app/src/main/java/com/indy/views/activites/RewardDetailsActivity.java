package com.indy.views.activites;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.text.util.LinkifyCompat;
import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.util.Linkify;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.indy.R;
import com.indy.controls.DailogeInterface;
import com.indy.controls.ServiceUtils;
import com.indy.customviews.CustomEditText;
import com.indy.customviews.PicassoMarker;
import com.indy.helpers.GoogleMapsHelper;
import com.indy.models.redeemVoucher.RedeemVoucherInput;
import com.indy.models.redeemVoucher.RedeemVoucherOutput;
import com.indy.models.rewards.RewardsList;
import com.indy.models.rewards.StoreLocation;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.models.voucherCode.vouchercodeInput;
import com.indy.models.voucherCode.vouchercodeOutput;
import com.indy.services.RedeemVoucherService;
import com.indy.services.VoucherCodeService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.utils.GPSTracker;
import com.indy.utils.SharedPrefrencesManger;
import com.indy.views.fragments.rewards.ItemsCarousel.MainAvailableRewardsFragment;
import com.indy.views.fragments.rewards.UsedRewardsFragment;
import com.indy.views.fragments.rewards.VoucherSucessfullyRedeemedFragment;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Amir.jehangir on 10/25/2016.
 */
public class RewardDetailsActivity extends MasterActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleMap.OnInfoWindowClickListener, LocationListener, DailogeInterface {

    TextView DealName, numberofdeals, voucherCodetext, voucherCode_vc;
    ImageView shopImage;
    TextView Discount, Expire;
    Button btn_set_Voucher;
    WebView webviewDetails;
    TextView shopNameChange, ChangeShop, distanceKm;
    Button btn_redeem_now, btn_use_voucher;
    public GoogleMap mMap;
    public MarkerOptions marker;
    LocationManager locationManager;
    GPSTracker gpsTracker;
    Location mlocation;
    int total, count;
    public static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    ArrayList<StoreLocation> mStoreLocationsList;
    public static final int LOCATION_REQUEST = 1340;
    GoogleMapsHelper googleMapsHelper;
    String icon_url;

    RelativeLayout mapframe;
    LinearLayout RL_Voucher_view, RL_Voucher_Added_bottomheet;
    ScrollView nested_scroll;


    CustomEditText code1, code2, code3, code4;
    public static int[] tvListIds = {R.id.code_1, R.id.code_2, R.id.code_3, R.id.code_4};
    ArrayList<EditText> tv_digits;
    TextView titletext;
    RelativeLayout frameLayout;

    RewardsList rewardsList;
    public SharedPrefrencesManger sharedPrefrencesManger;

    Circle circle;
    //  private MasterInputResponse masterInputResponse;
    private vouchercodeOutput vouchercodeOutput;
    private vouchercodeInput vouchercodeInput;

    //for redeem voucher
    private RedeemVoucherOutput redeemVoucherOutput;
    private RedeemVoucherInput redeemVoucherInput;

    public static String voucherCodeSave = "";

    public static String ServiceType = "";
    public static String voucherPin = "";

    View ViewLine;
    private SupportMapFragment mapFragment;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rewards_details);
        Bundle bundle = getIntent().getExtras();

        mStoreLocationsList = new ArrayList<StoreLocation>();
        if (bundle != null) {
            if (bundle.getParcelableArrayList("storeLocation") != null) {
                mStoreLocationsList = bundle.getParcelableArrayList("storeLocation");
            }
            if (bundle.getParcelable(ConstantUtils.RewardsDetails) != null) {
                rewardsList = bundle.getParcelable(ConstantUtils.RewardsDetails);
            }
        }
        initUI();


        setData();
        tv_digits = new ArrayList<EditText>();
        for (int i = 0; i < tvListIds.length; i++) {
            EditText et_temp = (EditText) findViewById(tvListIds[i]);
            et_temp.addTextChangedListener(new CustomTextWatcher(et_temp));
            tv_digits.add(et_temp);
        }

//        View bottomSheet = findViewById(R.id.RL_Voucher_Added_bottomheet);
//        behavior = BottomSheetBehavior.from(bottomSheet);
//        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
//            @Override
//            public void onStateChanged(@NonNull View bottomSheet, int newState) {
//                switch (newState) {
//                    case BottomSheetBehavior.STATE_DRAGGING:
//                        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                        break;
//                    case BottomSheetBehavior.STATE_SETTLING:
//                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_SETTLING");
//                        break;
//                    case BottomSheetBehavior.STATE_EXPANDED:
//                        //   behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                        break;
//                    case BottomSheetBehavior.STATE_COLLAPSED:
//                        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                        break;
//                    case BottomSheetBehavior.STATE_HIDDEN:
//                        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                        break;
//                }
//            }
//
//            @Override
//            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
//                Log.i("BottomSheetCallback", "slideOffset: " + slideOffset);
//            }
//        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sharedPrefrencesManger.getNavigationIndex().length() > 0 || sharedPrefrencesManger.getNavigationBadgeIndex().length() > 0) {
            finish();
        }
    }

    @Override
    public void initUI() {
        super.initUI();

        titletext = (TextView) findViewById(R.id.titleTxt);
        titletext.setText(getResources().getString(R.string.details_spaced));
        DealName = (TextView) findViewById(R.id.shopname);
        shopImage = (ImageView) findViewById(R.id.shopImage);
        numberofdeals = (TextView) findViewById(R.id.numberofdeals);
        Discount = (TextView) findViewById(R.id.Discounttxt);
        Expire = (TextView) findViewById(R.id.Expiretxt);
        shopNameChange = (TextView) findViewById(R.id.changedeals);
        ChangeShop = (TextView) findViewById(R.id.change);
        distanceKm = (TextView) findViewById(R.id.distangeKm);
        shopImage = (ImageView) findViewById(R.id.shopImage);
        webviewDetails = (WebView) findViewById(R.id.webviewDetails);
        btn_redeem_now = (Button) findViewById(R.id.btn_redeem_now);
        RL_Voucher_view = (LinearLayout) findViewById(R.id.RL_Voucher_view);
        btn_use_voucher = (Button) findViewById(R.id.btn_use_voucher);
        mapframe = (RelativeLayout) findViewById(R.id.mapframe);
        nested_scroll = (ScrollView) findViewById(R.id.nested_scroll);
        voucherCodetext = (TextView) findViewById(R.id.voucherCodetext);
        frameLayout = (RelativeLayout) findViewById(R.id.mapframe);
        voucherCode_vc = (TextView) findViewById(R.id.voucherCode_vc);
        btn_set_Voucher = (Button) findViewById(R.id.btn_set_Voucher);
        ViewLine = (View) findViewById(R.id.ViewLine);
        ViewLine.setVisibility(View.GONE);
        btn_set_Voucher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewLine.setVisibility(View.VISIBLE);
                showLoadingFragment();
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_reward_redeem_voucher);

                ServiceType = ServiceUtils.RedeemVoucher;
                voucherPin = code1.getText().toString() + code2.getText().toString() + code3.getText().toString() + code4.getText().toString();
                onConsumeService();
//                replaceFragmnet(new VoucherSucessfullyRedeemedFragment(), R.id.frameLayout, true);
                // finish();
            }
        });
        if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().getInt("position") != -1) {
            count = getIntent().getExtras().getInt("position");
        }
        if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().getInt("total") != -1) {
            total = getIntent().getExtras().getInt("total");
        }
        if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().getString("icon_url") != null) {
            icon_url = getIntent().getExtras().getString("icon_url");
        }
        numberofdeals.setText(count + "/" + total);

        RL_Voucher_Added_bottomheet = (LinearLayout) findViewById(R.id.RL_Voucher_Added_bottomheet);
        code1 = (CustomEditText) findViewById(R.id.code_1);
        code2 = (CustomEditText) findViewById(R.id.code_2);
        code3 = (CustomEditText) findViewById(R.id.code_3);
        code4 = (CustomEditText) findViewById(R.id.code_4);
        code1.setFocusable(true);
        code4.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    //do here your stuff f

                    return true;
                }
                return false;
            }

        });
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        if (rewardsList != null) {
            if ((mStoreLocationsList != null && mStoreLocationsList.size() > 0) && (rewardsList.getCategoryId() != null && rewardsList.getCategoryId().length() > 0 && !rewardsList.getCategoryId().equalsIgnoreCase("Online & Digital"))) {
                // Obtain the SupportMapFragment and get notified when the map is ready to be used.
                mapFragment = (SupportMapFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.map);
            } else {
                frameLayout.setVisibility(View.GONE);
//                btn_use_voucher.setVisibility(View.GONE);
            }
        } else {
            frameLayout.setVisibility(View.GONE);
//            btn_use_voucher.setVisibility(View.GONE);
        }


        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
        googleMapsHelper = new GoogleMapsHelper(this);
        btn_redeem_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLoadingFragment();
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_reward_redeem_now_button_clicked);
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_reward_redeem_now_button_clicked + "_" + rewardsList.getCategoryId());

                MainAvailableRewardsFragment.shouldRefresh = true;
                UsedRewardsFragment.shouldRefresh = true;
                ServiceType = ServiceUtils.VoucherCode;
                onConsumeService();
            }
        });

        btn_use_voucher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendScroll();
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_reward_use_voucher_button_clicked);
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_reward_use_voucher_button_clicked + "_" + rewardsList.getCategoryId());
                // ServiceType = ServiceUtils.RedeemVoucher;
                Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.buttom_up);

                RL_Voucher_view.setVisibility(View.GONE);
                // RL_Voucher_view.startAnimation(animation);
                btn_redeem_now.setVisibility(View.GONE);

                RL_Voucher_Added_bottomheet.setVisibility(View.VISIBLE);
                RL_Voucher_Added_bottomheet.startAnimation(animation);
            }
        });

//        setData();
//..............header Buttons............
        RelativeLayout backLayout = (RelativeLayout) findViewById(R.id.backLayout);
        RelativeLayout helpLayout = (RelativeLayout) findViewById(R.id.helpLayout);

//
        helpLayout.setVisibility(View.INVISIBLE);
        helpLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(RewardDetailsActivity.this, HelpActivity.class);
                startActivity(intent);
            }
        });

        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard();
                onBackPressed();
            }
        });
        //..............header Buttons............
    }


    private void showLoadingFragment() {
        Bundle bundle = new Bundle();
//        bundle.putString("fromRedeem", "fromRedeem");
        LoadingFragmnet loadingFragmnet = new LoadingFragmnet();
        loadingFragmnet.setArguments(bundle);
        addFragmnet(loadingFragmnet, R.id.frameLayout, true);
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();

        if (ServiceType.equals(ServiceUtils.RedeemVoucher)) {

            //   progressBar.setVisibility(View.VISIBLE);
            redeemVoucherInput = new RedeemVoucherInput();
            redeemVoucherInput.setAppVersion(appVersion);
            redeemVoucherInput.setToken(token);
            redeemVoucherInput.setOsVersion(osVersion);
            redeemVoucherInput.setChannel(channel);
            redeemVoucherInput.setDeviceId(deviceId);
            redeemVoucherInput.setMsisdn(sharedPrefrencesManger.getMobileNo());
            redeemVoucherInput.setOfferId(rewardsList.getOfferID());
            redeemVoucherInput.setVoucherCode(voucherCodeSave);
            redeemVoucherInput.setVoucherPin(voucherPin);
            redeemVoucherInput.setAuthToken(authToken);
            new RedeemVoucherService(this, redeemVoucherInput);


        } else {
            //   progressBar.setVisibility(View.VISIBLE);
            vouchercodeInput = new vouchercodeInput();
            vouchercodeInput.setAppVersion(appVersion);
            vouchercodeInput.setToken(token);
            vouchercodeInput.setOsVersion(osVersion);
            vouchercodeInput.setChannel(channel);
            vouchercodeInput.setDeviceId(deviceId);
            vouchercodeInput.setMsisdn(sharedPrefrencesManger.getMobileNo());
            vouchercodeInput.setOfferId(rewardsList.getOfferID());
            vouchercodeInput.setAuthToken(authToken);
            new VoucherCodeService(this, vouchercodeInput);


        }

    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            onBackPressed();
            if (responseModel != null && responseModel.getResultObj() != null) {

                // progressBar.setVisibility(View.GONE);
                if (responseModel.getServiceType().equals(ServiceUtils.VoucherCode)) {
                    vouchercodeOutput = (vouchercodeOutput) responseModel.getResultObj();
                    if (vouchercodeOutput != null) {
                        if (vouchercodeOutput.getErrorCode() != null) {
                            if (vouchercodeOutput.getErrorMsgEn() != null) {
                                if (currentLanguage.equals("en")) {
                                    showErrorFragment(vouchercodeOutput.getErrorMsgEn());
                                } else {
                                    showErrorFragment(vouchercodeOutput.getErrorMsgAr());
                                }
                            } else {
                                showErrorFragment(getString(R.string.error_));
                            }

                        } else {
                            if (vouchercodeOutput.getVoucherCode() != null) {
                                ViewLine.setVisibility(View.VISIBLE);
                                if (mMap != null)
                                    mMap.getUiSettings().setScrollGesturesEnabled(false);
                                sendScroll();

                                Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.buttom_up);
                                RL_Voucher_view.setVisibility(View.VISIBLE);
                                RL_Voucher_view.startAnimation(animation);
                                btn_redeem_now.setVisibility(View.GONE);
                                voucherCode_vc.setText(voucherCodeSave);
                                voucherCodetext.setText(voucherCodeSave);
                                voucherCodetext.setText(vouchercodeOutput.getVoucherCode().toString());
                                voucherCode_vc.setText(vouchercodeOutput.getVoucherCode().toString());
                                voucherCodeSave = vouchercodeOutput.getVoucherCode();
                                if (rewardsList != null) {
                                    if ((mStoreLocationsList == null || mStoreLocationsList.size() == 0) || (rewardsList.getCategoryId() == null && rewardsList.getCategoryId().length() == 0 || rewardsList.getCategoryId().equalsIgnoreCase("Online & Digital"))) {
                                        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
                                        btn_use_voucher.setVisibility(View.GONE);
                                    }
                                } else {
                                    btn_use_voucher.setVisibility(View.GONE);
                                }

                            } else {
                                if (vouchercodeOutput.getErrorMsgEn() != null) {
                                    if (currentLanguage.equals("en")) {
                                        showErrorFragment(vouchercodeOutput.getErrorMsgEn());
                                    } else {
                                        showErrorFragment(vouchercodeOutput.getErrorMsgAr());
                                    }
                                } else {
                                    showErrorFragment(getString(R.string.error_));
                                }
                            }

                        }

                    }
                } else if (responseModel.getServiceType().equals(ServiceUtils.RedeemVoucher)) {
                    redeemVoucherOutput = (RedeemVoucherOutput) responseModel.getResultObj();
                    if (redeemVoucherOutput != null) {
                        if (redeemVoucherOutput.getErrorCode() != null) {
                            CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_reward_voucher_redeem_failure + "_" + rewardsList.getCategoryId());


                            if (redeemVoucherOutput.getErrorMsgEn() != null) {
                                if (currentLanguage.equals("en")) {
                                    showErrorFragment(redeemVoucherOutput.getErrorMsgEn());
                                } else {
                                    showErrorFragment(redeemVoucherOutput.getErrorMsgAr());
                                }
                            } else {
                                showErrorFragment(getString(R.string.generice_error));
                            }
                        } else {
                            if (redeemVoucherOutput.getRedeemed() != null && redeemVoucherOutput.getRedeemed()) {

                                CommonMethods.logContentPurchaseEvent(rewardsList.getOfferID(), rewardsList.getAverageSaving());
                                VoucherSucessfullyRedeemedFragment voucherSucessfullyRedeemedFragment = new VoucherSucessfullyRedeemedFragment();
                                Bundle bundle = new Bundle();
                                bundle.putString("VOUCHERCODE", voucherCodeSave);
                                //  this.onBackPressed();
                                voucherSucessfullyRedeemedFragment.setArguments(bundle);
                                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_reward_voucher_redeem_success);
                                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_reward_voucher_redeem_success + "_" + rewardsList.getCategoryId());

                                addFragmnet(voucherSucessfullyRedeemedFragment, R.id.frameLayout, true);

                                // replaceFragmnet(new VoucherSucessfullyRedeemedFragment(), R.id.frameLayout, true);
                            } else {
                                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_reward_voucher_redeem_failure);
                                if (redeemVoucherOutput.getErrorMsgEn() != null) {
                                    if (currentLanguage.equals("en")) {
                                        showErrorFragment(redeemVoucherOutput.getErrorMsgEn());
                                    } else {
                                        showErrorFragment(redeemVoucherOutput.getErrorMsgAr());
                                    }
                                } else {
                                    showErrorFragment(getString(R.string.error_));
                                }
                            }

                        }
                    }

                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void showErrorFragment(String error) {

        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, error);
        errorFragment.setArguments(bundle);
        replaceFragmnet(errorFragment, R.id.frameLayout, true);

    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        try {
            // progressBar.setVisibility(View.GONE);
            onBackPressed();
//        showErrorFragment();
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

    }


    private void sendScroll() {
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        nested_scroll.fullScroll(View.FOCUS_DOWN);
                    }
                });
            }
        }).start();
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onInfoWindowClick(Marker marker) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (mStoreLocationsList != null && mStoreLocationsList.size() > 0 && mStoreLocationsList.get(0) != null && mStoreLocationsList.get(0).getGeoLocation() != null) {

            for (int i = 0; i < mStoreLocationsList.size(); i++) {

                LatLng mLatLng = new LatLng(mStoreLocationsList.get(i).getGeoLocation().getLatitude(), mStoreLocationsList.get(i).getGeoLocation().getLongitude());
                marker = new MarkerOptions().position(mLatLng);

                // this.mMap = googleMap;
                mMap = googleMap;
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//                circle = mMap.addCircle(new CircleOptions()
//                        .center(mLatLng)
//                        .radius(10000)
//                        .strokeColor(Color.RED)
//                        .fillColor(Color.TRANSPARENT));


//            mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
//                @Override
//                public void onMapClick(LatLng latlng) {
//                    if (marker != null) {
//                        mMap.clear();
//                        //   mMap.addMarker(marker.position(latlng).title(""));\
//                        mMap.addMarker(marker.position(latlng).title(""));
//                        Location location = new Location("");
//                        location.setLatitude(latlng.latitude);
//                        location.setLongitude(latlng.longitude);
//                        float[] distance = new float[2];
//                        Location.distanceBetween(marker.getPosition().latitude, marker.getPosition().longitude,
//                                circle.getCenter().latitude, circle.getCenter().longitude, distance);
//                        if (distance[0] > circle.getRadius()) {
//                            Toast.makeText(getBaseContext(), "Outside", Toast.LENGTH_LONG).show();
//                            // mMap.clear();
//                        } else {
//                            Toast.makeText(getBaseContext(), "Inside", Toast.LENGTH_LONG).show();
//
//                        }
////                    LocationModel locationModel = new LocationModel();
////                    locationModel.setLocation(location);
////                    locationModel.setActivity(RewardDetailsActivity.this);
////                    new LocationServiceHelper(locationModel, RewardDetailsActivity.this);
//                    }
//
//                }
//            });


                Marker temp = mMap.addMarker(new MarkerOptions()
                        .title(mStoreLocationsList.get(i).getAddressEn())
                        .snippet(mStoreLocationsList.get(i).getNameEn())
                        .position(mLatLng));
                PicassoMarker markerTarget = new PicassoMarker(temp);
                Picasso.with(this)
                        .load(icon_url)
                        .resize(CommonMethods.dip2px(this, 20), CommonMethods.dip2px(this, 30))
                        .into(markerTarget);
            }
            LatLng mLatLng = new LatLng(mStoreLocationsList.get(0).getGeoLocation().getLatitude(), mStoreLocationsList.get(0).getGeoLocation().getLongitude());

            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mLatLng, 9));

        } else {
//            mMap = googleMap;
//            LatLng sydney = new LatLng(24.326190, 54.753607);
//            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 13));
//
//            mMap.addMarker(new MarkerOptions()
//                    .title("Dubai")
//                    .snippet("Pizza hut")
//                    .position(sydney));
        }


        // checkPermission();
        //   isGPSEnabled();
        Log.d("--***** MAP  ", "::Map ready");


        //  mMap.setMyLocationEnabled(true);


//        GoogleMapsHelper openGoogleMaps = new GoogleMapsHelper(this);
//        openGoogleMaps.onGoogleMpasOpen(/*marker.getPosition().latitude*/22.2222,23.323233/* marker.getPosition().longitude*/, "Test");

    }

    @Override
    public void onAgreeListener(String alertType) {

    }

    @Override
    public void onCancelListener(String alertType) {

    }


//    public LatLngBounds toBounds(LatLng center, double radius) {
//        LatLng southwest = SphericalUtil.computeOffset(center, radius * Math.sqrt(2.0), 225);
//        LatLng northeast = SphericalUtil.computeOffset(center, radius * Math.sqrt(2.0), 45);
//        return new LatLngBounds(southwest, northeast);
//    }

    private void checkPermission() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                this.requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            }
        }
    }

    private boolean canAccessLocation() {
        if (!hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
            return false;
        } else {
            return true;
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == checkSelfPermission(perm));
    }

    private void isGPSEnabled() {
        LocationManager locationManager = (LocationManager)
                getSystemService(Activity.LOCATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (canAccessLocation()) {// gps is enable
//                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && canAccessLocation()) {// gps is enable
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {// gps is disable

                } else {
                    if (gpsTracker.canGetLocation()) {
                        mlocation = gpsTracker.getLocation();

                        //   onConsumeService();
                    }
//                    chec/kLocation();
                }
//                checkLocation();
            } else {
                requestPermissions(LOCATION_PERMS, 0);
            }

        } else {
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) { // gps is disable
//                checkForGps();
            } else { // gps is enable
//                checkLocation();
            }

        }

    }

    private void checkLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, true);
        locationManager.requestLocationUpdates(provider, 10000, 0, this);
        mMap.setMyLocationEnabled(true);

        mlocation = locationManager.getLastKnownLocation(provider);
        if (mlocation == null) {
            mlocation = mMap.getMyLocation();
        }
        if (mlocation != null) {
            onLocationChanged(mlocation);
        }
    }

    private void setData() {

        disableNextBtn();

        sharedPrefrencesManger = new SharedPrefrencesManger(this);
//        rewardsList = new RewardsList(Parcel.obtain());

        try {
            if (rewardsList != null) {

                if (rewardsList.getNameEn() != null) {

                    if (sharedPrefrencesManger.getLanguage().equals("en"))
                        DealName.setText(rewardsList.getNameEn().toString());
                    else
                        DealName.setText(rewardsList.getNameAr().toString());


                } else {
                    DealName.setText("-");
                }

                if (rewardsList.getAverageSaving() != null) {

                    if (sharedPrefrencesManger.getLanguage().equals("en"))
                        Discount.setText(getString(R.string.aed) + " " + rewardsList.getAverageSaving().toString() + " " + getResources().getString(R.string.Reward_discount));
                    else
                        Discount.setText(getString(R.string.Reward_discount) + " " + rewardsList.getAverageSaving().toString() + " " + getResources().getString(R.string.aed));

                } else {
                    Discount.setText("-");
                }

//                if (rewardsList.getExpiryDate() != null) {
//
//                    if (sharedPrefrencesManger.getLanguage().equals("en"))
//                        Expire.setText(getResources().getString(R.string.expire) + " " + CommonMethods.parseDate(rewardsList.getExpiryDate()));
//                    else
//                        Expire.setText(getResources().getString(R.string.expire) + CommonMethods.parseDate(rewardsList.getExpiryDate()));
//                } else {
//                    Expire.setText("-");
//                }

                if (currentLanguage != null) {
                    if (currentLanguage.equals("en")) {
//                        Expire.setText(getResources().getString(R.string.expires) + " " + MainAvailableRewardsFragment.expiryDateEn);
                    } else {
//                        Expire.setText(getResources().getString(R.string.expires) + " " + MainAvailableRewardsFragment.expiryDateAr);

                    }
                } else {
                    if (sharedPrefrencesManger.getLanguage().equals("en")) {
//                        Expire.setText(getResources().getString(R.string.expires) + " " + MainAvailableRewardsFragment.expiryDateEn);
                    } else {
//                        Expire.setText(getResources().getString(R.string.expires) + " " + MainAvailableRewardsFragment.expiryDateAr);

                    }
                }
                if (rewardsList.getOfferDescEn() != null) {

                    if (sharedPrefrencesManger.getLanguage().equals("en")) {

                        WebSettings settings = webviewDetails.getSettings();

                        String htmlText = "<html><body style=color:#576977;\"text-align:justify\"> %s </body></Html>";
                        Spannable sp = new SpannableString(rewardsList.getOfferDescEn());
                        LinkifyCompat.addLinks(sp, Linkify.WEB_URLS);
                        webviewDetails.loadData(String.format(htmlText, Html.toHtml(sp)), "text/html", "utf-8");
                        webviewDetails.getSettings().setJavaScriptEnabled(true);
                        webviewDetails.getSettings().setSaveFormData(true);
                    } else {
                        String htmlText = "<html><body style=color:#576977;\"text-align:justify\"> %s </body></Html>";
                        Spannable sp = new SpannableString(rewardsList.getOfferDescAr());
                        LinkifyCompat.addLinks(sp, Linkify.WEB_URLS);
                        webviewDetails.loadData(String.format(htmlText, Html.toHtml(sp)), "text/html", "utf-8");
                        webviewDetails.getSettings().setJavaScriptEnabled(true);
                        webviewDetails.getSettings().setSaveFormData(true);
                    }
                } else {
                    Expire.setText("-");
                }


                if (rewardsList.getNameEn() != null) {
                    shopNameChange.setText(rewardsList.getNameEn().toString());
                } else {
                    shopNameChange.setText(rewardsList.getNameAr().toString());
                }


                if (rewardsList.getImageUrl() != null)
                    Picasso.with(getApplicationContext()).load(rewardsList.getImageUrl()).into(shopImage);
                else
                    shopImage.setImageResource(-1);


                if (rewardsList.getStoreLocations() != null && rewardsList.getStoreLocations().size() > 0) {


                    String lat = mStoreLocationsList.get(0).getGeoLocation().getLatitude().toString();
                    String lng = mStoreLocationsList.get(0).getGeoLocation().getLongitude().toString();


                } else {
                    Expire.setText("-");
                }

            } else {

                Toast.makeText(this, "No Data Found", Toast.LENGTH_LONG).show();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }


//        String htmlText = "<html><body style=\"text-align:justify\"> %s </body></Html>";
//        String myData = "<p><strong><font size='2' >Lorem Ipsum.</strong>, is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</font>";
//        webviewDetails.loadData(String.format(htmlText, myData), "text/html", "utf-8");
//        webviewDetails.getSettings().setJavaScriptEnabled(true);
//        webviewDetails.getSettings().setSaveFormData(true);
        //   webviewDetails.getSettings().setBuiltInZoomControls(true);
    }

    public class CustomTextWatcher implements TextWatcher {
        private EditText view;

        private CustomTextWatcher(EditText view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

            String text = editable.toString();
            if (getCode().length() == 4)
                enableNextBtn();
            else
                disableNextBtn();
            if (text.length() == 1) {
                view.clearFocus();
                int currentIndex = tv_digits.indexOf(view);
                if (currentIndex == 3) {

                    enableNextBtn();

                    hideKeyBoard();
                    view.clearFocus();
                    view.setCursorVisible(false);
                    code1.setCursorVisible(false);
                    code2.setCursorVisible(false);
                    code3.setCursorVisible(false);
                    code4.setCursorVisible(false);
                    return;

                    // tv_digits.get(3).setFocusable(true);
                } else {
                    for (int i = currentIndex + 1; i < tv_digits.size(); i++) {
                        tv_digits.get(i).requestFocus();

                        return;

                    }
                }

            }

        }
    }


    private void enableNextBtn() {
        btn_set_Voucher.setAlpha(1f);
        btn_set_Voucher.setClickable(true);
    }

    private void disableNextBtn() {
        btn_set_Voucher.setAlpha(.5f);
        btn_set_Voucher.setClickable(false);
    }

    private String getCode() {
        String codeEntered = "";
        codeEntered += code1.getText().toString();
        codeEntered += code2.getText().toString();
        codeEntered += code3.getText().toString();
        codeEntered += code4.getText().toString();
        return codeEntered;
    }

}
