package com.indy.views.activites;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;

import com.indy.R;
import com.indy.controls.AdjustEvents;
import com.indy.models.finalizeregisteration.AdditionalInfo;
import com.indy.models.rechargeCreditCard.CreditCardPaymentRequestInput;
import com.indy.models.rechargeCreditCard.RechargeCreditCardRequestOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.services.CreditCardRechargeRequestService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.buynewline.PaymentFailsFragment;
import com.indy.views.fragments.store.recharge.CongratulationsFragmentRecharge;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;

/**
 * Created by emad on 8/16/16.
 */
public class  CreditCardPaymentActivity extends MasterActivity {

    private WebView paymentWebView;
    //    private ProgressBar progressID;
    private StringBuilder stringBuilder = new StringBuilder();
    private String params = null;
    private String sParams = null;
    private CreditCardPaymentRequestInput creditCardPaymentRequestInput;
    private RechargeCreditCardRequestOutput finalizeRegisterationRequestOutput;
    Button backImg;
    double amount = 0;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
//        logEvent(AdjustEvents.CREDIT_CARD_PAYMENT_INITIALIZED, new Bundle());
        if (getIntent().getExtras() != null && getIntent().getExtras().getString("amount") != null) {
            try {
                amount = Double.parseDouble(getIntent().getExtras().getString("amount"));
            }catch(Exception ex){
                finish();
            }
        }
        initUI();
    }

    @Override
    public void initUI() {
        super.initUI();
        paymentWebView = (WebView) findViewById(R.id.paymentWebView);
//        progressID = (ProgressBar) findViewById(R.id.progressID);
//        progressID.setVisibility(View.VISIBLE);
        TextView headerText = (TextView) findViewById(R.id.titleID);
        headerText.setText(getString(R.string.payment_spaced));
        replaceFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);

        backImg = (Button) findViewById(R.id.backImg);
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        WebSettings settings = paymentWebView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setAllowContentAccess(true);
        settings.setLoadWithOverviewMode(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        paymentWebView.addJavascriptInterface(new LoadListener(), "HTMLOUT");
//        settings.setAllowFileAccess(true);
//        settings.setAllowUniversalAccessFromFileURLs(true);
//        settings.setNeedInitialFocus(true);

        if (Build.VERSION.SDK_INT >= 21) {
            settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }

        /*
        dialoge is appear in case errors this is css dialoge
        setWebChromeClient is used for enable css dialoge.
         */
        paymentWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                //Required functionality here
                return super.onJsAlert(view, url, message, result);
            }
        });

        onConsumeService();
//        loadURL("","");
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
//        progressID.setVisibility(View.GONE);
        try {
            onBackPressed();
            if(responseModel!=null && responseModel.getResultObj()!=null)
            finalizeRegisterationRequestOutput = (RechargeCreditCardRequestOutput) responseModel.getResultObj();
            if (finalizeRegisterationRequestOutput != null) {
                if (finalizeRegisterationRequestOutput.getErrorCode() != null)
                    onFinalizeRegisterationError();
                else{
                    CommonMethods.logPurchaseEvent(amount);
                    onFinalizeRegisterationSucess();
                }
            }
        }catch (Exception ex){

        }
    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        onBackPressed();
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    private void onFinalizeRegisterationSucess() {
        if (finalizeRegisterationRequestOutput != null) {
            if (finalizeRegisterationRequestOutput.getTransactionId() != null && !finalizeRegisterationRequestOutput.getTransactionId().isEmpty()) {
                sharedPrefrencesManger.setTransactionID(finalizeRegisterationRequestOutput.getTransactionId());
            }
            if (finalizeRegisterationRequestOutput.getAdditionalInfo().size() > 0) {
                for (AdditionalInfo additionalInfo : finalizeRegisterationRequestOutput.getAdditionalInfo()) {
                    stringBuilder.append(additionalInfo.getName());
                    stringBuilder.append("=");
                    stringBuilder.append(additionalInfo.getValue());
                    stringBuilder.append("&");
                }
                params = stringBuilder.toString();
                if (params.toString().length() > 0)
                    sParams = params.substring(0, params.length() - 1);
                loadURL(finalizeRegisterationRequestOutput.getPaymentGateWayUrl(), sParams);

            }
        }
    }

    private void showErrorFragment(String error) {
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, error);
        bundle.putBoolean("finish",true);
        ErrorFragment errorFragment = new ErrorFragment();
        errorFragment.setArguments(bundle);

        replaceFragmnet(errorFragment, R.id.frameLayout, true);
    }

    private void onFinalizeRegisterationError() {
        if (finalizeRegisterationRequestOutput != null && finalizeRegisterationRequestOutput.getErrorMsgEn() != null) {
            if (currentLanguage.equals("en")) {
                showErrorFragment(finalizeRegisterationRequestOutput.getErrorMsgEn());
            } else {
                showErrorFragment(finalizeRegisterationRequestOutput.getErrorMsgAr());
            }
        } else {
            showErrorFragment(getString(R.string.error_));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(finalizeRegisterationRequestOutput!=null && finalizeRegisterationRequestOutput.getErrorMsgEn()!=null){
            finish();
        }else if(finalizeRegisterationRequestOutput!=null && finalizeRegisterationRequestOutput.getTransactionId()==null){
            finish();
        }

    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        //784-1992-6040730-1
//        progressID.setVisibility(View.VISIBLE);
        creditCardPaymentRequestInput = new CreditCardPaymentRequestInput();
        creditCardPaymentRequestInput.setLang(currentLanguage);
        creditCardPaymentRequestInput.setAppVersion(appVersion);
        creditCardPaymentRequestInput.setOsVersion(osVersion);
        creditCardPaymentRequestInput.setToken(sharedPrefrencesManger.getToken());
        creditCardPaymentRequestInput.setChannel(channel);
        creditCardPaymentRequestInput.setDeviceId(deviceId);
        creditCardPaymentRequestInput.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        creditCardPaymentRequestInput.setMsisdn(sharedPrefrencesManger.getMobileNo());
        creditCardPaymentRequestInput.setAmount(amount + "");


        creditCardPaymentRequestInput.setOrderType(ConstantUtils.RechargeOrderType);
        creditCardPaymentRequestInput.setAuthToken(authToken);
        new CreditCardRechargeRequestService(this, creditCardPaymentRequestInput);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        try {
//        progressID.setVisibility(View.GONE);
            onBackPressed();
        }catch (Exception ex){
            if(ex!=null){
                ex.printStackTrace();
            }
        }
    }

    @JavascriptInterface
    private void loadURL(String paymnetURL, String postData) {
//        progressID.setVisibility(View.GONE);

//        paymnetURL = "https://www.google.com/";

        paymentWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView wView, String url) {
                Log.v("url", url + wView.getUrl()
                        + " ");
//                replaceFragmnet(new PaymentSucessfulllyFragment(), R.id.frameLayout, true);
                return false;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                view.loadUrl("javascript:window.HTMLOUT.processHTML('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
            }

        });
        try {
            byte[] bytes = postData.toString().getBytes("UTF-8");
            paymentWebView.postUrl(paymnetURL, bytes);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onPaymentFailure() {
        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_recharge_credit_card_payment_failure);
        logEvent(AdjustEvents.TRANSACTION_FAILED,new Bundle());
        replaceFragmnet(new PaymentFailsFragment(), R.id.frameLayout, true);
    }

    @Override
    public void onBackPressed() {

        if (getSupportFragmentManager().getFragments() != null && getSupportFragmentManager().getFragments().size() > 0
                && getSupportFragmentManager().getFragments().get(0) != null) {
            if (getSupportFragmentManager().getFragments().get(0) instanceof CongratulationsFragmentRecharge) {
                finish();
                CommonMethods.createAppRatingDialog(this);
            }


            if (getSupportFragmentManager().getFragments().get(0) instanceof PaymentFailsFragment) {
                finish();

            }

        }
        super.onBackPressed();
    }

    class LoadListener {
        @JavascriptInterface
        public void processHTML(String html) {
            Log.e("result", html);
            if (html.toLowerCase().contains(ConstantUtils.success_string)) {
                CongratulationsFragmentRecharge congratulationsFragmentRecharge = new CongratulationsFragmentRecharge();
                Bundle bundle = new Bundle();
                bundle.putBoolean("isCreditCard", true);
                bundle.putString("rechargeAmount", amount + "");
                congratulationsFragmentRecharge.setArguments(bundle);
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_recharge_credit_card_payment_success);
                addFragmnet(congratulationsFragmentRecharge, R.id.frameLayout, true);

            } else if (html.toLowerCase().contains(ConstantUtils.failure_string)) {
                onPaymentFailure();
            } else {
                return;
            }
        }
    }
}

