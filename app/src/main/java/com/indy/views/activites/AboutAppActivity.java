package com.indy.views.activites;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.adapters.LanguageChangeSpinnerAdapter;
import com.indy.controls.ServiceUtils;
import com.indy.models.logout.LogoutOutput;
import com.indy.models.updateLanguage.UpdateLanguageInput;
import com.indy.models.updateLanguage.UpdateLanguageOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.models.utils.MasterInputResponse;
import com.indy.services.LogoutService;
import com.indy.services.UpdateLanguageService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.AboutUs.About_TermsAndConditions;
import com.indy.views.fragments.esim.ActivateESIMQRCodeFragment;
import com.indy.views.fragments.gamification.models.GetNotificationSettingsOutput;
import com.indy.views.fragments.gamification.models.UpdateUserNotificationSettingOutputResponse;
import com.indy.views.fragments.gamification.models.UpdateUserNotificationSettingsInput;
import com.indy.views.fragments.gamification.services.GetUserNotificationStatusService;
import com.indy.views.fragments.gamification.services.UpdateUserNotificationSettingsService;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;

import java.util.Locale;

import static com.indy.utils.ConstantUtils.updateLanguage;

;

/**
 * Created by emad on 9/27/16.
 */

public class AboutAppActivity extends ShareMasterActivity implements AdapterView.OnItemSelectedListener {
    private TextView appVersionTxt;
    CardView SpinnerLanguage_Card;
    CardView AccountDetails_Card;
    CardView AccountDetails_eSim;
    UpdateLanguageOutput updateLanguageOutput;
    UpdateUserNotificationSettingOutputResponse updateUserNotificationSettingOutputResponse;
    GetNotificationSettingsOutput getNotificationSettingsOutput;
    FrameLayout frameLayout;
    public static Context context;
    private Spinner selectlanguageSpinner;
    private String fragmentType;
    TextView user_name, title, tv_sign_out, tv_last_updated;
    public static TextView tv_fbUsername, tv_twitterUsername;
    private Button backImg, helpBtn;
    public static Button bt_linkFacebook, bt_linkTwitter;
    boolean isUserInteraction = false;
    LogoutOutput logoutOutput;
    ToggleButton tb_appNotifications;
    String serviceType;
    String updatedLanguage;

    // test commit
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_indy_activity);
        initUI();
        loadSpinnerItems();
        callGetNotificationSettingStatusService();
    }


    @Override
    public void initUI() {
        super.initUI();
        selectlanguageSpinner = (Spinner) findViewById(R.id.selectlanguageSpinner);
        SpinnerLanguage_Card = (CardView) findViewById(R.id.SpinnerLanguage_Card);
        AccountDetails_Card = (CardView) findViewById(R.id.AccountDetails_Card);
        AccountDetails_eSim = (CardView) findViewById(R.id.AccountDetails_eSim);
        title = (TextView) findViewById(R.id.titleTxt);
        context = this;
//        lastUpdatetxt = (TextView) findViewById(R.id.user_name);
//        lastUpdatetxt.setVisibility(View.GONE);
        title.setText(R.string.setting_title);
        backImg = (Button) findViewById(R.id.backImg);
        helpBtn = (Button) findViewById(R.id.helpBtn);
        //..............header Buttons............
        RelativeLayout backLayout = (RelativeLayout) findViewById(R.id.backLayout);
        RelativeLayout helpLayout = (RelativeLayout) findViewById(R.id.helpLayout);
        CommonMethods.setStatusBarTransparent(this);
        helpLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AboutAppActivity.this, HelpActivity.class);
                startActivity(intent);
            }
        });

        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard();
                onBackPressed();
            }
        });
        //..............header Buttons............
        helpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AboutAppActivity.this, HelpActivity.class);
                startActivity(intent);
            }
        });
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        helpLayout.setVisibility(View.INVISIBLE);
        AccountDetails_Card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                displayNextFragment();
            }
        });


        //appVersionTxt = (TextView) findViewById(R.id.appVersionTxt);
        // appVersionTxt.setText(sharedPrefrencesManger.getAppVersion());


        tv_sign_out = (TextView) findViewById(R.id.tv_sign_out);
        tv_sign_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                serviceType = ConstantUtils.logoutService;
                onConsumeService();
            }
        });
        updatedLanguage = sharedPrefrencesManger.getLanguage();
        frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
        frameLayout.setVisibility(View.GONE);


        if (!sharedPrefrencesManger.getESIMBitmapCachedFlag()) {
            AccountDetails_eSim.setVisibility(View.GONE);
        } else {
            AccountDetails_eSim.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    frameLayout.setVisibility(View.VISIBLE);
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("fromSettings", true);
                    ActivateESIMQRCodeFragment activateESIMQRCodeFragment = new ActivateESIMQRCodeFragment();
                    activateESIMQRCodeFragment.setArguments(bundle);
                    replaceFragmnet(activateESIMQRCodeFragment, R.id.frameLayout, true);
                }
            });
        }


        tv_fbUsername = (TextView) findViewById(R.id.tv_fb_name);
        tv_twitterUsername = (TextView) findViewById(R.id.tv_twitter_name);
        tb_appNotifications = (ToggleButton) findViewById(R.id.tb_app_notifications);
        bt_linkFacebook = (Button) findViewById(R.id.btn_link_facebook);
        bt_linkTwitter = (Button) findViewById(R.id.btn_link_twitter);
        String twitterAuthToken = sharedPrefrencesManger.getTwitterAuthToken();
        String twitterAuthSecret = sharedPrefrencesManger.getTwitterAuthSecret();
        if (!twitterAuthToken.isEmpty() && !twitterAuthSecret.isEmpty()) {
            tv_twitterUsername.setText(sharedPrefrencesManger.getTwitterUsername());
            bt_linkTwitter.setText(getString(R.string.unlink));

            bt_linkTwitter.setBackground(ActivityCompat.getDrawable(AboutAppActivity.this, R.drawable.rounded_layout_transparent_pink_border));
            bt_linkTwitter.setTextColor(ActivityCompat.getColor(AboutAppActivity.this, R.color.pink_maps));
        } else {
            bt_linkTwitter.setText(getString(R.string.link));
            bt_linkTwitter.setBackground(ActivityCompat.getDrawable(AboutAppActivity.this, (R.drawable.profile_save_btn)));
            bt_linkTwitter.setTextColor(ActivityCompat.getColor(AboutAppActivity.this, R.color.color_white));

        }
        if (AccessToken.getCurrentAccessToken() != null) {
            bt_linkFacebook.setBackground(ActivityCompat.getDrawable(AboutAppActivity.this, (R.drawable.rounded_layout_transparent_pink_border)));
            bt_linkFacebook.setText(getString(R.string.unlink));
            bt_linkFacebook.setTextColor(ActivityCompat.getColor(AboutAppActivity.this, R.color.pink_maps));
            tv_fbUsername.setText(getFacebookFullname());
        } else {

            bt_linkFacebook.setBackground(ActivityCompat.getDrawable(AboutAppActivity.this, R.drawable.profile_save_btn));
            bt_linkFacebook.setText(getString(R.string.link));
            bt_linkFacebook.setTextColor(ActivityCompat.getColor(AboutAppActivity.this, R.color.color_white));

        }
        bt_linkFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AccessToken.getCurrentAccessToken() != null) {
                    LoginManager.getInstance().logOut();
                    bt_linkFacebook.setBackground(ActivityCompat.getDrawable(AboutAppActivity.this, (R.drawable.profile_save_btn)));
                    bt_linkFacebook.setText(getString(R.string.link));
                    bt_linkFacebook.setTextColor(ActivityCompat.getColor(AboutAppActivity.this, R.color.color_white));
                    sharedPrefrencesManger.setFacebookUsername("");
                    tv_fbUsername.setText("");
                } else {
                    facebookLogin(false, "", "");
                }
            }
        });
        bt_linkTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String twitterAuthToken = sharedPrefrencesManger.getTwitterAuthToken();
                String twitterAuthSecret = sharedPrefrencesManger.getTwitterAuthSecret();
                if (!twitterAuthToken.isEmpty() && !twitterAuthSecret.isEmpty()) {
                    twitterLogout();
                    bt_linkTwitter.setBackground(ActivityCompat.getDrawable(AboutAppActivity.this, (R.drawable.profile_save_btn)));
                    bt_linkTwitter.setText(getString(R.string.link));
                    bt_linkTwitter.setTextColor(ActivityCompat.getColor(AboutAppActivity.this, R.color.color_white));
                    sharedPrefrencesManger.setTwitterUsername("");
                    tv_twitterUsername.setText("");

                } else {
                    twitterLogin(false, "", "");
                }
            }
        });

    }

    public void callGetNotificationSettingStatusService() {
        frameLayout.setVisibility(View.VISIBLE);
        replaceFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);

        MasterInputResponse masterInputResponse = new MasterInputResponse();
        masterInputResponse.setAppVersion(appVersion);
        masterInputResponse.setToken(token);
        masterInputResponse.setLang(currentLanguage);
        masterInputResponse.setOsVersion(osVersion);
        masterInputResponse.setChannel(channel);
        masterInputResponse.setDeviceId(deviceId);
        masterInputResponse.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        masterInputResponse.setMsisdn(sharedPrefrencesManger.getMobileNo());
        masterInputResponse.setImsi(sharedPrefrencesManger.getMobileNo());
        masterInputResponse.setAuthToken(authToken);
        new GetUserNotificationStatusService(this, masterInputResponse);
    }


    public void callUpdateNotificationSettingsService(boolean isChecked) {
        frameLayout.setVisibility(View.VISIBLE);
        replaceFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);

        UpdateUserNotificationSettingsInput updateUserNotificationSettingsInput = new UpdateUserNotificationSettingsInput();
        updateUserNotificationSettingsInput.setAppVersion(appVersion);

        updateUserNotificationSettingsInput.setToken(token);
        updateUserNotificationSettingsInput.setLang(currentLanguage);
        updateUserNotificationSettingsInput.setOsVersion(osVersion);
        updateUserNotificationSettingsInput.setChannel(channel);
        updateUserNotificationSettingsInput.setDeviceId(deviceId);
        updateUserNotificationSettingsInput.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        updateUserNotificationSettingsInput.setMsisdn(sharedPrefrencesManger.getMobileNo());
        updateUserNotificationSettingsInput.setImsi(sharedPrefrencesManger.getMobileNo());
        updateUserNotificationSettingsInput.setAuthToken(authToken);
        updateUserNotificationSettingsInput.setNotificationsFlag(isChecked);
        new UpdateUserNotificationSettingsService(this, updateUserNotificationSettingsInput);
    }


    @Override
    public void onConsumeService() {
        super.onConsumeService();
        frameLayout.setVisibility(View.VISIBLE);
        replaceFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);

        if (serviceType.equals(ConstantUtils.logoutService)) {
            MasterInputResponse inputModel = new MasterInputResponse();
            inputModel.setAppVersion(appVersion);
//        sharedPrefrencesManger.setToken("testingDummy");
//        token = sharedPrefrencesManger.getToken();
            inputModel.setToken(token);
            inputModel.setLang(currentLanguage);
            inputModel.setOsVersion(osVersion);
            inputModel.setChannel(channel);
            inputModel.setDeviceId(deviceId);
            inputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
            inputModel.setImsi(sharedPrefrencesManger.getMobileNo());
            inputModel.setAuthToken(authToken);
            new LogoutService(this, inputModel);
        } else {
            UpdateLanguageInput updateLanguageInput = new UpdateLanguageInput();
            updateLanguageInput.setDeviceId(deviceId);
            updateLanguageInput.setMsisdn(sharedPrefrencesManger.getMobileNo());
            updateLanguageInput.setOsVersion(osVersion);
            updateLanguageInput.setImsi(sharedPrefrencesManger.getMobileNo());
            updateLanguageInput.setLang(updatedLanguage);
            updateLanguageInput.setAppVersion(appVersion);
            updateLanguageInput.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
            updateLanguageInput.setAuthToken(authToken);
            updateLanguageInput.setToken(token);

            new UpdateLanguageService(this, updateLanguageInput);
        }
    }


    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            frameLayout.setVisibility(View.GONE);

            if (responseModel != null && responseModel.getResultObj() != null && responseModel.getServiceType().equals(ServiceUtils.logoutService)) {
                logoutOutput = (LogoutOutput) responseModel.getResultObj();
                if (logoutOutput != null && logoutOutput.isLoggedOut()) {
                    logoutSuccess();
                } else {
                    onBackPressed();// to remove loading fragment
                    logoutFailure();
                }
            } else if (responseModel != null && responseModel.getServiceType().equals(ServiceUtils.UPDATE_LANGUAGE)) {
                updateLanguageOutput = (UpdateLanguageOutput) responseModel.getResultObj();
                if (updateLanguageOutput != null && updateLanguageOutput.getErrorCode() == null && updateLanguageOutput.getResponseMsg() != null) {
                    onSuccessfulLanguageChange();
                } else {
                    onFailure();
                }

            } else if (responseModel != null && responseModel.getResultObj() != null && responseModel.getServiceType().equals(ServiceUtils.GET_USER_NOTIFICATION_SETTINGS)) {
                getNotificationSettingsOutput = (GetNotificationSettingsOutput) responseModel.getResultObj();
                if (getNotificationSettingsOutput != null && getNotificationSettingsOutput.getInAppNotifications()) {
                    tb_appNotifications.setChecked(true);
                } else {
                    tb_appNotifications.setChecked(false);
                }
                tb_appNotifications.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        callUpdateNotificationSettingsService(b);

                    }
                });
            } else if (responseModel != null && responseModel.getResultObj() != null && responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.UPDATE_USER_NOTIFICATION_SETTING)) {
                updateUserNotificationSettingOutputResponse = (UpdateUserNotificationSettingOutputResponse) responseModel.getResultObj();
                if (updateUserNotificationSettingOutputResponse != null && updateUserNotificationSettingOutputResponse.isFlagUpdated()) {

                } else {
                    tb_appNotifications.toggle();
                }

            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    public void onSuccessfulLanguageChange() {

        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_settings_change_language);

        frameLayout.setVisibility(View.GONE);
        sharedPrefrencesManger.setLanguage(updatedLanguage);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                Locale locale = new Locale(language);
//                Locale.setDefault(locale);
//                changeLocale(locale,AboutAppActivity.this);

//                LocalizationUtils.switchLocalizaion(sharedPrefrencesManger.getLanguage(),AboutAppActivity.this);
//                recreate();
                finish();

                startActivity(new Intent(AboutAppActivity.this, AboutAppActivity.class));
            }
        });
    }

    private void changeLocale(@NonNull final Locale locale, Context context) {
        final Resources res = context.getResources();
        final Configuration config = res.getConfiguration();
        config.setLocale(locale);
        final Context newContext = context.createConfigurationContext(config);
        // What should I do with the newContext???
//        recreate();
//        super.attachBaseContext(newContext);
    }

    public void onFailure() {
        frameLayout.setVisibility(View.VISIBLE);
        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_settings_change_language_failure);

        if (updateLanguageOutput != null && updateLanguageOutput.getErrorMsgEn() != null) {
            if (sharedPrefrencesManger.getLanguage().equals("en")) {
                showErrorFragment(updateLanguageOutput.getErrorMsgEn());
            } else {
                showErrorFragment(updateLanguageOutput.getErrorMsgAr());
            }
        } else {
            showErrorFragment(getString(R.string.generice_error));
        }
    }

    private void showErrorFragment(String error) {
        frameLayout.setVisibility(View.VISIBLE);
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, error);
        errorFragment.setArguments(bundle);
        replaceFragmnet(errorFragment, R.id.frameLayout, true);
    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse responseModel) {
        onBackPressed();// to remove loading fragment
        super.onUnAuthorizeToken(responseModel);
    }

    public void logoutSuccess() {
        //do logout here


        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_settings_sign_out);
        this.finish();
        sharedPrefrencesManger.clearSharedPref();
        SwpeMainActivity.swpeMainActivityInstance.finish();
        Intent intent = new Intent(this, RegisterationActivity.class);
        intent.putExtra(ConstantUtils.showBackBtn, true);
        intent.putExtra("fragIndex", "7");

        startActivity(intent);

    }

    public void logoutFailure() {
        frameLayout.setVisibility(View.VISIBLE);
        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_settings_sign_out_failure);

        Fragment errorFragmnet = new ErrorFragment();
        Bundle bundle = new Bundle();
        if (currentLanguage != null && logoutOutput != null && logoutOutput.getErrorMsgEn() != null) {
            if (currentLanguage.equalsIgnoreCase("en"))
                bundle.putString(ConstantUtils.errorString, logoutOutput.getErrorMsgEn());
            else
                bundle.putString(ConstantUtils.errorString, logoutOutput.getErrorMsgAr());
        } else {
            bundle.putString(ConstantUtils.errorString, getString(R.string.error_));
        }
        errorFragmnet.setArguments(bundle);
        replaceFragmnet(errorFragmnet, R.id.frameLayout, true);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        onBackPressed();
//        try {
//            logoutFailure();
//        }catch (Exception ex) {
//            if (ex != null) {
//                ex.printStackTrace();
//            }
//        }
    }


    private void displayNextFragment() {
//        replaceFragmnet(new About_TermsAndConditions(), R.id.frameLayout, true);
        Bundle bundle2 = new Bundle();

        bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_settings_about_swyp);
        bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_settings_about_swyp);
        mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_settings_about_swyp, bundle2);
        Intent intent = new Intent(AboutAppActivity.this, About_TermsAndConditions.class);
        startActivity(intent);
    }


    private void loadSpinnerItems() {
        selectlanguageSpinner = (Spinner) findViewById(R.id.selectlanguageSpinner);
        LanguageChangeSpinnerAdapter adapter = new LanguageChangeSpinnerAdapter(this, R.layout.item_gender);
        adapter.setDropDownViewResource(R.layout.item_gender);
        selectlanguageSpinner.setAdapter(adapter);

        if (sharedPrefrencesManger.getLanguage().equals(ConstantUtils.enLng)) {
            selectlanguageSpinner.setSelection(0);
        } else {
            selectlanguageSpinner.setSelection(1);
        }


        selectlanguageSpinner.setOnItemSelectedListener(this);
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (isUserInteraction) {
            if (i == 0) {
                String language = ConstantUtils.lang_english;

                if (!language.equals(sharedPrefrencesManger.getLanguage())) {
                    updatedLanguage = language;
                    serviceType = updateLanguage;

                    onConsumeService();
//                    onSuccessfulLanguageChange();
                }
            } else if (i == 1) {
                String language = ConstantUtils.lang_arabic;

                if (!language.equals(sharedPrefrencesManger.getLanguage())) {
                    updatedLanguage = language;
                    serviceType = updateLanguage;
                    onConsumeService();
//                    onSuccessfulLanguageChange();

                }
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onBackPressed() {
        if (frameLayout.getVisibility() == View.VISIBLE) {
            frameLayout.setVisibility(View.GONE);
        } else {
            super.onBackPressed();
            Intent intent = new Intent(this, SwpeMainActivity.class);
            intent.putExtra("isSliderOpen", "true");
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
            finish();
            this.overridePendingTransition(0, 0);
        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        isUserInteraction = true;
    }

    public static void faceBookLoginSuccess() {
        bt_linkFacebook.setBackground(ActivityCompat.getDrawable(context, R.drawable.rounded_layout_transparent_pink_border));
        bt_linkFacebook.setText(context.getString(R.string.unlink));
        bt_linkFacebook.setTextColor(ActivityCompat.getColor(context, R.color.pink_maps));
        tv_fbUsername.setText(getFacebookFullname());
    }

    public static void twitterLoginSuccess() {
        bt_linkTwitter.setBackground(ActivityCompat.getDrawable(context, R.drawable.rounded_layout_transparent_pink_border));
        bt_linkTwitter.setText(context.getString(R.string.unlink));
        bt_linkTwitter.setTextColor(ActivityCompat.getColor(context, R.color.pink_maps));

    }

}
