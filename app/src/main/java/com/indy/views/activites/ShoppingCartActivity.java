package com.indy.views.activites;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.indy.R;
import com.indy.adapters.ShoppingCartListAdapter;
import com.indy.controls.OnContactsListItemClickListener;
import com.indy.controls.OnPrefferredNumberFocusChangeListener;
import com.indy.controls.OnStoreDeleteItemClickListener;
import com.indy.controls.OnstoreClickInterface;
import com.indy.models.balance.BalanceInputModel;
import com.indy.models.balance.BalanceResponseModel;
import com.indy.models.bundles.BundleList;
import com.indy.models.purchaseStoreBundle.OrderAdditionalInfoList;
import com.indy.models.purchaseStoreBundle.OrderItems;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.services.GetUserBalanceService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.gamification.models.serviceInputOutput.CheckinRewardModel;
import com.indy.views.fragments.store.mainstore.MyStoreFragment;
import com.indy.views.fragments.usage.UsageFragment;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.recyclerview.animators.FadeInRightAnimator;

/**
 * Created by emad on 10/18/16.
 */
public class ShoppingCartActivity extends MasterActivity implements OnStoreDeleteItemClickListener, OnstoreClickInterface,
        OnContactsListItemClickListener, OnPrefferredNumberFocusChangeListener {

    private RelativeLayout backLayout, helpLayout;
    boolean isFromConfirmButton = false;
    private RecyclerView mRecyclerView;
    ShoppingCartListAdapter shoppingCartListAdapter;
    private FrameLayout frameLayout;
    private int totalCount = 0;
    private double totalAmount = 0;
    Button btn_shopMore, btn_confirmBtn;
    TextView tv_totalCost, tv_balance, tv_lastUpdated, tv_headerText, tv_vatText;
    int currentIndex = -1;
    private List<BundleList> mList;
    ScrollView sv_root;
    public static ShoppingCartActivity shoppingCartActivityInstance;
    private CheckinRewardModel checkinRewardModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_cart);
        mList = new ArrayList<BundleList>();
        shoppingCartActivityInstance = this;
        checkinRewardModel = (CheckinRewardModel) getIntent().getParcelableExtra(ConstantUtils.lottery_reward);
        if (checkinRewardModel != null) {
            BundleList bundleList = new BundleList();
            bundleList.setNameEn(checkinRewardModel.getPackageName());
            bundleList.setNameAr(checkinRewardModel.getPackageName());
            bundleList.setId(checkinRewardModel.getPackageCode());
            bundleList.setBundleName(checkinRewardModel.getPackageDescription());
            bundleList.setBundleNameAr(checkinRewardModel.getPackageDescription());
            bundleList.setCount(1);
            bundleList.setSelected(true);
            bundleList.setRatePlanType("NORMAL");
            bundleList.setValidtyInDays(checkinRewardModel.getTotalDescription());
            bundleList.setPrice(String.valueOf(checkinRewardModel.getCurrentPrice()));
            bundleList.setAddSubtractEnabled(false);
            bundleList.setRewardID(checkinRewardModel.getRewardId());
            MyStoreFragment.selectedItems = new ArrayList<BundleList>();
            MyStoreFragment.selectedItems.add(bundleList);
        }
        if (MyStoreFragment.selectedItems != null) {
            for (int i = 0; i < MyStoreFragment.selectedItems.size(); i++) {
                mList.add(MyStoreFragment.selectedItems.get(i));
            }
        }
        initUI();
        setListeners();

    }

    @Override
    public void initUI() {
        super.initUI();
        helpLayout = (RelativeLayout) findViewById(R.id.helpLayout);
        backLayout = (RelativeLayout) findViewById(R.id.backLayout);
        tv_totalCost = (TextView) findViewById(R.id.tv_total_amount);
        tv_balance = (TextView) findViewById(R.id.tv_titleTxt);
        tv_lastUpdated = (TextView) findViewById(R.id.tv_last_updated);
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        frameLayout = (FrameLayout) findViewById(R.id.frameLayout);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        btn_shopMore = (Button) findViewById(R.id.showMoreBtn);
        btn_shopMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_store_shop_more);

                finish();
            }
        });
        helpLayout.setVisibility(View.INVISIBLE);
        btn_confirmBtn = (Button) findViewById(R.id.confirmBtn);
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tv_headerText = (TextView) findViewById(R.id.titleTxt);
        tv_headerText.setText(getString(R.string.shopping_cart_activity_spaced));
        tv_vatText = (TextView) findViewById(R.id.tv_vat_text);
        shoppingCartListAdapter = new ShoppingCartListAdapter(mList, this, this, this, this, this);
        mRecyclerView.setAdapter(shoppingCartListAdapter);
//        sv_root = (ScrollView) findViewById(R.id.sv_root);
        mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                    View currentFocus = getCurrentFocus();
                    if (currentFocus != null) {
                        currentFocus.clearFocus();
                        hideKeyBoard();
                    }
                }
            }
        });
        mRecyclerView.setItemAnimator(new FadeInRightAnimator());
        if (checkinRewardModel != null) {
            tv_balance.setText(UsageFragment.balanceID.getText());

        } else {
            tv_balance.setText(getString(R.string.your_balance_is_aed) + " " + MyStoreFragment.userBalance+ " " + getString(R.string.aed_arabic_only));
        }
        tv_lastUpdated.setText(getString(R.string.last_updated) + " " + sharedPrefrencesManger.getLastUpdatedBalance() +
                " " + getString(R.string.min_ago));
        updateTotal();

        onConsumeService();
    }

    @Override
    public void onBackPressed() {
        if (frameLayout.getVisibility() == View.VISIBLE) {
            super.onBackPressed();
            frameLayout.setVisibility(View.GONE);
        } else {
            super.onBackPressed();
        }
        if (sharedPrefrencesManger.isVATEnabled()) {
            tv_vatText.setVisibility(View.VISIBLE);
        } else {
            tv_vatText.setVisibility(View.GONE);
        }

    }

    //    BalanceResponseModel balanceResponseModel;
    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        onBackPressed();

        if (responseModel != null && responseModel.getResultObj() != null) {
            balanceResponseModel = (BalanceResponseModel) responseModel.getResultObj();
            if (balanceResponseModel != null) {
                onGetBalance(responseModel);
            } else {
                onGetBalanceError();
            }
        }

        if (responseModel != null && responseModel.getResultObj() != null) {
            balanceResponseModel = (BalanceResponseModel) responseModel.getResultObj();
            if (balanceResponseModel != null) {
                onConfirmSelect();
            }
        }
    }


    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);

        onBackPressed();
        ((FrameLayout) findViewById(R.id.frameLayout)).setVisibility(View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (tv_lastUpdated != null)
            tv_lastUpdated.setText(getString(R.string.last_updated) + " " + sharedPrefrencesManger.getLastUpdatedBalance() +
                    " " + getString(R.string.min_ago));
        if (sharedPrefrencesManger.getNavigationIndex().length() > 0 || sharedPrefrencesManger.getNavigationBadgeIndex().length() > 0) {

            finish();
        }
    }

    @Override
    public void onDeleteStoreItem(BundleList bundleList, int position) {
        //reduce count
        //if count = 0 remove object
        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_store_item_minus);
        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_store_item_minus + "_" + bundleList.getBundleName());

        int counter = bundleList.getCount();
        if (counter > 0) {
            counter--;
            bundleList.setCount(counter);

            if (counter == 0) {
                bundleList.setSelected(false);
                for (int i = 0; i < MyStoreFragment.selectedItems.size(); i++) {
                    if (MyStoreFragment.selectedItems.get(i).getId().equals(bundleList.getId())) {
                        MyStoreFragment.selectedItems.set(i, bundleList);

                    }
                }
                mList.remove(bundleList);
                shoppingCartListAdapter.notifyItemRemoved(position);


            } else {
                mList.set(position, bundleList);
                for (int i = 0; i < MyStoreFragment.selectedItems.size(); i++) {
                    if (MyStoreFragment.selectedItems.get(i).getId().equals(bundleList.getId())) {
                        MyStoreFragment.selectedItems.set(i, bundleList);

                    }
                }
                shoppingCartListAdapter.notifyDataSetChanged();
            }


            updateTotal();
        }
    }

    @Override
    public void onItemClick(BundleList bundleList, int position) {
        //increase count of already
        // added item
        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_store_item_plus);
        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_store_item_plus + "_" + bundleList.getBundleName());
        if (bundleList.getCount() < bundleList.getMaxCount()) {
            int counter = bundleList.getCount();

            counter++;
            bundleList.setCount(counter);
            for (int i = 0; i < MyStoreFragment.selectedItems.size(); i++) {
                if (MyStoreFragment.selectedItems.get(i).getId().equals(bundleList.getId())) {
                    MyStoreFragment.selectedItems.set(i, bundleList);

                }
            }
            mList.set(position, bundleList);
            shoppingCartListAdapter.notifyDataSetChanged();
            updateTotal();
        } else {

        }
    }


    public void updateTotal() {
        totalCount = 0;
        totalAmount = 0;
        for (int i = 0; i < mList.size(); i++) {
            double temp = Double.parseDouble(mList.get(i).getPrice());
            temp = temp * mList.get(i).getCount();
            if (sharedPrefrencesManger.isVATEnabled()) {
                if (mList.get(i).isVatapplied()) {
                    temp += (temp * 0.05);
                }
            }
            totalAmount += temp;

            totalCount += mList.get(i).getCount();
        }

        tv_totalCost.setText(getString(R.string.aed) + " " + totalAmount);
        if (totalCount == 0) {
            btn_confirmBtn.setEnabled(false);
            btn_confirmBtn.setAlpha(0.5f);
            btn_confirmBtn.setOnClickListener(null);
            finish();
        } else {
            btn_confirmBtn.setEnabled(true);
            btn_confirmBtn.setAlpha(1.0f);
            btn_confirmBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isFromConfirmButton = true;
                    onConsumeService();
                }
            });
        }
    }

//    @Override
//    public void onConsumeService() {
//        super.onConsumeService();
//        ((FrameLayout) findViewById(R.id.frameLayout)).setVisibility(View.VISIBLE);
//        addFragmnet(new LoadingFragmnet(),R.id.frameLayout,true);
//        BalanceInputModel balanceInputModel = new BalanceInputModel();
//        balanceInputModel.setAppVersion(appVersion);
//        balanceInputModel.setToken(token);
//        balanceInputModel.setOsVersion(osVersion);
//        balanceInputModel.setChannel(channel);
//        balanceInputModel.setDeviceId(deviceId);
//        balanceInputModel.setLang(currentLanguage);
//        balanceInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
//        balanceInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
//        balanceInputModel.setAuthToken(authToken);
//        balanceInputModel.setBillingPeriod(CommonMethods.getBillingPeriod());
//
//        new GetUserBalanceService(this, balanceInputModel, this);
//    }
//


    public void setListeners() {
        btn_confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isFromConfirmButton = true;
                onConsumeService();
            }
        });
    }

    private void onConfirmSelect() {
        boolean retValue = true;

//        if (balanceResponseModel != null && balanceResponseModel.getAmount() >= totalAmount) {

        ArrayList<OrderItems> idsList = new ArrayList<OrderItems>();

        for (int i = 0; i < MyStoreFragment.selectedItems.size(); i++) {
            for (int j = 0; j < MyStoreFragment.selectedItems.get(i).getCount(); j++) {
                OrderItems orderItems = new OrderItems();
                OrderAdditionalInfoList[] additionalInfoListArray = new OrderAdditionalInfoList[1];
                OrderAdditionalInfoList additionalInfoList = new OrderAdditionalInfoList();
                if (currentLanguage.equals("en")) {
                    additionalInfoList.setName(MyStoreFragment.selectedItems.get(i).getNameEn());
                } else {
                    additionalInfoList.setName(MyStoreFragment.selectedItems.get(i).getNameAr());

                }
                additionalInfoList.setValue(MyStoreFragment.selectedItems.get(i).getPrice());
                orderItems.setOfferId(MyStoreFragment.selectedItems.get(i).getId());

                if (MyStoreFragment.selectedItems.get(i).getRatePlanType() != null && (MyStoreFragment.selectedItems.get(i).getRatePlanType().equals("NORMAL")
                        || MyStoreFragment.selectedItems.get(i).getRatePlanType().isEmpty())) {
                    orderItems.setPreferedMsisdn("");
                } else {
                    if (MyStoreFragment.selectedItems.get(i).getPreferredNumber() != null &&
                            MyStoreFragment.selectedItems.get(i).getPreferredNumber().length() == 10 &&
                            MyStoreFragment.selectedItems.get(i).getPreferredNumber().startsWith("05")) {
                        orderItems.setPreferedMsisdn(MyStoreFragment.selectedItems.get(i).getPreferredNumber());

                    } else {
                        //showErrorhere
                        retValue = false;
                    }
                }
                additionalInfoListArray[0] = additionalInfoList;
                orderItems.setOrderAdditionalInfoList(additionalInfoListArray);
                idsList.add(orderItems);
            }
        }
        if (isFromConfirmButton) {
            isFromConfirmButton = false;
            if (balanceResponseModel != null && balanceResponseModel.getAmount() >= totalAmount) {
                if (retValue) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(ConstantUtils.itemsIdList, idsList);
                    bundle.putString("total", totalAmount + "");
                    if (checkinRewardModel != null) {
                        bundle.putBoolean(ConstantUtils.FROM_LOTTERY_DEAL, true);
                        bundle.putString(ConstantUtils.REWARD_ID, checkinRewardModel.getRewardId());
                        bundle.putString(ConstantUtils.LUCKY_DEAL_ID, checkinRewardModel.getNotificationResponseId());
                    }
                    CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_store_confirm_purchase_button);
                    for (int i = 0; i < MyStoreFragment.selectedItems.size(); i++) {
                        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_store_confirm_purchase_button + "_" + MyStoreFragment.selectedItems.get(i).getBundleName());

                    }
                    bundle.putDouble(ConstantUtils.USER_BALANCE, balanceResponseModel.getAmount());
                    Intent intent = new Intent(ShoppingCartActivity.this, ConfirmStorePurchaseFragment.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
// else {
//                Bundle bundle = new Bundle();
//                bundle.putDouble(ConstantUtils.USER_BALANCE,balanceResponseModel.getAmount());
//                Intent intent = new Intent(ShoppingCartActivity.this, ConfirmStorePurchaseFragment.class);
//                intent.putExtras(bundle);
//
//                startActivity(intent);
//            } else {
////            Toast.makeText(ShoppingCartActivity.this, getString(R.string.invalid_number_request), Toast.LENGTH_SHORT).show();
//            }
            } else {
                ErrorFragment errorFragment = new ErrorFragment();
                Bundle bundle = new Bundle();
                bundle.putString(ConstantUtils.errorString, getString(R.string.your_request_cannot_be_processed_because_you_dont_have_enough_balance));
                bundle.putString(ConstantUtils.errorTitle, getString(R.string.error_));
                errorFragment.setArguments(bundle);
                frameLayout.setVisibility(View.VISIBLE);
                addFragmnet(errorFragment, R.id.frameLayout, true);
            }
        }
    }

    @Override
    public void onContactsClick(BundleList bundleList, int index) {
        currentIndex = index;
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
        startActivityForResult(intent, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (data != null && data.getData() != null) {//cancel action
                String phoneNo = null;
                Uri uri = data.getData();
                Cursor cursor = getContentResolver().query(uri, null, null, null, null);

                if (cursor.moveToFirst()) {
                    int phoneIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                    phoneNo = cursor.getString(phoneIndex);
                    phoneNo = phoneNo.replace("+", "");
                    phoneNo = phoneNo.replace("971", "0");
                    phoneNo = phoneNo.replace(" ", "");
                    mList.get(currentIndex).setPreferredNumber(phoneNo);
                    shoppingCartListAdapter.notifyDataSetChanged();
                }

                cursor.close();
//
            }
        }
    }

    @Override
    public void onFocusChanged(int index, String updatedNumber) {

        MyStoreFragment.selectedItems.get(index).setPreferredNumber(updatedNumber);

    }

    protected class MyScrollListener implements AbsListView.OnScrollListener {


        public void onScroll(AbsListView view, int firstVisibleItem,
                             int visibleItemCount, int totalItemCount) {
            // do nothing
        }

        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if (SCROLL_STATE_TOUCH_SCROLL == scrollState) {
                View currentFocus = getCurrentFocus();
                if (currentFocus != null) {
                    currentFocus.clearFocus();
                }
            }
        }

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    private void onGetBalance(BaseResponseModel responseModel) {
        balanceResponseModel = (BalanceResponseModel) responseModel.getResultObj();
        if (balanceResponseModel != null) {
            if (balanceResponseModel.getErrorCode() != null) {
                if (currentLanguage != null)
                    if (currentLanguage.equalsIgnoreCase("en"))
                        tv_balance.setText(getResources().getString(R.string.your_balance_is_only) + "0.0");
                    else
                        tv_balance.setText(getResources().getString(R.string.your_balance_is_only) + "0.0");

            } else
                onGetBalanceSucess();
//                onGetBalanceError();


        }
    }

    BalanceResponseModel balanceResponseModel;

    private void onGetBalanceSucess() {
        if (currentLanguage != null) {
            if (currentLanguage.equalsIgnoreCase("en")) {
                tv_balance.setText(getResources().getString(R.string.your_balance_is_only) + " " + getResources().getString(R.string.aed) + " " + balanceResponseModel.getAmount());
            } else {
                tv_balance.setText(getResources().getString(R.string.your_balance_is_only) + " " + balanceResponseModel.getAmount() + " " + getResources().getString(R.string.aed));
            }
        }
        tv_lastUpdated.setText(getString(R.string.last_updated) + " " + sharedPrefrencesManger.getLastUpdatedBalance() +
                " " + getString(R.string.min_ago));
        CommonMethods.updateBalanceAndLastUpdated(getString(R.string.your_balance_is_aed) + " " + balanceResponseModel.getAmount()+ " " + getString(R.string.aed_arabic_only)
                , getString(R.string.last_updated) + " " + sharedPrefrencesManger.getLastUpdatedBalance() +
                        " " + getString(R.string.min_ago));
        // expiryTxt.setText(balanceResponseModel.getExpiryTextEn() + " " + balanceResponseModel.getExpiryIntervalEn());
        if (isFromConfirmButton) {
            onConfirmSelect();
        }
    }

    private void onGetBalanceError() {

        Fragment errorFragmnet = new ErrorFragment();
        Bundle bundle = new Bundle();
        if (currentLanguage != null) {
            if (currentLanguage.equalsIgnoreCase("en"))
                bundle.putString(ConstantUtils.errorString, balanceResponseModel.getErrorMsgEn());
            else
                bundle.putString(ConstantUtils.errorString, balanceResponseModel.getErrorMsgAr());
        }
        errorFragmnet.setArguments(bundle);
        replaceFragmnet(errorFragmnet, R.id.frameLayout, true);

    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        frameLayout.setVisibility(View.VISIBLE);
        addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        BalanceInputModel balanceInputModel = new BalanceInputModel();
        balanceInputModel.setAppVersion(appVersion);
        balanceInputModel.setToken(token);
        balanceInputModel.setOsVersion(osVersion);
        balanceInputModel.setChannel(channel);
        balanceInputModel.setDeviceId(deviceId);
        balanceInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        balanceInputModel.setBillingPeriod(CommonMethods.getBillingPeriod());//cureent month
        balanceInputModel.setAuthToken(authToken);
        new GetUserBalanceService(this, balanceInputModel, this);
    }

}