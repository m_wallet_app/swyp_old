package com.indy.views.activites;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.findus.FindUsFragment;
import com.indy.views.fragments.login.StaticPackagesFragments;

/**
 * Created by emad on 9/18/16.
 */
public class HelpActivity extends MasterActivity {
    private TextView headerTxt;
    private Button backImg, helpBtn;
    private LinearLayout ll_findUs, ll_faq, ll_liveChat, ll_whatsSwyp;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.buttom_up, R.anim.buttom_down);
        setContentView(R.layout.activity_help);
        initUI();
        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_support_open);


    }


    @Override
    public void initUI() {
        super.initUI();
        headerTxt = (TextView) findViewById(R.id.titleTxt);
        headerTxt.setText(getString(R.string.support_spaced));
        backImg = (Button) findViewById(R.id.backImg);
        helpBtn = (Button) findViewById(R.id.helpBtn);

        ll_findUs = (LinearLayout) findViewById(R.id.ll_find_us_help);
        ll_findUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_support_find_us);

                startActivity(new Intent(HelpActivity.this, FindUsFragment.class));
            }
        });

        helpBtn.setVisibility(View.GONE);
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ll_faq = (LinearLayout) findViewById(R.id.ll_faq_help);
        ll_faq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_support_faqs);
                startActivity(new Intent(HelpActivity.this, FaqsActivity.class));
            }
        });

        ll_liveChat = (LinearLayout) findViewById(R.id.ll_live_chat_help);
        ll_whatsSwyp = (LinearLayout) findViewById(R.id.ll_whats_swyp_help);
        ll_whatsSwyp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HelpActivity.this, StaticPackagesFragments.class);
                startActivity(intent);
            }
        });


        //..............header Buttons............
        RelativeLayout backLayout = (RelativeLayout) findViewById(R.id.backLayout);
        RelativeLayout helpLayout = (RelativeLayout) findViewById(R.id.helpLayout);
        ((TextView) findViewById(R.id.tv_whats_swyp_description)).setText("            " + getString(R.string.whats_swyp_detail));
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard();
                onBackPressed();
            }
        });
        //..............header Buttons............
        if (sharedPrefrencesManger != null)
            if (sharedPrefrencesManger.getLiveChatStaus() == true)
                enableLiveChatBtn();
            else
                disableLiveChatBtn();

    }

    private void disableLiveChatBtn() {
        ll_liveChat.setAlpha(0.5f);
        ll_liveChat.setEnabled(false);
        ll_liveChat.setOnClickListener(null);
    }

    private void enableLiveChatBtn() {
        ll_liveChat.setAlpha(1.0f);
        ll_liveChat.setEnabled(true);
        ll_liveChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_support_live_chat);

                startActivity(new Intent(HelpActivity.this, LiveChatActivity.class));
            }
        });
    }
}
