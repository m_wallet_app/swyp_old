package com.indy.views.activites;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.Utils;
import com.indy.R;
import com.indy.adapters.PackagesListAdapter;
import com.indy.controls.ServiceUtils;
import com.indy.helpers.DateHelpers;
import com.indy.models.packages.OfferList;
import com.indy.models.packages.PackageConsumption;
import com.indy.models.packages.UsagePackageList;
import com.indy.models.packageusage.GraphPointList;
import com.indy.models.packageusage.OfferInfo;
import com.indy.models.packageusage.PackageInfo;
import com.indy.models.packageusage.PackageUsageInput;
import com.indy.models.packageusage.PackageUsageOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.services.PackageUsageService;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.usage.ManagePreferredNumber;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class UnlimitedMinutesPackageActivity extends MasterActivity {

    private LineChart mChart;
    int xVal = 30;
    float yVal = 100;

    float upperLimt = 0;
    float lowerLimit = 0f;
    private YAxis leftAxis;
    private LimitLine llXAxis;
    private Button helpBtn;
    public DateHelpers dateUtils;

    ArrayList<OfferList> offerList;
    private TextView titleTxt, headerTxt;
    TextView tv_expires, tv_extraDataToBeHidden, tv_preferred_number;
    private Button backImg, helpImg, bt_subUnsub;
    public LinearLayout ll_topLayout, ll_content;
    FrameLayout frameLayout;
    TextView tv_edit, tv_packageExpirationWarning;
    public final static int request_permission_for_contacts = 10;
    private PackageUsageOutput packageUsageOutput;
    private PackageUsageInput packageUsageInput;
    UsagePackageList userPackgeListItem;
    public static boolean isMembershipVlaid = true;
    ArrayList<Entry> values = new ArrayList<Entry>();
    TextView startDateID, endDateId;
    TextView data_package_name, tv_balance_expire, tv_expire_left;
    private ImageView iv_packageIcon;

    TextView tv_dataUsedToday, tv_dailyEstimatedUsage, tv_forecastUsage;
    public static boolean isPackageCancelled = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unlimited_package);
        initUI();
        onConsumeService();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (isPackageCancelled) {
            if (bt_subUnsub != null) {
                bt_subUnsub.setAlpha(0.4f);
                bt_subUnsub.setEnabled(false);
                isMembershipVlaid = false;
            }
        }
    }

    @Override
    public void initUI() {
        super.initUI();
        mChart = (LineChart) findViewById(R.id.chart1);
        data_package_name = (TextView) findViewById(R.id.data_package_name);
        iv_packageIcon = (ImageView) findViewById(R.id.iv_packageIcon);

        tv_expire_left = (TextView) findViewById(R.id.tv_expire_days_left);
        headerTxt = (TextView) findViewById(R.id.titleTxt);

        tv_dailyEstimatedUsage = (TextView) findViewById(R.id.tv_mbused_daily);
        tv_dataUsedToday = (TextView) findViewById(R.id.tv_mbused);
        tv_forecastUsage = (TextView) findViewById(R.id.tv_mbused_forecast);
        tv_preferred_number = (TextView) findViewById(R.id.tv_preferred_number);
        headerTxt.setText(getString(R.string.data));
        backImg = (Button) findViewById(R.id.backImg);
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        helpImg = (Button) findViewById(R.id.helpBtn);
        helpImg.setVisibility(View.INVISIBLE);
        helpImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(UnlimitedMinutesPackageActivity.this, HelpActivity.class));
            }
        });
        frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
        frameLayout.setVisibility(View.GONE);
        ll_topLayout = (LinearLayout) findViewById(R.id.ll_top_layout);
        ll_content = (LinearLayout) findViewById(R.id.ll_content);
        userPackgeListItem = getIntent().getParcelableExtra(ServiceUtils.packageListType);

        offerList = getIntent().getParcelableArrayListExtra("OfferInfoList");
        userPackgeListItem.setOfferList(offerList);
//        tv_dataAllowanceRemaining = (TextView) findViewById(R.id.tv_user_balance_amount);
        tv_balance_expire = (TextView) findViewById(R.id.tv_balance_expire);

        startDateID = (TextView) findViewById(R.id.startDateID);
        endDateId = (TextView) findViewById(R.id.endDateId);
        if (currentLanguage.equals("en")) {

            headerTxt.setText(userPackgeListItem.getNameEn().toUpperCase());
        } else {
            headerTxt.setText(userPackgeListItem.getNameAr().toUpperCase());
        }
        tv_edit = (TextView) findViewById(R.id.edit);
        tv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList("OfferInfoList", offerList);
                bundle.putString(ConstantUtils.oldPreferredNumber, packageUsageOutput.getUsageGraphList().get(0).getPreferredNumber());
//                packageUsageOutput.getUsageGraphList().get(0).getPreferredNumber());
                ManagePreferredNumber managePreferredNumber = new ManagePreferredNumber();
                managePreferredNumber.setArguments(bundle);
                frameLayout.setVisibility(View.VISIBLE);
                ll_content.setVisibility(View.GONE);
                addFragmnet(managePreferredNumber, R.id.frameLayout, true);
            }
        });
        ll_content.setVisibility(View.VISIBLE);
        bt_subUnsub = (Button) findViewById(R.id.btnUnsubSub);
        bt_subUnsub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                SubscribeUnsubscribePackage subscribeUnsubscribePackage = new SubscribeUnsubscribePackage();
//                Bundle bundle = new Bundle();
//                bundle.putBoolean(ConstantUtils.isMembershipValid, isMembershipVlaid);
//                bundle.putString(ConstantUtils.mobileNo, packageUsageOutput.getUsageGraphList().get(0).getPreferredNumber());
//                subscribeUnsubscribePackage.setArguments(bundle);
//                showFrame();
//                replaceFragmnet(subscribeUnsubscribePackage, R.id.frameLayout, true);
            }
        });


        tv_edit.setAlpha(1.0f);
        tv_edit.setEnabled(true);

        bt_subUnsub.setAlpha(1.0f);
        bt_subUnsub.setEnabled(true);
        tv_packageExpirationWarning = (TextView) findViewById(R.id.tv_expiration_date);
        isMembershipVlaid = (sharedPrefrencesManger.getUserProfileObject().getMembershipData().getStatus() == ConstantUtils.validMembership);
        if (isMembershipVlaid) {
            tv_packageExpirationWarning.setVisibility(View.VISIBLE);
        } else {
            tv_packageExpirationWarning.setVisibility(View.GONE);
        }

        setupData();
        // onConsumeService();

    }

    public void setupData() {

//        tv_expires.setText(getString(R.string.get_remaining_balance_Expire).toUpperCase() + " " + parseDate(userPackgeListItem.getOfferList().get(0).getInOfferEndDate()));
        String remainingAllowance = "";
        if (currentLanguage.equals("en")) {
            remainingAllowance = getString(R.string.you_have_unlimited_minutes) + " " + userPackgeListItem.getOfferList().get(0).getPackageConsumption().getRemaining() + userPackgeListItem.getOfferList().get(0).getPackageConsumption().getConsumptionUnitEn();
            data_package_name.setText(userPackgeListItem.getDescriptionEn().toString());
            tv_balance_expire.setText(" " + parseDateFormatChart(userPackgeListItem.getOfferList().get(0).getInOfferEndDate()));

        } else {
            remainingAllowance = getString(R.string.you_have_unlimited_minutes) + " " + userPackgeListItem.getOfferList().get(0).getPackageConsumption().getRemaining() + userPackgeListItem.getOfferList().get(0).getPackageConsumption().getConsumptionUnitAr();
            data_package_name.setText(userPackgeListItem.getDescriptionAr().toString());
            tv_balance_expire.setText(" " + parseDateFormatChart(userPackgeListItem.getOfferList().get(0).getInOfferEndDate()));

        }
//        tv_dataAllowanceRemaining.setText(remainingAllowance);

        if (userPackgeListItem.getOfferList().get(0).getInOfferEndDate() != null) {
            tv_expire_left.setText(daysDifference(userPackgeListItem.getOfferList().get(0).getInOfferEndDate(), getString(R.string.days_left)));
        } else {
            tv_expire_left.setText("");

        }

        if (userPackgeListItem.getIconUrl() != null) {
            Picasso.with(this).load(userPackgeListItem.getIconUrl()).
                    networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE)

                    .into(iv_packageIcon);
        }
    }

    private void showErrorFragment(String error) {
        frameLayout.setVisibility(View.VISIBLE);
        ll_content.setVisibility(View.GONE);
//        frameLayout.bringToFront();
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, error);
        errorFragment.setArguments(bundle);
        addFragmnet(errorFragment, R.id.frameLayout, true);
    }

    public void showFrame() {
        frameLayout.setVisibility(View.VISIBLE);
        ll_content.setVisibility(View.GONE);
    }

    public void hideFrame() {
        frameLayout.setVisibility(View.GONE);
        ll_content.setVisibility(View.VISIBLE);
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();


        addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        frameLayout.setVisibility(View.VISIBLE);
        packageUsageInput = new PackageUsageInput();
        packageUsageInput.setAppVersion(appVersion);
        packageUsageInput.setToken(token);
        packageUsageInput.setOsVersion(osVersion);
        packageUsageInput.setChannel(channel);
        packageUsageInput.setDeviceId(deviceId);
        packageUsageInput.setMsisdn(sharedPrefrencesManger.getMobileNo());
        packageUsageInput.setAuthToken(authToken);
        PackageInfo packageInfo = new PackageInfo();
        packageInfo.setPackageId(userPackgeListItem.getPackageType() + "");
        List<OfferList> offerInfoList = new ArrayList<OfferList>();
        offerInfoList.addAll(offerList);
        List<OfferInfo> offerInfo = new ArrayList<OfferInfo>();

        OfferInfo offerInfo1 = new OfferInfo();
        for (int i = 0; i < offerInfoList.size(); i++) {
            offerInfo1.setStartDate(parseDateFormat(userPackgeListItem.getOfferList().get(i).getInOfferStartDate()));
            offerInfo1.setEndDate(parseDateFormat(userPackgeListItem.getOfferList().get(i).getInOfferEndDate()));
            offerInfo1.setOfferId(offerInfoList.get(i).getInOfferId());
            offerInfo.add(offerInfo1);
        }
        packageInfo.setOfferInfo(offerInfo);
        packageUsageInput.setPackageInfo(packageInfo);
        new PackageUsageService(this, packageUsageInput);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        try {
            onBackPressed();
            tv_edit.setAlpha(0.5f);
            tv_edit.setEnabled(false);
            frameLayout.setVisibility(View.GONE);
            if (sharedPrefrencesManger.getUsedActivity().equals("100")) {

            } else {
//            initTabLayout();
            }
            sharedPrefrencesManger.setUsedActivity("");
        }catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        if (frameLayout.getVisibility() == View.VISIBLE) {
            hideFrame();
        }
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            onBackPressed();

            frameLayout.setVisibility(View.GONE);
            packageUsageOutput = (PackageUsageOutput) responseModel.getResultObj();
            values.clear();
            if (packageUsageOutput.getUsageGraphList() != null && packageUsageOutput.getUsageGraphList().size() > 0 && packageUsageOutput.getUsageGraphList().get(0) != null) {
                if (packageUsageOutput.getUsageGraphList().get(0).getPreferredNumber() != null
                        && !packageUsageOutput.getUsageGraphList().get(0).getPreferredNumber().equals("")) {
                    tv_edit.setAlpha(1f);
                    tv_edit.setEnabled(true);
                } else {
                    tv_edit.setAlpha(0.5f);
                    tv_edit.setEnabled(false);
                }
                AssembleChart();

            } else {
                onPackageFail();

            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
//        if (packageUsageOutput != null)
//            Log.v("oooooooooops", packageUsageOutput.getUsageGraphList().get(0).getColor() + "");
    }


    public void onPackageFail() {

        if (packageUsageOutput != null && packageUsageOutput.getErrorMsgEn() != null) {
            if (currentLanguage.equals("en")) {
                showErrorFragment(packageUsageOutput.getErrorMsgEn());
            } else if (currentLanguage.equals("ar")) {
                showErrorFragment(packageUsageOutput.getErrorMsgAr());
            } else {
                showErrorFragment(getString(R.string.generice_error));
            }
        }

        tv_edit.setAlpha(0.5f);
        tv_edit.setEnabled(false);

        bt_subUnsub.setAlpha(0.5f);
        bt_subUnsub.setEnabled(false);
    }

    private void AssembleChart() {
//        mChart.setViewPortOffsets(0, 20, 0, 0);

        tv_edit.setAlpha(1.0f);
        tv_edit.setEnabled(true);

        bt_subUnsub.setAlpha(1.0f);
        bt_subUnsub.setEnabled(true);


        mChart.setDrawGridBackground(false);

        // no description text
        mChart.setDescription("");

        PackageConsumption packageConsumption = PackagesListAdapter.usagePackageItem.getOfferList().get(0).getPackageConsumption();
        mChart.setNoDataTextDescription("You need to provide data for the chart.");

        // String toStartDate =  convertToDate(userPackgeListItem.getStartDate()).toString();
        startDateID.setText(parseDateForStartEndDate(userPackgeListItem.getOfferList().get(0).getInOfferStartDate()).toString());


        endDateId.setText(parseDateForStartEndDate(userPackgeListItem.getOfferList().get(0).getInOfferEndDate()).toString());

        tv_preferred_number.setText(getString(R.string.your_preferred_number_is) + " " + packageUsageOutput.getUsageGraphList().get(0).getPreferredNumber());

        if (packageUsageOutput.getUsageGraphList().get(0).getPreferredNumberOfferStatus().equals("active")) {
            tv_packageExpirationWarning.setText(getString(R.string.your_package_is_expiring_in) + " " +
                    daysDifference(userPackgeListItem.getOfferList().get(0).getInOfferEndDate(), getString(R.string.days)));
            tv_packageExpirationWarning.setVisibility(View.VISIBLE);
            bt_subUnsub.setText(getString(R.string.autorenew_off));
            isMembershipVlaid = true;
        } else if (packageUsageOutput.getUsageGraphList().get(0).getPreferredNumberOfferStatus().equals("active")) {
            tv_packageExpirationWarning.setVisibility(View.INVISIBLE);
            isMembershipVlaid = false;
            bt_subUnsub.setAlpha(0.4f);
            bt_subUnsub.setEnabled(false);
        }
        // add data
        String limitFromService = PackagesListAdapter.usagePackageItem.getOfferList().get(0).getPackageConsumption().getQuota().toString();
        setData(xVal, Float.parseFloat(limitFromService));
        //   mChart.setVisibleXRange(20);
        // mChart.setVisibleYRange(20f, YAxis.AxisDependency.LEFT);
        //   mChart.centerViewTo(20, 50, YAxis.AxisDependency.LEFT);


        //          upperLimt =70 ;
        // enable touch gestures
        mChart.setTouchEnabled(false);
//
        // enable scaling and dragging
        mChart.setDragEnabled(false);
        mChart.setScaleEnabled(false);
//        // mChart.setScaleXEnabled(true);
        mChart.setScaleYEnabled(true);
//
        // if disabled, scaling can be done on§ x- and y-axis separately
        mChart.setPinchZoom(false);

        // set an alternative background color
        // mChart.setBackgroundColor(Color.GRAY);

        // create a custom MarkerView (extend MarkerView) and specify the layout
        // to use for it
//        MyMarkerView mv = new MyMarkerView(this, R.layout.custom_marker_view);
//        // set the marker to the chart
//        mChart.setMarkerView(mv);
        // x-axis limit line
        llXAxis = new LimitLine(Float.parseFloat(limitFromService), "Index 10");
        llXAxis.setLineWidth(1f);

        llXAxis.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
        llXAxis.setTextSize(10f);
        XAxis xAxis = mChart.getXAxis();
        xAxis.setEnabled(true);
//        xAxis.enableGridDashedLine(10f, 10f, 0f);
        xAxis.disableGridDashedLine();
        //xAxis.setValueFormatter(new MyCustomXAxisValueFormatter());
        //xAxis.addLimitLine(llXAxis); // add x-axis limit line
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setAxisLineWidth(1);
        xAxis.setAxisLineColor(getResources().getColor(R.color.graph_line_color));
        upperLimt = Float.parseFloat(limitFromService);
        lowerLimit = 0f;
        LimitLine ll1;

        if (sharedPrefrencesManger.getLanguage().equals("en")) {
            ll1 = new LimitLine(upperLimt, "");
//            ll1.setLineWidth(1f);
//            ll1.setLineColor(getResources().getColor(R.color.graph_line_color));
            tv_dataUsedToday.setText(packageUsageOutput.getUsageGraphList().get(0).getUsedToday() + " " + packageConsumption.getConsumptionUnitEn());
            tv_forecastUsage.setText(packageUsageOutput.getUsageGraphList().get(0).getForecastedUsage() + " " + packageConsumption.getConsumptionUnitEn());
//        tv_dataAllowanceRemaining.setText(packageUsageOutput.getUsageGraphList().get(0).getDailyBudget());
//            tv_dailyEstimatedUsage.setText(packageUsageOutput.getUsageGraphList().get(0).getDailyBudget() + " " + packageConsumption.getConsumptionUnitEn());
        } else {
            ll1 = new LimitLine(upperLimt, "");
//            ll1.setLineWidth(1f);
//            ll1.setLineColor(getResources().getColor(R.color.graph_line_color));
            tv_dataUsedToday.setText(packageUsageOutput.getUsageGraphList().get(0).getUsedToday() + " " + packageConsumption.getConsumptionUnitAr());
            tv_forecastUsage.setText(packageUsageOutput.getUsageGraphList().get(0).getForecastedUsage() + " " + packageConsumption.getConsumptionUnitAr());
//        tv_dataAllowanceRemaining.setText(packageUsageOutput.getUsageGraphList().get(0).getDailyBudget());
//            tv_dailyEstimatedUsage.setText(packageUsageOutput.getUsageGraphList().get(0).getDailyBudget() + " " + packageConsumption.getConsumptionUnitAr());

        }
        ll1.setLineWidth(1f);
        ll1.setLineColor(getResources().getColor(R.color.graph_line_color));

//        ll1.enableDashedLine(10f, 10f, 0f);
        ll1.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
        ll1.setTextSize(14f);
//        ll1.setTypeface(tf);

//        LimitLine ll2 = new LimitLine(lowerLimit, "Lower Limit");
//        ll2.setLineWidth(2f);
////        ll2.enableDashedLine(10f, 10f, 0f);
//        ll2.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
//        ll2.setTextSize(10f);
////        ll2.setTypeface(tf);
//        ll2.setLineColor(android.R.color.transparent);
        leftAxis = mChart.getAxisLeft();
        leftAxis.removeAllLimitLines(); // reset all limit lines to avoid overlapping lines
        leftAxis.addLimitLine(ll1);
        //  leftAxis.addLimitLine(ll2);
        leftAxis.setAxisMaxValue(upperLimt);
        leftAxis.setAxisMinValue(lowerLimit);
        leftAxis.setYOffset(0f);
        //   leftAxis.enableGridDashedLine(10f, 10f, 0f);
        leftAxis.setDrawZeroLine(false);
        // limit lines are drawn behind data (and not on top)
        leftAxis.setDrawLimitLinesBehindData(false);
        leftAxis.setMaxWidth(1);
        leftAxis.setAxisLineWidth(1);
        mChart.getAxisRight().setEnabled(false);

        //mChart.getViewPortHandler().setMaximumScaleY(2f);
        //mChart.getViewPortHandler().setMaximumScaleX(2f);

        mChart.animateX(2500);
        //mChart.invalidate();

        // get the legend (only possible after setting data)
        mChart.getLegend().setEnabled(false);

        // modify the legend ...
        // l.setPosition(LegendPosition.LEFT_OF_CHART);
        // l.setForm(Legend.LegendForm.LINE);

//        disableCordinaets();
//        enableCordinates();
        // // dont forget to refresh the drawing
        // mChart.invalidate();
        toogleValues();
        toogleCubed();
        toogleCircles();

        animateXY();
        disableGridView();
        disableCordinaets();
    }

    private void disableCordinaets() {
        mChart.getXAxis().setTextColor(android.R.color.transparent);
        leftAxis.setTextColor(android.R.color.transparent);
    }

    private void disableGridView() {
        mChart.getAxisLeft().setDrawGridLines(false);
        mChart.getXAxis().setDrawGridLines(false);
    }

    private void enableCordinates() {
        mChart.getXAxis().setTextColor(android.R.color.transparent);
        leftAxis.setTextColor(android.R.color.transparent);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
    }

    private void toogleValues() {
        List<ILineDataSet> sets = mChart.getData()
                .getDataSets();

        for (ILineDataSet iSet : sets) {

            LineDataSet set = (LineDataSet) iSet;
            set.setDrawValues(!set.isDrawValuesEnabled());
        }

        mChart.invalidate();
    }

    private void toogleCubed() {
        List<ILineDataSet> sets = mChart.getData()
                .getDataSets();

        for (ILineDataSet iSet : sets) {

            LineDataSet set = (LineDataSet) iSet;
            set.setMode(set.getMode() == LineDataSet.Mode.CUBIC_BEZIER
                    ? LineDataSet.Mode.LINEAR
                    : LineDataSet.Mode.CUBIC_BEZIER);
        }
        mChart.invalidate();
    }

    private void toogleCircles() {
        List<ILineDataSet> sets = mChart.getData()
                .getDataSets();

        for (ILineDataSet iSet : sets) {

            LineDataSet set = (LineDataSet) iSet;
            if (set.isDrawCirclesEnabled())
                set.setDrawCircles(false);
            else
                set.setDrawCircles(true);
        }
        mChart.invalidate();
    }

    private void animateXY() {
        mChart.animateXY(3000, 3000);

    }


    private void setData(int count, float range) {


        List<GraphPointList> tempList = packageUsageOutput.getUsageGraphList().get(0).getGraphPointList();

        Collections.sort(tempList, new Comparator<GraphPointList>() {
            @Override
            public int compare(GraphPointList lhs, GraphPointList rhs) {
                if (lhs.getYAxisValue() < rhs.getYAxisValue()) {
                    return -1;
                } else {
                    return 1;
                }
            }
        });


        ArrayList<Entry> values = new ArrayList<Entry>();
        for (int i = 0; i < count; i++) {
            values.add(new Entry(i, 0));
        }
        for (int i = 0; i < packageUsageOutput.getUsageGraphList().get(0).getGraphPointList().size(); i++) {
//                if (i < 10) {
            String valToFloat = tempList.get(i).getYAxisValue().toString();
            float val = Float.parseFloat(valToFloat);
            upperLimt = (float) (tempList.get(i).getYAxisValue() + 5);

            String xValueToChange = packageUsageOutput.getUsageGraphList().get(0).getGraphPointList().get(i).getXAxisValue().toString();
            float xValue = Float.parseFloat(xValueToChange);
            //   int abc = Integer.parseInt(Collections.max(tempList.get(i).getYAxisValue()/11));

            values.set(Math.round(xValue),
                    new Entry(xValue, val));
//                } else {
//                    float val = (float) (tempList.get(i).getYAxisValue()/11);
//                    values.add(new Entry(i, val));
//
//                }
        }


        LineDataSet set1;

        if (mChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) mChart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            set1.setAxisDependency(YAxis.AxisDependency.LEFT);
            set1.setColor(getResources().getColor(R.color.orange_color));
            set1.setCircleColor(getResources().getColor(R.color.orange_color));
            set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        } else {
            // create a dataset and give it a type
//            set1 = new LineDataSet(values, "10 days Remaining");
            set1 = new LineDataSet(values, "");
            // set the line to be drawn like this "- - - - - -"
//            set1.enableDashedLine(10f, 5f, 0f);
            set1.enableDashedHighlightLine(10f, 5f, 0f);
            set1.setAxisDependency(YAxis.AxisDependency.LEFT);
            set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
            set1.setColor(getResources().getColor(R.color.orange_color));

            set1.setCircleColor(getResources().getColor(R.color.orange_color));
            set1.setLineWidth(1f);
            set1.setCircleRadius(3f);
            set1.setDrawCircleHole(false);
            set1.setValueTextSize(9f);
            set1.setDrawFilled(true);

            if (Utils.getSDKInt() >= 18) {
                // fill drawable only supported on api level 18 and above
                Drawable drawable = ContextCompat.getDrawable(this, R.drawable.fade_red);
                set1.setFillDrawable(drawable);
            } else {
                set1.setFillColor(getResources().getColor(R.color.graph_line_color));
            }

            ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
            dataSets.add(set1); // add the datasets

            // create a data object with the datasets
            LineData data = new LineData(dataSets);

            // set data
            mChart.setData(data);
        }
    }


    public static String parseDate(String date) {
        String returnString = "";
        SimpleDateFormat dateFormatter;
        try {
            Date formattedDate;
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
            formattedDate = df.parse(date);
            dateFormatter = new SimpleDateFormat("dd MMM", Locale.US);
            return dateFormatter.format(formattedDate);
        } catch (Exception ex) {
            return returnString;

        }
    }

    public static String parseDateFormat(String date) {
        String returnString = "";
        SimpleDateFormat dateFormatter;
        try {
            Date formattedDate;
            //28/09/2016 19:13:20
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
            formattedDate = df.parse(date);
            dateFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
            return dateFormatter.format(formattedDate);
        } catch (Exception ex) {
            return returnString;

        }
    }


    public static String parseDateFormatChart(String date) {
        String returnString = "";
        SimpleDateFormat dateFormatter;
        try {
            Date formattedDate;
            //28/09/2016 19:13:20
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
            formattedDate = df.parse(date);
            dateFormatter = new SimpleDateFormat("dd MMM, HH:mm", Locale.US);
            return dateFormatter.format(formattedDate);
        } catch (Exception ex) {
            return returnString;

        }
    }


    public static String parseDateForStartEndDate(String date) {
        String returnString = "";
        SimpleDateFormat dateFormatter;
        try {
            Date formattedDate;
            //28/09/2016 19:13:20
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
            formattedDate = df.parse(date);
            dateFormatter = new SimpleDateFormat("dd MMM", Locale.US);
            return dateFormatter.format(formattedDate);
        } catch (Exception ex) {
            return returnString;

        }
    }

    public void showConfirmationFragment(int mode) {
        switch (mode) {
            case 0:


                break;

            case 1:
                break;
        }
    }

    public String daysDifference(String date, String valueToAppend) {
        Date dateToPass;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
        try {

            dateToPass = simpleDateFormat.parse(date);

        } catch (Exception ex) {
            dateToPass = null;
        }
        if (dateToPass != null) {
            long diffInMils = dateToPass.getTime() - Calendar.getInstance().getTime().getTime();
            long daysDiff = TimeUnit.MILLISECONDS.toDays(diffInMils);
            return daysDiff + " " + valueToAppend;
        } else {
            return date + " " + getString(R.string.days_left);
        }
    }


}
