package com.indy.views.activites;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareContent;
import com.facebook.share.model.ShareMediaContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.widget.ShareDialog;
import com.indy.R;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.ConstantUtils;
import com.indy.utils.ImageUtil;
import com.indy.views.fragments.gamification.models.socialshare.SocialShareInput;
import com.indy.views.fragments.gamification.models.socialshare.SocialShareOutput;
import com.indy.views.fragments.gamification.services.SocialShareService;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.tweetcomposer.ComposerActivity;
import com.twitter.sdk.android.tweetcomposer.TweetUploadService;

import java.util.Arrays;

import javax.annotation.Nullable;

/**
 * Created by Tohamy on 10/1/2017.
 */

public class ShareMasterActivity extends MasterActivity {

    private static final String FACEBOOK_LOGIN_FLAG = "FACEBOOK_LOGIN_FLAG";
    private static final String TWITTER_LOGIN_FLAG = "TWITTER_LOGIN_FLAG";
    protected boolean twitterLogin;
    private TwitterResultReceiver resultReceiver;
    protected boolean faceBookLogin;
    private TwitterAuthClient twitterAuthClient;
    private CallbackManager callbackManager;
    protected Bitmap bitmap;
    protected String referenceID = null;
    protected String itemType = null;
    String idForService = "";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        callbackManager = CallbackManager.Factory.create();
    }

    public void setBitmapToShare(Bitmap bitmap){
        this.bitmap = bitmap;
    }

    protected void twitterLogin(final boolean share,final String id,final String stringToShare) {
        twitterLogin = true;
        faceBookLogin = false;
        String twitterAuthToken = sharedPrefrencesManger.getTwitterAuthToken();
        String twitterAuthSecret = sharedPrefrencesManger.getTwitterAuthSecret();
        if (!twitterAuthToken.isEmpty() && !twitterAuthSecret.isEmpty()) {
            if (share)
                twitterIntentShare(id,stringToShare);
        } else {
            twitterAuthClient = new TwitterAuthClient();
            twitterAuthClient.authorize(ShareMasterActivity.this, new Callback<TwitterSession>() {
                @Override
                public void success(Result<TwitterSession> twitterSessionResult) {
                    final TwitterSession session = twitterSessionResult.data;
                    TwitterAuthToken authToken = session.getAuthToken();
                    if (authToken != null && authToken.token != null && authToken.secret != null) {
                        sharedPrefrencesManger.setTwitterAuthToken(authToken.token);
                        sharedPrefrencesManger.setTwitterAuthSecret(authToken.secret);
                    }
                    if (share) {
                        twitterIntentShare(id,stringToShare);
//                    TwitterCo
                    }else {
                        try {
                            sharedPrefrencesManger.setTwitterUsername(twitterSessionResult.data.getUserName());
                            AboutAppActivity.tv_twitterUsername.setText(twitterSessionResult.data.getUserName());
                            AboutAppActivity.twitterLoginSuccess();
                        }catch (Exception ex){

                        }
                    }
                }

                @Override
                public void failure(com.twitter.sdk.android.core.TwitterException e) {
                    if (share)
                        shareFail();
                }
            });
        }
    }

    protected void twitterLogout() {
        TwitterCore.getInstance().getSessionManager().clearActiveSession();
        sharedPrefrencesManger.setTwitterAuthToken("");
        sharedPrefrencesManger.setTwitterAuthSecret("");

    }

    private void twitterIntentShare(String id,String stringToShare) {
        idForService = id;
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(TweetUploadService.UPLOAD_SUCCESS);
        intentFilter.addAction(TweetUploadService.UPLOAD_FAILURE);
        intentFilter.addAction(TweetUploadService.TWEET_COMPOSE_CANCEL);
        resultReceiver = new TwitterResultReceiver();
        registerReceiver(resultReceiver, intentFilter);
        final TwitterSession session = TwitterCore.getInstance().getSessionManager()
                .getActiveSession();
        Uri uri = ImageUtil.getImageUri(getApplicationContext(), bitmap != null ? bitmap : BitmapFactory.decodeResource(getResources(), R.drawable.dummy_bg_event_detail));
        Intent intent = new ComposerActivity.Builder(ShareMasterActivity.this)
                .session(session)
                .text("#swyp")
                .image(uri).createIntent();
        startActivity(intent);
    }



    protected void facebookShare(String id, @Nullable String messageToShare) {
        idForService = id;
        faceBookLogin = true;
        twitterLogin = false;
        SharePhoto sharePhoto1 = new SharePhoto.Builder()
                .setBitmap(bitmap != null ? bitmap : BitmapFactory.decodeResource(getResources(), R.drawable.dummy_bg_event_detail))
                .setCaption("#swyp")
                .build();
        ShareContent shareContent = new ShareMediaContent.Builder()
                .addMedium(sharePhoto1)
                .build();
        ShareDialog shareDialog = new ShareDialog(ShareMasterActivity.this);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Log.d("Facaebook Result", result.getPostId() + "id");
                shareSuccess();
            }

            @Override
            public void onCancel() {
                Log.d("Facebook status", "Facaebook share cancelled");
                shareFail();
            }

            @Override
            public void onError(FacebookException error) {
                Log.d("Facaebook Result", error.getMessage());
                shareFail();
            }
        });
        shareDialog.show(shareContent, ShareDialog.Mode.AUTOMATIC);
    }


    private void onSocialShare() {
        if(idForService!=null && idForService.length()>0) {
            super.onConsumeService();
            SocialShareInput socialShareInput = new SocialShareInput();
            socialShareInput.setChannel(channel);
            socialShareInput.setLang(currentLanguage);
            socialShareInput.setMsisdn(sharedPrefrencesManger.getMobileNo());
            socialShareInput.setAppVersion(appVersion);
            socialShareInput.setAuthToken(authToken);
            socialShareInput.setDeviceId(deviceId);
            socialShareInput.setToken(token);
            socialShareInput.setImsi(sharedPrefrencesManger.getMobileNo());
            socialShareInput.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
            socialShareInput.setOsVersion(osVersion);
            socialShareInput.setUserSessionId(sharedPrefrencesManger.getUserSessionId());
            socialShareInput.setUserId(sharedPrefrencesManger.getUserId());
            socialShareInput.setApplicationId(ConstantUtils.INDY_TALOS_ID);
            socialShareInput.setMediaChannel(faceBookLogin ? ConstantUtils.FACEBOOK : ConstantUtils.TWITTER);
            socialShareInput.setItemId(idForService);
            socialShareInput.setItemType(itemType);
            socialShareInput.setReferenceId(referenceID);
            new SocialShareService(this, socialShareInput);
        }
    }
    public void shareSuccess() {
        onSocialShare();
    }

    public void shareFail() {

    }

    public class TwitterResultReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
//            Bundle intentExtras = intent.getExtras();
            if (TweetUploadService.UPLOAD_SUCCESS.equals(intent.getAction())) {
                // success
                //  final Long tweetId = intentExtras.getLong(TweetUploadService.EXTRA_TWEET_ID);
                shareSuccess();
            } else if (TweetUploadService.UPLOAD_FAILURE.equals(intent.getAction())) {
                // failure
                //  final Intent retryIntent = intentExtras.getParcelable(TweetUploadService.EXTRA_RETRY_INTENT);
                shareFail();
            } else if (TweetUploadService.TWEET_COMPOSE_CANCEL.equals(intent.getAction())) {
                // cancel
                shareFail();
            }
            unregisterReceiver(resultReceiver);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (callbackManager != null && faceBookLogin) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        if (twitterAuthClient != null && twitterLogin) {
            twitterAuthClient.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(FACEBOOK_LOGIN_FLAG, faceBookLogin);
        outState.putBoolean(TWITTER_LOGIN_FLAG, twitterLogin);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        faceBookLogin = savedInstanceState.getBoolean(FACEBOOK_LOGIN_FLAG);
        twitterLogin = savedInstanceState.getBoolean(TWITTER_LOGIN_FLAG);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try{
            if(responseModel!=null && responseModel.getResultObj()!=null) {
                SocialShareOutput socialShareOutput = (SocialShareOutput) responseModel.getResultObj();
                if(socialShareOutput!=null && socialShareOutput.isSuccess()){

                }
            }
        }catch (Exception ex){

        }
    }



    protected void facebookLogin(final boolean share,final String id,final String stringToShare) {
        faceBookLogin = true;
        twitterLogin = false;
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.d("Success", "Login");
                        faceBookLoginSuccess(share,id,stringToShare);
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(ShareMasterActivity.this, "Login Cancel", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Toast.makeText(ShareMasterActivity.this, exception.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends"));
    }

    public void faceBookLoginSuccess(final boolean share,final String id,String stringToShare) {
        if(share){
            facebookShare(id,stringToShare);
        }else{
            try {
                sharedPrefrencesManger.setFacebookUsername(getFacebookFullname());
                AboutAppActivity.faceBookLoginSuccess();
            }catch (Exception ex){

            }
        }
    }

    public static String getFacebookFullname() {
        Profile profile = Profile.getCurrentProfile();

        return profile.getName();
    }

    public static void twitterLoginSuccess() {
        AboutAppActivity.twitterLoginSuccess();

    }
}
