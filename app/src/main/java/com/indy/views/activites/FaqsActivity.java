package com.indy.views.activites;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.models.faqs.FaqsInputModel;
import com.indy.models.faqs.FaqsResponseModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.FaqsService;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;

/**
 * Created by emad on 8/16/16.
 */
public class FaqsActivity extends MasterActivity {

    private WebView paymentWebView;
    private ProgressBar progressID;
    Button backImg, helpBtn;
    TextView titleTxt;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faqs);
        initUI();
    }

    @Override
    public void initUI() {
        super.initUI();
        paymentWebView = (WebView) findViewById(R.id.paymentWebView);
        progressID = (ProgressBar) findViewById(R.id.progressID);
        backImg = (Button) findViewById(R.id.backImg);
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        titleTxt = (TextView) findViewById(R.id.titleTxt);
        titleTxt.setText(getString(R.string.faq_spaced));

        helpBtn = (Button) findViewById(R.id.helpBtn);
        helpBtn.setVisibility(View.INVISIBLE);
        WebSettings settings = paymentWebView.getSettings();
        settings.setJavaScriptEnabled(true);
        paymentWebView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

        replaceFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        onConsumeService();
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //..............header Buttons............
        RelativeLayout backLayout = (RelativeLayout) findViewById(R.id.backLayout);
        RelativeLayout helpLayout = (RelativeLayout) findViewById(R.id.helpLayout);

        helpLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FaqsActivity.this, HelpActivity.class);
                startActivity(intent);
            }
        });

        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard();
                onBackPressed();
            }
        });
        //..............header Buttons............

    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        FaqsInputModel faqsInputModel = new FaqsInputModel();
        faqsInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
        faqsInputModel.setLang(currentLanguage);
        faqsInputModel.setAppVersion(appVersion);
        faqsInputModel.setToken(token);
        faqsInputModel.setChannel(channel);
        faqsInputModel.setSignificant(ConstantUtils.faqs_url);
        faqsInputModel.setDeviceId(deviceId);
        faqsInputModel.setOsVersion(osVersion);
        faqsInputModel.setAuthToken(authToken);
        new FaqsService(this, faqsInputModel);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            if(responseModel!=null && responseModel.getResultObj()!=null) {
                FaqsResponseModel faqsResponseModel = (FaqsResponseModel) responseModel.getResultObj();
                if (faqsResponseModel != null && faqsResponseModel.getSrcUrlEn() != null) {
                    if (currentLanguage.equals("en")) {
                        loadPaymnetURL(faqsResponseModel.getSrcUrlEn());
                    } else {
                        loadPaymnetURL(faqsResponseModel.getSrcUrlAr());
                    }
                } else if(faqsResponseModel != null && faqsResponseModel.getErrorMsgEn()!=null){
                    if (currentLanguage.equals("en")) {
                        showErrorFragment(faqsResponseModel.getErrorMsgEn());
                    } else {
                        showErrorFragment(faqsResponseModel.getErrorMsgAr());
                    }
                }
            }

        }catch (Exception ex){
            if(ex!=null){
                ex.printStackTrace();
            }
        }
    }

    private void showErrorFragment(String error) {

        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, error);
        errorFragment.setArguments(bundle);
        replaceFragmnet(errorFragment, R.id.frameLayout, true);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        try {
            onBackPressed();
        }catch (Exception ex){
            if(ex!=null){
                ex.printStackTrace();
            }
        }
    }

    private void loadPaymnetURL(String paymnetURL) {
        onBackPressed();
        paymentWebView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
//                onBackPressed();
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Log.v("errror", "errror");
//                onBackPressed();
            }
        });
        paymentWebView.loadUrl(paymnetURL);
    }
}
