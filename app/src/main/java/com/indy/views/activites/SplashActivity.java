package com.indy.views.activites;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.controls.BaseInterface;
import com.indy.controls.ServiceUtils;
import com.indy.helpers.DeviceUuidFactory;
import com.indy.helpers.GooglePlayServiceUtils;
import com.indy.helpers.RetrofitErrorHandeler;
import com.indy.models.faqs.FaqsInputModel;
import com.indy.models.faqs.FaqsResponseModel;
import com.indy.models.login.LoginOutputModel;
import com.indy.models.onboarding_push_notifications.GetOnBoardingDetailsRequestModel;
import com.indy.models.onboarding_push_notifications.GetOnBoardingDetailsResponseModel;
import com.indy.models.profilestatus.NewProileStautsOutput;
import com.indy.models.profilestatus.ProfileStatusInput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.ocr.screens.EmailAddressFragment;
import com.indy.ocr.screens.EmiratesIdDetailsFragment;
import com.indy.services.FaqsService;
import com.indy.services.GetOnboardingDetailsService;
import com.indy.services.GetVATEnabledService;
import com.indy.services.ProfileStatusService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.utils.IndyApplication;
import com.indy.utils.SharedPrefrencesManger;
import com.indy.views.fragments.AboutUs.InviteFriendFragment;
import com.indy.views.fragments.StartScreenFragment;
import com.indy.views.fragments.buynewline.CashOnDeliveryFragment;
import com.indy.views.fragments.buynewline.DeliveryDetailsAddress;
import com.indy.views.fragments.buynewline.NameDOBFragment;
import com.indy.views.fragments.buynewline.NewNumberFragment;
import com.indy.views.fragments.buynewline.OrderFragment;
import com.indy.views.fragments.buynewline.exisitingnumber.EmiratesIDVerificationFragment;
import com.indy.views.fragments.buynewline.exisitingnumber.PromoCodeVerificationSuccessfulFragment;
import com.indy.views.fragments.gamification.events.EventsMainFragment;
import com.indy.views.fragments.rewards.RewardsFragment;
import com.indy.views.fragments.store.mainstore.MyStoreFragment;
import com.indy.views.fragments.usage.UsageFragment;

import static com.indy.utils.ConstantUtils.KEY_DL_GET_SIM;
import static com.indy.utils.ConstantUtils.TAGGING_open_the_app;


/**
 * Created by emad on 9/6/16.
 */
public class SplashActivity extends MasterActivity implements BaseInterface {
    private final long SPLASH_TIME = 1000L;
    private VideoView videoHolder;
    public String appVersion, token, osVersion, channel, deviceId, currentLanguage;
    private NewProileStautsOutput profileStatusOutput;
    private FragmentTransaction transaction;
    public static boolean isTutorialRunnung = false;
    public static String notificationType = "";
    public DeviceUuidFactory deviceUuidFactory;
    private GooglePlayServiceUtils googlePlayServiceUtils;
    private ProgressBar progressID;
    public static Intent service;
    public static LoginOutputModel loginOutputModel;
    public SplashActivity splashActivity;
    public Context mContext;
    public static FaqsResponseModel faqsResponseModel;
    boolean isResume = false;
    boolean isDeepLink = false;
    boolean isDeepLinkURI = false;
    String screenNameDL = null;
    private String screenNameDeepLinkingURI = "";
    private String itemIDDeepLinkingURI = "";
    private GetOnBoardingDetailsResponseModel getOnBoardingDetailsResponseModel;
    private String currentNotification;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
        setContentView(R.layout.intro_splash_screen);
        deviceUuidFactory = new DeviceUuidFactory(this);
        mContext = this;
        initUI();
        isTutorialRunnung = false;
        sharedPrefrencesManger.setDeviceId(deviceUuidFactory.getDeviceUuid().toString());
        progressID = (ProgressBar) findViewById(R.id.progressID);


        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), TAGGING_open_the_app);

        if(getIntent() != null && getIntent().getData() != null){

        }

        if (getIntent() != null && getIntent().getExtras() != null) {
            Log.d("getIntent().getExtras()", getIntent().getExtras().toString());
            if (getIntent().getExtras().getString(ConstantUtils.notification_type) != null && !getIntent().getExtras().getString(ConstantUtils.notification_type).isEmpty()) {
                sharedPrefrencesManger.setNotificationTypeForPopup(getIntent().getExtras().getString(ConstantUtils.notification_type));
                notificationType = getIntent().getExtras().getString(ConstantUtils.notification_type);
            }
            if (getIntent().getExtras().getString(ConstantUtils.KEY_NOTIFICATION_ID) != null && !getIntent().getExtras().getString(ConstantUtils.KEY_NOTIFICATION_ID).isEmpty()) {
                sharedPrefrencesManger.setNotificationIdForPopup(getIntent().getExtras().getString(ConstantUtils.KEY_NOTIFICATION_ID));
            }

            if (getIntent().getExtras().getString(ConstantUtils.KEY_ITEM_ID) != null && !getIntent().getExtras().getString(ConstantUtils.KEY_ITEM_ID).isEmpty()) {
                sharedPrefrencesManger.setItemId(getIntent().getExtras().getString(ConstantUtils.KEY_ITEM_ID));

            }
        }

        if (getIntent() != null && getIntent().getAction() != null && getIntent().getExtras() != null) {
            if (getIntent().getAction().equalsIgnoreCase(Intent.ACTION_VIEW)) {
                if (getIntent().getData() != null) {
                    isDeepLink = true;
                    if (getIntent().getData().toString().split("swyp://").length > 1) {
                        String[] queryParam = getIntent().getData().toString().split("swyp://");
                        screenNameDL = queryParam[1];
                        isDeepLink = true;
                        Log.d("isDeepLink", getIntent().getData().toString());
                        Log.d("screenNameDL", screenNameDL);
                    } else if (getIntent().getData().toString().split("swypapp/").length > 1) {
                        isDeepLinkURI = true;
                        isDeepLink = false;
                        String[] queryParam = getIntent().getData().toString().split("swypapp/");
                        if (queryParam[1].contains("/")) {
                            String[] deepLinkParams = queryParam[1].split("/");
                            if (deepLinkParams.length > 1) {
                                screenNameDeepLinkingURI = deepLinkParams[0].toLowerCase();
                                itemIDDeepLinkingURI = deepLinkParams[1].toLowerCase();
                            }
                        } else {
                            screenNameDeepLinkingURI = queryParam[1].toLowerCase();
                            itemIDDeepLinkingURI = "";
                        }
                    } else if (getIntent().getData().toString().split("screen_name=").length > 1) {
                        isDeepLinkURI = true;
                        isDeepLink = false;
                        String[] queryParam = getIntent().getData().toString().split("screen_name=");
                        if (queryParam[1].contains("id")) {
                            String[] deepLinkParams = queryParam[1].split("&id=");
                            if (deepLinkParams.length > 0) {
                                screenNameDeepLinkingURI = deepLinkParams[0].toLowerCase();
                                itemIDDeepLinkingURI = deepLinkParams[1].toLowerCase();
                            }
                        } else {
                            screenNameDeepLinkingURI = queryParam[1].toLowerCase();
                            itemIDDeepLinkingURI = "";
                        }
                    }
                }
            }
        }
    }



    public void onSucessListener(BaseResponseModel responseModel) {
        try {
            progressID.setVisibility(View.GONE);

            if (responseModel != null && responseModel.getResultObj() != null) {
                if (responseModel.getServiceType() != null && responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.profileStatusServiceType)) {
                    profileStatusOutput = (NewProileStautsOutput) responseModel.getResultObj();
                    if (profileStatusOutput != null) {
                        if (profileStatusOutput.getErrorCode() != null)
                            onProfileStatusErrror(responseModel);
                        else {

                            setAppFlagsValue();
                            if (profileStatusOutput.getUserProfile() == null) {
                                onProfileStatusSucess();// user didnot has authtoken.
                            } else {// user has authtoken and it will be login automaticlly.
                                onLoginSucess();
                            }
                        }
                    } else {
//            Toast.makeText(getApplicationContext(), getString(R.string.generice_error), Toast.LENGTH_SHORT).show();
                        onProfileStatusErrror(null);
                    }
                }
                else if (responseModel.getServiceType() != null && responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.faqsService)) {
                    try {
                        faqsResponseModel = (FaqsResponseModel) responseModel.getResultObj();

                        if (faqsResponseModel != null && faqsResponseModel.getAdditionalInfoList() != null && faqsResponseModel.getAdditionalInfoList().size() > 0) {
                            if (sharedPrefrencesManger.getLatestStoreVersion() < CommonMethods.storeLatestVersion(faqsResponseModel.getAdditionalInfoList().get(0).getValue())) {
                                sharedPrefrencesManger.setIsAppRated(false);
                                sharedPrefrencesManger.setLatestStoreVersion(CommonMethods.storeLatestVersion(faqsResponseModel.getAdditionalInfoList().get(0).getValue()));
                            }
                            if (faqsResponseModel.getAdditionalInfoList().get(0).getName().equalsIgnoreCase(ConstantUtils.app_rating_versions)) {
                                String versions = faqsResponseModel.getAdditionalInfoList().get(0).getValue();
                                int packageCode = 0;
                                try {
                                    PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
                                    packageCode = pInfo.versionCode;
                                } catch (PackageManager.NameNotFoundException e) {
                                    e.printStackTrace();
                                }
                                if (versions.contains(String.valueOf(packageCode))) {
                                    sharedPrefrencesManger.setIsAppRatingAllowed(true);
                                } else {
                                    sharedPrefrencesManger.setIsAppRatingAllowed(false);
                                }
                            }
                            if (faqsResponseModel.getAdditionalInfoList().get(1) != null && faqsResponseModel.getAdditionalInfoList().get(1).getName().equalsIgnoreCase(ConstantUtils.gamification_enabled)) {
                                String result = faqsResponseModel.getAdditionalInfoList().get(1).getValue();
                                if (result.equalsIgnoreCase("true")) {
                                    sharedPrefrencesManger.setGamificationEnabled(true);
                                } else {
                                    sharedPrefrencesManger.setGamificationEnabled(false);
                                }
                            }
                        }

                        if (faqsResponseModel != null && faqsResponseModel.getAppUpdateRequired() != null && faqsResponseModel.getAppUpdateRequired().length() > 0) {
                            if (faqsResponseModel.getAppUpdateRequired().equalsIgnoreCase(ConstantUtils.soft)
                                    || faqsResponseModel.getAppUpdateRequired().equalsIgnoreCase(ConstantUtils.hard)) {
                                Intent intent = new Intent(this, UpdateApplicationActivity.class);
                                intent.putExtra(ConstantUtils.updateType, faqsResponseModel.getAppUpdateRequired());

                                startActivityForResult(intent, 1002);
                            } else {
                                displayNextScreen();
                            }
                        } else {
                            displayNextScreen();
                        }
                    } catch (Exception ex) {
                        CommonMethods.logException(ex);
                        displayNextScreen();
                    }
                } else if (responseModel.getServiceType() != null && responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.isVATEnabled)) {
                    FaqsResponseModel faqsResponseModel = (FaqsResponseModel) responseModel.getResultObj();
                    if (faqsResponseModel != null) {
                        if (faqsResponseModel.getMembershipPrice() != null && faqsResponseModel.getMembershipPrice().length() > 0) {
                            sharedPrefrencesManger.setMembershipPrice(faqsResponseModel.getMembershipPrice());
                        } else {
                            sharedPrefrencesManger.setMembershipPrice("50");
                        }
                        if (faqsResponseModel.getNewMembershipPrice() != null && faqsResponseModel.getNewMembershipPrice().length() > 0) {
                            sharedPrefrencesManger.setNewMembershipPrice(faqsResponseModel.getNewMembershipPrice());
                        } else {
                            sharedPrefrencesManger.setNewMembershipPrice("50");
                        }
                        if (faqsResponseModel.getSimPrice() != null && faqsResponseModel.getSimPrice().length() > 0) {
                            sharedPrefrencesManger.setSIMPrice(faqsResponseModel.getSimPrice());
                        } else {
                            sharedPrefrencesManger.setSIMPrice("0");
                        }

                        if (faqsResponseModel.getLoggedInMembershipPrice() != null && faqsResponseModel.getLoggedInMembershipPrice().length() > 0) {
                            sharedPrefrencesManger.setAfterLoginPrice(faqsResponseModel.getLoggedInMembershipPrice());
                        } else {
                            sharedPrefrencesManger.setAfterLoginPrice("50");
                        }

                        if (faqsResponseModel.isDisplayVAT()) {
                            sharedPrefrencesManger.setVATEnabled(true);
                            CommonMethods.isVatEnabled = true;
                        } else {
                            sharedPrefrencesManger.setVATEnabled(false);
                            CommonMethods.isVatEnabled = false;
                        }
                    }
                }
            }
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case 1002:

                isResume = true;

                break;

            default:
                break;
        }
    }

    private void setAppFlagsValue() {
        if (sharedPrefrencesManger != null) {
            if (profileStatusOutput != null) {
                if (profileStatusOutput.getAppFlags() != null) {
                    sharedPrefrencesManger.enableBalanceTransfer(profileStatusOutput.getAppFlags().getUbtEnabled());
                    sharedPrefrencesManger.enableRewards(profileStatusOutput.getAppFlags().getMarketplaceEnabled());
                    sharedPrefrencesManger.enableStore(profileStatusOutput.getAppFlags().getStoreEnabled());
                    sharedPrefrencesManger.enableLiveChat(profileStatusOutput.getAppFlags().getLiveChatEnabled());
                    sharedPrefrencesManger.enableLUsage(profileStatusOutput.getAppFlags().getUsageEnabled());
                }
            }
        }
    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse responseModel) {
        Intent intent = new Intent(SplashActivity
                .this, RegisterationActivity.class);
        intent.putExtra("fragIndex", "7");
        startActivity(intent);
        SplashActivity.this.finish();
    }

    private void onProfileStatusErrror(BaseResponseModel responseModel) {
        if (profileStatusOutput != null /*&& profileStatusOutput.getErrorCode() != null
                && profileStatusOutput.getErrorCode().equals("0000124")*/) {
            if (profileStatusOutput.getErrorCode() != null) {
                Intent intent = new Intent(SplashActivity
                        .this, RegisterationActivity.class);
                if (notificationType!= null && getNotificationTypeForOnboarding(notificationType)) {
                    intent.putExtra(ConstantUtils.notification_type, notificationType);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                } else {
                    intent.putExtra("fragIndex", "-1");
                }
                startActivity(intent);
                SplashActivity.this.finish();
            } else {
                Throwable t = (Throwable) responseModel.getResultObj();
                RetrofitErrorHandeler retrofitErrorHandeler = new RetrofitErrorHandeler();
                retrofitErrorHandeler.handleError(t, getApplicationContext());
//           Intent intent = new Intent(SplashActivity
//                .this, RegisterationActivity.class);
//                intent.putExtra("fragIndex", "-1");
//                startActivity(intent);
//                SplashActivity.this.finish();

            }

        } else {
            if (responseModel != null) {
                Throwable t = (Throwable) responseModel.getResultObj();
                RetrofitErrorHandeler retrofitErrorHandeler = new RetrofitErrorHandeler();
                retrofitErrorHandeler.handleError(t, getApplicationContext());
//            Intent intent = new Intent(SplashActivity
//                    .this, RegisterationActivity.class);
//            intent.putExtra("fragIndex", "-1");
//            startActivity(intent);
//            SplashActivity.this.finish();
            } else {
                Intent intent = new Intent(SplashActivity
                        .this, RegisterationActivity.class);
                intent.putExtra("fragIndex", "-1");
                startActivity(intent);
                SplashActivity.this.finish();
            }

        }
    }

    private void onProfileStatusSucess() {
        Intent intent = new Intent(SplashActivity
                .this, RegisterationActivity.class);
        if (profileStatusOutput.getProfileStatus() != null) {
            if (isDeepLink) {
                if (screenNameDL.equalsIgnoreCase(KEY_DL_GET_SIM)) {
                    intent.putExtra("fragIndex", "-1");
                }
            } else if (sharedPrefrencesManger.getNotificationTypeForPopup() != null && getNotificationTypeForOnboarding(sharedPrefrencesManger.getNotificationTypeForPopup())) {
                intent.putExtra(ConstantUtils.notification_type, notificationType);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            } else {
                intent.putExtra("fragIndex", profileStatusOutput.getProfileStatus().toString());
            }
        }
        startActivity(intent);
        SplashActivity.this.finish();
    }

    private boolean getNotificationTypeForOnboarding(String type) {
        switch (type) {

            case "14":
                return true;
            case "15":
                return true;
            case "16":
                return true;
            case "17":
                return true;
            case "18":
                return true;

            default:
                return false;

        }
    }

    public static void stopMyService() {
//        if (splashActivity != null) {
//            splashActivity.stopService(service);
//        }
    }

    public static void startMyService() {
//        service = new Intent(mContext, LastUpdated.class);
//        mContext.startService(service);
    }

    private void onLoginSucess() {
        if (service == null) {
            startMyService();
        }
//        sharedPrefrencesManger.setPassword(password.getText().toString().trim());
        sharedPrefrencesManger.setRegisterationID(profileStatusOutput.getUserProfile().getRegistrationId());
        sharedPrefrencesManger.setEmail(profileStatusOutput.getUserProfile().getEmail());
        sharedPrefrencesManger.setNickName(profileStatusOutput.getUserProfile().getNickName());
        sharedPrefrencesManger.setGender(profileStatusOutput.getUserProfile().getGender());
        sharedPrefrencesManger.setUserProfileImage(profileStatusOutput.getUserProfile().getProfilePicUrl());
        sharedPrefrencesManger.setFullName(profileStatusOutput.getUserProfile().getFullName());
        sharedPrefrencesManger.setMobileNo(profileStatusOutput.getUserProfile().getMsisdn());
        sharedPrefrencesManger.setUserProfileObj(profileStatusOutput.getUserProfile());
        sharedPrefrencesManger.setInvitation(profileStatusOutput.getUserProfile().getInvitationCode());
//        sharedPrefrencesManger.setAuthToken(mBaseResponseModel.getAuthToken());
        Intent intent = new Intent(this, SwpeMainActivity.class);
        intent.putExtra(ConstantUtils.userProfile, profileStatusOutput.getUserProfile());
        if (isDeepLinkURI) {
            intent.putExtra(ConstantUtils.KEY_IS_DEEP_LINK_URI, true);
            intent.putExtra(ConstantUtils.KEY_DEEP_LINK_SCREEN_NAME, screenNameDeepLinkingURI);
            intent.putExtra(ConstantUtils.KEY_DEEP_LINK_URI_ITEM_ID, itemIDDeepLinkingURI);
        }
        startActivity(intent);
        sharedPrefrencesManger.setLastUpdated("");
        if (TutorialActivity.tutorialActivityInstance != null)
            TutorialActivity.tutorialActivityInstance.finish();
        this.finish();
        isDeepLinkURI = false;
        screenNameDeepLinkingURI = "";
        itemIDDeepLinkingURI = "";
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        progressID.setVisibility(View.GONE);

        if (responseModel != null && responseModel.getServiceType() != null) {
            if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.faqsService)) {
                displayNextScreen();
            } else if (responseModel.getServiceType() != null && responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.isVATEnabled)) {
                CommonMethods.isVatEnabled = true;
                sharedPrefrencesManger.setVATEnabled(true);
            } else {
                onProfileStatusErrror(responseModel);
            }
        } else {
            onProfileStatusErrror(responseModel);
        }

    }

    @Override
    public void initUI() {
        super.initUI();
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        videoHolder = (VideoView) findViewById(R.id.myvideoview);

//        sharedPrefrencesManger = new SharedPrefrencesManger(this);
        currentLanguage = sharedPrefrencesManger.getLanguage();
        googlePlayServiceUtils = new GooglePlayServiceUtils(this);
        googlePlayServiceUtils.registerReceiver();
        googlePlayServiceUtils.startIntentService();
        splashActivity = this;
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                URL url = null;
//                try {
//                    url = new URL("https://uat-swypapp.etisalat.ae/Indy/EPGServlet?orderId=MTI2MzU3NTU5&paymentType=MQ==");
//
//                    URLConnection conn = null;
//                    try {
//                        conn = url.openConnection();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//
//                    Map<String, List<String>> headerFields = conn.getHeaderFields();
//
//                    Set<String> headerFieldsSet = headerFields.keySet();
//                    Iterator<String> hearerFieldsIter = headerFieldsSet.iterator();
//
//                    while (hearerFieldsIter.hasNext()) {
//
//                        String headerFieldKey = hearerFieldsIter.next();
//                        List<String> headerFieldValue = headerFields.get(headerFieldKey);
//
//                        StringBuilder sb = new StringBuilder();
//                        for (String value : headerFieldValue) {
//                            sb.append(value);
//                            sb.append("");
//                        }
//
//                        System.out.println(headerFieldKey + "=" + sb.toString());
//
//                    }
//                } catch (MalformedURLException e) {
//                    e.printStackTrace();
//                }
//            }
//        }).start();

        appVersion = sharedPrefrencesManger.getAppVersion();
        token = sharedPrefrencesManger.getToken();
        if (token == null)
            token = "DEVICETOKEN";
        osVersion = ConstantUtils.osVersion;
        channel = ConstantUtils.channel;
        deviceId = token;
    }

    @Override
    public void onConsumeService() {
        try {
            FaqsInputModel faqsInputModel = new FaqsInputModel();
            faqsInputModel.setAppVersion(IndyApplication.getAppVersionCode(SplashActivity.this));
            faqsInputModel.setDeviceId(deviceId);
            faqsInputModel.setToken(token);
            faqsInputModel.setOsVersion(ConstantUtils.osVersion);
            faqsInputModel.setChannel(ConstantUtils.channel);
            if (sharedPrefrencesManger.getLanguage() != null && sharedPrefrencesManger.getLanguage().length() > 0) {
                faqsInputModel.setLang(sharedPrefrencesManger.getLanguage());
            } else {
                faqsInputModel.setLang("en");
            }
//        faqsInputModel.setAppVersion("1.9");
            //String.valueOf(Double.valueOf(CommonMethods.getApplicationVersionCode(this))));
//        faqsInputModel.setSignificant("REG_FAQ");
            faqsInputModel.setSignificant(ConstantUtils.SIGNIFICANT_APP_UPDATE);
            faqsInputModel.setLang(currentLanguage);
            new FaqsService(this, faqsInputModel);

            faqsInputModel.setSignificant(ConstantUtils.vat_significant_v2);
            new GetVATEnabledService(this, faqsInputModel);
        } catch (Exception ex) {
            CommonMethods.logException(ex);

            displayNextScreen();
        }
    }

    private void playIntroVideo() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        videoHolder.setLayoutParams(new RelativeLayout.LayoutParams(width, height));

        try {
            Uri video;
            if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP && android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                video = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.logo);

            } else {
                video = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.logo);
            }

            Log.v("video path : ", video + " ");
            videoHolder.setVideoURI(video);
            videoHolder.setFitsSystemWindows(true);
            videoHolder.setActivated(true);
            videoHolder.setZOrderOnTop(true);
            videoHolder.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
//                    mp.setVolume(0, 0);
                    if (isResume) {
                        displayNextScreen();
                    } else {
//                        if (dataDiffForLIVPopup()) {
//                            sharedPrefrencesManger.setLIVPopupDateTime(System.currentTimeMillis());
//                            startActivityForResult(new Intent(SplashActivity.this, LivPopupActivity.class), 020);
//                        } else {
//                            onConsumeService();
//                        }
                        onConsumeService();
                    }

//                    finish();
                }

            });


            videoHolder.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
//                    mp.setLooping(true);
                    videoHolder.setBackgroundColor(getResources().getColor(R.color.transparent));
                    videoHolder.start();
//                    mp.setVolume(0, 0);

                }
            });
        } catch (Exception ex) {
            // jump();
            CommonMethods.logException(ex);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        playIntroVideo();
        googlePlayServiceUtils.registerReceiver();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (videoHolder != null)
            videoHolder.stopPlayback();

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (videoHolder != null)
            videoHolder.stopPlayback();
    }

    private void displayNextScreen() {
//        Log.v("oooops", sharedPrefrencesManger.isFiristTimeLaunch() + " ");
        new Handler().postDelayed((Runnable) new Runnable() {
            @Override
            public void run() {

                if (sharedPrefrencesManger.isFiristTimeLaunch()) {// it is the first time for launch
                    isTutorialRunnung = true;
                    startActivity(new Intent(SplashActivity
                            .this, TutorialActivity.class));
                    SplashActivity.this.finish();
                } else {
                    if (sharedPrefrencesManger.getRegisterationID() == null
                            || sharedPrefrencesManger.getRegisterationID().isEmpty()) {
                        Intent intent = new Intent(SplashActivity
                                .this, RegisterationActivity.class);
                        intent.putExtra("fragIndex", "-1");
                        startActivity(intent);
                        SplashActivity.this.finish();
                    } else {
                        progressID.setVisibility(View.VISIBLE);

                        getProfileStatus();
                    }
                }
            }
        }, SPLASH_TIME);
    }

    private void getProfileStatus() {// 4

        ProfileStatusInput profileStatusInput = new ProfileStatusInput();
        profileStatusInput.setAppVersion(appVersion);
        profileStatusInput.setToken(token);
        profileStatusInput.setOsVersion(osVersion);
        profileStatusInput.setChannel(channel);
        profileStatusInput.setDeviceId(deviceId);
        profileStatusInput.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        profileStatusInput.setChannel(channel);
        profileStatusInput.setAuthToken(sharedPrefrencesManger.getAuthToken());
        new ProfileStatusService(this, profileStatusInput);
    }
    //0509981533
    //784199884370371
    //update registeratin ID

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent != null && intent.getExtras() != null) {

            if (getIntent().getExtras().getString(ConstantUtils.notification_type) != null && !getIntent().getExtras().getString(ConstantUtils.notification_type).isEmpty()) {
                sharedPrefrencesManger.setNotificationTypeForPopup(getIntent().getExtras().getString(ConstantUtils.notification_type));
                notificationType = getIntent().getExtras().getString(ConstantUtils.notification_type);
            }
            if (getIntent().getExtras().getString(ConstantUtils.KEY_NOTIFICATION_ID) != null && !getIntent().getExtras().getString(ConstantUtils.KEY_NOTIFICATION_ID).isEmpty()) {
                sharedPrefrencesManger.setNotificationIdForPopup(getIntent().getExtras().getString(ConstantUtils.KEY_NOTIFICATION_ID));
            }

            if (getIntent().getExtras().getString(ConstantUtils.KEY_ITEM_ID) != null && !getIntent().getExtras().getString(ConstantUtils.KEY_ITEM_ID).isEmpty()) {
                sharedPrefrencesManger.setItemId(getIntent().getExtras().getString(ConstantUtils.KEY_ITEM_ID));

            }
            onConsumeService();
        }


        if (getIntent() != null && getIntent().getAction() != null && getIntent().getExtras() != null) {
            if (getIntent().getAction().equalsIgnoreCase(Intent.ACTION_VIEW)) {
                if (getIntent().getData() != null) {
                    isDeepLink = true;
//                    String[] queryParam = getIntent().getData().toString().split("swyp://");
//                    screenNameDL = queryParam[1];
                    Log.d("isDeepLink", getIntent().getData().toString());
                    Toast.makeText(this, getIntent().getData().toString(), Toast.LENGTH_SHORT).show();
//                    Log.d("screenNameDL", screenNameDL);
                }
            }
        }
    }

}