package com.indy.views.activites;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.indy.R;
import com.indy.utils.ConstantUtils;

public class UpdateApplicationActivity extends MasterActivity {


    String updateType = "none";
    boolean canGoBack = true;
    TextView tv_title, tv_text;
    Button updateButton, cancelButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_application);
        initUI();
    }

    @Override
    public void initUI() {
        super.initUI();


        tv_text = (TextView) findViewById(R.id.errorTxt);
        tv_title = (TextView) findViewById(R.id.titleTxtView);
        updateButton = (Button) findViewById(R.id.okBtn);
        cancelButton = (Button) findViewById(R.id.cancelBtn);
        if (SplashActivity.faqsResponseModel != null) {
            if (currentLanguage != null && currentLanguage.equalsIgnoreCase(ConstantUtils.lang_english)) {
                if (SplashActivity.faqsResponseModel.getAppUpdateErrorMessageEn() != null) {
                    tv_text.setText(SplashActivity.faqsResponseModel.getAppUpdateErrorMessageEn());
                }
            } else if (currentLanguage != null && currentLanguage.equalsIgnoreCase(ConstantUtils.lang_arabic)) {
                if (SplashActivity.faqsResponseModel.getAppUpdateErrorMessageAr() != null) {
                    tv_text.setText(SplashActivity.faqsResponseModel.getAppUpdateErrorMessageAr());
                }
            } else {
                if (SplashActivity.faqsResponseModel.getAppUpdateErrorMessageEn() != null) {
                    tv_text.setText(SplashActivity.faqsResponseModel.getAppUpdateErrorMessageEn());
                }
            }
        }
        tv_title.setText(getString(R.string.notice));
        if (getIntent() != null && getIntent().getExtras() != null) {
            updateType = getIntent().getExtras().getString(ConstantUtils.updateType);
            if (updateType != null && updateType.equalsIgnoreCase(ConstantUtils.hard)) {
                canGoBack = false;
                cancelButton.setVisibility(View.INVISIBLE);
            } else if (updateType != null && updateType.equalsIgnoreCase(ConstantUtils.soft)) {
                canGoBack = true;
                cancelButton.setVisibility(View.VISIBLE);
                cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });
            } else {
                setResult(100);
                finish();
            }
        }

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPrefrencesManger.setIsAppRated(false);
                openWebPage(SplashActivity.faqsResponseModel.getAppUpdateDownloadUrl());
            }
        });


    }


    @Override
    public void onBackPressed() {
        if (canGoBack) {
            setResult(100);
            finish();
        }

    }


    public void openWebPage(String url) {
        try {
            if (!url.toLowerCase().startsWith("http") && !url.toLowerCase().startsWith("https")) {
                url = "http:\\" + url;
            }
            Uri webPage = Uri.parse(url);
            Intent intent = new Intent(Intent.ACTION_VIEW, webPage);
            startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            ex.printStackTrace();
        }
    }

}

