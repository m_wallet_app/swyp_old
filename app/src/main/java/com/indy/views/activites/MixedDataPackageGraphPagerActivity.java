package com.indy.views.activites;

import android.Manifest;
import android.animation.Animator;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.view.Display;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.indy.R;
import com.indy.adapters.GraphPagerAdapter;
import com.indy.controls.ServiceUtils;
import com.indy.customviews.CustomTabLayout;
import com.indy.models.balance.BalanceInputModel;
import com.indy.models.balance.BalanceResponseModel;
import com.indy.models.packages.OfferList;
import com.indy.models.packages.UsagePackageList;
import com.indy.models.packageusage.OfferInfo;
import com.indy.models.packageusage.PackageInfo;
import com.indy.models.packageusage.PackageUsageInput;
import com.indy.models.packageusage.PackageUsageOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.services.PackageUsageService;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.utils.ErrorFragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MixedDataPackageGraphPagerActivity extends MasterActivity {


    ViewPager viewPager;
    private TextView titleTxt, headerTxt;
    TextView tv_dataAllowanceRemaining, tv_expires, tv_extraDataToBeHidden;
    private Button backImg, helpImg;
    public LinearLayout ll_topLayout, ll_errorLayout, ll_loadingLayout;
    FrameLayout frameLayout;
    BalanceInputModel balanceInputModel;
    BalanceResponseModel balanceResponseModel;
    public final static int request_permission_for_contacts = 10;
    private PackageUsageOutput packageUsageOutput;
    private PackageUsageInput packageUsageInput;
    public static UsagePackageList userPackgeListItem;
    ArrayList<OfferList> offerList;
    TextView bt_retry;
    ScrollView relativeLayout;
//    RelativeLayout relativeLayout;

    private void circularRevealActivity() {

        int cx = relativeLayout.getWidth() / 2;
        int cy = relativeLayout.getHeight() / 2;
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        float finalRadius = Math.max(width + 200, height + 200);

        // create the animator for this view (the start radius is zero)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Animator circularReveal = ViewAnimationUtils.createCircularReveal(relativeLayout, 0, height, 0, finalRadius);
            circularReveal.setDuration(1000);

            // make the view visible and start the animation
            relativeLayout.setVisibility(View.VISIBLE);
            circularReveal.start();
        }else{
            relativeLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        overridePendingTransition(R.anim.do_not_move, R.anim.do_not_move);

        setContentView(R.layout.activity_mixed_data_package_graph_pager);

        initUI();
        onConsumeService();
        if (savedInstanceState == null) {

        }


    }


    @Override
    public void initUI() {
        super.initUI();
        try {
            titleTxt = (TextView) findViewById(R.id.tv_title_findus);
            headerTxt = (TextView) findViewById(R.id.titleTxt);
            headerTxt.setText(getString(R.string.data));
            backImg = (Button) findViewById(R.id.backImg);
            backImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });

            helpImg = (Button) findViewById(R.id.helpBtn);
            helpImg.setVisibility(View.INVISIBLE);
            helpImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    startActivity(new Intent(MixedDataPackageGraphPagerActivity.this, HelpActivity.class));
                }
            });
            frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
            frameLayout.setVisibility(View.GONE);
            ll_topLayout = (LinearLayout) findViewById(R.id.ll_top_layout);
//        initTabLayout();

//            relativeLayout= (RelativeLayout) findViewById(R.id.rl_root);
            relativeLayout = (ScrollView) findViewById(R.id.sv_mixData);


            userPackgeListItem = getIntent().getParcelableExtra(ServiceUtils.packageListType);
            offerList = getIntent().getParcelableArrayListExtra("OfferInfoList");
            userPackgeListItem.setOfferList(offerList);
            tv_dataAllowanceRemaining = (TextView) findViewById(R.id.tv_user_balance_amount);
            tv_expires = (TextView) findViewById(R.id.tv_balance_expire);
            if (currentLanguage.equals("en")) {
                headerTxt.setText(userPackgeListItem.getNameEn().toUpperCase());
            } else {
                headerTxt.setText(userPackgeListItem.getNameAr().toUpperCase());
            }
            ll_errorLayout = (LinearLayout) findViewById(R.id.ll_dummy_error_container);
            ll_loadingLayout = (LinearLayout) findViewById(R.id.ll_progressBar);
            ll_errorLayout.setVisibility(View.GONE);
            bt_retry = (TextView) findViewById(R.id.retryBtn);
            bt_retry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    onConsumeService();
                }
            });

            setupData();


//            int x = relativeLayout.getLeft();
//            int y = relativeLayout.getBottom();
//
//            int startRadius = x;
//            int endRadius = (int) Math.hypot(relativeLayout.getWidth(), relativeLayout.getHeight());
//
////            Animator anim = ViewAnimationUtils.createCircularReveal(relativeLayout, 0, 0, x, y);
//            Animator anim = prepareUnrevealAnimator(x,y);
//;            anim.start();


//            onConsumeService();
        } catch (Exception ex) {
            showErrorFragment(getString(R.string.generice_error));
        }

    }

    public void setupData() {
        tv_expires.setText(" " + parseDate(userPackgeListItem.getOfferList().get(0).getInOfferEndDate()));
        String remainingAllowance = "";
        if (currentLanguage.equals("en")) {
            remainingAllowance = getString(R.string.your_remaining_data_allowance_is) + " " + userPackgeListItem.getOfferList().get(0).getPackageConsumption().getRemaining() + userPackgeListItem.getOfferList().get(0).getPackageConsumption().getConsumptionUnitEn();
        } else {
            remainingAllowance = getString(R.string.your_remaining_data_allowance_is) + " " + userPackgeListItem.getOfferList().get(0).getPackageConsumption().getRemaining() + userPackgeListItem.getOfferList().get(0).getPackageConsumption().getConsumptionUnitAr();
        }
        tv_dataAllowanceRemaining.setText(remainingAllowance);
    }

    /**
     * Get the animator to unreveal the circle
     *
     * @param cx center x of the circle (or where the view was touched)
     * @param cy center y of the circle (or where the view was touched)
     * @return Animator object that will be used for the animation
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public Animator prepareUnrevealAnimator(float cx, float cy) {
        int radius = getEnclosingCircleRadius(viewPager, (int) cx, (int) cy);
        Animator anim = ViewAnimationUtils.createCircularReveal(viewPager, (int) cx, (int) cy, radius, 0);
        anim.setInterpolator(new AccelerateInterpolator(2f));
        anim.setDuration(1000);
        return anim;
    }


    /**
     * To be really accurate we have to start the circle on the furthest corner of the view
     *
     * @param v  the view to unreveal
     * @param cx center x of the circle
     * @param cy center y of the circle
     * @return the maximum radius
     */
    private int getEnclosingCircleRadius(View v, int cx, int cy) {
        int realCenterX = cx + v.getLeft();
        int realCenterY = cy + v.getTop();
        int distanceTopLeft = (int) Math.hypot(realCenterX - v.getLeft(), realCenterY - v.getTop());
        int distanceTopRight = (int) Math.hypot(v.getRight() - realCenterX, realCenterY - v.getTop());
        int distanceBottomLeft = (int) Math.hypot(realCenterX - v.getLeft(), v.getBottom() - realCenterY);
        int distanceBottomRight = (int) Math.hypot(v.getRight() - realCenterX, v.getBottom() - realCenterY);

        Integer[] distances = new Integer[]{distanceTopLeft, distanceTopRight, distanceBottomLeft,
                distanceBottomRight};

        return Collections.max(Arrays.asList(distances));
    }


    private void initTabLayout() {
        /*
        the fragments will be displayed according to offer type.
         */
        viewPager = (ViewPager) findViewById(R.id.pager);
        final CustomTabLayout tabLayout = (CustomTabLayout) findViewById(R.id.tab_layout);

        tabLayout.setSharedPrefrencesManger(sharedPrefrencesManger);
        tabLayout.removeAllTabs();
        tabLayout.addTab(tabLayout.newTab().setText((getString(R.string.data))));
//        tabLayout.addTab(tabLayout.newTab().setText((getString(R.string.social_data))));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);

        if (viewPager != null) {
            String newStartDate = parseDateWithDateSubtraction(userPackgeListItem.getEndDate());
            if (userPackgeListItem.getOfferList() != null && userPackgeListItem.getOfferList().size() > 0) {
                userPackgeListItem.getOfferList().get(0).setInOfferStartDate(newStartDate);
            }
            final GraphPagerAdapter adapter = new GraphPagerAdapter
                    (getSupportFragmentManager(), 1, packageUsageOutput, offerList, userPackgeListItem);
            viewPager.setAdapter(adapter);
            viewPager.setCurrentItem(0, true);
            viewPager.setSelected(true);
            viewPager.setFocusable(true);
            tabLayout.getTabAt(0).select();
            tabLayout.setTabMode(TabLayout.MODE_FIXED);
            tabLayout.setTabMode(TabLayout.MODE_FIXED);
//        titleTxt.setText(getString(R.string.locate_our_shop_now));
            tabLayout.setFillViewport(true);
            viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
            tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    viewPager.setCurrentItem(tab.getPosition());
                    String remainingAllowance = "";
                    if (currentLanguage.equals("en")) {
                        if (tab.getPosition() == 0) {
                            tv_expires.setText(" " + parseDate(userPackgeListItem.getOfferList().get(0).getInOfferEndDate()));

                            remainingAllowance = getString(R.string.your_remaining_data_allowance_is) + " " + userPackgeListItem.getOfferList().get(0).getPackageConsumption().getRemaining() + userPackgeListItem.getOfferList().get(0).getPackageConsumption().getConsumptionUnitEn();
                        } else {
                            tv_expires.setText(" " + parseDate(userPackgeListItem.getOfferList().get(1).getInOfferEndDate()));

                            remainingAllowance = getString(R.string.your_remaining_data_allowance_is) + " " + userPackgeListItem.getOfferList().get(1).getPackageConsumption().getRemaining() + userPackgeListItem.getOfferList().get(1).getPackageConsumption().getConsumptionUnitEn();

                        }
                    } else {
                        if (tab.getPosition() == 1) {
                            tv_expires.setText(" " + parseDate(userPackgeListItem.getOfferList().get(1).getInOfferEndDate()));

                            remainingAllowance = getString(R.string.your_remaining_data_allowance_is) + " " + userPackgeListItem.getOfferList().get(1).getPackageConsumption().getRemaining() + userPackgeListItem.getOfferList().get(0).getPackageConsumption().getConsumptionUnitAr();
                        } else {
                            tv_expires.setText(" " + parseDate(userPackgeListItem.getOfferList().get(0).getInOfferEndDate()));

                            remainingAllowance = getString(R.string.your_remaining_data_allowance_is) + " " + userPackgeListItem.getOfferList().get(0).getPackageConsumption().getRemaining() + userPackgeListItem.getOfferList().get(1).getPackageConsumption().getConsumptionUnitAr();

                        }
                    }
                    tv_dataAllowanceRemaining.setText(remainingAllowance);
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {
                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {
                }
            });
        }

//        sv_root.setVisibility(View.INVISIBLE);

        //CODE FOR REVEAL ANIMATION
        relativeLayout.setVisibility(View.INVISIBLE);
        ViewTreeObserver viewTreeObserver = relativeLayout.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    circularRevealActivity();
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        relativeLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    } else {
                        relativeLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                }
            });
        }

    }

    private void showErrorFragment(String error) {
        frameLayout.setVisibility(View.VISIBLE);
//        frameLayout.bringToFront();
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, error);
        errorFragment.setArguments(bundle);
        addFragmnet(errorFragment, R.id.frameLayout, true);
    }

    public void hideTopLayout() {
        ll_topLayout.setVisibility(View.GONE);
        frameLayout.setVisibility(View.GONE);
    }

    public void showTopLayout() {
        ll_topLayout.setVisibility(View.VISIBLE);
        frameLayout.setVisibility(View.VISIBLE);
    }

    public void showFrame() {
        frameLayout.setVisibility(View.VISIBLE);
    }

    public void hideFrame() {
        frameLayout.setVisibility(View.GONE);
    }


    public void checkPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.READ_CONTACTS}, request_permission_for_contacts);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == request_permission_for_contacts) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                // permission was granted, yay! Do the
                // contacts-related task you need to do.

            } else {

                // permission denied, boo! Disable the
                // functionality that depends on this permission.
            }
        }

    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        ll_loadingLayout.setVisibility(View.VISIBLE);
        packageUsageInput = new PackageUsageInput();
        packageUsageInput.setAppVersion(appVersion);
        packageUsageInput.setToken(token);
        packageUsageInput.setOsVersion(osVersion);
        packageUsageInput.setChannel(channel);
        packageUsageInput.setDeviceId(deviceId);
        packageUsageInput.setMsisdn(sharedPrefrencesManger.getMobileNo());
        packageUsageInput.setAuthToken(authToken);
        PackageInfo packageInfo = new PackageInfo();
        packageInfo.setPackageId(userPackgeListItem.getPackageType() + "");
        List<OfferList> offerInfoList = new ArrayList<OfferList>();
        offerInfoList.addAll(offerList);
        List<OfferInfo> offerInfo = new ArrayList<OfferInfo>();


        for (int i = 0; i < offerInfoList.size(); i++) {
            OfferInfo offerInfo1 = new OfferInfo();
            offerInfo1.setStartDate(parseDateFormat(userPackgeListItem.getOfferList().get(i).getInOfferStartDate()));
            offerInfo1.setEndDate(parseDateFormat(userPackgeListItem.getOfferList().get(i).getInOfferEndDate()));
            offerInfo1.setOfferId(offerInfoList.get(i).getInOfferId());
            offerInfo.add(offerInfo1);
        }
        packageInfo.setOfferInfo(offerInfo);
        packageUsageInput.setPackageInfo(packageInfo);
        new PackageUsageService(this, packageUsageInput);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
//        super.onErrorListener(responseModel);
        try {
            ll_loadingLayout.setVisibility(View.GONE);
            if (sharedPrefrencesManger.getUsedActivity().equals("100")) {

            } else {
//            initTabLayout();
            }
            onGetPackageUsageFailure();
            sharedPrefrencesManger.setUsedActivity("");
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (frameLayout.getVisibility() == View.VISIBLE) {
            hideFrame();
        }
    }

    //0544486207
//0544486263
    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            ll_loadingLayout.setVisibility(View.GONE);

            if (responseModel != null && responseModel.getResultObj() != null) {
                packageUsageOutput = (PackageUsageOutput) responseModel.getResultObj();

                if (packageUsageOutput != null && packageUsageOutput.getUsageGraphList() != null && packageUsageOutput.getUsageGraphList().size() >= 1) {

                    initTabLayout();
                } else {
                    onGetPackageUsageFailure();
                }
            } else {
                onGetPackageUsageFailure();
            }
            ll_loadingLayout.setVisibility(View.GONE);
//        if (packageUsageOutput != null)
//            Log.v("oooooooooops", packageUsageOutput.getUsageGraphList().get(0).getColor() + "");
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        super.onUnAuthorizeToken(masterErrorResponse);
    }


    public String parseDate(String date) {
        String returnString = "";
        SimpleDateFormat dateFormatter;
        try {
            Date formattedDate;
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
            formattedDate = df.parse(date);
            if (currentLanguage.equals("en")) {
                dateFormatter = new SimpleDateFormat("dd MMM, HH:mm", Locale.US);
            } else {
                dateFormatter = new SimpleDateFormat("dd MMM, HH:mm", Locale.US);
            }
            return dateFormatter.format(formattedDate);

        } catch (Exception ex) {
            return returnString;

        }
    }

    public static String parseDateFormat(String date) {
        String returnString = "";
        SimpleDateFormat dateFormatter;
        try {
            Date formattedDate;
            //28/09/2016 19:13:20
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            formattedDate = df.parse(date);
            dateFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
            return dateFormatter.format(formattedDate);
        } catch (Exception ex) {
            return returnString;

        }
    }

    public static String parseDateWithDateSubtraction(String date) {
        String returnString = "";
        SimpleDateFormat dateFormatter;
        try {
            Date formattedDate;
            //28/09/2016 19:13:20
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            formattedDate = df.parse(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(formattedDate);
            long timeOld = calendar.getTimeInMillis();
            long timeToSubtract = 2592000000L;
            long newTime = timeOld - timeToSubtract;
            Date dateAfterSubtraction = new Date(newTime);

            dateFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
            return dateFormatter.format(dateAfterSubtraction);
        } catch (Exception ex) {
            return returnString;

        }
    }

    public void onGetPackageUsageFailure() {
        ll_errorLayout.setVisibility(View.VISIBLE);
    }
}