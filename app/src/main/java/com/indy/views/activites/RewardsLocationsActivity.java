package com.indy.views.activites;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.indy.R;
import com.indy.adapters.AvilableRewardsAdapter;
import com.indy.controls.DailogeInterface;

/**
 * Created by emad on 9/27/16.
 */

public class RewardsLocationsActivity extends MasterActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleMap.OnInfoWindowClickListener, LocationListener, DailogeInterface {
    private GoogleMap mMap;
    public MarkerOptions marker;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rewards_locations);
        initUI();
    }

    @Override
    public void initUI() {
        super.initUI();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onInfoWindowClick(Marker marker) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        for (int i = 0; i < AvilableRewardsAdapter.storeLocations.size(); i++) {
            LatLng fLatLng = new LatLng(AvilableRewardsAdapter.storeLocations.get(i).getGeoLocation().getLatitude(),
                    AvilableRewardsAdapter.storeLocations.get(i).getGeoLocation().getLongitude());
            marker = new MarkerOptions().position(fLatLng);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(fLatLng,
                    10));
            mMap.addMarker(marker.position(fLatLng).title(""));

        }

        mMap.setOnMarkerClickListener(this);
        mMap.setOnInfoWindowClickListener(this);
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latlng) {

            }
        });
    }

    @Override
    public void onAgreeListener(String alertType) {

    }

    @Override
    public void onCancelListener(String alertType) {

    }
}
