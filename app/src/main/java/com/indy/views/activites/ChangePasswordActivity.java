package com.indy.views.activites;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.helpers.ValidationHelper;
import com.indy.models.ForgotPassword.ForgotPasswordInputModel;
import com.indy.models.ForgotPassword.ForgotPasswordOutputModel;
import com.indy.models.changepassword.ChangePasswordInput;
import com.indy.models.changepassword.ChangePasswordOutput;
import com.indy.models.login.UserProfile;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.services.ChangePasswordService;
import com.indy.services.ForgotPasswordService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.usage.ChangePasswordCodeFragment;
import com.indy.views.fragments.usage.ForgotPasswordSettingsFragment;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;

/**
 * Created by emad on 9/27/16.
 */

public class ChangePasswordActivity extends MasterActivity {
    private EditText currentPasswordEtxt;
    private EditText newPasswordEtxt;
    private EditText retypePasswordEtxt;
    private TextView forgotPasswordEtxt, btn_show;
    private TextInputLayout til_currentPassword, til_newPassword, til_reTypePassword;
    private Button backImg, bt_okConfirmation;
    private ChangePasswordOutput changePasswordOutput;
    private ChangePasswordInput changePasswordInput;
    private TextView goto_change_pass,btn_continue;
    private Button helpBtn;

    private LinearLayout ll_main, ll_confirmation;
    //    ProgressBar progressBar;
    UserProfile userProfile;
    private ForgotPasswordInputModel forgotPasswordInputModel;
    private ForgotPasswordOutputModel forgotPasswordOutputModel;
    private String mobileNoTag = "MOBILENOTAG";
    private String codeTag = "CODETAG";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_changepassword);
        initUI();
        setListeners();
    }

    @Override
    public void initUI() {
        super.initUI();
        currentPasswordEtxt = (EditText) findViewById(R.id.currentPassword_user_profile);
        newPasswordEtxt = (EditText) findViewById(R.id.New_password_user_profile);
        retypePasswordEtxt = (EditText) findViewById(R.id.ReType_password_user_profile);
        forgotPasswordEtxt = (TextView) findViewById(R.id.tv_forgot_password);
        btn_continue = (TextView) findViewById(R.id.ConformSetNewPasswordbtn);
        til_currentPassword = (TextInputLayout) findViewById(R.id.currentPassword_text_input_layout);
        til_newPassword = (TextInputLayout) findViewById(R.id.New_password_text_input_layout);
        til_reTypePassword = (TextInputLayout) findViewById(R.id.ReType_password_text_input_layout);
        if (sharedPrefrencesManger != null) {
            if (sharedPrefrencesManger.getUserProfileObject() != null) {
                userProfile = sharedPrefrencesManger.getUserProfileObject();
            }
        }

        ll_main = (LinearLayout) findViewById(R.id.ll_main_content_view);
        ll_confirmation = (LinearLayout) findViewById(R.id.ll_confirmation_message);

        ll_main.setVisibility(View.VISIBLE);
        ll_confirmation.setVisibility(View.GONE);
//        userProfile = getIntent().getParcelableExtra(ConstantUtils.userProfile);
        backImg = (Button) findViewById(R.id.backImg);
        helpBtn = (Button) findViewById(R.id.helpBtn);
        btn_show = (TextView) findViewById(R.id.show_password);
        TextView headerTxt = (TextView) findViewById(R.id.titleTxt);
        headerTxt.setText(getString(R.string.change_password_spaced));
//        currentPasswordEtxt.setText(sharedPrefrencesManger.getPassword());
//        currentPasswordEtxt.setEnabled(false);
//        currentPasswordEtxt.setAlpha(1.0f);

//        if (getIntent().getExtras() != null && getIntent().getExtras().getString(ConstantUtils.password) != null) {
//            currentPasswordEtxt.setText(getIntent().getExtras().getString(ConstantUtils.password));
//        }

//        disableContinueBtn();

        //..............header Buttons............
        RelativeLayout backLayout = (RelativeLayout) findViewById(R.id.backLayout);
        RelativeLayout helpLayout = (RelativeLayout) findViewById(R.id.helpLayout);

        helpLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChangePasswordActivity.this, HelpActivity.class);
                startActivity(intent);
            }
        });

        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard();
                onBackPressed();
            }
        });
        btn_show.setVisibility(View.GONE);
        //..............header Buttons............

//        progressBar = (ProgressBar) findViewById(R.id.progressID);
    }

    private void setListeners() {
        forgotPasswordEtxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                onForgotPassword();
                onForgotPasswordService();
                Bundle bundle2 = new Bundle();

                bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_edit_profile_forgot_password);
                bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_edit_profile_forgot_password);
                mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_edit_profile_forgot_password, bundle2);
            }
        });
        btn_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onConfrim();
            }
        });
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        btn_show.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        currentPasswordEtxt.setInputType(InputType.TYPE_CLASS_TEXT);
                        break;
                    case MotionEvent.ACTION_UP:
                        currentPasswordEtxt.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        break;
                }
                return true;
            }
        });
        helpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChangePasswordActivity.this, HelpActivity.class);
                startActivity(intent);
            }
        });
        bt_okConfirmation = (Button) findViewById(R.id.okBtn);
        bt_okConfirmation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChangePasswordActivity.this.finish();
                if (SwpeMainActivity.swpeMainActivityInstance != null)
                    SwpeMainActivity.swpeMainActivityInstance.finish();
                if (UserProfileActivity.userProfileActivityInstance != null)
                    UserProfileActivity.userProfileActivityInstance.finish();
                Intent intent = new Intent(ChangePasswordActivity.this, RegisterationActivity.class);
                intent.putExtra("fragIndex", "7");
                startActivity(intent);
            }
        });
        newPasswordEtxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (newPasswordEtxt.getText().toString().length() > 0 && retypePasswordEtxt.getText().toString().length() > 0 && currentPasswordEtxt.getText().toString().length() > 0) {
                    enableContinueBtn();
                } else {
                    disableContinueBtn();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        retypePasswordEtxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (newPasswordEtxt.getText().toString().length() > 0 && retypePasswordEtxt.getText().toString().length() > 0 && currentPasswordEtxt.getText().toString().length() > 0) {
                    enableContinueBtn();
                } else {
                    disableContinueBtn();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        currentPasswordEtxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (newPasswordEtxt.getText().toString().length() > 0 && retypePasswordEtxt.getText().toString().length() > 0 && currentPasswordEtxt.getText().toString().length() > 0) {
                    enableContinueBtn();
                } else {
                    disableContinueBtn();
                }

                if (currentPasswordEtxt.getText().toString().length() == 0) {
                    btn_show.setVisibility(View.GONE);
                } else {
                    btn_show.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
//        currentPasswordEtxt.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onForgotPassword();
//            }
//        });
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            onBackPressed();
            if(responseModel!=null && responseModel.getResultObj()!=null) {
                if (responseModel.getServiceType().equalsIgnoreCase(ConstantUtils.forgotPasswordService)) {
                    forgotPasswordOutputModel = (ForgotPasswordOutputModel) responseModel.getResultObj();
                    if (forgotPasswordOutputModel != null) {
                        if (forgotPasswordOutputModel.getErrorCode() != null)
                            onError();
                        else
                            onContinue();
                    }
                } else {
                    changePasswordOutput = (ChangePasswordOutput) responseModel.getResultObj();
                    if (changePasswordOutput != null) {
                        if (changePasswordOutput.getErrorCode() != null)
                            onPasswordChangedFailure();
                        else
                            onPasswordChangedSucessfully();
                    }

                }
            }
        }catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    private void onError() {

        Fragment errorFragmnet = new ErrorFragment();
        Bundle bundle = new Bundle();
        if (currentLanguage != null) {
            if (currentLanguage.equalsIgnoreCase("en"))
                bundle.putString(ConstantUtils.errorString, forgotPasswordOutputModel.getErrorMsgEn());
            else
                bundle.putString(ConstantUtils.errorString, forgotPasswordOutputModel.getErrorMsgAr());
        }
        errorFragmnet.setArguments(bundle);
        replaceFragmnet(errorFragmnet, R.id.frameLayout, true);

    }

    private void onContinue() {

        Fragment forgotPasswordCodeFragment = new ChangePasswordCodeFragment();
        Bundle bundle = new Bundle();
        bundle.putString(mobileNoTag, sharedPrefrencesManger.getMobileNo());
        bundle.putString(codeTag, forgotPasswordOutputModel.getGeneratedCode());
        forgotPasswordCodeFragment.setArguments(bundle);
        replaceFragmnet(forgotPasswordCodeFragment, R.id.frameLayout, true);

    }


    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        onBackPressed();
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    private void onPasswordChangedSucessfully() {
        if (changePasswordOutput.getUpdated()) {

            CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(),ConstantUtils.TAGGING_edit_profile_set_new_password);

            showConfirmationView();
//            Toast.makeText(getApplicationContext(), R.string.password_changed_sucessfuly, Toast.LENGTH_SHORT).show();
        }
        //        else
    }


    public void showConfirmationView(){
        ll_main.setVisibility(View.GONE);
        ll_confirmation.setVisibility(View.VISIBLE);
    }

    private void onPasswordChangedFailure() {
        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(),ConstantUtils.TAGGING_edit_profile_change_password_failure);

        showErrorFragment();
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        try {
            onBackPressed();
        }catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
//        progressBar.setVisibility(View.GONE);

    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
//        progressBar.setVisibility(View.VISIBLE);
        replaceFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        String password = ValidationHelper.getMd5(currentPasswordEtxt.getText().toString().trim()).toUpperCase();
        String newPassword = ValidationHelper.getMd5(newPasswordEtxt.getText().toString().trim()).toUpperCase();
        String passwordConfirmation = ValidationHelper.getMd5(retypePasswordEtxt.getText().toString().trim()).toUpperCase();
        changePasswordInput = new ChangePasswordInput();
        changePasswordInput.setOldPassword(password);
        changePasswordInput.setPassword(newPassword);
        changePasswordInput.setPasswordConfirmation(passwordConfirmation);
        changePasswordInput.setAppVersion(appVersion);
        changePasswordInput.setToken(token);
        changePasswordInput.setAuthToken(authToken);
        changePasswordInput.setOsVersion(osVersion);
        changePasswordInput.setChannel(channel);
        changePasswordInput.setDeviceId(deviceId);
        changePasswordInput.setLang(currentLanguage);
        if (userProfile != null)
            changePasswordInput.setMsisdn(userProfile.getMsisdn());
        else
            changePasswordInput.setMsisdn("");
        new ChangePasswordService(this, changePasswordInput);
    }


    //0502047219
    //test
//    6539
    //
    //    public void onClick(View view) {
//        switch (view.getId()) {
//            case R.id.ConformSetNewPasswordbtn:
//                onConfrim();
//                break;
//            case R.id.tv_forgot_password:
//                onForgotPassword();
//                break;
//        }
//    }
//

    private void onForgotPasswordService() {
        addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        forgotPasswordInputModel = new ForgotPasswordInputModel();
        forgotPasswordInputModel.setAppVersion(appVersion);
        forgotPasswordInputModel.setToken(token);
        forgotPasswordInputModel.setOsVersion(osVersion);
        forgotPasswordInputModel.setChannel(channel);
        forgotPasswordInputModel.setDeviceId(deviceId);
        forgotPasswordInputModel.setLang(currentLanguage);
        forgotPasswordInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        forgotPasswordInputModel.setContact(sharedPrefrencesManger.getMobileNo());
        forgotPasswordInputModel.setAuthToken(authToken);
        forgotPasswordInputModel.setLang(currentLanguage);
        new ForgotPasswordService(this, forgotPasswordInputModel);

    }

    private void onConfrim() {
        if (isValid())
            onConsumeService();
//        addFragmnet(new );
    }

    private void showErrorFragment() {
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();

        if (changePasswordOutput != null && changePasswordOutput.getErrorMsgEn() != null) {

            if (currentLanguage.equals("en")) {
                bundle.putString(ConstantUtils.errorString, changePasswordOutput.getErrorMsgEn());
            } else {
                bundle.putString(ConstantUtils.errorString, changePasswordOutput.getErrorMsgAr());
            }
        } else {
            bundle.putString(ConstantUtils.errorString, getString(R.string.error_));

        }


//                SwypeErrorResponse swypeErrorResponse = new SwypeErrorResponse();out
//                swypeErrorResponse.setSwyperErrorString(getString(R.string.password_didnot_match));
//                bundle.putSerializable(ConstantUtils.master_error_response, swypeErrorResponse);
        errorFragment.setArguments(bundle);
        replaceFragmnet(errorFragment, R.id.frameLayout, true);
    }

    private void onForgotPassword() {
        hideKeyBoard();
        ForgotPasswordSettingsFragment forgotPasswordFragment = new ForgotPasswordSettingsFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.fragmentType, ConstantUtils.changePasswordActivity);
        forgotPasswordFragment.setArguments(bundle);
        replaceFragmnet(forgotPasswordFragment, R.id.frameLayout, true);
//        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
//        frameLayout.bringToFront();
    }

    private boolean isValid() {
        boolean isValid = true;
        if (newPasswordEtxt.getText().toString().trim().length() == 0) {
            setTextInputLayoutError(til_newPassword, getString(R.string.please_enter_password));
            isValid = false;
        } else {
            removeTextInputLayoutError(til_newPassword);
        }
        if (currentPasswordEtxt.getText().toString().trim().length() == 0) {
            setTextInputLayoutError(til_currentPassword, getString(R.string.please_enter_password));
            isValid = false;
        } else {
            removeTextInputLayoutError(til_currentPassword);
        }

        if (retypePasswordEtxt.getText().toString().trim().length() == 0) {
            setTextInputLayoutError(til_reTypePassword, getString(R.string.please_confirm_password));
            isValid = false;
        } else {
            removeTextInputLayoutError(til_reTypePassword);

        }

        if (isValid) {
            if (!newPasswordEtxt.getText().toString().trim().equals(retypePasswordEtxt.getText().toString().trim())) {
                //error fragment handling here
                hideKeyBoard();
                isValid = false;
                setTextInputLayoutError(til_newPassword, getString(R.string.password_didnot_match));

//                ErrorFragment errorFragment = new ErrorFragment();
//                Bundle bundle = new Bundle();
//                bundle.putString(ConstantUtils.errorString, getString(R.string.password_didnot_match));
//
////                SwypeErrorResponse swypeErrorResponse = new SwypeErrorResponse();
////                swypeErrorResponse.setSwyperErrorString(getString(R.string.password_didnot_match));
////                bundle.putSerializable(ConstantUtils.master_error_response, swypeErrorResponse);
//                errorFragment.setArguments(bundle);
//                replaceFragmnet(errorFragment, R.id.frameLayout, true);
            } else {
                removeTextInputLayoutError(til_newPassword);
                if (isValid && (!ValidationHelper.isValidPassword(newPasswordEtxt.getText().toString().trim()) || newPasswordEtxt.getText().toString().trim().length() < 8)) {
                    setTextInputLayoutError(til_newPassword, getString(R.string.password_criteria_not_matched));
                    isValid = false;
                } else if (isValid) {
                    removeTextInputLayoutError(til_newPassword);
                }
            }
        }

        return isValid;
    }

    private void disableContinueBtn() {
        btn_continue.setAlpha(0.5f);
        btn_continue.setEnabled(false);
    }

    private void enableContinueBtn() {
        btn_continue.setAlpha(1.0f);
        btn_continue.setEnabled(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


}
