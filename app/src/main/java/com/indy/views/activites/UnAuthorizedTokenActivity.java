package com.indy.views.activites;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.indy.R;

/**
 * Created by emad on 11/14/16.
 */

public class UnAuthorizedTokenActivity extends MasterActivity {
    private TextView errorTxt;
    private Button okBtn;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_error);
        errorTxt = (TextView) findViewById(R.id.errorTxt);
        if (getIntent().getExtras() != null)
            if (getIntent().getExtras().getString("msg") != null) {
                errorTxt.setText(getIntent().getExtras().getString("msg"));
            } else {
                errorTxt.setText(getString(R.string.token_expires));
            }
        okBtn = (Button) findViewById(R.id.okBtn);
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                if (SwpeMainActivity.swpeMainActivityInstance != null)
                    SwpeMainActivity.swpeMainActivityInstance.finish();
                if (sharedPrefrencesManger != null)
                    sharedPrefrencesManger.clearSharedPref();
                Intent intent = new Intent(UnAuthorizedTokenActivity.this, RegisterationActivity.class);
                intent.putExtra("fragIndex", "7");
                startActivity(intent);
            }
        });
    }


}
