package com.indy.views.activites;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.controls.ServiceUtils;
import com.indy.helpers.DateHelpers;
import com.indy.helpers.GooglePlayServiceUtils;
import com.indy.models.LogEventInputModel;
import com.indy.models.onboarding_push_notifications.GetOnBoardingDetailsRequestModel;
import com.indy.models.onboarding_push_notifications.GetOnBoardingDetailsResponseModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.ocr.screens.EmailAddressFragment;
import com.indy.ocr.screens.EmiratesIdDetailsFragment;
import com.indy.services.GetOnboardingDetailsService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.utils.LocalizationUtils;
import com.indy.utils.SharedPrefrencesManger;
import com.indy.views.fragments.StartScreenFragment;
import com.indy.views.fragments.buynewline.CashOnDeliveryFragment;
import com.indy.views.fragments.buynewline.DeliveryDetailsAddress;
import com.indy.views.fragments.buynewline.NameDOBFragment;
import com.indy.views.fragments.buynewline.NewNumberFragment;
import com.indy.views.fragments.buynewline.OrderFragment;
import com.indy.views.fragments.buynewline.PaymentSucessfulllyFragment;
import com.indy.views.fragments.buynewline.Registeration2Fragment;
import com.indy.views.fragments.buynewline.exisitingnumber.EmiratesIDVerificationFragment;
import com.indy.views.fragments.buynewline.exisitingnumber.PromoCodeVerificationSuccessfulFragment;
import com.indy.views.fragments.login.LoginFragment;
import com.kofax.mobile.sdk.capture.creditcard.CreditCardWorkflowActivity;
import com.kofax.mobile.sdk.capture.model.CreditCard;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by emad on 8/2/16.
 */
public class RegisterationActivity extends MasterActivity {

    private Button backImg/*, langImg*/, helpBtn;
    //    private SharedPrefrencesManger sharedPrefrencesManger;
    LocalizationUtils localizationUtils;
    public static RegisterationActivity regInstance;
    private GooglePlayServiceUtils googlePlayServiceUtils;
    private String fragmentType;
    private RelativeLayout headerLayoutID;
    private TextView titleTxt;
    //    boolean isFromCheck = false;
    RelativeLayout backLayout;
    public static LogEventInputModel logEventInputModel;
    public String notificationType = null;
    private GetOnBoardingDetailsResponseModel getOnBoardingDetailsResponseModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_registeration);
        setContentView(R.layout.activity_registeration);
        localizationUtils = new LocalizationUtils(getApplicationContext());

//        if (getIntent().getBooleanExtra("isFromCheck", false)) {
//            isFromCheck = true;
//        } else {
//            isFromCheck = false;
//        }
        initUI();

    }

    @Override
    public void initUI() {
        super.initUI();
        backImg = (Button) findViewById(R.id.backImg);
        headerLayoutID = (RelativeLayout) findViewById(R.id.headerLayoutID);
        helpBtn = (Button) findViewById(R.id.helpBtn);
        titleTxt = (TextView) findViewById(R.id.titleTxt);
        logEventInputModel = new LogEventInputModel();
        logEventInputModel.setAppVersion(appVersion);
        logEventInputModel.setToken(token);
        logEventInputModel.setOsVersion(osVersion);
        logEventInputModel.setLang(sharedPrefrencesManger.getLanguage());
        logEventInputModel.setChannel(channel);
        logEventInputModel.setDeviceId(deviceId);
//        langImg = (ImageView) findViewById(R.id.langImg);
        Bundle extra = getIntent().getExtras();
        if (extra != null) {
            fragmentType = getIntent().getExtras().getString("fragIndex");
            notificationType = getIntent().getExtras().getString(ConstantUtils.notification_type);
            Log.d("RegistrationActivity: ", " notificationType: " + notificationType);
        }

//        backImg.setVisibility(View.GONE);
        regInstance = this;
//        sharedPrefrencesManger = new SharedPrefrencesManger(getApplicationContext());
        sharedPrefrencesManger.setTempAppId(CommonMethods.getUDIDForLogging(this, token));
        //todo : if need to check directly the success tracking screen, uncomment following line:
//        replaceFragmnet(new PaymentSucessfulllyFragment(), R.id.frameLayout, false);
        displayNextFragment();
//        backImg.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onBackPressed();
//            }
//        });
        String date = new DateHelpers().getCurrentDate();
        googlePlayServiceUtils = new GooglePlayServiceUtils(this);
        Log.v("current_date", date + " ");
        googlePlayServiceUtils.registerReceiver();
        googlePlayServiceUtils.startIntentService();
        //..............header Buttons............
        backLayout = (RelativeLayout) findViewById(R.id.backLayout);
        RelativeLayout helpLayout = (RelativeLayout) findViewById(R.id.helpLayout);

        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard();
                onBackPressed();
//                Intent intent = new Intent(RegisterationActivity.this,TutorialActivity.class);
//                startActivity(intent);
            }
        });
        helpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterationActivity.this, HelpActivity.class);
                startActivity(intent);
            }
        });
        helpLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterationActivity.this, HelpActivity.class);
                startActivity(intent);
            }
        });

        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard();
                onBackPressed();
            }
        });
        //..............header Buttons............
    }


    public void showBackBtn() {
        backImg.setVisibility(View.VISIBLE);
        backLayout.setVisibility(View.VISIBLE);
    }

    public void hideBackBtn() {
        backImg.setVisibility(View.GONE);
        backLayout.setVisibility(View.GONE);

    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);

        if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.log_onboarding_details_service)) {
            getOnBoardingDetailsResponseModel = (GetOnBoardingDetailsResponseModel) responseModel.getResultObj();
            if (getOnBoardingDetailsResponseModel != null) {
                if (getOnBoardingDetailsResponseModel.getProfileId() != null)
                    openOnboardingNotification(notificationType);
                else
                    openOnboardingNotification("14");
            }

        }
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);

        if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.log_onboarding_details_service)) {
            openOnboardingNotification("14");
        }
    }


    //    private void onLangChange() {
//        langImg.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.arLanguage)) {
//                    langImg.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.en));
//                    sharedPrefrencesManger.setLanguage(ConstantUtils.enLanguage);
//                    localizationUtils.switchLocalizaion(ConstantUtils.arLng);
//
//                } else {
//                    langImg.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.ar));
//                    sharedPrefrencesManger.setLanguage(ConstantUtils.arLanguage);
//                    localizationUtils.switchLocalizaion(ConstantUtils.enLng);
//
//                }
//            }
//        });
//    }

    public static boolean isAnimationCustom = false;

    @Override
    public void onBackPressed() {
        try {
            if (PaymentSucessfulllyFragment.paymentSucessfulllyFragmentInstance == null) {
                super.onBackPressed();
            } else {
                finish();
            }
//        Intent intent = new Intent(RegisterationActivity.this,TutorialActivity.class);
//        startActivity(intent);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void displayNextFragment() {
        Log.v("fragmentIndex", fragmentType + " ");

//        if (isFromCheck) {
//            replaceFragmnet(new LoginFragment(), R.id.frameLayout, false);
//            isFromCheck = false;
//        } else {

        if (fragmentType != null) {

            switch (fragmentType) {
                case "0":
                    isAnimationCustom = true;

                    replaceFragmnet(new StartScreenFragment(), R.id.frameLayout, false);
                    break;
                case "-1":
                    isAnimationCustom = true;
                    replaceFragmnet(new StartScreenFragment(), R.id.frameLayout, false);
                    break;
                case "4":
                    replaceFragmnet(new PaymentSucessfulllyFragment(), R.id.frameLayout, false);
                    break;
                case "7":
                case "5":
                    if (sharedPrefrencesManger != null)
                        sharedPrefrencesManger.clearSharedPref();
                    Fragment loginFrag = new LoginFragment();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(ConstantUtils.showBackBtn, false);
                    loginFrag.setArguments(bundle);
                    replaceFragmnet(loginFrag, R.id.frameLayout, false);
                    break;

                case "101":
                    replaceFragmnet(new StartScreenFragment(), R.id.frameLayout, false);
                    replaceFragmnet(new Registeration2Fragment(), R.id.frameLayout, true);
                    break;

                case "1011":

                    SplashActivity.isTutorialRunnung = false;
                    Fragment startScreenFragment = new StartScreenFragment();
                    Bundle bundle2 = new Bundle();
                    bundle2.putBoolean(ConstantUtils.showBackBtn, false);
                    startScreenFragment.setArguments(bundle2);
                    replaceFragmnet(startScreenFragment, R.id.frameLayout, false);
                    break;
                default:
                    isAnimationCustom = true;

                    replaceFragmnet(new StartScreenFragment(), R.id.frameLayout, false);
                    break;

            }
        } else if (notificationType != null) {

            switch (notificationType) {

                case "14":
                    if (sharedPrefrencesManger.getUserProfileObject() == null) {
                        Log.d("getNotificationData()", "14");
                        getNotificationData("14");
                    }
                    break;

                case "15":
                    if (sharedPrefrencesManger.getUserProfileObject() == null) {
                        Log.d("getNotificationData()", "15");
                        getNotificationData("15");
                    }
                    break;


                case "16":
                    if (sharedPrefrencesManger.getUserProfileObject() == null) {
                        Log.d("getNotificationData()", "16");
                        getNotificationData("16");
                    }
                    break;


                case "17":
                    if (sharedPrefrencesManger.getUserProfileObject() == null) {
                        Log.d("getNotificationData()", "17");
                        getNotificationData("17");
                    }
                    break;


                case "18":
                    if (sharedPrefrencesManger.getUserProfileObject() == null) {
                        Log.d("getNotificationData()", "18");
                        getNotificationData("18");
                    }
                    break;

            }
        } else {
            isAnimationCustom = true;
            SplashActivity.isTutorialRunnung = false;
            replaceFragmnet(new StartScreenFragment(), R.id.frameLayout, false);
        }
//        }
    }


    private void getNotificationData(String notificationId) {

        GetOnBoardingDetailsRequestModel getOnBoardingDetailsRequestModel = new GetOnBoardingDetailsRequestModel();
        getOnBoardingDetailsRequestModel.setAppVersion(appVersion);
        getOnBoardingDetailsRequestModel.setAuthToken(authToken);
        getOnBoardingDetailsRequestModel.setChannel(channel);
        getOnBoardingDetailsRequestModel.setDeviceId(deviceId);
        getOnBoardingDetailsRequestModel.setLang(currentLanguage);
        getOnBoardingDetailsRequestModel.setOsVersion(osVersion);
        getOnBoardingDetailsRequestModel.setImsi(sharedPrefrencesManger.getMobileNo());
        getOnBoardingDetailsRequestModel.setToken(token);
        getOnBoardingDetailsRequestModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        getOnBoardingDetailsRequestModel.setRegistrationId(sharedPrefrencesManger.getRegisterationID());

        new GetOnboardingDetailsService(this, getOnBoardingDetailsRequestModel);
    }

    public String getSelectedDate(String time) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd/MM/yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    private void openOnboardingNotification(String notificationId) {

        OrderFragment orderFragment = null;
        NewNumberFragment newNumberFragment = null;
        PromoCodeVerificationSuccessfulFragment promoCodeVerificationSuccessfulFragment = null;
        EmailAddressFragment emailAddressFragment = null;
        NameDOBFragment nameDOBFragment = null;
        EmiratesIDVerificationFragment emiratesIDVerificationFragment = null;
        EmiratesIdDetailsFragment emiratesIdDetailsFragment = null;
        DeliveryDetailsAddress deliveryDetailsAddress = null;
        CashOnDeliveryFragment cashOnDeliveryFragment = null;


        if (!notificationId.equalsIgnoreCase("14")) {
            orderFragment = new OrderFragment();
            newNumberFragment = new NewNumberFragment();
            promoCodeVerificationSuccessfulFragment = new PromoCodeVerificationSuccessfulFragment();
            emailAddressFragment = new EmailAddressFragment();
            nameDOBFragment = new NameDOBFragment();
            emiratesIDVerificationFragment = new EmiratesIDVerificationFragment();
            emiratesIdDetailsFragment = new EmiratesIdDetailsFragment();
            deliveryDetailsAddress = new DeliveryDetailsAddress();
            cashOnDeliveryFragment = new CashOnDeliveryFragment();


            OrderFragment.orderTypeStr = getOnBoardingDetailsResponseModel.getOrderType();
            OrderFragment.shippingMethodStr = getOnBoardingDetailsResponseModel.getShippingInfo();
            OrderFragment.totalAmount = Integer.valueOf(sharedPrefrencesManger.getMembershipPrice());
            OrderFragment.totalAmountSIM = Integer.valueOf(sharedPrefrencesManger.getSIMPrice());
            NewNumberFragment.msisdnNumber = getOnBoardingDetailsResponseModel.getMsisdn();
            EmailAddressFragment.mobileNoStr = getOnBoardingDetailsResponseModel.getContactNumber();
            EmailAddressFragment.emailStr = getOnBoardingDetailsResponseModel.getEmail();
            NameDOBFragment.emiratedID = getOnBoardingDetailsResponseModel.getEmiratesId();
            NameDOBFragment.fullName = getOnBoardingDetailsResponseModel.getFullName();
            EmiratesIDVerificationFragment.emiratesID = getOnBoardingDetailsResponseModel.getEmiratesId();
            EmiratesIdDetailsFragment.fullName = getOnBoardingDetailsResponseModel.getFullName();


        }

        if (getOnBoardingDetailsResponseModel.getPromoCode() != null && getOnBoardingDetailsResponseModel.getPromoCode().length() > 0) {
            OrderFragment.promoCode = getOnBoardingDetailsResponseModel.getPromoCode();
        } else {
            OrderFragment.promoCode = "";
        }

        if (getOnBoardingDetailsResponseModel.getPromoCodeType() != null && getOnBoardingDetailsResponseModel.getPromoCodeType().length() > 0) {
            PromoCodeVerificationSuccessfulFragment.promoCodeType = Integer.parseInt(getOnBoardingDetailsResponseModel.getPromoCodeType());
        } else {
            PromoCodeVerificationSuccessfulFragment.promoCodeType = -1;
        }

        switch (notificationId)

        {
            case "14":
                StartScreenFragment startScreenFragment = new StartScreenFragment();
                Bundle startBundle = new Bundle();
                startBundle.putString(ConstantUtils.KEY_NOTIFICATION_ITEM_ID, sharedPrefrencesManger.getItemId());
                startBundle.putBoolean(ConstantUtils.KEY_FROM_NOTIFICATION, true);
                startScreenFragment.setArguments(startBundle);
                replaceFragmnet(startScreenFragment, R.id.frameLayout, true);
                sharedPrefrencesManger.setNotificationIdForPopup("");
                sharedPrefrencesManger.setNotificationTypeForPopup("");
                sharedPrefrencesManger.setItemId("");
                SplashActivity.notificationType = "";
                break;

            case "15":
                Bundle orderBundle = new Bundle();
                orderBundle.putString(ConstantUtils.KEY_NOTIFICATION_ITEM_ID, sharedPrefrencesManger.getItemId());
                orderBundle.putBoolean(ConstantUtils.KEY_FROM_NOTIFICATION, true);
                orderFragment.setArguments(orderBundle);
                replaceFragmnet(orderFragment, R.id.frameLayout, true);
                sharedPrefrencesManger.setNotificationIdForPopup("");
                sharedPrefrencesManger.setNotificationTypeForPopup("");
                sharedPrefrencesManger.setItemId("");
                SplashActivity.notificationType = "";
                break;

            case "16":
                Bundle orderBundlePromo = new Bundle();
                orderBundlePromo.putString("promoCode", OrderFragment.promoCode);
                orderBundlePromo.putString(ConstantUtils.KEY_NOTIFICATION_ITEM_ID, sharedPrefrencesManger.getItemId());
                orderBundlePromo.putBoolean(ConstantUtils.KEY_FROM_NOTIFICATION, true);
                orderFragment.setArguments(orderBundlePromo);
                replaceFragmnet(orderFragment, R.id.frameLayout, true);
                sharedPrefrencesManger.setNotificationIdForPopup("");
                sharedPrefrencesManger.setNotificationTypeForPopup("");
                sharedPrefrencesManger.setItemId("");
                SplashActivity.notificationType = "";
                break;

            case "17":
                Bundle emailAddressBundle = new Bundle();
                emailAddressBundle.putString(ConstantUtils.KEY_NOTIFICATION_ITEM_ID, sharedPrefrencesManger.getItemId());
                if (getOnBoardingDetailsResponseModel.getBirthDate() != null && !getOnBoardingDetailsResponseModel.getBirthDate().isEmpty()) {
                    emailAddressBundle.putString(ConstantUtils.birthdate, getSelectedDate(getOnBoardingDetailsResponseModel.getBirthDate()));
                }
                emailAddressBundle.putBoolean(ConstantUtils.KEY_FROM_NOTIFICATION, true);
                emailAddressFragment.setArguments(emailAddressBundle);
                hideBackBtn();
                FragmentManager fm = getSupportFragmentManager();
                for (int i = 0; i < fm.getBackStackEntryCount(); i++) {
                    fm.popBackStack();
                }
                replaceFragmnet(emailAddressFragment, R.id.frameLayout, false);
                sharedPrefrencesManger.setNotificationIdForPopup("");
                sharedPrefrencesManger.setNotificationTypeForPopup("");
                sharedPrefrencesManger.setItemId("");
                SplashActivity.notificationType = "";
                break;

            case "18":
                if (OrderFragment.promoCode != null && OrderFragment.promoCode.length() > 0 && PromoCodeVerificationSuccessfulFragment.promoCodeType == 2) {
                    CashOnDeliveryFragment.orderTypeStr = "2";
                    Bundle emailCashOnDeliveryBundle = new Bundle();
                    emailCashOnDeliveryBundle.putString(ConstantUtils.KEY_NOTIFICATION_ITEM_ID, sharedPrefrencesManger.getItemId());
                    emailCashOnDeliveryBundle.putBoolean(ConstantUtils.KEY_FROM_NOTIFICATION, true);
                    deliveryDetailsAddress.setArguments(emailCashOnDeliveryBundle);
                    replaceFragmnet(deliveryDetailsAddress, R.id.frameLayout, true);
                    sharedPrefrencesManger.setNotificationIdForPopup("");
                    sharedPrefrencesManger.setNotificationTypeForPopup("");
                    sharedPrefrencesManger.setItemId("");
                    SplashActivity.notificationType = "";

                } else {
                    Bundle emailCashOnDeliveryBundle = new Bundle();
                    emailCashOnDeliveryBundle.putString(ConstantUtils.KEY_NOTIFICATION_ITEM_ID, sharedPrefrencesManger.getItemId());
                    emailCashOnDeliveryBundle.putBoolean(ConstantUtils.KEY_FROM_NOTIFICATION, true);
                    cashOnDeliveryFragment.setArguments(emailCashOnDeliveryBundle);
                    replaceFragmnet(cashOnDeliveryFragment, R.id.frameLayout, true);
                    sharedPrefrencesManger.setNotificationIdForPopup("");
                    sharedPrefrencesManger.setNotificationTypeForPopup("");
                    sharedPrefrencesManger.setItemId("");
                    SplashActivity.notificationType = "";
                }
                break;
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        googlePlayServiceUtils.registerReceiver();
        if (NewNumberFragment.isTimerActive) {

        }
    }

    @Override
    protected void onPause() {
//        googlePlayServiceUtils.stopReceiver();
        super.onPause();
    }

    public static void setRegInstance(RegisterationActivity regInstance) {
        RegisterationActivity.regInstance = regInstance;
    }

    public static RegisterationActivity getRegInstance() {
        return regInstance;
    }

    public void showHeaderLayout() {
        headerLayoutID.setVisibility(View.VISIBLE);
        setHeaderTitle("");
    }

    public void hideHeaderLayout() {
        headerLayoutID.setVisibility(View.GONE);
    }

    public void setHeaderTitle(String title) {
        titleTxt.setText(title);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1001) {
            if (data != null && data.hasExtra(CreditCardWorkflowActivity.CREDIT_CARD_RESULTS)) {
                CreditCard cardResult = data.getParcelableExtra(CreditCardWorkflowActivity.CREDIT_CARD_RESULTS);
//                Registeration2Fragment.tv_results.setText("Card number: " + cardResult.getCardNumber()
//                        + "\nExpiry:" + cardResult.getExpirationMonth() + "/" + cardResult.getExpirationYear());
            } else {
//                Registeration2Fragment.tv_results.setText("No result!");
            }
        }

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent != null && intent.getExtras() != null) {

            if (getIntent().getExtras().getString(ConstantUtils.notification_type) != null && !getIntent().getExtras().getString(ConstantUtils.notification_type).isEmpty()) {
                sharedPrefrencesManger.setNotificationTypeForPopup(getIntent().getExtras().getString(ConstantUtils.notification_type));
                notificationType = getIntent().getExtras().getString(ConstantUtils.notification_type);
            }
            if (getIntent().getExtras().getString(ConstantUtils.KEY_NOTIFICATION_ID) != null && !getIntent().getExtras().getString(ConstantUtils.KEY_NOTIFICATION_ID).isEmpty()) {
                sharedPrefrencesManger.setNotificationIdForPopup(getIntent().getExtras().getString(ConstantUtils.KEY_NOTIFICATION_ID));
            }

            if (getIntent().getExtras().getString(ConstantUtils.KEY_ITEM_ID) != null && !getIntent().getExtras().getString(ConstantUtils.KEY_ITEM_ID).isEmpty()) {
                sharedPrefrencesManger.setItemId(getIntent().getExtras().getString(ConstantUtils.KEY_ITEM_ID));

            }
            if (SplashActivity.notificationType != null && !SplashActivity.notificationType.isEmpty()) {
                notificationType = SplashActivity.notificationType;
                fragmentType = null;
                FragmentManager fm = getSupportFragmentManager();
                for (int i = 0; i < fm.getBackStackEntryCount(); i++) {
                    fm.popBackStack();
                }
                displayNextFragment();
            }
        }
    }

}


