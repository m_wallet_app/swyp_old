package com.indy.views.activites;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.indy.R;
import com.indy.controls.AdjustEvents;
import com.indy.controls.DailogeInterface;
import com.indy.helpers.GoogleMapsHelper;
import com.indy.helpers.autocomplete.AutoCompleteSearchAdapter;
import com.indy.helpers.autocomplete.Search;
import com.indy.models.LogEventInputModel;
import com.indy.models.location.GeoLocation;
import com.indy.models.location.LocationInputModel;
import com.indy.models.location.LocationModelResponse;
import com.indy.models.location.NearestLocationList;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.services.GetNearestLocationService;
import com.indy.services.LogApplicationFlowService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.utils.GPSTracker;
import com.indy.views.fragments.utils.ErrorFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class NearestStoreActivity extends MasterActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
        GoogleMap.OnInfoWindowClickListener, LocationListener, DailogeInterface {

    public GoogleMap mMap;
    public TextView distanceId, locationNameId, headerText;
    private Button backBtn, helpBtn;
    public MarkerOptions marker;
    public static HashMap<String, Integer> mMarkerList;
    //    private ProgressBar progressID;
    public LocationModelResponse mLocationModelResponse;
    private Button locateMe;
    public Double mLatitude, mLongitude;
    public String locationName = null;
    LocationManager locationManager;
    FrameLayout frameLayout;
    GPSTracker gpsTracker;
    Location mlocation;

    public static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
    };
    public static final int LOCATION_REQUEST = 1340;
    GoogleMapsHelper googleMapsHelper;
    public static NearestStoreActivity instance;

    //-----------AUTOCOMPLETE SEARCH------//
    AutoCompleteSearchAdapter autoCompleteSearchAdapter;
    ArrayList<Search> searches;
    AutoCompleteTextView et_autoComplete;
    TextView tv_change, tv_name, tv_distance;
    LinearLayout ll_infoWindow;
    RelativeLayout ll_marker;
    ImageView iv_locateMe;
    LogEventInputModel logEventInputModel;
    //------------AUTOCOMPLETE SEARCH----------//

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearest_stores);
        gpsTracker = new GPSTracker(this);
        mMarkerList = new HashMap<String, Integer>();
        initUI();
    }

    @Override
    public void initUI() {
        super.initUI();
        try {
            backBtn = (Button) findViewById(R.id.backImg);
            helpBtn = (Button) findViewById(R.id.helpBtn);
            distanceId = (TextView) findViewById(R.id.distanceId);
            locationNameId = (TextView) findViewById(R.id.locationNameId);
            locateMe = (Button) findViewById(R.id.locateMe);
            frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
//        progressID = (ProgressBar) findViewById(R.id.progressID);
//        addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
            helpBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(NearestStoreActivity.this, HelpActivity.class);
                    startActivity(intent);
                }
            });

            headerText = (TextView) findViewById(R.id.titleTxt);
            headerText.setText(getString(R.string.find_swyp_spaced));

            if(getIntent()!=null && getIntent().getExtras()!=null&& getIntent().getExtras().containsKey("logging_object")){
                logEventInputModel = (LogEventInputModel) getIntent().getExtras().getSerializable("logging_object");
            }

            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            if (mapFragment != null) {
                mapFragment.getMapAsync(this);
            }


            instance = this;
            googleMapsHelper = new GoogleMapsHelper(getApplicationContext());
            locateMe.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mLatitude != null && mLongitude != null)
                        googleMapsHelper.onGoogleMpasOpen(mLatitude, mLongitude, locationName);
                }
            });
            //..............header Buttons............
            RelativeLayout backLayout = (RelativeLayout) findViewById(R.id.backLayout);
            RelativeLayout helpLayout = (RelativeLayout) findViewById(R.id.helpLayout);

            helpLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(NearestStoreActivity.this, HelpActivity.class);
                    startActivity(intent);
                }
            });

            backLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    hideKeyBoard();
                    onBackPressed();
                }
            });
            //..............header Buttons............
            logEvent(AdjustEvents.NEAREST_STORE_LOCATION,new Bundle());

            et_autoComplete = (AutoCompleteTextView) findViewById(R.id.et_search_all);
            tv_change = (TextView) findViewById(R.id.tv_change);
            tv_distance = (TextView) findViewById(R.id.tv_distance);
            tv_name = (TextView) findViewById(R.id.tv_name);

            tv_change.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    et_autoComplete.setVisibility(View.VISIBLE);
                    ll_marker.setVisibility(View.GONE);
                    et_autoComplete.requestFocus();
                }
            });
            ll_marker = (RelativeLayout) findViewById(R.id.ll_marker);
            ll_marker.setVisibility(View.GONE);
            et_autoComplete.setVisibility(View.GONE);
            iv_locateMe = (ImageView) findViewById(R.id.iv_locateMe);
            iv_locateMe.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mMap != null && mlocation != null) {
                        LatLng latLng = new LatLng(mlocation.getLatitude(), mlocation.getLongitude());
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, ConstantUtils.zoomLevel));
                    }
                }
            });
        } catch (Exception ex) {
            CommonMethods.logException(ex);

        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        checkPermission();
        isGPSEnabled();
    }


    private void checkLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, true);
        locationManager.requestLocationUpdates(provider, 10000, 0, this);
        mMap.setMyLocationEnabled(true);

        mlocation = locationManager.getLastKnownLocation(provider);
        if (mlocation == null) {
            mlocation = mMap.getMyLocation();
        }
        if (mlocation != null) {
            onLocationChanged(mlocation);
        }
    }


    @Override
    public boolean onMarkerClick(Marker marker) {
        int index = -1;
        try {
            if (marker != null && marker.getId() != null) {
                index = mMarkerList.get(marker.getId());
                if (index != -1) {
                    String distance = mLocationModelResponse.getNearestLocationList().get(index).getDistance().toString();

                    locationName = mLocationModelResponse.getNearestLocationList().get(index).getAddressEn();

                    mLatitude = mLocationModelResponse.getNearestLocationList().get(index).getGeoLocation().getLatitude();
                    mLongitude = mLocationModelResponse.getNearestLocationList().get(index).getGeoLocation().getLongitude();
                    locationNameId.setText(locationName);

                    LatLng latLng = new LatLng(mLatitude, mLongitude);
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
//            ll_marker.setVisibility(View.VISIBLE);
                    et_autoComplete.setVisibility(View.GONE);
                    tv_name.setText(locationName);
                    tv_distance.setText(distance);
                }
            }
        } catch (Exception ex) {
            CommonMethods.logException(ex);

        }

        logEvent();

        return true;
    }


    @Override
    public void onInfoWindowClick(Marker marker) {
        GoogleMapsHelper openGoogleMaps = new GoogleMapsHelper(getApplicationContext());
        openGoogleMaps.onGoogleMpasOpen(marker.getPosition().latitude, marker.getPosition().longitude, locationName);
    }


    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
//        progressID.setVisibility(View.GONE);
//        onBackPressed();
        try {
            if (responseModel != null && responseModel.getResultObj() != null) {
                mLocationModelResponse = (LocationModelResponse) responseModel.getResultObj();
                if (mLocationModelResponse != null && mLocationModelResponse.getNearestLocationList() != null
                        && mLocationModelResponse.getNearestLocationList().size() > 0) {
                    googleMapsHelper.drawaMapPins();
                    createLocationList(mLocationModelResponse.getNearestLocationList());
                    addMarkerAtCurrentPosition(mlocation);
//                    ll_marker.setVisibility(View.VISIBLE);
                } else if (mLocationModelResponse != null && mLocationModelResponse.getErrorMsgEn() != null) {
                    if (sharedPrefrencesManger.getLanguage().equals("en")) {
                        showServiceErrorFragment(mLocationModelResponse.getErrorMsgEn());
                    } else {
                        showServiceErrorFragment(mLocationModelResponse.getErrorMsgAr());
                    }
                } else {
                    showServiceErrorFragment(getString(R.string.generice_error));
                }
//            if (mLocationModelResponse.getErrorCode() == null)
//                if (mLocationModelResponse.getNearestLocationList() != null)
//                    if (mLocationModelResponse.getNearestLocationList().size() > 0)
            }
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }
    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
    }

    @Override
    public void onLocationChanged(Location newLocation) {
        mlocation = newLocation;
        mLatitude = mlocation.getLatitude();
        mLongitude = mlocation.getLongitude();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


    private boolean canAccessLocation() {
        if (!hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
            return false;
        } else {
            return true;
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == checkSelfPermission(perm));
    }

    private void isGPSEnabled() {
        LocationManager locationManager = (LocationManager)
                getSystemService(LOCATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (canAccessLocation()) {// gps is enable
//                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && canAccessLocation()) {// gps is enable
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {// gps is disable
                    checkForGps();
                } else {
                    if (gpsTracker.canGetLocation()) {
                        mlocation = gpsTracker.getLocation();
                        if (mlocation != null) {
                            onConsumeService();
                            updateMap();
                        }

                    }
//                    chec/kLocation();
                }
//                checkLocation();
            } else {
                requestPermissions(LOCATION_PERMS, 0);
            }

        } else {
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) { // gps is disable
                checkForGps();
            } else { // gps is enable
                if (gpsTracker.canGetLocation()) {
                    mlocation = gpsTracker.getLocation();

                    onConsumeService();
                    updateMap();
                }
            }

        }

    }


    public void checkForGps() {
        showErrorFragment();
//        showErrorFragment();AlertDailogueHelper.showDaiogue(this, NearestStoreActivity.this, "Alert",
//                "Cannot get Location please enable GPS?", "Settings", "Cancel", null);
    }


    @Override
    public void onAgreeListener(String alertType) {
        try {
            Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivityForResult(intent, 100);
        }catch (ActivityNotFoundException ex){
            CommonMethods.logException(ex);
        }

    }

    private void showErrorFragment() {
        try {
            frameLayout.setVisibility(View.VISIBLE);
            ErrorFragment errorFragment = new ErrorFragment();
            Bundle bundle = new Bundle();
            bundle.putString(ConstantUtils.errorString, getString(R.string.enable_gps));
            bundle.putString(ConstantUtils.settingsButtonEnabled, "true");
            bundle.putString(ConstantUtils.buttonTitle, getString(R.string.settings));
            errorFragment.setArguments(bundle);
            addFragmnet(errorFragment, R.id.frameLayout, true);
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }
    }

    private void showServiceErrorFragment(String show) {
        frameLayout.setVisibility(View.VISIBLE);
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, show);

        errorFragment.setArguments(bundle);
        addFragmnet(errorFragment, R.id.frameLayout, true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        frameLayout.setVisibility(View.GONE);


        switch (requestCode) {
            case 65636:
                LocationManager locationManager = (LocationManager)
                        getSystemService(LOCATION_SERVICE);
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    checkForGps();//
                } else {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    mlocation = gpsTracker.getLocation();
                    if (mlocation != null) {
                        onConsumeService();
                        updateMap();
                    }
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onCancelListener(String alertType) {
        finish();
    }
    int counter = 0;
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        counter++;
        switch (requestCode) {
            case 1340:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    isGPSEnabled();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if(counter<6)
                        this.requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
                    }
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            default:
                break;
        }
    }

    private void checkPermission() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                this.requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            }
        }
    }

    public void updateMap() {
//        if (mMap != null) {
//            int zoomValue = 16;
//
//            CameraUpdate update = CameraUpdateFactory.newLatLng(new LatLng(mlocation.getLatitude(), mlocation.getLongitude()));
//
//            CameraUpdate zoom = CameraUpdateFactory.zoomTo(zoomValue);
//            mMap.moveCamera(update);
//
//            mMap.animateCamera(zoom);
//            mMap.addMarker(new MarkerOptions()
//                    .position(new LatLng(mlocation.getLatitude(), mlocation.getLongitude()))
//                    .title("Etisalat Store"));
//        } else {
////            Log.d("contact us","update map null");
//        }
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
//        progressID.setVisibility(View.VISIBLE);
        try {
            LocationInputModel locationInputModel = new LocationInputModel();
            locationInputModel.setSignificant(ConstantUtils.shops_findus);
            locationInputModel.setSize(ConstantUtils.number_of_locations);
            GeoLocation geoLocation = new GeoLocation();
            if (mlocation != null) {
                geoLocation.setLatitude(mlocation.getLatitude());
                geoLocation.setLongitude(mlocation.getLongitude());
            }
            locationInputModel.setGeoLocation(geoLocation);
            locationInputModel.setAppVersion(appVersion);
            locationInputModel.setToken(token);
            locationInputModel.setOsVersion(osVersion);
            locationInputModel.setChannel(channel);
            locationInputModel.setDeviceId(deviceId);
            locationInputModel.setAuthToken(authToken);
            new GetNearestLocationService(this, locationInputModel);
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }

    }

    private void createLocationList(final List<NearestLocationList> locationList) {

        if (locationList != null && !locationList.isEmpty()) {

            if (locationList.size() > 0) {
                searches = new ArrayList<Search>(locationList.size());
                if (sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)) {
                    for (NearestLocationList nearestLocationList : locationList) {
                        searches.add(new Search(nearestLocationList.getAddressEn(), nearestLocationList.getGeoLocation().getLatitude(),
                                nearestLocationList.getGeoLocation().getLongitude()));


                    }
                } else {
                    for (NearestLocationList nearestLocationList : locationList) {

                        searches.add(new Search(nearestLocationList.getAddressAr(), nearestLocationList.getGeoLocation().getLatitude(),
                                nearestLocationList.getGeoLocation().getLongitude()));

                    }
                }
                if (searches != null) {
                    if (this != null) {

                        autoCompleteSearchAdapter = new AutoCompleteSearchAdapter(this, R.layout.list_item_search, searches);

                        et_autoComplete.setAdapter(autoCompleteSearchAdapter);

                        et_autoComplete.setThreshold(1);

                    }
                }
            }
            autoCompleteSearchAdapter.setOnItemClickListener(new AutoCompleteSearchAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position, Search search) {
                    TextView charac = (TextView) view.findViewById(R.id.searchNameLabel);
                    String text = charac.getText().toString();
                    et_autoComplete.setText("");
                    et_autoComplete.clearListSelection();
                    et_autoComplete.dismissDropDown();
                    hideKeyBoard();
                    //HomeFragment.hideKeyboard(et_search);
                    NearestLocationList nearestLocationList = getLocationObject(search, locationList);
                    if (mMap != null) {
                        LatLng latLng = new LatLng(nearestLocationList.getGeoLocation().getLatitude(),
                                nearestLocationList.getGeoLocation().getLongitude());
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
//                        ll_marker.setVisibility(View.VISIBLE);
                        et_autoComplete.setVisibility(View.GONE);
                        if (currentLanguage.equals("en")) {
                            tv_name.setText(nearestLocationList.getAddressEn());
                        } else {
                            tv_name.setText(nearestLocationList.getAddressAr());
                        }
                        tv_distance.setText(nearestLocationList.getDistance() + getString(R.string.km));
                    }
                }
            });
        }
    }

    public NearestLocationList getLocationObject(Search search, List<NearestLocationList> locationList) {
        for (int i = 0; i < locationList.size(); i++) {
            if ((locationList.get(i).getAddressEn().equalsIgnoreCase(search.getName()) ||
                    locationList.get(i).getAddressAr().equalsIgnoreCase(search.getName()))
                    && locationList.get(i).getGeoLocation().getLatitude().equals(search.getLatitude())
                    && locationList.get(i).getGeoLocation().getLongitude().equals(search.getLongitude())) {

                return locationList.get(i);

            }
        }
        return null;

    }

    public void addMarkerAtCurrentPosition(Location mLocation) {
        if (mLocation != null) {
            LatLng mLatLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
            MarkerOptions tempMarker = new MarkerOptions().position(mLatLng);
            mMap.addMarker(
                    tempMarker.position(mLatLng)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.locate_me_marker)));
        }
    }

    private void logEvent() {

        logEventInputModel.setActionTransactionId(sharedPrefrencesManger.getTempAppId());
        logEventInputModel.setScreenId(ConstantUtils.SCREEN_ID_0001);
        logEventInputModel.setLocation(locationNameId.getText().toString());

        new LogApplicationFlowService(this,logEventInputModel);

    }
}
