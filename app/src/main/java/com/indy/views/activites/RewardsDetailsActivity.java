package com.indy.views.activites;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.indy.R;
import com.indy.models.rewards.RewardsList;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.rewards.UseVoucherFragment;
import com.squareup.picasso.Picasso;

/**
/**
 * Created by emad on 9/27/16.
 */

public class RewardsDetailsActivity extends MasterActivity {
    private Button mapBtn, reedeemBtn;
    private TextView rewardName, rewardPrice,numberofdeals,Discounttxt,Expiretxt;
    private RewardsList rewardsList;
    private ImageView rewardImage,shopImage;
    TextView titletext;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rewards_details);
        initUI();
    }

    @Override
    public void initUI() {
        super.initUI();
        rewardName = (TextView) findViewById(R.id.shopname);
        rewardPrice = (TextView) findViewById(R.id.rewardPrice);
        rewardImage = (ImageView) findViewById(R.id.rewardImg);
        numberofdeals = (TextView)findViewById(R.id.numberofdeals);
        shopImage = (ImageView)findViewById(R.id.shopImage);
        Discounttxt=(TextView)findViewById(R.id.Discounttxt);
        Expiretxt =(TextView)findViewById(R.id.Expiretxt);


        rewardsList = getIntent().getParcelableExtra(ConstantUtils.RewardsDetails);
        rewardName.setText(rewardsList.getNameEn());
        rewardName.setText(rewardsList.getAverageSaving() + " ");// + rewardsList.getPriceUnitEn());
        Picasso.with(getApplicationContext()).load(rewardsList.getImageUrl()).into(rewardImage);

        titletext = (TextView)findViewById(R.id.titleTxt);
        titletext.setText(getResources().getString(R.string.rewards_capital_header_spaced));
//        Log.v("store_locations", rewardsList.getStoreLocations().get(0).getAddressEn() + "emad");
    }

    public void onClick(View view) {
//        switch (view.getId()) {
//            case R.id.reedeemID:
//                onReedemClcik();
//                break;
//            case R.id.mapID:
//                onMapClicked();
//                break;
//        }
    }

    private void onReedemClcik() {
        replaceFragmnet(new UseVoucherFragment(), R.id.frameLayout, true);
    }

    private void onMapClicked() {
        Intent intent = new Intent(this, RewardsLocationsActivity.class);
        startActivity(intent);
    }
}
