package com.indy.views.activites;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.indy.R;
import com.indy.models.updateLanguage.UpdateLanguageInput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.services.UpdateLanguageService;
import com.indy.utils.ConstantUtils;
import com.indy.utils.LocalizationUtils;
import com.indy.views.fragments.utils.ErrorFragment;

/**
 * Created by emad on 9/27/16.
 */

public class MySettingsActivity extends MasterActivity implements AdapterView.OnItemSelectedListener {
    private Spinner selectLangSpinner;
    ArrayAdapter<CharSequence> adapter;
    MasterErrorResponse masterErrorResponse;
    FrameLayout frameLayout;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_settings);
        initUI();
        loadSpinnerItems();
    }

    @Override
    public void initUI() {
        super.initUI();
        selectLangSpinner = (Spinner) findViewById(R.id.selectLangSpinner);
        if(sharedPrefrencesManger.getLanguage().equals(ConstantUtils.enLng)) {
            selectLangSpinner.setSelection(0);
        }else{
            selectLangSpinner.setSelection(1);
        }
        frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
        frameLayout.setVisibility(View.GONE);
        selectLangSpinner.setOnItemSelectedListener(this);


    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();

        UpdateLanguageInput updateLanguageInput = new UpdateLanguageInput();
        updateLanguageInput.setDeviceId(deviceId);
        updateLanguageInput.setMsisdn(sharedPrefrencesManger.getMobileNo());
        updateLanguageInput.setOsVersion(osVersion);
        updateLanguageInput.setImsi(sharedPrefrencesManger.getMobileNo());
        updateLanguageInput.setLang(sharedPrefrencesManger.getLanguage());
        updateLanguageInput.setAppVersion(appVersion);
        updateLanguageInput.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        updateLanguageInput.setAuthToken(authToken);
        updateLanguageInput.setToken(token);

        new UpdateLanguageService(this,updateLanguageInput);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);

        masterErrorResponse = (MasterErrorResponse) responseModel.getResultObj();
        if(masterErrorResponse!=null && masterErrorResponse.getErrorCode()!=null){

            onFailure();
        }else{
            onSuccessfulLanguageChange();
        }

    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        onFailure();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if ((parent.getChildAt(0)) != null) {
            ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
            String language = ConstantUtils.lang_english;
            Log.e("IN LANGUAGE CHANGE","changing");
            if (!language.equals(sharedPrefrencesManger.getLanguage())) {
                sharedPrefrencesManger.setLanguage(ConstantUtils.enLng);
                onConsumeService();
            }
        }

        if ((parent.getChildAt(1)) != null) {
            ((TextView) parent.getChildAt(1)).setTextColor(Color.WHITE);
            String language = ConstantUtils.lang_arabic;

            if (!language.equals(sharedPrefrencesManger.getLanguage())) {
                sharedPrefrencesManger.setLanguage(ConstantUtils.arLng);
               onConsumeService();
            }
        }

    }



    public void onSuccessfulLanguageChange(){
        LocalizationUtils.switchLocalizaion(sharedPrefrencesManger.getLanguage(), this);
        finish();
        startActivity(new Intent(getApplicationContext(), AboutAppActivity.class));
    }

    public void onFailure(){
        if(masterErrorResponse!=null && masterErrorResponse.getErrorMsgEn()!=null){
            if(sharedPrefrencesManger.getLanguage().equals("en")){
                showErrorFragment(masterErrorResponse.getErrorMsgEn());
            }else{
                showErrorFragment(masterErrorResponse.getErrorMsgAr());
            }
        }else{
            showErrorFragment(getString(R.string.generice_error));
        }
    }

    private void showErrorFragment(String error) {
        frameLayout.setVisibility(View.VISIBLE);
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, error);
        errorFragment.setArguments(bundle);
        replaceFragmnet(errorFragment, R.id.frameLayout, true);
    }

    @Override
    public void onBackPressed() {

        if(frameLayout.getVisibility()== View.VISIBLE){

            frameLayout.setVisibility(View.GONE);
        }else{
            super.onBackPressed();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        if ((parent.getChildAt(0)) != null)
            ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
    }

    private void loadSpinnerItems() {
        selectLangSpinner = (Spinner) findViewById(R.id.selectLangSpinner);
        adapter = ArrayAdapter.createFromResource(this,
                R.array.languages_types, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        selectLangSpinner.setAdapter(adapter);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.aboutIndy:
                onAboutClick();
                break;
        }
    }

    private void onAboutClick() {
        startActivity(new Intent(this, AboutAppActivity.class));
    }
}
