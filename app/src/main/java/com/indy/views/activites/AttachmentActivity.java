package com.indy.views.activites;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

import com.indy.controls.DeletedImgeInterfcae;
import com.indy.controls.ImageListinerInterface;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.ImageItem;
import com.indy.utils.CommonMethods;
import com.indy.utils.ImgManage;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;


/**
 * Created by Eabdelhamid on 11/17/2015.
 */
public class AttachmentActivity extends MasterActivity implements DeletedImgeInterfcae {
    private ImageItem imageItem;
    public ImgManage imgManage;
    private ImageListinerInterface imageListinerInterface;
    private Uri takenPhotoUri, selectedPhotoURI;
    private String pathFromGallery = "";
    public final int PIC_FROM_CAMERA = 0;
    public final int PIC_FROM_Gallary = 1;
    public final int PIC_FOR_CROP = 100;
    public final int PIC_FOR_GALLERY_CROP = 200;
    public final int PIC_MIN_HEIGHT = 300;
    public final int PIC_MIN_WIDTH = 600;
    private Uri outputURI;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imgManage = new ImgManage(AttachmentActivity.this);
        //Initialize on every usage

    }

    private void doCrop(Uri picUri, int isFromCamera) {
        try {

            Intent cropIntent = new Intent("com.android.camera.action.CROP");

            cropIntent.setDataAndType(picUri, "image/*");
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("outputX", 128);
            cropIntent.putExtra("outputY", 128);
            cropIntent.putExtra("return-data", true);
            if (isFromCamera == 1) {
                startActivityForResult(cropIntent, PIC_FOR_CROP);
            } else {
                startActivityForResult(cropIntent, PIC_FOR_GALLERY_CROP);
            }
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {

            if (resultCode != 0) {
                switch (requestCode) {
                    case PIC_FROM_CAMERA:
                        takenPhotoUri = Uri.parse(imgManage.mCurrentPhotoPath);

                        Bitmap imageBitmap = imgManage.decodeUri(takenPhotoUri);

                        if (imageBitmap != null) {
//                        doCrop(takenPhotoUri,1);
                            imageItem = new ImageItem();
                            imageItem.setBitmap(imgManage.rotateBitmap(imageBitmap, takenPhotoUri, PIC_FROM_CAMERA));
                            imageItem.setUri(takenPhotoUri);
                            imageListinerInterface.onImageSet(imageItem);

                            CropImage.activity(takenPhotoUri)
                                    .setAllowRotation(false)
                                    .setAspectRatio(16, 10)
                                    .setMinCropWindowSize(PIC_MIN_WIDTH, PIC_MIN_HEIGHT)


                                    .setCropShape(CropImageView.CropShape.RECTANGLE)
                                    .start(this);
                        } else {
                            imageListinerInterface.onImageSet(null);
                        }
                        break;

                    case PIC_FROM_Gallary:
                        if (data != null) {
                            Bitmap bitmap2 = null;
                            selectedPhotoURI = data.getData();
                            pathFromGallery = selectedPhotoURI.getPath();
                            try {
                                bitmap2 = imgManage.decodeUri(selectedPhotoURI);
                            } catch (Exception e) {
                                e.printStackTrace();
                            } catch (OutOfMemoryError oumError) {
                                oumError.printStackTrace();
                            }
                            if (bitmap2 != null) {
                                CropImage.activity(selectedPhotoURI)

                                        .setAllowRotation(true)

                                        .setMinCropWindowSize(PIC_MIN_WIDTH, PIC_MIN_HEIGHT)
                                        .setAspectRatio(PIC_MIN_WIDTH, PIC_MIN_HEIGHT)
                                        .setCropShape(CropImageView.CropShape.RECTANGLE)
                                        .start(this);
//                        } else {
                                imageListinerInterface.onImageSet(null);
                            }

                        }


                        break;
                    case PIC_FOR_CROP:
                        if (data != null) {
                            Bundle extras = data.getExtras();
                            Bitmap bitmap = extras.getParcelable("data");
                            imageItem = new ImageItem();
                            imageItem.setBitmap(bitmap);
                            imageItem.setUri(takenPhotoUri);
                            imageListinerInterface.onImageSet(imageItem);
//                        yourImageView.setImageBitmap(bitmap);
                        }

                        break;

                    case PIC_FOR_GALLERY_CROP:
                        if (data != null) {
                            Bundle extras = data.getExtras();
                            Bitmap bitmap = extras.getParcelable("data");

                            imageItem = new ImageItem();
                            imageItem.setUri(selectedPhotoURI);
//                        if (data.getData().getPath() != null) {
                            imageItem.setImgUrl(pathFromGallery);
//                        }
                            imageItem.setBitmap(bitmap);
                            imageListinerInterface.onImageSet(imageItem);
                            break;
                        }
                        break;


                    case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                        CropImage.ActivityResult result = CropImage.getActivityResult(data);
                        if (resultCode == RESULT_OK) {
                            Uri resultUri = result.getUri();
                            imageItem = new ImageItem();
                            imageItem.setUri(resultUri);
//                        if (data.getData().getPath() != null) {
//                        resultUri.getPath()
                            imageItem.setImgUrl(resultUri.getPath());
//                        }
                            try {
                                imageItem.setBitmap(getThumbnail(resultUri));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            imageListinerInterface.onImageSet(imageItem);
                            pathFromGallery = "";
                        } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                            Exception error = result.getError();
                        }
                        break;

                }
            }
        }catch (Exception ex){
            CommonMethods.logException(ex);
        }
    }

    public void setImageListinerInterface(ImageListinerInterface imageListinerInterface, int imagePosition) {
        this.imageListinerInterface = imageListinerInterface;
    }


    @Override
    public void onDeleteImage(int imgPos) {
        imageListinerInterface.onImageSet(null);
    }

    @Override
    public void onEditMode(boolean isInEditMode, int imgPOs) {

    }

    public Bitmap getThumbnail(Uri uri) throws FileNotFoundException, IOException {
        InputStream input = this.getContentResolver().openInputStream(uri);

        BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
        onlyBoundsOptions.inJustDecodeBounds = true;
        onlyBoundsOptions.inDither = true;//optional
        onlyBoundsOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//optional
        BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
        input.close();
        if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1))
            return null;

        int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;

        double ratio = (originalSize > 400) ? (originalSize / 400) : 1.0;

        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
        bitmapOptions.inDither = true;//optional
        bitmapOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//optional
        input = getContentResolver().openInputStream(uri);
        Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);
        input.close();
        return bitmap;
    }

    private static int getPowerOfTwoForSampleRatio(double ratio) {
        int k = Integer.highestOneBit((int) Math.floor(ratio));
        if (k == 0) return 1;
        else return k;
    }


    @Override
    public void onErrorListener(BaseResponseModel responseModel) {

    }
}
