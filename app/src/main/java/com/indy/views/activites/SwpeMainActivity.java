package com.indy.views.activites;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.helpers.TripleDESEncryption;
import com.indy.models.faqs.FaqsResponseModel;
import com.indy.models.login.UserProfile;
import com.indy.models.onboarding_push_notifications.GetOnBoardingDetailsRequestModel;
import com.indy.models.onboarding_push_notifications.GetOnBoardingDetailsResponseModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.ocr.screens.EmailAddressFragment;
import com.indy.ocr.screens.EmiratesIdDetailsFragment;
import com.indy.services.GetOnboardingDetailsService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.AboutUs.InviteFriendFragment;
import com.indy.views.fragments.StartScreenFragment;
import com.indy.views.fragments.buynewline.CashOnDeliveryFragment;
import com.indy.views.fragments.buynewline.DeliveryDetailsAddress;
import com.indy.views.fragments.buynewline.NameDOBFragment;
import com.indy.views.fragments.buynewline.NewNumberFragment;
import com.indy.views.fragments.buynewline.OrderFragment;
import com.indy.views.fragments.buynewline.exisitingnumber.EmiratesIDVerificationFragment;
import com.indy.views.fragments.buynewline.exisitingnumber.PromoCodeVerificationSuccessfulFragment;
import com.indy.views.fragments.gamification.challenges.ChallengesMainFragment;
import com.indy.views.fragments.gamification.events.EventsMainFragment;
import com.indy.views.fragments.gamification.inbox.InboxFragment;
import com.indy.views.fragments.gamification.models.serviceInputOutput.OptInGamificationInputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.OptInGamificationOutputResponse;
import com.indy.views.fragments.gamification.services.OptInGamificationService;
import com.indy.views.fragments.login.LoginFragment;
import com.indy.views.fragments.rewards.RewardsFragment;
import com.indy.views.fragments.store.mainstore.MyStoreFragment;
import com.indy.views.fragments.usage.UsageFragment;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import static com.indy.utils.ConstantUtils.KEY_IS_DEEP_LINK_URI;
import static com.indy.utils.ConstantUtils.TAGGING_HAMBURGER;
import static com.indy.utils.ConstantUtils.TAGGING_menu_closed;
import static com.indy.utils.ConstantUtils.TAGGING_menu_opened;

/**
 * Created by emad on 10/10/16.
 */

public class SwpeMainActivity extends MasterActivity {
    private static final String TAG = "DemoActivity";
    private FrameLayout frameLayout;
    public SlidingUpPanelLayout mLayout;
    OptInGamificationOutputResponse optInGamificationOutputResponse;
    GetOnBoardingDetailsResponseModel getOnBoardingDetailsResponseModel;
    public static TextView usageTxtID;
    private TextView userName;
    private UserProfile userProfile;
    public static ImageView imgBg;
    private Context mContext;
    public static SwpeMainActivity swpeMainActivityInstance;
    private ImageView ic_scroll_up;
    private LinearLayout lv_main_swip;
    public static int clickedItem = 1;
    public static int usageeClicked = 2;
    public static int storeClicked = 3;
    public static int rewardsClicked = 4;
    public static int eventsClicked = 5;
    public static int challengesClicked = 6;
    public String currentNotification = "";
    public static boolean isMenuOpen = false;
    Intent service;
    private String[] fNickName;
    private TextView storeTxt, usageTxt, rewardsTxt, eventsTxt, challengesTxt;
    public static TripleDESEncryption tripleDESEncryption;
    public UsageFragment usageFragment;
    private String deepLinkScreenName = "";
    private String deepLinkItemId = "";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swype);


        if (dataDiffForLIVPopup()) {
            sharedPrefrencesManger.setLIVPopupDateTime(System.currentTimeMillis());
            startActivityForResult(new Intent(SwpeMainActivity.this, LivPopupActivity.class), 020);
        } else {
            initUI();
            setLisetners();
            callOptInService();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
//        super.onSaveInstanceState(outState, outPersistentState);
    }

    public void callOptInService() {
        addFragmnet(new LoadingFragmnet(), R.id.contentFrameLayout, true);

        OptInGamificationInputModel optInGamificationInputModel = new OptInGamificationInputModel();
        optInGamificationInputModel.setChannel(channel);
        optInGamificationInputModel.setLang(currentLanguage);
        optInGamificationInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        optInGamificationInputModel.setEmail(sharedPrefrencesManger.getEmail());
        optInGamificationInputModel.setAppVersion(appVersion);
        optInGamificationInputModel.setAuthToken(authToken);
        optInGamificationInputModel.setDeviceId(deviceId);
        optInGamificationInputModel.setToken(token);
        if (sharedPrefrencesManger.getGender().equalsIgnoreCase(getString(R.string.male))) {
            optInGamificationInputModel.setGender(1);
        } else {
            optInGamificationInputModel.setGender(2);
        }
        optInGamificationInputModel.setName(sharedPrefrencesManger.getFullName());
        optInGamificationInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
        optInGamificationInputModel.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        optInGamificationInputModel.setOsVersion(osVersion);
        optInGamificationInputModel.setApplicationId("B19B885D-5A48-48D7-A8C8-671A4EFBE4BC");
        new OptInGamificationService(this, optInGamificationInputModel);
    }


    @Override
    public void initUI() {
        super.initUI();
        mLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        usageTxtID = (TextView) findViewById(R.id.usageTxtID);
        userName = (TextView) findViewById(R.id.nameID);
        lv_main_swip = (LinearLayout) findViewById(R.id.lv_main_swip);
        ic_scroll_up = (ImageView) findViewById(R.id.ic_scroll_up);
        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
        userProfile = getIntent().getParcelableExtra(ConstantUtils.userProfile);
        this.mContext = getApplicationContext();
        imgBg = (ImageView) findViewById(R.id.imgBg);
//        Glide.with(this.mContext) // replace with 'this' if it's in activity
//                .load("https://giphy.com/gifs/3o6fJ8WVHKIO74nISk")
//                .asGif()
//                .error(R.drawable.bitmap_default_image) // show error drawable if the image is not a gif
//                .into(imgBg)
//                ;
        storeTxt = (TextView) findViewById(R.id.storeID);
        rewardsTxt = (TextView) findViewById(R.id.rewardsID);
        usageTxt = (TextView) findViewById(R.id.usageID);
        eventsTxt = (TextView) findViewById(R.id.eventsID);
        challengesTxt = (TextView) findViewById(R.id.challengesID);
        eventsTxt = (TextView) findViewById(R.id.eventsID);
        this.swpeMainActivityInstance = this;
        clickedItem = 2;
        if (((LinearLayout) findViewById(R.id.ll_usage)) != null) {
            ((LinearLayout) findViewById(R.id.ll_usage)).setPaddingRelative(0, 20, 0, 0);
        }
        setMenuStatus();
        mLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                Log.i(TAG, "onPanelSlide, offset " + slideOffset);
                Display display = getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int height = size.y;
                Bundle bundle = new Bundle();
//                if(((LinearLayout) findViewById(R.id.ll_usage))!=null) {
//                    ((LinearLayout) findViewById(R.id.ll_usage)).setPaddingRelative(0, 20, 0, 0);
//                }

                CommonMethods.logFirebaseEvent(mFirebaseAnalytics, TAGGING_HAMBURGER);

                if (slideOffset > (height * 0.7)) {
//                    Toast.makeText(SwpeMainActivity.this, "Change screen", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
//                Log.i(TAG, "onPanelStateChanged " + newState);
                if (newState == SlidingUpPanelLayout.PanelState.DRAGGING) {
                    usageTxtID.setVisibility(View.INVISIBLE);

                    ic_scroll_up.setBackground(getResources().getDrawable(R.drawable.ic_up));
                    if (UsageFragment.usageFragmentInsatnce != null)
                        UsageFragment.usageFragmentInsatnce.onSlideUp();
                    if (MyStoreFragment.myStoreFragmentInstance != null)
                        MyStoreFragment.myStoreFragmentInstance.onSlideUp();
                    if (RewardsFragment.rewardsFragmentInstance != null)
                        RewardsFragment.rewardsFragmentInstance.onSlideUp();
                    if (ChallengesMainFragment.challengesMainFragment != null)
                        ChallengesMainFragment.challengesMainFragment.onSlideUp();
                    if (com.indy.views.fragments.gamification.events.EventsMainFragment.eventsFragmentInstance != null)
                        EventsMainFragment.eventsFragmentInstance.onSlideUp();

                } else {
                    if (newState == SlidingUpPanelLayout.PanelState.COLLAPSED) {

                        ((LinearLayout) findViewById(R.id.lv_main_swip)).setVisibility(View.VISIBLE);

                        CommonMethods.logFirebaseEvent(mFirebaseAnalytics, TAGGING_menu_opened);

                        usageTxtID.setVisibility(View.INVISIBLE);
                        ic_scroll_up.setBackground(getResources().getDrawable(R.drawable.ic_up));
                        if (UsageFragment.usageFragmentInsatnce != null)
                            UsageFragment.usageFragmentInsatnce.onSlideDown();
                        if (MyStoreFragment.myStoreFragmentInstance != null)
                            MyStoreFragment.myStoreFragmentInstance.onSlideDown();
                        if (RewardsFragment.rewardsFragmentInstance != null)
                            RewardsFragment.rewardsFragmentInstance.onSlideDown();
                        if (ChallengesMainFragment.challengesMainFragment != null)
                            ChallengesMainFragment.challengesMainFragment.onSlideDown();
                        if (EventsMainFragment.eventsFragmentInstance != null)
                            EventsMainFragment.eventsFragmentInstance.onSlideDown();
                    } else {


                        CommonMethods.logFirebaseEvent(mFirebaseAnalytics, TAGGING_menu_closed);
                        usageTxtID.setVisibility(View.VISIBLE);
                        ic_scroll_up.setBackground(getResources().getDrawable(R.drawable.ic_scroll));
                    }
                }
            }
        });
        if (sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)) {
            userName.setLineSpacing(0, 1f);
        } else {
            userName.setLineSpacing(10, 1.4f);
        }
        mLayout.setFadeOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }
        });
        usageFragment = new UsageFragment();
        replaceFragmnet(usageFragment, R.id.contentFrameLayout, false);
        frameLayout = (FrameLayout) findViewById(R.id.contentFrameLayout);
        try {
            tripleDESEncryption = new TripleDESEncryption(getPackageName());
        } catch (Exception e) {
            e.printStackTrace();
        }
//
//        if(service==null) {
//            service = new Intent(this, LastUpdated.class);
//            startService(service);
//        }
        if (getIntent() != null && getIntent().getExtras() != null) {
            if (getIntent().getExtras().getString(ConstantUtils.notification_type) != null && !getIntent().getExtras().getString(ConstantUtils.notification_type).isEmpty()) {
                sharedPrefrencesManger.setNotificationTypeForPopup(getIntent().getExtras().getString(ConstantUtils.notification_type));
            }
            if (getIntent().getExtras().getString(ConstantUtils.KEY_NOTIFICATION_ID) != null && !getIntent().getExtras().getString(ConstantUtils.KEY_NOTIFICATION_ID).isEmpty()) {
                sharedPrefrencesManger.setNotificationIdForPopup(getIntent().getExtras().getString(ConstantUtils.KEY_NOTIFICATION_ID));
            }

            if (getIntent().getExtras().getString(ConstantUtils.KEY_ITEM_ID) != null && !getIntent().getExtras().getString(ConstantUtils.KEY_ITEM_ID).isEmpty()) {
                sharedPrefrencesManger.setItemId(getIntent().getExtras().getString(ConstantUtils.KEY_ITEM_ID));

            }

            if (getIntent().getExtras().getBoolean(KEY_IS_DEEP_LINK_URI)) {
                deepLinkScreenName = getIntent().getExtras().getString(ConstantUtils.KEY_DEEP_LINK_SCREEN_NAME);
                deepLinkItemId = getIntent().getExtras().getString(ConstantUtils.KEY_DEEP_LINK_URI_ITEM_ID);
                performDeepLinking(deepLinkScreenName, deepLinkItemId);
            }
        }
        checkForNotificationPopup();
    }


    public void checkForNotificationPopup() {

//        Notification type screens as below
//        14 - Get Sim Screen
//        15 - number reserved (promo code screen)
//        16 - promo code entered (promo code screen with filled promo code)
//        17 - verify emirates id data ( let us know you better screen)
//        18 - payment method screen (payment method screen with title tell us how you want to pay)

        if (sharedPrefrencesManger.getNotificationTypeForPopup() != null && !sharedPrefrencesManger.getNotificationTypeForPopup().isEmpty()) {
            String type = sharedPrefrencesManger.getNotificationTypeForPopup();
            switch (type) {

                case "2":
//                    Intent shareIntent = new Intent(getApplicationContext(), SuccessfulShareActivity.class);
//                    shareIntent.setFlags(FLAG_ACTIVITY_NEW_TASK);
//                    shareIntent.putExtra(ConstantUtils.KEY_NOTIFICATION_ID_FOR_SERVICE, sharedPrefrencesManger.getNotificationIdForPopup());
//
//                    sharedPrefrencesManger.setNotificationIdForPopup("");
//                    sharedPrefrencesManger.setNotificationTypeForPopup("");
//                    startActivity(shareIntent);
                    break;
                case "3":
//                    Intent badgeIntent = new Intent(getApplicationContext(), BadgeUnlockedActivity.class);
//                    badgeIntent.setFlags(FLAG_ACTIVITY_NEW_TASK);
//                    badgeIntent.putExtra(ConstantUtils.KEY_NOTIFICATION_ID_FOR_SERVICE, sharedPrefrencesManger.getNotificationIdForPopup());
//                    badgeIntent.putExtra(ConstantUtils.notification_type, sharedPrefrencesManger.getNotificationIdForPopup());
//                    sharedPrefrencesManger.setNotificationIdForPopup("");
//                    sharedPrefrencesManger.setNotificationTypeForPopup("");
//                    startActivity(badgeIntent);
                    break;
                case "1":
//                    Intent badgeIntent2 = new Intent(getApplicationContext(), BadgeUnlockedActivity.class);
//                    badgeIntent2.setFlags(FLAG_ACTIVITY_NEW_TASK);
//                    badgeIntent2.putExtra(ConstantUtils.KEY_NOTIFICATION_ID_FOR_SERVICE, sharedPrefrencesManger.getNotificationIdForPopup());
//                    badgeIntent2.putExtra(ConstantUtils.notification_type, sharedPrefrencesManger.getNotificationIdForPopup());
//
//                    sharedPrefrencesManger.setNotificationIdForPopup("");
//                    sharedPrefrencesManger.setNotificationTypeForPopup("");
//                    startActivity(badgeIntent2);
                    break;
                case "6":
                    clickedItem = rewardsClicked;
                    usageTxtID.setText(getString(R.string.rewards_capital_header_spaced));
                    lv_main_swip.setBackground(getResources().getDrawable(R.drawable.support_bg_a));
                    collapseMenu();
                    isMenuOpen = true;
                    Bundle bundle2 = new Bundle();
                    Bundle rewardsBundle = new Bundle();
                    rewardsBundle.putString(ConstantUtils.KEY_NOTIFICATION_ITEM_ID, sharedPrefrencesManger.getItemId());
                    bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_rewards_button);
                    bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_rewards_button);
                    mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_rewards_button, bundle2);
                    RewardsFragment rewardsFragment = new RewardsFragment();
                    rewardsFragment.setArguments(rewardsBundle);
                    replaceFragmnet(rewardsFragment, R.id.contentFrameLayout, true);
                    sharedPrefrencesManger.setNotificationIdForPopup("");
                    sharedPrefrencesManger.setNotificationTypeForPopup("");
                    sharedPrefrencesManger.setItemId("");
                    break;
                case "7":
                    clickedItem = storeClicked;
                    usageTxtID.setText(getString(R.string.store_capital_header_spaced));
//                lv_main_swip.setBackground(getResources().getDrawable(R.drawable.gra));
                    collapseMenu();
                    isMenuOpen = true;
                    MyStoreFragment myStoreFragment = new MyStoreFragment();
                    bundle2 = new Bundle();
                    Bundle bundleStore = new Bundle();
                    bundleStore.putString(ConstantUtils.STORE_NAVIGATION_INDEX, "1");
                    myStoreFragment.setArguments(bundleStore);
                    bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_store_button);
                    bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_store_button);
                    mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_store_button, bundle2);
                    replaceFragmnet(myStoreFragment, R.id.contentFrameLayout, true);
                    sharedPrefrencesManger.setNotificationIdForPopup("");
                    sharedPrefrencesManger.setNotificationTypeForPopup("");
                    sharedPrefrencesManger.setItemId("");
                    break;
                case "8":

                    break;
                case "9":
                    clickedItem = storeClicked;
                    usageTxtID.setText(getString(R.string.store_capital_header_spaced));
//                lv_main_swip.setBackground(getResources().getDrawable(R.drawable.gra));
                    collapseMenu();
                    isMenuOpen = true;
                    bundle2 = new Bundle();

                    bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_store_button);
                    bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_store_button);
                    mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_store_button, bundle2);
                    replaceFragmnet(new MyStoreFragment(), R.id.contentFrameLayout, true);
                    sharedPrefrencesManger.setNotificationIdForPopup("");
                    sharedPrefrencesManger.setNotificationTypeForPopup("");
                    sharedPrefrencesManger.setItemId("");
                    break;
                case "10":
                    clickedItem = eventsClicked;
                    usageTxtID.setText(getString(R.string.events));
//                lv_main_swip.setBackground(getResources().getDrawable(R.drawable.gra));
                    collapseMenu();
                    isMenuOpen = true;
                    EventsMainFragment eventsMainFragment = new EventsMainFragment();
                    Bundle eventsBundle = new Bundle();
                    eventsBundle.putString(ConstantUtils.KEY_NOTIFICATION_ITEM_ID, sharedPrefrencesManger.getItemId());
                    eventsMainFragment.setArguments(eventsBundle);
                    replaceFragmnet(eventsMainFragment, R.id.contentFrameLayout, true);
                    sharedPrefrencesManger.setNotificationIdForPopup("");
                    sharedPrefrencesManger.setNotificationTypeForPopup("");
                    sharedPrefrencesManger.setItemId("");
                    break;
                case "11":
                    break;

                case "12":
                    Bundle bundleInvite = new Bundle();

                    bundleInvite.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_invite_friend_button);
                    bundleInvite.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_invite_friend_button);
                    mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_invite_friend_button, bundleInvite);
                    startActivity(new Intent(getApplicationContext(), InviteFriendFragment.class));
                    break;

                case "13":
                    UsageFragment.tabLayoutIndex = 2;
                    break;

                case "14":
                    if (sharedPrefrencesManger.getUserProfileObject() == null) {
                        getNotificationData("14");
                    }
                    break;

                case "15":
                    if (sharedPrefrencesManger.getUserProfileObject() == null) {
                        getNotificationData("15");
                    }
                    break;


                case "16":
                    if (sharedPrefrencesManger.getUserProfileObject() == null) {
                        getNotificationData("16");
                    }
                    break;


                case "17":
                    if (sharedPrefrencesManger.getUserProfileObject() == null) {
                        getNotificationData("17");
                    }
                    break;


                case "18":
                    if (sharedPrefrencesManger.getUserProfileObject() == null) {
                        getNotificationData("18");
                    }
                    break;
                default:
                    break;

            }
        }
    }

    private void openOnboardingNotification(String notificationId) {

        OrderFragment orderFragment = null;
        NewNumberFragment newNumberFragment = null;
        PromoCodeVerificationSuccessfulFragment promoCodeVerificationSuccessfulFragment = null;
        EmailAddressFragment emailAddressFragment = null;
        NameDOBFragment nameDOBFragment = null;
        EmiratesIDVerificationFragment emiratesIDVerificationFragment = null;
        EmiratesIdDetailsFragment emiratesIdDetailsFragment = null;

        if (!notificationId.equalsIgnoreCase("14")) {
            orderFragment = new OrderFragment();
            newNumberFragment = new NewNumberFragment();
            promoCodeVerificationSuccessfulFragment = new PromoCodeVerificationSuccessfulFragment();
            emailAddressFragment = new EmailAddressFragment();
            nameDOBFragment = new NameDOBFragment();
            emiratesIDVerificationFragment = new EmiratesIDVerificationFragment();
            emiratesIdDetailsFragment = new EmiratesIdDetailsFragment();

            OrderFragment.orderTypeStr = getOnBoardingDetailsResponseModel.getOrderType();
            OrderFragment.shippingMethodStr = getOnBoardingDetailsResponseModel.getShippingInfo();
            OrderFragment.totalAmount = Integer.valueOf(sharedPrefrencesManger.getMembershipPrice());
            OrderFragment.totalAmountSIM = Integer.valueOf(sharedPrefrencesManger.getSIMPrice());
            NewNumberFragment.msisdnNumber = getOnBoardingDetailsResponseModel.getMsisdn();
            EmailAddressFragment.mobileNoStr = getOnBoardingDetailsResponseModel.getContactMsisdn();
            EmailAddressFragment.emailStr = getOnBoardingDetailsResponseModel.getEmail();
            NameDOBFragment.emiratedID = getOnBoardingDetailsResponseModel.getEmiratesId();
            NameDOBFragment.fullName = getOnBoardingDetailsResponseModel.getFullName();
            EmiratesIDVerificationFragment.emiratesID = getOnBoardingDetailsResponseModel.getEmiratesId();
            EmiratesIdDetailsFragment.fullName = getOnBoardingDetailsResponseModel.getFullName();
        }

        if (getOnBoardingDetailsResponseModel.getPromoCode() != null && getOnBoardingDetailsResponseModel.getPromoCode().length() > 0) {
            OrderFragment.promoCode = getOnBoardingDetailsResponseModel.getPromoCode();
        } else {
            OrderFragment.promoCode = "";
        }

        if (getOnBoardingDetailsResponseModel.getPromoCodeType() != null && getOnBoardingDetailsResponseModel.getPromoCodeType().length() > 0) {
            PromoCodeVerificationSuccessfulFragment.promoCodeType = Integer.parseInt(getOnBoardingDetailsResponseModel.getPromoCodeType());
        } else {
            PromoCodeVerificationSuccessfulFragment.promoCodeType = -1;
        }

        switch (notificationId) {
            case "14":
                StartScreenFragment startScreenFragment = new StartScreenFragment();
                Bundle startBundle = new Bundle();
                startBundle.putString(ConstantUtils.KEY_NOTIFICATION_ITEM_ID, sharedPrefrencesManger.getItemId());
                startScreenFragment.setArguments(startBundle);
                replaceFragmnet(startScreenFragment, R.id.contentFrameLayout, true);
                sharedPrefrencesManger.setNotificationIdForPopup("");
                sharedPrefrencesManger.setNotificationTypeForPopup("");
                sharedPrefrencesManger.setItemId("");
                break;

            case "15":
                Bundle orderBundle = new Bundle();
                orderBundle.putString(ConstantUtils.KEY_NOTIFICATION_ITEM_ID, sharedPrefrencesManger.getItemId());
                orderFragment.setArguments(orderBundle);
                replaceFragmnet(orderFragment, R.id.contentFrameLayout, true);
                sharedPrefrencesManger.setNotificationIdForPopup("");
                sharedPrefrencesManger.setNotificationTypeForPopup("");
                sharedPrefrencesManger.setItemId("");
                break;

            case "16":
                Bundle orderBundlePromo = new Bundle();
                orderBundlePromo.putString("promoCode", OrderFragment.promoCode);
                orderBundlePromo.putString(ConstantUtils.KEY_NOTIFICATION_ITEM_ID, sharedPrefrencesManger.getItemId());
                orderFragment.setArguments(orderBundlePromo);
                replaceFragmnet(orderFragment, R.id.contentFrameLayout, true);
                sharedPrefrencesManger.setNotificationIdForPopup("");
                sharedPrefrencesManger.setNotificationTypeForPopup("");
                sharedPrefrencesManger.setItemId("");
                break;

            case "17":
                Bundle emailAddressBundle = new Bundle();
                emailAddressBundle.putString(ConstantUtils.KEY_NOTIFICATION_ITEM_ID, sharedPrefrencesManger.getItemId());
                emailAddressFragment.setArguments(emailAddressBundle);
                replaceFragmnet(emailAddressFragment, R.id.contentFrameLayout, true);
                sharedPrefrencesManger.setNotificationIdForPopup("");
                sharedPrefrencesManger.setNotificationTypeForPopup("");
                sharedPrefrencesManger.setItemId("");
                break;

            case "18":
                if (OrderFragment.promoCode != null && OrderFragment.promoCode.length() > 0 && PromoCodeVerificationSuccessfulFragment.promoCodeType == 2) {
                    CashOnDeliveryFragment.orderTypeStr = "2";
                    Bundle emailCashOnDeliveryBundle = new Bundle();
                    emailCashOnDeliveryBundle.putString(ConstantUtils.KEY_NOTIFICATION_ITEM_ID, sharedPrefrencesManger.getItemId());
                    emailAddressFragment.setArguments(emailCashOnDeliveryBundle);
                    replaceFragmnet(new DeliveryDetailsAddress(), R.id.contentFrameLayout, true);
                    sharedPrefrencesManger.setNotificationIdForPopup("");
                    sharedPrefrencesManger.setNotificationTypeForPopup("");
                    sharedPrefrencesManger.setItemId("");

                } else {
                    Bundle emailCashOnDeliveryBundle = new Bundle();
                    emailCashOnDeliveryBundle.putString(ConstantUtils.KEY_NOTIFICATION_ITEM_ID, sharedPrefrencesManger.getItemId());
                    emailAddressFragment.setArguments(emailCashOnDeliveryBundle);
                    replaceFragmnet(new CashOnDeliveryFragment(), R.id.contentFrameLayout, true);
                    sharedPrefrencesManger.setNotificationIdForPopup("");
                    sharedPrefrencesManger.setNotificationTypeForPopup("");
                    sharedPrefrencesManger.setItemId("");
                }
                break;
        }

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent != null && intent.getExtras() != null) {

            if (getIntent().getExtras().getString(ConstantUtils.notification_type) != null && !getIntent().getExtras().getString(ConstantUtils.notification_type).isEmpty()) {
                sharedPrefrencesManger.setNotificationTypeForPopup(getIntent().getExtras().getString(ConstantUtils.notification_type));
            }
            if (getIntent().getExtras().getString(ConstantUtils.KEY_NOTIFICATION_ID) != null && !getIntent().getExtras().getString(ConstantUtils.KEY_NOTIFICATION_ID).isEmpty()) {
                sharedPrefrencesManger.setNotificationIdForPopup(getIntent().getExtras().getString(ConstantUtils.KEY_NOTIFICATION_ID));
            }

            if (getIntent().getExtras().getString(ConstantUtils.KEY_ITEM_ID) != null && !getIntent().getExtras().getString(ConstantUtils.KEY_ITEM_ID).isEmpty()) {
                sharedPrefrencesManger.setItemId(getIntent().getExtras().getString(ConstantUtils.KEY_ITEM_ID));

            }

            if (intent.getExtras().getBoolean(KEY_IS_DEEP_LINK_URI)) {
                deepLinkScreenName = intent.getExtras().getString(ConstantUtils.KEY_DEEP_LINK_SCREEN_NAME);
                deepLinkItemId = intent.getExtras().getString(ConstantUtils.KEY_DEEP_LINK_URI_ITEM_ID);
                performDeepLinking(deepLinkScreenName, deepLinkItemId);
                return;
            }

            checkForNotificationPopup();
        }
    }


    private void getNotificationData(String notificationId) {

        currentNotification = notificationId;
        GetOnBoardingDetailsRequestModel getOnBoardingDetailsRequestModel = new GetOnBoardingDetailsRequestModel();
        getOnBoardingDetailsRequestModel.setAppVersion(appVersion);
        getOnBoardingDetailsRequestModel.setAuthToken(authToken);
        getOnBoardingDetailsRequestModel.setChannel(channel);
        getOnBoardingDetailsRequestModel.setDeviceId(deviceId);
        getOnBoardingDetailsRequestModel.setLang(currentLanguage);
        getOnBoardingDetailsRequestModel.setOsVersion(osVersion);
        getOnBoardingDetailsRequestModel.setImsi(sharedPrefrencesManger.getMobileNo());
        getOnBoardingDetailsRequestModel.setToken(token);
        getOnBoardingDetailsRequestModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        getOnBoardingDetailsRequestModel.setRegistrationId(sharedPrefrencesManger.getRegisterationID());

        new GetOnboardingDetailsService(this, getOnBoardingDetailsRequestModel);
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
//        findViewById(R.id.shareID).setEnabled(false);
//        FaqsInputModel faqsInputModel = new FaqsInputModel();
//        faqsInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
//        faqsInputModel.setLang(currentLanguage);
//        faqsInputModel.setAppVersion(appVersion);
//        faqsInputModel.setToken(token);
//        faqsInputModel.setChannel(channel);
//        faqsInputModel.setSignificant(ConstantUtils.share_significant);
//        faqsInputModel.setDeviceId(deviceId);
//        faqsInputModel.setOsVersion(osVersion);
//        faqsInputModel.setAuthToken(authToken);
//        new FaqsService(this, faqsInputModel);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
//            findViewById(R.id.shareID).setEnabled(true);
            if (responseModel != null && responseModel.getResultObj() != null) {

                if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.optInGamification)) {

                    optInGamificationOutputResponse = (OptInGamificationOutputResponse) responseModel.getResultObj();
                    if (optInGamificationOutputResponse != null && optInGamificationOutputResponse.getUserId() != null &&
                            optInGamificationOutputResponse.getUserSessionId() != null) {
                        sharedPrefrencesManger.setUserSessionId(optInGamificationOutputResponse.getUserSessionId());
                        sharedPrefrencesManger.setUserId(optInGamificationOutputResponse.getUserId());


                    }
                    onBackPressed();
                } else if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.log_onboarding_details_service)) {
                    getOnBoardingDetailsResponseModel = (GetOnBoardingDetailsResponseModel) responseModel.getResultObj();
                    if (getOnBoardingDetailsResponseModel != null) {
                        openOnboardingNotification(currentNotification);
                    }

                } else {
                    FaqsResponseModel faqsResponseModel = (FaqsResponseModel) responseModel.getResultObj();
                    if (faqsResponseModel != null && faqsResponseModel.getSrcUrlEn() != null) {
                        if (currentLanguage.equals("en")) {
                            onShareLinkSuccess(faqsResponseModel.getSrcUrlEn());
                        } else {
                            onShareLinkSuccess(faqsResponseModel.getSrcUrlAr());
                        }
                    } else if (faqsResponseModel != null && faqsResponseModel.getErrorMsgEn() != null) {
                        if (currentLanguage.equals("en")) {
                            onShareLinkFailure(faqsResponseModel.getErrorMsgEn());
                        } else {
                            onShareLinkFailure(faqsResponseModel.getErrorMsgAr());
                        }
                    } else {
                        onShareLinkFailure(getString(R.string.generice_error));
                    }
                }
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
//        findViewById(R.id.shareID).setEnabled(true);
    }

    private void setMenuStatus() {
        if (sharedPrefrencesManger != null)
            if (sharedPrefrencesManger.getUsageStaus() == true)
                usageTxt.setVisibility(View.VISIBLE);
            else
                usageTxt.setVisibility(View.GONE);
        if (sharedPrefrencesManger.getRewardsStatuse() == true)
            rewardsTxt.setVisibility(View.VISIBLE);
        else
            rewardsTxt.setVisibility(View.GONE);
        if (sharedPrefrencesManger.getStoreStaus() == true)
            storeTxt.setVisibility(View.VISIBLE);
        else
            storeTxt.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        if (mLayout != null &&
                (mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED || mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.ANCHORED)) {
            collapseMenu();
//            if(isMenuOpen){
//                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
//                isMenuOpen = false ;
//            }
        } else {
            super.onBackPressed();
            clickedItem = -1;
            if (getSupportFragmentManager() != null && getSupportFragmentManager().getBackStackEntryCount() == 0) {// if stack is empty
                try {
                    LoginFragment.stopServiceNow();
                } catch (Exception ex) {
                }
            }
        }
    }

    public void setLisetners() {


        storeTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickedItem != storeClicked) {
                    clickedItem = storeClicked;
                    usageTxtID.setText(getString(R.string.store_capital_header_spaced));
//                lv_main_swip.setBackground(getResources().getDrawable(R.drawable.gra));
                    collapseMenu();
                    isMenuOpen = true;
                    Bundle bundle2 = new Bundle();

                    bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_store_button);
                    bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_store_button);
                    mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_store_button, bundle2);
                    replaceFragmnet(new MyStoreFragment(), R.id.contentFrameLayout, true);
                } else {
                    collapseMenu();
                }
            }
        });
        if (sharedPrefrencesManger.isEventsEnabled()) {
            eventsTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (clickedItem != eventsClicked) {
                        clickedItem = eventsClicked;
                        usageTxtID.setText(getString(R.string.events));
//                lv_main_swip.setBackground(getResources().getDrawable(R.drawable.gra));
                        collapseMenu();
                        isMenuOpen = true;

                        replaceFragmnet(new EventsMainFragment(), R.id.contentFrameLayout, true);
                    } else {
                        collapseMenu();
                    }
                }
            });
        } else {
            eventsTxt.setVisibility(View.GONE);
        }

        rewardsTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickedItem != rewardsClicked) {
                    clickedItem = rewardsClicked;
                    usageTxtID.setText(getString(R.string.rewards_capital_header_spaced));
                    lv_main_swip.setBackground(getResources().getDrawable(R.drawable.support_bg_a));
                    collapseMenu();
                    isMenuOpen = true;
                    Bundle bundle2 = new Bundle();

                    bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_rewards_button);
                    bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_rewards_button);
                    mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_rewards_button, bundle2);
                    replaceFragmnet(new RewardsFragment(), R.id.contentFrameLayout, true);
//                    replaceFragmnet(new EventsMainFragment(), R.id.contentFrameLayout, true);
                } else {
                    collapseMenu();

                }
            }
        });

//        findViewById(R.id.shareID).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                onConsumeService();
//
////                onShareLinkSuccess("");
//            }
//        });


        findViewById(R.id.inboxID).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                onConsumeService();
                addFragmnet(new InboxFragment(), R.id.contentFrameLayout, true);
                collapseMenu();
//                onShareLinkSuccess("");
            }
        });

        findViewById(R.id.invitefriendID).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle2 = new Bundle();

                bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_invite_friend_button);
                bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_invite_friend_button);
                mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_invite_friend_button, bundle2);
                startActivity(new Intent(getApplicationContext(), InviteFriendFragment.class));

            }
        });
        findViewById(R.id.getSupportID).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle2 = new Bundle();

                bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_Support_button);
                bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_Support_button);
                mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_Support_button, bundle2);
                startActivity(new Intent(getApplicationContext(), HelpActivity.class));

            }
        });


        findViewById(R.id.settingsID).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle2 = new Bundle();

                bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_settings_button);
                bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_settings_button);
                mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_settings_button, bundle2);
                startActivity(new Intent(getApplicationContext(), AboutAppActivity.class));
                finish();
            }
        });
        findViewById(R.id.editProfileID).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEditUserPrfile();
            }
        });

        if (sharedPrefrencesManger.isGamificationEnabled()) {
            challengesTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (clickedItem != challengesClicked) {
                        clickedItem = challengesClicked;
                        usageTxtID.setText(getString(R.string.challenges));
//                lv_main_swip.setBackground(getResources().getDrawable(R.drawable.gra));
                        collapseMenu();
                        isMenuOpen = true;
//                    CommonMethods.logFirebaseEvent(mFirebaseAnalytics,ConstantUtils.TAGGING_CHALLENGES);
//                    replaceFragmnet(new ChallengesMainFragment(), R.id.contentFrameLayout, true);
                        replaceFragmnet(new ChallengesMainFragment(), R.id.contentFrameLayout, true);
                    } else {
                        collapseMenu();
                    }
                }
            });
        } else {
            challengesTxt.setVisibility(View.GONE);
        }
        usageTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickedItem != usageeClicked) {
                    clickedItem = usageeClicked;
                    usageTxtID.setText(getString(R.string.usage_spaced));
                    Bundle bundle2 = new Bundle();
                    usageFragment = new UsageFragment();
                    bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_usage_button);
                    bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_usage_button);
                    mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_usage_button, bundle2);
                    replaceFragmnet(usageFragment, R.id.contentFrameLayout, true);
                    collapseMenu();
                    isMenuOpen = true;
                } else {
                    collapseMenu();
                }
            }
        });

    }

    public void onShareLinkSuccess(String urlToShare) {

        Bundle bundle2 = new Bundle();

        bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_share_button);
        bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_share_button);
        mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_share_button, bundle2);
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.share_swyp));
        String textToShare = getString(R.string.share_swype_app) + "\n" + urlToShare;
        share.putExtra(Intent.EXTRA_TEXT, textToShare);

        startActivityForResult(Intent.createChooser(share, "Share Invite"), 120);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
            switch (requestCode) {
                case 120:
                    CommonMethods.createAppRatingDialog(this, mFirebaseAnalytics);
                    break;
                case 020:
                    initUI();
                    setLisetners();
                    callOptInService();
                    break;
            }
    }

    public void onShareLinkFailure(String message) {

        Fragment errorFragmnet = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, message);
        errorFragmnet.setArguments(bundle);
        replaceFragmnet(errorFragmnet, R.id.contentFrameLayout, true);
        collapseMenu();

    }

    private void onEditUserPrfile() {

        Bundle bundle2 = new Bundle();

        bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_edit_profile_button);
        bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_edit_profile_button);
        mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_edit_profile_button, bundle2);
        Intent intent = new Intent(this, UserProfileActivity.class);
        intent.putExtra(ConstantUtils.userProfile, userProfile);
        startActivity(intent);
//        ActivityOptionsCompat options = ActivityOptionsCompat.
//                makeSceneTransitionAnimation(this, imgBg, ConstantUtils.userProfile);
//        startActivity(intent, options.toBundle());
//        collapseMenu();

    }


    public void collapseMenu() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mLayout.setTouchEnabled(false);
                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                mLayout.setTouchEnabled(true);
            }
        });
    }

    public void expandMenu() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mLayout.setTouchEnabled(false);
                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                mLayout.setTouchEnabled(true);
            }
        });
    }

    public void hideFrame() {
        frameLayout.setVisibility(View.GONE);
    }

    public void showFrame() {
        frameLayout.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onResume() {
        try {
            super.onResume();
            if (sharedPrefrencesManger != null) {
                if (sharedPrefrencesManger.getUserProfileImage() != null && !sharedPrefrencesManger.getUserProfileImage().isEmpty()) {
                    Picasso.with(mContext).
                            load(MyEndPointsInterface.imgBaseUrl + sharedPrefrencesManger.getUserProfileImage()).
                            networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE).memoryPolicy(MemoryPolicy.NO_CACHE)
                            .into(imgBg);
                }
//            if (sharedPrefrencesManger.getFullName() != null && !sharedPrefrencesManger.getFullName().isEmpty()) {
//                if (sharedPrefrencesManger.getFullName().length() < 13) {
//                    userName.setText(sharedPrefrencesManger.getFullName());
//                } else {
//                    userName.setText(sharedPrefrencesManger.getFullName().substring(0, 13));
//                }
//            }
                if (sharedPrefrencesManger.getUserProfileObject() != null) {
                    if (sharedPrefrencesManger.getUserProfileObject().getNickName() != null && !sharedPrefrencesManger.getUserProfileObject().getNickName().isEmpty()) {
                        userName.setText(sharedPrefrencesManger.getUserProfileObject().getNickName());

                    } else {
                        if (sharedPrefrencesManger.getUserProfileObject().getFullName() != null && !sharedPrefrencesManger.getUserProfileObject().getFullName().isEmpty()) {
                            userName.setText(sharedPrefrencesManger.getUserProfileObject().getFullName());

                        }
                    }
//            if (sharedPrefrencesManger.getFullName().length() < 13) {
//                user_name.setText(sharedPrefrencesManger.getFullName());
//            } else {
//                user_name.setText(sharedPrefrencesManger.getFullName().substring(0, 12) + "...");
//            }
                }

            }
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }
    }

    private void performDeepLinking(String screenName, String itemID) {
        if (screenName != null) {
            screenName = screenName.toLowerCase();
            if (itemID != null && itemID.length() > 0) {
                itemID = itemID.toLowerCase();
            }
            switch (screenName) {

                case "perks":
                    clickedItem = rewardsClicked;
                    usageTxtID.setText(getString(R.string.rewards_capital_header_spaced));
                    lv_main_swip.setBackground(getResources().getDrawable(R.drawable.support_bg_a));
                    collapseMenu();
                    isMenuOpen = true;
                    Bundle bundle2 = new Bundle();
                    Bundle rewardsBundle = new Bundle();
                    if (itemID != null && itemID.length() > 0) {
                        rewardsBundle.putString(ConstantUtils.KEY_NOTIFICATION_ITEM_ID, itemID);
                    }
                    bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_rewards_button);
                    bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_rewards_button);
                    mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_rewards_button, bundle2);
                    RewardsFragment rewardsFragment = new RewardsFragment();
                    rewardsFragment.setArguments(rewardsBundle);
                    replaceFragmnet(rewardsFragment, R.id.contentFrameLayout, true);
                    sharedPrefrencesManger.setNotificationIdForPopup("");
                    sharedPrefrencesManger.setNotificationTypeForPopup("");
                    sharedPrefrencesManger.setItemId("");
                    break;

                case "store":
                    clickedItem = storeClicked;
                    usageTxtID.setText(getString(R.string.store_capital_header_spaced));
//                lv_main_swip.setBackground(getResources().getDrawable(R.drawable.gra));
                    collapseMenu();
                    isMenuOpen = true;
                    bundle2 = new Bundle();

                    bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_store_button);
                    bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_store_button);
                    mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_store_button, bundle2);
                    replaceFragmnet(new MyStoreFragment(), R.id.contentFrameLayout, true);
                    sharedPrefrencesManger.setNotificationIdForPopup("");
                    sharedPrefrencesManger.setNotificationTypeForPopup("");
                    sharedPrefrencesManger.setItemId("");
                    break;

                case "transfercredit":
                    clickedItem = storeClicked;
                    usageTxtID.setText(getString(R.string.store_capital_header_spaced));
//                lv_main_swip.setBackground(getResources().getDrawable(R.drawable.gra));
                    collapseMenu();
                    isMenuOpen = true;
                    bundle2 = new Bundle();

                    bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_store_button);
                    bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_store_button);
                    mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_store_button, bundle2);
                    replaceFragmnet(new MyStoreFragment(), R.id.contentFrameLayout, true);
                    sharedPrefrencesManger.setNotificationIdForPopup("");
                    sharedPrefrencesManger.setNotificationTypeForPopup("");
                    sharedPrefrencesManger.setItemId("");
                    break;

                case "usage":
                    //already handled
                    break;

                case "invitefriend":
                    Bundle bundleInvite = new Bundle();

                    bundleInvite.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_invite_friend_button);
                    bundleInvite.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_invite_friend_button);
                    mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_invite_friend_button, bundleInvite);
                    startActivity(new Intent(getApplicationContext(), InviteFriendFragment.class));
                    break;

                case "events":
                    clickedItem = eventsClicked;
                    usageTxtID.setText(getString(R.string.events));
//                lv_main_swip.setBackground(getResources().getDrawable(R.drawable.gra));
                    collapseMenu();
                    isMenuOpen = true;
                    EventsMainFragment eventsMainFragment = new EventsMainFragment();
                    Bundle eventsBundle = new Bundle();
                    if (itemID != null && itemID.length() > 0) {
                        eventsBundle.putString(ConstantUtils.KEY_NOTIFICATION_ITEM_ID, itemID);
                    }
                    eventsMainFragment.setArguments(eventsBundle);
                    replaceFragmnet(eventsMainFragment, R.id.contentFrameLayout, true);
                    sharedPrefrencesManger.setNotificationIdForPopup("");
                    sharedPrefrencesManger.setNotificationTypeForPopup("");
                    sharedPrefrencesManger.setItemId("");
                    break;

                case "challenges":
                    clickedItem = challengesClicked;
                    usageTxtID.setText(getString(R.string.challenges));
//                lv_main_swip.setBackground(getResources().getDrawable(R.drawable.gra));
                    collapseMenu();
                    isMenuOpen = true;
//                    CommonMethods.logFirebaseEvent(mFirebaseAnalytics,ConstantUtils.TAGGING_CHALLENGES);
//                    replaceFragmnet(new ChallengesMainFragment(), R.id.contentFrameLayout, true);
                    replaceFragmnet(new ChallengesMainFragment(), R.id.contentFrameLayout, true);
                    sharedPrefrencesManger.setNotificationIdForPopup("");
                    sharedPrefrencesManger.setNotificationTypeForPopup("");
                    sharedPrefrencesManger.setItemId("");
                    break;

                case "wifihotspot":
                    if (UsageFragment.usageFragmentInsatnce != null) {
                        UsageFragment.setWifiHotspotPage();
                    }

                    break;

            }
        }
    }

    private boolean dataDiffForLIVPopup() {
        long diff = System.currentTimeMillis() - sharedPrefrencesManger.getLIVPopupDateTime();
        long diffDays = diff / (24 * 60 * 60 * 1000);

        return sharedPrefrencesManger.getLIVPopupDateTime() == 0 || diffDays >= 30;
    }
}
