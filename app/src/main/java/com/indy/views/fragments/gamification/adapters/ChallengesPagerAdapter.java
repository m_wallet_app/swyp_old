package com.indy.views.fragments.gamification.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.indy.views.fragments.gamification.challenges.BadgesFragment;
import com.indy.views.fragments.gamification.challenges.CheckInFragment;
import com.indy.views.fragments.gamification.challenges.RafflesFragment;

/**
 * Created by hadi on 23/9/17.
 */
public class ChallengesPagerAdapter extends FragmentStatePagerAdapter {
    private int mNumOfTabs;

    public ChallengesPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }


    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                BadgesFragment badgesFragment= new BadgesFragment();
               return badgesFragment;
            case 1:
                CheckInFragment checkInFragment= new CheckInFragment();
                return checkInFragment;
            case 2:
                RafflesFragment rafflesFragment= new RafflesFragment();
                return rafflesFragment;
        default:
            return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }


}

