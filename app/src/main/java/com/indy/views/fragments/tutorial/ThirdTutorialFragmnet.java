package com.indy.views.fragments.tutorial;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indy.R;
import com.indy.views.activites.TutorialActivity;
import com.indy.views.fragments.utils.MasterFragment;

/**
 * Created by emad on 8/28/16.
 */
public class ThirdTutorialFragmnet extends MasterFragment {
    View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            view = inflater.inflate(R.layout.fragment_third_tutorial, container, false);
            initUI();
        ((TutorialActivity) getActivity()).skipBtn.setVisibility(View.VISIBLE);
//        TutorialActivity.ll_fourth.setVisibility(View.INVISIBLE);
//        TutorialActivity.tv_content.setText(getString(R.string.track_your_usage_str));
//        TutorialActivity.tv_title.setText(getString(R.string.track_your_usage));
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();

//        ((TutorialActivity) getActivity()).changeBulletColor(2);

    }
}
