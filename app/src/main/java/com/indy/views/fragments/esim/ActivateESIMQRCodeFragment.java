package com.indy.views.fragments.esim;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.indy.R;
import com.indy.views.activites.AboutAppActivity;
import com.indy.views.activites.MasterActivity;
import com.indy.views.activites.SwpeMainActivity;
import com.indy.views.fragments.usage.PackagesFragments;
import com.indy.views.fragments.utils.MasterFragment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class ActivateESIMQRCodeFragment extends MasterFragment {

    private View baseView;
    private ImageView iv_esim_qrcode;
    private String activationCode = "";
    private String simDPAddress = "";
    private boolean fromSettings = false;
    private Button bt_lets_go, backImg, helpBtn;
    private TextView titleTxt, tv_esim_share_qr_code, tv_esim_learn_how_to;
    private MasterActivity mSwypeMainActivity;
    private RelativeLayout headerLayoutID, backLayout, helpLayout;
    private int headerTitle;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        baseView = inflater.inflate(R.layout.fragment_esim_activation_qrcode, null, false);
        initUI();
        return baseView;
    }

    @Override
    public void initUI() {
//        mSwypeMainActivity = (SwpeMainActivity) getActivity();
        super.initUI();
        initToolbar();
        if (getArguments() != null) {
            if (getArguments().containsKey("activationCode"))
                activationCode = getArguments().getString("activationCode");
            if (getArguments().containsKey("simDPAddress"))
                simDPAddress = getArguments().getString("simDPAddress");
            if (getArguments().containsKey("fromSettings"))
                fromSettings = getArguments().getBoolean("fromSettings");

        }
        if (fromSettings) {
            mSwypeMainActivity = (AboutAppActivity) getActivity();
        } else {
            mSwypeMainActivity = (SwpeMainActivity) getActivity();
        }

        iv_esim_qrcode = (ImageView) baseView.findViewById(R.id.iv_esim_qrcode);
        tv_esim_share_qr_code = (TextView) baseView.findViewById(R.id.tv_esim_share_qr_code);
        bt_lets_go = (Button) baseView.findViewById(R.id.bt_lets_go);
        if (fromSettings) {
            headerTitle = R.string.qr_code_for_esim;
            bt_lets_go.setVisibility(View.GONE);
        } else {
            headerTitle = R.string.activate_your_esim;
            bt_lets_go.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSwypeMainActivity.getSupportFragmentManager().popBackStack();
                    if (PackagesFragments.instance != null)
                        PackagesFragments.instance.refresh();

                }
            });
        }
        tv_esim_share_qr_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareQRCode();
            }
        });
        setupToolbar(headerTitle);
        generateQRCode();
    }

    private void generateQRCode() {

        if (fromSettings) {
            if (loadQRCodeFromStorage() != null) {
                iv_esim_qrcode.setImageBitmap(loadQRCodeFromStorage());
            }
        } else {
            QRCodeWriter writer = new QRCodeWriter();
            try {
                BitMatrix bitMatrix = writer.encode(getQRCodeContent(activationCode, simDPAddress), BarcodeFormat.QR_CODE, 512, 512);
                int width = bitMatrix.getWidth();
                int height = bitMatrix.getHeight();
                Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
                for (int x = 0; x < width; x++) {
                    for (int y = 0; y < height; y++) {
                        bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                    }
                }
                saveBitmapToCache(bmp);
                iv_esim_qrcode.setImageBitmap(bmp);

            } catch (WriterException e) {
                e.printStackTrace();
            }
        }
    }

    private String getQRCodeContent(String activationCode, String simDPAddress) {
        return "LPA:1$" + simDPAddress + "$" + activationCode;
    }

    private void initToolbar() {

        backImg = (Button) baseView.findViewById(R.id.backImg);
        backLayout = (RelativeLayout) baseView.findViewById(R.id.backLayout);
        helpLayout = (RelativeLayout) baseView.findViewById(R.id.helpLayout);
        headerLayoutID = (RelativeLayout) baseView.findViewById(R.id.headerLayoutID);
        helpBtn = (Button) baseView.findViewById(R.id.helpBtn);
        titleTxt = (TextView) baseView.findViewById(R.id.titleTxt);
        tv_esim_learn_how_to = (TextView) baseView.findViewById(R.id.tv_esim_learn_how_to);
        tv_esim_learn_how_to.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!fromSettings) {
                    LearnHowToActivateFragment learnHowToActivateFragment = new LearnHowToActivateFragment();
                    learnHowToActivateFragment.setMasterActivity(mSwypeMainActivity);
                    mSwypeMainActivity.replaceFragmnet(learnHowToActivateFragment, R.id.contentFrameLayout, true);
                } else {
                    LearnHowToActivateFragment learnHowToActivateFragment = new LearnHowToActivateFragment();
                    learnHowToActivateFragment.setMasterActivity(mSwypeMainActivity);
                    mSwypeMainActivity.replaceFragmnet(learnHowToActivateFragment, R.id.frameLayout, true);
                }
            }
        });
    }

    private void setupToolbar(int title) {
        titleTxt.setText(title);
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard();
                mSwypeMainActivity.onBackPressed();
//                Intent intent = new Intent(RegisterationActivity.this,TutorialActivity.class);
//                startActivity(intent);
            }
        });
//        helpBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(RegisterationActivity.this, HelpActivity.class);
//                startActivity(intent);
//            }
//        });
//        helpLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(RegisterationActivity.this, HelpActivity.class);
//                startActivity(intent);
//            }
//        });
        helpLayout.setVisibility(View.INVISIBLE);
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard();
                mSwypeMainActivity.onBackPressed();
            }
        });
    }

    private void saveBitmapToCache(Bitmap bitmap) {

        try {

            File cachePath = new File(mSwypeMainActivity.getCacheDir(), "images");
            cachePath.mkdirs(); // don't forget to make the directory
            FileOutputStream stream = new FileOutputStream(cachePath + "/QR_CODE" + ".png"); // overwrites this image every time
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            stream.close();
            sharedPrefrencesManger.setESIMBitmapCachedFlag(true);
        } catch (IOException e) {
            e.printStackTrace();
            sharedPrefrencesManger.setESIMBitmapCachedFlag(false);
        }

        /*
        ContextWrapper cw = new ContextWrapper(mSwypeMainActivity);

        File mypath = new File(getImageFilePath(getActivity()));
        if (mypath.exists()) {
            mypath.delete();
            mypath = new File(getImageFilePath(getActivity()));
        }
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
            sharedPrefrencesManger.setBitmapCachedFlag(true);
        } catch (Exception e) {
            sharedPrefrencesManger.setBitmapCachedFlag(false);
            Log.e("SAVE_IMAGE", e.getMessage(), e);
        }
        */
    }

    private Bitmap loadQRCodeFromStorage() {
        try {
            File imagePath = new File(mSwypeMainActivity.getCacheDir(), "images");
            File newFile = new File(imagePath, "QR_CODE.png");
            Uri contentUri = FileProvider.getUriForFile(mSwypeMainActivity, "com.indy.provider.fileprovider", newFile);
            InputStream ims = mSwypeMainActivity.getContentResolver().openInputStream(contentUri);
            // just display image in imageview
            return BitmapFactory.decodeStream(ims);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }

/*        try {
            ContextWrapper cw = new ContextWrapper(mSwypeMainActivity);
            File f = new File(getImageFilePath(getActivity()));
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            return b;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }*/

    }

    private void shareQRCode() {
        File imagePath = new File(mSwypeMainActivity.getCacheDir(), "images");
        File newFile = new File(imagePath, "QR_CODE.png");
        Uri contentUri = FileProvider.getUriForFile(mSwypeMainActivity, "com.indy.provider.fileprovider", newFile);

        if (contentUri != null) {
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION); // temp permission for receiving app to read this file
            shareIntent.setDataAndType(contentUri, mSwypeMainActivity.getContentResolver().getType(contentUri));
            shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri);
            startActivity(Intent.createChooser(shareIntent, "Choose an app"));
        }
    }

    private String getImageFilePath(Context context) {
        final File dir = context.getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "Android-" + "QR_CODE" + ".jpg";
    }

}


