package com.indy.views.fragments.gamification.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.BaseServiceManger;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.gamification.models.serviceInputOutput.DoCheckInInputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetCheckinLocationsOutputModel;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**

 */
public class GetCheckInLocationsListService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private DoCheckInInputModel doCheckInInputModel;


    public GetCheckInLocationsListService(BaseInterface baseInterface, DoCheckInInputModel doCheckInInputModel) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.doCheckInInputModel = doCheckInInputModel;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<GetCheckinLocationsOutputModel> call = apiService.getCheckinLocationsList(doCheckInInputModel);
        call.enqueue(new Callback<GetCheckinLocationsOutputModel>() {
            @Override
            public void onResponse(Call<GetCheckinLocationsOutputModel> call, Response<GetCheckinLocationsOutputModel> response) {
                mBaseResponseModel.setServiceType(ServiceUtils.getCheckinLocationsService);
                mBaseResponseModel.setResultObj(response.body());

                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (response.body().getErrorMsgEn() != null) {
//                        String urlForTag = call.request().url().toString();
//                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
//                        urlForTag = urlForTag.replace("/", "_");
//                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<GetCheckinLocationsOutputModel> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.getCheckinLocationsService);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
