package com.indy.views.fragments.gamification.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.rewards.RewardsList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by emad on 9/27/16.
 */

public class EventsList implements Parcelable {




    @SerializedName("active")
    @Expose
    private boolean active;

    ///////////////////////




    @SerializedName("titleEn")
    @Expose
    private String nameEn;
    @SerializedName("titleAr")
    @Expose
    private String nameAr;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;

    @SerializedName("descriptionEn")
    @Expose
    private String offerDescEn;
    @SerializedName("descriptionAr")
    @Expose
    private String offerDescAr;
    @SerializedName("indyEventId")
    @Expose
    private String indyEventId;

    @SerializedName("eventURL")
    @Expose
    private String eventURL;

    private List<EventPhoto> eventPhotosList;
    private EventPhoto userUploadedPhoto;

    private String eventVenueEn;
    private String eventVenueAr;

    @SerializedName("location")
    @Expose
    private String location;

    private int selectedMarker;
    private int tempPosition;
    @SerializedName("endsOnDate")
    @Expose
    private String date;

    @SerializedName("buyButtonURL")
    @Expose
    private String buyButtonURL;

    @SerializedName("buyButtonText")
    @Expose
    private String buyButtonText;

    @SerializedName("buyButtonEnabled")
    @Expose
    private boolean buyButtonEnabled;

    @SerializedName("uploadButtonEnabled")
    @Expose
    private boolean uploadButtonEnabled;

    @SerializedName("buyButtonVisible")
    @Expose
    private boolean buyButtonVisible;

    @SerializedName("tournamentActualDate")
    @Expose
    private String tournamentActualDate;

    @SerializedName("detailImageUrl")
    @Expose
    private String detailImageUrl;

    @SerializedName("isOngoingEvent")
    @Expose
    private boolean isOngoingEvent;
    public EventsList() {
        this.setNameEn("Test1");
    }
    private boolean isActive;
    public EventsList(Parcel in) {
        nameEn = in.readString();
        nameAr = in.readString();
        imageUrl = in.readString();

        offerDescEn = in.readString();
        offerDescAr = in.readString();
        indyEventId = in.readString();
        date = in.readString();
        eventVenueEn = in.readString();
        eventVenueAr = in.readString();
        eventPhotosList = new ArrayList<>();
        location = in.readString();
        buyButtonURL = in.readString();
        buyButtonText = in.readString();
        int result = in.readInt();
        buyButtonEnabled = result == 1;
        int result2 = in.readInt();
        uploadButtonEnabled = result2 ==1;
//        in.readTypedList(eventPhotosList, EventPhoto.CREATOR);
        int result3 = in.readInt();
        buyButtonEnabled = result3 == 1;
        tournamentActualDate = in.readString();
        this.detailImageUrl = ((String) in.readValue(String.class.getClassLoader()));
        int dateCheck = ((int) in.readValue((int.class.getClassLoader())));
        if(dateCheck==0){
            this.isActive = false;
        }else{
            this.isActive = true;
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nameEn);
        dest.writeString(nameAr);
        dest.writeString(imageUrl);

        dest.writeString(offerDescEn);
        dest.writeString(offerDescAr);

        dest.writeString(indyEventId);
        dest.writeString(date);
        dest.writeString(eventVenueEn);
        dest.writeString(eventVenueAr);
        dest.writeString(location);
        dest.writeString(buyButtonURL);
        dest.writeString(buyButtonText);
        if(buyButtonEnabled) {
            dest.writeInt(1);
        }else{
            dest.writeInt(0);
        }

        if(uploadButtonEnabled) {
            dest.writeInt(1);
        }else{
            dest.writeInt(0);
        }

        if(buyButtonVisible){
            dest.writeInt(1);
        }else{
            dest.writeInt(0);
        }
        dest.writeString(tournamentActualDate);

        dest.writeValue(detailImageUrl);
        if(isActive){
            dest.writeValue(1);
        }else{
            dest.writeValue(0);
        }
//        if(isOngoingEvent){
//            dest.writeValue(1);
//        }else{
//            dest.writeValue(0);
//
//        }
//        dest.writeList(eventPhotosList);
    }

    public EventsList(RewardsList item) {
        this.imageUrl = item.getImageUrl();
        this.nameAr = item.getNameAr();
        this.nameEn = item.getNameEn();
        this.offerDescEn = item.getOfferDescEn();
        this.offerDescAr = item.getOfferDescAr();

        this.indyEventId = item.getCategoryId();


    }

    public String getTournamentActualDate() {
        return tournamentActualDate;
    }

    public void setTournamentActualDate(String tournamentActualDate) {
        this.tournamentActualDate = tournamentActualDate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public static final Creator<EventsList> CREATOR = new Creator<EventsList>() {
        @Override
        public EventsList createFromParcel(Parcel in) {
            return new EventsList(in);
        }

        @Override
        public EventsList[] newArray(int size) {
            return new EventsList[size];
        }
    };

    /**
     * @return The nameEn
     */
    public String getNameEn() {
        return nameEn;
    }

    /**
     * @param nameEn The nameEn
     */
    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    /**
     * @return The nameAr
     */
    public String getNameAr() {
        return nameAr;
    }

    /**
     * @param nameAr The nameAr
     */
    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }

    /**
     * @return The price
     */

    /**
     * @return The imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * @param imageUrl The imageUrl
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     * @return The storeLocations
     */

    @Override
    public int describeContents() {
        return 0;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getIndyEventId() {
        return indyEventId;
    }

    public void setIndyEventId(String indyEventId) {
        this.indyEventId = indyEventId;
    }

    public List<EventPhoto> getEventPhotosList() {
        return eventPhotosList;
    }

    public void setEventPhotosList(List<EventPhoto> eventPhotosList) {
        this.eventPhotosList = eventPhotosList;
    }

    public EventPhoto getUserUploadedPhoto() {
        return userUploadedPhoto;
    }

    public void setUserUploadedPhoto(EventPhoto userUploadedPhoto) {
        this.userUploadedPhoto = userUploadedPhoto;
    }

    public String getOfferDescEn() {
        return offerDescEn;
    }

    public void setOfferDescEn(String offerDescEn) {
        this.offerDescEn = offerDescEn;
    }

    public String getOfferDescAr() {
        return offerDescAr;
    }

    public void setOfferDescAr(String offerDescAr) {
        this.offerDescAr = offerDescAr;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getEventVenueEn() {
        return eventVenueEn;
    }

    public String getEventVenueAr(){
        return eventVenueAr;
    }

    public void setEventVenueEn(String eventVenue) {
        this.eventVenueEn = eventVenue;
    }

    public void setEventVenueAr(String eventVenueAr) {
        this.eventVenueAr = eventVenueAr;
    }

    public String getBuyButtonURL() {
        return buyButtonURL;
    }

    public void setBuyButtonURL(String buyButtonURL) {
        this.buyButtonURL = buyButtonURL;
    }

    public String getBuyButtonText() {
        return buyButtonText;
    }

    public void setBuyButtonText(String buyButtonText) {
        this.buyButtonText = buyButtonText;
    }

    public boolean isBuyButtonEnabled() {
        return buyButtonEnabled;
    }

    public void setBuyButtonEnabled(boolean buyButtonEnabled) {
        this.buyButtonEnabled = buyButtonEnabled;
    }

    public boolean isUploadButtonEnabled() {
        return uploadButtonEnabled;
    }

    public void setUploadButtonEnabled(boolean uploadButtonEnabled) {
        this.uploadButtonEnabled = uploadButtonEnabled;
    }

    public boolean isBuyButtonVisible() {

        return buyButtonVisible;
    }

    public void setBuyButtonVisible(boolean buyButtonVisible) {
        this.buyButtonVisible = buyButtonVisible;
    }

    public String getDetailImageUrl() {
        return detailImageUrl;
    }

    public void setDetailImageUrl(String detailImageUrl) {
        this.detailImageUrl = detailImageUrl;
    }

    public boolean isOngoingEvent() {
        return isOngoingEvent;
    }

    public void setOngoingEvent(boolean ongoingEvent) {
        isOngoingEvent = ongoingEvent;
    }
}
