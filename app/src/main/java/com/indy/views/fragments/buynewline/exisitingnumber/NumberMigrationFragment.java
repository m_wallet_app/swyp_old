package com.indy.views.fragments.buynewline.exisitingnumber;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.controls.ServiceUtils;
import com.indy.customviews.CustomEditText;
import com.indy.customviews.MaskedEditText;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.verfiymsisdnusingeid.VerfyMsisidnUsingEidInput;
import com.indy.models.verfiymsisdnusingeid.VerfyMsisidnUsingEidOutput;
import com.indy.services.LogApplicationFlowService;
import com.indy.services.VerfiyMsisdByEidService;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.buynewline.ocr_flow.GetNodIDFragment;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.indy.views.fragments.utils.MasterFragment;

import java.util.ArrayList;

import static com.indy.views.activites.RegisterationActivity.logEventInputModel;

/**
 * Created by emad on 9/25/16.
 */

public class NumberMigrationFragment extends MasterFragment {
    private MaskedEditText eIDTxt;
    private Button verfyBtn;
    private View view;
    private VerfyMsisidnUsingEidInput verfyMsisidnUsingEidInput;
    private VerfyMsisidnUsingEidOutput verfyMsisidnUsingEidOutput;
    private int status;
    private TextView tv_title;
    public static String emiratedID;
    public static String migrationMobileNumber="";
    String mobileNumber = "";
    public static NumberMigrationFragment numberMigrationFragmentInstance;
    private CustomEditText code1, code2, code3, code4;
    public int NO_OF_DIGITS = 15;
    int[] tvListIds = {R.id.code_1, R.id.code_2, R.id.code_3, R.id.code_4};
    ArrayList<EditText> tv_digits;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_number_migration, container, false);
        initUI();
        setListeners();

        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
        regActivity.setHeaderTitle("");
        regActivity.showHeaderLayout();
        eIDTxt = (MaskedEditText) view.findViewById(R.id.eidtxt);
        verfyBtn = (Button) view.findViewById(R.id.verfyBtn);
        numberMigrationFragmentInstance = this;
        tv_title = (TextView) view.findViewById(R.id.title_number_migration);
        String textToReplace = tv_title.getText().toString().trim();
        code1 = (CustomEditText) view.findViewById(R.id.code_1);
        code2 = (CustomEditText) view.findViewById(R.id.code_2);
        code3 = (CustomEditText) view.findViewById(R.id.code_3);
        code4 = (CustomEditText) view.findViewById(R.id.code_4);

        if (getArguments() != null && getArguments().getString(ConstantUtils.mobileNo) != null) {
            mobileNumber = getArguments().getString(ConstantUtils.mobileNo);

            textToReplace = textToReplace + "\n" + mobileNumber;
            tv_title.setText(textToReplace);

        }
        tv_digits = new ArrayList<EditText>();
        for (int i = 0; i < tvListIds.length; i++) {
            EditText et_temp = (EditText) view.findViewById(tvListIds[i]);
            et_temp.addTextChangedListener(new CustomTextWatcher(et_temp));
            tv_digits.add(et_temp);
        }
        enableVerifyButton();
    }
    private String getCode(boolean forService) {
        String codeEntered = "";
        if(forService){
            codeEntered += code1.getText().toString();
            codeEntered +="-";
            codeEntered += code2.getText().toString();
            codeEntered +="-";
            codeEntered += code3.getText().toString();
            codeEntered +="-";
            codeEntered += code4.getText().toString();
        }else {
            codeEntered += code1.getText().toString();
            codeEntered += code2.getText().toString();
            codeEntered += code3.getText().toString();
            codeEntered += code4.getText().toString();
        }
        return codeEntered;
    }
    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            if (isVisible()) {
                if(responseModel!=null && responseModel.getServiceType()!=null && responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.LOG_USER_EVENT)) {
                }else{
                    regActivity.onBackPressed();
                    if (responseModel != null && responseModel.getResultObj() != null) {
                        verfyMsisidnUsingEidOutput = (VerfyMsisidnUsingEidOutput) responseModel.getResultObj();
                        if (verfyMsisidnUsingEidOutput != null)
                            if (verfyMsisidnUsingEidOutput.getErrorCode() != null)
                                onNumberMigrationFail();
                            else
                                onNumberMigrationSucess();
                    }
                }
            }
        }catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        try {
            if (isVisible() && regActivity != null) {
                if(responseModel!=null && responseModel.getServiceType()!=null && responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.LOG_USER_EVENT)) {
                }else{
                    regActivity.onBackPressed();
                }
            }
        }catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

    }

    private void showLoadingFragment() {
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.loadingString, getString(R.string.verifying_eid));
        LoadingFragmnet loadingFragmnet = new LoadingFragmnet();
        loadingFragmnet.setArguments(bundle);
        regActivity.addFragmnet(loadingFragmnet, R.id.loadingFragmeLayout, true);
    }

    private void showErrorFragment(String error) {
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, error);
        ErrorFragment errorFragment = new ErrorFragment();
        errorFragment.setArguments(bundle);
        regActivity.addFragmnet(errorFragment, R.id.frameLayout, true);
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        showLoadingFragment();
        logEvent();
        verfyMsisidnUsingEidInput = new VerfyMsisidnUsingEidInput();
        verfyMsisidnUsingEidInput.setAppVersion(appVersion);
        verfyMsisidnUsingEidInput.setToken(token);
        verfyMsisidnUsingEidInput.setOsVersion(osVersion);
        verfyMsisidnUsingEidInput.setChannel(channel);
        verfyMsisidnUsingEidInput.setDeviceId(deviceId);
        verfyMsisidnUsingEidInput.setMsisdn(getArguments().getString(ConstantUtils.mobileNo));
        verfyMsisidnUsingEidInput.setEmiratesId(getCode(true));
        verfyMsisidnUsingEidInput.setLang(currentLanguage);
        verfyMsisidnUsingEidInput.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        new VerfiyMsisdByEidService(this, verfyMsisidnUsingEidInput);
    }

    private boolean isValid() {
//        if (eIDTxt.getText().toString().trim().length() == 0)
//            return false;
//        else
        if(getCode(false).length()!=NO_OF_DIGITS){
            return false;
        }
        else
            return true;
    }

    private void setListeners() {
        verfyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPrefrencesManger.setIsFromMigration(true);
                migrationMobileNumber = mobileNumber;
                regActivity.replaceFragmnet(new GetNodIDFragment(),R.id.frameLayout,true);
//                if (isValid()) {
////                    onNotEligableDueToPaidAmount();
////                }
//                    Bundle bundle2 = new Bundle();
//
//                    bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_migration_eid_entered);
//                    bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_migration_eid_entered);
//                    mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_migration_eid_entered, bundle2);
//                    onConsumeService();
////                    Toast.makeText(getActivity(), "Code is:"+ getCode(true), Toast.LENGTH_SHORT).show();
//                } else {
//                    showErrorFragment(getString(R.string.EID_NO));
//                }
            }
        });

        eIDTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (eIDTxt.getText().toString().trim().length() > 0) {
                    enableVerifyButton();
                } else {
                    disableVerifyButton();
                }
//                if (eIDTxt.getText().toString().trim().length() == 3 || eIDTxt.getText().toString().trim().length() == 8 || eIDTxt.getText().toString().trim().length() == 16) {
//                    eIDTxt.setText(eIDTxt.getText().toString().trim() + ('-'));
//
//
//                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (eIDTxt.getText().toString().trim().length() == 3 || eIDTxt.getText().toString().trim().length() == 8 || eIDTxt.getText().toString().trim().length() == 16) {
                    // eIDTxt.setText(editable.append("-").toString());
                    editable.append("-");

                }
            }
        });
    }

    private void onNumberMigrationSucess() {
        try {
            status = verfyMsisidnUsingEidOutput.getStatus();
            emiratedID = getCode(true);

            switch (status) {
                case 1: // eligable
                    onEligable();
                    break;
                case 2: // not eligable due to un paid amount
                    onNotEligableDueToPaidAmount();
                    break;
                case 3: // Not eligible due to migrate to Indy
                    onNotEligableDueToMigrateIndy();
                    break;
                case 4: //Failure
                    Bundle bundle2 = new Bundle();

                    bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_migration_invalid_eid_number);
                    bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_migration_invalid_eid_number);
                    mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_migration_invalid_eid_number, bundle2);
                    onInValidEidResponse();
                    break;
                case 6:
                    onAlreadyCustomerResponse();
                    break;
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void onAlreadyCustomerResponse(){



        regActivity.replaceFragmnet(new RegisteredNonEmirateIDDocumentFragment(), R.id.frameLayout, true);

    }

    private void onInValidEidResponse() {
        InvalidEIDFrgament invalidEIDFrgament = new InvalidEIDFrgament();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.eidNO, getCode(false));
        bundle.putString(ConstantUtils.mobileNo,mobileNumber);
        invalidEIDFrgament.setArguments(bundle);
        regActivity.replaceFragmnet(invalidEIDFrgament, R.id.frameLayout, true);

    }

    private void onNotEligableDueToMigrateIndy() {
        Fragment CoorporateNumberMigrationFragment = new CoorporateNumberMigrationFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.mobileNo, mobileNumber);
        CoorporateNumberMigrationFragment.setArguments(bundle);
        regActivity.replaceFragmnet(CoorporateNumberMigrationFragment, R.id.frameLayout, true);

    }

    private void onNotEligableDueToPaidAmount() {
        Bundle bundle = new Bundle();
        bundle.putString("AMOUNT", String.valueOf(verfyMsisidnUsingEidOutput.getAmount()));
        Fragment pendingFragmmnet = new PendingPaymentFragment();
        pendingFragmmnet.setArguments(bundle);
        regActivity.replaceFragmnet(pendingFragmmnet, R.id.frameLayout, true);
    }

    private void onEligable() {
        Bundle bundle2 = new Bundle();

        bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_migration_valid_eid_entered);
        bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_migration_valid_eid_entered);
        mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_migration_valid_eid_entered, bundle2);
        EligableNumberMigrationFragment eligableNumberMigrationFragment = new EligableNumberMigrationFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.mobileNo, mobileNumber);
        eligableNumberMigrationFragment.setArguments(bundle);
        regActivity.replaceFragmnet(eligableNumberMigrationFragment, R.id.frameLayout, true);
    }


    private void onNumberMigrationFail() {
        if(currentLanguage.equalsIgnoreCase("en"))
            showErrorFragment(verfyMsisidnUsingEidOutput.getErrorMsgEn());
        else
            showErrorFragment(verfyMsisidnUsingEidOutput.getErrorMsgAr());

//        ((RegisterationActivity) getActivity()).replaceFragmnet(new RegisteredNonEmirateIDDocumentFragment(), R.id.frameLayout, true);

    }


    private void enableVerifyButton() {
        verfyBtn.setAlpha(1.0f);
        verfyBtn.setEnabled(true);
    }

    private void disableVerifyButton() {
        verfyBtn.setAlpha(0.5f);
        verfyBtn.setEnabled(false);
    }


    public class CustomTextWatcher implements TextWatcher {
        private EditText view;

        private CustomTextWatcher(EditText view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

            String text = editable.toString();
            if ((tv_digits.indexOf(view) ==0) && text.length() == 3) {
                moveToNext();
            }else if((tv_digits.indexOf(view) ==1) && text.length() == 4){
                moveToNext();
            }else if((tv_digits.indexOf(view) ==2) && text.length() == 7){
                moveToNext();
            }else if((tv_digits.indexOf(view) ==3) && text.length() == 1){
                moveToNext();
            }
            else
                disableVerifyButton();
        }

        public void moveToNext(){
            view.clearFocus();

            int currentIndex = tv_digits.indexOf(view);
            if(getCode(false).length()==NO_OF_DIGITS) {
                enableVerifyButton();
            }
            if (currentIndex == 3) {
                hideKeyBoard();
                if(getCode(false).length()==NO_OF_DIGITS) {
                    enableVerifyButton();
                }
            } else {
                for(int i = currentIndex + 1; i < tv_digits.size(); i++) {
                    tv_digits.get(i).requestFocus();

                    return;

                }
            }
        }
    }
    private void logEvent() {

       logEventInputModel.setScreenId(ConstantUtils.SCREEN_ID_3003);

        logEventInputModel.setEmiratesId(getCode(true));

        new LogApplicationFlowService(this, logEventInputModel);
    }
}
