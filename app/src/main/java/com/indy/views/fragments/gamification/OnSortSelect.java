package com.indy.views.fragments.gamification;

/**
 * Created by Tohamy on 10/2/2017.
 */

public interface OnSortSelect {
    void selectSort(String sortValue, int index);
}
