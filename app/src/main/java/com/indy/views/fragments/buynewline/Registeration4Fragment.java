package com.indy.views.fragments.buynewline;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import com.indy.R;
import com.indy.controls.DateSelectInterface;
import com.indy.helpers.DateHelpers;
import com.indy.services.LogApplicationFlowService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.RegisterationActivity;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.MasterFragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static com.indy.views.activites.RegisterationActivity.logEventInputModel;

/**
 * Created by emad on 9/7/16.
 */
public class Registeration4Fragment extends MasterFragment {
    private View view;
    private Button dayBtn, monthBtn, yearBtn;
    TextView nextBtn;
    Animation dayAnim, monthAnim, yearAnim;
    public static DateSelectInterface dateSelectInterface;
    DateHelpers dateUtils;
    private int yearInt, monthInt, dayInt = 0;
    private String userName, email;
    private TextView daysSpinner, monthsSpinner, yearSpinner;
    private TextView dayTxtView, monthTxtView, yearTxtView;
    public static int dayVal, monthVal, yearVal = 0;
    public static String year;
    private double diffYears = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(view==null)
        view = inflater.inflate(R.layout.fragment_4_registeration, container, false);
        initUI();
//        loadDaysSpinner();
//        loadMonthsSpinner();
//        loadYearsSpinner();

        return view;
    }

    @Override
    public void initUI() {

        super.initUI();
        try {
            regActivity.showHeaderLayout();
            regActivity.setHeaderTitle("");

            dateUtils = new DateHelpers();
            yearInt = 0;
            monthInt = 0;
            dayInt = 0;
            dayBtn = (Button) view.findViewById(R.id.dayBtn);
            monthBtn = (Button) view.findViewById(R.id.monthBtn);
            yearBtn = (Button) view.findViewById(R.id.yearBtn);
            nextBtn = (TextView) view.findViewById(R.id.nextBtn);
            dayTxtView = (TextView) view.findViewById(R.id.dayTxt);
            monthTxtView = (TextView) view.findViewById(R.id.monthTxt);
            yearTxtView = (TextView) view.findViewById(R.id.yearTxt);
            userName = getArguments().getString(ConstantUtils.userName);
            email = getArguments().getString(ConstantUtils.email);

            monthsSpinner = (TextView) view.findViewById(R.id.monthSpin);
            daysSpinner = (TextView) view.findViewById(R.id.daySpin);
            yearSpinner = (TextView) view.findViewById(R.id.yearSpin);


            dayAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.zoom_anim);
            monthAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.zoom_anim);
            yearAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.zoom_anim);
            ((RegisterationActivity) getActivity()).showBackBtn();
            loadAnimation();
            disableNextBtn();
            daysSpinner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                DialogFragment newFragment = new DatePickerFragment();
//
//
//                newFragment.show(getActivity().getFragmentManager(), "datePicker");
                    showCalendar();
                }
            });
            monthsSpinner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                DialogFragment newFragment = new DatePickerFragment();
//                newFragment.show(getActivity().getFragmentManager(), "datePicker");
                    showCalendar();

                }
            });
            yearSpinner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                DialogFragment newFragment = new DatePickerFragment();
//                newFragment.show(getActivity().getFragmentManager(), "datePicker");
                    showCalendar();

                }
            });
            nextBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        hideKeyBoard();
//                long diffYears = dateUtils.validateDates(dateUtils.getCurrentDate(), getSelectedDate());

//                    dateUtils.getDiffYears(getSelectedDateForDifference(), dateUtils.getCurrentDateFor());
                        if (diffYears >= 30 || diffYears < 18) {
                            showErrorFragment(getString(R.string.ilgble_age));
                        } else {
//                    ((RegisterationActivity) getActivity()).replaceFragmnet(new Greater24Fragment(), R.id.frameLayout, true);
//                } else {
                            TermsAndConditionsFragment termsAndConditionsFragment = new TermsAndConditionsFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString(ConstantUtils.userName, userName);
                            bundle.putString(ConstantUtils.email, email);
                            bundle.putString(ConstantUtils.birthdate, getSelectedDate());
                            bundle.putString(ConstantUtils.newAccountMobileNo, getArguments().getString(ConstantUtils.newAccountMobileNo));

                            termsAndConditionsFragment.setArguments(bundle);

                            CommonMethods.logFirebaseEvent(mFirebaseAnalytics, ConstantUtils.TAGGING_dob_entered);
                            logEvent();
                            regActivity.replaceFragmnet(termsAndConditionsFragment, R.id.frameLayout, true);
                        }
//                }
                    } catch (Exception ex) {
                        CommonMethods.logException(ex);
                    }
                }

            });
        }catch (Exception ex){
            CommonMethods.logException(ex);
        }
    }

    private void loadAnimation() {
        dayBtn.startAnimation(dayAnim);
        dayAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                monthBtn.setVisibility(View.VISIBLE);
                monthBtn.startAnimation(monthAnim);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        monthAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                yearBtn.setVisibility(View.VISIBLE);
                yearBtn.startAnimation(yearAnim);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public void showDatePickerFragment() {

    }


    public void onDateSelect(String day, String month, String year) {
        yearSpinner.setText(year);
        try {
            int monthValTemp = Integer.parseInt(month);
            monthValTemp += 1;
            monthsSpinner.setText(monthValTemp + "");
        } catch (Exception ex) {
            monthsSpinner.setText(month);
        }
        daysSpinner.setText(day);
        yearInt = Integer.valueOf(year);
        monthInt = Integer.valueOf(month);
        dayInt = Integer.valueOf(day);
        dayTxtView.setVisibility(View.VISIBLE);
        monthTxtView.setVisibility(View.VISIBLE);
        yearTxtView.setVisibility(View.VISIBLE);
        enableNextBtn();

    }

    private Date getSelectedDateForDifference(int day, int month, int year) {
        Calendar c = Calendar.getInstance();
        c.set(year, month + 1, day);
        return c.getTime();
    }

    private String getSelectedDate() {
        Calendar c = Calendar.getInstance();
        c.set(yearInt, monthInt - 1, dayInt);
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        String formattedDate = df.format(c.getTime());
        Log.v("selecteday", formattedDate + "  " + yearInt + " " + yearInt + " " + dayInt + " " + (monthInt - 1));
        return formattedDate;
    }

    private boolean isBirthValid() {
        boolean isvlaid = true;
        if (dayInt == 0 || yearInt == 0 || monthInt == 0) {
            isvlaid = false;
        }
        return isvlaid;
    }

    private void enableNextBtn() {
        nextBtn.setAlpha(ConstantUtils.ALPHA_BUTTON_ENABLED);
        nextBtn.setClickable(true);
        nextBtn.setEnabled(true);
    }

    private void disableNextBtn() {
        nextBtn.setAlpha(.5f);
        nextBtn.setClickable(false);
        nextBtn.setEnabled(false);
    }

    public void showErrorFragment(String error) {
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();

        bundle.putString(ConstantUtils.errorString, error);
        errorFragment.setArguments(bundle);
        regActivity.addFragmnet(errorFragment, R.id.frameLayout, true);
    }

//    private void checkNextStaus() {
//        if (dayVal == 0 || monthVal == 0 || yearVal == 0)
//            disableNextBtn();
//        else
//            enableNextBtn();
//
//    }

    public void showCalendar() {
        try {
            final Dialog dialog = new Dialog(getActivity());
            dialog.setContentView(R.layout.testing);

            final DatePicker datePicker = (DatePicker) dialog.findViewById(R.id.datePicker);
            Calendar cal1 = Calendar.getInstance();
            cal1 = Calendar.getInstance();

            cal1.setTimeInMillis(new Date().getTime());
            cal1.add(Calendar.YEAR, -18);
//        cal1.add(Calendar.DAY_OF_MONTH,-1);
            datePicker.setMaxDate(cal1.getTimeInMillis());
            cal1.setTime(new Date());
            cal1.add(Calendar.DAY_OF_MONTH, 1);
            cal1.add(Calendar.YEAR, -30);
            datePicker.setMinDate(cal1.getTimeInMillis());

            Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
            // if button is clicked, close the custom dialog
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int day = datePicker.getDayOfMonth();
                    int month = datePicker.getMonth();
                    int year = datePicker.getYear();
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(year, month, day);
                    SimpleDateFormat dateFormatter;

                    DateHelpers dateHelpers = new DateHelpers();
//                long diffYears = dateHelpers.validateDates(dateUtils.getCurrentDate(), getSelectedDate());

                    diffYears = dateUtils.getDiffYears(newDate.getTime(), dateUtils.getCurrentDateFor());
                    if (diffYears >= 30 || diffYears < 18) {
                        disableNextBtn();
//                    showErrorFragment(getString(R.string.ilgble_age));
                    } else {
//                    enableNextBtn();
                        dialog.dismiss();
                        onDateSelect(day + "", month + "", year + "");
                    }
                }
            });

            Button dialogCancelButton = (Button) dialog.findViewById(R.id.dialogButtonCancel);
            dialogCancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.cancel();
                }
            });

            dialog.show();
         }catch (Exception ex){
            CommonMethods.logException(ex);
        }
    }

    private void logEvent() {


        logEventInputModel.setActionTransactionId(sharedPrefrencesManger.getTempAppId());
        logEventInputModel.setScreenId(ConstantUtils.SCREEN_ID_0003);

        logEventInputModel.setDay(daysSpinner.getText().toString());
        logEventInputModel.setMonth(monthsSpinner.getText().toString());
        logEventInputModel.setYear(yearSpinner.getText().toString());

        new LogApplicationFlowService(this,logEventInputModel);

    }

}
