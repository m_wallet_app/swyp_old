package com.indy.views.fragments.AboutUs;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.AboutAppActivity;
import com.indy.views.activites.HelpActivity;
import com.indy.views.activites.MasterActivity;

/**
 * Created by Amir.jehangir on 10/10/2016.
 */
public class About_TermsAndConditions extends MasterActivity {
    private View view;
    TextView versionname;
    TextView termsandconditionstv, user_name, lastUpdatetxt;
    About_TermsAndConditions about_terem;
    AboutAppActivity aboutActivity;
    private Button backImg, helpBtn;
    TextView title;
    RelativeLayout backLayout, helpLayout;

//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        view = inflater.inflate(R.layout.fragment_about_termsandconditions, container, false);
//        view.bringToFront();
//        initUI();
//        return view;
//    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_about_termsandconditions);
        initUI();
    }

    @Override
    public void initUI() {
        super.initUI();
//        aboutActivity = (AboutAppActivity) getActivity();
        versionname = (TextView) findViewById(R.id.versionname);
        user_name = (TextView) findViewById(R.id.user_balance_amount);
        lastUpdatetxt = (TextView) findViewById(R.id.tv_last_updated);
        backLayout = (RelativeLayout) findViewById(R.id.backLayout);
        helpLayout = (RelativeLayout) findViewById(R.id.helpLayout);

        lastUpdatetxt.setVisibility(View.GONE);
        title = (TextView) findViewById(R.id.titleTxt);
        title.setText(R.string.about_us_title);
        backImg = (Button) findViewById(R.id.backImg);
        helpBtn = (Button) findViewById(R.id.helpBtn);
        helpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(About_TermsAndConditions.this, HelpActivity.class);
                startActivity(intent);
            }
        });
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        helpLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(About_TermsAndConditions.this, HelpActivity.class);
                startActivity(intent);
            }
        });

        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

//        versionname.setText(getString(R.string.version) + sharedPrefrencesManger.getAppVersion());
        user_name.setText(getString(R.string.about_us));
        termsandconditionstv = (TextView) findViewById(R.id.termsandconditionstv);
//        aboutActivity = (AboutAppActivity) getActivity();
        termsandconditionstv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayNextFragment();
            }
        });
        helpBtn.setVisibility(View.INVISIBLE);
    }


    private void displayNextFragment() {
//        aboutActivity.replaceFragmnet(new TermsAndConditionsWebView(), R.id.frameLayout, true);
        Bundle bundle2 = new Bundle();

        bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_settings_about_terms_conditions);
        bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_settings_about_terms_conditions);
        mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_settings_about_terms_conditions, bundle2);
        Intent intent = new Intent(About_TermsAndConditions.this, TermsAndConditionsWebView.class);
        startActivity(intent);

    }
}
