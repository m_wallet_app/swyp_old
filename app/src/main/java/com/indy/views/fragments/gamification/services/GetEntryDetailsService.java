package com.indy.views.fragments.gamification.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.BaseServiceManger;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetEntryDetailsInputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetEntryDetailsOutputModel;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**

 */
public class GetEntryDetailsService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private GetEntryDetailsInputModel getEntryDetailsInputModel;


    public GetEntryDetailsService(BaseInterface baseInterface, GetEntryDetailsInputModel getEntryDetailsInputModel) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.getEntryDetailsInputModel = getEntryDetailsInputModel;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<GetEntryDetailsOutputModel> call = apiService.getEntryDetails(getEntryDetailsInputModel);
        call.enqueue(new Callback<GetEntryDetailsOutputModel>() {
            @Override
            public void onResponse(Call<GetEntryDetailsOutputModel> call, Response<GetEntryDetailsOutputModel> response) {
                mBaseResponseModel.setServiceType(ServiceUtils.getEntryDetailsService);
                mBaseResponseModel.setResultObj(response.body());

                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (response.body().getErrorMsgEn() != null) {
//                        String urlForTag = call.request().url().toString();
//                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
//                        urlForTag = urlForTag.replace("/", "_");
//                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<GetEntryDetailsOutputModel> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.getEntryDetailsService);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
