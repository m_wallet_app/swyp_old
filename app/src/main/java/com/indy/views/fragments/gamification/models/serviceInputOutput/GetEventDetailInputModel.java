package com.indy.views.fragments.gamification.models.serviceInputOutput;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mobile on 24/09/2017.
 */

public class GetEventDetailInputModel extends GamificationBaseInputModel {


    @SerializedName("indyEventId")
    @Expose
    private String indyEventId;
    @SerializedName("sortOrder")
    @Expose
    private String sortOrder;


    public String getIndyEventId() {
        return indyEventId;
    }

    public void setIndyEventId(String indyEventId) {
        this.indyEventId = indyEventId;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }
}
