package com.indy.views.fragments.gamification.services;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.indy.BuildConfig;
import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.helpers.FileUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.gamification.models.uploadeventimage.UploadEventImageOutput;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Tohamy on 10/3/2017.
 */

public class UploadEventImageService {

    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private Uri imgPath;
    private int TIMEOUT = 60; // seconds
    private Context mContext;
    public static final String MULTIPART_FORM_DATA = "multipart/form-data";
    private String deviceId, userSessionId, applicationId, userId, lang, channel, appVersion, osVersion, regId, msisdn, indyEventId, sortOrder, entryCaption, token, imsi, authToken;
    private MyEndPointsInterface apiService;

    public UploadEventImageService(BaseInterface mBaseInterface, Uri imgPath, Context mContext, String deviceId, String userSessionId, String applicationId, String userId, String lang, String channel, String appVersion, String osVersion, String regId, String msisdn, String indyEventId, String sortOrder, String entryCaption, String token, String imsi, String authToken) {
        mBaseResponseModel = new BaseResponseModel();
        this.mBaseInterface = mBaseInterface;
        this.imgPath = imgPath;
        this.mContext = mContext;
        this.deviceId = deviceId;
        this.userSessionId = userSessionId;
        this.applicationId = applicationId;
        this.userId = userId;
        this.lang = lang;
        this.channel = channel;
        this.appVersion = appVersion;
        this.osVersion = osVersion;
        this.regId = regId;
        this.msisdn = msisdn;
        this.indyEventId = indyEventId;
        this.sortOrder = sortOrder;
        this.entryCaption = entryCaption;
        this.token = token;
        this.imsi = imsi;
        this.authToken = authToken;
        consumeService();
    }

    private void consumeService() {
        initParams();
        HashMap<String, RequestBody> params = new HashMap<>();
        RequestBody mDeviceId = createPartFromString(deviceId);
        RequestBody mUserSessionId = createPartFromString(userSessionId);
        RequestBody mApplicationId = createPartFromString(applicationId);
        RequestBody mUserId = createPartFromString(userId);
        RequestBody mLang = createPartFromString(lang);
        RequestBody mChannel = createPartFromString(channel);
        RequestBody mAppVersion = createPartFromString(appVersion);
        RequestBody mOsVersion = createPartFromString(osVersion);
        RequestBody mRegId = createPartFromString(regId);
        RequestBody mMsisdn = createPartFromString(msisdn);
        RequestBody mIndyEventId = createPartFromString(indyEventId);
        RequestBody mSortOrder = createPartFromString(sortOrder);
        RequestBody mEntryCaption = createPartFromString(entryCaption);
        RequestBody mToken = createPartFromString(token);
        RequestBody mImsi = createPartFromString(imsi);
        RequestBody mAuthToken = createPartFromString(authToken);
        params.put("deviceId", mDeviceId);
        params.put("userSessionId", mUserSessionId);
        params.put("applicationId", mApplicationId);
        params.put("userId", mUserId);
        params.put("lang", mLang);
        params.put("channel", mChannel);
        params.put("appVersion", mAppVersion);
        params.put("osVersion", mOsVersion);
        params.put("regId", mRegId);
        params.put("msisdn", mMsisdn);
        params.put("indyEventId", mIndyEventId);
        params.put("sortOrder", mSortOrder);
        params.put("entryCaption", mEntryCaption);
        params.put("token", mToken);
        params.put("imsi", mImsi);
        params.put("authToken", mAuthToken);

        Log.v("regID", regId + "");

        if (imgPath != null) {
            File file = FileUtils.getFile(mContext, imgPath);
            RequestBody requestFile =
                    RequestBody.create(MediaType.parse("multipart/form-data"), file);
            MultipartBody.Part body =
                    MultipartBody.Part.createFormData("entryImage", file.getName(), requestFile);
            Call<UploadEventImageOutput> call = apiService.uploadEventImage(requestFile, body, params);
            call.enqueue(new Callback<UploadEventImageOutput>() {
                @Override
                public void onResponse(Call<UploadEventImageOutput> call,
                                       Response<UploadEventImageOutput> response) {
                    mBaseResponseModel.setResultObj(response.body());
                    mBaseResponseModel.setServiceType(ServiceUtils.UPLOAD_EVENT_IMAGE);
                    if (response.code() == ConstantUtils.unAuthorizeToken) {
                        try {
                            mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        if (response.body() != null && response.body().getErrorMsgEn() != null) {
                            String urlForTag = call.request().url().toString();
                            urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                            urlForTag = urlForTag.replace("/", "_");
                            CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                        }
                        mBaseInterface.onSucessListener(mBaseResponseModel);
                    }
                }

                @Override
                public void onFailure(Call<UploadEventImageOutput> call, Throwable t) {
                    mBaseResponseModel.setResultObj(t);
                    mBaseResponseModel.setServiceType(ServiceUtils.UPLOAD_EVENT_IMAGE);

                    mBaseInterface.onErrorListener(mBaseResponseModel);

                }
            });
        }
    }


    private RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(
                MediaType.parse(MULTIPART_FORM_DATA), descriptionString);
    }

    public MasterErrorResponse getResponseModel(String reponse) {
        MasterErrorResponse responseStr = null;
        Gson gson = new GsonBuilder().create();
        try {
            responseStr = gson.fromJson(reponse, MasterErrorResponse.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseStr;
    }

    private void initParams() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
        httpClient.networkInterceptors().add(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();
                Request request;
                if (BuildConfig.FLAVOR.equalsIgnoreCase("prod")) {
                    request = original.newBuilder()
                            .header("Accept", "application/json")
                            .method(original.method(), original.body())
                            .build();
                }
                else{
                    request = original.newBuilder()
                            .header("Accept", "application/json")
//                            .header("custom_header", "pre_prod")
                            .method(original.method(), original.body())
                            .build();
                }
                return chain.proceed(request);
            }
        });
        httpClient.addInterceptor(interceptor);
        OkHttpClient client = httpClient.readTimeout(TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MyEndPointsInterface.baseUrl).addConverterFactory(GsonConverterFactory.create())
                .client(client).build();

        apiService = retrofit.create(MyEndPointsInterface.class);
    }
}



