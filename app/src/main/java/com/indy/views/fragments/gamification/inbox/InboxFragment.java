package com.indy.views.fragments.gamification.inbox;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.SwpeMainActivity;
import com.indy.views.fragments.gamification.models.getnotificationslist.GetNotificationsListInput;
import com.indy.views.fragments.gamification.models.getnotificationslist.GetNotificationsListOutput;
import com.indy.views.fragments.gamification.models.getnotificationslist.NotificationModel;
import com.indy.views.fragments.gamification.services.GetNotificationsListService;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.MasterFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class InboxFragment extends MasterFragment implements OnNotificationClickInterface {

    private View rootView;
    private RecyclerView rv_notifications;
    private NotificationListAdapter notificationListAdapter;
    private ProgressBar progressBar;
    private GetNotificationsListOutput getNotificationsListOutput;
    private RelativeLayout rl_back;
    private RelativeLayout rl_help;
    private TextView tv_headerTitle;

    public InboxFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_inbox, container, false);
        initUI();
        return rootView;
    }

    @Override
    public void initUI() {
        super.initUI();
        rl_back = (RelativeLayout) rootView.findViewById(R.id.backLayout);
        rl_help = (RelativeLayout) rootView.findViewById(R.id.helpLayout);
        rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        tv_headerTitle = (TextView) rootView.findViewById(R.id.titleTxt);
        tv_headerTitle.setText(getResources().getString(R.string.inbox));
        rl_help.setVisibility(View.INVISIBLE);
        rv_notifications = (RecyclerView) rootView.findViewById(R.id.rv_notifications);
        rv_notifications.setLayoutManager(new LinearLayoutManager(getActivity()));
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressID);
        onConsumeService();
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        progressBar.setVisibility(View.VISIBLE);
        GetNotificationsListInput getNotificationsListInput = new GetNotificationsListInput();
        getNotificationsListInput.setChannel(channel);
        getNotificationsListInput.setLang(currentLanguage);
        getNotificationsListInput.setMsisdn(sharedPrefrencesManger.getMobileNo());
        getNotificationsListInput.setAppVersion(appVersion);
        getNotificationsListInput.setAuthToken(authToken);
        getNotificationsListInput.setDeviceId(deviceId);
        getNotificationsListInput.setToken(token);
        getNotificationsListInput.setUserSessionId(sharedPrefrencesManger.getUserSessionId());
        getNotificationsListInput.setUserId(sharedPrefrencesManger.getUserId());
        getNotificationsListInput.setApplicationId(ConstantUtils.INDY_TALOS_ID);
        getNotificationsListInput.setImsi(sharedPrefrencesManger.getMobileNo());
        getNotificationsListInput.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        getNotificationsListInput.setOsVersion(osVersion);
        new GetNotificationsListService(this, getNotificationsListInput);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        progressBar.setVisibility(View.GONE);
        switch (responseModel.getServiceType()) {
            case ServiceUtils.GET_NOTIFICATIONS_LIST: {
                if (responseModel != null) {
                    getNotificationsListOutput = (GetNotificationsListOutput) responseModel.getResultObj();
                    if (getNotificationsListOutput != null)
                        if (getNotificationsListOutput.getNotificationsList() != null
                                && !getNotificationsListOutput.getNotificationsList().isEmpty()) {
                            getNotificationsListSuccess();
                        } else {
                            getNotificationsListError();
                        }
                }
                break;
            }
        }
    }

    private void getNotificationsListSuccess() {
        notificationListAdapter = new NotificationListAdapter(getNotificationsListOutput.getNotificationsList(), getActivity(), this);
        rv_notifications.setAdapter(notificationListAdapter);
    }

    private void getNotificationsListError() {
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)
                ? getNotificationsListOutput.getErrorMsgEn() : getNotificationsListOutput.getErrorMsgAr());
        errorFragment.setArguments(bundle);
        ((SwpeMainActivity) getActivity()).replaceFragmnet(errorFragment, R.id.frameLayout, true);
    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onNotificationClick(NotificationModel notificationModel, int index) {
        NotificationDetailFragment notificationDetailFragment = new NotificationDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.NOTIFICATIONS_ID, notificationModel.getNotificationId());
        notificationDetailFragment.setArguments(bundle);
        ((SwpeMainActivity) getActivity()).replaceFragmnet(notificationDetailFragment, R.id.contentFrameLayout, true);
    }
}
