package com.indy.views.fragments.newaccount;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.models.createprofilestatus.CreateProfileStatusInput;
import com.indy.models.createprofilestatus.CreateProfileStatusoutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.CreateProfileService;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.HelpActivity;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.indy.views.fragments.utils.MasterFragment;

/**
 * Created by emad on 9/18/16.
 */
public class NewAccountFragment extends MasterFragment {
    private View view, topBar;
    private TextView continueBtn, ib_yes;
    private EditText mobileNO;
    TextInputLayout tilMobile;

    LinearLayout ll_hidden;
    ScrollView sv_content;
    TextView tv_edit, tv_title;
    CreateProfileStatusoutput createProfileStatusoutput;
    private Button backImg, helpBtn;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_new_account, container, false);
        initUI();
        setListeners();
        // requestPermission();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
        continueBtn = (Button) view.findViewById(R.id.continueBtn);
        mobileNO = (EditText) view.findViewById(R.id.mobileNO);
        backImg = (Button) view.findViewById(R.id.backImg);
        helpBtn = (Button) view.findViewById(R.id.helpBtn);

        sv_content = (ScrollView) view.findViewById(R.id.sv_screen_content);
        ll_hidden = (LinearLayout) view.findViewById(R.id.ll_confirmation_message);
        topBar = (View) view.findViewById(R.id.layout_header);
        ib_yes = (Button) view.findViewById(R.id.yesBtn);
        tv_edit = (TextView) view.findViewById(R.id.editFunctionality);
        tv_title = (TextView) view.findViewById(R.id.tv_title_number);
        tilMobile = (TextInputLayout) view.findViewById(R.id.mobile_input_layout);
        hideConfirmationMessage();
        //..............header Buttons............
        RelativeLayout backLayout = (RelativeLayout) view.findViewById(R.id.backLayout);
        RelativeLayout helpLayout = (RelativeLayout) view.findViewById(R.id.helpLayout);

        helpLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), HelpActivity.class);
                startActivity(intent);
            }
        });

        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard();
                getActivity().onBackPressed();
            }
        });

        disableNextBtn();
    }

    private void setListeners() {
        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard();
                if (isValid()) {
                    showConfirmationMessage();
                }else{
                    Bundle bundle2 = new Bundle();

                    bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_create_account_enter_invalid_number);
                    bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_create_account_enter_invalid_number);
                    mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_create_account_enter_invalid_number, bundle2);
                }
            }
        });

        mobileNO.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (continueBtn.getText().toString().trim().length() > 0) {
                    enableNextBtn();
                } else {
                    disableNextBtn();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        ib_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                regActivity.addFragmnet(new LoadingFragmnet(), R.id.loadingFragmeLayout, true);
                hideConfirmationMessage();
                CreateProfileStatusInput createProfileStatusInput = new CreateProfileStatusInput();
                createProfileStatusInput.setAppVersion(appVersion);
                createProfileStatusInput.setToken(token);
                createProfileStatusInput.setOsVersion(osVersion);
                createProfileStatusInput.setChannel(channel);
                createProfileStatusInput.setDeviceId(deviceId);
                createProfileStatusInput.setMsisdn(mobileNO.getText().toString().trim());
                createProfileStatusInput.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
                new CreateProfileService(NewAccountFragment.this, createProfileStatusInput);

            }
        });

        tv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideConfirmationMessage();
            }
        });


    }

    private void showConfirmationMessage() {
        ll_hidden.setVisibility(View.VISIBLE);
        topBar.setVisibility(View.GONE);
        sv_content.setVisibility(View.GONE);
        String phoneNumberText = mobileNO.getText().toString().trim();
        phoneNumberText = getString(R.string.number_confirmation) + "\n" + phoneNumberText;
        tv_title.setText(phoneNumberText);
    }

    private void hideConfirmationMessage() {
        ll_hidden.setVisibility(View.GONE);
        topBar.setVisibility(View.VISIBLE);
        sv_content.setVisibility(View.VISIBLE);
    }


    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            regActivity.onBackPressed();
            if (isVisible()) {
                if(responseModel!=null && responseModel.getResultObj()!=null) {
                    createProfileStatusoutput = (CreateProfileStatusoutput) responseModel.getResultObj();
                    if (createProfileStatusoutput != null)
                        onCreateProfileSucess();
                    else
                        onCreateProfileError();
                }
            }
        }catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    //6539
    void onCreateProfileSucess() {
        if (createProfileStatusoutput.getErrorCode() != null) {
            if (currentLanguage.equalsIgnoreCase("en")) {
                showErrorFragment(createProfileStatusoutput.getErrorMsgEn());
            } else {
                showErrorFragment(createProfileStatusoutput.getErrorMsgAr());
            }
        } else {
            Log.v("code_code", createProfileStatusoutput.getGeneratedCode() + " ");
            NewAccountCodeFragment newAccountCodeFragment = new NewAccountCodeFragment();
            Bundle bundle = new Bundle();
            bundle.putString(ConstantUtils.codeNO, createProfileStatusoutput.getGeneratedCode());
            bundle.putString(ConstantUtils.newAccountMobileNo, mobileNO.getText().toString().trim());
            newAccountCodeFragment.setArguments(bundle);

            Bundle bundle2 = new Bundle();

            bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_create_account_enter_valid_number);
            bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_create_account_enter_valid_number);
            mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_create_account_enter_valid_number, bundle2);

            sharedPrefrencesManger.setRegisterationID(createProfileStatusoutput.getRegistrationId());
            regActivity.replaceFragmnet(newAccountCodeFragment, R.id.frameLayout, true);
        }

    }

    void onCreateProfileError() {
        showErrorFragment(getString(R.string.connection_error));
    }

    private void showErrorFragment(String txt) {
        ErrorFragment loadingFragmnet = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, /*createProfileStatusoutput.getErrorMsgEn()*/txt);
        loadingFragmnet.setArguments(bundle);
        regActivity.addFragmnet(loadingFragmnet, R.id.frameLayout, true);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        regActivity.onBackPressed();
    }

    private boolean isValid() {
        boolean isValid = true;
        if (mobileNO.getText().toString().length() < 10) {
            isValid = false;
            tilMobile.setError(getString(R.string.invalid_delivery_number));
        } else {
            String noStr = mobileNO.getText().toString().substring(0, 2);
            if (!noStr.equalsIgnoreCase("05")) {
                isValid = false;
                tilMobile.setError(getString(R.string.invalid_delivery_number));
            } else {
                removeTextInputLayoutError(tilMobile);
            }
        }
        return isValid;
    }


    public boolean requestPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.RECEIVE_SMS)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                requestPermissions(new String[]{Manifest.permission.RECEIVE_SMS}, 100);
            }
            return false;
            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
            // app-defined int constant. The callback method gets the
            // result of the request.

        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 100: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    Toast.makeText(getActivity(), "service Off", Toast.LENGTH_SHORT).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void disableNextBtn() {
        continueBtn.setAlpha(0.5f);
        continueBtn.setEnabled(false);
    }

    private void enableNextBtn() {
        continueBtn.setAlpha(1.0f);
        continueBtn.setEnabled(true);
    }


}
