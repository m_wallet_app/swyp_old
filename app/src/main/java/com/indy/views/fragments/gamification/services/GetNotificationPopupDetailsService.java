package com.indy.views.fragments.gamification.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.BaseServiceManger;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.gamification.models.getnotificationpopupdetails.GetNotificationPopupDetailsInput;
import com.indy.views.fragments.gamification.models.getnotificationpopupdetails.GetNotificationPopupDetailsOutput;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Tohamy on 10/1/2017.
 */

public class GetNotificationPopupDetailsService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private GetNotificationPopupDetailsInput getNotificationPopupDetailsInput;


    public GetNotificationPopupDetailsService(BaseInterface baseInterface, GetNotificationPopupDetailsInput getNotificationPopupDetailsInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.getNotificationPopupDetailsInput = getNotificationPopupDetailsInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<GetNotificationPopupDetailsOutput> call = apiService.getNotificationPopupDetails(getNotificationPopupDetailsInput);
        call.enqueue(new Callback<GetNotificationPopupDetailsOutput>() {
            @Override
            public void onResponse(Call<GetNotificationPopupDetailsOutput> call, Response<GetNotificationPopupDetailsOutput> response) {
                mBaseResponseModel.setServiceType(ServiceUtils.GET_NOTIFICATION_POPUP_DETAILS);
                mBaseResponseModel.setResultObj(response.body());

                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (response.body().getErrorMsgEn() != null) {
//                        String urlForTag = call.request().url().toString();
//                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
//                        urlForTag = urlForTag.replace("/", "_");
//                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<GetNotificationPopupDetailsOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.GET_NOTIFICATION_POPUP_DETAILS);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}

