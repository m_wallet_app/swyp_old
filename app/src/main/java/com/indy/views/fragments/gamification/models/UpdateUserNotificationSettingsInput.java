package com.indy.views.fragments.gamification.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterInputResponse;

/**
 * Created by mobile on 23/10/2017.
 */

public class UpdateUserNotificationSettingsInput extends MasterInputResponse {

    @SerializedName("notificationsFlag")
    @Expose
    private boolean notificationsFlag;


    public boolean isNotificationsFlag() {
        return notificationsFlag;
    }

    public void setNotificationsFlag(boolean notificationsFlag) {
        this.notificationsFlag = notificationsFlag;
    }
}
