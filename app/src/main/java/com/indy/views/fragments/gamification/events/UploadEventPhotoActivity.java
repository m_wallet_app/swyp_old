package com.indy.views.fragments.gamification.events;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.controls.ImageListinerInterface;
import com.indy.models.utils.ImageItem;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.AttachmentActivity;
import com.indy.views.fragments.gamification.models.EventsList;

import java.util.ArrayList;

public class UploadEventPhotoActivity extends AttachmentActivity implements ImageListinerInterface {

    private ImageItem imageItem;
    private LinearLayout ll_root;
    ImageView iv_photo;
    TextView tv_takePhoto, tv_chooseFromGallary;
    Button continueBtn, cancelButton;
    FrameLayout frameLayout;
    private EventsList eventObject;
    public static boolean isUploaded = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_event_photo);
        eventObject = (EventsList) getIntent().getParcelableExtra(ConstantUtils.EVENT_KEY);
        initUI();
    }

    @Override
    public void initUI() {
        super.initUI();
        continueBtn = (Button) findViewById(R.id.continueBtn);
        continueBtn.setText(getString(R.string.upload));
        tv_takePhoto = (TextView) findViewById(R.id.takePhoto);
        tv_chooseFromGallary = (TextView) findViewById(R.id.chooseFromGallary);
        iv_photo = (ImageView) findViewById(R.id.iv_profile_image);
        ll_root = (LinearLayout) findViewById(R.id.editemail_bottom_sheet);
        frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
        frameLayout.setVisibility(View.GONE);
        tv_takePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTakePhoto();
            }
        });

        tv_chooseFromGallary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onChooseFromGallary();
            }
        });

        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imageItem != null) {
                    ImageProcessingFragment imageProcessingFragment = new ImageProcessingFragment();
                    Bundle bundle = new Bundle();
                    ll_root.setVisibility(View.GONE);
                    frameLayout.setVisibility(View.VISIBLE);
                    bundle.putParcelable(ConstantUtils.SOURCE_IMAGE, imageItem.getBitmap());
                    imageProcessingFragment.setArguments(bundle);
                    addFragmnet(imageProcessingFragment, R.id.frameLayout, true);
                }
            }
        });

        cancelButton = (Button) findViewById(R.id.cancelBtn);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (eventObject != null && eventObject.getUserUploadedPhoto() != null) {
//            finish();
        }
    }

    @Override
    public void onBackPressed() {

        if(frameLayout.getVisibility()==View.GONE){
            finish();
        }

        if (getSupportFragmentManager().getBackStackEntryCount() <= 1) {
            frameLayout.setVisibility(View.GONE);
            ll_root.setVisibility(View.VISIBLE);
        } else {
            super.onBackPressed();
        }
        if(isUploaded){
            finish();
        }

    }

    private void onTakePhoto() {

        setImageListinerInterface(this, 0);
        imgManage.getImg(PIC_FROM_CAMERA);
    }

    private void onChooseFromGallary() {
        setImageListinerInterface(this, 0);
        imgManage.getImg(PIC_FROM_Gallary);

    }

    @Override
    public void onImageSet(ImageItem imageItem) {
        if (imageItem != null) {
            enableSavePhoto();
            iv_photo.setImageBitmap(imageItem.getBitmap());
        } else {
            iv_photo.setImageResource(R.drawable.bitmap_default_image);
            //            iv_profileImage.setImageResource(R.drawable.bitmap_default_image);
        }
        this.imageItem = imageItem;

    }

    private void enableSavePhoto() {
        continueBtn.setAlpha(1f);
        continueBtn.setClickable(true);
    }

    private void disableSavePhoto() {
        continueBtn.setAlpha(.5f);
        continueBtn.setClickable(false);
    }


    @Override
    public void onDeleteImage(ArrayList<ImageItem> imageItems, int imgPosition) {

    }
}
