package com.indy.views.fragments.store.recharge;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.indy.R;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.utils.MaterialCheckBox;
import com.indy.views.activites.RechargeActivity;
import com.indy.views.fragments.gamification.models.serviceInputOutput.rechargeBalance.LogRechargeBalanceEventInputModel;
import com.indy.views.fragments.gamification.services.RegisterRechargeBalanceEventService;
import com.indy.views.fragments.utils.MasterFragment;

/**
 * Created by Amir.jehangir on 10/15/2016.
 */
public class CongratulationsFragmentRecharge extends MasterFragment {

    View view;
    Button okBtn;
    TextView tv_successText;
    RechargeActivity rechargeActivity;
    String amount = "0";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_congratulation_payment, container, false);
        //  view.bringToFront();
        initUI();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
//        rechargeActivity = (RechargeActivity) getActivity();
        okBtn = (Button) view.findViewById(R.id.okBtn);
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();



                // rechargeActivity.startActivity(new Intent(getActivity(),RechargeActivity.class));
                // rechargeActivity.replaceFragmnet(new RechargeCardFragment(), R.id.frameLayout, false);
            }
        });

        RechargeActivity.isCharged = true;

        tv_successText = (TextView) view.findViewById(R.id.tv_successful_text);
        boolean isCreditCard = false;
        if (getArguments() != null && getArguments().getString("rechargeAmount") != null) {
            amount = getArguments().getString("rechargeAmount");
        } else {
            amount = "0";
        }
        if(getArguments() !=null && getArguments().getBoolean("isCreditCard")){
            tv_successText.setText(getString(R.string.you_have_added_) + " " + amount + " " + getString(R.string.to_your_line_with_period));
        }else{
            tv_successText.setText(getString(R.string.recharge_sucessfully));
        }

        try {
            if (Double.parseDouble(amount) >= 0) {
                registerCreditCardPaymentEvent(amount);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        MaterialCheckBox materialCheckBox = (MaterialCheckBox) view.findViewById(R.id.fl_tick_animation);
        ImageView iv_tick = (ImageView) view.findViewById(R.id.iv_tick);
        CommonMethods.drawCircle(materialCheckBox,iv_tick);

    }

    private void registerCreditCardPaymentEvent(String amount) {
        try {
            LogRechargeBalanceEventInputModel gamificationBaseInputModel = new LogRechargeBalanceEventInputModel();
            gamificationBaseInputModel.setChannel(channel);
            gamificationBaseInputModel.setLang(currentLanguage);
            gamificationBaseInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
            gamificationBaseInputModel.setAppVersion(appVersion);
            gamificationBaseInputModel.setAuthToken(authToken);
            gamificationBaseInputModel.setDeviceId(deviceId);
            gamificationBaseInputModel.setToken(token);
            gamificationBaseInputModel.setUserSessionId(sharedPrefrencesManger.getUserSessionId());
            gamificationBaseInputModel.setUserId(sharedPrefrencesManger.getUserId());
            gamificationBaseInputModel.setApplicationId(ConstantUtils.INDY_TALOS_ID);
            gamificationBaseInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
            gamificationBaseInputModel.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
            gamificationBaseInputModel.setOsVersion(osVersion);
            gamificationBaseInputModel.setAmount(Double.parseDouble(amount));
            new RegisterRechargeBalanceEventService(this, gamificationBaseInputModel);
        }catch (Exception ex){
            CommonMethods.logException(ex);
        }
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
//        super.onSucessListener(responseModel);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
//        super.onErrorListener(responseModel);
    }
}
