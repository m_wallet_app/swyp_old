package com.indy.views.fragments.gamification.events;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.controls.ServiceUtils;
import com.indy.helpers.GridLayoutColumnItemDecoration;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.MasterActivity;
import com.indy.views.fragments.gamification.OnPhotoItemClick;
import com.indy.views.fragments.gamification.OnSortSelect;
import com.indy.views.fragments.gamification.OnVoteUnVoteClick;
import com.indy.views.fragments.gamification.adapters.EventPhotosAdapter;
import com.indy.views.fragments.gamification.adapters.FilterOptionListAdapter;
import com.indy.views.fragments.gamification.models.EventPhoto;
import com.indy.views.fragments.gamification.models.EventsList;
import com.indy.views.fragments.gamification.models.eventfilters.EventFilerInput;
import com.indy.views.fragments.gamification.models.eventfilters.EventFilterOptions;
import com.indy.views.fragments.gamification.models.eventfilters.EventFilterOutput;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetEventDetailInputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetEventDetailOutputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.LikeUnlikeEntryInputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.LikeUnlikeEntryOutputModel;
import com.indy.views.fragments.gamification.services.GetEventDetailsService;
import com.indy.views.fragments.gamification.services.GetEventFilterService;
import com.indy.views.fragments.gamification.services.LikeUnlikeEntryService;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class EventDetailActivity extends MasterActivity implements OnVoteUnVoteClick, OnPhotoItemClick, OnSortSelect {

    public static final int REQUEST_CODE = 125;
    RecyclerView rv_photos;
    ImageView iv_topBanner, iv_sort, iv_uploadedImage;
    TextView tv_title, tv_description, tv_voteCounter, tv_sort, tv_apply, tv_noDataFound, tv_noValidMembership, tv_headerTitle,
            tv_descriptionLong, tv_showMoreLess;
    FrameLayout frameLayout;
    public BottomSheetBehavior behavior;
    public Button resetBtn, uploadBtn, btn_buyTickets;
    public View bottomSheet;
    //    public CategoriesListAdapter categoriesListAdapter;
    public FilterOptionListAdapter filterOptionListAdapter;
    RecyclerView rv_filterOptions;
    List<EventFilterOptions> filterOptions;
    public static String selectedCategory = "";
    LinearLayout ll_sort, ll_selfParticipation, ll_below_header;
    private EventsList eventObject;
    EventPhotosAdapter eventPhotosAdapter;
    List<EventPhoto> eventPhotosList;
    int position, max;
    int status;
    RelativeLayout rl_back, rl_help;
    GetEventDetailOutputModel getEventDetailOutputModel;
    LikeUnlikeEntryOutputModel likeUnlikeEntryOutputModel;
    private int selectedSortIndex = -1;
    private String sortValue;
    private EventPhoto updatedEventPhoto;
    private int updatedIndex;
    private boolean isExpanded = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);
        initUI();
        onConsumeFilter();
    }

    public void reloadScreen() {
        if (getEventDetailOutputModel != null) {

            onConsumeService();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        reloadScreen();
//        setupParticipationView();
    }

    @Override
    public void initUI() {
        super.initUI();
        try {

            frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
            ll_below_header = (LinearLayout) findViewById(R.id.ll_below_header);
            if (getIntent() != null && getIntent().getExtras() != null) {
                Bundle bundle = getIntent().getExtras();
                if (bundle != null && bundle.getParcelable(ConstantUtils.KEY_EVENT_DETAIL) != null) {
                    eventObject = bundle.getParcelable(ConstantUtils.KEY_EVENT_DETAIL);
                } else {
                    finish();
                }
                if (bundle != null && bundle.getInt("total") > 0) {
                    max = bundle.getInt("total");
                }

                if (bundle != null && bundle.getInt("position") > 0) {
                    max = bundle.getInt("position");
                }
                onConsumeService();
//                onConsumeFilter();
            }

            btn_buyTickets = (Button) findViewById(R.id.btn_get_tickets);
            tv_headerTitle = (TextView) findViewById(R.id.titleTxt);
            tv_headerTitle.setText(getResources().getString(R.string.details_spaced));

            rl_back = (RelativeLayout) findViewById(R.id.backLayout);
            rl_help = (RelativeLayout) findViewById(R.id.helpLayout);
            rl_back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            rl_help.setVisibility(View.INVISIBLE);

            rv_photos = (RecyclerView) findViewById(R.id.rv_event_photos);
            rv_photos.setLayoutManager(new GridLayoutManager(this, 2));
            rv_photos.setHasFixedSize(true);

            GridLayoutColumnItemDecoration gridLayoutColumnItemDecoration = new GridLayoutColumnItemDecoration(2, 10, true);
            rv_photos.addItemDecoration(gridLayoutColumnItemDecoration);

            eventPhotosList = new ArrayList<>();


            rv_photos.setAdapter(eventPhotosAdapter);
            rv_photos.setNestedScrollingEnabled(false);

            tv_description = (TextView) findViewById(R.id.tv_event_description);
            tv_title = (TextView) findViewById(R.id.tv_event_name);
            tv_sort = (TextView) findViewById(R.id.tv_sort);
            iv_sort = (ImageView) findViewById(R.id.iv_sort);
            iv_topBanner = (ImageView) findViewById(R.id.iv_event_details);
            iv_uploadedImage = (ImageView) findViewById(R.id.ivuploaded);
            tv_voteCounter = (TextView) findViewById(R.id.tv_vote_counter);
            tv_descriptionLong = (TextView) findViewById(R.id.tv_event_description_long);
            tv_showMoreLess = (TextView) findViewById(R.id.tv_show_more_less);


            CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinateLayoutID);

            bottomSheet = coordinatorLayout.findViewById(R.id.bottom_sheet_linear_layout);
            behavior = BottomSheetBehavior.from(bottomSheet);
            behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    // React to state change
                    if (newState == BottomSheetBehavior.STATE_COLLAPSED) {

                    } else if (newState == BottomSheetBehavior.STATE_EXPANDED) {

                    }
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                    // React to dragging events
                }
            });

            tv_apply = (TextView) findViewById(R.id.tv_apply);
            resetBtn = (Button) findViewById(R.id.resetBtn);
            resetBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectedSortIndex = -1;
                    if (filterOptionListAdapter != null) {
                        filterOptionListAdapter.reset();
                    }
                }
            });
//            tv_filterCount = ((TextView) findViewById(R.id.tv_filter_count));
//            tv_filterCount.setVisibility(View.GONE);
            Typeface tf = Typeface.createFromAsset(getResources().getAssets(), "fonts/OpenSans-Regular.ttf");
//            tv_filterCount.setTypeface(tf);

            rv_filterOptions = (RecyclerView) findViewById(R.id.rv_categories);
            rv_filterOptions.setLayoutManager(new LinearLayoutManager(this));
            tv_noDataFound = (TextView) findViewById(R.id.tv_no_data_found);

            tv_apply.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    onConsumeService();
                }
            });
            ll_selfParticipation = (LinearLayout) findViewById(R.id.ll_self_participation);
            ll_sort = (LinearLayout) findViewById(R.id.ll_sort);
            ll_sort.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                }
            });

            tv_noValidMembership = (TextView) findViewById(R.id.tv_no_membership);
            uploadBtn = (Button) findViewById(R.id.btn_upload);
            tv_showMoreLess.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (isExpanded) {
                        tv_description.setVisibility(View.VISIBLE);
                        tv_descriptionLong.setVisibility(View.GONE);
                        tv_showMoreLess.setText(getString(R.string.show_more));
                    } else {
                        tv_description.setVisibility(View.GONE);
                        tv_descriptionLong.setVisibility(View.VISIBLE);
                        tv_showMoreLess.setText(getString(R.string.show_less));

                    }
                    isExpanded = !isExpanded;
                }
            });

            btn_buyTickets.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });


        } catch (Exception ex) {
            Log.e(TAG, ex.getMessage());
            //CommonMethods.logException(ex);
        }
    }


    @Override
    public void onConsumeService() {
        super.onConsumeService();
        frameLayout.setVisibility(View.VISIBLE);
        addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        GetEventDetailInputModel getEventDetailInputModel = new GetEventDetailInputModel();
        getEventDetailInputModel.setChannel(channel);
        getEventDetailInputModel.setLang(currentLanguage);
        getEventDetailInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        getEventDetailInputModel.setAppVersion(appVersion);
        getEventDetailInputModel.setAuthToken(authToken);
        getEventDetailInputModel.setDeviceId(deviceId);
        getEventDetailInputModel.setToken(token);
        getEventDetailInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
        getEventDetailInputModel.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        getEventDetailInputModel.setOsVersion(osVersion);

        getEventDetailInputModel.setApplicationId(ConstantUtils.INDY_TALOS_ID);
        getEventDetailInputModel.setUserId(sharedPrefrencesManger.getUserId());
        getEventDetailInputModel.setUserSessionId(sharedPrefrencesManger.getUserSessionId());
        getEventDetailInputModel.setIndyEventId(eventObject.getIndyEventId());
        if (selectedSortIndex != -1 && sortValue != null && !sortValue.isEmpty()) {
            getEventDetailInputModel.setSortOrder(sortValue);
        } else {
            getEventDetailInputModel.setSortOrder("");
        }
//        else {
//            getEventDetailInputModel.setSortOrder("TOP_RATED");
//        }
        new GetEventDetailsService(this, getEventDetailInputModel);


    }

    public void onConsumeFilter() {

        EventFilerInput evrntFilterModel = new EventFilerInput();
        evrntFilterModel.setChannel(channel);
        evrntFilterModel.setLang(currentLanguage);
        evrntFilterModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        evrntFilterModel.setAppVersion(appVersion);
        evrntFilterModel.setAuthToken(authToken);
        evrntFilterModel.setDeviceId(deviceId);
        evrntFilterModel.setToken(token);
        evrntFilterModel.setImsi(sharedPrefrencesManger.getMobileNo());
        evrntFilterModel.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        evrntFilterModel.setOsVersion(osVersion);

        evrntFilterModel.setApplicationId(ConstantUtils.INDY_TALOS_ID);
        evrntFilterModel.setUserId(sharedPrefrencesManger.getUserId());
        evrntFilterModel.setUserSessionId(sharedPrefrencesManger.getUserSessionId());
        new GetEventFilterService(this, evrntFilterModel);
    }

    public void onCallLikeUnlikeService(String entryId, boolean like) {
        frameLayout.setVisibility(View.VISIBLE);
        addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        LikeUnlikeEntryInputModel likeUnlikeEntryInputModel = new LikeUnlikeEntryInputModel();
        likeUnlikeEntryInputModel.setChannel(channel);
        likeUnlikeEntryInputModel.setLang(currentLanguage);
        likeUnlikeEntryInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        likeUnlikeEntryInputModel.setAppVersion(appVersion);
        likeUnlikeEntryInputModel.setAuthToken(authToken);
        likeUnlikeEntryInputModel.setDeviceId(deviceId);
        likeUnlikeEntryInputModel.setToken(token);
        likeUnlikeEntryInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
        likeUnlikeEntryInputModel.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        likeUnlikeEntryInputModel.setOsVersion(osVersion);
        likeUnlikeEntryInputModel.setApplicationId(ConstantUtils.INDY_TALOS_ID);
        likeUnlikeEntryInputModel.setUserId(sharedPrefrencesManger.getUserId());
        likeUnlikeEntryInputModel.setUserSessionId(sharedPrefrencesManger.getUserSessionId());
        likeUnlikeEntryInputModel.setIndyEventId(eventObject.getIndyEventId());
        likeUnlikeEntryInputModel.setIndyEventEntryId(entryId);
        likeUnlikeEntryInputModel.setLike(like);
        new LikeUnlikeEntryService(this, likeUnlikeEntryInputModel);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        if (responseModel != null && responseModel.getServiceType() != null) {
            switch (responseModel.getServiceType()) {
                case ServiceUtils.getEventDetailsService:
                    onBackPressed();
                    getEventDetailsService(responseModel);
                    break;
                case ServiceUtils.likeUnlikeEntryService:
                    onBackPressed();
                    isServiceCalled = false;
                    getLikeAndUnLikeEntryService(responseModel);
                    break;
                case ServiceUtils.EventsFilter:
                    onGetEventFilter(responseModel);
                    break;

            }

        }
    }

    public void getEventDetailsService(BaseResponseModel responseModel) {
        getEventDetailOutputModel = (GetEventDetailOutputModel) responseModel.getResultObj();
        if (getEventDetailOutputModel != null && getEventDetailOutputModel.getErrorCode() == null) {
            eventObject.setUserUploadedPhoto(getEventDetailOutputModel.getUserEntry());

            eventObject.setEventPhotosList(getEventDetailOutputModel.getEntriesList());
            eventObject.setEventPhotosList(getEventDetailOutputModel.getEntriesList());
            eventObject.setBuyButtonEnabled(getEventDetailOutputModel.getIndyEvent().isBuyButtonEnabled());
            eventObject.setBuyButtonText(getEventDetailOutputModel.getIndyEvent().getBuyButtonText());
            eventObject.setBuyButtonURL(getEventDetailOutputModel.getIndyEvent().getBuyButtonURL());
            eventObject.setBuyButtonVisible(getEventDetailOutputModel.getIndyEvent().isBuyButtonVisible());
            eventObject.setUploadButtonEnabled(getEventDetailOutputModel.getIndyEvent().isUploadButtonEnabled());

            onEventDetailSuccess();
        } else {
            onEventDetailFailure();
        }
    }

    private void onEventDetailFailure() {
        frameLayout.setVisibility(View.VISIBLE);
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        if (getEventDetailOutputModel != null && getEventDetailOutputModel.getErrorMsgEn() != null) {
            if (sharedPrefrencesManger != null && sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)) {
                bundle.putString(ConstantUtils.errorString, getEventDetailOutputModel.getErrorMsgEn());

            } else {
                bundle.putString(ConstantUtils.errorString, getEventDetailOutputModel.getErrorMsgAr());

            }
        } else {
            bundle.putString(ConstantUtils.errorString, getString(R.string.generice_error));

        }
        errorFragment.setArguments(bundle);
        addFragmnet(errorFragment, R.id.frameLayout, true);
        setupParticipationView();
    }

    public void getLikeAndUnLikeEntryService(BaseResponseModel responseModel) {
        likeUnlikeEntryOutputModel = (LikeUnlikeEntryOutputModel) responseModel.getResultObj();
        if (likeUnlikeEntryOutputModel != null) {
            if (likeUnlikeEntryOutputModel.isSuccess()) {
                updatedEventPhoto.setSelected(!updatedEventPhoto.isSelected());
                if (eventPhotosAdapter != null)
                    eventPhotosAdapter.notifyItemRangeChanged(updatedIndex, 1);
            }
        }
    }

    public void onGetEventFilter(BaseResponseModel responseModel) {
        EventFilterOutput getEventDetailOutputModel = (EventFilterOutput) responseModel.getResultObj();
        if (getEventDetailOutputModel != null) {
            filterOptions = getEventDetailOutputModel.getOptions();
            filterOptionListAdapter = new FilterOptionListAdapter(filterOptions, this);
            rv_filterOptions.setAdapter(filterOptionListAdapter);
            ll_sort.setEnabled(true);
        } else {
            ll_sort.setEnabled(false);
        }
    }

    private void onEventDetailSuccess() {
        setupParticipationView();
        if (eventObject.getEventPhotosList() != null && eventObject.getEventPhotosList().size() > 0) {
            eventPhotosAdapter = new EventPhotosAdapter(eventObject.getEventPhotosList(), this, this, this,
                    status != ConstantUtils.inValidMembership);
            rv_photos.setAdapter(eventPhotosAdapter);
            setRecyclerViewHeight(rv_photos);
            rv_photos.smoothScrollToPosition(0);
            ll_below_header.setVisibility(View.VISIBLE);
            rv_photos.setVisibility(View.VISIBLE);
            tv_noDataFound.setVisibility(View.GONE);
        } else {
            rv_photos.setVisibility(View.GONE);
            ll_below_header.setVisibility(View.GONE);
            tv_noDataFound.setVisibility(View.VISIBLE);
        }

        if (sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)) {
            tv_title.setText(eventObject.getNameEn());
            tv_description.setText(eventObject.getOfferDescEn());
            tv_descriptionLong.setText(eventObject.getOfferDescEn());

        } else {
            tv_title.setText(eventObject.getNameAr());
            tv_description.setText(eventObject.getOfferDescAr());
            tv_descriptionLong.setText(eventObject.getOfferDescAr());
        }

        if (eventObject.getImageUrl() != null && !eventObject.getImageUrl().isEmpty())
            ImageLoader.getInstance().displayImage(eventObject.getImageUrl(), iv_topBanner);

        if(CommonMethods.isMembershipValid(this)) {
            if (eventObject.isBuyButtonVisible()) {
                if (eventObject.getBuyButtonText() == null || eventObject.getBuyButtonText().length() < 1 ||
                        eventObject.getBuyButtonURL() == null || eventObject.getBuyButtonURL().length() < 1) {
                    btn_buyTickets.setVisibility(View.INVISIBLE);
                } else {

                    btn_buyTickets.setText(eventObject.getBuyButtonText());

                    btn_buyTickets.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            CommonMethods.openExternalWebView(eventObject.getBuyButtonURL(), EventDetailActivity.this);
                        }
                    });
                    if (!eventObject.isBuyButtonEnabled()) {
                        disableBuyButton();
                    } else {
                        enableBuyButton();
                    }
                }
            } else {
                btn_buyTickets.setVisibility(View.INVISIBLE);
            }
        }

    }

    public void setRecyclerViewHeight(RecyclerView recyclerView) {
        int multiplier = recyclerView.getAdapter().getItemCount();
        if (multiplier < 2) {
            multiplier = 2;
        }

        if (multiplier % 2 > 0) {
            multiplier += 1;
        }
        int viewHeight = (int) getResources().getDimension(R.dimen.x125) * ((multiplier / 2));
        viewHeight += recyclerView.getAdapter().getItemCount();
        recyclerView.getLayoutParams().height = viewHeight;

    }

    private void setupParticipationView() {
        if (sharedPrefrencesManger.getUserProfileObject().getMembershipData() != null && sharedPrefrencesManger.getUserProfileObject().getMembershipData().getStatus() != null) {
            status = sharedPrefrencesManger.getUserProfileObject().getMembershipData().getStatus();
//            status = 2;

            // in case of invalid membership
            if (!CommonMethods.isMembershipValid(this)) {
                tv_noValidMembership.setVisibility(View.VISIBLE);

                disableButtons();

                ll_selfParticipation.setVisibility(View.GONE);
            } else {
                if (eventObject.getUserUploadedPhoto() != null && eventObject.getUserUploadedPhoto().getImageURL() != null) {


                    tv_noValidMembership.setVisibility(View.GONE);
                    uploadBtn.setVisibility(View.INVISIBLE);
                    ll_selfParticipation.setVisibility(View.VISIBLE);
                    // TODO: 01/10/2017 Base URl is removed please check and verify again
                    if (eventObject.getUserUploadedPhoto().getImageURL().length() > 0) {
                        Picasso.with(this).
                                load(/*MyEndPointsInterface.imgBaseUrl +*/ eventObject.getUserUploadedPhoto().getImageURL())
                                .networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE)
                                .into(iv_uploadedImage);
                    }
                    tv_voteCounter.setText(eventObject.getUserUploadedPhoto().getVotes() + " " + getString(R.string.votes));
                    ll_selfParticipation.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(EventDetailActivity.this, PhotoDetailFragment.class);
                            intent.putExtra(ConstantUtils.IS_OWNER, true);
                            intent.putExtra(ConstantUtils.EVENT_PHOTO, eventObject.getUserUploadedPhoto());
                            startActivity(intent);

                        }
                    });
                } else {
                    tv_noValidMembership.setVisibility(View.GONE);
                    if(eventObject.isUploadButtonEnabled()) {
                        enableUploadButton();
                    }else{
                        disableUploadButton();
                    }
                    disableBuyButton();
                    uploadBtn.setVisibility(View.VISIBLE);
                    uploadBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
//                            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                            Intent intent = new Intent(EventDetailActivity.this, UploadEventPhotoActivity.class);
                            intent.putExtra(ConstantUtils.EVENT_KEY, eventObject);
                            startActivityForResult(intent, REQUEST_CODE);
                        }
                    });
                    ll_selfParticipation.setVisibility(View.GONE);
                }

            }

//            uploadBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
////                            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                    Intent intent = new Intent(EventDetailActivity.this, UploadEventPhotoActivity.class);
//                    intent.putExtra(ConstantUtils.EVENT_KEY, eventObject);
//                    startActivityForResult(intent, REQUEST_CODE);
//                }
//            });
        }
    }


    public void applyFilter() {
//        selectedCategory;
        if (selectedCategory.equalsIgnoreCase("1")) {//sort by votes, descending order

        } else if (selectedCategory.equalsIgnoreCase("2")) {////sort by upload date, descending order

        }
    }

//    public void setupFilterOptions() {
//        filterOptions = new ArrayList<>();
//        Category category1 = new Category();
//        category1.setCategoryId("1");
//        category1.setCategoryImageUrl("asdasdasd");
//        category1.setCategoryName("votes");
//        category1.setCategoryNameAr("Most votes");
//        category1.setCategoryNameEn("Most votes");
//
//        filterOptions.add(category1);
//
//        Category category2 = new Category();
//        category2.setCategoryId("2");
//        category2.setCategoryImageUrl("asdasdasd");
//        category2.setCategoryName("upload_time");
//        category2.setCategoryNameAr("Recently uploaded");
//        category2.setCategoryNameEn("Recently uploaded");
//
//        filterOptions.add(category1);
//
//    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        if (frameLayout.getVisibility() == View.VISIBLE) {
            frameLayout.setVisibility(View.GONE);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public void onPhotoClicked(int index) {
        Intent intent = new Intent(this, PhotoDetailFragment.class);
        intent.putExtra(ConstantUtils.EVENT_PHOTO, eventObject.getEventPhotosList().get(index));
        startActivity(intent);


    }

    public boolean isServiceCalled = false;

    @Override
    public void onVoteUnVoteClick(EventPhoto eventPhoto, boolean isChecked, int index) {
        status = sharedPrefrencesManger.getUserProfileObject().getMembershipData().getStatus();
        if (status != ConstantUtils.inValidMembership && status != 4) {
            if (!isServiceCalled) {
                this.updatedEventPhoto = eventPhoto;
                this.updatedIndex = index;
                isServiceCalled = true;
                onCallLikeUnlikeService(eventPhoto.getEntryId(), isChecked);
            }
        }
    }

    @Override
    public void selectSort(String sortValue, int index) {
        this.selectedSortIndex = index;
        this.sortValue = sortValue;
        if (index == -1) {
            resetBtn.setEnabled(false);
            resetBtn.setAlpha(0.3f);
        } else {
            resetBtn.setEnabled(true);
            resetBtn.setAlpha(1.0f);
        }
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        if (responseModel != null && responseModel.getServiceType() != null) {
            switch (responseModel.getServiceType()) {
                case ServiceUtils.getEventDetailsService:
                    onBackPressed();

                    break;
                case ServiceUtils.likeUnlikeEntryService:
                    onBackPressed();
                    isServiceCalled = false;
                    break;
                case ServiceUtils.EventsFilter:

                    break;

            }

        }
        if (frameLayout.getVisibility() == View.VISIBLE) {
            frameLayout.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            onConsumeService();
        }
    }

    private void disableBuyButton() {
        btn_buyTickets.setVisibility(View.VISIBLE);
        btn_buyTickets.setEnabled(false);
        btn_buyTickets.setAlpha(0.5f);
    }

    private void enableBuyButton() {
        btn_buyTickets.setVisibility(View.VISIBLE);
        btn_buyTickets.setEnabled(true);
        btn_buyTickets.setAlpha(1.0f);
    }

    private void disableUploadButton() {
        uploadBtn.setVisibility(View.VISIBLE);
        uploadBtn.setEnabled(false);
        uploadBtn.setAlpha(0.5f);
    }

    private void enableUploadButton() {

        uploadBtn.setVisibility(View.VISIBLE);
        uploadBtn.setEnabled(true);
        uploadBtn.setAlpha(1.0f);
    }

    private void disableButtons() {
        disableBuyButton();
        disableUploadButton();
    }

    private void enableButtons() {
        enableBuyButton();
        enableUploadButton();
    }


}
