package com.indy.views.fragments.gamification.adapters;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.indy.R;
import com.indy.utils.SharedPrefrencesManger;
import com.indy.views.fragments.gamification.OnBadgeListClicked;
import com.indy.views.fragments.gamification.models.getbadgeslist.Badge;
import com.indy.views.fragments.gamification.models.getbadgeslist.BadgeCategory;

import java.util.List;

public class BadgesListAdapter extends RecyclerView.Adapter<BadgesListAdapter.ViewHolder> implements OnBadgeListClicked {

    private Context mContext;
    private SharedPrefrencesManger sharedPrefrencesManger;
    private List<BadgeCategory> badgeCategories;
    private OnBadgeListClicked onBadgeListClicked;

    public BadgesListAdapter(List<BadgeCategory> badgeCategories, Context context, OnBadgeListClicked onBadgeListClicked) {

        this.mContext = context;
        if(context!=null)
        this.sharedPrefrencesManger = new SharedPrefrencesManger(context);
        this.badgeCategories = badgeCategories;
        this.onBadgeListClicked = onBadgeListClicked;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.item_badges_list, viewGroup, false);
        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        BadgeCategory badgeCategory = badgeCategories.get(position);

        holder.rv_badges.setLayoutManager(new GridLayoutManager(mContext, 2));
        holder.rv_badges.setHasFixedSize(true);
        BadgesAdapter mBadgesAdapter = new BadgesAdapter(badgeCategory.getBadges(), mContext, this);
        holder.rv_badges.setAdapter(mBadgesAdapter);
        setRecyclerViewHeight(holder.rv_badges);
//        if (sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)) {
//            holder.tv_title.setText(badgeCategory.getCategoryNameEn());
//        } else {
            holder.tv_title.setText(badgeCategory.getCategoryNameEn());
//        }

        holder.tv_count.setText(badgeCategory.getBadgesCompleted() + "/" + badgeCategory.getBadgesCount());
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return badgeCategories.size();
    }

    @Override
    public void onBadgeClicked(Badge badge,int position) {
        onBadgeListClicked.onBadgeClicked(badge,position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_title, tv_count;
        RecyclerView rv_badges;

        public ViewHolder(View v) {
            super(v);
            tv_title = (TextView) v.findViewById(R.id.tv_title);
            tv_count = (TextView) v.findViewById(R.id.tv_count);
            rv_badges = (RecyclerView) v.findViewById(R.id.rv_badges);
        }
    }

    public void setRecyclerViewHeight(RecyclerView recyclerView) {
        double total = recyclerView.getAdapter().getItemCount();
        double rows = Math.ceil(total / 2);
        int viewHeight = (int) mContext.getResources().getDimension(R.dimen.x180) * ((int) rows);
        viewHeight += recyclerView.getAdapter().getItemCount();
        recyclerView.getLayoutParams().height = viewHeight;
    }
}
