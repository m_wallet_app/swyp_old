package com.indy.views.fragments.buynewline.exisitingnumber;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.indy.R;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.buynewline.NewNumberFragment;
import com.indy.views.fragments.utils.MasterFragment;

import org.jetbrains.annotations.Nullable;


public class ReserveCreditDueMigrationFailedFragment extends MasterFragment {
    private View view;
    private Button newNoBtn, bt_tryAgain;
    private TextView mobileNo;
    private String mobileNotxt;
    public static ReserveCreditDueMigrationFailedFragment coorporateNumberMigrationFragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_reserve_credit_due_migration_payment, container, false);
        initUI();
        setListeners();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
        regActivity.setHeaderTitle("");
        regActivity.showHeaderLayout();
        newNoBtn = (Button) view.findViewById(R.id.newNoID);
        bt_tryAgain = (Button) view.findViewById(R.id.bt_try_again);
        mobileNo = (TextView) view.findViewById(R.id.mobileNo);
        coorporateNumberMigrationFragment = this;
        if (getArguments().getString(ConstantUtils.mobileNo, "") != null)
            mobileNo.setText(getArguments().getString(ConstantUtils.mobileNo, ""));
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
    }

    private void setListeners() {
        newNoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment[] myFragments = {ReserveCreditDueMigrationFailedFragment.coorporateNumberMigrationFragment,
                        NumberMigrationFragment.numberMigrationFragmentInstance,
                        NumberTransferFragment.numberTransferFragment,
                        NewNumberFragment.newNumberFragment
                };
                regActivity.removeFragment(myFragments);
                regActivity.replaceFragmnet(new NewNumberFragment(), R.id.frameLayout, true);
            }
        });
        bt_tryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onConsumeService();
            }
        });
    }
}

