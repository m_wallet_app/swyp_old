package com.indy.views.fragments.buynewline.exisitingnumber;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.controls.ServiceUtils;
import com.indy.models.MigrateEtisaltNumber.MigrateEtisalatNumerOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterInputResponse;
import com.indy.services.LogApplicationFlowService;
import com.indy.services.NumberMigrationService;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.buynewline.NewNumberFragment;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.indy.views.fragments.utils.MasterFragment;

import static com.indy.views.activites.RegisterationActivity.logEventInputModel;

/**
 * Created by emad on 9/25/16.
 */

public class EligableNumberMigrationFragment extends MasterFragment {

    private View view;
    private Button migrateNo, newNo;

    private MasterInputResponse masterInputResponse;
    private MigrateEtisalatNumerOutput migrateEtisalatNumerOutput;

    private TextView tv_title;
    String mobileNumber = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.eligable_number_migration, container, false);
        initUI();
        setListeners();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
        regActivity.setHeaderTitle("");
        regActivity.showHeaderLayout();
        migrateNo = (Button) view.findViewById(R.id.migrateNoBtn);
        newNo = (Button) view.findViewById(R.id.newNoBtn);

        tv_title = (TextView) view.findViewById(R.id.title_number_migration);
        String textToReplace = tv_title.getText().toString().trim();
        if (getArguments() != null && getArguments().getString(ConstantUtils.mobileNo) != null) {
            mobileNumber = getArguments().getString(ConstantUtils.mobileNo);

            textToReplace = textToReplace + "\n" + mobileNumber;
            tv_title.setText(textToReplace);


        }

    }

    private void setListeners() {

        migrateNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onConsumeService();
                logEvent(1);
//                regActivity.replaceFragmnet(new MigratingNumberCodeFragment(), R.id.frameLayout, true);

            }
        });

        newNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logEvent(2);
                regActivity.replaceFragmnet(new NewNumberFragment(), R.id.frameLayout, true);
            }
        });
    }

    private void showLoadingFragment() {
        Bundle bundle = new Bundle();
//        bundle.putString(ConstantUtils.loadingString, getString(R.string.migrating_number));
        LoadingFragmnet loadingFragmnet = new LoadingFragmnet();
        loadingFragmnet.setArguments(bundle);
        regActivity.addFragmnet(loadingFragmnet, R.id.loadingFragmeLayout, true);
    }


    @Override
    public void onConsumeService() {
        super.onConsumeService();
        showLoadingFragment();
        masterInputResponse = new MasterInputResponse();
        masterInputResponse.setAppVersion(appVersion);
        masterInputResponse.setToken(token);
        masterInputResponse.setOsVersion(osVersion);
        masterInputResponse.setChannel(channel);
        masterInputResponse.setDeviceId(deviceId);
        masterInputResponse.setMsisdn(getArguments().getString(ConstantUtils.mobileNo));
        new NumberMigrationService(EligableNumberMigrationFragment.this, masterInputResponse);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            if (isVisible()) {
                if(responseModel!=null && responseModel.getServiceType()!=null && responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.LOG_USER_EVENT)){

                }else {
                    regActivity.onBackPressed();
                    if (responseModel != null && responseModel.getResultObj() != null) {
                        migrateEtisalatNumerOutput = (MigrateEtisalatNumerOutput) responseModel.getResultObj();
//            if (migrateEtisalatNumerOutput != null)
                        onNumberMigrationResponse();
                        Bundle bundle2 = new Bundle();

                        bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_migration_confirmed);
                        bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_migration_confirmed);
                        mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_migration_confirmed, bundle2);
                    }
                }

            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    private void onNumberMigrationResponse() {
        if (migrateEtisalatNumerOutput.getErrorCode() != null)
            onNumberMigrationError();
        else
            onNumberMigrationSucess();
    }

    private void onNumberMigrationError() {

        if (currentLanguage != null) {
            if (migrateEtisalatNumerOutput != null
                    && migrateEtisalatNumerOutput.getErrorMsgEn() != null) {
                if (currentLanguage.equals("en")) {
                    showErrorFragment(migrateEtisalatNumerOutput.getErrorMsgEn());
                } else {
                    showErrorFragment(migrateEtisalatNumerOutput.getErrorMsgAr());
                }
            } else {
                showErrorFragment(getString(R.string.generice_error));
            }
        } else {
            if (sharedPrefrencesManger.getLanguage().equals("en")) {
                if (migrateEtisalatNumerOutput != null
                        && migrateEtisalatNumerOutput.getErrorMsgEn() != null) {

                    showErrorFragment(migrateEtisalatNumerOutput.getErrorMsgEn());

                } else {
                    showErrorFragment(getString(R.string.generice_error));
                }
            } else {
                if (migrateEtisalatNumerOutput != null
                        && migrateEtisalatNumerOutput.getErrorMsgAr() != null) {

                    showErrorFragment(migrateEtisalatNumerOutput.getErrorMsgAr());

                } else {
                    showErrorFragment(getString(R.string.generice_error));
                }
            }
        }

//        showErrorFragment(migrateEtisalatNumerOutput.getErrorMsgEn());
    }

    private void onNumberMigrationSucess() {
//        if (migrateEtisalatNumerOutput.getGeneratedCode() != null
//                && !migrateEtisalatNumerOutput.getGeneratedCode().isEmpty()) {
        Fragment migrateFrag = new MigratingNumberCodeFragment();
        Bundle bundle = new Bundle();
//            bundle.putString("CODE", migrateEtisalatNumerOutput.getGeneratedCode());
        bundle.putString(ConstantUtils.mobileNo, mobileNumber);
        migrateFrag.setArguments(bundle);
        regActivity.replaceFragmnet(migrateFrag, R.id.frameLayout, true);
//        } else {
//            onNumberMigrationError();
//        }


    }

    private void showErrorFragment(String error) {
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, error);
        ErrorFragment errorFragment = new ErrorFragment();
        regActivity.replaceFragmnet(errorFragment, R.id.frameLayout, false);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        if (isVisible()) {
            if (responseModel != null && responseModel.getServiceType() != null && responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.LOG_USER_EVENT)) {

            } else {
                regActivity.onBackPressed();
            }
        }
    }

    private void logEvent(int index) {

        logEventInputModel.setScreenId(ConstantUtils.SCREEN_ID_3005);

        if (index == 0) {
            logEventInputModel.setMigratedNumber(migrateNo.getText().toString());
        } else {
            logEventInputModel.setIsEligibleNewNumber(newNo.getText().toString());

        }

        new LogApplicationFlowService(this, logEventInputModel);
    }
}
