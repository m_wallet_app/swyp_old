package com.indy.views.fragments.gamification.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.BaseServiceManger;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.gamification.models.eventfilters.EventFilerInput;
import com.indy.views.fragments.gamification.models.eventfilters.EventFilterOutput;
import com.indy.views.fragments.gamification.models.serviceInputOutput.LikeUnlikeEntryInputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.LikeUnlikeEntryOutputModel;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**

 */
public class GetEventFilterService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private EventFilerInput eventFilerInput;


    public GetEventFilterService(BaseInterface baseInterface, EventFilerInput eventFilerInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.eventFilerInput = eventFilerInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<EventFilterOutput> call = apiService.getEventFilter(eventFilerInput);
        call.enqueue(new Callback<EventFilterOutput>() {
            @Override
            public void onResponse(Call<EventFilterOutput> call, Response<EventFilterOutput> response) {
                mBaseResponseModel.setServiceType(ServiceUtils.EventsFilter);
                mBaseResponseModel.setResultObj(response.body());

                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (response.body().getErrorMsgEn() != null) {
//                        String urlForTag = call.request().url().toString();
//                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
//                        urlForTag = urlForTag.replace("/", "_");
//                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<EventFilterOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.EventsFilter);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
