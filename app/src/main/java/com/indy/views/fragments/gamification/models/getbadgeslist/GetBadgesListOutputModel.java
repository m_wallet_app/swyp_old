package com.indy.views.fragments.gamification.models.getbadgeslist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

import java.util.List;

/**
 * Created by Tohamy on 10/1/2017.
 */

public class GetBadgesListOutputModel extends MasterErrorResponse {
    @SerializedName("badgeCategories")
    @Expose
    private List<BadgeCategory> badgeCategories = null;

    public List<BadgeCategory> getBadgeCategories() {
        return badgeCategories;
    }

    public void setBadgeCategories(List<BadgeCategory> badgeCategories) {
        this.badgeCategories = badgeCategories;
    }

}
