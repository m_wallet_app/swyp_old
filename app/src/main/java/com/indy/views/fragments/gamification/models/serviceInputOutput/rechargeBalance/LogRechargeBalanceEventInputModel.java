package com.indy.views.fragments.gamification.models.serviceInputOutput.rechargeBalance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GamificationBaseInputModel;

/**
 * Created by mobile on 05/11/2017.
 */

public class LogRechargeBalanceEventInputModel extends GamificationBaseInputModel {

    @SerializedName("amount")
    @Expose
    private double amount;

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
