package com.indy.views.fragments.rewards;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indy.R;
import com.indy.models.utils.BaseResponseModel;
import com.indy.views.fragments.utils.MasterFragment;

/**
 * Created by Amir.jehangir on 10/25/2016.
 */
public class RewardDetailsFragment extends MasterFragment {

    View rootView;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
rootView = inflater.inflate(R.layout.reward_details_fragment,container,true );
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initUI();
    }

    @Override
    public void initUI() {
        super.initUI();



    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.useVoucherID:

                Log.v("oooooooops", "oooops");
                break;
        }
    }




}
