package com.indy.views.fragments.gamification.models.viewluckydealservice;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GamificationBaseInputModel;

/**
 * Created by Tohamy on 10/9/2017.
 */

public class ViewLuckyDealInput extends GamificationBaseInputModel {

    @SerializedName("rewardItemId")
    @Expose
    private String rewardItemId;

    public String getRewardItemId() {
        return rewardItemId;
    }

    public void setRewardItemId(String rewardItemId) {
        this.rewardItemId = rewardItemId;
    }
}
