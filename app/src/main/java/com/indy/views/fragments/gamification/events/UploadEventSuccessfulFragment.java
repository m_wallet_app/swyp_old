package com.indy.views.fragments.gamification.events;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.indy.R;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.utils.MaterialCheckBox;
import com.indy.views.fragments.gamification.models.EventsList;
import com.indy.views.fragments.utils.MasterFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class UploadEventSuccessfulFragment extends MasterFragment {

    View rootView;
    Button shareButton;
    TextView tv_cancel;
    private EventsList eventObject;
    private String uploadedEntryId;
    private String totalWinner;
    private TextView tv_totalTickets;
    private TextView tv_rewardAmount;
    public UploadEventSuccessfulFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_upload_event_successful, container, false);
        eventObject = (EventsList) getActivity().getIntent().getParcelableExtra(ConstantUtils.EVENT_KEY);
        tv_totalTickets = (TextView) rootView.findViewById(R.id.tv_total_raffle_tickets);
        tv_rewardAmount = (TextView) rootView.findViewById(R.id.tv_rewards_count);
        uploadedEntryId = getArguments().getString(ConstantUtils.UPLOADED_ENTRY_ID);
        if (getArguments() != null && getArguments().getString(ConstantUtils.UPLOAD_SUCCESS_TEXT) != null) {
            totalWinner = getArguments().getString(ConstantUtils.UPLOAD_SUCCESS_TEXT);
            tv_totalTickets.setText(totalWinner);
            tv_rewardAmount.setText(getArguments().getString(ConstantUtils.UPLOAD_REWARD,""));
        }else{
            tv_totalTickets.setVisibility(View.INVISIBLE);
        }
        initUI();
        return rootView;
    }

    @Override
    public void initUI() {
        super.initUI();
        shareButton = (Button) rootView.findViewById(R.id.continueBtn);
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), PhotoDetailFragment.class);
                intent.putExtra(ConstantUtils.EVENT_KEY, eventObject);
                intent.putExtra(ConstantUtils.UPLOADED_ENTRY_ID, uploadedEntryId);
                intent.putExtra(ConstantUtils.IS_OWNER,true);
                startActivity(intent);
                getActivity().finish();
//                ((UploadEventPhotoActivity) getActivity()).replaceFragmnet(photoDetailFragment,R.id.frameLayout,true);
            }
        });


        tv_cancel = (TextView) rootView.findViewById(R.id.tv_cancel);
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
        MaterialCheckBox materialCheckBox = (MaterialCheckBox) rootView.findViewById(R.id.fl_tick_animation);
        ImageView iv_tick = (ImageView) rootView.findViewById(R.id.iv_tick);
        CommonMethods.drawCircle(materialCheckBox, iv_tick);
    }
}
