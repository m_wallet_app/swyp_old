package com.indy.views.fragments.gamification.models.serviceInputOutput.logPurchaseBundleEvent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GamificationBaseInputModel;

import java.util.List;

/**
 * Created by mobile on 05/11/2017.
 */

public class LogPurchaseBundleEventInputModel extends GamificationBaseInputModel {

    @SerializedName("rewardId")
    @Expose
    private String rewardId;

    @SerializedName("packageCodes")
    @Expose
    private List<String> packageCode;

    @SerializedName("id")
    @Expose
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRewardId() {
        return rewardId;
    }

    public void setRewardId(String rewardId) {
        this.rewardId = rewardId;
    }

    public List<String> getPackageCode() {
        return packageCode;
    }

    public void setPackageCode(List<String> packageCode) {
        this.packageCode = packageCode;
    }
}
