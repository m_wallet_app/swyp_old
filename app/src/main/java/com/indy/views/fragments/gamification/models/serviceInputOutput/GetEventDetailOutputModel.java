package com.indy.views.fragments.gamification.models.serviceInputOutput;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.views.fragments.gamification.models.EventPhoto;
import com.indy.views.fragments.gamification.models.EventsList;

import java.util.List;

/**
 * Created by mobile on 24/09/2017.
 */

public class GetEventDetailOutputModel extends MasterErrorResponse {

    @SerializedName("indyEvent")
    @Expose
    private EventsList indyEvent;
    @SerializedName("userEntry")
    @Expose
    private EventPhoto userEntry;
    @SerializedName("entriesList")
    @Expose
    private List<EventPhoto> entriesList = null;

    public EventsList getIndyEvent() {
        return indyEvent;
    }

    public void setIndyEvent(EventsList indyEvent) {
        this.indyEvent = indyEvent;
    }

    public EventPhoto getUserEntry() {
        return userEntry;
    }

    public void setUserEntry(EventPhoto userEntry) {
        this.userEntry = userEntry;
    }

    public List<EventPhoto> getEntriesList() {
        return entriesList;
    }

    public void setEntriesList(List<EventPhoto> entriesList) {
        this.entriesList = entriesList;
    }
}
