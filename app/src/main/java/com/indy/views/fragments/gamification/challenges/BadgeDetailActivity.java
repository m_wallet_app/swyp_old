package com.indy.views.fragments.gamification.challenges;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.indy.R;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.GetRewardForSharingService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.ShareMasterActivity;
import com.indy.views.fragments.gamification.models.getbadgesdetails.BadgeDetails;
import com.indy.views.fragments.gamification.models.getbadgesdetails.GetBadgeDetailsInputModel;
import com.indy.views.fragments.gamification.models.getbadgesdetails.GetBadgeDetailsOutputModel;
import com.indy.views.fragments.gamification.models.getbadgeslist.Badge;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetRewardForSharingInputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetRewardForSharingOutputResponse;
import com.indy.views.fragments.gamification.services.GetBadgeDetailsService;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import static com.indy.R.id.ll_badge_unlocked;

public class BadgeDetailActivity extends ShareMasterActivity {

    TextView tv_badgeName, tv_badgeDetail, tv_rewardsText, tv_rewardsAmount, tv_badgeDescription,
            tv_dateUnlocked, tv_rewardUnlocked;
    ImageView iv_badgeDetail, iv_badgeDetail2;
    SeekBar sb_badgeProgress;
    private Badge badge;

    private GetBadgeDetailsOutputModel getBadgeDetailsOutputModel;
    private Button bt_share;
    private FrameLayout frameLayout;
    private LinearLayout ll_unlockedBadge, ll_lockedBadge;
    private TextView fbShare;
    private TextView twitterShare;

    private CoordinatorLayout coordinatorLayout;
    private BottomSheetBehavior behavior;
    private View bottomSheet;
    private String badgeCategoryName;
    private String stringToShare = "";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_badge_detail);
        badge = (Badge) getIntent().getParcelableExtra(ConstantUtils.KEY_BADGE_DETAIL);
        badgeCategoryName = "";
        initUI();
    }

    @Override
    public void initUI() {
        super.initUI();
        TextView headerText = (TextView) findViewById(R.id.titleTxt);
        headerText.setText(getString(R.string.badge_details));
        //..............header Buttons............
        RelativeLayout backLayout = (RelativeLayout) findViewById(R.id.backLayout);
        RelativeLayout helpLayout = (RelativeLayout) findViewById(R.id.helpLayout);
        helpLayout.setVisibility(View.INVISIBLE);

        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard();
                onBackPressed();
            }
        });

        if(badgeCategoryName!=null){
            switch(badgeCategoryName){
//                case ConstantUtils.BADGE_CATEGORY_SHARE:
//                    stringToShare = getString(R.string.badge_share_message_share);
//                    break;
//                case ConstantUtils.BADGE_CATEGORY_CONTEST_PARTICIPATION:
//                    stringToShare = getString(R.string.badge_share_message_contest_participation);
//                    break;
//                case ConstantUtils.BADGE_CATEGORY_PACKAGE_PURCHASE:
//                    stringToShare = getString(R.string.badge_share_message_package_purchase);
//                    break;
//                case ConstantUtils.BADGE_CATEGORY_CHECK_IN:
//                    stringToShare = getString(R.string.badge_share_message_check_in);
//                    break;
                default:
                    stringToShare="#swyp";
                    break;
            }
        }
        //..............header Buttons............
        tv_badgeDetail = (TextView) findViewById(R.id.tv_badge_detail);
        tv_badgeName = (TextView) findViewById(R.id.tv_badge_name);
        tv_rewardsText = (TextView) findViewById(R.id.tv_progress_text);
        tv_rewardsAmount = (TextView) findViewById(R.id.tv_rewards_count);
        iv_badgeDetail = (ImageView) findViewById(R.id.iv_badge_detail);
        sb_badgeProgress = (SeekBar) findViewById(R.id.sb_badge_progress);
        tv_badgeDescription = (TextView) findViewById(R.id.tv_badge_description);
        tv_dateUnlocked = (TextView) findViewById(R.id.tv_date_unlocked);
        tv_rewardUnlocked = (TextView) findViewById(R.id.tv_reward_unlocked);
        ll_unlockedBadge = (LinearLayout) findViewById(ll_badge_unlocked);
        ll_lockedBadge = (LinearLayout) findViewById(R.id.ll_locked_progress);
        bt_share = (Button) findViewById(R.id.btn_share);
        iv_badgeDetail2 = (ImageView) findViewById(R.id.iv_badge_detail_dummy);
        sb_badgeProgress.setEnabled(false);
        frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
        frameLayout.setVisibility(View.VISIBLE);
        bt_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onConsumeGetRewardService();
            }
        });


        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinateLayoutID);
        bottomSheet = coordinatorLayout.findViewById(R.id.share_photo_bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {

                } else if (newState == BottomSheetBehavior.STATE_EXPANDED) {

                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });
        fbShare = (TextView) findViewById(R.id.fb_share);
        twitterShare = (TextView) findViewById(R.id.twitter_share);
        fbShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                referenceID = badge.getBadgeId();
                itemType = ConstantUtils.BADGE_TYPE;

//                BitmapDrawable drawable = (BitmapDrawable) iv_badgeDetail.getDrawable();
//                Bitmap bitmap = drawable.getBitmap();
                if (getBadgeDetailsOutputModel.getBadgeDetails().getImageUrlInProgress() != null && !getBadgeDetailsOutputModel.getBadgeDetails().getImageUrlInProgress().isEmpty())
                    ImageLoader.getInstance().displayImage(getBadgeDetailsOutputModel.getBadgeDetails().getImageToShareUrl(), iv_badgeDetail2, new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {

                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            setBitmapToShare(null);
                            facebookLogin(true, getBadgeDetailsOutputModel.getBadgeDetails().getBadgeId(),stringToShare);

                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            setBitmapToShare(loadedImage);
                            facebookLogin(true, getBadgeDetailsOutputModel.getBadgeDetails().getBadgeId(),stringToShare);

                        }

                        @Override
                        public void onLoadingCancelled(String imageUri, View view) {

                        }
                    });
//                facebookShare(getBadgeDetailsOutputModel.getBadgeDetails().getBadgeId());
            }
        });
        twitterShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                referenceID = badge.getBadgeId();
                itemType = ConstantUtils.BADGE_TYPE;
                if (getBadgeDetailsOutputModel.getBadgeDetails().getImageUrlInProgress() != null && !getBadgeDetailsOutputModel.getBadgeDetails().getImageUrlInProgress().isEmpty())
                    ImageLoader.getInstance().displayImage(getBadgeDetailsOutputModel.getBadgeDetails().getImageToShareUrl(), iv_badgeDetail2, new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {

                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            setBitmapToShare(null);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                if (checkSelfPermission(Manifest.permission_group.STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                    String[] newPerms = {Manifest.permission.READ_EXTERNAL_STORAGE,
                                            Manifest.permission.WRITE_EXTERNAL_STORAGE};
                                    requestPermissions(newPerms, 2000);
                                }
                                ;
                            } else {
                                twitterLogin(true, getBadgeDetailsOutputModel.getBadgeDetails().getBadgeId(),stringToShare);
                            }

                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            setBitmapToShare(loadedImage);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                if (checkSelfPermission(Manifest.permission_group.STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                    String[] newPerms = {Manifest.permission.READ_EXTERNAL_STORAGE,
                                            Manifest.permission.WRITE_EXTERNAL_STORAGE};
                                    requestPermissions(newPerms, 2000);
                                }
                                ;
                            } else {
                                twitterLogin(true, getBadgeDetailsOutputModel.getBadgeDetails().getBadgeId(),stringToShare);
                            }
                        }

                        @Override
                        public void onLoadingCancelled(String imageUri, View view) {

                        }
                    });


            }
        });

        onConsumeService();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            twitterLogin(true, getBadgeDetailsOutputModel.getBadgeDetails().getBadgeId(),stringToShare);
        } else {

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sharedPrefrencesManger.getNavigationIndex().length() > 0 || sharedPrefrencesManger.getNavigationBadgeIndex().length() >0) {

            finish();
        }
    }

    private void onGetShareRewardSuccess(String amountString) {
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        if (amountString.length() > 0) {
            ((TextView) findViewById(R.id.tv_reward_text)).setText(getString(R.string.share_your_achievement) + " " +
                    getString(R.string.and_get_rewarded_with) + amountString);
        } else {
            ((TextView) findViewById(R.id.tv_reward_text)).setText(getString(R.string.share_your_achievement));
        }
    }


    public void setupBadge() {
        BadgeDetails badgeDetails = getBadgeDetailsOutputModel.getBadgeDetails();
        if (badgeDetails != null) {
            if (badgeDetails.getCompleted()) {
                if (badgeDetails.getImageUrlCompleted() != null && !badgeDetails.getImageUrlCompleted().isEmpty())
                    ImageLoader.getInstance().displayImage(badgeDetails.getImageUrlCompleted(), iv_badgeDetail);
                ll_lockedBadge.setVisibility(View.GONE);
                ll_unlockedBadge.setVisibility(View.VISIBLE);
                tv_dateUnlocked.setText(getString(R.string.unlocked) + " " + CommonMethods.parseDateWithHoursMonthInFront(badgeDetails.getUnlockedDate(), currentLanguage));
                if(badgeDetails.getRewardAmount()!=null && badgeDetails.getRewardAmount().length()>0) {
                    tv_rewardUnlocked.setText(badgeDetails.getRewardAmount());
                }else{
                    ((LinearLayout) findViewById(R.id.ll_reward)).setVisibility(View.GONE);
                }
//                if(badgeDetails.getDescription2().length()>0){
//                    if(badgeDetails.getDescription2().substring(badgeDetails.getDescription2().length()-2,badgeDetails.getDescription2().length()-1).equalsIgnoreCase(".")){
//                        badgeDetails.setDescription2(badgeDetails.getDescription2().substring(0,badgeDetails.getDescription2().length()-2));
//
//                        badgeDetails.setDescription2(badgeDetails.getDescription2()+".");
//                    }
//
//
//                }
                try {
                    if (badgeDetails.getDescription2() != null)
                        badgeDetails.setDescription2(new String(badgeDetails.getDescription2().getBytes(), "UTF-8"));

                    Log.d("text", badgeDetails.getDescription2());
                    tv_badgeDetail.setText(badgeDetails.getDescription2());
                    Log.d("text_after", tv_badgeDetail.getText().toString());

//                    String name = "";
//                    try {
//                        name = new String(badgeDetails.getDescription2().getBytes(), "UTF-8");
//                    } catch (UnsupportedEncodingException e) {
//
//                        e.printStackTrace();
//                    }

                    byte[] byteString = badgeDetails.getDescription2().getBytes();
                    tv_badgeDetail.setText(new String(byteString, "UTF-8"));
//                    tv_badgeDetail.setText(tv_badgeDetail.getText().toString() + " " + getString(R.string.swypees));
//                    tv_badgeDetail.setText(Html.fromHtml(name).toString());
//                    tv_badgeDetail.setText(new String(badgeDetails.getDescription2().getBytes(), "UTF-8"));
                    if (badgeDetails.getDescription4() != null)
                        tv_badgeDescription.setText(new String(badgeDetails.getDescription4().getBytes(), "UTF-8"));
                } catch (Exception ex) {
                    ex.printStackTrace();
//                    tv_badgeDetail.setText(badgeDetails.getDescription2());
//                    tv_badgeDescription.setText(badgeDetails.getDescription4());
                }

            } else {
                if (badgeDetails.getImageUrlInProgress() != null && !badgeDetails.getImageUrlInProgress().isEmpty())
                    ImageLoader.getInstance().displayImage(badgeDetails.getImageUrlInProgress(), iv_badgeDetail);
                sb_badgeProgress.setMax(badgeDetails.getCompletionCount());
                sb_badgeProgress.setProgress(badgeDetails.getProgress());
                ll_unlockedBadge.setVisibility(View.GONE);
                ll_lockedBadge.setVisibility(View.VISIBLE);

//                tv_badgeDetail.setText(badgeDetails.getDescription1());
                tv_badgeDescription.setText(badgeDetails.getDescription3());
                try {
                    if (badgeDetails.getDescription1() != null)
                        tv_badgeDetail.setText(new String(badgeDetails.getDescription1().getBytes(), "UTF-8"));
                    if (badgeDetails.getDescription3() != null)
                        tv_badgeDescription.setText(new String(badgeDetails.getDescription3().getBytes(), "UTF-8"));
                } catch (Exception ex) {
                    ex.printStackTrace();
                    tv_badgeDetail.setText(badgeDetails.getDescription1());
                    tv_badgeDescription.setText(badgeDetails.getDescription3());
                }

            }

            tv_badgeName.setText(badgeDetails.getTitle());

            tv_rewardsText.setText(badgeDetails.getCompletionStatusMsg());
            if(badgeDetails.getRewardAmount()!=null && badgeDetails.getRewardAmount().length()>0) {
                tv_rewardsAmount.setText(badgeDetails.getRewardAmount());
            }else{
                ((LinearLayout)findViewById(R.id.ll_reward_locked)).setVisibility(View.GONE);
            }
        }
    }

    public void onConsumeGetRewardService() {
        addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        GetRewardForSharingInputModel getBadgeDetailsInputModel = new GetRewardForSharingInputModel();
        getBadgeDetailsInputModel.setChannel(channel);
        getBadgeDetailsInputModel.setLang(currentLanguage);
        getBadgeDetailsInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        getBadgeDetailsInputModel.setAppVersion(appVersion);
        getBadgeDetailsInputModel.setAuthToken(authToken);
        getBadgeDetailsInputModel.setDeviceId(deviceId);
        getBadgeDetailsInputModel.setToken(token);
        getBadgeDetailsInputModel.setUserSessionId(sharedPrefrencesManger.getUserSessionId());
        getBadgeDetailsInputModel.setUserId(sharedPrefrencesManger.getUserId());
        getBadgeDetailsInputModel.setApplicationId(ConstantUtils.INDY_TALOS_ID);
        getBadgeDetailsInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
        getBadgeDetailsInputModel.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        getBadgeDetailsInputModel.setOsVersion(osVersion);
        getBadgeDetailsInputModel.setActionName(ConstantUtils.badge_share);
        new GetRewardForSharingService(this, getBadgeDetailsInputModel);
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        GetBadgeDetailsInputModel getBadgeDetailsInputModel = new GetBadgeDetailsInputModel();
        getBadgeDetailsInputModel.setChannel(channel);
        getBadgeDetailsInputModel.setLang(currentLanguage);
        getBadgeDetailsInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        getBadgeDetailsInputModel.setAppVersion(appVersion);
        getBadgeDetailsInputModel.setAuthToken(authToken);
        getBadgeDetailsInputModel.setDeviceId(deviceId);
        getBadgeDetailsInputModel.setToken(token);
        getBadgeDetailsInputModel.setUserSessionId(sharedPrefrencesManger.getUserSessionId());
        getBadgeDetailsInputModel.setUserId(sharedPrefrencesManger.getUserId());
        getBadgeDetailsInputModel.setApplicationId(ConstantUtils.INDY_TALOS_ID);
        getBadgeDetailsInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
        getBadgeDetailsInputModel.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        getBadgeDetailsInputModel.setOsVersion(osVersion);
        getBadgeDetailsInputModel.setBadgeId(badge.getBadgeId());
        new GetBadgeDetailsService(this, getBadgeDetailsInputModel);
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        if (frameLayout.getVisibility() == View.VISIBLE) {
            frameLayout.setVisibility(View.GONE);
            super.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        onBackPressed();
        try {
            if (responseModel != null && responseModel.getResultObj() != null) {
                if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.SOCIAL_SHARE)) {

                } else if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.GET_BADGES_DETAILS)) {

                    getBadgeDetailsOutputModel = (GetBadgeDetailsOutputModel) responseModel.getResultObj();
                    if (getBadgeDetailsOutputModel != null && getBadgeDetailsOutputModel.getErrorCode() == null && getBadgeDetailsOutputModel.getBadgeDetails() != null) {
                        onGetBadgeDetailSuccess();
                    } else {
                        showErrorMessage();
                    }
                } else if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.getRewardForSharingService)) {
                    GetRewardForSharingOutputResponse getRewardForSharingOutputResponse = (GetRewardForSharingOutputResponse) responseModel.getResultObj();
                    if (getRewardForSharingOutputResponse.getRewardAmount() != null) {
                        onGetShareRewardSuccess(getRewardForSharingOutputResponse.getRewardAmount());
                    } else {
                        onGetShareRewardSuccess("");
                    }
                }
            } else {
                showErrorMessage();
            }
        } catch (Exception ex) {

        }
    }

    private void onGetBadgeDetailSuccess() {
        setupBadge();
    }


//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//        if(frameLayout.getVisibility()==View.VISIBLE){
//            frameLayout.setVisibility(View.GONE);
//        }else{
//            super.onBackPressed();
//        }
//    }


    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        onBackPressed();
        if (responseModel.getServiceType() != null && responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.getRewardForSharingService)) {
            onGetShareRewardSuccess("");
        }
    }

    private void showErrorMessage() {

        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        if (getBadgeDetailsOutputModel != null && getBadgeDetailsOutputModel.getErrorMsgEn() != null) {
            if (sharedPrefrencesManger != null && sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)) {
                bundle.putString(ConstantUtils.errorString, getBadgeDetailsOutputModel.getErrorMsgEn());

            } else {
                bundle.putString(ConstantUtils.errorString, getBadgeDetailsOutputModel.getErrorMsgAr());

            }
        } else {
            bundle.putString(ConstantUtils.errorString, getString(R.string.generice_error));

        }
        errorFragment.setArguments(bundle);
        addFragmnet(errorFragment, R.id.frameLayout, true);
        frameLayout.setVisibility(View.VISIBLE);
    }


}
