package com.indy.views.fragments.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.customviews.CustomButton;
import com.indy.customviews.CustomEditText;
import com.indy.helpers.ValidationHelper;
import com.indy.models.createnewaccount.CreateNewAccountInput;
import com.indy.models.createnewaccount.CreateNewAccountOutput;
import com.indy.models.login.LoginOutputModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.SetUserCredinalsService;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.HelpActivity;
import com.indy.views.activites.RegisterationActivity;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.indy.views.fragments.utils.MasterFragment;

/**
 * Created by emad on 9/7/16.
 */
public class SetNewPasswordFragment extends MasterFragment {
    private View view;
    private CustomButton loginBtn;
    private CustomEditText new_passwrd, confirm_new_password;
    private TextView help;
    private CreateNewAccountInput changePasswordInput;
    private Button bt_okConfirmation;
    LinearLayout ll_main, ll_confirmation;
    private CreateNewAccountOutput changePasswordOutput;
    private TextInputLayout til_password, til_confirmPassword;
    public static Intent service;
    public static LoginOutputModel loginOutputModel;
    BaseResponseModel mBaseResponseModel;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_set_new_password, container, false);
        initUI();
        setListeners();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();

        new_passwrd = (CustomEditText) view.findViewById(R.id.newPassword);
        confirm_new_password = (CustomEditText) view.findViewById(R.id.confirm_new_password);
        loginBtn = (CustomButton) view.findViewById(R.id.loginBtn);
        help = (TextView) view.findViewById(R.id.help);
        til_password = (TextInputLayout) view.findViewById(R.id.password_input_layout);
        til_confirmPassword = (TextInputLayout) view.findViewById(R.id.confirm_password_input_layout);
        ll_confirmation = (LinearLayout) view.findViewById(R.id.ll_confirmation_message);
        ll_confirmation.setVisibility(View.GONE);
        bt_okConfirmation = (Button) view.findViewById(R.id.okBtn);
        bt_okConfirmation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                getActivity().finish();
//
//                Intent intent = new Intent(getActivity(), RegisterationActivity.class);
//                intent.putExtra("fragIndex", "7");
//                startActivity(intent);
                onLogin();
            }
        });
        ll_main = (LinearLayout) view.findViewById(R.id.ll_main_content_view);
        ll_main.setVisibility(View.VISIBLE);
        ll_confirmation.setVisibility(View.GONE);
//        backImg = (Button) view.findViewById(R.id.backImg);
//        backImg.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                regActivity.onBackPressed();
//            }
//        });
        //..............header Buttons............
        RelativeLayout backLayout = (RelativeLayout) view.findViewById(R.id.backLayout);
        RelativeLayout helpLayout = (RelativeLayout) view.findViewById(R.id.helpLayout);

        helpLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), HelpActivity.class);
                startActivity(intent);
            }
        });

        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard();
                getActivity().onBackPressed();
            }
        });
        //..............header Buttons............

        disableNextBtn();
    }

    private void setListeners() {
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()) {

                    onConsumeService();
                }

            }
        });
//        help.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onHelp();
//            }
//        });

        confirm_new_password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (confirm_new_password.getText().toString().trim().length() > 0 && new_passwrd.getText().toString().trim().length() > 0) {
                    enableNextBtn();
                } else {
                    disableNextBtn();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        new_passwrd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (new_passwrd.getText().toString().trim().length() > 0 && confirm_new_password.getText().toString().trim().length() > 0) {
                    enableNextBtn();
                } else {
                    disableNextBtn();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    private void disableNextBtn() {
        loginBtn.setAlpha(0.5f);
        loginBtn.setEnabled(false);
    }

    private void enableNextBtn() {
        loginBtn.setAlpha(1.0f);
        loginBtn.setEnabled(true);
    }

    private void onLogin() {
        if (changePasswordOutput.getUpdated()) {
//            Fragment[] myFragments = {new SetNewPasswordFragment(),
//                    new ForgotPasswordFragment(),
//                    new ForgotPasswordCodeFragment(),
//                    new LoginFragment()
//            };
//
//            regActivity.removeFragment(myFragments);
            regActivity.finish();
            Intent intent = new Intent(getActivity(), RegisterationActivity.class);
            intent.putExtra("fragIndex", "7");
            startActivity(intent);
//            Fragment loginFrag = new LoginFragment();
//            Bundle bundle = new Bundle();
//            bundle.putBoolean(ConstantUtils.showBackBtn, true);
//            loginFrag.setArguments(bundle);
//            regActivity.replaceFragmnet(loginFrag, R.id.frameLayout, true);
        } else {
            showErrorFragment(getString(R.string.passwored_set_up_error));
        }
    }


    private void showErrorFragment(String error) {
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, error);
        errorFragment.setArguments(bundle);
        regActivity.replaceFragmnet(errorFragment, R.id.frameLayout, true);
    }

    private void onHelp() {
        Intent intent = new Intent(getActivity(), HelpActivity.class);
        startActivity(intent);
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        regActivity.addFragmnet(new LoadingFragmnet(), R.id.loadingFragmeLayout, true);
        changePasswordInput = new CreateNewAccountInput();
        changePasswordInput.setAppVersion(appVersion);
        changePasswordInput.setToken(token);
        changePasswordInput.setOsVersion(osVersion);
        changePasswordInput.setChannel(channel);
        changePasswordInput.setDeviceId(deviceId);
//        if (getArguments().getString("VCTAG", "") != null) {
//            changePasswordInput.setMsisdn(getArguments().getString("VCTAG", ""));
//        }

        if (ForgotPasswordCodeFragment.mobileNo != null) {
            changePasswordInput.setMsisdn(ForgotPasswordCodeFragment.mobileNo);
        }
        String newPass = ValidationHelper.getMd5(new_passwrd.getText().toString().trim());
        String confirmPass = ValidationHelper.getMd5(confirm_new_password.getText().toString().trim());
        changePasswordInput.setPassword(newPass.toUpperCase());
        changePasswordInput.setPasswordConfirmation(confirmPass.toUpperCase());
//        changePasswordInput.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        new SetUserCredinalsService(this, changePasswordInput);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            if(responseModel!=null && responseModel.getResultObj()!=null) {
                this.mBaseResponseModel = responseModel;
                if (isVisible()) {
                    Bundle bundle2 = new Bundle();

                    bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_forgot_password_changed_success);
                    bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_forgot_password_changed_success);
                    mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_forgot_password_changed_success, bundle2);
                    regActivity.onBackPressed();
                    if (responseModel != null && responseModel.getResultObj() != null) {
                        changePasswordOutput = (CreateNewAccountOutput) responseModel.getResultObj();
                        if (changePasswordOutput != null) {
                            if (changePasswordOutput.getErrorCode() != null)
                                onChangePasswordError();
                            else {

                                showConfirmationView();
//                    onLogin();
                            }
//                    onLoginSucess();
                        }
                    }
                }
            }
        }catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    public void showConfirmationView(){
            ll_confirmation.setVisibility(View.VISIBLE);
            ll_main.setVisibility(View.GONE);
    }

    private void onChangePasswordError() {
        showErrorFragment(changePasswordOutput.getErrorMsgEn());
    }


    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        try {
            if (isVisible())
                regActivity.onBackPressed();
        }catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

    }

    private boolean isValid() {
        boolean isValid = true;
        if (new_passwrd.getText().toString().trim().length() == 0) {
            setTextInputLayoutError(til_password, getString(R.string.please_enter_password));
            isValid = false;
        } else if (confirm_new_password.getText().toString().trim().length() == 0) {
            setTextInputLayoutError(til_confirmPassword, getString(R.string.please_confirm_password));
            isValid = false;
        }

        if (isValid) {
            if (!new_passwrd.getText().toString().trim().equals(confirm_new_password.getText().toString().trim())) {
                setTextInputLayoutError(til_confirmPassword, getString(R.string.password_didnot_match));
                //error fragment handling here
//                ErrorFragment errorFragment = new ErrorFragment();
//                Bundle bundle = new Bundle();
                isValid = false;
//                bundle.putString(ConstantUtils.errorString, getString(R.string.password_didnot_match));
//                errorFragment.setArguments(bundle);
//                regActivity.replaceFragmnet(errorFragment, R.id.frameLayout, true);
            } else {
                removeTextInputLayoutError(til_confirmPassword);
                if (isValid && (!ValidationHelper.isValidPassword(new_passwrd.getText().toString().trim()) || new_passwrd.getText().toString().trim().length() < 6)) {
                    removeTextInputLayoutError(til_confirmPassword);
                    setTextInputLayoutError(til_password, getString(R.string.password_criteria_not_matched));
                    isValid = false;
                } else if (isValid) {
                    removeTextInputLayoutError(til_password);
                }
            }
        }

        return isValid;
    }
}
