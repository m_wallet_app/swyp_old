package com.indy.views.fragments.gamification.models.getbadgesdetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GamificationBaseInputModel;

/**
 * Created by mobile on 10/10/2017.
 */

public class GetBadgeDetailsInputModel extends GamificationBaseInputModel {

    @SerializedName("badgeId")
    @Expose
    private String badgeId;

    public String getBadgeId() {
        return badgeId;
    }

    public void setBadgeId(String badgeId) {
        this.badgeId = badgeId;
    }
}
