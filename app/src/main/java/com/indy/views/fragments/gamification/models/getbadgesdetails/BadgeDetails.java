package com.indy.views.fragments.gamification.models.getbadgesdetails;

/**
 * Created by mobile on 10/10/2017.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BadgeDetails implements Parcelable {

    @SerializedName("badgeId")
    @Expose
    private String badgeId;
    @SerializedName("imageUrlCompleted")
    @Expose
    private String imageUrlCompleted;
    @SerializedName("imageUrlInProgress")
    @Expose
    private String imageUrlInProgress;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description1")
    @Expose
    private String description1;
    @SerializedName("description2")
    @Expose
    private String description2;
    @SerializedName("description3")
    @Expose
    private String description3;
    @SerializedName("description4")
    @Expose
    private String description4;
    @SerializedName("completed")
    @Expose
    private Boolean completed;
    @SerializedName("completionStatusMsg")
    @Expose
    private String completionStatusMsg;
    @SerializedName("progress")
    @Expose
    private Integer progress;
    @SerializedName("completionCount")
    @Expose
    private Integer completionCount;
    @SerializedName("unlockedDate")
    @Expose
    private String unlockedDate;
    @SerializedName("rewardName")
    @Expose
    private String rewardName;
    @SerializedName("rewardAmount")
    @Expose
    private String rewardAmount;
    @SerializedName("rewardImage")
    @Expose
    private String rewardImage;

    @SerializedName("imageToShareUrl")
    @Expose
    private String imageToShareUrl;
    public final static Parcelable.Creator<BadgeDetails> CREATOR = new Creator<BadgeDetails>() {


        @SuppressWarnings({
                "unchecked"
        })
        public BadgeDetails createFromParcel(Parcel in) {
            return new BadgeDetails(in);
        }

        public BadgeDetails[] newArray(int size) {
            return (new BadgeDetails[size]);
        }

    };

    private BadgeDetails(Parcel in) {
        this.badgeId = ((String) in.readValue((String.class.getClassLoader())));
        this.imageUrlCompleted = ((String) in.readValue((String.class.getClassLoader())));
        this.imageUrlInProgress = ((String) in.readValue((String.class.getClassLoader())));
        this.title = ((String) in.readValue((String.class.getClassLoader())));
        this.description1 = ((String) in.readValue((String.class.getClassLoader())));
        this.description2 = ((String) in.readValue((String.class.getClassLoader())));
        this.description3 = ((String) in.readValue((String.class.getClassLoader())));
        this.description4 = ((String) in.readValue((String.class.getClassLoader())));
        this.completed = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.completionStatusMsg = ((String) in.readValue((String.class.getClassLoader())));
        this.progress = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.completionCount = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.unlockedDate = ((String) in.readValue((String.class.getClassLoader())));
        this.rewardName = ((String) in.readValue((String.class.getClassLoader())));
        this.rewardAmount = ((String) in.readValue((String.class.getClassLoader())));
        this.rewardImage = ((String) in.readValue((String.class.getClassLoader())));
        this.imageToShareUrl = ((String) in.readValue(String.class.getClassLoader()));

    }

    public BadgeDetails() {
    }

    public String getBadgeId() {
        return badgeId;
    }

    public void setBadgeId(String badgeId) {
        this.badgeId = badgeId;
    }

    public String getImageUrlCompleted() {
        return imageUrlCompleted;
    }

    public void setImageUrlCompleted(String imageUrlCompleted) {
        this.imageUrlCompleted = imageUrlCompleted;
    }

    public String getImageUrlInProgress() {
        return imageUrlInProgress;
    }

    public void setImageUrlInProgress(String imageUrlInProgress) {
        this.imageUrlInProgress = imageUrlInProgress;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription1() {
        return description1;
    }

    public void setDescription1(String description1) {
        this.description1 = description1;
    }

    public String getDescription2() {
        return description2;
    }

    public void setDescription2(String description2) {
        this.description2 = description2;
    }

    public String getDescription3() {
        return description3;
    }

    public void setDescription3(String description3) {
        this.description3 = description3;
    }

    public String getDescription4() {
        return description4;
    }

    public void setDescription4(String description4) {
        this.description4 = description4;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public String getCompletionStatusMsg() {
        return completionStatusMsg;
    }

    public void setCompletionStatusMsg(String completionStatusMsg) {
        this.completionStatusMsg = completionStatusMsg;
    }

    public Integer getProgress() {
        return progress;
    }

    public void setProgress(Integer progress) {
        this.progress = progress;
    }

    public Integer getCompletionCount() {
        return completionCount;
    }

    public void setCompletionCount(Integer completionCount) {
        this.completionCount = completionCount;
    }

    public String getUnlockedDate() {
        return unlockedDate;
    }

    public void setUnlockedDate(String unlockedDate) {
        this.unlockedDate = unlockedDate;
    }

    public String getRewardName() {
        return rewardName;
    }

    public void setRewardName(String rewardName) {
        this.rewardName = rewardName;
    }

    public String getRewardAmount() {
        return rewardAmount;
    }

    public void setRewardAmount(String rewardAmount) {
        this.rewardAmount = rewardAmount;
    }

    public String getRewardImage() {
        return rewardImage;
    }

    public void setRewardImage(String rewardImage) {
        this.rewardImage = rewardImage;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(badgeId);
        dest.writeValue(imageUrlCompleted);
        dest.writeValue(imageUrlInProgress);
        dest.writeValue(title);
        dest.writeValue(description1);
        dest.writeValue(description2);
        dest.writeValue(description3);
        dest.writeValue(description4);
        dest.writeValue(completed);
        dest.writeValue(completionStatusMsg);
        dest.writeValue(progress);
        dest.writeValue(completionCount);
        dest.writeValue(unlockedDate);
        dest.writeValue(rewardName);
        dest.writeValue(rewardAmount);
        dest.writeValue(rewardImage);
        dest.writeValue(imageToShareUrl);
    }

    public String getImageToShareUrl() {
        return imageToShareUrl;
    }

    public void setImageToShareUrl(String imageToShareUrl) {
        this.imageToShareUrl = imageToShareUrl;
    }

    public int describeContents() {
        return 0;
    }

}