package com.indy.views.fragments.gamification.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.BaseServiceManger;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetEventDetailInputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetEventDetailOutputModel;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**

 */
public class GetEventDetailsService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private GetEventDetailInputModel getEventDetailInputModel;


    public GetEventDetailsService(BaseInterface baseInterface, GetEventDetailInputModel getEventDetailInputModel) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.getEventDetailInputModel = getEventDetailInputModel;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<GetEventDetailOutputModel> call = apiService.getEventDetails(getEventDetailInputModel);
        call.enqueue(new Callback<GetEventDetailOutputModel>() {
            @Override
            public void onResponse(Call<GetEventDetailOutputModel> call, Response<GetEventDetailOutputModel> response) {
                mBaseResponseModel.setServiceType(ServiceUtils.getEventDetailsService);
                mBaseResponseModel.setResultObj(response.body());

                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (response.body().getErrorMsgEn() != null) {
//                        String urlForTag = call.request().url().toString();
//                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
//                        urlForTag = urlForTag.replace("/", "_");
//                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<GetEventDetailOutputModel> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.getEventDetailsService);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
