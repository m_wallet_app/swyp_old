package com.indy.views.fragments.buynewline;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.controls.AdjustEvents;
import com.indy.controls.ServiceUtils;
import com.indy.models.acceptTermsConditionsService.AccpetTermsConditionsServiceInput;
import com.indy.models.emiratesIdVerification.VerifyEmiratesIdOutputString;
import com.indy.models.inilizeregisteration.InilizeRegisterationInput;
import com.indy.models.inilizeregisteration.InilizeRegisteratonOutput;
import com.indy.models.onboarding_push_notifications.LogContactDetailsRequestModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.ocr.screens.EmailAddressFragment;
import com.indy.ocr.screens.EmiratesIdDetailsFragment;
import com.indy.services.AcceptTermsConditionsService;
import com.indy.services.LogApplicationFlowService;
import com.indy.services.LogContactDetailsService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.buynewline.exisitingnumber.PromoCodeVerificationSuccessfulFragment;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.MasterFragment;

import static com.indy.views.activites.RegisterationActivity.logEventInputModel;

/**
 * Created by emad on 10/3/16.
 */

public class TermsAndConditionsConfirmation extends MasterFragment {
    private View view;
    private Button acceptBtn, cancelbtn;
    private InilizeRegisterationInput inilizeRegisterationInput;
    InilizeRegisteratonOutput inilizeRegisteratonOutput;
    BaseResponseModel mResponseModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.terms_conditions_confirmations, container, false);
        initUI();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
        acceptBtn = (Button) view.findViewById(R.id.confirmBtn);
        cancelbtn = (Button) view.findViewById(R.id.cancelBtn);
        if (regActivity != null) {
            regActivity.hideHeaderLayout();
        }
        acceptBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Bundle bundle2 = new Bundle();

                    bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_confirm_age_validations);
                    bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_confirm_age_validations);
                    mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_confirm_age_validations, bundle2);

                    logEvent(1);

                    AccpetTermsConditionsServiceInput accpetTermsConditionsServiceInput = new AccpetTermsConditionsServiceInput();
                    accpetTermsConditionsServiceInput.setAppVersion(appVersion);
                    accpetTermsConditionsServiceInput.setDeviceId(deviceId);
                    accpetTermsConditionsServiceInput.setOsVersion(osVersion);
                    accpetTermsConditionsServiceInput.setLang(currentLanguage);
                    accpetTermsConditionsServiceInput.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
                    accpetTermsConditionsServiceInput.setToken(token);
                    accpetTermsConditionsServiceInput.setChannel(channel);
                    accpetTermsConditionsServiceInput.setEmail(EmailAddressFragment.emailStr);
                    accpetTermsConditionsServiceInput.setContactNumber(EmailAddressFragment.mobileNoStr);
                    if (EmailAddressFragment.dob == null || EmailAddressFragment.dob.length() == 0) {
                        if (NameDOBFragment.dateOfBirth != null && NameDOBFragment.dateOfBirth.length() > 0) {
                            accpetTermsConditionsServiceInput.setBirthDate(NameDOBFragment.dateOfBirth);
                        } else if (EmiratesIdDetailsFragment.dateOfBirth != null && EmiratesIdDetailsFragment.dateOfBirth.length() > 0) {
                            accpetTermsConditionsServiceInput.setBirthDate(EmiratesIdDetailsFragment.dateOfBirth);
                        }
                    } else {
                        accpetTermsConditionsServiceInput.setBirthDate(EmailAddressFragment.dob);
                    }

                    new AcceptTermsConditionsService(TermsAndConditionsConfirmation.this, accpetTermsConditionsServiceInput);

                } catch (Exception ex) {
                    CommonMethods.logException(ex);
                }
            }
        });
        cancelbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle2 = new Bundle();
                logEvent(2);
                bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_confirm_age_validations_canceled);
                bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_confirm_age_validations_canceled);
                mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_confirm_age_validations_canceled, bundle2);

                regActivity.onBackPressed();
            }
        });
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            if (isVisible()) {
                this.mResponseModel = responseModel;
                if (responseModel != null && responseModel.getServiceType() != null && responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.accept_terms_conditions_service) && responseModel.getResultObj() != null) {
                    VerifyEmiratesIdOutputString verifyEmiratesIdOutputString = (VerifyEmiratesIdOutputString) responseModel.getResultObj();
                    if (verifyEmiratesIdOutputString.getStatus() != null) {
                        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_onboarding_terms_and_conditions_agreed);
                        LogContactDetailsRequestModel logContactDetailsRequestModel = new LogContactDetailsRequestModel();
                        logContactDetailsRequestModel.setContactNumber(EmailAddressFragment.mobileNoStr);
                        logContactDetailsRequestModel.setEmail(EmailAddressFragment.emailStr);
                        logContactDetailsRequestModel.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
                        new LogContactDetailsService(TermsAndConditionsConfirmation.this, logContactDetailsRequestModel);
                        logEvent(AdjustEvents.TnC_CONFIRM, new Bundle());
                        if (OrderFragment.promoCode != null && OrderFragment.promoCode.length() > 0 && PromoCodeVerificationSuccessfulFragment.promoCodeType == 2) {
                            CashOnDeliveryFragment.orderTypeStr = "2";
                            regActivity.replaceFragmnet(new DeliveryDetailsAddress(), R.id.frameLayout, false);

                        } else {
                            regActivity.replaceFragmnet(new CashOnDeliveryFragment(), R.id.frameLayout, true);
                        }
                    } else {
                        onTermsAndConditionsFail(verifyEmiratesIdOutputString);
                    }
                }
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    private void onTermsAndConditionsFail(VerifyEmiratesIdOutputString verifyEmiratesIdOutputString) {
        if (verifyEmiratesIdOutputString.getErrorMsgEn() != null) {
            if (currentLanguage.equalsIgnoreCase("en"))
                showErrorFragment(verifyEmiratesIdOutputString.getErrorMsgEn());
            else
                showErrorFragment(verifyEmiratesIdOutputString.getErrorMsgAr());
        } else {
            showErrorFragment(getString(R.string.generice_error));
        }
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
//        super.onErrorListener(responseModel);
        try {
            if (responseModel != null && responseModel.getServiceType() != null && (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.LOG_USER_EVENT) ||
                    responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.accept_terms_conditions_service))) {

            } else {

                showErrorFragment(getString(R.string.error_));
            }
        } catch (Exception ex) {

        }
    }

    private void onInitiateRegisterationRequestService() {
        inilizeRegisteratonOutput = (InilizeRegisteratonOutput) mResponseModel.getResultObj();
        if (inilizeRegisteratonOutput != null) {
            if (inilizeRegisteratonOutput.getErrorCode() != null)
                onInitiateRegisterationRequestServiceError();
            else
                onInitiateRegisterationRequestServiceSucess();

        }
    }

    private void onInitiateRegisterationRequestServiceSucess() {
        if (inilizeRegisteratonOutput.getRegistrationId() != null) {
//            regActivity.replaceFragmnet(new PaymentSucessfulllyFragment(), R.id.frameLayout, true);
//
            regActivity.addFragmnet(new NewNumberFragment(), R.id.frameLayout, true);

            sharedPrefrencesManger.setRegisterationID(inilizeRegisteratonOutput.getRegistrationId());
        }
    }

    private void onInitiateRegisterationRequestServiceError() {
        if (currentLanguage.equals("en")) {
            showErrorFragment(inilizeRegisteratonOutput.getErrorCode());
//            Toast.makeText(getActivity(), inilizeRegisteratonOutput.getErrorMsgEn(), Toast.LENGTH_SHORT).show();
        } else {
            showErrorFragment(inilizeRegisteratonOutput.getErrorMsgAr());
        }
    }


    private void showErrorFragment(String error) {
        try {
            Bundle bundle = new Bundle();
            bundle.putString(ConstantUtils.errorString, error);
            ErrorFragment errorFragment = new ErrorFragment();
            errorFragment.setArguments(bundle);
            regActivity.replaceFragmnet(errorFragment, R.id.frameLayout, true);
        } catch (Exception ex) {
//            CommonMethods.logException(ex);
        }
    }

    private void logEvent(int index) {


        logEventInputModel.setActionTransactionId(sharedPrefrencesManger.getTempAppId());
        logEventInputModel.setScreenId(ConstantUtils.SCREEN_ID_0005);

        if (index == 1) {
            logEventInputModel.setConfirmOrCancel(acceptBtn.getText().toString());
        } else {
            logEventInputModel.setConfirmOrCancel(cancelbtn.getText().toString());

        }
        new LogApplicationFlowService(this, logEventInputModel);

    }

}
