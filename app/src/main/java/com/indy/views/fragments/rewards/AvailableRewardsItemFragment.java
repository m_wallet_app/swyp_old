package com.indy.views.fragments.rewards;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.models.rewards.RewardsList;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.RewardsDetailsActivity;
import com.indy.views.fragments.utils.MasterFragment;
import com.squareup.picasso.Picasso;

/**
 * A simple {@link Fragment} subclass.
 */
public class AvailableRewardsItemFragment extends MasterFragment {

    View view;
    public TextView rewardName;
    public TextView rewardPrice;
    public TextView rewardExpirationDate;
    private ImageView rewardImg;
    private LinearLayout card_view;
    private TextView rewardCount;
    RewardsList mRewardsListItem;
    int position,total;
    public AvailableRewardsItemFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_available_rewards_item, container, false);
        if (getArguments() != null && getArguments().getParcelable(ConstantUtils.item_reward_view_pager) != null) {
            mRewardsListItem = getArguments().getParcelable(ConstantUtils.item_reward_view_pager);
        }
        if (getArguments() != null && getArguments().getInt("position")!=-1) {
            position = getArguments().getInt("position");
        }
        if (getArguments() != null && getArguments().getInt("total")!=-1) {
            total = getArguments().getInt("total");
        } if (getArguments() != null && getArguments().getParcelable(ConstantUtils.item_reward_view_pager) != null) {
            mRewardsListItem = getArguments().getParcelable(ConstantUtils.item_reward_view_pager);
        }
        if (getArguments() != null && getArguments().getInt("position")!=-1) {
            position = getArguments().getInt("position");
        }
        if (getArguments() != null && getArguments().getInt("total")!=-1) {
            total = getArguments().getInt("total");
        }
        initUI();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();

        rewardName = (TextView) view.findViewById(R.id.rewardName);
        rewardPrice = (TextView) view.findViewById(R.id.rewardPrice);
        rewardExpirationDate = (TextView) view.findViewById(R.id.expiresID);
        rewardImg = (ImageView) view.findViewById(R.id.rewardImg);
        card_view = (LinearLayout) view.findViewById(R.id.card_view);
        rewardCount = (TextView) view.findViewById(R.id.tv_count_);
        rewardName.setText(mRewardsListItem.getNameEn());
        rewardPrice.setText(mRewardsListItem.getAverageSaving() + " " );//+ mRewardsListItem.getPriceUnitEn());
        rewardExpirationDate.setText(getString(R.string.expires) + " ");// + mRewardsListItem.getExpiryDate());
        Picasso.with(getActivity()).load(mRewardsListItem.getImageUrl()).into(rewardImg);
        rewardCount.setText(position + "/" + total);
        card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), RewardsDetailsActivity.class);
                intent.putExtra(ConstantUtils.RewardsDetails, mRewardsListItem);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
    }
}
