package com.indy.views.fragments.esim;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.controls.BaseInterface;
import com.indy.views.activites.HelpActivity;
import com.indy.views.activites.RegisterationActivity;
import com.indy.views.activites.SwpeMainActivity;
import com.indy.views.fragments.utils.MasterFragment;

public class ActivateYourESIMFragment extends MasterFragment implements BaseInterface {

    private View baseView;
    private Button bt_lets_go, backImg, helpBtn;
    private TextView tv_esim_no, titleTxt;
    private SwpeMainActivity mSwypeMainActivity;
    private RelativeLayout headerLayoutID, backLayout, helpLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        baseView = inflater.inflate(R.layout.fragment_activate_esim, null, false);
        initUI();
        return baseView;
    }


    @Override
    public void initUI() {
        mSwypeMainActivity = (SwpeMainActivity) getActivity();
        super.initUI();
        initToolbar();
        setupToolbar(R.string.esim_activation);
        bt_lets_go = (Button) baseView.findViewById(R.id.bt_lets_go);
        bt_lets_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSwypeMainActivity.replaceFragmnet(new ESIMActivationNoticeFragment(), R.id.contentFrameLayout, false);

            }
        });
        tv_esim_no = baseView.findViewById(R.id.tv_esim_no);
        tv_esim_no.setText(sharedPrefrencesManger.getMobileNo());
    }

    private void initToolbar(){

        backImg = (Button) baseView.findViewById(R.id.backImg);
        backLayout = (RelativeLayout) baseView.findViewById(R.id.backLayout);
        helpLayout = (RelativeLayout) baseView.findViewById(R.id.helpLayout);
        headerLayoutID = (RelativeLayout) baseView.findViewById(R.id.headerLayoutID);
        helpBtn = (Button) baseView.findViewById(R.id.helpBtn);
        titleTxt = (TextView) baseView.findViewById(R.id.titleTxt);
    }

    private void setupToolbar(int title){
        titleTxt.setText(title);
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard();
                mSwypeMainActivity.onBackPressed();
//                Intent intent = new Intent(RegisterationActivity.this,TutorialActivity.class);
//                startActivity(intent);
            }
        });
//        helpBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(RegisterationActivity.this, HelpActivity.class);
//                startActivity(intent);
//            }
//        });
//        helpLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(RegisterationActivity.this, HelpActivity.class);
//                startActivity(intent);
//            }
//        });
        helpLayout.setVisibility(View.INVISIBLE);
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard();
                mSwypeMainActivity.onBackPressed();
            }
        });
    }

}
