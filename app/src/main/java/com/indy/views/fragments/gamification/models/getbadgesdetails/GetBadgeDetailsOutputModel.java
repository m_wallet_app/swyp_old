package com.indy.views.fragments.gamification.models.getbadgesdetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by mobile on 10/10/2017.
 */

public class GetBadgeDetailsOutputModel extends MasterErrorResponse {
    @SerializedName("badgeDetails")
    @Expose
    private BadgeDetails badgeDetails;


    public BadgeDetails getBadgeDetails() {
        return badgeDetails;
    }

    public void setBadgeDetails(BadgeDetails badgeDetails) {
        this.badgeDetails = badgeDetails;
    }
}
