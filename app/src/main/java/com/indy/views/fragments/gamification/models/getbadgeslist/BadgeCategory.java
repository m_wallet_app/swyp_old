package com.indy.views.fragments.gamification.models.getbadgeslist;

/**
 * Created by Tohamy on 10/1/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BadgeCategory {

    @SerializedName("categoryName")
    @Expose
    private String categoryNameEn;
    @SerializedName("categoryNameAr")
    @Expose
    private String categoryNameAr;
    @SerializedName("badgesCount")
    @Expose
    private int badgesCount;
    @SerializedName("badgesCompleted")
    @Expose
    private int badgesCompleted;
    @SerializedName("badges")
    @Expose
    private List<Badge> badges = null;

    public String getCategoryNameEn() {
        return categoryNameEn;
    }

    public void setCategoryNameEn(String categoryNameEn) {
        this.categoryNameEn = categoryNameEn;
    }

    public String getCategoryNameAr() {
        return categoryNameAr;
    }

    public void setCategoryNameAr(String categoryNameAr) {
        this.categoryNameAr = categoryNameAr;
    }

    public int getBadgesCount() {
        return badgesCount;
    }

    public void setBadgesCount(int badgesCount) {
        this.badgesCount = badgesCount;
    }

    public int getBadgesCompleted() {
        return badgesCompleted;
    }

    public void setBadgesCompleted(int badgesCompleted) {
        this.badgesCompleted = badgesCompleted;
    }

    public List<Badge> getBadges() {
        return badges;
    }

    public void setBadges(List<Badge> badges) {
        this.badges = badges;
    }

}