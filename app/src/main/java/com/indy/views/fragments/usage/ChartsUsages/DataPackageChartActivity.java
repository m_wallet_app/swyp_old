package com.indy.views.fragments.usage.ChartsUsages;

import android.animation.Animator;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.Display;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.Utils;
import com.indy.R;
import com.indy.adapters.PackagesListAdapter;
import com.indy.controls.ServiceUtils;
import com.indy.customviews.MyProgressBar;
import com.indy.helpers.DateHelpers;
import com.indy.models.finalizeregisteration.AdditionalInfo;
import com.indy.models.packages.OfferList;
import com.indy.models.packages.PackageConsumption;
import com.indy.models.packages.UsagePackageList;
import com.indy.models.packageusage.GraphPointList;
import com.indy.models.packageusage.OfferInfo;
import com.indy.models.packageusage.PackageInfo;
import com.indy.models.packageusage.PackageUsageInput;
import com.indy.models.packageusage.PackageUsageOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.services.PackageUsageService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.MasterActivity;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by Amir.jehangir on 11/3/2016.
 */
public class DataPackageChartActivity extends MasterActivity {

    private LineChart mChart;
    int xVal = 30;
    float yVal = 100;
    float minVal;
    float upperLimt = 0;
    float lowerLimit = 0f;
    private YAxis leftAxis;
    private LimitLine llXAxis;
    private Button backImg, helpBtn;
    UsagePackageList userPackgeListItem;
    ArrayList<OfferList> offerList;
    ArrayList<OfferList> offerListForServiceInput;
    private TextView remaingID, titletext, data_package_name, tv_daysLeftInGraph;
    private TextView expiresId;
    private TextView startDate, endDate;
    private PackageUsageOutput packageUsageOutput;
    private PackageUsageInput packageUsageInput;
    TextView tv_user_balance_amount, tv_balance_expire, tv_additional_subscription, tv_dailyUsedSubtitle;
    ArrayList<Entry> values = new ArrayList<Entry>();
    private ImageView iv_packageIcon;
    TextView tv_dataUsedToday, tv_dailyEstimatedUsage, tv_forecastUsage;
    FrameLayout frameLayout;
    LinearLayout lv_progressBar;
    ScrollView sv_main;
    Typeface typeface;
    MyProgressBar myProgressBar;

    TextView tv_offerPackageAmount;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.datapackage_chart_activity);

        initUI();
        onConsumeService();

    }

    @Override
    public void initUI() {
        super.initUI();
        try {
            data_package_name = (TextView) findViewById(R.id.data_package_name);
            iv_packageIcon = (ImageView) findViewById(R.id.numberofdeals);
            backImg = (Button) findViewById(R.id.backImg);
            helpBtn = (Button) findViewById(R.id.helpBtn);
            helpBtn.setVisibility(View.INVISIBLE);
            sv_main = (ScrollView) findViewById(R.id.sv_main);
            lv_progressBar = (LinearLayout) findViewById(R.id.ll_progressBar);
            backImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    hideKeyBoard();
                    onBackPressed();
                }
            });
            lv_progressBar.setVisibility(View.VISIBLE);
            tv_dailyUsedSubtitle = (TextView) findViewById(R.id.tv_dailyEsimation);
            sv_main.setVisibility(View.GONE);
            tv_dailyEstimatedUsage = (TextView) findViewById(R.id.tv_mbused_daily);
            tv_dataUsedToday = (TextView) findViewById(R.id.tv_mbused);
            tv_forecastUsage = (TextView) findViewById(R.id.tv_mbused_forecast);

            tv_offerPackageAmount = (TextView) findViewById(R.id.data_package_value);
            tv_daysLeftInGraph = (TextView) findViewById(R.id.tv_expire_days_left);
            userPackgeListItem = getIntent().getParcelableExtra(ServiceUtils.packageListType);
            offerList = getIntent().getParcelableArrayListExtra("OfferInfoList");
            offerListForServiceInput = new ArrayList<>();
            if (offerList != null && offerList.size() > 0) {
                for (int i = 0; i < offerList.size(); i++) {
                    OfferList offerListItem = new OfferList();
                    offerListItem.setInOfferEndDate(offerList.get(i).getInOfferEndDate());
                    offerListItem.setInOfferStartDate(offerList.get(i).getInOfferStartDate());
                    offerListItem.setInOfferId(offerList.get(i).getInOfferId());
                    offerListItem.setPackageConsumption(offerList.get(i).getPackageConsumption());
                    offerListForServiceInput.add(offerListItem);
                }
            }
            if (offerList != null && offerList.size() > 0) {
                offerList.get(0).setInOfferStartDate(parseDateWithDateSubtraction(offerList.get(0).getInOfferEndDate()));
            }
            userPackgeListItem.setOfferList(offerList);
            titletext = (TextView) findViewById(R.id.titleTxt);
            if (currentLanguage.equals("en")) {
                if (userPackgeListItem.getNameEn() != null)
                    titletext.setText(userPackgeListItem.getNameEn());
                else
                    titletext.setText("");
            } else {
                titletext.setText(userPackgeListItem.getNameAr());
            }
            mChart = (LineChart) findViewById(R.id.chart1);
            startDate = (TextView) findViewById(R.id.startDateID);
            endDate = (TextView) findViewById(R.id.endDateId);
            tv_user_balance_amount = (TextView) findViewById(R.id.tv_user_balance_amount);
            tv_balance_expire = (TextView) findViewById(R.id.tv_balance_expire);
            tv_additional_subscription = (TextView) findViewById(R.id.tv_additional_subscription);
            frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
            if (sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)) {
            } else {
            }
            if (currentLanguage.equals("en") && offerList != null && offerList.size() > 0 && offerList.get(0).getPackageConsumption().getRemaining() != null) {
                tv_user_balance_amount.setLineSpacing(0, 1f);

                if (userPackgeListItem.getPackageType() == 2) {
                    tv_user_balance_amount.setText(getResources().getString(R.string.your_remaining_minutes_are) + " " + offerList.get(0).getPackageConsumption().getRemaining() + offerList.get(0).getPackageConsumption().getConsumptionUnitEn());

                } else {

                    tv_user_balance_amount.setText(getResources().getString(R.string.your_remaining_data_allowance_is) + " " + offerList.get(0).getPackageConsumption().getRemaining() + offerList.get(0).getPackageConsumption().getConsumptionUnitEn());
                }
            } else if (currentLanguage.equals("ar") && offerList != null && offerList.size() > 0 && offerList.get(0).getPackageConsumption().getRemaining() != null) {
                tv_user_balance_amount.setLineSpacing(10, 1.2f);

                if (userPackgeListItem.getPackageType() == 2) {
                    tv_user_balance_amount.setText(getResources().getString(R.string.your_remaining_minutes_are) + " " + offerList.get(0).getPackageConsumption().getRemaining() + offerList.get(0).getPackageConsumption().getConsumptionUnitAr());

                } else {
                    tv_user_balance_amount.setText(getResources().getString(R.string.your_remaining_data_allowance_is) + " " + offerList.get(0).getPackageConsumption().getRemaining() + offerList.get(0).getPackageConsumption().getConsumptionUnitAr());

                }


            } else {
                tv_user_balance_amount.setText(getResources().getString(R.string.get_remaining_balance) + " 0");
            }

//            tv_daysLeftInGraph.setText(daysDifference(userPackgeListItem.getOfferList().get(0).getInOfferEndDate(), getString(R.string.days_left)));
            String toSet = parseDateFormatChart(userPackgeListItem.getOfferList().get(0).getInOfferEndDate());
            tv_balance_expire.setText(" " + toSet + " ");

            tv_additional_subscription.setText(getResources().getString(R.string.get_aditional_text));

            if (currentLanguage.equals("en")) {
                typeface = Typeface.createFromAsset(getResources().getAssets(), "fonts/OpenSans-Regular.ttf");

                if (userPackgeListItem.getDescriptionEn() != null)
                    data_package_name.setText(userPackgeListItem.getDescriptionEn().toString());
                else
                    data_package_name.setText("");
            } else {
                typeface = Typeface.createFromAsset(getResources().getAssets(), "fonts/aktive_grotest_rg.ttf");


                if (userPackgeListItem.getDescriptionAr() != null)
                    data_package_name.setText(userPackgeListItem.getDescriptionAr().toString());
                else
                    data_package_name.setText("");
            }

            if (userPackgeListItem.getIconUrl() != null)

            {
                Picasso.with(this).load(userPackgeListItem.getIconUrl()).
                        networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE).into(iv_packageIcon);
            }

            myProgressBar = (MyProgressBar) findViewById(R.id.myProgressBar);

        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }
    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    private void circularRevealActivity() {
        try {
            int cx = sv_main.getWidth() / 2;
            int cy = sv_main.getHeight() / 2;
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int width = size.x;
            int height = size.y;
            float finalRadius = Math.max(width + 200, height + 200);

            // create the animator for this view (the start radius is zero)
            Animator circularReveal = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                circularReveal = ViewAnimationUtils.createCircularReveal(sv_main, 0, height, 0, finalRadius);

                circularReveal.setDuration(1000);

                // make the view visible and start the animation
                sv_main.setVisibility(View.VISIBLE);
                circularReveal.start();
            } else {
                sv_main.setVisibility(View.VISIBLE);
            }
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            frameLayout.setVisibility(View.GONE);
            lv_progressBar.setVisibility(View.GONE);
            sv_main.setVisibility(View.VISIBLE);
            onBackPressed();
            if (responseModel != null && responseModel.getResultObj() != null) {
                packageUsageOutput = (PackageUsageOutput) responseModel.getResultObj();
                if (packageUsageOutput != null && packageUsageOutput.getUsageGraphList() != null && packageUsageOutput.getUsageGraphList().size() > 0 && packageUsageOutput.getUsageGraphList().get(0) != null) {
                    //    packageUsageOutput.getUsageGraphList().get(0).getGraphPointList().get(0).getXAxisValue();
                    //    Log.v("oooooooooops", packageUsageOutput.getUsageGraphList().get(0).getColor() + "");
                    values.clear();
                    sv_main.setVisibility(View.INVISIBLE);
                    ViewTreeObserver viewTreeObserver = sv_main.getViewTreeObserver();
                    if (viewTreeObserver.isAlive()) {
                        viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                            @Override
                            public void onGlobalLayout() {
                                circularRevealActivity();
                                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                                    sv_main.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                                } else {
                                    sv_main.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                                }
                            }
                        });
                    }
                    if (packageUsageOutput.getUsageGraphList() != null && packageUsageOutput.getUsageGraphList().size() > 0 && packageUsageOutput.getUsageGraphList().get(0) != null) {
//                        AssembleChart();
                        setupProgressBar();
                    }
                } else {
                    frameLayout.setVisibility(View.VISIBLE);
                    onPackageFail();
                }
            }
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }
    }


    public void setupProgressBar() {
//        rl_progressBarHeader.setVisibility(View.VISIBLE);
//        rl_dataHeader.setVisibility(View.GONE);
        mChart.setVisibility(View.GONE);
//        rl_progressBarLayout.setVisibility(View.VISIBLE);

        PackageConsumption packageConsumption;
//            if(PackagesListAdapter.usagePackageItem!=null && PackagesListAdapter.usagePackageItem.getOfferList()!=null)
        packageConsumption = PackagesListAdapter.usagePackageItem.getOfferList().get(0).getPackageConsumption();
        if (sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)) {
            data_package_name.setText(PackagesListAdapter.usagePackageItem.getNameEn());
        } else {
            data_package_name.setText(PackagesListAdapter.usagePackageItem.getNameAr());
        }
        myProgressBar.setDataLimitText(packageConsumption.getRemaining() + " " + packageConsumption.getConsumptionUnitEn() +
                " " + getString(R.string.data_left));
        Double remainingAllowance = (packageConsumption.getConsumption() / packageConsumption.getQuota()) * 100;
//        myProgressBar.setMax(100);

        int finalProgress = 100 - (int) Math.ceil(remainingAllowance);
//        remainingAllowance.intValue();

        myProgressBar.setProgress(finalProgress);

//        myProgressBar.setProgress(100 - 50);


        Double limitFromServiceValue = PackagesListAdapter.usagePackageItem.getOfferList().get(0).getPackageConsumption().getQuota();
        Double forecastedUsageValue = Double.parseDouble(packageUsageOutput.getUsageGraphList().get(0).getForecastedUsage());

        if (forecastedUsageValue > limitFromServiceValue) {
            tv_forecastUsage.setTextColor(getResources().getColor(R.color.red_color));
        } else {
            tv_forecastUsage.setTextColor(getResources().getColor(R.color.colorBlack));
        }

        tv_forecastUsage.setText(daysDifference(userPackgeListItem.getOfferList().get(0).getInOfferEndDate(), getString(R.string.days)));
//        tv_forecastUsage.setText(PackagesListAdapter.daysDifferenceForDetail(
//                MixedDataPackageGraphPagerActivity.userPackgeListItem.getOfferList().get(0).getInOfferEndDate()));//, getString(R.string.days_left)));

        if (sharedPrefrencesManger.getLanguage().equals("en")) {
            tv_dataUsedToday.setText(packageUsageOutput.getUsageGraphList().get(0).getUsedToday() + " " + packageConsumption.getConsumptionUnitEn());
            tv_dailyEstimatedUsage.setText(packageConsumption.getConsumption() + " " + packageConsumption.getConsumptionUnitEn());
//            tv_dailyUsedSubtitle.setText(getString(R.string.of) + " " + packageConsumption.getQuota()+packageConsumption.getConsumptionUnitEn() + " " + getString(R.string.used_lower_case));

            tv_dailyUsedSubtitle.setText(getString(R.string.total_used));
//            tv_forecastUsage.setText(usageGraphListObject.getForecastedUsage() + " " + packageConsumption.getConsumptionUnitEn());
//            tv_forecastUsage.setText(tv_balance_expire.getText().toString());
//            tv_dailyEstimatedUsage.setText(usageGraphListObject.getDailyBudget() + " " + packageConsumption.getConsumptionUnitEn());
//            tv_offerName.setText(packageConsumption.getTextEn());
            tv_offerPackageAmount.setText(packageConsumption.getQuota().toString() + " " + packageConsumption.getConsumptionUnitEn());
        } else {
            tv_dataUsedToday.setText(packageUsageOutput.getUsageGraphList().get(0).getUsedToday() + " " + packageConsumption.getConsumptionUnitAr());
//            tv_forecastUsage.setText(usageGraphListObject.getForecastedUsage() + " " + packageConsumption.getConsumptionUnitAr());
            tv_dailyEstimatedUsage.setText(getString(R.string.of) + " " + packageConsumption.getQuota() + packageConsumption.getConsumptionUnitAr() + " " + getString(R.string.used_lower_case));

            tv_dailyEstimatedUsage.setText(packageUsageOutput.getUsageGraphList().get(0).getDailyBudget() + " " + packageConsumption.getConsumptionUnitAr());
//            tv_offerName.setText(packageConsumption.getTextAr());
            tv_offerPackageAmount.setText(packageConsumption.getQuota().toString() + " " + packageConsumption.getConsumptionUnitAr());
        }
    }

    public void onPackageFail() {

        if (packageUsageOutput != null && packageUsageOutput.getErrorMsgEn() != null) {
            if (currentLanguage.equals("en")) {
                showErrorFragment(packageUsageOutput.getErrorMsgEn());
            } else if (currentLanguage.equals("ar")) {
                showErrorFragment(packageUsageOutput.getErrorMsgAr());
            } else {
                showErrorFragment(getString(R.string.generice_error));
            }
        }

    }

    private void showErrorFragment(String error) {
        frameLayout.setVisibility(View.VISIBLE);

        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("finish", true);
        bundle.putString(ConstantUtils.errorString, error);
        errorFragment.setArguments(bundle);
        addFragmnet(errorFragment, R.id.frameLayout, true);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
//        super.onErrorListener(responseModel);
//        onBackPressed();
        try {
            frameLayout.setVisibility(View.GONE);
            lv_progressBar.setVisibility(View.GONE);
            sv_main.setVisibility(View.VISIBLE);
            onPackageFail();
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }
    }


    @Override
    public void onConsumeService() {
        super.onConsumeService();
        try {
            addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
            lv_progressBar.setVisibility(View.VISIBLE);
            sv_main.setVisibility(View.GONE);
            frameLayout.setVisibility(View.VISIBLE);
            packageUsageInput = new PackageUsageInput();
            packageUsageInput.setAppVersion(appVersion);
//        packageUsageInput.setToken(token);
            packageUsageInput.setOsVersion(osVersion);
            packageUsageInput.setAdditionalInfo(new ArrayList<AdditionalInfo>());
            packageUsageInput.setDeviceId(deviceId);
            packageUsageInput.setMsisdn(sharedPrefrencesManger.getMobileNo());
            packageUsageInput.setAuthToken(authToken);

            PackageInfo packageInfo = new PackageInfo();
            packageInfo.setPackageId(userPackgeListItem.getPackageType() + "");
            List<OfferList> offerInfoList = new ArrayList<OfferList>();
            offerInfoList.addAll(offerList);
            List<OfferInfo> offerInfo = new ArrayList<OfferInfo>();

            OfferInfo offerInfo1 = new OfferInfo();
            for (int i = 0; i < offerInfoList.size(); i++) {
                offerInfo1.setStartDate(parseDateFormat(offerListForServiceInput.get(0).getInOfferStartDate()));
                offerInfo1.setEndDate(parseDateFormat(offerListForServiceInput.get(0).getInOfferEndDate()));
                offerInfo1.setOfferId(offerListForServiceInput.get(i).getInOfferId());
                offerInfo.add(offerInfo1);
            }
            packageInfo.setOfferInfo(offerInfo);
            packageUsageInput.setPackageInfo(packageInfo);
            packageUsageInput.setLang(currentLanguage);
            packageUsageInput.setChannel(channel);
            new PackageUsageService(this, packageUsageInput);
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }
    }

    private void disableCordinaets() {
        mChart.getXAxis().setTextColor(android.R.color.transparent);
        leftAxis.setTextColor(android.R.color.transparent);
    }

    private void disableGridView() {
        mChart.getAxisLeft().setDrawGridLines(false);
        mChart.getXAxis().setDrawGridLines(false);
    }

    private void enableCordinates() {
        mChart.getXAxis().setTextColor(android.R.color.transparent);
        leftAxis.setTextColor(android.R.color.transparent);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
    }

    private void toogleValues() {
        List<ILineDataSet> sets = mChart.getData()
                .getDataSets();

        for (ILineDataSet iSet : sets) {

            LineDataSet set = (LineDataSet) iSet;
            set.setDrawValues(!set.isDrawValuesEnabled());
        }

        mChart.invalidate();
    }

    private void toogleCubed() {
        List<ILineDataSet> sets = mChart.getData()
                .getDataSets();

        for (ILineDataSet iSet : sets) {

            LineDataSet set = (LineDataSet) iSet;
            set.setMode(set.getMode() == LineDataSet.Mode.CUBIC_BEZIER
                    ? LineDataSet.Mode.LINEAR
                    : LineDataSet.Mode.CUBIC_BEZIER);
        }
        mChart.invalidate();
    }

    private void toogleCircles() {
        List<ILineDataSet> sets = mChart.getData()
                .getDataSets();

        for (ILineDataSet iSet : sets) {

            LineDataSet set = (LineDataSet) iSet;
            if (set.isDrawCirclesEnabled())
                set.setDrawCircles(false);
            else
                set.setDrawCircles(true);
        }
        mChart.invalidate();
    }

    private void animateXY() {
        mChart.animateXY(3000, 3000);

    }

    private void AssembleChart() {
        try {
//        mChart.setViewPortOffsets(0, 20, 0, 0);
            mChart.setDrawGridBackground(false);

            // no description text
            mChart.setDescription("");

            PackageConsumption packageConsumption = PackagesListAdapter.usagePackageItem.getOfferList().get(0).getPackageConsumption();
            mChart.setNoDataTextDescription("You need to provide data for the chart.");

            startDate.setText(parseDateForStartEndDate(userPackgeListItem.getOfferList().get(0).getInOfferStartDate()
            ).toString());


            endDate.setText(parseDateForStartEndDate(userPackgeListItem.getOfferList().get(0).getInOfferEndDate())
                    .toString());


            // add data
            if (packageUsageOutput.getUsageGraphList() != null && packageUsageOutput.getUsageGraphList().size() > 0 && packageUsageOutput.getUsageGraphList().get(0) != null) {

            }
//
//        String limitFromService = PackagesListAdapter.usagePackageItem.getOfferList().get(0).getPackageConsumption().getQuota().toString();
//        setData(xVal, Float.parseFloat(limitFromService));
//        //   mChart.setVisibleXRange(20);
            // mChart.setVisibleYRange(20f, YAxis.AxisDependency.LEFT);
            //   mChart.centerViewTo(20, 50, YAxis.AxisDependency.LEFT);


            //          upperLimt =70 ;
            // enable touch gestures
            mChart.setTouchEnabled(false);
//
            // enable scaling and dragging
            mChart.setDragEnabled(false);
            mChart.setScaleEnabled(false);
//        // mChart.setScaleXEnabled(true);
            mChart.setScaleYEnabled(true);
//
            // if disabled, scaling can be done on§ x- and y-axis separately
            mChart.setPinchZoom(false);

            // set an alternative background color
            // mChart.setBackgroundColor(Color.GRAY);

            // create a custom MarkerView (extend MarkerView) and specify the layout
            // to use for it
//        MyMarkerView mv = new MyMarkerView(this, R.layout.custom_marker_view);
//        // set the marker to the chart
//        mChart.setMarkerView(mv);
            // x-axis limit line
            String limitFromService = PackagesListAdapter.usagePackageItem.getOfferList().get(0).getPackageConsumption().getQuota().toString();
            setData(xVal, Float.parseFloat(limitFromService));

            Double limitFromServiceValue = PackagesListAdapter.usagePackageItem.getOfferList().get(0).getPackageConsumption().getQuota();
            Double forecastedUsageValue = Double.parseDouble(packageUsageOutput.getUsageGraphList().get(0).getForecastedUsage());


            llXAxis = new LimitLine(Float.parseFloat(limitFromService), "Index 10");
            llXAxis.setLineWidth(1f);

            llXAxis.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
            llXAxis.setTextSize(10f);

            XAxis xAxis = mChart.getXAxis();
            xAxis.setEnabled(true);
//        xAxis.enableGridDashedLine(10f, 10f, 0f);
            xAxis.disableGridDashedLine();
            //xAxis.setValueFormatter(new MyCustomXAxisValueFormatter());
            //xAxis.addLimitLine(llXAxis); // add x-axis limit line
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            xAxis.setAxisLineWidth(1);
            upperLimt = Float.parseFloat(limitFromService);
            lowerLimit = 0f;
            LimitLine ll1;
            if (forecastedUsageValue > limitFromServiceValue) {
                tv_forecastUsage.setTextColor(getResources().getColor(R.color.red_color));
            } else {
                tv_forecastUsage.setTextColor(getResources().getColor(R.color.colorBlack));
            }

            if (sharedPrefrencesManger.getLanguage().equals("en")) {
                ll1 = new LimitLine(upperLimt, upperLimt + " " + packageConsumption.getConsumptionUnitEn() + " " + getResources().getString(R.string.chart_limit));
                ll1.setLineWidth(1f);
                ll1.setLineColor(getResources().getColor(R.color.graph_line_color));
                tv_dataUsedToday.setText(packageUsageOutput.getUsageGraphList().get(0).getUsedToday() + " " + packageConsumption.getConsumptionUnitEn());
                tv_forecastUsage.setText(packageUsageOutput.getUsageGraphList().get(0).getForecastedUsage() + " " + packageConsumption.getConsumptionUnitEn());
//        tv_dataAllowanceRemaining.setText(packageUsageOutput.getUsageGraphList().get(0).getDailyBudget());
                tv_dailyEstimatedUsage.setText(packageUsageOutput.getUsageGraphList().get(0).getDailyBudget() + " " + packageConsumption.getConsumptionUnitEn());

            } else {
                ll1 = new LimitLine(upperLimt, upperLimt + " " + packageConsumption.getConsumptionUnitAr() + " " + getResources().getString(R.string.chart_limit));
                ll1.setLineWidth(1f);
                ll1.setLineColor(getResources().getColor(R.color.graph_line_color));
                tv_dataUsedToday.setText(packageUsageOutput.getUsageGraphList().get(0).getUsedToday() + " " + packageConsumption.getConsumptionUnitAr());
                tv_forecastUsage.setText(packageUsageOutput.getUsageGraphList().get(0).getForecastedUsage() + " " + packageConsumption.getConsumptionUnitAr());
//        tv_dataAllowanceRemaining.setText(packageUsageOutput.getUsageGraphList().get(0).getDailyBudget());
                tv_dailyEstimatedUsage.setText(packageUsageOutput.getUsageGraphList().get(0).getDailyBudget() + " " + packageConsumption.getConsumptionUnitAr());

            }

            ll1.setLineWidth(1f);
            ll1.setLineColor(getResources().getColor(R.color.graph_line_color));


//        ll1.enableDashedLine(10f, 10f, 0f);
            ll1.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_TOP);
            ll1.setTextSize(14f);
            ll1.setTextColor(getResources().getColor(R.color.greyish_brown));

            ll1.setTypeface(typeface);
//        ll1.setTypeface(tf);

//        LimitLine ll2 = new LimitLine(lowerLimit, "Lower Limit");
//        ll2.setLineWidth(2f);
////        ll2.enableDashedLine(10f, 10f, 0f);
//        ll2.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
//        ll2.setTextSize(10f);
////        ll2.setTypeface(tf);
//        ll2.setLineColor(android.R.color.transparent);
            leftAxis = mChart.getAxisLeft();
            leftAxis.removeAllLimitLines(); // reset all limit lines to avoid overlapping lines
            if (ll1 != null && packageUsageOutput != null && packageUsageOutput.getUsageGraphList() != null && packageUsageOutput.getUsageGraphList().size() > 0) {

                leftAxis.addLimitLine(ll1);
            }
            //  leftAxis.addLimitLine(ll2);
            leftAxis.setAxisMaxValue(upperLimt);
            leftAxis.setAxisMinValue(lowerLimit);
//        leftAxis.setYOffset(1f);
//        leftAxis.setXOffset(0f);
            //   leftAxis.enableGridDashedLine(10f, 10f, 0f);
            leftAxis.setDrawZeroLine(false);
            // limit lines are drawn behind data (and not on top)
            leftAxis.setDrawLimitLinesBehindData(false);
            leftAxis.setMaxWidth(1);
            leftAxis.setAxisLineWidth(1);
            mChart.getXAxis().setAxisMinValue(-0.1f);
            mChart.getAxisRight().setEnabled(false);
            mChart.getAxisRight().setAxisMinValue(1);
//        mChart.getAxisRight().setXOffset(1f);
            //mChart.getViewPortHandler().setMaximumScaleY(2f);
            //mChart.getViewPortHandler().setMaximumScaleX(2f);

            mChart.animateX(2500);
            //mChart.invalidate();

            // get the legend (only possible after setting data)
            mChart.getLegend().setEnabled(false);

            // modify the legend ...
            // l.setPosition(LegendPosition.LEFT_OF_CHART);
            // l.setForm(Legend.LegendForm.LINE);

//        disableCordinaets();
//        enableCordinates();
            // // dont forget to refresh the drawing
            // mChart.invalidate();
//        toogleValues();
            toogleCubed();
            toogleCircles();
//        mChart.animateX(2500);

            animateXY();
            disableGridView();
            disableCordinaets();
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }
    }


    private void setData(int count, float range) {

        try {

            List<GraphPointList> tempList = packageUsageOutput.getUsageGraphList().get(0).getGraphPointList();

            Collections.sort(tempList, new Comparator<GraphPointList>() {
                @Override
                public int compare(GraphPointList lhs, GraphPointList rhs) {
                    if (lhs.getYAxisValue() < rhs.getYAxisValue()) {
                        return -1;
                    } else {
                        return 1;
                    }
                }
            });

            if (packageUsageOutput != null && packageUsageOutput.getUsageGraphList() != null &&
                    packageUsageOutput.getUsageGraphList().size() > 0 && packageUsageOutput.getUsageGraphList().get(0) != null &&
                    packageUsageOutput.getUsageGraphList().get(0).getGraphPointList() != null &&
                    packageUsageOutput.getUsageGraphList().get(0).getGraphPointList().get(0) != null && (
                    packageUsageOutput.getUsageGraphList().get(0).getGraphPointList().get(0).getXAxisValue() > 0 ||
                            packageUsageOutput.getUsageGraphList().get(0).getGraphPointList().get(0).getYAxisValue() > 0)) {
                values = new ArrayList<Entry>();
                for (int i = 0; i < count; i++) {
                    values.add(new Entry(i, 0));
                }
                int xValForGraph = -1;
                int previousX = 0;
                for (int i = 0; i < packageUsageOutput.getUsageGraphList().get(0).getGraphPointList().size(); i++) {
//                if (i < 10) {
                    String valToFloat = tempList.get(i).getYAxisValue().toString();
                    float val = Float.parseFloat(valToFloat);
                    float xval;

//            if(i!=0){
//                if(val==0){
//                    xval = Float.parseFloat(tempList.get(i-1).getXAxisValue().toString());
//                    xValForGraph = Math.round(xval);
//
//                    values.set(xValForGraph,new Entry(xValForGraph, val));
//                }else{
                    xval = Float.parseFloat(tempList.get(i).getXAxisValue().toString());
                    xValForGraph = Math.round(xval);
                    if (xValForGraph >= 0 && xValForGraph <= 30) {
//                        if(xValForGraph==0)
//                            xValForGraph = 1;
//                        if(i>=1 && previousX==0&& xValForGraph>0){
//                            values.add(xValForGraph - 1, new Entry(xValForGraph - 0.001f, 0));
//
//                        }
                        values.set(xValForGraph, new Entry(xValForGraph, val));
                        previousX = xValForGraph;
                    }
//                }
//            }else{
//                xval = Float.parseFloat(tempList.get(i).getXAxisValue().toString());
//                xValForGraph = Math.round(xval);
//                values.set(xValForGraph,new Entry(xValForGraph, val));
//            }


                    upperLimt = (float) (tempList.get(i).getYAxisValue() + 5);
                    //   int abc = Integer.parseInt(Collections.max(tempList.get(i).getYAxisValue()/11));

//                } else {
//                    float val = (float) (tempList.get(i).getYAxisValue()/11);
//                    values.add(new Entry(i, val));
//
//                }


//            } else {
//                values.add(new Entry(i, 0));
//            }
                }
                if (xValForGraph >= 0 && xValForGraph <= 30) {

                    values.add(xValForGraph + 1, new Entry(xValForGraph + 0.001f, 0));
                }

            } else {
                values = new ArrayList<Entry>();
            }
            LineDataSet set1;

            if (mChart.getData() != null &&
                    mChart.getData().getDataSetCount() > 0) {
                set1 = (LineDataSet) mChart.getData().getDataSetByIndex(0);
                set1.setDrawValues(false);
                set1.setValues(values);

                set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                mChart.getData().notifyDataChanged();
                mChart.notifyDataSetChanged();
            } else {
                // create a dataset and give it a type
//            set1 = new LineDataSet(values, "10 days Remaining");
                set1 = new LineDataSet(values, "");
                // set the line to be drawn like this "- - - - - -"
//            set1.enableDashedLine(10f, 5f, 0f);
                set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                set1.enableDashedHighlightLine(10f, 5f, 0f);
                set1.setColor(getResources().getColor(R.color.orange_color));
                set1.setCircleColor(getResources().getColor(R.color.orange_color));
                set1.setLineWidth(1f);
                set1.setCircleRadius(3f);
                set1.setDrawCircleHole(false);
                set1.setValueTextSize(9f);
                set1.setDrawFilled(true);
                set1.setDrawValues(false);
                if (Utils.getSDKInt() >= 18) {
                    // fill drawable only supported on api level 18 and above
                    Drawable drawable = ContextCompat.getDrawable(this, R.drawable.fade_red);
                    set1.setFillDrawable(drawable);
                } else {
                    set1.setFillColor(getResources().getColor(R.color.graph_line_color));
                }

                ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
                dataSets.add(set1); // add the datasets

                // create a data object with the datasets
                LineData data = new LineData(dataSets);

                // set data
                mChart.setData(data);
            }
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }
    }


    public static String parseDateFormat(String date) {
        String returnString = "";
        SimpleDateFormat dateFormatter;
        try {
            Date formattedDate;
            //28/09/2016 19:13:20
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
            formattedDate = df.parse(date);
            dateFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
            return dateFormatter.format(formattedDate);
        } catch (Exception ex) {
            return returnString;

        }
    }


    public static String parseDateFormatChart(String date) {
        String returnString = "";
        SimpleDateFormat dateFormatter;
        try {
            Date formattedDate;
            //28/09/2016 19:13:20
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
            formattedDate = df.parse(date);
            dateFormatter = new SimpleDateFormat("dd MMM, HH:mm", Locale.US);
            return dateFormatter.format(formattedDate);
        } catch (Exception ex) {
            return returnString;

        }
    }

    public static String parseDateForStartEndDate(String date) {
        String returnString = "";
        SimpleDateFormat dateFormatter;
        try {
            Date formattedDate;
            //28/09/2016 19:13:20
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
            formattedDate = df.parse(date);
            dateFormatter = new SimpleDateFormat("dd MMM", Locale.US);
            return dateFormatter.format(formattedDate);
        } catch (Exception ex) {
            return returnString;

        }
    }


    public static String daysDifference(String date, String toAppendAtEnd) {
        Date dateToPass;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
        try {

            dateToPass = simpleDateFormat.parse(date);

        } catch (Exception ex) {
            dateToPass = null;
        }
        if (dateToPass != null) {
            long diffInMils = dateToPass.getTime() - DateHelpers.getCurrentDateFor().getTime();
            long daysDiff = TimeUnit.MILLISECONDS.toDays(diffInMils);
            return daysDiff + " " + toAppendAtEnd;
        } else {
            return toAppendAtEnd + " " + date;
        }
    }

    public static String parseDateWithDateSubtraction(String date) {
        String returnString = "";
        SimpleDateFormat dateFormatter;
        try {
            Date formattedDate;
            //28/09/2016 19:13:20
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

            formattedDate = df.parse(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(formattedDate);
            long timeOld = calendar.getTimeInMillis();
            long timeToSubtract = 2592000000L;

            long newTime = timeOld - timeToSubtract;
            Date dateAfterSubtraction = new Date(newTime);

            dateFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
            return dateFormatter.format(dateAfterSubtraction);
        } catch (Exception ex) {
            return returnString;

        }
    }
}
