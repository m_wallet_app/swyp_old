package com.indy.views.fragments.gamification.models.serviceInputOutput;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mobile on 24/09/2017.
 */

public class LogShareEventInputModel extends GamificationBaseInputModel {

    @SerializedName("eventTypeId")
    @Expose
    private int eventTypeId;

    public int getEventTypeId() {
        return eventTypeId;
    }

    public void setEventTypeId(int eventTypeId) {
        this.eventTypeId = eventTypeId;
    }
}
