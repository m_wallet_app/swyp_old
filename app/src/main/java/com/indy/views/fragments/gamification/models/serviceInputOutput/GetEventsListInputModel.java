package com.indy.views.fragments.gamification.models.serviceInputOutput;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mobile on 24/09/2017.
 */

public class GetEventsListInputModel extends GamificationBaseInputModel {

    @SerializedName("pageFrom")
    @Expose
    private int pageFrom;
    @SerializedName("pageTo")
    @Expose
    private int pageTo;


    public int getPageFrom() {
        return pageFrom;
    }

    public void setPageFrom(int pageFrom) {
        this.pageFrom = pageFrom;
    }

    public int getPageTo() {
        return pageTo;
    }

    public void setPageTo(int pageTo) {
        this.pageTo = pageTo;
    }
}
