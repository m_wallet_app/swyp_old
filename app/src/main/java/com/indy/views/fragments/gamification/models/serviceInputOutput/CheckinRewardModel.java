package com.indy.views.fragments.gamification.models.serviceInputOutput;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mobile on 27/09/2017.
 */

public class CheckinRewardModel implements Parcelable {
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("raffleTicketsWon")
    @Expose
    private Integer raffleTicketsWon;
    @SerializedName("raffleTicketsTotal")
    @Expose
    private Integer raffleTicketsTotal;

    @SerializedName("previousPrice")
    @Expose
    private Integer previousPrice;
    @SerializedName("packageDescription")
    @Expose
    private String packageDescription;
    @SerializedName("currentPrice")
    @Expose
    private Integer currentPrice;
    @SerializedName("packageName")
    @Expose
    private String packageName;
    @SerializedName("packageCode")
    @Expose
    private String packageCode;

    @SerializedName("totalDescription")
    @Expose
    private String totalDescription;

    @SerializedName("name")
    @Expose
    private String name;


    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("rewardId")
    @Expose
    private String rewardId;

    String notificationResponseId;
    public String getTotalDescription() {
        return totalDescription;
    }

    public void setTotalDescription(String totalDescription) {
        this.totalDescription = totalDescription;
    }

    public String getNotificationResponseId() {
        return notificationResponseId;
    }

    public void setNotificationResponseId(String notificationResponseId) {
        this.notificationResponseId = notificationResponseId;
    }

    public String getRewardId() {
        return rewardId;
    }

    public void setRewardId(String rewardId) {
        this.rewardId = rewardId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static final Creator<CheckinRewardModel> CREATOR = new Creator<CheckinRewardModel>() {
        @Override
        public CheckinRewardModel createFromParcel(Parcel in) {
            return new CheckinRewardModel(in);
        }

        @Override
        public CheckinRewardModel[] newArray(int size) {
            return new CheckinRewardModel[size];
        }
    };

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getRaffleTicketsWon() {
        return raffleTicketsWon;
    }

    public void setRaffleTicketsWon(Integer raffleTicketsWon) {
        this.raffleTicketsWon = raffleTicketsWon;
    }

    public Integer getRaffleTicketsTotal() {
        return raffleTicketsTotal;
    }

    public void setRaffleTicketsTotal(Integer raffleTicketsTotal) {
        this.raffleTicketsTotal = raffleTicketsTotal;
    }

    public Integer getPreviousPrice() {
        return previousPrice;
    }

    public void setPreviousPrice(Integer previousPrice) {
        this.previousPrice = previousPrice;
    }

    public String getPackageDescription() {
        return packageDescription;
    }

    public void setPackageDescription(String packageDescription) {
        this.packageDescription = packageDescription;
    }

    public Integer getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(Integer currentPrice) {
        this.currentPrice = currentPrice;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getPackageCode() {
        return packageCode;
    }

    public void setPackageCode(String packageCode) {
        this.packageCode = packageCode;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(type);
        dest.writeValue(raffleTicketsWon);
        dest.writeValue(raffleTicketsTotal);
        dest.writeValue(packageName);
        dest.writeValue(packageDescription);
        dest.writeValue(previousPrice);
        dest.writeValue(currentPrice);
        dest.writeValue(packageCode);
        dest.writeValue(totalDescription);
        dest.writeValue(description);
        dest.writeValue(name);
        dest.writeValue(rewardId);
    }

    public CheckinRewardModel(){

    }

    protected CheckinRewardModel(Parcel in) {
        this.type = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.raffleTicketsWon = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.raffleTicketsTotal = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.packageName = ((String) in.readValue((String.class.getClassLoader())));
        this.packageDescription = ((String) in.readValue((String.class.getClassLoader())));
        this.previousPrice = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.currentPrice = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.packageCode = ((String) in.readValue((String.class.getClassLoader())));
        this.totalDescription = ((String) in.readValue((String.class.getClassLoader())));
        this.description = ((String) in.readValue((String.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.rewardId = ((String ) in.readValue((String.class.getClassLoader())));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
