//package com.indy.views.fragments.ref;
//
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.indy.R;
//import com.indy.dbt.listeners.OnDataTransferConfirmedListener;
//import com.indy.utils.CommonMethods;
//import com.indy.utils.MaterialCheckBox;
//import com.indy.views.fragments.utils.MasterFragment;
//
//public class DataTransferSuccessfulFragment extends MasterFragment implements View.OnClickListener {
//
//    private View baseView;
//    private Double amountDouble;
//    private String amount;
//    private String msisdn;
//    private Button okBtn;
//    private TextView tv_dbt_sent_data_amount, tv_dbt_sent_number;
//    private OnDataTransferConfirmedListener onDataTransferConfirmedListener;
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        baseView = inflater.inflate(R.layout.fragment_dbt_data_transfer_successful, null, false);
//        return baseView;
//    }
//
//    @Override
//    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//        if (getArguments() != null) {
//            amount = getArguments().getString(DataTransferConfirmationFragment.KEY_DATA_MB);
//            msisdn = getArguments().getString(DataTransferConfirmationFragment.KEY_MSISDN);
//            amountDouble = Double.parseDouble(amount);
//        }
//        initUI();
//    }
//
//    public void setOnDataTransferConfirmedListener(OnDataTransferConfirmedListener onDataTransferConfirmedListener) {
//        this.onDataTransferConfirmedListener = onDataTransferConfirmedListener;
//    }
//
//    @Override
//    public void initUI() {
//        super.initUI();
//        tv_dbt_sent_number = baseView.findViewById(R.id.tv_dbt_sent_number);
//        tv_dbt_sent_data_amount = baseView.findViewById(R.id.tv_dbt_sent_data_amount);
//        okBtn = baseView.findViewById(R.id.okBtn);
//        if (amountDouble > 999) {
//            tv_dbt_sent_data_amount.setText((amountDouble / 1000) + " GB");
//        }
//        else{
//            tv_dbt_sent_data_amount.setText(amountDouble + " MB");
//        }
//        tv_dbt_sent_number.setText(msisdn);
//        okBtn.setOnClickListener(this);
//        MaterialCheckBox materialCheckBox = (MaterialCheckBox) baseView.findViewById(R.id.fl_tick_animation);
//        ImageView iv_tick = (ImageView) baseView.findViewById(R.id.iv_tick);
//        CommonMethods.drawCircle(materialCheckBox, iv_tick);
//    }
//
//    @Override
//    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.okBtn:
//                if (onDataTransferConfirmedListener != null) {
//                    onDataTransferConfirmedListener.onOk();
//                }
//                break;
//        }
//    }
//}
