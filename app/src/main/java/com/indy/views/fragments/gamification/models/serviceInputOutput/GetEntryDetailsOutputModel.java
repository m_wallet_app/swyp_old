package com.indy.views.fragments.gamification.models.serviceInputOutput;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.views.fragments.gamification.models.EventPhoto;

/**
 * Created by mobile on 24/09/2017.
 */

public class GetEntryDetailsOutputModel extends MasterErrorResponse {

    @SerializedName("entryDetails")
    @Expose
    EventPhoto entryDetails;

    public EventPhoto getEntryDetails() {
        return entryDetails;
    }

    public void setEntryDetails(EventPhoto entryDetails) {
        this.entryDetails = entryDetails;
    }
}
