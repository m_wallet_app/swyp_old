package com.indy.views.fragments.store.recharge;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.indy.R;
import com.indy.views.activites.RechargeActivity;
import com.indy.views.fragments.utils.MasterFragment;

/**
 * Created by Amir.jehangir on 10/16/2016.
 */
public class PaymentBeneficiaryAndDescriptionFragment extends MasterFragment {
    private View view;
    TextView titleview;

    RechargeActivity rechargeActivity;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_paymentbeneficiary, container, false);
        rechargeActivity = (RechargeActivity)getActivity();
        view.bringToFront();
        initUI();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
      //  titleview = (TextView)view.findViewById(R.id.titleTxt);
      //  titleview.setText(getResources().getString(R.string.payment_title));



    }


    private void displayNextFragment() {
       // rechargeActivity.replaceFragmnet(new CongratulationsFragmentRecharge(), R.id.frameLayout, true);
    }
}
