package com.indy.views.fragments.esim;

import android.app.MediaRouteButton;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.views.activites.HelpActivity;
import com.indy.views.activites.MasterActivity;
import com.indy.views.activites.RegisterationActivity;
import com.indy.views.fragments.utils.MasterFragment;

public class LearnHowToActivateFragment extends MasterFragment {

    private View baseView;
    private Button bt_lets_go, backImg, helpBtn;
    private TextView titleTxt, tv_esim_share_qr_code, tv_esim_learn_how_to;
    private MasterActivity mSwypeMainActivity;
    private RelativeLayout headerLayoutID, backLayout, helpLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        baseView = inflater.inflate(R.layout.fragment_esim_learn_how_to, null, false);
        initToolbar();
        setupToolbar("");
        return baseView;
    }

    public void setMasterActivity(MasterActivity masterActivity){
        mSwypeMainActivity = masterActivity;
    }

    private void initToolbar() {

        backImg = (Button) baseView.findViewById(R.id.backImg);
        backLayout = (RelativeLayout) baseView.findViewById(R.id.backLayout);
        helpLayout = (RelativeLayout) baseView.findViewById(R.id.helpLayout);
        headerLayoutID = (RelativeLayout) baseView.findViewById(R.id.headerLayoutID);
        helpBtn = (Button) baseView.findViewById(R.id.helpBtn);
        titleTxt = (TextView) baseView.findViewById(R.id.titleTxt);

    }

    private void setupToolbar(String title) {
        titleTxt.setText(title);
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard();
                mSwypeMainActivity.onBackPressed();
//                Intent intent = new Intent(RegisterationActivity.this,TutorialActivity.class);
//                startActivity(intent);
            }
        });
//        helpBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(RegisterationActivity.this, HelpActivity.class);
//                startActivity(intent);
//            }
//        });
        helpLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mSwypeMainActivity, HelpActivity.class);
                startActivity(intent);
            }
        });
        helpLayout.setVisibility(View.INVISIBLE);
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard();
                mSwypeMainActivity.onBackPressed();
            }
        });
    }

}
