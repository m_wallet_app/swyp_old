package com.indy.views.fragments.newaccount;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.customviews.CustomButton;
import com.indy.customviews.CustomEditText;
import com.indy.helpers.ValidationHelper;
import com.indy.models.createnewaccount.CreateNewAccountInput;
import com.indy.models.createnewaccount.CreateNewAccountNewResponse;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.CreateNewAccountService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.HelpActivity;
import com.indy.views.fragments.login.InviteCodeFragment;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.indy.views.fragments.utils.MasterFragment;

/**
 * Created by emad on 9/7/16.
 */
public class SetPasswordFragment extends MasterFragment {
    private View view;
    private Button loginBtn;
    private EditText new_passwrd, confirm_new_password;
    private TextView help;
    private CreateNewAccountInput changePasswordInput;
    //    private ProgressBar progressBar;
    private CreateNewAccountNewResponse changePasswordOutput;
    private String mobNo;
    private TextInputLayout til_password, til_confirmPassword;
    private Button backImg;
    private Button helpBtn;
    public static Intent service;
    private BaseResponseModel mBaseResponseModel;

    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_set_password, container, false);
        initUI();
        setListeners();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
//        progressBar = (ProgressBar) view.findViewById(R.id.progressID);
        new_passwrd = (CustomEditText) view.findViewById(R.id.newPassword);
        loginBtn = (CustomButton) view.findViewById(R.id.loginBtn);
        confirm_new_password = (CustomEditText) view.findViewById(R.id.confirm_new_password);
        help = (TextView) view.findViewById(R.id.help);
        if (getArguments() != null && getArguments().getString(ConstantUtils.newAccountMobileNo) != null)
            mobNo = getArguments().getString(ConstantUtils.newAccountMobileNo);
        til_password = (TextInputLayout) view.findViewById(R.id.password_input_layout);
        til_confirmPassword = (TextInputLayout) view.findViewById(R.id.confirm_password_input_layout);

        backImg = (Button) view.findViewById(R.id.backImg);
        //..............header Buttons............
        RelativeLayout backLayout = (RelativeLayout) view.findViewById(R.id.backLayout);
        RelativeLayout helpLayout = (RelativeLayout) view.findViewById(R.id.helpLayout);

        helpLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), HelpActivity.class);
                startActivity(intent);
            }
        });
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard();
                getActivity().onBackPressed();
            }
        });

        disableLoginButton();
    }

    private void setListeners() {
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (isNotEmpty()) {
                if (isValid())
                    onConsumeService();
//                    else showErrorDalioge(getString(R.string.invalid_password));
//                        regActivity.replaceFragmnet(new InviteCodeFragment(), R.id.frameLayout, true);
//                }
            }
        });
//        help.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onHelp();
//            }
//        });

        new_passwrd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (new_passwrd.getText().toString().trim().length() > 0 && confirm_new_password.getText().toString().trim().length() > 0) {
                    enableLoginButton();
                } else {
                    disableLoginButton();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        confirm_new_password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (new_passwrd.getText().toString().trim().length() > 0 && confirm_new_password.getText().toString().trim().length() > 0) {
                    enableLoginButton();
                } else {
                    disableLoginButton();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void onLogin() {
        if (changePasswordOutput.getUpdated()) {
            setAppFlagsValue();
            InviteCodeFragment inviteCodeFragment = new InviteCodeFragment();
            Bundle bundle = new Bundle();
            bundle.putString(ConstantUtils.newAccountMobileNo, mobNo);
            inviteCodeFragment.setArguments(bundle);
            regActivity.replaceFragmnet(inviteCodeFragment, R.id.frameLayout, true);
        } else {
            Bundle bundle2 = new Bundle();

            bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_create_account_password_creation);
            bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_create_account_password_creation);
            mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_create_account_password_creation, bundle2);
            Fragment errorFragmnet = new ErrorFragment();
            Bundle bundle = new Bundle();
            if (currentLanguage != null) {
                if (changePasswordOutput.getErrorMsgEn() != null) {
                    if (currentLanguage.equalsIgnoreCase("en"))
                        bundle.putString(ConstantUtils.errorString, changePasswordOutput.getErrorMsgEn());
                    else
                        bundle.putString(ConstantUtils.errorString, changePasswordOutput.getErrorMsgAr());
                } else {
                    bundle.putString(ConstantUtils.errorString, getString(R.string.passwored_set_up_error));
                }
            } else {
                bundle.putString(ConstantUtils.errorString, getString(R.string.passwored_set_up_error));
            }
            errorFragmnet.setArguments(bundle);
            regActivity.replaceFragmnet(errorFragmnet, R.id.frameLayout, true);
        }
    }

    private void setAppFlagsValue() {
        if (sharedPrefrencesManger != null) {
            if (changePasswordOutput != null) {
                if (changePasswordOutput.getAppFlags() != null) {
                    if (sharedPrefrencesManger != null) {
                        sharedPrefrencesManger.enableBalanceTransfer(changePasswordOutput.getAppFlags().getUbtEnabled());
                        sharedPrefrencesManger.enableRewards(changePasswordOutput.getAppFlags().getMarketplaceEnabled());
                        sharedPrefrencesManger.enableStore(changePasswordOutput.getAppFlags().getStoreEnabled());
                        sharedPrefrencesManger.enableLiveChat(changePasswordOutput.getAppFlags().getLiveChatEnabled());
                        sharedPrefrencesManger.enableLUsage(changePasswordOutput.getAppFlags().getUsageEnabled());
                    }
                }
            }
        }
    }

    private void onHelp() {
        Intent intent = new Intent(getActivity(), HelpActivity.class);
        startActivity(intent);
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        regActivity.addFragmnet(new LoadingFragmnet(), R.id.loadingFragmeLayout, true);
//        progressBar.setVisibility(View.VISIBLE);
        changePasswordInput = new CreateNewAccountInput();
        changePasswordInput.setAppVersion(appVersion);
        changePasswordInput.setToken(token);
        changePasswordInput.setOsVersion(osVersion);
        changePasswordInput.setChannel(channel);
        changePasswordInput.setDeviceId(deviceId);
        changePasswordInput.setMsisdn(mobNo);
        String password = ValidationHelper.getMd5(new_passwrd.getText().toString().trim());
        String confirmpassword = ValidationHelper.getMd5(confirm_new_password.getText().toString().trim());
        changePasswordInput.setPassword(password.toUpperCase());
        changePasswordInput.setPasswordConfirmation(confirmpassword.toUpperCase());
        changePasswordInput.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        new CreateNewAccountService(this, changePasswordInput);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            this.mBaseResponseModel = responseModel;
            regActivity.onBackPressed();
            if (isVisible()) {
//            progressBar.setVisibility(View.GONE);
                if(responseModel!=null && responseModel.getResultObj()!=null) {
                    changePasswordOutput = (CreateNewAccountNewResponse) responseModel.getResultObj();
                    if (changePasswordOutput != null) {
                        if (changePasswordOutput.getErrorCode() != null)
                            onChangePasswordError();
                        else
                            onLoginSucess();
                    }
                }
            }
        }catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

    }

    private void onChangePasswordError() {
//        showErrorDalioge(changePasswordOutput.getErrorMsgEn());
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        if (currentLanguage.equalsIgnoreCase("en"))
            bundle.putString(ConstantUtils.errorString, changePasswordOutput.getErrorMsgEn());
        else
            bundle.putString(ConstantUtils.errorString, changePasswordOutput.getErrorMsgAr());

        errorFragment.setArguments(bundle);
        regActivity.replaceFragmnet(errorFragment, R.id.frameLayout, true);

    }


    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        try {
            if (isVisible())
                regActivity.onBackPressed();
//            progressBar.setVisibility(View.GONE);
        }catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    private boolean isNotEmpty() {
        if (new_passwrd.getText().toString().length() == 0
                || confirm_new_password.getText().toString().length() == 0)
            return false;
        else
            return true;
    }

    private boolean isValid() {
        boolean isValid = true;
        if (new_passwrd.getText().toString().trim().length() == 0) {
            setTextInputLayoutError(til_password, getString(R.string.please_enter_password));
            isValid = false;


        } else {
            removeTextInputLayoutError(til_password);
        }
        if (confirm_new_password.getText().toString().trim().length() == 0) {
            setTextInputLayoutError(til_confirmPassword, getString(R.string.please_confirm_password));
            isValid = false;
        } else {
            removeTextInputLayoutError(til_confirmPassword);
        }
        if (isValid && (!ValidationHelper.isValidPassword(new_passwrd.getText().toString().trim()) || new_passwrd.getText().toString().trim().length() < 6)) {
            setTextInputLayoutError(til_password, getString(R.string.password_criteria_not_matched));
            isValid = false;
        } else if (isValid) {
            removeTextInputLayoutError(til_password);
        }
        if (isValid) {
            if (!new_passwrd.getText().toString().trim().equals(confirm_new_password.getText().toString().trim())) {
                //error fragment handling here
                hideKeyBoard();
                setTextInputLayoutError(til_confirmPassword, getString(R.string.password_didnot_match));
                isValid = false;
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGGING_create_account_invalid_password);

//                ErrorFragment errorFragment = new ErrorFragment();
//                Bundle bundle = new Bundle();
//                isValid = false;
//                bundle.putString(ConstantUtils.errorString, getString(R.string.password_didnot_match));
//                errorFragment.setArguments(bundle);
//                regActivity.replaceFragmnet(errorFragment, R.id.frameLayout, true);
            }
        }else{
            CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGGING_create_account_invalid_password);

        }

        return isValid;
    }

    private void disableLoginButton() {
        loginBtn.setAlpha(0.5f);
        loginBtn.setEnabled(false);
    }

    private void enableLoginButton() {
        loginBtn.setAlpha(1.0f);
        loginBtn.setEnabled(true);
    }

    private void onLoginSucess() {
//        if (service == null) {
//            service = new Intent(getActivity(), LastUpdated.class);
//            TODO getActivity().startService(service);
//        }
//        sharedPrefrencesManger.setPassword(password.getText().toString().trim());
//        if(changePasswordOutput!=null)
//        sharedPrefrencesManger.setRegisterationID(changePasswordOutput.getUserProfile().getRegistrationId());
        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_create_account_password_creation);


        sharedPrefrencesManger.setEmail(changePasswordOutput.getUserProfile().getEmail());
        sharedPrefrencesManger.setNickName(changePasswordOutput.getUserProfile().getNickName());
        sharedPrefrencesManger.setGender(changePasswordOutput.getUserProfile().getGender());
        sharedPrefrencesManger.setUserProfileImage(changePasswordOutput.getUserProfile().getProfilePicUrl());
        sharedPrefrencesManger.setFullName(changePasswordOutput.getUserProfile().getFullName());
        sharedPrefrencesManger.setMobileNo(changePasswordOutput.getUserProfile().getMsisdn());
        sharedPrefrencesManger.setInvitation(changePasswordOutput.getUserProfile().getInvitationCode());
        sharedPrefrencesManger.setAuthToken(mBaseResponseModel.getAuthToken());
        sharedPrefrencesManger.setUserProfileObj(changePasswordOutput.getUserProfile());

        onLogin();
    }

}
