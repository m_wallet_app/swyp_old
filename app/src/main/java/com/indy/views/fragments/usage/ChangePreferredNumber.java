package com.indy.views.fragments.usage;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.indy.R;
import com.indy.adapters.PackagesListAdapter;
import com.indy.models.ChangePreferredNumberInputModel;
import com.indy.models.purchaseStoreBundle.OrderAdditionalInfoList;
import com.indy.models.purchaseStoreBundle.OrderItems;
import com.indy.models.purchaseStoreBundle.PurchaseStoreOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.services.ChangePreferredNumberService;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.UnlimitedMinutesPackageActivity;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.indy.views.fragments.utils.MasterFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChangePreferredNumber extends MasterFragment {

    public View mView;
    private TextView tv_numberConfirmation, tv_cancel;
    private Button bt_change,bt_ok;
    private LinearLayout ll_error;
    private ScrollView sv_root;
    private PurchaseStoreOutput purchaseStoreOutput;
    private FrameLayout frameLayout;
    public ChangePreferredNumber() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_change_preferred_number, container, false);
        initUI();
        return mView;
    }

    @Override
    public void initUI() {
        super.initUI();

        tv_cancel = (TextView) mView.findViewById(R.id.cancelBtn);
        bt_change = (Button) mView.findViewById(R.id.changeBtn);
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        ll_error = (LinearLayout) mView.findViewById(R.id.ll_error);
        sv_root = (ScrollView) mView.findViewById(R.id.sv_root);
        tv_numberConfirmation = (TextView) mView.findViewById(R.id.tv_confirmation_text);
//        ll_error.setVisibility(View.GONE);
//        sv_root.setVisibility(View.VISIBLE);
        frameLayout = (FrameLayout) mView.findViewById(R.id.frameLayout);
        frameLayout.setVisibility(View.GONE);
        String numberConfirmation;
        if (getArguments() != null && getArguments().getString(ConstantUtils.mobileNo) != null) {
            numberConfirmation = getString(R.string.the_number) + " " +getArguments().getString(ConstantUtils.mobileNo) +" " +
                    getString(R.string.will_be_set_as_your);

        }else{
            numberConfirmation = "";
        }
        tv_numberConfirmation.setText(numberConfirmation);

        bt_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//TODO web service here
                onConsumeService();
            }
        });



        bt_ok = (Button) mView.findViewById(R.id.btn_ok);
        bt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //ok

                getActivity().onBackPressed();
                getActivity().onBackPressed();
            }
        });
    }


    @Override
    public void onConsumeService() {
        super.onConsumeService();

        LoadingFragmnet loadingFragmnet = new LoadingFragmnet();
        Bundle bundle = new Bundle();
        bundle.putString("fromRedeem","check");
        loadingFragmnet.setArguments(bundle);
        ((UnlimitedMinutesPackageActivity) getActivity()).addFragmnet(loadingFragmnet,R.id.frameLayout,true);
        ChangePreferredNumberInputModel changePreferredNumberInputModel = new ChangePreferredNumberInputModel();
        changePreferredNumberInputModel.setDeviceId(deviceId);
        changePreferredNumberInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
        changePreferredNumberInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        changePreferredNumberInputModel.setLang(currentLanguage);
        changePreferredNumberInputModel.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        changePreferredNumberInputModel.setToken(token);
        changePreferredNumberInputModel.setAppVersion(appVersion);
        changePreferredNumberInputModel.setOsVersion(osVersion);
        changePreferredNumberInputModel.setChannel(channel);
        changePreferredNumberInputModel.setAuthToken(authToken);

        OrderItems orderItems = new OrderItems();
        OrderAdditionalInfoList[] additionalInfoListArray = new OrderAdditionalInfoList[1];
        OrderAdditionalInfoList additionalInfoList = new OrderAdditionalInfoList();
        if (currentLanguage.equals("en")) {
            additionalInfoList.setName(PackagesListAdapter.usagePackageItem.getNameEn());
        } else {
            additionalInfoList.setName(PackagesListAdapter.usagePackageItem.getNameAr());

        }
        additionalInfoList.setValue(""+PackagesListAdapter.usagePackageItem.getOfferList().get(0).getPackageConsumption().getQuota());
        orderItems.setOfferId(PackagesListAdapter.usagePackageItem.getId());
        orderItems.setOldPreferedMsisdn(getArguments().getString(ConstantUtils.oldPreferredNumber));
        orderItems.setPreferedMsisdn(getArguments().getString(ConstantUtils.mobileNo));
        orderItems.setOrderAdditionalInfoList(additionalInfoListArray);
        changePreferredNumberInputModel.setOrderItem(orderItems);
        new ChangePreferredNumberService(this,changePreferredNumberInputModel);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
//        getActivity().onBackPressed();
        if(responseModel!=null){
            purchaseStoreOutput = (PurchaseStoreOutput) responseModel.getResultObj();
            if(purchaseStoreOutput!=null && purchaseStoreOutput.getPurchaseBundleList()!=null &&
                    purchaseStoreOutput.getResponseCode()!=null && purchaseStoreOutput.getResponseCode().equals("true")){
                //successcase
            }else{
                //failure case
                onChangeNumberError();
            }
        }


    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        getActivity().onBackPressed();
        super.onErrorListener(responseModel);

    }

    public void onChangeNumberError(){
        if(purchaseStoreOutput!=null && purchaseStoreOutput.getErrorMsgEn()!=null) {
            if(currentLanguage.equals("en")){
                showErrorFragment(purchaseStoreOutput.getErrorMsgEn());
            }else{
                showErrorFragment(purchaseStoreOutput.getErrorMsgAr());
            }
        }else{
            showErrorFragment(getString(R.string.generice_error));
        }

    }

    private void showErrorFragment(String error) {
        frameLayout.setVisibility(View.VISIBLE);

        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, error);
        errorFragment.setArguments(bundle);
        ((UnlimitedMinutesPackageActivity)getActivity()).addFragmnet(errorFragment, R.id.frameLayout, true);
    }

}
