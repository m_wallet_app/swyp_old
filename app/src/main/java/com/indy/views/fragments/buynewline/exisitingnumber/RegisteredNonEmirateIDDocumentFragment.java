package com.indy.views.fragments.buynewline.exisitingnumber;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.indy.R;
import com.indy.views.activites.RegisterationActivity;
import com.indy.views.fragments.utils.MasterFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisteredNonEmirateIDDocumentFragment extends MasterFragment {

    private Button backImg, loginBtn;
    private TextView tv_cancel;
    private View mView;

    public RegisteredNonEmirateIDDocumentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_registered_non_emirate_iddocument, container, false);

        tv_cancel = (TextView) mView.findViewById(R.id.btn_cancel);
        initUI();
        return mView;
    }

    @Override
    public void initUI() {
        super.initUI();

//        backImg = (Button) mView.findViewById(R.id.backImg);
//        backImg.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getActivity().onBackPressed();
//            }
//        });
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        loginBtn = (Button) mView.findViewById(R.id.btn_ok);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                regActivity.finish();
                Intent intent = new Intent(getActivity(), RegisterationActivity.class);
                intent.putExtra("fragIndex", "7");
                startActivity(intent);
            }
        });
        try {
            ((RegisterationActivity) getActivity()).hideHeaderLayout();
        } catch (Exception ex) {

        }
    }


}
