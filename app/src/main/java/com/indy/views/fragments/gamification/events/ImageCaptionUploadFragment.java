package com.indy.views.fragments.gamification.events;


import android.app.Activity;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.utils.ConstantUtils;
import com.indy.utils.ImageUtil;
import com.indy.views.fragments.gamification.models.EventsList;
import com.indy.views.fragments.gamification.models.uploadeventimage.UploadEventImageOutput;
import com.indy.views.fragments.gamification.services.UploadEventImageService;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.MasterFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class ImageCaptionUploadFragment extends MasterFragment {

    private View rootView;
    private RelativeLayout rl_help, rl_back;
    private ImageView iv_photo;
    private Button btnUpload;
    private EditText et_caption;
    private Bitmap finalBitmap;
    private TextView tv_header;
    private Uri imageUri;
    private UploadEventImageOutput uploadEventImageOutput;
    private ProgressBar progressBar;
    private EventsList eventObject;

    public ImageCaptionUploadFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_image_caption_upload, container, false);
        eventObject = (EventsList) getActivity().getIntent().getParcelableExtra(ConstantUtils.EVENT_KEY);
        initUI();
        return rootView;
    }

    @Override
    public void initUI() {
        super.initUI();
        tv_header = (TextView) rootView.findViewById(R.id.titleTxt);
        tv_header.setText(getResources().getString(R.string.photo));
        rl_back = (RelativeLayout) rootView.findViewById(R.id.backLayout);
        rl_help = (RelativeLayout) rootView.findViewById(R.id.helpLayout);
        rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        rl_help.setVisibility(View.INVISIBLE);
        if (getArguments() != null && getArguments().getParcelable(ConstantUtils.SOURCE_IMAGE) != null) {
            finalBitmap = getArguments().getParcelable(ConstantUtils.SOURCE_IMAGE);
            imageUri = ImageUtil.getImageUri(getActivity(), finalBitmap);
        }
        //sourceBitmap = getBitmapSource();
        iv_photo = (ImageView) rootView.findViewById(R.id.iv_profile_image);
        rl_back = (RelativeLayout) rootView.findViewById(R.id.backLayout);
        rl_help = (RelativeLayout) rootView.findViewById(R.id.helpLayout);
        rl_help.setVisibility(View.INVISIBLE);
        rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        et_caption = (EditText) rootView.findViewById(R.id.et_caption);
        iv_photo.setImageBitmap(finalBitmap);
        btnUpload = (Button) rootView.findViewById(R.id.uploadBtn);
        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //todo call service here
                //navigate to success screen
                onConsumeService();
            }
        });
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressID);

    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        progressBar.setVisibility(View.VISIBLE);
        new UploadEventImageService(this, imageUri, getActivity(), deviceId, sharedPrefrencesManger.getUserSessionId(), ConstantUtils.INDY_TALOS_ID, sharedPrefrencesManger.getUserId(), currentLanguage, channel, appVersion, osVersion, sharedPrefrencesManger.getRegisterationID(), sharedPrefrencesManger.getMobileNo(), eventObject.getIndyEventId(), ConstantUtils.TOP_RATED, et_caption.getText().toString(), token, sharedPrefrencesManger.getMobileNo(), authToken);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            progressBar.setVisibility(View.GONE);
            switch (responseModel.getServiceType()) {
                case ServiceUtils.UPLOAD_EVENT_IMAGE: {
                    if (responseModel != null) {
                        uploadEventImageOutput = (UploadEventImageOutput) responseModel.getResultObj();
                        if (uploadEventImageOutput != null && uploadEventImageOutput.getErrorCode() != null)
                            uploadEventImageError();
                        else
                            uploadEventImageSuccess();
                    }
                    break;
                }
            }
        }catch (Exception ex){

        }
    }

    private void uploadEventImageSuccess() {
        if (uploadEventImageOutput.isUploaded()) {
            Fragment[] fragmentsList = {new ImageCaptionUploadFragment(),
                    new ImageProcessingFragment()};
            ((UploadEventPhotoActivity) getActivity()).removeFragment(fragmentsList);
            UploadEventSuccessfulFragment uploadEventSuccessfulFragment = new UploadEventSuccessfulFragment();
            Bundle bundle = new Bundle();
            bundle.putString(ConstantUtils.UPLOADED_ENTRY_ID, uploadEventImageOutput.getUploadedEntryId());
            bundle.putString(ConstantUtils.UPLOAD_SUCCESS_TEXT,uploadEventImageOutput.getTotalRaffleTicketsMsg());
            bundle.putString(ConstantUtils.UPLOAD_REWARD,uploadEventImageOutput.getUploadReward());
            uploadEventSuccessfulFragment.setArguments(bundle);
            UploadEventPhotoActivity.isUploaded = true;
            ((UploadEventPhotoActivity) getActivity()).replaceFragmnet(uploadEventSuccessfulFragment, R.id.frameLayout, true);
            getActivity().setResult(Activity.RESULT_OK);
        }
    }

    private void uploadEventImageError() {
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString,
                sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)
                        ? uploadEventImageOutput.getErrorMsgEn() : uploadEventImageOutput.getErrorMsgAr());
        errorFragment.setArguments(bundle);
        ((UploadEventPhotoActivity) getActivity()).replaceFragmnet(errorFragment, R.id.frameLayout, true);
    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        progressBar.setVisibility(View.GONE);
    }

}
