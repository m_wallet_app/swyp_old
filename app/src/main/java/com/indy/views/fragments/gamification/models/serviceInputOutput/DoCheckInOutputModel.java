package com.indy.views.fragments.gamification.models.serviceInputOutput;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by mobile on 27/09/2017.
 */

public class DoCheckInOutputModel extends MasterErrorResponse{

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("lotteryReward")
    @Expose
    private CheckinRewardModel lotteryReward = null;

    @SerializedName("constraintMessage")
    @Expose
    private String constraintMessage;

    public String getConstraintMessage() {
        return constraintMessage;
    }

    public void setConstraintMessage(String constraintMessage) {
        this.constraintMessage = constraintMessage;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public CheckinRewardModel getLotteryReward() {
        return lotteryReward;
    }

    public void setLotteryReward(CheckinRewardModel lotteryReward) {
        this.lotteryReward = lotteryReward;
    }


}
