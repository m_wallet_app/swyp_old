package com.indy.views.fragments.store.mainstore;

import com.indy.models.bundles.BundleListOutPut;

/**
 * Created by hadi.mehmood on 10/20/2016.
 */
public class StoreItem {

    BundleListOutPut bundleListOutPut;
    boolean isAdded;
    int count;

    public BundleListOutPut getBundleListOutPut() {
        return bundleListOutPut;
    }

    public void setBundleListOutPut(BundleListOutPut bundleListOutPut) {
        this.bundleListOutPut = bundleListOutPut;
    }

    public boolean isAdded() {
        return isAdded;
    }

    public void setAdded(boolean added) {
        isAdded = added;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
