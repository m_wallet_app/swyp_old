package com.indy.views.fragments.store.recharge;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.indy.R;
import com.indy.helpers.GoogleMapsHelper;
import com.indy.models.location.GeoLocation;
import com.indy.models.location.LocationInputModel;
import com.indy.models.location.LocationModelResponse;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.services.GetNearestLocationService;
import com.indy.utils.ConstantUtils;
import com.indy.utils.GPSTracker;
import com.indy.views.activites.HelpActivity;
import com.indy.views.activites.MasterActivity;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;

import java.util.HashMap;

/**
 * Created by Amir.jehangir on 10/16/2016.
 */
public class LcoateNearestKiosk extends MasterActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
        GoogleMap.OnInfoWindowClickListener, LocationListener {
    //    View view;
    public GoogleMap mMap;
    public MarkerOptions marker;
    public static HashMap<String, Integer> mMarkerList;
    LocationManager locationManager;
    GPSTracker gpsTracker;
    private Button locateMe;

    public TextView distanceId, locationNameId, headerText, titletext;
    public LocationModelResponse mLocationModelResponse;
    public String locationName = null;
    private Double mLatitude, mLongitude;
    Location mlocation;
    TextView title;
    private Button backImg, helpBtn;
    public static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
    };
    private FrameLayout frameLayout;
    public static final int LOCATION_REQUEST = 1340;
    GoogleMapsHelper googleMapsHelper;
    EditText textInputEditText_address;
    RelativeLayout headerLayoutID;


    public LcoateNearestKiosk() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_locatenearest);
        headerLayoutID = (RelativeLayout) findViewById(R.id.headerLayoutID);
        headerLayoutID.setBackground(getResources().getDrawable(R.drawable.support_bg));
        gpsTracker = new GPSTracker(this);
        mMarkerList = new HashMap<String, Integer>();
        initUI();
        setListeners();
    }

    @Override
    public void initUI() {
        super.initUI();
//        titletext = (TextView)view.findViewById(R.id.titleTxt);
//        ((RechargeActivity) getActivity()).titleView.setText(getResources().getString(R.string.payment_title));
        title = (TextView) findViewById(R.id.titleTxt);
        title.setText(R.string.payment_spaced);
        backImg = (Button) findViewById(R.id.backImg);
        helpBtn = (Button) findViewById(R.id.helpBtn);
        helpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LcoateNearestKiosk.this, HelpActivity.class);
                startActivity(intent);
            }
        });
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        helpBtn.setVisibility(View.INVISIBLE);
        textInputEditText_address = (EditText) findViewById(R.id.AddressEditText);
        frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }


        googleMapsHelper = new GoogleMapsHelper(this);
        locateMe = (Button) findViewById(R.id.btn_get_directions);

        locateMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mLatitude != null && mLongitude != null)
                    googleMapsHelper.onGoogleMpasOpen(mLatitude, mLongitude, locationName);
            }
        });

        setListeners();
        disableNextBtn();

    }

    private void setListeners() {
        textInputEditText_address.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (textInputEditText_address.getText().toString().trim().length() > 0) {
                    enableNextBtn();
                } else {
                    disableNextBtn();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        checkPermission();
        isGPSEnabled();
    }

    private void checkPermission() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                this.requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            }
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        int index = -1;
        index = mMarkerList.get(marker.getId());
        if (index != -1) {
            String distance = mLocationModelResponse.getNearestLocationList().get(index).getDistance().toString();

            locationName = mLocationModelResponse.getNearestLocationList().get(index).getAddressEn();

            mLatitude = mLocationModelResponse.getNearestLocationList().get(index).getGeoLocation().getLatitude();
            mLongitude = mLocationModelResponse.getNearestLocationList().get(index).getGeoLocation().getLongitude();
//            locationNameId.setText(locationName);
            LatLng latLng = new LatLng(mLatitude, mLongitude);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        }

        return false;
    }


    @Override
    public void onInfoWindowClick(Marker marker) {
        GoogleMapsHelper openGoogleMaps = new GoogleMapsHelper(this);
        openGoogleMaps.onGoogleMpasOpen(marker.getPosition().latitude, marker.getPosition().longitude, locationName);
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    private boolean canAccessLocation() {
        if (!hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
            return false;
        } else {
            return true;
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == checkSelfPermission(perm));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (gpsTracker != null) {
            gpsTracker.getLocation();
        }
        LocationManager locationManager = (LocationManager)
                getSystemService(LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            showErrorFragment();
        } else {
            onConsumeService();
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 0:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    isGPSEnabled();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    requestPermissions(LOCATION_PERMS, 0);
//                    Toast.makeText(this, "To use this feature, kindly allow Ding access to your location.", Toast.LENGTH_LONG).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 100:
                LocationManager locationManager = (LocationManager)
                        getSystemService(LOCATION_SERVICE);
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    onBackPressed();
                    showErrorFragment();
                } else {
                    onConsumeService();
                }
                break;
            default:
                break;
        }
        onBackPressed();
    }

    private void isGPSEnabled() {
        LocationManager locationManager = (LocationManager)
                getSystemService(LOCATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (canAccessLocation()) {// gps is enable
//                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && canAccessLocation()) {// gps is enable
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {// gps is disable
                    showErrorFragment();
                } else {
                    if (gpsTracker.canGetLocation()) {
                        mlocation = gpsTracker.getLocation();

                        onConsumeService();
                    }
//                    chec/kLocation();
                }
//                checkLocation();
            } else {
                requestPermissions(LOCATION_PERMS, 0);
            }

        } else {
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) { // gps is disable
//                checkForGps();
            } else { // gps is enable
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {// gps is disable

                } else {
                    if (gpsTracker.canGetLocation()) {
                        mlocation = gpsTracker.getLocation();

                        onConsumeService();
                    }
//                    chec/kLocation();
                }
            }

        }

    }

    private void checkLocation() {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, true);
        locationManager.requestLocationUpdates(provider, 10000, 0, this);
        mMap.setMyLocationEnabled(true);

        mlocation = locationManager.getLastKnownLocation(provider);
        if (mlocation == null) {
            mlocation = mMap.getMyLocation();
        }
        if (mlocation != null) {
            onLocationChanged(mlocation);
        }
    }

    public void showErrorFragment() {
        frameLayout.setVisibility(View.VISIBLE);
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, "Cannot get Location please enable GPS.");
        bundle.putString(ConstantUtils.settingsButtonEnabled, "true");
        bundle.putString(ConstantUtils.buttonTitle, "Settings");
        errorFragment.setArguments(bundle);
        replaceFragmnet(errorFragment, R.id.frameLayout, true);

    }

    public void showServiceErrorFragment() {
        frameLayout.setVisibility(View.VISIBLE);
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        if (mLocationModelResponse != null && mLocationModelResponse.getErrorMsgEn() != null) {
            if (currentLanguage != null && currentLanguage.equals("en")) {
                bundle.putString(ConstantUtils.errorString, mLocationModelResponse.getErrorMsgEn());
            } else {
                bundle.putString(ConstantUtils.errorString, mLocationModelResponse.getErrorMsgAr());
            }
        } else {
            bundle.putString(ConstantUtils.errorString, getString(R.string.generice_error));
        }
        errorFragment.setArguments(bundle);
        addFragmnet(errorFragment, R.id.frameLayout, true);

    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
//        progressID.setVisibility(View.VISIBLE);
        frameLayout.setVisibility(View.VISIBLE);
        if (mlocation != null) {
            addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
            LocationInputModel locationInputModel = new LocationInputModel();
            locationInputModel.setSignificant(ConstantUtils.shops_findus);
            locationInputModel.setSize(ConstantUtils.number_of_locations);
            GeoLocation geoLocation = new GeoLocation();
            geoLocation.setLatitude(mlocation.getLatitude());
            geoLocation.setLongitude(mlocation.getLongitude());
            locationInputModel.setGeoLocation(geoLocation);
            locationInputModel.setAppVersion(appVersion);
            locationInputModel.setToken(token);
            locationInputModel.setOsVersion(osVersion);
            locationInputModel.setChannel(channel);
            locationInputModel.setDeviceId(deviceId);
            locationInputModel.setAuthToken(authToken);
            new GetNearestLocationService(this, locationInputModel);
        } else {

            if (gpsTracker.canGetLocation()) {
                mlocation = gpsTracker.getLocation();
                onConsumeService();
            } else {
                onConsumeService();
            }
        }
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
//        progressID.setVisibility(View.GONE);
        try {
            if (responseModel != null && responseModel.getResultObj() != null) {
                onBackPressed();
                frameLayout.setVisibility(View.GONE);
                mLocationModelResponse = (LocationModelResponse) responseModel.getResultObj();
                if (mLocationModelResponse != null && mLocationModelResponse.getErrorMsgEn() != null) {
                    showServiceErrorFragment();
                } else if (mLocationModelResponse != null && mLocationModelResponse.getNearestLocationList() != null) {
                    drawMapPins();

                } else {
                    showErrorFragment();
                }
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
//        super.onErrorListener(responseModel);
        try {
            frameLayout.setVisibility(View.GONE);
            showServiceErrorFragment();
        }catch (Exception ex){
            if(ex!=null){
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    public void drawMapPins() {
        mMap.clear();
        LatLng fLatLng = null;
        String distance = null;
        Marker temp = null;
        for (int i = 0; i < mLocationModelResponse.getNearestLocationList().size(); i++) {
            fLatLng = new LatLng(mLocationModelResponse.getNearestLocationList().get(i).getGeoLocation().getLatitude(),
                    mLocationModelResponse.getNearestLocationList().get(i).getGeoLocation().getLongitude());
            LatLng mLatLng = new LatLng(mLocationModelResponse.getNearestLocationList().get(i).getGeoLocation().getLatitude(),
                    mLocationModelResponse.getNearestLocationList().get(i).getGeoLocation().getLongitude());
            marker = new MarkerOptions().position(mLatLng);


//             marker.icon(BitmapDescriptorFactory.fromBitmap(etisaltLogo));
            String title = "";
            if (currentLanguage.equals("en")) {
                title = mLocationModelResponse.getNearestLocationList().get(i).getAddressEn();
            } else {
                title = mLocationModelResponse.getNearestLocationList().get(i).getAddressAr();
            }
            if (i == 0) {
                temp = mMap.addMarker(
                        marker.position(mLatLng)
                                .title(title)
                                .snippet(mLocationModelResponse.getNearestLocationList().get(i).getDistance() + " Km")
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.location_icon_fill)));
            } else {
                temp = mMap.addMarker(
                        marker.position(mLatLng)
                                .title(title)
                                .snippet(mLocationModelResponse.getNearestLocationList().get(i).getDistance() + " Km")
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.location_orange)));

            }
            mMarkerList.put(temp.getId(), i);
        }
        if (mLocationModelResponse.getNearestLocationList().size() > 0) {
            fLatLng = new LatLng(mLocationModelResponse.getNearestLocationList().get(0).getGeoLocation().getLatitude(),
                    mLocationModelResponse.getNearestLocationList().get(0).getGeoLocation().getLongitude());

            distance = mLocationModelResponse.getNearestLocationList().get(0).getDistance().toString();
            locationName = mLocationModelResponse.getNearestLocationList().get(0).getAddressEn();
//         distanceId.setText( getString(R.string.nearest_shop) + " " + distance + "  " +  getString(R.string.away));
            mLatitude = mLocationModelResponse.getNearestLocationList().get(0).getGeoLocation().getLatitude();
            mLongitude = mLocationModelResponse.getNearestLocationList().get(0).getGeoLocation().getLongitude();
//         locationNameId.setText( locationName);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(fLatLng,
                    13));
            mMap.setOnMarkerClickListener(this);
            mMap.setOnInfoWindowClickListener(this);
            mMap.setInfoWindowAdapter(new MyInfoWindowAdapter());
        }
    }


    class MyInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private final View myContentsView;

        MyInfoWindowAdapter() {
            myContentsView = getLayoutInflater().inflate(R.layout.my_info_window, null);
        }

        @Override
        public View getInfoContents(Marker marker) {

            TextView tvTitle = ((TextView) myContentsView.findViewById(R.id.tv_name));
            tvTitle.setText(marker.getTitle());
            TextView tvSnippet = ((TextView) myContentsView.findViewById(R.id.tv_distance));
            tvSnippet.setText(marker.getSnippet());

            textInputEditText_address.setText(marker.getTitle());
            textInputEditText_address.setEnabled(false);
            textInputEditText_address.setInputType(0);

            return myContentsView;
        }

        @Override
        public View getInfoWindow(Marker marker) {
            // TODO Auto-generated method stub
            return null;
        }

    }

    private void enableNextBtn() {
        locateMe.setAlpha(1f);
        locateMe.setClickable(true);
    }

    private void disableNextBtn() {
        locateMe.setAlpha(.5f);
        locateMe.setClickable(false);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (frameLayout.getVisibility() != View.VISIBLE) {
//            finish();
        }
    }
}
