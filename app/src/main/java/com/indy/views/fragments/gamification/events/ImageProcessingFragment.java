package com.indy.views.fragments.gamification.events;


import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indy.MainActivity;
import com.indy.R;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.gamification.OnImageFilterSelected;
import com.indy.views.fragments.gamification.adapters.ImageFiltersAdapter;
import com.indy.views.fragments.gamification.models.getimagefilters.GetImageFiltersInput;
import com.indy.views.fragments.gamification.models.getimagefilters.GetImageFiltersOutput;
import com.indy.views.fragments.gamification.models.getimagefilters.ImageFilter;
import com.indy.views.fragments.gamification.services.GetImageFiltersService;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.MasterFragment;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ImageProcessingFragment extends MasterFragment implements OnImageFilterSelected {

    private View rootView;
    private RelativeLayout rl_help, rl_back;
    private TextView tv_title, tv_votes, tv_username, tv_photoDescription, tv_header;
    private ImageView iv_photo;
    private Button btnContinue, btnUpload;
    private Bitmap sourceBitmap, finalBitmap;
    private GetImageFiltersOutput getImageFiltersOutput;
    private ImageFiltersAdapter imageFiltersAdapter;
    private RecyclerView filterRecycler;
    private ProgressBar progressBar;
    public int height, width;

    public ImageProcessingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_image_processing, container, false);
        initUI();
        onConsumeService();
        return rootView;
    }

    @Override
    public void initUI() {
        super.initUI();
        if (getArguments() != null && getArguments().getParcelable(ConstantUtils.SOURCE_IMAGE) != null)
            sourceBitmap = getArguments().getParcelable(ConstantUtils.SOURCE_IMAGE);
        tv_header = (TextView) rootView.findViewById(R.id.titleTxt);
        tv_header.setText(getResources().getString(R.string.photo));
        rl_back = (RelativeLayout) rootView.findViewById(R.id.backLayout);
        rl_help = (RelativeLayout) rootView.findViewById(R.id.helpLayout);
        rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        rl_help.setVisibility(View.INVISIBLE);
        tv_photoDescription = (TextView) rootView.findViewById(R.id.titleTxt);
        tv_title = (TextView) rootView.findViewById(R.id.tv_photo_username);
        tv_photoDescription = (TextView) rootView.findViewById(R.id.tv_photo_description);
        tv_votes = (TextView) rootView.findViewById(R.id.tv_vote_counter);
        iv_photo = (ImageView) rootView.findViewById(R.id.iv_profile_image);
        rl_back = (RelativeLayout) rootView.findViewById(R.id.backLayout);
        rl_help = (RelativeLayout) rootView.findViewById(R.id.helpLayout);
        rl_help.setVisibility(View.INVISIBLE);
        rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        btnContinue = (Button) rootView.findViewById(R.id.continueBtn);
        btnUpload = (Button) rootView.findViewById(R.id.uploadBtn);
        iv_photo.setImageBitmap(sourceBitmap);
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finalBitmap = ((BitmapDrawable) iv_photo.getDrawable()).getBitmap();
                //navigate to caption screen
                ImageCaptionUploadFragment imageCaptionUploadFragment = new ImageCaptionUploadFragment();
                Bundle bundle = new Bundle();
                bundle.putParcelable(ConstantUtils.SOURCE_IMAGE, finalBitmap);
                imageCaptionUploadFragment.setArguments(bundle);
                ((UploadEventPhotoActivity) getActivity()).addFragmnet(imageCaptionUploadFragment, R.id.frameLayout, true);
            }
        });
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressID);
        filterRecycler = (RecyclerView) rootView.findViewById(R.id.filter_recycler);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        filterRecycler.setLayoutManager(linearLayoutManager);

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = (int) getResources().getDimension(R.dimen.x250);
        Log.d("actual height",height+"");
        DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();
        height *= displayMetrics.density;
        Log.d("density height",height+"");
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        progressBar.setVisibility(View.VISIBLE);
        GetImageFiltersInput getImageFiltersInput = new GetImageFiltersInput();
        getImageFiltersInput.setChannel(channel);
        getImageFiltersInput.setLang(currentLanguage);
        getImageFiltersInput.setMsisdn(sharedPrefrencesManger.getMobileNo());
        getImageFiltersInput.setAppVersion(appVersion);
        getImageFiltersInput.setAuthToken(authToken);
        getImageFiltersInput.setDeviceId(deviceId);
        getImageFiltersInput.setToken(token);
        getImageFiltersInput.setUserSessionId(sharedPrefrencesManger.getUserSessionId());
        getImageFiltersInput.setUserId(sharedPrefrencesManger.getUserId());
        getImageFiltersInput.setApplicationId(ConstantUtils.INDY_TALOS_ID);
        getImageFiltersInput.setImsi(sharedPrefrencesManger.getMobileNo());
        getImageFiltersInput.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        getImageFiltersInput.setOsVersion(osVersion);
        new GetImageFiltersService(this, getImageFiltersInput);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        progressBar.setVisibility(View.GONE);
        switch (responseModel.getServiceType()) {
            case ServiceUtils.GET_IMAGES_FILTERS: {
                if (responseModel != null) {
                    getImageFiltersOutput = (GetImageFiltersOutput) responseModel.getResultObj();
                    if (getImageFiltersOutput != null)
                        if (getImageFiltersOutput.getFilters() != null
                                && !getImageFiltersOutput.getFilters().isEmpty()) {
                            getImageFiltersSuccess();
                        } else {
                            getImageFiltersError();
                        }
                }
            }
        }
    }

    ImageView iv_caption;
    public List<ImageFilter> imageFilters = new ArrayList<>();
    private void getImageFiltersSuccess() {
        imageFilters = getImageFiltersOutput.getFilters();
        width = iv_photo.getWidth();
        for (int i = 0; i < getImageFiltersOutput.getFilters().size(); i++) {
//            Bitmap newBitmap = Bitmap.createBitmap(sourceBitmap.getWidth(), sourceBitmap.getHeight(), Bitmap.Config.ARGB_8888);
//// create a canvas where we can draw on
//            Canvas canvas = new Canvas(newBitmap);
//// create a paint instance with alpha
//            Paint alphaPaint = new Paint();
//            alphaPaint.setAlpha(42);
//// now lets draw using alphaPaint instance
//            canvas.drawBitmap(originalBitmap, 0, 0, alphaPaint);
            final int position = i;
//            getImageFiltersOutput.getFilters().get(i).setBitmap(CommonMethods.getFilterAppliedBitmap(sourceBitmap, "orange", getActivity()));
            ImageLoader.getInstance().loadImage(getImageFiltersOutput.getFilters().get(i).getUrl(), new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    super.onLoadingComplete(imageUri, view, loadedImage);
                    Log.d("Image URL",imageFilters.get(position+1).getUrl());


                    Bitmap resized = Bitmap.createScaledBitmap(loadedImage, width,height, false);
//                    Bitmap bitmap = BitmapFactory.decodeResource(getResources(),R.drawable.filter_final);
                    imageFilters.get(position+1).setBitmap(CommonMethods.getFilterAppliedBitmap(sourceBitmap, resized));
                    imageFiltersAdapter.notifyItemChanged(position+1);
                }
            });
        }
        ImageFilter imageFilterTemp = new ImageFilter();
        imageFilterTemp.setId(0);
        imageFilterTemp.setName("default image");
        imageFilterTemp.setUrl("");
        imageFilterTemp.setBitmap(sourceBitmap);
        imageFilters.add(0, imageFilterTemp);

        imageFiltersAdapter = new ImageFiltersAdapter(imageFilters, getActivity(), this);
        filterRecycler.setAdapter(imageFiltersAdapter);
    }

    private void getImageFiltersError() {
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)
                ? getImageFiltersOutput.getErrorMsgEn() : getImageFiltersOutput.getErrorMsgAr());
        errorFragment.setArguments(bundle);
        ((MainActivity) getActivity()).replaceFragmnet(errorFragment, R.id.frameLayout, true);
    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void filterSelected(ImageFilter filter, int position) {
        if (position == 0) {
            iv_photo.setImageBitmap(sourceBitmap);
        } else {
            if (filter.getUrl() != null && !filter.getUrl().isEmpty()){
                iv_photo.setImageBitmap(imageFilters.get(position).getBitmap());
            }
//                ImageLoader.getInstance().loadImage(filter.getUrl(), new SimpleImageLoadingListener() {
//                    @Override
//                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//                        super.onLoadingComplete(imageUri, view, loadedImage);
////                        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.filter_final);
//
//                        iv_photo.setImageBitmap(CommonMethods.getFilterAppliedBitmap(sourceBitmap, loadedImage));
//                    }
//                });
        }
    }
}