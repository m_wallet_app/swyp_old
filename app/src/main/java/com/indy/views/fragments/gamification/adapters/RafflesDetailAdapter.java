package com.indy.views.fragments.gamification.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.indy.R;
import com.indy.utils.SharedPrefrencesManger;
import com.indy.views.fragments.gamification.models.getrafflesdetails.Prize;

import java.util.List;

public class RafflesDetailAdapter extends RecyclerView.Adapter<RafflesDetailAdapter.ViewHolder>{

    private Context mContext;
    private SharedPrefrencesManger sharedPrefrencesManger;
    private List<Prize> prizeList;

    public RafflesDetailAdapter(List<Prize> prizeList, Context context) {

        this.mContext = context;
        this.sharedPrefrencesManger = new SharedPrefrencesManger(context);
        this.prizeList = prizeList;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.item_raffles_list, viewGroup, false);
        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        Prize prize = prizeList.get(position);

        holder.rv_winners.setLayoutManager(new LinearLayoutManager(mContext));
        holder.rv_winners.setHasFixedSize(true);
        RaffleWinnersAdapter mBadgesAdapter = new RaffleWinnersAdapter(prize.getWinnersList(), mContext);
        holder.rv_winners.setAdapter(mBadgesAdapter);
//        setRecyclerViewHeight(holder.rv_winners);

        holder.tv_title.setText(prize.getPrizeName());


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return prizeList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_title;
        RecyclerView rv_winners;

        public ViewHolder(View v) {
            super(v);
            tv_title = (TextView) v.findViewById(R.id.tv_title);

            rv_winners = (RecyclerView) v.findViewById(R.id.rv_winners);
        }
    }

    public void setRecyclerViewHeight(RecyclerView recyclerView) {
        double total = recyclerView.getAdapter().getItemCount();
        double rows = Math.ceil(total / 2);
        int viewHeight = (int) mContext.getResources().getDimension(R.dimen.x180) * ((int) rows);
        viewHeight += recyclerView.getAdapter().getItemCount();
        recyclerView.getLayoutParams().height = viewHeight;
    }
}
