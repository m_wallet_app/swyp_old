package com.indy.views.fragments.utils;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.indy.R;
import com.indy.utils.ConstantUtils;

import pl.droidsonroids.gif.GifImageView;

/**
 * Created by emad on 10/4/16.
 */

public class LoadingFragmnet extends MasterFragment {
    private View view;
    private TextView loadingTxt;
    private GifImageView iv_gifImageView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {

            if (getArguments() != null && getArguments().getString("fromRedeem") != null) {
                view = inflater.inflate(R.layout.fragment_loading_progress, container, false);
            } else {
                try {
                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        // only for gingerbread and newer versions
                        view = inflater.inflate(R.layout.fragment_loading, container, false);// out of memory
                    }else{
                        view = inflater.inflate(R.layout.fragment_loading_progress, container, false);
                    }

                } catch (OutOfMemoryError error) {
                    view = inflater.inflate(R.layout.fragment_loading_progress, container, false);

                } catch (Exception ex){
                    view = inflater.inflate(R.layout.fragment_loading_progress, container, false);

                }
            }

        }
        initUI();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
        loadingTxt = (TextView) view.findViewById(R.id.loadingTxt);
        if(getArguments()!=null && getArguments().getString(ConstantUtils.loadingTitle)!=null){
            loadingTxt.setText(getArguments().getString(ConstantUtils.loadingTitle));
        }
        if (regActivity != null) {
            regActivity.hideHeaderLayout();
        }

    }

}
