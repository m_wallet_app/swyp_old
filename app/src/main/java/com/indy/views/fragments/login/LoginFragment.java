package com.indy.views.fragments.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.controls.ServiceUtils;
import com.indy.customviews.CustomButton;
import com.indy.helpers.ValidationHelper;
import com.indy.models.getTokenForPassword.GetPreLoginOutput;
import com.indy.models.login.LoginInputModel;
import com.indy.models.login.LoginOutputModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterInputResponse;
import com.indy.services.GetPreLoginInfoService;
import com.indy.services.LoginService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.utils.SharedPrefrencesManger;
import com.indy.views.activites.HelpActivity;
import com.indy.views.activites.RegisterationActivity;
import com.indy.views.activites.SwpeMainActivity;
import com.indy.views.activites.TutorialActivity;
import com.indy.views.fragments.buynewline.PaymentSucessfulllyFragment;
import com.indy.views.fragments.newaccount.NewAccountFragment;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.indy.views.fragments.utils.MasterFragment;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * Created by emad on 9/7/16.
 */
public class LoginFragment extends MasterFragment {
    private View view;
    private Button loginBtn, createAccoutBtn;
    private EditText mobileNo, password;
    private LinearLayout forgotpassword;
    private TextView help;
    public static SessionIdentifierGenerator generator;
    private LoginInputModel loginInputModel;
    public LoginOutputModel loginOutputModel;
    TextInputLayout til_password, til_mobileNumber;
    private Button backImg, helpBtn;
    BaseResponseModel mBaseResponseModel;
    private GetPreLoginOutput getPreLoginOutput;
    public static Intent service;
    public static RegisterationActivity registerationActivity;
    private ImageView ic_arrow;
    public static Context mContext;

    // 0566665924
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_login, container, false);
        initUI();
        setListeners();

        return view;
    }


    public static void stopMyService() {
//        if (registerationActivity != null) {
//            registerationActivity.stopService(service);
//        }
    }

    public static void startMyService() {
//        service = new Intent(mContext, LastUpdated.class);
//        mContext.startService(service);
    }

    @Override
    public void initUI() {
        super.initUI();
        mobileNo = (EditText) view.findViewById(R.id.mobileNo);
        password = (EditText) view.findViewById(R.id.password);
        loginBtn = (Button) view.findViewById(R.id.loginBtn);
        ic_arrow = (ImageView) view.findViewById(R.id.ic_arrow);
        mContext = getActivity();
        registerationActivity = regActivity;

        try {

            PaymentSucessfulllyFragment.paymentSucessfulllyFragmentInstance = null;
        }catch (Exception ex){
            ex.printStackTrace();
        }
        createAccoutBtn = (CustomButton) view.findViewById(R.id.createAccoutBtn);
        forgotpassword = (LinearLayout) view.findViewById(R.id.forgotpassword);
        help = (TextView) view.findViewById(R.id.help);
        til_mobileNumber = (TextInputLayout) view.findViewById(R.id.mobile_number_text_input_layout);
        til_password = (TextInputLayout) view.findViewById(R.id.password_text_input_layout);
        //..............header Buttons............
        RelativeLayout backLayout = (RelativeLayout) view.findViewById(R.id.backLayout);
        RelativeLayout helpLayout = (RelativeLayout) view.findViewById(R.id.helpLayout);
        Button backImg = (Button) view.findViewById(R.id.backImg);
        helpLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), HelpActivity.class);
                startActivity(intent);
            }
        });

        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard();
                getActivity().onBackPressed();
            }
        });
        if (currentLanguage.equalsIgnoreCase("en"))
            ic_arrow.setVisibility(View.VISIBLE);
        else
            ic_arrow.setVisibility(View.GONE);
        sharedPrefrencesManger.setMobileNo("");
        disableNextBtn();
        if (getArguments().getBoolean(ConstantUtils.showBackBtn)) {
            backLayout.setVisibility(View.VISIBLE);
            ((RegisterationActivity) getActivity()).showBackBtn();
        } else {
            backLayout.setVisibility(View.GONE);
            backImg.setVisibility(View.GONE);
            ((RegisterationActivity) getActivity()).hideBackBtn();

        }
        generator = new SessionIdentifierGenerator();

        //..............header Buttons............
        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_LOGIN);


    }

    private void setListeners() {
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //   startActivity(new Intent(getActivity(), RechargeActivity.class));
                if (isValid())
                    onLogin();


            }
        });
        createAccoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//
                onCreateAccount();
            }
        });

        forgotpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onForgotPassword();
            }
        });

        mobileNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (mobileNo.getText().toString().trim().length() > 0 && password.getText().toString().trim().length() > 0) {
                    enableNextBtn();
                } else {
                    disableNextBtn();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (mobileNo.getText().toString().trim().length() > 0 && password.getText().toString().trim().length() > 0) {
                    enableNextBtn();
                } else {
                    disableNextBtn();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void onLogin() {
//
        super.onConsumeService();
        onCallPreLogin();
//        onConsumeService();
    }

    private void onCreateAccount() {
        Bundle bundle2 = new Bundle();

        bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_create_account_selected);
        bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_create_account_selected);
        mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_create_account_selected, bundle2);
        regActivity.replaceFragmnet(new NewAccountFragment(), R.id.frameLayout, true);

    }

    private void onHelp() {
        Intent intent = new Intent(getActivity(), HelpActivity.class);
        startActivity(intent);
    }

    private void onForgotPassword() {

        Bundle bundle2 = new Bundle();

        bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_forgot_password);
        bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_forgot_password);
        mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_forgot_password, bundle2);

        ForgotPasswordFragment forgotPasswordFragment = new ForgotPasswordFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.fragmentType, ConstantUtils.registerationActivity);
        forgotPasswordFragment.setArguments(bundle);
        regActivity.replaceFragmnet(forgotPasswordFragment, R.id.frameLayout, true);
    }

    public void onCallPreLogin() {
        MasterInputResponse masterInputResponse = new MasterInputResponse();
        masterInputResponse.setImsi(mobileNo.getText().toString().trim());
        masterInputResponse.setMsisdn(mobileNo.getText().toString().trim());
        masterInputResponse.setOsVersion(osVersion);
        masterInputResponse.setAppVersion(appVersion);
        masterInputResponse.setChannel(channel);
        masterInputResponse.setToken(token);
        masterInputResponse.setLang(currentLanguage);
        masterInputResponse.setDeviceId(deviceId);

        new GetPreLoginInfoService(this, masterInputResponse);
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        regActivity.addFragmnet(new LoadingFragmnet(), R.id.loadingFragmeLayout, true);


        loginInputModel = new LoginInputModel();
        loginInputModel.setAppVersion(appVersion);
//        sharedPrefrencesManger.setToken("testingDummy");
//        token = sharedPrefrencesManger.getToken();
        loginInputModel.setToken(token);
        loginInputModel.setLang(currentLanguage);
        loginInputModel.setOsVersion(osVersion);
        loginInputModel.setChannel(channel);
        loginInputModel.setDeviceId(deviceId);
        String md5Pass = ValidationHelper.getMd5(password.getText().toString().trim());
        md5Pass = md5Pass.toUpperCase();
//

        md5Pass = ValidationHelper.getMd5(md5Pass + "Y0u1h53y.P1n9").toUpperCase();
        String finalValToSend = getPreLoginOutput.getIdentifier() + md5Pass + generator.nextSessionId();
//
//
        loginInputModel.setPassword(finalValToSend);
//        loginInputModel.setPassword(md5Pass);
        loginInputModel.setMsisdn(mobileNo.getText().toString().trim());
        new LoginService(this, loginInputModel);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            if (isVisible()) {
                if(responseModel!=null && responseModel.getResultObj()!=null) {
                    if (responseModel.getServiceType().equals(ServiceUtils.loginService)) {
                        regActivity.onBackPressed();
                        mBaseResponseModel = responseModel;
                        loginOutputModel = (LoginOutputModel) responseModel.getResultObj();
                        if (loginOutputModel != null) {
                            if (loginOutputModel.getErrorCode() != null)
                                onLoginError();
                            else
                                onLoginSucess();
                        }

                    } else {
                        if (responseModel.getServiceType().equals(ServiceUtils.isPreLogin)) {

                            mBaseResponseModel = responseModel;
                            getPreLoginOutput = (GetPreLoginOutput) responseModel.getResultObj();
                            if (getPreLoginOutput != null && getPreLoginOutput.getIdentifier() != null
                                    && getPreLoginOutput.getIdentifier().length() > 0) {
                                onConsumeService();
                            } else {
                                showErrorFragment(getString(R.string.generice_error));
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    private void onLoginSucess() {

//        sharedPrefrencesManger.setPassword(password.getText().toString().trim());
        if (loginOutputModel.getUserProfile() != null) {
            if (service == null) {
                startMyService();
            }
            if (sharedPrefrencesManger == null) {
                sharedPrefrencesManger = new SharedPrefrencesManger(getActivity());
            }
            if (sharedPrefrencesManger != null) {
                sharedPrefrencesManger.setRegisterationID(loginOutputModel.getUserProfile().getRegistrationId());
                sharedPrefrencesManger.setEmail(loginOutputModel.getUserProfile().getEmail());
                sharedPrefrencesManger.setNickName(loginOutputModel.getUserProfile().getNickName());
                sharedPrefrencesManger.setGender(loginOutputModel.getUserProfile().getGender());
                sharedPrefrencesManger.setUserProfileImage(loginOutputModel.getUserProfile().getProfilePicUrl());
                sharedPrefrencesManger.setFullName(loginOutputModel.getUserProfile().getFullName());
                sharedPrefrencesManger.setMobileNo(loginOutputModel.getUserProfile().getMsisdn());
                sharedPrefrencesManger.setInvitation(loginOutputModel.getUserProfile().getInvitationCode());
                sharedPrefrencesManger.setAuthToken(mBaseResponseModel.getAuthToken());
                sharedPrefrencesManger.setUserProfileObj(loginOutputModel.getUserProfile());
                setAppFlagsValue();
                sharedPrefrencesManger.setLastUpdated("");
            }
            CommonMethods.logFirebaseEvent(mFirebaseAnalytics,ConstantUtils.TAGGING_login_success);
            Intent intent = new Intent(getActivity(), SwpeMainActivity.class);
            intent.putExtra(ConstantUtils.userProfile, loginOutputModel.getUserProfile());
            startActivity(intent);
            if (TutorialActivity.tutorialActivityInstance != null)
                TutorialActivity.tutorialActivityInstance.finish();

            regActivity.finish();
        }
    }

    private void setAppFlagsValue() {
        if (sharedPrefrencesManger != null) {
            if (loginOutputModel != null) {
                if (loginOutputModel.getAppFlags() != null) {
                    sharedPrefrencesManger.enableBalanceTransfer(loginOutputModel.getAppFlags().getUbtEnabled());
                    sharedPrefrencesManger.enableRewards(loginOutputModel.getAppFlags().getMarketplaceEnabled());
                    sharedPrefrencesManger.enableStore(loginOutputModel.getAppFlags().getStoreEnabled());
                    sharedPrefrencesManger.enableLiveChat(loginOutputModel.getAppFlags().getLiveChatEnabled());
                    sharedPrefrencesManger.enableLUsage(loginOutputModel.getAppFlags().getUsageEnabled());
                }
            }
        }
    }

    private void onLoginError() {
        CommonMethods.logFirebaseEvent(mFirebaseAnalytics,ConstantUtils.TAGGING_login_failure);
        if (currentLanguage != null && loginOutputModel != null && loginOutputModel.getErrorMsgEn() != null) {
            if (currentLanguage.equals("en")) {
                showErrorFragment(loginOutputModel.getErrorMsgEn());
            } else {
                showErrorFragment(loginOutputModel.getErrorMsgAr());
            }
        } else {
            showErrorFragment(getString(R.string.generice_error));
        }

    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        try {
            if (isVisible())
                regActivity.onBackPressed();
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }


    private boolean isValid() {
        boolean isValid = true;
        if (mobileNo.getText().toString().length() < 10) {
            isValid = false;
            til_mobileNumber.setError(getString(R.string.invalid_delivery_number));

        } else {
            String noStr = mobileNo.getText().toString().substring(0, 2);
            if (!noStr.equalsIgnoreCase("05")) {
                isValid = false;
                til_mobileNumber.setError(getString(R.string.invalid_delivery_number));
            } else {
                removeTextInputLayoutError(til_mobileNumber);
            }
        }
        if (password.getText().toString().trim().length() == 0) {
            setTextInputLayoutError(til_password, getString(R.string.please_enter_password));
            isValid = false;

        }
        return isValid;
    }

    private void showErrorFragment(String error) {
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, error);
        errorFragment.setArguments(bundle);
        regActivity.replaceFragmnet(errorFragment, R.id.frameLayout, true);
    }

    private void disableNextBtn() {
        loginBtn.setAlpha(0.5f);
        loginBtn.setEnabled(false);
    }

    private void enableNextBtn() {
        loginBtn.setAlpha(1.0f);
        loginBtn.setEnabled(true);
    }

    public static void stopServiceNow() {
        registerationActivity.stopService(service);
    }

    public final class SessionIdentifierGenerator {
        private SecureRandom random = new SecureRandom();

        public String nextSessionId() {
            return new BigInteger(130, random).toString(8);
        }
    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }
}
