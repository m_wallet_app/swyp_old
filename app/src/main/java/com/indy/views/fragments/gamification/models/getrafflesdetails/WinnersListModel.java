package com.indy.views.fragments.gamification.models.getrafflesdetails;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mobile on 12/10/2017.
 */

public class WinnersListModel implements Parcelable {

    @SerializedName("userId")
    @Expose
    private Object userId;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("userGroupId")
    @Expose
    private Object userGroupId;
    @SerializedName("score")
    @Expose
    private Object score;
    @SerializedName("ranking")
    @Expose
    private Integer ranking;
    @SerializedName("gameTypeId")
    @Expose
    private Object gameTypeId;
    @SerializedName("userPicUrl")
    @Expose
    private Object userPicUrl;
    @SerializedName("nationality")
    @Expose
    private Object nationality;
    @SerializedName("profilePicUrl")
    @Expose
    private Object profilePicUrl;
    @SerializedName("status")
    @Expose
    private Boolean status;
    public final static Parcelable.Creator<WinnersListModel> CREATOR = new Creator<WinnersListModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public WinnersListModel createFromParcel(Parcel in) {
            return new WinnersListModel(in);
        }

        public WinnersListModel[] newArray(int size) {
            return (new WinnersListModel[size]);
        }

    };

    protected WinnersListModel(Parcel in) {
        this.userId = ((Object) in.readValue((Object.class.getClassLoader())));
        this.username = ((String) in.readValue((String.class.getClassLoader())));
        this.userGroupId = ((Object) in.readValue((Object.class.getClassLoader())));
        this.score = ((Object) in.readValue((Object.class.getClassLoader())));
        this.ranking = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.gameTypeId = ((Object) in.readValue((Object.class.getClassLoader())));
        this.userPicUrl = ((Object) in.readValue((Object.class.getClassLoader())));
        this.nationality = ((Object) in.readValue((Object.class.getClassLoader())));
        this.profilePicUrl = ((Object) in.readValue((Object.class.getClassLoader())));
        this.status = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
    }

    public WinnersListModel() {
    }

    public Object getUserId() {
        return userId;
    }

    public void setUserId(Object userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Object getUserGroupId() {
        return userGroupId;
    }

    public void setUserGroupId(Object userGroupId) {
        this.userGroupId = userGroupId;
    }

    public Object getScore() {
        return score;
    }

    public void setScore(Object score) {
        this.score = score;
    }

    public Integer getRanking() {
        return ranking;
    }

    public void setRanking(Integer ranking) {
        this.ranking = ranking;
    }

    public Object getGameTypeId() {
        return gameTypeId;
    }

    public void setGameTypeId(Object gameTypeId) {
        this.gameTypeId = gameTypeId;
    }

    public Object getUserPicUrl() {
        return userPicUrl;
    }

    public void setUserPicUrl(Object userPicUrl) {
        this.userPicUrl = userPicUrl;
    }

    public Object getNationality() {
        return nationality;
    }

    public void setNationality(Object nationality) {
        this.nationality = nationality;
    }

    public Object getProfilePicUrl() {
        return profilePicUrl;
    }

    public void setProfilePicUrl(Object profilePicUrl) {
        this.profilePicUrl = profilePicUrl;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(userId);
        dest.writeValue(username);
        dest.writeValue(userGroupId);
        dest.writeValue(score);
        dest.writeValue(ranking);
        dest.writeValue(gameTypeId);
        dest.writeValue(userPicUrl);
        dest.writeValue(nationality);
        dest.writeValue(profilePicUrl);
        dest.writeValue(status);
    }

    public int describeContents() {
        return 0;
    }
}