package com.indy.views.fragments.gamification.models.getnotificationslist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.views.fragments.gamification.models.getbadgeslist.BadgeCategory;

import java.util.List;

/**
 * Created by Tohamy on 10/4/2017.
 */

public class GetNotificationsListOutput extends MasterErrorResponse {

    @SerializedName("notificationsList")
    @Expose
    private List<NotificationModel> notificationsList = null;

    public List<NotificationModel> getNotificationsList() {
        return notificationsList;
    }

    public void setNotificationsList(List<NotificationModel> notificationsList) {
        this.notificationsList = notificationsList;
    }

}

