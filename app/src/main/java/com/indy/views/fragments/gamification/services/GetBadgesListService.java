package com.indy.views.fragments.gamification.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.BaseServiceManger;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.gamification.models.getbadgeslist.GetBadgesListInputModel;
import com.indy.views.fragments.gamification.models.getbadgeslist.GetBadgesListOutputModel;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Tohamy on 10/1/2017.
 */

public class GetBadgesListService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private GetBadgesListInputModel getBadgesListInputModel;


    public GetBadgesListService(BaseInterface baseInterface, GetBadgesListInputModel getBadgesListInputModel) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.getBadgesListInputModel = getBadgesListInputModel;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<GetBadgesListOutputModel> call = apiService.getBadgesList(getBadgesListInputModel);
        call.enqueue(new Callback<GetBadgesListOutputModel>() {
            @Override
            public void onResponse(Call<GetBadgesListOutputModel> call, Response<GetBadgesListOutputModel> response) {
                mBaseResponseModel.setServiceType(ServiceUtils.GET_BADGES_LIST);
                mBaseResponseModel.setResultObj(response.body());

                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (response.body()!=null && response.body().getErrorMsgEn() != null) {
//                        String urlForTag = call.request().url().toString();
//                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
//                        urlForTag = urlForTag.replace("/", "_");
//                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<GetBadgesListOutputModel> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.GET_BADGES_LIST);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}

