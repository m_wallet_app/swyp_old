package com.indy.views.fragments.esim;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.indy.R;
import com.indy.controls.BaseInterface;
import com.indy.models.esimActivation.ActivateESIMResponseModel;
import com.indy.models.esimActivation.CheckESIMEligibilityRequestModel;
import com.indy.models.inilizeregisteration.InilizeRegisterationInput;
import com.indy.models.inilizeregisteration.InilizeRegisteratonOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.ActivateESIMService;
import com.indy.services.LogApplicationFlowService;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.SwpeMainActivity;
import com.indy.views.fragments.buynewline.NewNumberFragment;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.indy.views.fragments.utils.MasterFragment;

import static com.indy.views.activites.RegisterationActivity.logEventInputModel;

public class ESIMActivationNoticeFragment extends MasterFragment implements BaseInterface {

    private View view;
    private Button acceptBtn, cancelbtn;
    private InilizeRegisterationInput inilizeRegisterationInput;
    InilizeRegisteratonOutput inilizeRegisteratonOutput;
    BaseResponseModel mResponseModel;
    private SwpeMainActivity mSwypeMainActivity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_esim_activation_notice, container, false);
        initUI();
        return view;
    }

    @Override
    public void initUI() {
        mSwypeMainActivity = (SwpeMainActivity) getActivity();
        super.initUI();
        acceptBtn = (Button) view.findViewById(R.id.confirmBtn);
        cancelbtn = (Button) view.findViewById(R.id.cancelBtn);
        if (regActivity != null) {
            regActivity.hideHeaderLayout();
        }
        acceptBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


//                mSwypeMainActivity.replaceFragmnet(new ActivateESIMQRCodeFragment(), R.id.contentFrameLayout, false);

                mSwypeMainActivity.addFragmnet(new LoadingFragmnet(), R.id.contentFrameLayout, true);

                CheckESIMEligibilityRequestModel checkESIMEligibilityRequestModel = new CheckESIMEligibilityRequestModel();
                checkESIMEligibilityRequestModel.setAppVersion(appVersion);
                checkESIMEligibilityRequestModel.setToken(token);
                checkESIMEligibilityRequestModel.setOsVersion(osVersion);
                checkESIMEligibilityRequestModel.setLang(currentLanguage);
                checkESIMEligibilityRequestModel.setChannel(channel);
                checkESIMEligibilityRequestModel.setDeviceId(deviceId);
                checkESIMEligibilityRequestModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
                checkESIMEligibilityRequestModel.setAuthToken(authToken);
                checkESIMEligibilityRequestModel.setAdditionalInfo(ConstantUtils.FIREBASE_TOKEN_KEY, sharedPrefrencesManger.getToken());
                new ActivateESIMService(ESIMActivationNoticeFragment.this, checkESIMEligibilityRequestModel);
            }
        });
        cancelbtn.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                mSwypeMainActivity.onBackPressed();
            }
        });
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            mSwypeMainActivity.getSupportFragmentManager().popBackStack();
            if (responseModel != null && responseModel.getResultObj() != null) {
                ActivateESIMResponseModel activateESIMResponseModel = (ActivateESIMResponseModel) responseModel.getResultObj();
                if (activateESIMResponseModel != null && activateESIMResponseModel.getResponseMsg() != null && activateESIMResponseModel.getResponseMsg().equalsIgnoreCase("success")) {
                    if (activateESIMResponseModel.getActivationCode() != null &&
                            activateESIMResponseModel.getActivationCode().length() > 0 &&
                            activateESIMResponseModel.getSimDPAddress() != null &&
                            activateESIMResponseModel.getSimDPAddress().length() > 0) {
                        Bundle bundle = new Bundle();
                        bundle.putString("activationCode", activateESIMResponseModel.getActivationCode());
                        bundle.putString("simDPAddress", activateESIMResponseModel.getSimDPAddress());
                        ActivateESIMQRCodeFragment activateESIMQRCodeFragment = new ActivateESIMQRCodeFragment();
                        activateESIMQRCodeFragment.setArguments(bundle);
                        mSwypeMainActivity.replaceFragmnet(activateESIMQRCodeFragment, R.id.contentFrameLayout, false);
                    } else {
                        onTermsAndConditionsFail(activateESIMResponseModel);
                    }
                } else {
                    onTermsAndConditionsFail(activateESIMResponseModel);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onTermsAndConditionsFail(ActivateESIMResponseModel
                                                  verifyEmiratesIdOutputString) {
        if (verifyEmiratesIdOutputString != null) {
            if (verifyEmiratesIdOutputString.getErrorMsgEn() != null) {
                if (currentLanguage.equalsIgnoreCase("en"))
                    showErrorFragment(verifyEmiratesIdOutputString.getErrorMsgEn(), getString(R.string.title));
                else
                    showErrorFragment(verifyEmiratesIdOutputString.getErrorMsgAr(), getString(R.string.title));
            } else {
                showErrorFragment(getString(R.string.generice_error), getString(R.string.title));
            }
        } else {
            showErrorFragment(getString(R.string.generice_error), getString(R.string.title));
        }
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
//        super.onErrorListener(responseModel);
        try {
            if (responseModel != null && responseModel.getServiceType() != null) {

            } else {

                showErrorFragment(getString(R.string.error_), getString(R.string.title));
            }
        } catch (Exception ex) {

        }
    }


    private void showErrorFragment(String error, String title) {
        try {
            Bundle bundle = new Bundle();
            bundle.putString(ConstantUtils.errorString, error);
            bundle.putString(ConstantUtils.errorTitle, title);
            ErrorFragment errorFragment = new ErrorFragment();
            errorFragment.setArguments(bundle);
            mSwypeMainActivity.replaceFragmnet(errorFragment, R.id.contentFrameLayout, true);
        } catch (Exception ex) {
//            CommonMethods.logException(ex);
        }
    }

    private void logEvent(int index) {


        logEventInputModel.setActionTransactionId(sharedPrefrencesManger.getTempAppId());
        logEventInputModel.setScreenId(ConstantUtils.SCREEN_ID_0005);

        if (index == 1) {
            logEventInputModel.setConfirmOrCancel(acceptBtn.getText().toString());
        } else {
            logEventInputModel.setConfirmOrCancel(cancelbtn.getText().toString());

        }
        new LogApplicationFlowService(this, logEventInputModel);

    }

}

