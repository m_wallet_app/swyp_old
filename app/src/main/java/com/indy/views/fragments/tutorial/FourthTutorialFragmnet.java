package com.indy.views.fragments.tutorial;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.adapters.StaticPackagesListAdapter;
import com.indy.controls.AdjustEvents;
import com.indy.controls.ServiceUtils;
import com.indy.models.getStaticBundles.BundleList;
import com.indy.models.getStaticBundles.StaticBundleOutput;
import com.indy.models.packages.UsagePackageList;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterInputResponse;
import com.indy.services.GetStaticPackageListService;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.RegisterationActivity;
import com.indy.views.activites.TutorialActivity;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.MasterFragment;

import java.util.List;

/**
 * Created by emad on 8/28/16.
 */
public class FourthTutorialFragmnet extends MasterFragment {
    private View view;
    private Button continueBtn;
    private LinearLayout viewPackagesBtn;
    public static BottomSheetBehavior behavior;


    //------------PACKAGES BOTTOMSHEET-----//////////
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mGridLayoutManger;
    private List<BundleList> usagePackageList;
    private MasterInputResponse masterInputResponse;
    private StaticBundleOutput usagePackagesoutput;
    private StaticPackagesListAdapter packagesListAdapter;
    private UsagePackageList emptyUsagePackageListlItem;
    private Button signupBtn;
    TutorialActivity tutorialActivity;
    private TextView tv_swyp_description;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_fourth_tutorial, container, false);
//        initUI();
//        initStaticPackagesUI();

        ((TutorialActivity) getActivity()).skipBtn.setVisibility(View.VISIBLE);

        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
        continueBtn = (Button) view.findViewById(R.id.loginBtn);
        tutorialActivity = (TutorialActivity) getActivity();
        tv_swyp_description = (TextView) view.findViewById(R.id.tv_swyp_description);
        if (sharedPrefrencesManger.isVATEnabled()) {
            Double price = Double.parseDouble(sharedPrefrencesManger.getAfterLoginPrice());
            tv_swyp_description.setText(String.format(getString(R.string.is_the_mobile_service_for_new_generation), String.valueOf(price + (price * 0.05))));

        } else {
            tv_swyp_description.setText(String.format(getString(R.string.is_the_mobile_service_for_new_generation), sharedPrefrencesManger.getAfterLoginPrice()));
        }
        viewPackagesBtn = (LinearLayout) view.findViewById(R.id.viewPackagesId);
        viewPackagesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tutorialActivity.shouldContinue = false;
                ((TutorialActivity) getActivity()).showBottomSheetSetup();
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                onConsumeService();
            }
        });

        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logEvent(AdjustEvents.CLICKS_SIGN_UP_OR_SKIP,new Bundle());
                sharedPrefrencesManger.setIsFiristTime(false);
                ((TutorialActivity) getActivity()).onSkip();
            }
        });

        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) view.findViewById(R.id.coordinateLayoutID);
        View bottomSheet = coordinatorLayout.findViewById(R.id.bottom_sheet_static_packages_view);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    ((TutorialActivity) getActivity()).hideBottomSheetSetup();
                } else if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                    ((TutorialActivity) getActivity()).showBottomSheetSetup();
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });

    }


    private void initStaticPackagesUI() {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mGridLayoutManger = new LinearLayoutManager(getActivity());
        mGridLayoutManger.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mGridLayoutManger);
        mRecyclerView.setHasFixedSize(true);
        signupBtn = (Button) view.findViewById(R.id.loginBtn_static);

        signupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tutorialActivity.shouldContinue = false;
                Intent intent = new Intent(getActivity(), RegisterationActivity.class);
                startActivity(intent);
//                getActivity().finish();
            }
        });
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
//        getActivity().onBackPressed();
        try {
            if(responseModel!=null && responseModel.getResultObj()!=null) {
                if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.packageServiceType)) {
                    getPackagesList(responseModel);
                }
            }
        }catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    private void getPackagesList(BaseResponseModel responseModel) {
        usagePackagesoutput = (StaticBundleOutput) responseModel.getResultObj();
        if (usagePackagesoutput != null) {
            if (usagePackagesoutput.getErrorCode() != null) {
                onGetPackagesError();
            } else
                onGetPackageSucess();
        }
    }


    private void onGetPackageSucess() {
        usagePackageList = usagePackagesoutput.getBundleList();
        if (usagePackageList != null)
            if (usagePackageList.size() > 0) {
                packagesListAdapter = new StaticPackagesListAdapter(usagePackageList, getContext());
                mRecyclerView.setAdapter(packagesListAdapter);
            }
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
//        getActivity().onBackPressed();
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
//        ((TutorialActivity) getActivity()).addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        masterInputResponse = new MasterInputResponse();
        masterInputResponse.setAppVersion(appVersion);
        masterInputResponse.setToken(token);
        masterInputResponse.setOsVersion(osVersion);
        masterInputResponse.setChannel(channel);
        masterInputResponse.setDeviceId(deviceId);
        masterInputResponse.setMsisdn("0505555555");
        new GetStaticPackageListService(this, masterInputResponse);

    }

    private void onGetPackagesError() {
        showErrorFragment(usagePackagesoutput.getErrorMsgEn());
    }

    public void showErrorFragment(String error) {
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, error);
        errorFragment.setArguments(bundle);
        ((TutorialActivity) getActivity()).replaceFragmnet(errorFragment, R.id.frameLayout, true);
    }
}
