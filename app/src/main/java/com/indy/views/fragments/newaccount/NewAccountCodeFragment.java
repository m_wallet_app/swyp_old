package com.indy.views.fragments.newaccount;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.customviews.CustomButton;
import com.indy.customviews.CustomEditText;
import com.indy.customviews.CustomTextView;
import com.indy.models.createprofilestatus.CreateProfileStatusInput;
import com.indy.models.createprofilestatus.CreateProfileStatusoutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.validateotp.ValidateOtpInput;
import com.indy.models.validateotp.ValidateOtpOutput;
import com.indy.services.CreateProfileService;
import com.indy.services.ValidateOtpService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.HelpActivity;
import com.indy.views.fragments.buynewline.TermsAndConditionsFragment;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.indy.views.fragments.utils.MasterFragment;

import java.util.ArrayList;

/**
 * Created by emad on 9/7/16.
 */
public class NewAccountCodeFragment extends MasterFragment {
    private View view;
    private CustomTextView resendBtn;
    private CustomButton continueButton;
    public static CustomEditText code1, code2, code3, code4;
    private TextView help;
    private String codeGenerated;
    private String vCtag = "VCTAG";
    private CountDownTimer countDownTimer;
    private Button backImg;
    int[] tvListIds = {R.id.code_1, R.id.code_2, R.id.code_3, R.id.code_4};
    ArrayList<EditText> tv_digits;
    private String mobileNo;
    //sms handler
    static NewAccountCodeFragment newAccountCodeFragment;
    //  ProgressBar progressBar;
    public static Context context;
    public static Bundle bundle;
    static String codeToEnter;
    Animation shake;
    static ProgressDialog progressDialog;
    CreateProfileStatusoutput createProfileStatusoutput;
    ValidateOtpInput validateOtpInput;
    ValidateOtpOutput validateOtpOutput;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_forgot_password_code, container, false);
        initUI();

        setListeners();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        newAccountCodeFragment = NewAccountCodeFragment.this;
        context = getActivity();
        shake = AnimationUtils.loadAnimation(getActivity(), R.anim.fab_open);
        bundle = getActivity().getIntent().getExtras();
        if (bundle != null) {
            mobileNo = getArguments().getString(ConstantUtils.newAccountMobileNo);
        }
        // progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        //   progressBar.setMax(100);
        //  progressBar.setProgress(30);
        disableResebdCodeBtn();
        disableContinueBtn();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
        code1 = (CustomEditText) view.findViewById(R.id.code_1);
        code2 = (CustomEditText) view.findViewById(R.id.code_2);
        code3 = (CustomEditText) view.findViewById(R.id.code_3);
        code4 = (CustomEditText) view.findViewById(R.id.code_4);

        resendBtn = (CustomTextView) view.findViewById(R.id.resendBtn);
        help = (TextView) view.findViewById(R.id.help);
        continueButton = (CustomButton) view.findViewById(R.id.continueBtn);
        showKeyBoard();
        if (getArguments() != null && !getArguments().getString(ConstantUtils.codeNO, "").isEmpty()) {
            codeGenerated = getArguments().getString(ConstantUtils.codeNO);
            Log.v("oooops", codeGenerated + " ");
        }
        startTimer();
        tv_digits = new ArrayList<EditText>();
        for (int i = 0; i < tvListIds.length; i++) {
            EditText et_temp = (EditText) view.findViewById(tvListIds[i]);
            et_temp.addTextChangedListener(new CustomTextWatcher(et_temp));
            tv_digits.add(et_temp);
        }

//        backImg = (Button) view.findViewById(R.id.backImg);
        //..............header Buttons............
        RelativeLayout backLayout = (RelativeLayout) view.findViewById(R.id.backLayout);
        RelativeLayout helpLayout = (RelativeLayout) view.findViewById(R.id.helpLayout);

        helpLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onHelp();
            }
        });

        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard();
                getActivity().onBackPressed();
            }
        });

    }

    private void startTimer() {
        countDownTimer = new CountDownTimer(61000, 1000) {

            public void onTick(long millisUntilFinished) {
                if (isVisible()) {
                    int minutes = (int) millisUntilFinished / (60 * 1000);
                    int seconds = (int) (millisUntilFinished / 1000) % 60;
                    String str = String.format("%d:%02d", minutes, seconds);
                    resendBtn.setText(getResources().getString(R.string.recent_code) + " " + str);
                } else {
                    cancel();
                }
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                if (isVisible()) {
                    enableContinueBtn();
                    enableResendCodeBtn();
                    resendBtn.setText(getString(R.string.resend_code_txt));
                    resendBtn.setEnabled(true);
                    cancel();
                }
            }

        }.start();
        resendBtn.setEnabled(false);
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();

    }

    private void setListeners() {
        resendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //// TODO: 10/3/2016 : resend service to be called here
                onConsumeService();
            }
        });
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onContinue();

                CommonMethods.logFirebaseEvent(mFirebaseAnalytics,ConstantUtils.TAGGING_create_account_validation_code_entered);
            }
        });
    }

    private void onContinue() {
        hideKeyBoard();
        if (countDownTimer != null)
            countDownTimer.onFinish();
        countDownTimer = null;

        regActivity.addFragmnet(new LoadingFragmnet(), R.id.loadingFragmeLayout, true);
        validateOtpInput = new ValidateOtpInput();
        validateOtpInput.setAuthToken(null);
        validateOtpInput.setAppVersion(appVersion);
        validateOtpInput.setChannel(channel);
        validateOtpInput.setDeviceId(deviceId);
        validateOtpInput.setImsi(sharedPrefrencesManger.getMobileNo());
        validateOtpInput.setLang(currentLanguage);
        validateOtpInput.setOtpCode(getCode());
        validateOtpInput.setOsVersion(osVersion);
        validateOtpInput.setToken(token);
        validateOtpInput.setMsisdn(getArguments().getString(ConstantUtils.newAccountMobileNo));
        new ValidateOtpService(this, validateOtpInput);

    }

    private void onValidOtp() {

        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_create_account_enter_valid_number);


        Fragment termsAndCondtions = new TermsAndConditionsFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(ConstantUtils.isFromCreateNewAccount, true);
        bundle.putString(ConstantUtils.newAccountMobileNo, getArguments().getString(ConstantUtils.newAccountMobileNo));
        termsAndCondtions.setArguments(bundle);
        regActivity.replaceFragmnet(termsAndCondtions, R.id.frameLayout, true);
    }

    private void onInValidOtp() {

        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_create_account_validation_code_invalid);

        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, getString(R.string.invalid_code));
        ErrorFragment errorFragment = new ErrorFragment();
        errorFragment.setArguments(bundle);
        regActivity.replaceFragmnet(errorFragment, R.id.frameLayout, true);

    }

    private void onHelp() {
        Intent intent = new Intent(getActivity(), HelpActivity.class);
        startActivity(intent);
    }

    public void showKeyBoard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, 0);
        }
    }


    public class CustomTextWatcher implements TextWatcher {
        private EditText view;

        private CustomTextWatcher(EditText view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

            String text = editable.toString();
            if (getCode().length() == 4)
                enableContinueBtn();
            else
                disableContinueBtn();
            if (text.length() == 1) {
                view.clearFocus();
                int currentIndex = tv_digits.indexOf(view);
                if (currentIndex == 3) {
                    hideKeyBoard();
                } else {
                    for (int i = currentIndex + 1; i < tv_digits.size(); i++) {
                        tv_digits.get(i).requestFocus();
                        return;

                    }
                }

            }

        }
    }

    private String getCode() {
        String codeEntered = "";

        codeEntered += code1.getText().toString();
        codeEntered += code2.getText().toString();
        codeEntered += code3.getText().toString();
        codeEntered += code4.getText().toString();
        return codeEntered;
    }


    @Override
    public void onConsumeService() {
        super.onConsumeService();
        regActivity.replaceFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        CreateProfileStatusInput createProfileStatusInput = new CreateProfileStatusInput();
        createProfileStatusInput.setAppVersion(appVersion);
        createProfileStatusInput.setToken(token);
        createProfileStatusInput.setOsVersion(osVersion);
        createProfileStatusInput.setChannel(channel);
        createProfileStatusInput.setDeviceId(deviceId);
        createProfileStatusInput.setMsisdn(mobileNo);
        createProfileStatusInput.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        new CreateProfileService(NewAccountCodeFragment.this, createProfileStatusInput);

    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            regActivity.onBackPressed();

            if (isVisible()) {
                if(responseModel!=null && responseModel.getResultObj()!=null) {
                    if (responseModel.getServiceType().equalsIgnoreCase(ConstantUtils.validateOtp)) {
                        this.validateOtpOutput = (ValidateOtpOutput) responseModel.getResultObj();
                        if (validateOtpOutput.getErrorCode() == null && validateOtpOutput.getIsValid() != null && validateOtpOutput.getIsValid())
                            onValidOtp();
                        else
                            onInValidOtp();
                    } else {
                        createProfileStatusoutput = (CreateProfileStatusoutput) responseModel.getResultObj();
                        if (createProfileStatusoutput != null)
                            onCreateProfileSucess();

                    }
                }
            }
        }catch (Exception ex) {
            CommonMethods.logException(ex);
        }

    }

    void onCreateProfileSucess() {
        if (createProfileStatusoutput.getErrorCode() != null)
            showErrorFragment(createProfileStatusoutput.getErrorMsgEn());
        else {
            startTimer();
        }

    }

    private void disableContinueBtn() {
        continueButton.setAlpha(0.5f);
        continueButton.setEnabled(false);
        continueButton.setOnClickListener(null);

    }

    private void disableResebdCodeBtn() {
        resendBtn.setAlpha(0.20f);
        resendBtn.setEnabled(false);

    }

    private void enableResendCodeBtn() {
        resendBtn.setAlpha(1.0f);
        resendBtn.setEnabled(true);

    }

    private void enableContinueBtn() {
        continueButton.setAlpha(1.0f);
        continueButton.setEnabled(true);
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onContinue();
            }
        });
    }

    private void showErrorFragment(String txt) {
        ErrorFragment loadingFragmnet = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, /*createProfileStatusoutput.getErrorMsgEn()*/txt);
        loadingFragmnet.setArguments(bundle);
        regActivity.addFragmnet(loadingFragmnet, R.id.frameLayout, true);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        try {
            regActivity.onBackPressed();
        }catch (Exception ex) {
            CommonMethods.logException(ex);

        }
    }
}
