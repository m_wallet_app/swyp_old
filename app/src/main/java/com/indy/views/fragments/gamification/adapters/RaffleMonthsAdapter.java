package com.indy.views.fragments.gamification.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.utils.SharedPrefrencesManger;
import com.indy.views.fragments.gamification.OnRaffleListClick;
import com.indy.views.fragments.gamification.challenges.EarnRaffleTicketsActivity;
import com.indy.views.fragments.gamification.models.getraffleslist.Raffle;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import static com.indy.views.fragments.gamification.challenges.ChallengesMainFragment.challengesMainFragment;

public class RaffleMonthsAdapter extends RecyclerView.Adapter<RaffleMonthsAdapter.ViewHolder> {

    private Context mContext;
    private SharedPrefrencesManger sharedPrefrencesManger;
    private List<Raffle> raffles;
    private OnRaffleListClick onRaffleListClick;

    public RaffleMonthsAdapter(List<Raffle> raffles, Context context, OnRaffleListClick onRaffleListClick) {

        this.mContext = context;
        if(context!=null)
        this.sharedPrefrencesManger = new SharedPrefrencesManger(context);
        this.raffles = raffles;
        this.onRaffleListClick = onRaffleListClick;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.item_raffle_month, viewGroup, false);
        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Raffle raffle = raffles.get(position);

        if (holder.getAdapterPosition() == 0) {
            holder.ll_header.setVisibility(View.VISIBLE);
        } else {
            holder.ll_header.setVisibility(View.GONE);
        }
        if (holder.getAdapterPosition() == (raffles.size() - 1)) {
            holder.ll_footer.setVisibility(View.VISIBLE);
        } else {
            holder.ll_footer.setVisibility(View.GONE);
        }


        if (sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)) {
            if (raffle.getTitleEn() != null && !raffle.getTitleEn().isEmpty())
                holder.tv_title.setText(raffle.getTitleEn());
            if (raffle.getDescription() != null && !raffle.getDescription().isEmpty()) {
                if (raffle.getActive()) {
                    holder.tv_description.setText(raffle.getDescription());
                } else {
                    holder.tv_description2.setText(raffle.getDescription());
                }
            }
        } else {
            if (raffle.getTitleAr() != null && !raffle.getTitleAr().isEmpty())
                holder.tv_title.setText(raffle.getTitleAr());
            if (raffle.getDescription() != null && !raffle.getDescription().isEmpty()) {
                if (raffle.getActive()) {
                    holder.tv_description.setText(raffle.getDescription());
                } else {
                    holder.tv_description2.setText(raffle.getDescription());
                }
            }
        }
        if (raffle.getTitle() != null) {
            holder.tv_title.setText(raffle.getTitle());
        }
        if (raffle.getActive()) {
            holder.ll_previousMonths.setVisibility(View.GONE);
            holder.ll_thisMonth.setVisibility(View.VISIBLE);
            if (raffle.getImageUrl() != null && !raffle.getImageUrl().isEmpty())
                ImageLoader.getInstance().displayImage(raffle.getImageUrl(), holder.iv_raffle);
            if(CommonMethods.isMembershipValid(mContext)) {
                holder.tv_winnersText.setText(mContext.getString(R.string.your_raffle_tickets) + ": " + raffle.getUserRaffleTickets());
                holder.tv_winnersText.setVisibility(View.VISIBLE);
                holder.tv_noMembership.setVisibility(View.GONE);

            }else{
                holder.tv_noMembership.setVisibility(View.VISIBLE);
                holder.tv_winnersText.setVisibility(View.GONE);
            }
                if (raffle.getEndsOnDate() != null && !raffle.getEndsOnDate().isEmpty())
                holder.tv_expiry.setText(String.format(mContext.getString(R.string.ends_in), String.valueOf(CommonMethods.getDaysDifference(raffle.getEndsOnDate()))));
        } else {
            if (raffle.getImageUrl() != null && !raffle.getImageUrl().isEmpty())
                ImageLoader.getInstance().displayImage(raffle.getImageUrl(), holder.iv_raffleOld);

            holder.ll_previousMonths.setVisibility(View.VISIBLE);
            holder.ll_thisMonth.setVisibility(View.GONE);
            if (raffle.getEndsOnDate() != null && !raffle.getEndsOnDate().isEmpty())
                holder.tv_expiry2.setText(String.format(mContext.getString(R.string.ended), CommonMethods.parseDate(raffle.getEndsOnDate(), sharedPrefrencesManger.getLanguage())));
            holder.tv_previousTitle.setText(raffle.getTitle());
        }
        holder.tv_listOfWinners.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onRaffleListClick != null) {
                    onRaffleListClick.onRaffleClicked(raffle);
                }
            }
        });
        holder.btn_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, EarnRaffleTicketsActivity.class);
                challengesMainFragment.startActivityForResult(intent, ConstantUtils.RAFFLES_NAVIGATION);
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return raffles.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView iv_raffle, iv_raffleOld;
        TextView tv_title, tv_description, tv_expiry, tv_expiry2, tv_winnersText, tv_listOfWinners, btn_profile, tv_previousTitle, tv_description2, tv_noMembership;
        LinearLayout ll_thisMonth, ll_previousMonths, ll_header, ll_footer;

        public ViewHolder(View v) {
            super(v);
            iv_raffle = (ImageView) v.findViewById(R.id.iv_raffle);
            iv_raffleOld = (ImageView) v.findViewById(R.id.iv_raffleOld);
            tv_title = (TextView) v.findViewById(R.id.tv_title);
            tv_description = (TextView) v.findViewById(R.id.tv_descriptionn);
            tv_expiry = (TextView) v.findViewById(R.id.tv_date);
            tv_expiry2 = (TextView) v.findViewById(R.id.tv_date2);
            tv_listOfWinners = (TextView) v.findViewById(R.id.tv_list_of_winners);
            ll_previousMonths = (LinearLayout) v.findViewById(R.id.item_previous_month);
            ll_thisMonth = (LinearLayout) v.findViewById(R.id.item_this_month);
            ll_header = (LinearLayout) v.findViewById(R.id.ll_header);
            ll_footer = (LinearLayout) v.findViewById(R.id.ll_footer);
            tv_winnersText = (TextView) v.findViewById(R.id.tv_your_raffle_tickets);
            tv_noMembership = (TextView) v.findViewById(R.id.tv_no_membership);
            btn_profile = (TextView) v.findViewById(R.id.btn_saveProfile);
            tv_previousTitle = (TextView) v.findViewById(R.id.tv_title_previous);
            tv_description2 = (TextView) v.findViewById(R.id.tv_descriptionn_2);

        }
    }


}
