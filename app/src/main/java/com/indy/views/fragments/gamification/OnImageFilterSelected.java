package com.indy.views.fragments.gamification;

import com.indy.views.fragments.gamification.models.getimagefilters.ImageFilter;

/**
 * Created by Tohamy on 10/3/2017.
 */

public interface OnImageFilterSelected {
    void filterSelected(ImageFilter filter, int position);
}
