package com.indy.views.fragments.gamification;

import com.indy.views.fragments.gamification.models.getraffleslist.Raffle;

/**
 * Created by Tohamy on 10/4/2017.
 */

public interface OnRaffleListClick {
    void onRaffleClicked(Raffle raffle);

}
