package com.indy.views.fragments.buynewline;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.controls.ServiceUtils;
import com.indy.models.acceptTermsConditionsService.AccpetTermsConditionsServiceInput;
import com.indy.models.faqs.FaqsInputModel;
import com.indy.models.faqs.FaqsResponseModel;
import com.indy.models.inilizeregisteration.InilizeRegisterationInput;
import com.indy.models.inilizeregisteration.InilizeRegisteratonOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.ocr.screens.EmailAddressFragment;
import com.indy.ocr.screens.EmiratesIdDetailsFragment;
import com.indy.services.AcceptTermsConditionsService;
import com.indy.services.FaqsService;
import com.indy.services.LogApplicationFlowService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.buynewline.exisitingnumber.PromoCodeVerificationSuccessfulFragment;
import com.indy.views.fragments.newaccount.SetPasswordFragment;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.MasterFragment;

import static com.indy.views.activites.RegisterationActivity.logEventInputModel;


/**
 * Created by emad on 8/16/16.
 */
public class TermsAndConditionsFragment extends MasterFragment {

    private WebView paymentWebView;
    //    private ProgressBar progressID;
    //    TextView backImg;
    Button acceptId, cancelId;
    private View view;
    FaqsResponseModel faqsResponseModel;
    InilizeRegisteratonOutput inilizeRegisteratonOutput;
    BaseResponseModel mResponseModel;

    public static String userName, email, birtdate;
    private InilizeRegisterationInput inilizeRegisterationInput;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.activity_terms_conditions, container, false);
            initUI();
//            regActivity.addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        }
        if (regActivity != null) {
            regActivity.showHeaderLayout();
            regActivity.setHeaderTitle("");
        }
        return view;
    }


    @Override
    public void initUI() {
        super.initUI();
        paymentWebView = (WebView) view.findViewById(R.id.paymentWebView);
        paymentWebView.setBackgroundColor(0);
        regActivity.showHeaderLayout();
        regActivity.setHeaderTitle("");
//        progressID = (ProgressBar) view.findViewById(R.id.progressID);
//        backImg = (TextView) view.findViewById(R.id.backImg);
        acceptId = (Button) view.findViewById(R.id.acceptId);
        cancelId = (Button) view.findViewById(R.id.cancelId);
        if (!getArguments().getBoolean(ConstantUtils.isFromCreateNewAccount)) {

            userName = getArguments().getString(ConstantUtils.userName);
            email = getArguments().getString(ConstantUtils.email);
            birtdate = getArguments().getString(ConstantUtils.birthdate);
        }

        Bundle bundle2 = new Bundle();

        bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_onboarding_t_and_c_agreed);
        bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_onboarding_t_and_c_agreed);
        mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_onboarding_t_and_c_agreed, bundle2);


//        backImg.setText(getString(R.string.terms_conditions));
        WebSettings settings = paymentWebView.getSettings();
//        progressID.setVisibility(View.VISIBLE);
        settings.setJavaScriptEnabled(true);
        paymentWebView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        FaqsInputModel faqsInputModel = new FaqsInputModel();
        faqsInputModel.setAppVersion(appVersion);
        faqsInputModel.setDeviceId(deviceId);
        faqsInputModel.setToken(token);
        faqsInputModel.setOsVersion(osVersion);
        faqsInputModel.setChannel(channel);
        faqsInputModel.setLang(currentLanguage);
        //0562237176
//        faqsInputModel.setSignificant("REG_FAQ");
        faqsInputModel.setSignificant(ConstantUtils.REG_TERMSANDCONDITIONS);
        new FaqsService(this, faqsInputModel);
        disableAcceptBtn();
//        backImg.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                finish();
//            }
//        });
//        acceptId.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                logEvent(1);
//                if (getArguments() != null) {
//                    if (getArguments().getBoolean(ConstantUtils.isFromCreateNewAccount)) {
//                        fromNewAccount();
//                    } else {
//                        try {
//                            Bundle bundle2 = new Bundle();
//
//                            bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_confirm_age_validations);
//                            bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_confirm_age_validations);
//                            mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_confirm_age_validations, bundle2);
//
//                            logEvent(1);
//
//                            AccpetTermsConditionsServiceInput accpetTermsConditionsServiceInput = new AccpetTermsConditionsServiceInput();
//                            accpetTermsConditionsServiceInput.setAppVersion(appVersion);
//                            accpetTermsConditionsServiceInput.setDeviceId(deviceId);
//                            accpetTermsConditionsServiceInput.setOsVersion(osVersion);
//                            accpetTermsConditionsServiceInput.setLang(currentLanguage);
//                            accpetTermsConditionsServiceInput.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
//                            accpetTermsConditionsServiceInput.setToken(token);
//                            accpetTermsConditionsServiceInput.setChannel(channel);
//                            accpetTermsConditionsServiceInput.setEmail(EmailAddressFragment.emailStr);
//                            accpetTermsConditionsServiceInput.setContactNumber(EmailAddressFragment.mobileNoStr);
//
//                            if (NameDOBFragment.dateOfBirth != null && NameDOBFragment.dateOfBirth.length() > 0) {
//                                accpetTermsConditionsServiceInput.setBirthDate(NameDOBFragment.dateOfBirth);
//                            } else if (EmiratesIdDetailsFragment.dateOfBirth != null && EmiratesIdDetailsFragment.dateOfBirth.length() > 0) {
//                                accpetTermsConditionsServiceInput.setBirthDate(EmiratesIdDetailsFragment.dateOfBirth);
//                            }
//
//                            new AcceptTermsConditionsService(TermsAndConditionsFragment.this, accpetTermsConditionsServiceInput);
//
//                            if (OrderFragment.promoCode != null && OrderFragment.promoCode.length() > 0 && PromoCodeVerificationSuccessfulFragment.promoCodeType == 2) {
//                                CashOnDeliveryFragment.orderTypeStr = "2";
//                                regActivity.replaceFragmnet(new DeliveryDetailsAddress(), R.id.frameLayout, false);
//
//                            } else {
//                                regActivity.replaceFragmnet(new CashOnDeliveryFragment(), R.id.frameLayout, true);
//                            }
//
//                        } catch (Exception ex) {
//                            CommonMethods.logException(ex);
//                        }
//                    }
//                }
//
//            }
//        });
        cancelId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle2 = new Bundle();
                logEvent(2);
                bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_confirm_age_validations_canceled);
                bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_confirm_age_validations_canceled);
                mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_confirm_age_validations_canceled, bundle2);

                regActivity.onBackPressed();
            }
        });
        acceptId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                onAcceptDailoge();
                if (getArguments() != null) {
                    if (getArguments().getBoolean(ConstantUtils.isFromCreateNewAccount)) {
                        fromNewAccount();
                    } else {
                        regActivity.replaceFragmnet(new TermsAndConditionsConfirmation(), R.id.frameLayout, true);
                    }
                }

            }
        });
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
//        super.onErrorListener(responseModel);
        try {
            if (responseModel != null && responseModel.getServiceType() != null && (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.LOG_USER_EVENT) ||
                    responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.accept_terms_conditions_service))) {

            } else {

                showErrorFragment(getString(R.string.error_));
            }
        }catch (Exception ex){

        }
    }

    private void showErrorFragment(String error) {
        try {
            Bundle bundle = new Bundle();
            bundle.putString(ConstantUtils.errorString, error);
            ErrorFragment errorFragment = new ErrorFragment();
            errorFragment.setArguments(bundle);
            regActivity.replaceFragmnet(errorFragment, R.id.frameLayout, true);
        } catch (Exception ex) {
//            CommonMethods.logException(ex);
        }
    }

    private void fromNewAccount() {

        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_create_account_accept_terms);


        SetPasswordFragment setPasswordFragment = new SetPasswordFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.newAccountMobileNo, getArguments().getString(ConstantUtils.newAccountMobileNo));
        setPasswordFragment.setArguments(bundle);
        regActivity.replaceFragmnet(setPasswordFragment, R.id.frameLayout, true);
    }

    private void logEvent(int index) {


        logEventInputModel.setActionTransactionId(sharedPrefrencesManger.getTempAppId());
        logEventInputModel.setScreenId(ConstantUtils.SCREEN_ID_0004);

        logEventInputModel.setAccept(acceptId.getText().toString());

        new LogApplicationFlowService(this, logEventInputModel);

        if (index == 1) {
            logEventInputModel.setConfirmOrCancel(acceptId.getText().toString());
        } else {
            logEventInputModel.setConfirmOrCancel(cancelId.getText().toString());

        }
        new LogApplicationFlowService(this, logEventInputModel);

    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            if (isVisible()) {
                if (responseModel != null && responseModel.getResultObj() != null) {
                    this.mResponseModel = responseModel;
//        progressID.setVisibility(View.GONE);
                    if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.faqsService))
                        onTermsAndCoditionService();
                    else
                        onInitiateRegisterationRequestService();
                }
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

    }

    private void onInitiateRegisterationRequestService() {
        if (inilizeRegisteratonOutput != null) {
            if (inilizeRegisteratonOutput.getErrorCode() != null)
                onInitiateRegisterationRequestServiceError();
            else
                onInitiateRegisterationRequestServiceSucess();

        }
    }

    private void onInitiateRegisterationRequestServiceSucess() {
        if (inilizeRegisteratonOutput.getRegistrationId() != null) {
            regActivity.replaceFragmnet(new NewNumberFragment(), R.id.frameLayout, true);
            sharedPrefrencesManger.setRegisterationID(inilizeRegisteratonOutput.getRegistrationId());
        }
    }

    private void onInitiateRegisterationRequestServiceError() {
        Fragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, inilizeRegisteratonOutput.getErrorMsgEn());
        errorFragment.setArguments(bundle);
        regActivity.replaceFragmnet(errorFragment, R.id.frameLayout, true);
    }

    private void onTermsAndCoditionService() {
        faqsResponseModel = (FaqsResponseModel) mResponseModel.getResultObj();
        if (faqsResponseModel != null) {
            if (faqsResponseModel.getErrorCode() != null)
                onTermsAndCondtionsError();
            else
                onTermsAndCondtionsSucess();
        }
    }

    private void onTermsAndCondtionsSucess() {
        enableAcceptBtn();
        if (currentLanguage.equalsIgnoreCase("en"))
            getTermsAndConditions(faqsResponseModel.getSrcUrlEn());
        else
            getTermsAndConditions(faqsResponseModel.getSrcUrlAr());
    }

    private void onTermsAndCondtionsError() {
        Fragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, faqsResponseModel.getErrorMsgEn());
        errorFragment.setArguments(bundle);
        regActivity.replaceFragmnet(errorFragment, R.id.frameLayout, true);
    }


    private void getTermsAndConditions(String termsLinks) {
        paymentWebView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
//                progressID.setVisibility(View.GONE);
//                regActivity.onBackPressed();
                enableAcceptBtn();

            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//                progressID.setVisibility(View.GONE);
            }
        });
        paymentWebView.loadUrl(termsLinks);
    }

//    private void onAcceptDailoge() {
//        new SweetAlertDialog(getActivity(), SweetAlertDialog.NORMAL_TYPE)
//                .setTitleText("Notice")
//                .setContentText("By Accepting you are confirming that you have read T&C and the user is not below 7 years and not older than 24 years, there will be no refunded .")
//                .setConfirmText("Confirmed").setCancelText("Cancel")
//                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                    @Override
//                    public void onClick(SweetAlertDialog sDialog) {
//                        // reuse previous dialog instance
////                        finish();
//                        sDialog.dismissWithAnimation();
////                        progressID.setVisibility(View.VISIBLE);
//                        inilizeRegisterationInput = new InilizeRegisterationInput();
//                        inilizeRegisterationInput.setEmail(email);
//                        inilizeRegisterationInput.setFullName(userName);
//                        inilizeRegisterationInput.setBirthDate(birtdate);
//                        inilizeRegisterationInput.setAppVersion(appVersion);
//                        inilizeRegisterationInput.setDeviceId(deviceId);
//                        inilizeRegisterationInput.setToken(token);
//                        inilizeRegisterationInput.setOsVersion(osVersion);
//                        inilizeRegisterationInput.setChannel(channel);
//                        new InilizeRegisterationRequestService(TermsAndConditionsFragment.this, inilizeRegisterationInput);
//                    }
//                })
//                .show();
//    }

    private void enableAcceptBtn() {
        acceptId.setAlpha(1f);
        acceptId.setClickable(true);
//        acceptId.setEnabled(true);
//        acceptId.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                onAcceptDailoge();
//                logEvent(1);
//
//                if (getArguments() != null) {
//                    if (getArguments().getBoolean(ConstantUtils.isFromCreateNewAccount))
//                        fromNewAccount();
//                    else
//                        regActivity.replaceFragmnet(new TermsAndConditionsConfirmation(), R.id.frameLayout, true);
//                }
//            }
//        });
    }

    private void disableAcceptBtn() {
        acceptId.setAlpha(.5f);
        acceptId.setClickable(false);
        acceptId.setOnClickListener(null);
//        acceptId.setEnabled(false);

    }

}
