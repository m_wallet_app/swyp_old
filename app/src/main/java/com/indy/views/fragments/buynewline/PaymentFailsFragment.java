package com.indy.views.fragments.buynewline;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.indy.R;
import com.indy.services.LogApplicationFlowService;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.utils.MasterFragment;

import static com.indy.views.activites.RegisterationActivity.logEventInputModel;

/**
 * Created by emad on 10/8/16.
 */

public class PaymentFailsFragment extends MasterFragment {
    private View view;
    private Button bt_tryAgain;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.payment_error_fragment, container, false);
        initUI();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();

        bt_tryAgain = (Button) view.findViewById(R.id.iv_back);
        bt_tryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logEvent();
                getActivity().finish();
            }
        });
    }
    private void logEvent() {
        try {
            if (sharedPrefrencesManger != null)
                logEventInputModel.setActionTransactionId(sharedPrefrencesManger.getTempAppId());
            logEventInputModel.setScreenId(ConstantUtils.SCREEN_ID_1007);

            logEventInputModel.setTryAgain(bt_tryAgain.getText().toString());
            new LogApplicationFlowService(this, logEventInputModel);
        }catch (Exception ex){

        }
    }


}
