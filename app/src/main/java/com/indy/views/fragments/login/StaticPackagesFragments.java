package com.indy.views.fragments.login;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.adapters.StaticPackagesListAdapter;
import com.indy.controls.ServiceUtils;
import com.indy.models.getStaticBundles.BundleList;
import com.indy.models.getStaticBundles.StaticBundleOutput;
import com.indy.models.packages.UsagePackageList;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterInputResponse;
import com.indy.services.GetStaticPackageListService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.MasterActivity;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;

import java.util.List;

/**
 * Created by emad on 8/9/16.
 */
public class StaticPackagesFragments extends MasterActivity {
    //    private View view;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mGridLayoutManger;
    private List<BundleList> usagePackageList;
    private MasterInputResponse masterInputResponse;
    private StaticBundleOutput usagePackagesoutput;
    private StaticPackagesListAdapter packagesListAdapter;
    private UsagePackageList emptyUsagePackageListlItem;
    private Button bt_buyNow;
    private RelativeLayout rl_help, rl_back;
    private LinearLayout ll_buyNow;
    private TextView tv_swyp_description;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_static_packages_layout_red_bg);
        initUI();
        setListeners();
        onConsumeService();
        CommonMethods.logFirebaseEvent(mFirebaseAnalytics, ConstantUtils.TAGGING_what_is_swyp);
    }

//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        view = inflater.inflate(R.layout.fragment_static_packages_layout, container, false);
//        initUI();
//        setListeners();
//        onConsumeService();
//        return view;
//}

    @Override
    public void initUI() {
        super.initUI();
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mGridLayoutManger = new LinearLayoutManager(this);
        mGridLayoutManger.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mGridLayoutManger);
        mRecyclerView.setHasFixedSize(true);
        tv_swyp_description = (TextView) findViewById(R.id.tv_swyp_description);
        bt_buyNow = (Button) findViewById(R.id.bt_buyy_now);
        ll_buyNow = (LinearLayout) findViewById(R.id.ll_buy_now);
        if (sharedPrefrencesManger.isVATEnabled()) {
            Double price = Double.parseDouble(sharedPrefrencesManger.getAfterLoginPrice());
            tv_swyp_description.setText(String.format(getString(R.string.is_the_mobile_service_for_new_generation), String.valueOf(price + (price * 0.05))));

        } else {
            tv_swyp_description.setText(String.format(getString(R.string.is_the_mobile_service_for_new_generation), sharedPrefrencesManger.getAfterLoginPrice()));
        }
        if (sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)) {
            ((TextView) findViewById(R.id.tv_unlimited_wifi_english)).setVisibility(View.VISIBLE);
            ((LinearLayout) findViewById(R.id.ll_wifi_unlimited_arabic)).setVisibility(View.GONE);

            ((TextView) findViewById(R.id.tv_unlimited_wifi_description_arabic)).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.tv_unlimited_wifi_description_arabic)).setText(getString(R.string.unlimited_wifi_description));

            ((TextView) findViewById(R.id.tv_unlimited_wifi_description_english)).setVisibility(View.GONE);
        } else {
            ((TextView) findViewById(R.id.tv_unlimited_wifi_english)).setVisibility(View.GONE);
            ((LinearLayout) findViewById(R.id.ll_wifi_unlimited_arabic)).setVisibility(View.VISIBLE);


            ((TextView) findViewById(R.id.tv_unlimited_wifi_description_arabic)).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.tv_unlimited_wifi_description_arabic)).setText("            " + getString(R.string.unlimited_wifi_description));
            ((TextView) findViewById(R.id.tv_unlimited_wifi_description_english)).setVisibility(View.VISIBLE);
        }
//        if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().getBoolean(ConstantUtils.SHOW_BUY_NOW)) {
//
//            ll_buyNow.setVisibility(View.VISIBLE);
//            bt_buyNow.setVisibility(View.VISIBLE);
//            bt_buyNow.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    setResult(1);
//                    finish();
//
//                }
//            });
//        } else {
//            ll_buyNow.setVisibility(View.GONE);
//
//            bt_buyNow.setVisibility(View.GONE);
//        }
        bt_buyNow.setVisibility(View.GONE);
        rl_help = (RelativeLayout) findViewById(R.id.helpLayout);
        rl_help.setVisibility(View.INVISIBLE);

        rl_back = (RelativeLayout) findViewById(R.id.backLayout);
        rl_back.setVisibility(View.VISIBLE);
        rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            onBackPressed();
            if (responseModel != null && responseModel.getResultObj() != null) {
                if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.packageServiceType)) {
                    getPackagesList(responseModel);
                }
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }


    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        try {
            onBackPressed();
            if (responseModel != null) {
                showErrorFragment(responseModel.toString());
            } else {
                showErrorFragment("Error from service.");
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    private void getPackagesList(BaseResponseModel responseModel) {
        usagePackagesoutput = (StaticBundleOutput) responseModel.getResultObj();
        if (usagePackagesoutput != null) {
            if (usagePackagesoutput.getErrorCode() != null)
                onGetPackagesError();
            else
                onGetPackageSucess();
        }
    }


    private void onGetPackageSucess() {
        usagePackageList = usagePackagesoutput.getBundleList();
        if (usagePackageList != null)
            if (usagePackageList.size() > 0) {
//                BundleList bundleList = new BundleList();
//                bundleList.setBundleName("dummy element");
//                bundleList.setNameEn("dummy element");
//                usagePackageList.add(bundleList);
                packagesListAdapter = new StaticPackagesListAdapter(usagePackageList, getApplicationContext());
                mRecyclerView.setAdapter(packagesListAdapter);
            }

    }

    public void setListeners() {
//        continueBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(StaticPackagesFragments.this, RegisterationActivity.class);
//                startActivity(intent);
//                finish();
//            }
//        });
    }

    private void onGetPackagesError() {
        showErrorFragment(usagePackagesoutput.getErrorMsgEn());
    }

    public void showErrorFragment(String error) {
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();

        bundle.putString(ConstantUtils.errorString, error);
        errorFragment.setArguments(bundle);
        addFragmnet(errorFragment, R.id.frameLayout, true);
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        masterInputResponse = new MasterInputResponse();
        masterInputResponse.setAppVersion(appVersion);
        masterInputResponse.setToken(sharedPrefrencesManger.getToken());
        masterInputResponse.setOsVersion(osVersion);
        masterInputResponse.setChannel(channel);
        masterInputResponse.setDeviceId(deviceId);
        new GetStaticPackageListService(this, masterInputResponse);

    }


}
