package com.indy.views.fragments.buynewline.exisitingnumber;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.indy.R;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.LogApplicationFlowService;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.buynewline.NewNumberFragment;
import com.indy.views.fragments.utils.MasterFragment;

import static com.indy.views.activites.RegisterationActivity.logEventInputModel;

/**
 * Created by emad on 9/25/16.
 */

public class PendingPaymentFragment extends MasterFragment {

    private View view;
    private Button checkoutBalance, newNoBtn;
    private TextView tv_outstandingAmount;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_pending_payment, container, false);
        initUI();
        setListensers();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
        regActivity.showHeaderLayout();
        regActivity.setHeaderTitle("");
        newNoBtn = (Button) view.findViewById(R.id.newNoID);
        tv_outstandingAmount = (TextView) view.findViewById(R.id.tv_outstanding_amount);
        tv_outstandingAmount.setText(getArguments().getString("AMOUNT"));

    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
    }

    private void setListensers() {


        newNoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard();
                logEvent();
                regActivity.replaceFragmnet(new NewNumberFragment(), R.id.frameLayout, true);
            }
        });

    }

    private void logEvent() {

     logEventInputModel.setScreenId(ConstantUtils.SCREEN_ID_3041);

        logEventInputModel.setEligibilityFailureOutstandingAmountNewNumber(newNoBtn.getText().toString());

        new LogApplicationFlowService(this, logEventInputModel);
    }
}
