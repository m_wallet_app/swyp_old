package com.indy.views.fragments.gamification.models.serviceInputOutput;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by mobile on 21/12/2017.
 */

public class GetRewardForSharingOutputResponse extends MasterErrorResponse {

    @SerializedName("rewardAmount")
    @Expose
    private String rewardAmount;


    public String getRewardAmount() {
        return rewardAmount;
    }

    public void setRewardAmount(String actionName) {
        this.rewardAmount = actionName;
    }
}
