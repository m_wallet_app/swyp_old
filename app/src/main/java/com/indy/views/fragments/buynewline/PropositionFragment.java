package com.indy.views.fragments.buynewline;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.controls.AdjustEvents;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.HelpActivity;
import com.indy.views.activites.RegisterationActivity;
import com.indy.views.activites.SplashActivity;
import com.indy.views.fragments.login.LoginFragment;
import com.indy.views.fragments.login.StaticPackagesFragments;
import com.indy.views.fragments.utils.MasterFragment;

/**
 * Created by emad on 8/2/16.
 */
public class PropositionFragment extends MasterFragment {
    private View view;
    private Button buynow, loginBtn, langBtn, helpBtn;
    private TextView faqsId;
    private RelativeLayout backLayout, rl_whats_swyp;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (view == null) {
            view = inflater.inflate(R.layout.proposition_fragment, container, false);
            initUI();
            PaymentSucessfulllyFragment.paymentSucessfulllyFragmentInstance = null;
//            loadingAnimation();
//            startService();
        } else {
            regActivity.hideHeaderLayout();
            regActivity.setHeaderTitle("");
        }
        try {
            PaymentSucessfulllyFragment.paymentSucessfulllyFragmentInstance = null;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return view;

    }

    private void loadingAnimation() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            view.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop,
                                           int oldRight, int oldBottom) {
                    v.removeOnLayoutChangeListener(this);
                    int cx = v.getWidth() / 2;
                    int cy = v.getHeight() / 2;

                    // get the hypothenuse so the radius is from one corner to the other
                    int radius = (int) Math.hypot(top, bottom);


                    Animator reveal = ViewAnimationUtils.createCircularReveal(v, cx, cy, 0, radius);
                    reveal.setInterpolator(new DecelerateInterpolator(2f));
                    reveal.setDuration(6000);
                    reveal.start();

                }
            });
        }


    }

    @Override
    public void initUI() {
        super.initUI();
        buynow = (Button) view.findViewById(R.id.buynow);
        loginBtn = (Button) view.findViewById(R.id.loginBtn);
        langBtn = (Button) view.findViewById(R.id.langBtn);
        faqsId = (TextView) view.findViewById(R.id.faqsId);
        helpBtn = (Button) view.findViewById(R.id.helpBtn);
        backLayout = (RelativeLayout) view.findViewById(R.id.backLayout);
        rl_whats_swyp = (RelativeLayout) view.findViewById(R.id.rl_whats_swyp);
        regActivity.hideHeaderLayout();
        regActivity.setHeaderTitle("");
        if (sharedPrefrencesManger.getLanguage().equals(ConstantUtils.lang_english)) {
            langBtn.setText(getString(R.string.arabic));
        } else {
            langBtn.setText(getString(R.string.english));
        }
        helpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), HelpActivity.class));
//                stopService();

            }
        });
        faqsId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), HelpActivity.class);
                startActivity(intent);
            }
        });
        buynow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logEvent(AdjustEvents.CLICKS_GET_SIM,new Bundle());
                regActivity.replaceFragmnet(new Registeration2Fragment(), R.id.frameLayout, true);
                // regActivity.replaceFragmnet(new SwypeToReachUsFragmnet(), R.id.frameLayout, true);
                // startActivity(new Intent(getActivity(), DataPackageChartActivity.class));
//                startActivity(new Intent(getActivity(), SwypeToReachUsFragmnet.class));
                Bundle bundle = new Bundle();

                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_buy_now);
                bundle.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_buy_now);
                mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_buy_now, bundle);
            }
        });
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment loginFrag = new LoginFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean(ConstantUtils.showBackBtn, true);
                loginFrag.setArguments(bundle);
                regActivity.replaceFragmnet(loginFrag, R.id.frameLayout, true);
                Bundle bundle2 = new Bundle();

                bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_login_button);
                bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_login_button);
                mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_login_button, bundle2);
            }
        });

        rl_whats_swyp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), StaticPackagesFragments.class);
                intent.putExtra(ConstantUtils.SHOW_BUY_NOW,true);
                startActivityForResult(intent,ConstantUtils.SHOW_BUY_NOW_CODE);
            }
        });

        if (sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)) {
            Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/aktive_grotesk_rg.ttf");
            langBtn.setTypeface(tf);
        } else {
            Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/aller_rg.ttf");
            langBtn.setTypeface(tf);
        }

        langBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle bundle = new Bundle();

                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_change_language);
                bundle.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_change_language);
                mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_change_language, bundle);

                if (langBtn.getText().toString().equals(getString(R.string.arabic))) {

//                    langBtn.setText(getString(R.string.english));
                    sharedPrefrencesManger.setLanguage(ConstantUtils.arLng);
//                    ((MasterActivity) getActivity()).switchLocalizaion(sharedPrefrencesManger.getLanguage(),(MasterActivity)getActivity());
//                    LocalizationUtils.switchLocalizaion(sharedPrefrencesManger.getLanguage(), (MasterActivity) getActivity());
                    regActivity.finish();
                    regActivity.startActivity(new Intent(getActivity(), RegisterationActivity.class));
                } else if (langBtn.getText().toString().equals(getString(R.string.english))) {
//                    langBtn.setText(getString(R.string.arabic));

                    sharedPrefrencesManger.setLanguage(ConstantUtils.enLng);
//                    ((MasterActivity) getActivity()).switchLocalizaion(sharedPrefrencesManger.getLanguage(),(MasterActivity)getActivity());
//
//                    LocalizationUtils.switchLocalizaion(sharedPrefrencesManger.getLanguage(), (MasterActivity) getActivity());
                    regActivity.finish();
                    regActivity.startActivity(new Intent(getActivity(), RegisterationActivity.class));
                }
            }
        });
//.........................
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
//                Intent intent = new Intent(getActivity(), TutorialActivity.class);
//                getActivity().startActivity(intent);
            }
        });

        if (SplashActivity.isTutorialRunnung)
            backLayout.setVisibility(View.VISIBLE);
        else
            backLayout.setVisibility(View.GONE);


    }

//    public void startService() {
//        Intent lastUpdatedService = new Intent(getActivity(), LastUpdated.class);
//        getActivity().startService(lastUpdatedService);
//    }
//
//    public void stopService() {
//        Intent lastUpdatedService = new Intent(getActivity(), LastUpdated.class);
//        getActivity().stopService(lastUpdatedService);
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==ConstantUtils.SHOW_BUY_NOW_CODE){
            if(resultCode==1){
                regActivity.addFragmnet(new Registeration2Fragment(), R.id.frameLayout, true);
            }
        }
    }
}
