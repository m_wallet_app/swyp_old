package com.indy.views.fragments.gamification.models.getrafflesdetails;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mobile on 12/10/2017.
 */

public class Prize implements Parcelable
{

    @SerializedName("prizeName")
    @Expose
    private String prizeName;
    @SerializedName("prizeId")
    @Expose
    private String prizeId;
    @SerializedName("winnersList")
    @Expose
    private List<WinnersListModel> winnersList = null;
    public final static Parcelable.Creator<Prize> CREATOR = new Creator<Prize>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Prize createFromParcel(Parcel in) {
            return new Prize(in);
        }

        public Prize[] newArray(int size) {
            return (new Prize[size]);
        }

    }
            ;

    protected Prize(Parcel in) {
        this.prizeName = ((String) in.readValue((String.class.getClassLoader())));
        this.prizeId = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.winnersList, (WinnersListModel.class.getClassLoader()));
    }

    public Prize() {
    }

    public String getPrizeName() {
        return prizeName;
    }

    public void setPrizeName(String prizeName) {
        this.prizeName = prizeName;
    }

    public String getPrizeId() {
        return prizeId;
    }

    public void setPrizeId(String prizeId) {
        this.prizeId = prizeId;
    }

    public List<WinnersListModel> getWinnersList() {
        return winnersList;
    }

    public void setWinnersList(List<WinnersListModel> winnersList) {
        this.winnersList = winnersList;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(prizeName);
        dest.writeValue(prizeId);
        dest.writeList(winnersList);
    }

    public int describeContents() {
        return 0;
    }

}