package com.indy.views.fragments.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.customviews.CustomButton;
import com.indy.customviews.CustomEditText;
import com.indy.customviews.CustomTextView;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.validateinvitioncode.ValidateInvitaionCodeInput;
import com.indy.models.validateinvitioncode.ValidateInvitaionCodeOutput;
import com.indy.services.ValidateInvitaionCodeService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.HelpActivity;
import com.indy.views.activites.SwypeToReachUsFragmnet;
import com.indy.views.activites.TutorialActivity;
import com.indy.views.fragments.newaccount.CongratulationsFragmentNewAccount;
import com.indy.views.fragments.newaccount.NewAccountCodeFragment;
import com.indy.views.fragments.newaccount.NewAccountFragment;
import com.indy.views.fragments.newaccount.SetPasswordFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.indy.views.fragments.utils.MasterFragment;

/**
 * Created by emad on 9/7/16.
 */
public class InviteCodeFragment extends MasterFragment {
    private View view;
    private Button proceedBtn;
    private TextView skipBtn;
    private EditText code;

    private ValidateInvitaionCodeInput validateInvitaionCodeInput;
    private ValidateInvitaionCodeOutput validateInvitaionCodeOutput;
    private TextInputLayout til_code;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_invite_code, container, false);
        initUI();
        setListeners();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();

        code = (CustomEditText) view.findViewById(R.id.code);

        proceedBtn = (CustomButton) view.findViewById(R.id.proceedBtn);
        skipBtn = (CustomTextView) view.findViewById(R.id.skipBtn);
        til_code = (TextInputLayout) view.findViewById(R.id.code_input_layout);
        //..............header Buttons............
        RelativeLayout backLayout = (RelativeLayout) view.findViewById(R.id.backLayout);
        RelativeLayout helpLayout = (RelativeLayout) view.findViewById(R.id.helpLayout);

        helpLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), HelpActivity.class);
                startActivity(intent);
            }
        });

        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard();
                getActivity().onBackPressed();
            }
        });
        //..............header Buttons............

    }

    private void setListeners() {
        proceedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()) {

                    CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_create_account_enter_invite_code);


                    onConsumeService();
                }
//                    onConsumeService();
//                else
//                    showErrorDalioge(getString(R.string.enter_invitaion_code));
            }
        });
        skipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                regActivity.replaceFragmnet(new SwypeToReachUsFragmnet(), R.id.frameLayout, true);
                if (TutorialActivity.tutorialActivityInstance != null)
                    TutorialActivity.tutorialActivityInstance.finish();

//                onSkip();
            }
        });


        proceedBtn.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (code.getText().toString().trim().length() > 0) {
                    enableLoginButton();
                } else {
                    disableLoginButton();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void onSkip() {
        Fragment[] myFragments = {new NewAccountFragment(),
                new NewAccountCodeFragment(),
                new SetPasswordFragment(),
                new InviteCodeFragment()
        };

        regActivity.removeFragment(myFragments);
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        regActivity.addFragmnet(new LoadingFragmnet(), R.id.loadingFragmeLayout, true);
        validateInvitaionCodeInput = new ValidateInvitaionCodeInput();
        validateInvitaionCodeInput.setAppVersion(appVersion);
        validateInvitaionCodeInput.setToken(token);
        validateInvitaionCodeInput.setOsVersion(osVersion);
        validateInvitaionCodeInput.setChannel(channel);
        validateInvitaionCodeInput.setDeviceId(deviceId);
        validateInvitaionCodeInput.setMsisdn(getArguments().getString(ConstantUtils.newAccountMobileNo));
        validateInvitaionCodeInput.setInvitationCode(code.getText().toString().trim());
        validateInvitaionCodeInput.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        new ValidateInvitaionCodeService(this, validateInvitaionCodeInput);
    }

    private boolean isValid() {
        if (code.getText().toString().length() == 0) {
            setTextInputLayoutError(til_code, getString(R.string.please_enter_code));
            return false;
        } else {
            removeTextInputLayoutError(til_code);
            return true;
        }


    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            if (isVisible()) {
                regActivity.onBackPressed();
                if(responseModel!=null && responseModel.getResultObj()!=null) {
                    validateInvitaionCodeOutput = (ValidateInvitaionCodeOutput) responseModel.getResultObj();
                    if (validateInvitaionCodeOutput != null) {
                        if (validateInvitaionCodeOutput.getErrorCode() != null)
                            onErrorCode();
                        else
                            onValidCode();
                    } else {
                        onErrorCode();
                    }
                }
            }
        } catch (Exception ex) {

        }
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        try {
            if (isVisible()) {
                regActivity.onBackPressed();
                setTextInputLayoutError(til_code, getString(R.string.code_provided_is_not_valid));
            }
        }catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    private void onValidCode() {
        if (validateInvitaionCodeOutput.getValid()) {
//            onSkip();
            Bundle bundle2 = new Bundle();

            bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_create_account_validation_code_valid);
            bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_create_account_validation_code_valid);
            mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_create_account_validation_code_valid, bundle2);
            CongratulationsFragmentNewAccount congratulationsFragmentNewAccount = new CongratulationsFragmentNewAccount();
            Bundle bundle = new Bundle();
            bundle.putString(ConstantUtils.newAccountMobileNo, getArguments().getString(ConstantUtils.newAccountMobileNo));
            congratulationsFragmentNewAccount.setArguments(bundle);
            regActivity.replaceFragmnet(congratulationsFragmentNewAccount, R.id.frameLayout, true);
        } else {
            setTextInputLayoutError(til_code, getString(R.string.code_provided_is_not_valid));
        }
//            showErrorFrafggment(getString(R.string.invalid_invitaion_code));
    }


    private void onErrorCode() {
//        showErrorDalioge(validateInvitaionCodeOutput.getErrorMsgEn());
        Bundle bundle2 = new Bundle();

        bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_create_account_invalid_invite_code);
        bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_create_account_invalid_invite_code);
        mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_create_account_invalid_invite_code, bundle2);
        setTextInputLayoutError(til_code, getString(R.string.code_provided_is_not_valid));

    }

    private void disableLoginButton() {
        proceedBtn.setAlpha(0.5f);
        proceedBtn.setEnabled(false);
    }

    private void enableLoginButton() {
        proceedBtn.setAlpha(1.0f);
        proceedBtn.setEnabled(true);
    }
}
