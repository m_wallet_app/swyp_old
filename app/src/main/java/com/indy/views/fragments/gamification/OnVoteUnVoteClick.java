package com.indy.views.fragments.gamification;

import com.indy.views.fragments.gamification.models.EventPhoto;

/**
 * Created by mobile on 04/09/2017.
 */

public interface OnVoteUnVoteClick {

    void onVoteUnVoteClick(EventPhoto eventPhoto, boolean isChecked, int index);
}
