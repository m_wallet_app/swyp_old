package com.indy.views.fragments.buynewline.exisitingnumber;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.controls.ServiceUtils;
import com.indy.models.MigrateEtisaltNumber.MigrateEtisalatNumerOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterInputResponse;
import com.indy.models.validateotp.ValidateOtpInput;
import com.indy.models.validateotp.ValidateOtpOutput;
import com.indy.ocr.screens.EmailAddressFragment;
import com.indy.services.LogApplicationFlowService;
import com.indy.services.NumberMigrationService;
import com.indy.services.ValidateOtpService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.HelpActivity;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.indy.views.fragments.utils.MasterFragment;

import java.util.ArrayList;

import static com.indy.views.activites.RegisterationActivity.logEventInputModel;

/**
 * Created by emad on 9/7/16.
 */
public class MigratingNumberCodeFragment extends MasterFragment {
    private View view;
    private TextView resendBtn;
    private Button continueButton;
    private EditText code1, code2, code3, code4;
    private TextView help;
    //    private String codeGenerated;
    private String vCtag = "CODE";
    private CountDownTimer countDownTimer;
    int[] tvListIds = {R.id.code_1, R.id.code_2, R.id.code_3, R.id.code_4};
    ArrayList<EditText> tv_digits;
    private MigrateEtisalatNumerOutput migrateEtisalatNumerOutput;
    ProgressBar progressID;
    ValidateOtpOutput validateOtpOutput;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_migrating_number_code, container, false);
        initUI();
        setListeners();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
        regActivity.showHeaderLayout();
        regActivity.setHeaderTitle("");
        code1 = (EditText) view.findViewById(R.id.code_1);
        code2 = (EditText) view.findViewById(R.id.code_2);
        code3 = (EditText) view.findViewById(R.id.code_3);
        code4 = (EditText) view.findViewById(R.id.code_4);
        resendBtn = (TextView) view.findViewById(R.id.resendBtn);
        progressID = (ProgressBar) view.findViewById(R.id.progressID);
        help = (TextView) view.findViewById(R.id.help);
        continueButton = (Button) view.findViewById(R.id.continueBtn);
//        if (getArguments() != null && !getArguments().getString(vCtag, "").isEmpty()) {
//            codeGenerated = getArguments().getString(vCtag);
//        }
        showKeyBoard();

        startTimer();
        tv_digits = new ArrayList<EditText>();
        for (int i = 0; i < tvListIds.length; i++) {
            EditText et_temp = (EditText) view.findViewById(tvListIds[i]);
            et_temp.addTextChangedListener(new CustomTextWatcher(et_temp));
            tv_digits.add(et_temp);
        }


    }

    private void startTimer() {
//        if (countDownTimer == null) {
        countDownTimer = new CountDownTimer(61000, 1000) {

            public void onTick(long millisUntilFinished) {
                resendBtn.setText("Resend code in: " + millisUntilFinished / 1000);
                continueButton.setEnabled(true);
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                resendBtn.setText("Resend code");
                continueButton.setEnabled(false);
                cancel();
            }

        }.start();
//        }
    }

    private void showErrorFragment(String error) {
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, error);
        ErrorFragment errorFragment = new ErrorFragment();
        regActivity.replaceFragmnet(errorFragment, R.id.frameLayout, false);
    }

    private void showLoadingFragment() {
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.loadingString, getString(R.string.verifying_code));
        LoadingFragmnet loadingFragmnet = new LoadingFragmnet();
        loadingFragmnet.setArguments(bundle);
        regActivity.addFragmnet(loadingFragmnet, R.id.loadingFragmeLayout, true);
    }

    private void setListeners() {
        resendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //// TODO: 10/3/2016 : resend service to be called here\
                onConsumeService();
            }
        });
        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onHelp();
            }
        });
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onContinue();
            }
        });
    }

    private void onContinue() {
        if (countDownTimer != null)
            countDownTimer.onFinish();
        countDownTimer = null;


//        if (getCode().equals(codeGenerated)) {
//            regActivity.replaceFragmnet(new OrderFragment(), R.id.frameLayout, true);
//        } else {
//            showErrorFragment("Invalid code entered. Please try again.");
//        }

        progressID.setVisibility(View.VISIBLE);
        ValidateOtpInput validateOtpInput = new ValidateOtpInput();
        validateOtpInput.setAuthToken(authToken);
        validateOtpInput.setAppVersion(appVersion);
        validateOtpInput.setChannel(channel);
        validateOtpInput.setDeviceId(deviceId);
        validateOtpInput.setImsi(sharedPrefrencesManger.getMobileNo());
        validateOtpInput.setLang(currentLanguage);
        validateOtpInput.setOtpCode(getCode());
        validateOtpInput.setOsVersion(osVersion);
        validateOtpInput.setToken(token);
        validateOtpInput.setMsisdn(getArguments().getString(ConstantUtils.mobileNo));
        new ValidateOtpService(this, validateOtpInput);

    }


    private void onHelp() {
        Intent intent = new Intent(getActivity(), HelpActivity.class);
        startActivity(intent);
    }

    public void showKeyBoard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, 0);
        }
    }


    public class CustomTextWatcher implements TextWatcher {
        private EditText view;

        private CustomTextWatcher(EditText view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

            String text = editable.toString();
            if (text.length() == 1) {
                view.clearFocus();
                int currentIndex = tv_digits.indexOf(view);
                for (int i = currentIndex + 1; i < tv_digits.size(); i++) {
                    tv_digits.get(i).requestFocus();
                    return;

                }

            }

        }
    }

    private String getCode() {
        String codeEntered = "";

        codeEntered += code1.getText().toString();
        codeEntered += code2.getText().toString();
        codeEntered += code3.getText().toString();
        codeEntered += code4.getText().toString();
        return codeEntered;
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            if (responseModel != null && responseModel.getServiceType() != null && responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.LOG_USER_EVENT)) {

            } else {


                regActivity.onBackPressed();
                progressID.setVisibility(View.GONE);
                if (responseModel != null && responseModel.getResultObj() != null) {
                    if (responseModel.getServiceType().equalsIgnoreCase(ConstantUtils.validateOtp)) {
                        this.validateOtpOutput = (ValidateOtpOutput) responseModel.getResultObj();
                        if (validateOtpOutput.getErrorCode() == null && validateOtpOutput.getIsValid())
                            onValidateOtpSucess();
                        else
                            onvalidateOtpError();


                    } else {
                        migrateEtisalatNumerOutput = (MigrateEtisalatNumerOutput) responseModel.getResultObj();
//            if (migrateEtisalatNumerOutput != null)
                        onNumberMigrationResponse();
                    }
                }
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    private void onValidateOtpSucess() {
//        logEvent();
        CommonMethods.logFirebaseEvent(mFirebaseAnalytics,ConstantUtils.TAGGING_migration_sim_order);
        regActivity.replaceFragmnet(new EmailAddressFragment(), R.id.frameLayout, true);
    }

    private void onvalidateOtpError() {
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        if (currentLanguage != null) {
            if (currentLanguage.equalsIgnoreCase("en"))
                bundle.putString(ConstantUtils.errorString, validateOtpOutput.getErrorMsgEn());
            else
                bundle.putString(ConstantUtils.errorString, validateOtpOutput.getErrorMsgAr());
        }
        errorFragment.setArguments(bundle);
        regActivity.replaceFragmnet(errorFragment, R.id.frameLayout, true);
    }

    private void onNumberMigrationResponse() {
        if (migrateEtisalatNumerOutput.getErrorCode() != null)
            onNumberMigrationError();
        else
            onNumberMigrationSucess();
    }

    private void onNumberMigrationError() {
        showErrorFragment(migrateEtisalatNumerOutput.getErrorMsgEn());
    }

    private void onNumberMigrationSucess() {
//        if (migrateEtisalatNumerOutput.getGeneratedCode() != null
//              /*  && !migrateEtisalatNumerOutput.getGeneratedCode().isEmpty()*/) {
        startTimer();
//        } else {
//            showErrorFragment(getString(R.string.number_migration_error));
//        }


    }


    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        try {
            progressID.setVisibility(View.GONE);
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        regActivity.addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        MasterInputResponse masterInputResponse = new MasterInputResponse();
        masterInputResponse.setAppVersion(appVersion);
        masterInputResponse.setToken(token);
        masterInputResponse.setOsVersion(osVersion);
        masterInputResponse.setChannel(channel);
        masterInputResponse.setDeviceId(deviceId);
        masterInputResponse.setMsisdn(getArguments().getString(ConstantUtils.mobileNo));
        new NumberMigrationService(MigratingNumberCodeFragment.this, masterInputResponse);
    }

    private void logEvent() {

        logEventInputModel.setScreenId(ConstantUtils.SCREEN_ID_3006);

        logEventInputModel.setOtp(getCode());
        new LogApplicationFlowService(this, logEventInputModel);
    }
}
