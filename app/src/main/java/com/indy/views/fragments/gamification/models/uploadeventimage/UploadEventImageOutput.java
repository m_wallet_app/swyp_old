package com.indy.views.fragments.gamification.models.uploadeventimage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by Tohamy on 10/3/2017.
 */

public class UploadEventImageOutput extends MasterErrorResponse {

    @SerializedName("uploaded")
    @Expose
    private boolean uploaded;
    @SerializedName("uploadedEntryId")
    @Expose
    private String uploadedEntryId;

    @SerializedName("totalRaffleTicketsMsg")
    @Expose
    private String totalRaffleTicketsMsg;

    @SerializedName("uploadReward")
    @Expose
    private String uploadReward;


    public boolean isUploaded() {
        return uploaded;
    }

    public void setUploaded(boolean uploaded) {
        this.uploaded = uploaded;
    }

    public String getUploadedEntryId() {
        return uploadedEntryId;
    }

    public void setUploadedEntryId(String uploadedEntryId) {
        this.uploadedEntryId = uploadedEntryId;
    }

    public String getTotalRaffleTicketsMsg() {
        return totalRaffleTicketsMsg;
    }

    public void setTotalRaffleTicketsMsg(String totalRaffleTicketsMsg) {
        this.totalRaffleTicketsMsg = totalRaffleTicketsMsg;
    }

    public String getUploadReward() {
        return uploadReward;
    }

    public void setUploadReward(String uploadReward) {
        this.uploadReward = uploadReward;
    }
}
