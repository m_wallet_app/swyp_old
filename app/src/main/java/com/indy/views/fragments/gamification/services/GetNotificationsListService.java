package com.indy.views.fragments.gamification.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.BaseServiceManger;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.gamification.models.getnotificationslist.GetNotificationsListInput;
import com.indy.views.fragments.gamification.models.getnotificationslist.GetNotificationsListOutput;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Tohamy on 10/4/2017.
 */

public class GetNotificationsListService extends BaseServiceManger {

    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private GetNotificationsListInput getNotificationsListInput;

    public GetNotificationsListService(BaseInterface baseInterface, GetNotificationsListInput getNotificationsListInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.getNotificationsListInput = getNotificationsListInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<GetNotificationsListOutput> call = apiService.getNotificationsList(getNotificationsListInput);
        call.enqueue(new Callback<GetNotificationsListOutput>() {
            @Override
            public void onResponse(Call<GetNotificationsListOutput> call, Response<GetNotificationsListOutput> response) {
                mBaseResponseModel.setServiceType(ServiceUtils.GET_NOTIFICATIONS_LIST);
                mBaseResponseModel.setResultObj(response.body());

                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (response.body().getErrorMsgEn() != null) {
//                        String urlForTag = call.request().url().toString();
//                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
//                        urlForTag = urlForTag.replace("/", "_");
//                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<GetNotificationsListOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.GET_NOTIFICATIONS_LIST);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}