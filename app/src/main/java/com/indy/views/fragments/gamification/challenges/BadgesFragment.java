package com.indy.views.fragments.gamification.challenges;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.indy.R;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.SwpeMainActivity;
import com.indy.views.fragments.gamification.OnBadgeListClicked;
import com.indy.views.fragments.gamification.adapters.BadgesListAdapter;
import com.indy.views.fragments.gamification.models.getbadgeslist.Badge;
import com.indy.views.fragments.gamification.models.getbadgeslist.GetBadgesListInputModel;
import com.indy.views.fragments.gamification.models.getbadgeslist.GetBadgesListOutputModel;
import com.indy.views.fragments.gamification.services.GetBadgesListService;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.MasterFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class BadgesFragment extends MasterFragment implements OnBadgeListClicked {

    BadgesListAdapter mBadgesListAdapter;
    RecyclerView rv_badgesList;
    View rootView;
    private GetBadgesListOutputModel getBadgesListOutputModel;
    private ProgressBar progressBar;

    public BadgesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_badges, container, false);
        initUI();
        return rootView;
    }

    @Override
    public void initUI() {
        super.initUI();
        rv_badgesList = (RecyclerView) rootView.findViewById(R.id.rv_badges_list);
        rv_badgesList.setLayoutManager(new LinearLayoutManager(getActivity()));
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressID);
        onConsumeService();
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        progressBar.setVisibility(View.VISIBLE);
        GetBadgesListInputModel getBadgesListInputModel = new GetBadgesListInputModel();
        getBadgesListInputModel.setChannel(channel);
        getBadgesListInputModel.setLang(currentLanguage);
        getBadgesListInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        getBadgesListInputModel.setAppVersion(appVersion);
        getBadgesListInputModel.setAuthToken(authToken);
        getBadgesListInputModel.setDeviceId(deviceId);
        getBadgesListInputModel.setToken(token);
        getBadgesListInputModel.setUserSessionId(sharedPrefrencesManger.getUserSessionId());
        getBadgesListInputModel.setUserId(sharedPrefrencesManger.getUserId());
        getBadgesListInputModel.setApplicationId(ConstantUtils.INDY_TALOS_ID);
        getBadgesListInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
        getBadgesListInputModel.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        getBadgesListInputModel.setOsVersion(osVersion);
        new GetBadgesListService(this, getBadgesListInputModel);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        progressBar.setVisibility(View.GONE);
        switch (responseModel.getServiceType()) {
            case ServiceUtils.GET_BADGES_LIST: {
                if (responseModel != null) {
                    getBadgesListOutputModel = (GetBadgesListOutputModel) responseModel.getResultObj();
                    if (getBadgesListOutputModel != null)
                        if (getBadgesListOutputModel.getBadgeCategories() != null
                                && !getBadgesListOutputModel.getBadgeCategories().isEmpty()) {
                            getBadgesListSuccess();
                        } else {
                            getBadgesListError();
                        }
                }
                break;
            }
        }
    }

    private void getBadgesListSuccess() {
        mBadgesListAdapter = new BadgesListAdapter(getBadgesListOutputModel.getBadgeCategories(), getActivity(), this);
        rv_badgesList.setAdapter(mBadgesListAdapter);
    }

    private void getBadgesListError() {
        try {
            ErrorFragment errorFragment = new ErrorFragment();
            Bundle bundle = new Bundle();
            bundle.putString(ConstantUtils.errorString, sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)
                    ? getBadgesListOutputModel.getErrorMsgEn() : getBadgesListOutputModel.getErrorMsgAr());
            errorFragment.setArguments(bundle);
            ((SwpeMainActivity) getActivity()).replaceFragmnet(errorFragment, R.id.frameLayout, true);
        }catch (Exception ex){

        }
    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onBadgeClicked(Badge badge, int index) {
        Intent intent = new Intent(getActivity(), BadgeDetailActivity.class);
        intent.putExtra(ConstantUtils.KEY_BADGE_DETAIL, badge);
//        intent.putExtra(ConstantUtils.KEY_BADGE_CATEGORY,getBadgesListOutputModel.getBadgeCategories().get(index).getCategoryNameEn());
        startActivity(intent);
    }
}
