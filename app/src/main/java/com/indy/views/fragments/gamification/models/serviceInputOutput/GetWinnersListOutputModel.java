package com.indy.views.fragments.gamification.models.serviceInputOutput;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.views.fragments.gamification.models.EventWinnerModel;
import com.indy.views.fragments.gamification.models.EventsList;

import java.util.List;

/**
 * Created by mobile on 25/09/2017.
 */

public class GetWinnersListOutputModel extends MasterErrorResponse {
    @SerializedName("winnersList")
    @Expose
    private List<EventWinnerModel> winnersList = null;
    @SerializedName("indyEventDetails")
    @Expose
    private EventsList indyEventDetails;
    @SerializedName("userWon")
    @Expose
    private Boolean userWon;

    public List<EventWinnerModel> getWinnersList() {
        return winnersList;
    }

    public void setWinnersList(List<EventWinnerModel> winnersList) {
        this.winnersList = winnersList;
    }

    public EventsList getIndyEventDetails() {
        return indyEventDetails;
    }

    public void setIndyEventDetails(EventsList indyEventDetails) {
        this.indyEventDetails = indyEventDetails;
    }

    public Boolean getUserWon() {
        return userWon;
    }

    public void setUserWon(Boolean userWon) {
        this.userWon = userWon;
    }
}
