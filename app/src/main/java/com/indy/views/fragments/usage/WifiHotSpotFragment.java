package com.indy.views.fragments.usage;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.helpers.GoogleMapsHelper;
import com.indy.helpers.autocomplete.AutoCompleteSearchAdapter;
import com.indy.helpers.autocomplete.Search;
import com.indy.models.location.GeoLocation;
import com.indy.models.location.LocationInputModel;
import com.indy.models.location.LocationModelResponse;
import com.indy.models.location.NearestLocationList;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.services.GetNearestLocationService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.utils.GPSTracker;
import com.indy.views.fragments.findus.EndSessionConfirmationFragment;
import com.indy.views.fragments.utils.MasterFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by emad on 9/27/16.
 */

public class WifiHotSpotFragment extends MasterFragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
        GoogleMap.OnInfoWindowClickListener, LocationListener {
    View view;
    public GoogleMap mMap;
    public MarkerOptions marker;
    public static HashMap<String, Integer> mMarkerList;
    public static HashMap<LatLng, Marker> mMarkersForSettingList;
    Marker highlightedMarker;
    LocationManager locationManager;
    GPSTracker gpsTracker;
    private Button locateMe;
    public TextView tv_logoutFromHotSpots;
    public LocationModelResponse mLocationModelResponse;
    public String locationName = null;
    private Double mLatitude, mLongitude;
    Location mlocation;
    LinearLayout ll_progress;
    public static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    public static final int LOCATION_REQUEST = 1340;
    GoogleMapsHelper googleMapsHelper;

    //---------
    LinearLayout ll_dummyContainer;
    TextView bt_retry;
    TextView tv_error;
    //------
    LinearLayout ll_progressBar, ll_body;


    //-----------AUTOCOMPLETE SEARCH------//
    AutoCompleteSearchAdapter autoCompleteSearchAdapter;
    ArrayList<Search> searches;
    AutoCompleteTextView et_autoComplete;
    TextView tv_change, tv_name, tv_distance;
    ImageView iv_locateMe;
    LinearLayout ll_infoWindow;
    RelativeLayout ll_marker;

    //------------AUTOCOMPLETE SEARCH----------//
    public WifiHotSpotFragment() {
        // Required empty public constructor
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_wifi_hotspots, container, false);
        mMarkerList = new HashMap<String, Integer>();
        mMarkersForSettingList = new HashMap<>();
        initUI();
//        onConsumeService();
        return view;
    }


    @Override
    public void initUI() {
        super.initUI();


        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        ll_progressBar = (LinearLayout) view.findViewById(R.id.ll_progressBar);
        ll_progressBar.setVisibility(View.GONE);
        googleMapsHelper = new GoogleMapsHelper(getContext());
        locateMe = (Button) view.findViewById(R.id.btn_get_directions);

        locateMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mLatitude != null && mLongitude != null)
                    googleMapsHelper.onGoogleMpasOpen(mLatitude, mLongitude, locationName);
            }
        });
        ll_body = (LinearLayout) view.findViewById(R.id.ll_body);
        iv_locateMe = (ImageView) view.findViewById(R.id.iv_locateMe);
        iv_locateMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mMap != null && mlocation != null) {
                    LatLng latLng = new LatLng(mlocation.getLatitude(), mlocation.getLongitude());
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, ConstantUtils.zoomLevel));
                }
            }
        });

        tv_logoutFromHotSpots = (TextView) view.findViewById(R.id.tv_logout_findus);
        tv_logoutFromHotSpots.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_usage_logout_from_hotspots);

                startActivity(new Intent(getActivity(), EndSessionConfirmationFragment.class));
            }
        });
        if (!sharedPrefrencesManger.getAuthToken().isEmpty()) {
            enableLogout();
        } else {
            disableLogout();
        }
        Bundle bundle = new Bundle();

        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "wifi_hotspots_screen");
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "wifi_hotspots_screen");
        mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_WIFI, bundle);


        et_autoComplete = (AutoCompleteTextView) view.findViewById(R.id.et_search_all);
        tv_change = (TextView) view.findViewById(R.id.tv_change);
        tv_distance = (TextView) view.findViewById(R.id.tv_distance);
        tv_name = (TextView) view.findViewById(R.id.tv_name);
        SpannableString content = new SpannableString(getString(R.string.change));

        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        tv_change.setText(content);
        tv_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_autoComplete.setVisibility(View.VISIBLE);
                ll_marker.setVisibility(View.GONE);
                et_autoComplete.requestFocus();
            }
        });
        ll_marker = (RelativeLayout) view.findViewById(R.id.ll_marker);
        et_autoComplete.setVisibility(View.GONE);
        initErrorLayout();
    }

    private void initErrorLayout() {
        ll_dummyContainer = (LinearLayout) view.findViewById(R.id.ll_dummy_error_container);

        ll_dummyContainer.setVisibility(View.GONE);
        bt_retry = (TextView) view.findViewById(R.id.retryBtn);
        bt_retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onConsumeService();
            }
        });
        tv_error = (TextView) view.findViewById(R.id.tv_error);
    }

    private void disableLogout() {
        tv_logoutFromHotSpots.setAlpha(.5f);
        tv_logoutFromHotSpots.setClickable(false);

    }

    private void enableLogout() {
        tv_logoutFromHotSpots.setAlpha(1f);
        tv_logoutFromHotSpots.setClickable(true);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        checkPermission();
        isGPSEnabled();
    }

    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                this.requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            }
        }
    }


    @Override
    public boolean onMarkerClick(Marker marker) {
        int index = -1;
        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_usage_click_marker_hotspots);
        try {
            if (mMarkerList != null && mMarkerList.size() > 0 && marker != null && marker.getId() != null) {

                index = mMarkerList.get(marker.getId());
            } else {
                index = -1;
            }
            if (index != -1) {
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_usage_click_marker_hotspots + "_" + mLocationModelResponse.getNearestLocationList().get(index).getAddressEn());
                if (currentLanguage.equals("en")) {
                    locationName = mLocationModelResponse.getNearestLocationList().get(index).getAddressEn();
                } else {
                    locationName = mLocationModelResponse.getNearestLocationList().get(index).getAddressAr();
                }


                mLatitude = mLocationModelResponse.getNearestLocationList().get(index).getGeoLocation().getLatitude();
                mLongitude = mLocationModelResponse.getNearestLocationList().get(index).getGeoLocation().getLongitude();
//            locationNameId.setText(locationName);
                LatLng latLng = new LatLng(mLatitude, mLongitude);
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, ConstantUtils.zoomLevel));
                ll_marker.setVisibility(View.VISIBLE);
                et_autoComplete.setVisibility(View.GONE);
//            tv_name.setText(locationName);
//            tv_distance.setText(distance + " Km");

            }
        }catch (Exception ex){

        }

        return false;
    }


    @Override
    public void onInfoWindowClick(Marker marker) {
        GoogleMapsHelper openGoogleMaps = new GoogleMapsHelper(getContext());
        openGoogleMaps.onGoogleMpasOpen(marker.getPosition().latitude, marker.getPosition().longitude, locationName);
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    private boolean canAccessLocation() {
        if (!hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
            return false;
        } else {
            return true;
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == getActivity().checkSelfPermission(perm));
    }

    private void isGPSEnabled() {
        LocationManager locationManager = (LocationManager)
                getActivity().getSystemService(getActivity().LOCATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (canAccessLocation()) {// gps is allowed
//                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && canAccessLocation()) {// gps is enable
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {// gps is disable
                    checkForGps();
//                    onConsumeService();
                } else {
                    gpsTracker = new GPSTracker(getActivity());
                    if (gpsTracker.canGetLocation()) {
                        mlocation = gpsTracker.getLocation();
                        if (mlocation != null) {
                            onConsumeService();

                        } else {
                            Toast.makeText(getActivity(), "Fetching Location, please wait..", Toast.LENGTH_SHORT).show();
                            try {
                                Thread.sleep(1500);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            mlocation = gpsTracker.getLocation();
                            onConsumeService();

                        }
                    } else {
                        onConsumeService();
                    }
//                    chec/kLocation();
                }
//                checkLocation();
            } else {
                requestPermissions(LOCATION_PERMS, 0);
            }

        } else {
            gpsTracker = new GPSTracker(getActivity());

            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) { // gps is disable
//                checkForGps();

                onConsumeService();
            } else { // gps is enable
                if (gpsTracker.canGetLocation()) {
                    mlocation = gpsTracker.getLocation();
                    if (mlocation != null) {
                        onConsumeService();
                    } else {
                        Toast.makeText(getActivity(), "Fetching Location, please wait..", Toast.LENGTH_SHORT).show();
                        try {
                            Thread.sleep(1500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        mlocation = gpsTracker.getLocation();
                        onConsumeService();
                    }
                }
            }

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 0:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    isGPSEnabled();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    checkForGps();
//                    Toast.makeText(this, "To use this feature, kindly allow Ding access to your location.", Toast.LENGTH_LONG).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ll_dummyContainer.setVisibility(View.GONE);
        switch (requestCode) {
            case 100:
                LocationManager locationManager = (LocationManager)
                        getActivity().getSystemService(getActivity().LOCATION_SERVICE);
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    checkForGps();
                } else {
                    isGPSEnabled();
                }
                break;
            default:
                break;
        }

    }

    private void showErrorFragment() {
//        frameLayout.setVisibility(View.VISIBLE);
//        ErrorFragment errorFragment = new ErrorFragment();
//        Bundle bundle = new Bundle();
//        bundle.putString(ConstantUtils.errorString, "Cannot get Location please enable GPS.");
//        bundle.putString(ConstantUtils.settingsButtonEnabled, "true");
//        bundle.putString(ConstantUtils.buttonTitle, "Settings");
//        errorFragment.setArguments(bundle);
//        ((SwpeMainActivity) getActivity()).replaceFragmnet(errorFragment, R.id.contentFrameLayout, true);
//------------------------******************
        tv_error.setText(getString(R.string.enable_gps));
        ll_dummyContainer.setVisibility(View.VISIBLE);
        ll_body.setVisibility(View.GONE);
        ll_progressBar.setVisibility(View.GONE);
        bt_retry.setText(getString(R.string.settings));
        bt_retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, 100);
            }
        });

        //------------------------******************

    }

    private void showServiceError() {
        try {
            if (mLocationModelResponse != null && currentLanguage != null) {

                if (currentLanguage.equalsIgnoreCase("en"))
                    tv_error.setText(mLocationModelResponse.getErrorMsgEn());
                else
                    tv_error.setText(mLocationModelResponse.getErrorMsgAr());
            } else {
                tv_error.setText(getString(R.string.generice_error));

            }
            ll_progressBar.setVisibility(View.GONE);
            ll_dummyContainer.setVisibility(View.VISIBLE);
            ll_body.setVisibility(View.GONE);
            bt_retry.setText(getString(R.string.retry));
            bt_retry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onConsumeService();
                }
            });
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }
    }

    public void checkForGps() {
        showErrorFragment();
//        showErrorFragment();AlertDailogueHelper.showDaiogue(this, NearestStoreActivity.this, "Alert",
//
    }

    private void checkLocation() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager = (LocationManager) getActivity().getSystemService(getActivity().LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, true);
        locationManager.requestLocationUpdates(provider, 10000, 0, this);

        mlocation = locationManager.getLastKnownLocation(provider);
        if (mlocation == null) {
            mlocation = mMap.getMyLocation();
        }
        if (mlocation != null) {
            onLocationChanged(mlocation);
        }
    }


    @Override
    public void onConsumeService() {
        super.onConsumeService();
        ll_progressBar.setVisibility(View.VISIBLE);
        ll_body.setVisibility(View.GONE);
        ll_progressBar.setVisibility(View.GONE);
        if (mlocation != null) {
            LocationInputModel locationInputModel = new LocationInputModel();
            locationInputModel.setSignificant(ConstantUtils.wifi_hotspots);
            locationInputModel.setSize(ConstantUtils.number_of_locations);
            GeoLocation geoLocation = new GeoLocation();
            geoLocation.setLatitude(mlocation.getLatitude());
            geoLocation.setLongitude(mlocation.getLongitude());
            locationInputModel.setGeoLocation(geoLocation);
            locationInputModel.setAppVersion(appVersion);
            locationInputModel.setToken(token);
            locationInputModel.setOsVersion(osVersion);
            locationInputModel.setChannel(channel);
            locationInputModel.setDeviceId(deviceId);
            locationInputModel.setAuthToken(authToken);
            new GetNearestLocationService(this, locationInputModel);
        }


    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
//        progressID.setVisibility(View.GONE);
        try {
            if (isVisible()) {
//        onBackPressed();

                ll_progressBar.setVisibility(View.GONE);
                if (responseModel != null && responseModel.getResultObj() != null) {
                    mLocationModelResponse = (LocationModelResponse) responseModel.getResultObj();
                    if (mLocationModelResponse != null && mLocationModelResponse.getNearestLocationList().size() > 0) {
                        ll_body.setVisibility(View.VISIBLE);
                        ll_dummyContainer.setVisibility(View.GONE);
                        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_usage_wifi_hotspots);

                        drawMapPins();
                        createLocationList(mLocationModelResponse.getNearestLocationList());

                        if (mlocation == null) {
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(24.980223, 55.265673),
                                    ConstantUtils.zoomLevelWithoutLocation));
                        }

                    } else {
                        showServiceError();
                    }
                }
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }


    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
//        super.onErrorListener(responseModel);
        try {
            showServiceError();
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }
    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    public void drawMapPins() {
        mMap.clear();
        LatLng fLatLng = null;
        addMarkerAtCurrentPosition(mlocation);

        Marker temp = null;
        ll_dummyContainer.setVisibility(View.GONE);

        for (int i = 0; i < mLocationModelResponse.getNearestLocationList().size(); i++) {
            fLatLng = new LatLng(mLocationModelResponse.getNearestLocationList().get(i).getGeoLocation().getLatitude(),
                    mLocationModelResponse.getNearestLocationList().get(i).getGeoLocation().getLongitude());
            LatLng mLatLng = new LatLng(mLocationModelResponse.getNearestLocationList().get(i).getGeoLocation().getLatitude(),
                    mLocationModelResponse.getNearestLocationList().get(i).getGeoLocation().getLongitude());
            marker = new MarkerOptions().position(mLatLng);


//             marker.icon(BitmapDescriptorFactory.fromBitmap(etisaltLogo));
            String title = "";
            if (currentLanguage.equals("en")) {
                title = mLocationModelResponse.getNearestLocationList().get(i).getAddressEn();
            } else {
                title = mLocationModelResponse.getNearestLocationList().get(i).getAddressAr();
            }
            String dist = "-";
            if (mLocationModelResponse.getNearestLocationList().get(i).getDistance() != null) {
                dist = mLocationModelResponse.getNearestLocationList().get(i).getDistance() + " Km";
            }
            if (i == 0) {
                temp = mMap.addMarker(
                        marker.position(mLatLng)
                                .title(title)
                                .snippet(dist)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.location_icon_fill)));
                tv_name.setText(title);
                tv_distance.setText(dist);
                ll_marker.setVisibility(View.VISIBLE);
                highlightedMarker = temp;

            } else {
                temp = mMap.addMarker(
                        marker.position(mLatLng)
                                .title(title)
                                .snippet(dist)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.location_orange)));

            }
            mMarkerList.put(temp.getId(), i);
            mMarkersForSettingList.put(mLatLng, temp);


//            if(i==0){
//                tempMarker=temp;
//            }
        }
        fLatLng = new LatLng(mLocationModelResponse.getNearestLocationList().get(0).getGeoLocation().getLatitude(),
                mLocationModelResponse.getNearestLocationList().get(0).getGeoLocation().getLongitude());

        if (currentLanguage.equals("en")) {
            locationName = mLocationModelResponse.getNearestLocationList().get(0).getAddressEn();
        } else {
            locationName = mLocationModelResponse.getNearestLocationList().get(0).getAddressAr();
        }
//         distanceId.setText( getString(R.string.nearest_shop) + " " + distance + "  " +  getString(R.string.away));
        mLatitude = mLocationModelResponse.getNearestLocationList().get(0).getGeoLocation().getLatitude();
        mLongitude = mLocationModelResponse.getNearestLocationList().get(0).getGeoLocation().getLongitude();
//         locationNameId.setText( locationName);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(fLatLng,
                ConstantUtils.zoomLevel));

        mMap.setOnMarkerClickListener(this);
        mMap.setOnInfoWindowClickListener(this);
        mMap.setInfoWindowAdapter(new MyInfoWindowAdapter());
//        if(tempMarker!=null){
//            tempMarker.showInfoWindow();
//        }

    }


    class MyInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private final View myContentsView;

        MyInfoWindowAdapter() {
            myContentsView = getActivity().getLayoutInflater().inflate(R.layout.my_info_window, null);
        }

        @Override
        public View getInfoContents(Marker marker) {

            TextView tvTitle = ((TextView) myContentsView.findViewById(R.id.tv_name));
            tvTitle.setText(marker.getTitle());
            TextView tvSnippet = ((TextView) myContentsView.findViewById(R.id.tv_distance));
            tvSnippet.setText(marker.getSnippet());

            return myContentsView;
        }

        @Override
        public View getInfoWindow(Marker marker) {
            // TODO Auto-generated method stub
            return null;
        }

    }


    private void createLocationList(final List<NearestLocationList> locationList) {

        if (locationList != null && !locationList.isEmpty()) {

            if (locationList.size() > 0) {
                searches = new ArrayList<Search>(locationList.size());
                if (sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)) {
                    for (NearestLocationList nearestLocationList : locationList) {
                        searches.add(new Search(nearestLocationList.getAddressEn(), nearestLocationList.getGeoLocation().getLatitude(),
                                nearestLocationList.getGeoLocation().getLongitude()));


                    }
                } else {
                    for (NearestLocationList nearestLocationList : locationList) {

                        searches.add(new Search(nearestLocationList.getAddressAr(), nearestLocationList.getGeoLocation().getLatitude(),
                                nearestLocationList.getGeoLocation().getLongitude()));

                    }
                }
                if (searches != null) {
                    if (getActivity() != null) {

                        autoCompleteSearchAdapter = new AutoCompleteSearchAdapter(getActivity(), R.layout.list_item_search, searches);

                        et_autoComplete.setAdapter(autoCompleteSearchAdapter);

                        et_autoComplete.setThreshold(1);

                    }
                }
            }
            autoCompleteSearchAdapter.setOnItemClickListener(new AutoCompleteSearchAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position, Search search) {
                    TextView charac = (TextView) view.findViewById(R.id.searchNameLabel);
                    String text = charac.getText().toString();
                    CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_usage_search_wifi_hotspots);

                    et_autoComplete.setText("");
                    et_autoComplete.clearListSelection();
                    et_autoComplete.dismissDropDown();
                    hideKeyBoard();
                    //HomeFragment.hideKeyboard(et_search);
                    NearestLocationList nearestLocationList = getLocationObject(search, locationList);
                    if (mMap != null) {
                        LatLng latLng = new LatLng(nearestLocationList.getGeoLocation().getLatitude(),
                                nearestLocationList.getGeoLocation().getLongitude());
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, ConstantUtils.zoomLevel));
                        ll_marker.setVisibility(View.VISIBLE);
                        et_autoComplete.setVisibility(View.GONE);
                        if (currentLanguage.equals("en")) {
                            tv_name.setText(nearestLocationList.getAddressEn());
                        } else {
                            tv_name.setText(nearestLocationList.getAddressAr());
                        }
                        if (nearestLocationList.getDistance() != null) {
                            tv_distance.setText(nearestLocationList.getDistance() + getString(R.string.km));
                        }
                        highlightedMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.location_orange));
                        mMarkersForSettingList.get(latLng).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.location_icon_fill));
                        highlightedMarker = mMarkersForSettingList.get(latLng);
                    }
                }
            });
        }
    }

    public NearestLocationList getLocationObject(Search search, List<NearestLocationList> locationList) {
        for (int i = 0; i < locationList.size(); i++) {
            if ((locationList.get(i).getAddressEn().equalsIgnoreCase(search.getName()) ||
                    locationList.get(i).getAddressAr().equalsIgnoreCase(search.getName()))
                    && locationList.get(i).getGeoLocation().getLatitude().equals(search.getLatitude())
                    && locationList.get(i).getGeoLocation().getLongitude().equals(search.getLongitude())) {

                return locationList.get(i);

            }
        }
        return null;

    }

    public void addMarkerAtCurrentPosition(Location mLocation) {
        if (mLocation != null) {
            LatLng mLatLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
            MarkerOptions tempMarker = new MarkerOptions().position(mLatLng);
            mMap.addMarker(
                    tempMarker.position(mLatLng)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.locate_me_marker)));
        }
    }

}
