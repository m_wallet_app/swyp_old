package com.indy.views.fragments.gamification;

/**
 * Created by mobile on 04/09/2017.
 */

public interface OnPhotoItemClick {

    void onPhotoClicked(int index);
}
