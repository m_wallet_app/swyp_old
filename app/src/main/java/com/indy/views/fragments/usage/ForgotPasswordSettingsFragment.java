package com.indy.views.fragments.usage;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.indy.R;
import com.indy.models.ForgotPassword.ForgotPasswordInputModel;
import com.indy.models.ForgotPassword.ForgotPasswordOutputModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.services.ForgotPasswordService;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.ChangePasswordActivity;
import com.indy.views.activites.HelpActivity;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.indy.views.fragments.utils.MasterFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class ForgotPasswordSettingsFragment extends MasterFragment {
    private EditText emailAddress, mobileNo;
    private TextInputLayout til_email, til_mobileNumber;
    private ForgotPasswordInputModel forgotPasswordInputModel;
    private ForgotPasswordOutputModel forgotPasswordOutputModel;
    Button continueBtn, backImg, helpBtn;
    private String mobileNoTag = "MOBILENOTAG";
    private String codeTag = "CODETAG";
    View view;
    String fragmentType;


    public ForgotPasswordSettingsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_forgotpassword, container, false);
        initUI();
        setListeners();
        disableContinueBtn();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
        emailAddress = (EditText) view.findViewById(R.id.Email_user_profile);
        mobileNo = (EditText) view.findViewById(R.id.mobilenumber_user_profile);
        continueBtn = (Button) view.findViewById(R.id.ConformSetNewPasswordbtn);

        til_email = (TextInputLayout) view.findViewById(R.id.Email_text_input_layout);
        til_mobileNumber = (TextInputLayout) view.findViewById(R.id.mobielnumber_text_input_layout);

        if (fragmentType != null && fragmentType.equalsIgnoreCase(ConstantUtils.registerationActivity))

            continueBtn.setAlpha(0.5f);
        continueBtn.setEnabled(false);

        backImg = (Button) view.findViewById(R.id.backImg);
        helpBtn = (Button) view.findViewById(R.id.helpBtn);
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();

            }
        });
        helpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), HelpActivity.class);
                startActivity(intent);
            }
        });

        mobileNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (mobileNo.getText().toString().length() > 0) {
                    enableContinueBtn();
                } else {
                    disableContinueBtn();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void setListeners() {
        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid())
                    onConsumeService();
            }
        });

        mobileNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (mobileNo.getText().toString().trim().length() > 0) {
                    enableNextBtn();
                } else if (emailAddress.getText().toString().trim().length() > 0) {
                    enableNextBtn();
                } else {
                    disableNextBtn();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        emailAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (emailAddress.getText().toString().trim().length() > 0) {
                    enableNextBtn();
                } else if (mobileNo.getText().toString().trim().length() > 0) {
                    enableNextBtn();
                } else {
                    disableNextBtn();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        TextView headerTxt = (TextView) view.findViewById(R.id.titleTxt);
        headerTxt.setText(getString(R.string.forgot_password_spaced));
    }

    private void disableNextBtn() {
        continueBtn.setAlpha(0.5f);
        continueBtn.setEnabled(false);
    }

    private void enableNextBtn() {
        continueBtn.setAlpha(1.0f);
        continueBtn.setEnabled(true);
    }


    private void onContinue() {

        Fragment forgotPasswordCodeFragment = new ChangePasswordCodeFragment();
        Bundle bundle = new Bundle();
        bundle.putString(mobileNoTag, mobileNo.getText().toString().trim());
        bundle.putString(codeTag, forgotPasswordOutputModel.getGeneratedCode());
        forgotPasswordCodeFragment.setArguments(bundle);
        ((ChangePasswordActivity) getActivity()).replaceFragmnet(forgotPasswordCodeFragment, R.id.frameLayout, true);

    }


    private void onHelp() {
        Intent intent = new Intent(getActivity(), HelpActivity.class);
        startActivity(intent);
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        if (getArguments().get(ConstantUtils.fragmentType) != null) {

            ((ChangePasswordActivity) getActivity()).addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);

            forgotPasswordInputModel = new ForgotPasswordInputModel();
            forgotPasswordInputModel.setAppVersion(appVersion);
            forgotPasswordInputModel.setToken(token);
            forgotPasswordInputModel.setOsVersion(osVersion);
            forgotPasswordInputModel.setChannel(channel);
            forgotPasswordInputModel.setDeviceId(deviceId);
            forgotPasswordInputModel.setMsisdn(mobileNo.getText().toString().trim());
            forgotPasswordInputModel.setContact(mobileNo.getText().toString().trim());
            forgotPasswordInputModel.setLang(currentLanguage);

            forgotPasswordInputModel.setAuthToken(authToken);
            forgotPasswordInputModel.setLang(currentLanguage);
            new ForgotPasswordService(this, forgotPasswordInputModel);

        }
    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        ((ChangePasswordActivity) getActivity()).onBackPressed();
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
//        progressBar.setVisibility(View.GONE);
        if (isVisible()) {
            try {
                ((ChangePasswordActivity) getActivity()).onBackPressed();
                if(responseModel!=null && responseModel.getResultObj()!=null) {
                    forgotPasswordOutputModel = (ForgotPasswordOutputModel) responseModel.getResultObj();
                    if (forgotPasswordOutputModel != null) {
                        if (forgotPasswordOutputModel.getErrorCode() != null)
                            onError();
                        else
                            onContinue();
                    }
                }
            }catch (Exception ex) {
                if (ex != null) {
                    ex.printStackTrace();
                }
            }
        }
    }

    private void onError() {

        Fragment errorFragmnet = new ErrorFragment();
        Bundle bundle = new Bundle();
        if (currentLanguage != null) {
            if (currentLanguage.equalsIgnoreCase("en"))
                bundle.putString(ConstantUtils.errorString, forgotPasswordOutputModel.getErrorMsgEn());
            else
                bundle.putString(ConstantUtils.errorString, forgotPasswordOutputModel.getErrorMsgAr());
        }
        errorFragmnet.setArguments(bundle);
        ((ChangePasswordActivity) getActivity()).replaceFragmnet(errorFragmnet, R.id.frameLayout, true);


    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        ((ChangePasswordActivity) getActivity()).onBackPressed();
    }

    private boolean isValid() {
        boolean isValid = true;
        if (mobileNo.getText().toString().length() < 10) {
            isValid = false;
            setTextInputLayoutError(til_mobileNumber, getString(R.string.invalid_delivery_number));
//            til_mobileNumber.setError(getString(R.string.invalid_delivery_number));
        } else {
            String noStr = mobileNo.getText().toString().substring(0, 2);
            if (!noStr.equalsIgnoreCase("05")) {
                isValid = false;
                setTextInputLayoutError(til_mobileNumber, getString(R.string.invalid_delivery_number));
            } else {
                removeTextInputLayoutError(til_mobileNumber);
            }
        }

        return isValid;
    }

    private void showErrorFragment() {
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, getString(R.string.password_didnot_match));
        errorFragment.setArguments(bundle);
        ((ChangePasswordActivity) getActivity()).replaceFragmnet(errorFragment, R.id.frameLayout, true);
    }

    private void disableContinueBtn() {
        continueBtn.setAlpha(0.5f);
        continueBtn.setEnabled(false);
    }

    private void enableContinueBtn() {
        continueBtn.setAlpha(1.0f);
        continueBtn.setEnabled(true);
    }
}
