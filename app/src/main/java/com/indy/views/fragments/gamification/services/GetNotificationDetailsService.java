package com.indy.views.fragments.gamification.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.BaseServiceManger;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.gamification.models.getnotificationdetails.GetNotificationDetailsInput;
import com.indy.views.fragments.gamification.models.getnotificationdetails.GetNotificationDetailsOutput;
import com.indy.views.fragments.gamification.models.getnotificationslist.GetNotificationsListInput;
import com.indy.views.fragments.gamification.models.getnotificationslist.GetNotificationsListOutput;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Tohamy on 10/4/2017.
 */

public class GetNotificationDetailsService extends BaseServiceManger {

    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private GetNotificationDetailsInput getNotificationDetailsInput;

    public GetNotificationDetailsService(BaseInterface baseInterface, GetNotificationDetailsInput getNotificationDetailsInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.getNotificationDetailsInput = getNotificationDetailsInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<GetNotificationDetailsOutput> call = apiService.getNotificationDetails(getNotificationDetailsInput);
        call.enqueue(new Callback<GetNotificationDetailsOutput>() {
            @Override
            public void onResponse(Call<GetNotificationDetailsOutput> call, Response<GetNotificationDetailsOutput> response) {
                mBaseResponseModel.setServiceType(ServiceUtils.GET_NOTIFICATION_DETAILS);
                mBaseResponseModel.setResultObj(response.body());

                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (response.body().getErrorMsgEn() != null) {
//                        String urlForTag = call.request().url().toString();
//                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
//                        urlForTag = urlForTag.replace("/", "_");
//                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<GetNotificationDetailsOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.GET_NOTIFICATION_DETAILS);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}