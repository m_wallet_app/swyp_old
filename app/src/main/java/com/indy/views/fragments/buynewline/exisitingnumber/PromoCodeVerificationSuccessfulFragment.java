package com.indy.views.fragments.buynewline.exisitingnumber;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.indy.R;
import com.indy.controls.AdjustEvents;
import com.indy.models.onboarding_push_notifications.LogPromoCodeDetailsRequestModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.LogPromoCodeService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.utils.MaterialCheckBox;
import com.indy.views.fragments.buynewline.DeliveryDetailsAreaFragment;
import com.indy.views.fragments.buynewline.OrderFragment;
import com.indy.views.fragments.buynewline.ocr_flow.GetNodIDFragment;
import com.indy.views.fragments.utils.MasterFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class PromoCodeVerificationSuccessfulFragment extends MasterFragment {


    View rootView;
    private TextView tv_verify_code_response;
    private Button et_nextBtn;
    public static int promoCodeType = 0;
    public static String promoCodeDescription = "";
    public static String promoCode = "";

    public PromoCodeVerificationSuccessfulFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState);
        rootView = inflater.inflate(R.layout.fragment_promo_code_verification_successful, container, false);

        MaterialCheckBox materialCheckBox = (MaterialCheckBox) rootView.findViewById(R.id.fl_tick_animation);
        ImageView iv_tick = (ImageView) rootView.findViewById(R.id.iv_tick);
        CommonMethods.drawCircle(materialCheckBox, iv_tick);
        tv_verify_code_response = (TextView) rootView.findViewById(R.id.tv_verify_code_response);
        et_nextBtn = (Button) rootView.findViewById(R.id.nextBtn);
        if (getArguments() != null && getArguments().getString(ConstantUtils.KEY_PROMO_CODE_DESCRIPTION) != null && getArguments().getString(ConstantUtils.KEY_PROMO_CODE_DESCRIPTION).length() > 0) {
            promoCodeDescription = getArguments().getString(ConstantUtils.KEY_PROMO_CODE_DESCRIPTION);
            tv_verify_code_response.setText(promoCodeDescription);
            promoCodeType = getArguments().getInt(ConstantUtils.KEY_PROMO_CODE_TYPE);
            promoCode = getArguments().getString(ConstantUtils.KEY_PROMO_CODE);
        } else {

        }

        et_nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                regActivity.replaceFragmnet(new DeliveryDetailsAreaFragment(), R.id.frameLayout, true);
                LogPromoCodeDetailsRequestModel logPromoCodeDetailsRequestModel = new LogPromoCodeDetailsRequestModel();
                logPromoCodeDetailsRequestModel.setPromoCode(promoCode);
                logPromoCodeDetailsRequestModel.setPromoCodeDescription(promoCodeDescription);
                logPromoCodeDetailsRequestModel.setPromoCodeType("" + promoCodeType);
                logPromoCodeDetailsRequestModel.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
                new LogPromoCodeService(PromoCodeVerificationSuccessfulFragment.this, logPromoCodeDetailsRequestModel);
                sharedPrefrencesManger.setIsFromMigration(false);
                logEvent(AdjustEvents.ORDER_NEXT, new Bundle());
                regActivity.replaceFragmnet(new GetNodIDFragment(), R.id.frameLayout, true);
            }
        });
        return rootView;
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
    }
}
