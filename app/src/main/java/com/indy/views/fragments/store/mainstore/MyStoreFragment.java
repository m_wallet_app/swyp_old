package com.indy.views.fragments.store.mainstore;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.adapters.BundlesListAdapter;
import com.indy.controls.OnstoreClickInterface;
import com.indy.controls.ServiceUtils;
import com.indy.customviews.CustomTextView;
import com.indy.dbt.activities.DBTPagerActivity;
import com.indy.models.balance.BalanceInputModel;
import com.indy.models.balance.BalanceResponseModel;
import com.indy.models.bundles.BundleList;
import com.indy.models.bundles.BundleListOutPut;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.models.utils.MasterInputResponse;
import com.indy.new_membership.NewMembershipSuccessListener;
import com.indy.services.BundleListService;
import com.indy.services.GetUserBalanceService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.HistoryActivity;
import com.indy.views.activites.RechargeActivity;
import com.indy.views.activites.ShoppingCartActivity;
import com.indy.views.activites.SwpeMainActivity;
import com.indy.views.fragments.NewMembershipUpgradeBenefitsFragment;
import com.indy.views.fragments.rewards.RewardsFragment;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.MasterFragment;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by emad on 10/15/16.
 */

public class MyStoreFragment extends MasterFragment implements OnstoreClickInterface, SeekBar.OnSeekBarChangeListener, NewMembershipSuccessListener {
    private View view;
    private TextView rechargeTxt, transfereTxt, historyTxt, tv_totalItems, tv_totalCost;
    public static TextView tv_titleTxt;
    private MasterInputResponse masterInputResponse;
    private BundleListOutPut bundleListOutPut;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mGridLayoutManger;
    private LinearLayout ll_swype_cart;
    private BundleList bundleListItem = null;
    private SeekBar sb;
    private CustomTextView seekbartest;
    BundlesListAdapter bundlesListAdapter;
    ProgressBar progressBar;
    public static int totalCount = 0;
    public static double totalAmount = 0;
    public static double userBalance = 0;
    public static boolean isMembershipValid = false;
    public static List<BundleList> selectedItems;
    public List<BundleList> allItems;
    String serviceType = "-1";
    private TextView tv_noMembership;
    LinearLayout ll_recharge, ll_transfer, ll_history;
    BalanceResponseModel balanceResponseModel;
    private TextView memberShipCycleID, tv_membershipCycleText;
    private TextView usageTxtID;
    public static TextView tv_lastUpdated;
    public static MyStoreFragment myStoreFragmentInstance;
    private ImageView ic_scroll_up;

    public TextView tv_storeTitle;


    //-----TOOLBAR COLLAPSING

    Toolbar toolbar;
    CollapsingToolbarLayout collapsingToolbarLayout;
    AppBarLayout app_bar;
    LinearLayout ll_headerStore;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        totalCount = 0;
        totalAmount = 0;
        selectedItems = new ArrayList<BundleList>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_my_store, container, false);
        initUI();

        return view;
    }

    @Override
    public void initUI() {
        super.initUI();

        rechargeTxt = (TextView) view.findViewById(R.id.recharegID);
        transfereTxt = (TextView) view.findViewById(R.id.transferID);
        historyTxt = (TextView) view.findViewById(R.id.historyID);
        ll_swype_cart = (LinearLayout) view.findViewById(R.id.ll_swype_cart);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        tv_totalCost = (TextView) view.findViewById(R.id.tv_total_amount);
        tv_totalItems = (TextView) view.findViewById(R.id.tv_total_item_count);
        tv_titleTxt = (TextView) view.findViewById(R.id.tv_balance);
        tv_noMembership = (TextView) view.findViewById(R.id.tv_no_valid_membership);
        memberShipCycleID = (TextView) view.findViewById(R.id.memberShipCycleID);
        tv_membershipCycleText = (TextView) view.findViewById(R.id.tv_membership_cycle_text);
        ll_history = (LinearLayout) view.findViewById(R.id.ll_history);
        ll_recharge = (LinearLayout) view.findViewById(R.id.ll_recharge);
        ll_transfer = (LinearLayout) view.findViewById(R.id.ll_transfer);
        ic_scroll_up = (ImageView) view.findViewById(R.id.ic_scroll_up);
        ic_scroll_up.setBackground(getResources().getDrawable(R.drawable.ic_scroll));
        tv_lastUpdated = (TextView) view.findViewById(R.id.tv_last_updated);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
//        ((TextView) view.findViewById(R.id.usageTxtID)).setText(getString(R.string.store_capital_header_spaced));
        tv_lastUpdated.setText(getString(R.string.last_updated) + " " + sharedPrefrencesManger.getLastUpdatedBalance() +
                " " + getString(R.string.min_ago));
        if (sharedPrefrencesManger != null && sharedPrefrencesManger.getUserProfileObject().getMembershipData().getStatus() != null) {
//            Log.v("valid_member_ship", LoginFragment.loginOutputModel.getUserProfile().getMembershipData().getStatus() + "");
            if (sharedPrefrencesManger.getUserProfileObject().getMembershipData().getStatus() == ConstantUtils.inValidMembership
                    || sharedPrefrencesManger.getUserProfileObject().getMembershipData().getStatus() == 3 ||
                    sharedPrefrencesManger.getUserProfileObject().getMembershipData().getStatus() == 4) { // for inValid member ship
                tv_noMembership.setVisibility(View.VISIBLE);
                isMembershipValid = false;
            } else { // for invalid member ship
                tv_noMembership.setVisibility(View.GONE);
                isMembershipValid = true;
            }

        } else {
            tv_noMembership.setVisibility(View.GONE);
        }
        mRecyclerView.setHasFixedSize(true);
        mGridLayoutManger = new LinearLayoutManager(getActivity());
        mGridLayoutManger.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mGridLayoutManger);
        mRecyclerView.setHasFixedSize(true);
        sb = (SeekBar) view.findViewById(R.id.myseek);
        seekbartest = (CustomTextView) view.findViewById(R.id.slider_text);
        seekbartest.setText(getString(R.string.swyp_to_see_cart));
        sb.setVisibility(View.VISIBLE);

        sb.setProgress(3);
        Bundle bundle = new Bundle();

        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "store_screen");
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "store_screen");
        mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_STORE, bundle);
        selectedItems = new ArrayList<BundleList>();
        allItems = new ArrayList<BundleList>();
        if (sharedPrefrencesManger != null)
            if (sharedPrefrencesManger.getUserProfileObject().getMembershipData().getSubscribtionDaysLeft() != null) {
//                memberShipCycleID.setText(getString(R.string.membership_cycle) + "  " + sharedPrefrencesManger.getUserProfileObject().getMembershipData().getSubscribtionDaysLeft() + getString(R.string.days));
                String textView = sharedPrefrencesManger.getUserProfileObject().getMembershipData().getExpiryDate();
                textView += " 2016";
                memberShipCycleID.setText(" " + /*DateHelpers.getExpirtyDate(*/parseDateForExpiry(sharedPrefrencesManger.getUserProfileObject().getMembershipData().getExpiryDate())/*)*/);
                tv_membershipCycleText.setVisibility(View.VISIBLE);

            } else {
                tv_membershipCycleText.setVisibility(View.GONE);

                memberShipCycleID.setVisibility(View.GONE);
            }
        else
            tv_membershipCycleText.setVisibility(View.GONE);
        setListeners();
        serviceType = ServiceUtils.balanceServiceType;
        onConsumeService();
        serviceType = ServiceUtils.bundleStore;
        onConsumeService();
//        setDummyyStoreList();

        usageTxtID = (TextView) view.findViewById(R.id.usageTxtID);
        usageTxtID.setText(getString(R.string.store_capital_header_spaced));
        myStoreFragmentInstance = this;
        bundlesListAdapter = new BundlesListAdapter(allItems, getActivity(), this, (SwpeMainActivity) getActivity(), true);
        mRecyclerView.setAdapter(bundlesListAdapter);
//        tv_storeTitle = (TextView) view.findViewById(R.id.packages_title) ;
        //**-------------- COORDINATOR LAYOUT CODE -------------------------//
        ll_headerStore = (LinearLayout) view.findViewById(R.id.ll_header_store);
        toolbar = (Toolbar) view.findViewById(R.id.toolbarchangePackages);
        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((SwpeMainActivity) getActivity()).mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED)
                    ((SwpeMainActivity) getActivity()).expandMenu();
                else if (((SwpeMainActivity) getActivity()).mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED)
                    ((SwpeMainActivity) getActivity()).collapseMenu();
            }
        });

        LinearLayout lv_main_swip = (LinearLayout) view.findViewById(R.id.lv_main_swip);
        lv_main_swip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((SwpeMainActivity) getActivity()).mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED)
                    ((SwpeMainActivity) getActivity()).expandMenu();
                else if (((SwpeMainActivity) getActivity()).mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED)
                    ((SwpeMainActivity) getActivity()).collapseMenu();
            }
        });

        app_bar = (AppBarLayout) view.findViewById(R.id.appBarLayout);
        app_bar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

                int maxScroll = appBarLayout.getTotalScrollRange();
                float percentage = (float) Math.abs(verticalOffset) / (float) maxScroll;

                if (percentage >= 0.95) {
                    toolbar.setVisibility(View.VISIBLE);
                    toolbar.setAlpha(1f);

                    ll_headerStore.setVisibility(View.INVISIBLE);
                    //  tv_header_details.setVisibility(View.INVISIBLE);
//                    tv_storeTitle.setVisibility(View.VISIBLE);

                } else if (percentage >= 0.9) {
                    toolbar.setVisibility(View.VISIBLE);
                    toolbar.setAlpha(0.95f);
                    ll_headerStore.setVisibility(View.VISIBLE);
                    ll_headerStore.setAlpha(0.1f);
                } else if (percentage >= 0.8) {
                    toolbar.setVisibility(View.VISIBLE);
                    toolbar.setAlpha(0.7f);
                    ll_headerStore.setVisibility(View.VISIBLE);
                    ll_headerStore.setAlpha(0.2f);
                } else if (percentage >= 0.7) {
                    toolbar.setVisibility(View.VISIBLE);
                    toolbar.setAlpha(0.6f);
                    ll_headerStore.setVisibility(View.VISIBLE);
                    ll_headerStore.setAlpha(0.3f);
                } else if (percentage >= 0.6) {
                    toolbar.setVisibility(View.VISIBLE);
                    toolbar.setAlpha(0.5f);
                    ll_headerStore.setVisibility(View.VISIBLE);
                    ll_headerStore.setAlpha(0.4f);
                } else if (percentage >= 0.5) {
                    toolbar.setVisibility(View.VISIBLE);
                    toolbar.setAlpha(0.45f);
                    ll_headerStore.setVisibility(View.VISIBLE);
                    ll_headerStore.setAlpha(0.5f);
                } else if (percentage >= 0.4) {
                    toolbar.setVisibility(View.VISIBLE);
                    toolbar.setAlpha(0.4f);
                    ll_headerStore.setVisibility(View.VISIBLE);
                    ll_headerStore.setAlpha(0.6f);
                } else if (percentage >= 0.3) {
                    toolbar.setVisibility(View.VISIBLE);
                    toolbar.setAlpha(0.3f);
                    ll_headerStore.setVisibility(View.VISIBLE);
                    ll_headerStore.setAlpha(0.7f);
                } else if (percentage >= 0.2) {
                    toolbar.setVisibility(View.VISIBLE);
                    toolbar.setAlpha(0.2f);
                    ll_headerStore.setVisibility(View.VISIBLE);
                    ll_headerStore.setAlpha(0.8f);
                } else if (percentage >= 0.1) {
                    toolbar.setVisibility(View.VISIBLE);
                    toolbar.setAlpha(0.1f);
                    ll_headerStore.setVisibility(View.VISIBLE);
                    ll_headerStore.setAlpha(0.9f);
                } else {
//                    toolbar.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.black50));
                    ll_headerStore.setVisibility(View.VISIBLE);
                    toolbar.setVisibility(View.GONE);

//                    tv_storeTitle.setVisibility(View.GONE);
                    ;
                }


            }
        });


        //********************------------------END ----------------------//

        if (getArguments() != null && getArguments().containsKey(ConstantUtils.STORE_NAVIGATION_INDEX)) {
            if (getArguments().getString(ConstantUtils.STORE_NAVIGATION_INDEX).equalsIgnoreCase("1")) {
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_store_transfer_button);
                startActivity(new Intent(getActivity(), DBTPagerActivity.class));
            }
        }
    }


    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            if (isVisible()) {

                if (responseModel != null && responseModel.getResultObj() != null) {
                    if (responseModel.getServiceType() != null && responseModel.getServiceType().equals(ServiceUtils.balanceServiceType)) {
                        if (serviceType.equalsIgnoreCase(ServiceUtils.balanceServiceType)) {
                            getActivity().onBackPressed();
                        }
                        balanceResponseModel = (BalanceResponseModel) responseModel.getResultObj();
                        if (balanceResponseModel != null) {
                            if (balanceResponseModel.getErrorCode() != null) {
//                    showErrorFragment(balanceResponseModel.getErrorMsgEn());
                                Fragment errorFragment = new ErrorFragment();
                                if (((SwpeMainActivity) getActivity()) != null)
                                    ((SwpeMainActivity) getActivity()).replaceFragmnet(errorFragment, R.id.contentFrameLayout, true);
                            } else {
                                if (isVisible()) {

                                    userBalance = balanceResponseModel.getAmount();
                                    tv_lastUpdated.setText(getString(R.string.last_updated) + " " + sharedPrefrencesManger.getLastUpdatedBalance() +
                                            " " + getString(R.string.min_ago));
                                    String text = getResources().getString(R.string.your_balance_is_aed) + " " + balanceResponseModel.getAmount() + " " + getString(R.string.aed_arabic_only);
                                    CommonMethods.updateBalanceAndLastUpdated(text, getString(R.string.last_updated) + " " + sharedPrefrencesManger.getLastUpdatedBalance() +
                                            " " + getString(R.string.min_ago));
                                    onBalanceSuccess();
                                }
                            }
                        }

                    } else {
                        progressBar.setVisibility(View.GONE);
                        bundleListOutPut = (BundleListOutPut) responseModel.getResultObj();
                        if (isVisible()) {
                            if (bundleListOutPut != null)
                                if (bundleListOutPut.getErrorCode() != null)
                                    onGetBundlesErorr();
                                else
                                    onGetBundlesSucess();
                        }
                    }
//            serviceType = ServiceUtils.balanceServiceType;
//            onConsumeService();
                }
            }

        } catch (Exception ex) {


        }
    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        super.onUnAuthorizeToken(masterErrorResponse);
        if (getActivity() != null)
            getActivity().onBackPressed();

    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        try {
            if (responseModel != null && responseModel.getResultObj() != null) {
                if (responseModel.getServiceType() != null && responseModel.getServiceType().equals(ServiceUtils.balanceServiceType)) {
                    getActivity().onBackPressed();
                } else if (responseModel.getServiceType() != null && responseModel.getServiceType().equals(ServiceUtils.bundleStore)) {
                    progressBar.setVisibility(View.GONE);
                }
            }
        } catch (Exception ex) {

        }
//
    }

    private void onGetBundlesSucess() {
        if (bundleListOutPut.getBundleList() != null)
            if (bundleListOutPut.getBundleList().size() > 0) {
                allItems = bundleListOutPut.getBundleList();
//                BundleList bundleList = new BundleList();
//                bundleList.setViewType(1);
//                bundleList.setId("1");
//                bundleList.setPrice("100");
//                bundleList.setDescriptionEn("Dummy description en");
//                bundleList.setDescriptionAr("Dummy description ar");
//                bundleList.setCount(0);
//                bundleList.setNameEn("New hope");
//                allItems.add(bundleList);
                bundlesListAdapter = new BundlesListAdapter(allItems, getActivity(), this, (SwpeMainActivity) getActivity(), bundleListOutPut.isNewMembershipSubscribed());
                mRecyclerView.setAdapter(bundlesListAdapter);

            }

    }

    private void onGetBundlesErorr() {
        Fragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, bundleListOutPut.getErrorMsgEn());
        errorFragment.setArguments(bundle);
        ((SwpeMainActivity) getActivity()).replaceFragmnet(errorFragment, R.id.contentFrameLayout, true);
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        if (serviceType.equals(ServiceUtils.bundleStore)) {
//            ((SwpeMainActivity) getActivity()).showFrame();
//            Bundle bundle = new Bundle();
//            bundle.putString("fromRedeem", "fromRedeem");
//            LoadingFragmnet loadingFragmnet = new LoadingFragmnet();
//            loadingFragmnet.setArguments(bundle);
//            ((SwpeMainActivity) getActivity()).addFragmnet(loadingFragmnet, R.id.contentFrameLayout, true);
            progressBar.setVisibility(View.VISIBLE);
            masterInputResponse = new MasterInputResponse();
            masterInputResponse.setLang(currentLanguage);
            masterInputResponse.setImsi(sharedPrefrencesManger.getMobileNo());
            masterInputResponse.setAppVersion(appVersion);
            masterInputResponse.setMsisdn(sharedPrefrencesManger.getMobileNo());
            masterInputResponse.setOsVersion(osVersion);
            masterInputResponse.setChannel(channel);
            masterInputResponse.setToken(sharedPrefrencesManger.getToken());
            masterInputResponse.setDeviceId(deviceId);
            masterInputResponse.setAuthToken(authToken);
            masterInputResponse.setSignificant("bundle_list_v2");
            new BundleListService(this, masterInputResponse);
        } else if (serviceType.equals(ServiceUtils.balanceServiceType)) {

            BalanceInputModel balanceInputModel = new BalanceInputModel();
            balanceInputModel.setAppVersion(appVersion);
            balanceInputModel.setToken(token);
            balanceInputModel.setOsVersion(osVersion);
            balanceInputModel.setChannel(channel);
            balanceInputModel.setDeviceId(deviceId);
            balanceInputModel.setLang(currentLanguage);
            balanceInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
            balanceInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
            balanceInputModel.setAuthToken(authToken);
            balanceInputModel.setBillingPeriod(CommonMethods.getBillingPeriod());
            new GetUserBalanceService(this, balanceInputModel, getActivity());
        }
    }

    private void setListeners() {
        if (sharedPrefrencesManger != null)
            if (sharedPrefrencesManger.getBalanceTransfarerStatuse() == true)
                enablellTransfer();
            else
                disablellTransfer();
        ll_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_store_history_button);

                startActivity(new Intent(getActivity(), HistoryActivity.class));

            }
        });

        ll_recharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_store_recharge_account_button);
                startActivity(new Intent(getActivity(), RechargeActivity.class));

            }
        });
        sb.setOnSeekBarChangeListener(this);

        sb.setMinimumHeight(50);
        sb.getProgressDrawable().setBounds(sb.getLeft(), sb.getTop(), sb.getRight(), sb.getBottom());
    }

    private void enablellTransfer() {
        ll_transfer.setAlpha(1.0f);
        ll_transfer.setEnabled(true);
        ll_transfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_store_transfer_button);
                startActivity(new Intent(getActivity(), DBTPagerActivity.class));
            }
        });
    }

    private void disablellTransfer() {
        ll_transfer.setOnClickListener(null);
        ll_transfer.setAlpha(0.5f);
        ll_transfer.setEnabled(false);

    }

    @Override
    public void onItemClick(BundleList bundleList, int position) {
        this.bundleListItem = bundleList;
        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_store_add_item_button);
        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_store_add_item_button + "_" + bundleList.getBundleName());

        if (bundleList.isNewMembershipPack()) {
            NewMembershipUpgradeBenefitsFragment newMembershipUpgradeBenefitsFragment = new NewMembershipUpgradeBenefitsFragment();
            newMembershipUpgradeBenefitsFragment.setNewMembershipSuccessListener(MyStoreFragment.this);
            ((SwpeMainActivity) getActivity()).replaceFragmnet(newMembershipUpgradeBenefitsFragment, R.id.contentFrameLayout, true);

        } else {
            if (bundleList.getCount() < bundleList.getMaxCount()) {
                bundleList.setSelected(true);
                totalCount++;
                double tempDouble = Double.parseDouble(bundleList.getPrice());
                totalAmount += tempDouble;
                bundleList.setCount(bundleList.getCount() + 1);

                bundlesListAdapter.updateItem(bundleList, position);
                int index = isAddedToList(bundleList);
                if (index >= 0) {
                    selectedItems.set(index, bundleList);
                } else {
                    selectedItems.add(bundleList);
                }
            }

            if (totalCount > 0) {
                ll_swype_cart.setVisibility(View.VISIBLE);
                tv_totalCost.setText(getString(R.string.aed) + " " + totalAmount);
                if (totalCount > 1) {
                    tv_totalItems.setText(totalCount + " " + getString(R.string.items));
                } else {
                    tv_totalItems.setText(totalCount + " " + getString(R.string.items));
                }
            }
        }
    }

    public int isAddedToList(BundleList bundleList) {
        for (int i = 0; i < selectedItems.size(); i++) {
            if (selectedItems.get(i).getId().equalsIgnoreCase(bundleList.getId())) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (tv_lastUpdated != null)
                tv_lastUpdated.setText(getString(R.string.last_updated) + " " + sharedPrefrencesManger.getLastUpdatedBalance() +
                        " " + getString(R.string.min_ago));
            if (selectedItems != null && selectedItems.size() > 0) {
                if (sb != null) {
                    sb.setProgress(5);
                    seekbartest.setText(getString(R.string.swyp_to_see_cart));
                    seekbartest.setVisibility(View.VISIBLE);
                }

                for (int i = 0; i < selectedItems.size(); i++) {

                    allItems.set(allItems.indexOf(selectedItems.get(i)), selectedItems.get(i));
                }
                bundlesListAdapter.notifyDataSetChanged();

                for (int i = 0; i < selectedItems.size(); i++) {
                    if (selectedItems.get(i).getCount() < 1) {
                        selectedItems.remove(i);
                        i--;
                    }
                }
                updateTotal();
            } else {
                if (ll_swype_cart != null) {
                    ll_swype_cart.setVisibility(View.GONE);
                    resetCart();
                }
                if (allItems != null) {
                    for (int i = 0; i < allItems.size(); i++) {
                        allItems.get(i).setCount(0);
                        allItems.get(i).setSelected(false);
                    }
                    bundlesListAdapter.notifyDataSetChanged();
                }
                updateTotal();

            }
        } catch (Exception ex) {

        }
    }

    public void resetCart() {
        sb.setProgress(5);

        seekbartest.setVisibility(View.VISIBLE);
        seekbartest.setText(getString(R.string.swyp_to_see_cart));
    }

    public void updateTotal() {
        totalCount = 0;
        totalAmount = 0;
        for (int i = 0; i < selectedItems.size(); i++) {
            double temp = Double.parseDouble(selectedItems.get(i).getPrice());
            temp = temp * selectedItems.get(i).getCount();
            totalAmount += temp;

            totalCount += selectedItems.get(i).getCount();
        }

        tv_totalCost.setText(getString(R.string.aed) + " " + totalAmount);
        if (totalCount > 1) {
            tv_totalItems.setText(totalCount + " " + getString(R.string.items));
        } else {
            tv_totalItems.setText(totalCount + " " + getString(R.string.item));
        }

        if (totalCount > 0) {
            ll_swype_cart.setVisibility(View.VISIBLE);
        } else {
            ll_swype_cart.setVisibility(View.GONE);

        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (seekBar.getProgress() < 5) {
            seekBar.setProgress(5);
        } else if (seekBar.getProgress() > 94) {
            seekBar.setProgress(95);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        seekbartest.setVisibility(View.INVISIBLE);

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        if (seekBar.getProgress() < 80) {
            resetCart();

        } else {
            seekBar.setProgress(95);
            hideKeyBoard();
            CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_store_swyp_to_checkout);
            for (int i = 0; i < selectedItems.size(); i++) {
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_store_swyp_to_checkout + "_" + selectedItems.get(i).getBundleName());
            }
            Intent intent = new Intent(getActivity(), ShoppingCartActivity.class);
            startActivity(intent);
        }

    }

    private void onBalanceSuccess() {
        String text = getResources().getString(R.string.your_balance_is_aed) + " " + balanceResponseModel.getAmount() + " " + getString(R.string.aed_arabic_only);
        tv_titleTxt.setText(text);
    }

    public void onSlideUp() {
        if (usageTxtID != null) {
            usageTxtID.setVisibility(View.VISIBLE);
            if (isVisible())
                ic_scroll_up.setBackground(getResources().getDrawable(R.drawable.ic_scroll));
        }
    }

    public void onSlideDown() {
        if (usageTxtID != null) {
            usageTxtID.setVisibility(View.INVISIBLE);
            if (isVisible())
                ic_scroll_up.setBackground(getResources().getDrawable(R.drawable.ic_up));
        }
    }

    public void refreshBalance() {
        serviceType = ServiceUtils.refreshBalance;
        BalanceInputModel balanceInputModel = new BalanceInputModel();
        balanceInputModel.setAppVersion(appVersion);
        balanceInputModel.setToken(token);
        balanceInputModel.setOsVersion(osVersion);
        balanceInputModel.setChannel(channel);
        balanceInputModel.setDeviceId(deviceId);
        balanceInputModel.setLang(currentLanguage);
        balanceInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
        balanceInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        balanceInputModel.setAuthToken(authToken);
        balanceInputModel.setBillingPeriod(CommonMethods.getBillingPeriod());

        new GetUserBalanceService(this, balanceInputModel, getActivity());
    }

    public String parseDateForExpiry(String date) {
        Date dateToPass;

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM, HH:mm yyyy");
        try {
            Date formattedDate;

            formattedDate = simpleDateFormat.parse(date);
            simpleDateFormat = new SimpleDateFormat("dd MMM, HH:mm");
            return simpleDateFormat.format(formattedDate);
        } catch (Exception ex) {
            dateToPass = null;
        }
        if (date != null) {
            return date;
        } else {
            return "";
        }
    }

    @Override
    public void onOKPressed() {
        ((SwpeMainActivity)getActivity()).onBackPressed();
    }
}
