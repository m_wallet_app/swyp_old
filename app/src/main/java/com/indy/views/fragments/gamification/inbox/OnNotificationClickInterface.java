package com.indy.views.fragments.gamification.inbox;

import com.indy.views.fragments.gamification.models.getnotificationslist.NotificationModel;

public interface OnNotificationClickInterface {
    void onNotificationClick(NotificationModel notificationModel, int index);
}
