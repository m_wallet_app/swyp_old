package com.indy.views.fragments.gamification.challenges;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.indy.R;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.utils.GPSTracker;
import com.indy.views.activites.RechargeActivity;
import com.indy.views.activites.SwpeMainActivity;
import com.indy.views.fragments.gamification.models.serviceInputOutput.DoCheckInInputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.DoCheckInOutputModel;
import com.indy.views.fragments.gamification.services.DoCheckInService;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.indy.views.fragments.utils.MasterFragment;

import static android.content.Context.LOCATION_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 */
public class CheckInFragment extends MasterFragment {

    View rootView;
    LinearLayout ll_noMembership;
    FrameLayout frameLayout;
    TextView bt_getMemberhsip, bt_checkIn, tv_viewMap, tv_membership_cycle;
    GPSTracker gpsTracker;
    public static final int LOCATION_REQUEST = 1340;
    public static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };

    Location mLocation;

    public CheckInFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_check_in, container, false);
        initUI();
        return rootView;
    }

    @Override
    public void initUI() {
        super.initUI();

        frameLayout = (FrameLayout) rootView.findViewById(R.id.frameLayout);
        ll_noMembership = (LinearLayout) rootView.findViewById(R.id.ll_no_membership);
        bt_getMemberhsip = (TextView) rootView.findViewById(R.id.membershipBtn);
        bt_checkIn = (TextView) rootView.findViewById(R.id.btn_checkin);
        tv_viewMap = (TextView) rootView.findViewById(R.id.tv_view_map);
        tv_membership_cycle = (TextView) rootView.findViewById(R.id.tv_membership_cycle);
        gpsTracker = new GPSTracker(getActivity());

        bt_getMemberhsip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), RechargeActivity.class);
                startActivity(intent);
            }
        });


        bt_checkIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //call service here
                if (mLocation != null) {
                    callChcekInService();
                } else {
                    gpsTracker = new GPSTracker(getActivity());
                    checkPermission();
                }

            }
        });

        tv_viewMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //start map activity here.
                startActivity(new Intent(getActivity(), CheckInLocationsActivity.class));
            }
        });

        if (CommonMethods.isMembershipValid(getActivity())) {

        } else {
            showNoMembershipError();
        }

        checkPermission();
    }



    private void showNoMembershipError() {
        ll_noMembership.setVisibility(View.VISIBLE);
        if (sharedPrefrencesManger.isVATEnabled()) {
            Double price = Double.parseDouble(sharedPrefrencesManger.getAfterLoginPrice());
            tv_membership_cycle.setText(getString(R.string.you_dont_have_a_swyp_membership));

        } else {
            tv_membership_cycle.setText(getString(R.string.you_dont_have_a_swyp_membership));
        }
    }

    private void showCheckInView() {
        ll_noMembership.setVisibility(View.GONE);
        frameLayout.setVisibility(View.GONE);
    }

    private void callChcekInService() {
        try {
            LoadingFragmnet loadingFragmnet = new LoadingFragmnet();
            Bundle bundle = new Bundle();
            bundle.putString(ConstantUtils.loadingTitle, getString(R.string.exploring));
            loadingFragmnet.setArguments(bundle);
            ((SwpeMainActivity) getActivity()).addFragmnet(loadingFragmnet, R.id.contentFrameLayout, true);

            DoCheckInInputModel doCheckInInputModel = new DoCheckInInputModel();
            doCheckInInputModel.setChannel(channel);
            doCheckInInputModel.setLang(currentLanguage);
            doCheckInInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
            doCheckInInputModel.setAppVersion(appVersion);
            doCheckInInputModel.setAuthToken(authToken);
            doCheckInInputModel.setDeviceId(deviceId);
            doCheckInInputModel.setToken(token);
            doCheckInInputModel.setUserSessionId(sharedPrefrencesManger.getUserSessionId());
            doCheckInInputModel.setUserId(sharedPrefrencesManger.getUserId());
            doCheckInInputModel.setApplicationId(ConstantUtils.INDY_TALOS_ID);
            doCheckInInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
            doCheckInInputModel.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
            doCheckInInputModel.setOsVersion(osVersion);
//            doCheckInInputModel.setLatitude(mLocation.getLatitude());
//            doCheckInInputModel.setLongitude(mLocation.getLongitude());

            doCheckInInputModel.setLatitude(24.48481);
            doCheckInInputModel.setLongitude(54.35996);

            new DoCheckInService(this, doCheckInInputModel);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    DoCheckInOutputModel doCheckInOutputModel;

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            if (responseModel != null && responseModel.getResultObj() != null) {
                doCheckInOutputModel = (DoCheckInOutputModel) responseModel.getResultObj();

                if (doCheckInOutputModel != null
                        && doCheckInOutputModel.getSuccess() != null && doCheckInOutputModel.getSuccess()) {
                    onCheckInSuccess();
                } else {
                    onCheckInFailure();
                }
            } else {
                onCheckInFailure();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        try {
            onCheckInFailure();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void onCheckInSuccess() {
        getActivity().onBackPressed();
        SuccessfulCheckInFragment successfulCheckInFragment = new SuccessfulCheckInFragment();

        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.title_checkin_success, getString(R.string.check_in_successful));

        bundle.putString(ConstantUtils.subtitle_checkin_success, doCheckInOutputModel.getConstraintMessage());
        if (doCheckInOutputModel.getLotteryReward() != null) {
            bundle.putString(ConstantUtils.reward_type_checkin_success, doCheckInOutputModel.getLotteryReward().getType() + "");
            bundle.putString(ConstantUtils.raffle_tickets_won, doCheckInOutputModel.getLotteryReward().getRaffleTicketsWon() + "");
            bundle.putString(ConstantUtils.raffle_tickets_total, doCheckInOutputModel.getLotteryReward().getRaffleTicketsTotal() + "");
            bundle.putParcelable(ConstantUtils.lottery_reward, doCheckInOutputModel.getLotteryReward());
        }
        successfulCheckInFragment.setArguments(bundle);
        ((SwpeMainActivity) getActivity()).addFragmnet(successfulCheckInFragment, R.id.contentFrameLayout, true);
    }

    private void onCheckInFailure() {
        getActivity().onBackPressed();
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();

        if (doCheckInOutputModel != null && doCheckInOutputModel.getErrorCode() != null) {
            if (currentLanguage.equalsIgnoreCase(ConstantUtils.lang_english))
                bundle.putString(ConstantUtils.errorString, doCheckInOutputModel.getErrorMsgEn());
            else
                bundle.putString(ConstantUtils.errorString, doCheckInOutputModel.getErrorMsgAr());
            switch (doCheckInOutputModel.getErrorCode()) {
                case "3000010": {
                    bundle.putString(ConstantUtils.errorTitle, getString(R.string.check_in_limit_reached));
                    break;
                }
                case "3000011": {
                    bundle.putString(ConstantUtils.errorTitle, getString(R.string.already_check_in));
                    bundle.putBoolean(ConstantUtils.VIEW_MAP_VISIBLE, true);
                    bundle.putString(ConstantUtils.NEGATIVE_BUTTON_TEXT, getString(R.string.view_map));
                    break;
                }
                case "3000012": {
                    bundle.putString(ConstantUtils.errorTitle, getString(R.string.nothing_around_you));
                    bundle.putString(ConstantUtils.errorString, getString(R.string.it_appears_nothing_hidden));
                    bundle.putBoolean(ConstantUtils.VIEW_MAP_VISIBLE, false);
                    bundle.putString(ConstantUtils.NEGATIVE_BUTTON_TEXT, getString(R.string.view_map));


                    break;
                }
                default: {
                    bundle.putString(ConstantUtils.errorTitle, getString(R.string.error_));
                    break;
                }
            }
        } else {
            bundle.putString(ConstantUtils.errorString, getString(R.string.error_));
            bundle.putString(ConstantUtils.errorTitle, getString(R.string.error_txt));
        }
        errorFragment.setArguments(bundle);
        ((SwpeMainActivity) getActivity()).addFragmnet(errorFragment, R.id.contentFrameLayout, true);
    }

    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                this.requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            } else {
                isGPSEnabled();
            }
        } else {
            isGPSEnabled();
        }
    }


    private boolean canAccessLocation() {
        return Build.VERSION.SDK_INT < 23 || PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case LOCATION_REQUEST:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    isGPSEnabled();
                } else {
                    checkPermission();
                }
                break;
        }
    }

    int counter = 0;

    private void isGPSEnabled() {
        counter++;
        LocationManager locationManager = (LocationManager)
                getActivity().getSystemService(LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {// gps is disable
            showGPSError();
        } else {
            if (gpsTracker == null) {
                gpsTracker = new GPSTracker(getActivity());
            }
            if (gpsTracker.canGetLocation()) {
                mLocation = gpsTracker.getLocation();
                if (mLocation != null) {

                } else {
                    Toast.makeText(getActivity(), "Fetching Location, please wait..", Toast.LENGTH_SHORT).show();
                    try {
                        Thread.sleep(1500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    mLocation = gpsTracker.getLocation();
//                    Toast.makeText(getActivity(), "Latlong is got: " + mLocation.getLatitude(), Toast.LENGTH_SHORT).show();

                }
            } else {
                try {
                    if (counter < 2) {
                        Thread.sleep(3000);
                    } else {
                        Toast.makeText(getActivity(), "Cannot get location, please try again.", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    isGPSEnabled();
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }

            }
//                    chec/kLocation();
        }
//                checkLocation();


    }

    private void showGPSError() {
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, getString(R.string.enable_gps_checkin));
        bundle.putString(ConstantUtils.settingsButtonEnabled, "true");
        bundle.putString(ConstantUtils.buttonTitle, getString(R.string.enable));
        bundle.putBoolean("cancel", true);
        errorFragment.setArguments(bundle);
        ((SwpeMainActivity) getActivity()).addFragmnet(errorFragment, R.id.contentFrameLayout, true);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 100:
                getActivity().onBackPressed();
                LocationManager locationManager = (LocationManager)
                        getActivity().getSystemService(getActivity().LOCATION_SERVICE);
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    showGPSError();
                } else {
                    isGPSEnabled();
                }
                break;
            default:
                break;
        }
    }
}
