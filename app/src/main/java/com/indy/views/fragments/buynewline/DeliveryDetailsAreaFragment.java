package com.indy.views.fragments.buynewline;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.controls.AdjustEvents;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.LogApplicationFlowService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.utils.MasterFragment;

import java.util.Arrays;
import java.util.List;

import static com.indy.views.activites.RegisterationActivity.logEventInputModel;

/**
 * Created by emad on 9/21/16.
 */

public class DeliveryDetailsAreaFragment extends MasterFragment implements AdapterView.OnItemSelectedListener {

    private View view;
    private Spinner selectCitySpinner;
    private Button continueBtn;
    private EditText enterareaID;
    public static String contactNoStr, areaStr, addressLine1Str, emirateStr, cityCodeStr;
    private TextInputLayout  enterareaInputLayoutID;
    private static int position;
    private CharSequence  areaCharSeq;
    public static DeliveryDetailsAreaFragment deliveryDetailsAreaFragment;
    private TextView tv_choose_city;
    List<String> cityCodeList;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_delivery_details, container, false);
        initUI();
        loadSpinnerItems();
        setListeners();
        disableNextBtn();
        changeNextState();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
        regActivity.showHeaderLayout();
        regActivity.setHeaderTitle("");
        selectCitySpinner = (Spinner) view.findViewById(R.id.selectCitySpinner);
        continueBtn = (Button) view.findViewById(R.id.continueId);
        enterareaID = (EditText) view.findViewById(R.id.enterareaID);
        enterareaInputLayoutID = (TextInputLayout) view.findViewById(R.id.enterareaInputLayoutID);
        selectCitySpinner.setOnItemSelectedListener(this);
        deliveryDetailsAreaFragment = this;
        tv_choose_city = (TextView) view.findViewById(R.id.tv_choose_city);
        tv_choose_city.setVisibility(View.INVISIBLE);
        cityCodeList = Arrays.asList(getResources().getStringArray(R.array.uae_cities_code));
    }

    private void loadSpinnerItems() {
        selectCitySpinner = (Spinner) view.findViewById(R.id.selectCitySpinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.uae_cities, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        selectCitySpinner.setAdapter(adapter);
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
    }

    void setListeners() {
        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard();
//                if (isValid()) {
//                    contactNoStr = mobileNo.getText().toString().trim();
                    areaStr = enterareaID.getText().toString().trim();
                    CommonMethods.logFirebaseEvent(mFirebaseAnalytics,ConstantUtils.TAGGING_delivery_info_entered);
                    logEvent(AdjustEvents.ADJUST_CHOOSE_CITY, new Bundle());
                    regActivity.replaceFragmnet(new DeliveryDetailsAddress(), R.id.frameLayout, true);
//                }
            }
        });
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        this.position = position;
        if ((parent.getChildAt(0)) != null) {


            CommonMethods.logFirebaseEvent(mFirebaseAnalytics,ConstantUtils.TAGGING_delivery_city_selected);
            ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
            if (position != 0) {
                cityCodeStr = cityCodeList.get(position);

                emirateStr = ((TextView) parent.getChildAt(0)).getText().toString();
                Log.v("emirate_ID", emirateStr + " test");
                tv_choose_city.setVisibility(View.VISIBLE);

            }else{
                tv_choose_city.setVisibility(View.INVISIBLE);

            }
            if (areaCharSeq != null) {
                if ( areaCharSeq.length() == 0 || position == 0) {
                    disableNextBtn();
                } else {
                    emirateStr = ((TextView) parent.getChildAt(0)).getText().toString();
                    enableNextBtn();
                }
            } else {
                disableNextBtn();
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        if ((parent.getChildAt(0)) != null)
            ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
    }

    private void changeNextState() {

//        if (mobileNo.getText().toString().length() == 0
//                || enterareaID.getText().toString().length() == 0)
//            return false;
//        else
//            return true;
        enterareaID.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                areaCharSeq = s;
                    if (s.length() == 0 || position == 0) {
                        disableNextBtn();
                    }else {
                        enableNextBtn();
                    }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
            }
        });

    }

    private void enableNextBtn() {
        continueBtn.setAlpha(1f);
        continueBtn.setEnabled(true);
    }

    private void disableNextBtn() {
        continueBtn.setAlpha(.5f);
        continueBtn.setEnabled(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(NewNumberFragment.isTimerActive && CommonMethods.isTimerUp(sharedPrefrencesManger.getTimerValue())){
            Fragment[] myFragments = {OrderFragment.orderFragmentInstance, NewNumberFragment.loadingFragmnet,
                    NewNumberFragment.newNumberFragment,
                    DeliveryDetailsAreaFragment.deliveryDetailsAreaFragment};
            regActivity.removeFragment(myFragments);
            regActivity.replaceFragmnet(new NewNumberFragment(),R.id.frameLayout,true);
            NewNumberFragment.isTimerActive=false;

        }
    }

    private void logEvent() {
        logEventInputModel.setActionTransactionId(sharedPrefrencesManger.getTempAppId());
        logEventInputModel.setScreenId(ConstantUtils.SCREEN_ID_1003);
        logEventInputModel.setCity(emirateStr);
        logEventInputModel.setArea(areaStr);
        new LogApplicationFlowService(this, logEventInputModel);
    }

}
