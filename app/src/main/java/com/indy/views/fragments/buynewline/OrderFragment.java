package com.indy.views.fragments.buynewline;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.controls.AdjustEvents;
import com.indy.controls.ServiceUtils;
import com.indy.models.onboarding_push_notifications.LogPromoCodeDetailsRequestModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.verifyPromoCode.VerifyPromoCodeInputModel;
import com.indy.models.verifyPromoCode.VerifyPromoCodeOutputResponse;
import com.indy.services.LogApplicationFlowService;
import com.indy.services.LogPromoCodeService;
import com.indy.services.VerifyPromoCodeService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.buynewline.exisitingnumber.PromoCodeVerificationSuccessfulFragment;
import com.indy.views.fragments.buynewline.ocr_flow.GetNodIDFragment;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.indy.views.fragments.utils.MasterFragment;

import static com.indy.utils.ConstantUtils.TAGGING_onboarding_select_number;
import static com.indy.utils.ConstantUtils.TAGGING_package_selected;
import static com.indy.views.activites.RegisterationActivity.logEventInputModel;


/**
 * Created by emad on 9/21/16.
 */

public class OrderFragment extends MasterFragment {
    private View view;
    private Button nextBtn;
    private RadioButton paidRBtn, freeRBtn;
    private EditText et_promoCode;
    private TextView tv_membership_price, tv_sim_price, tv_exclude_vat_sim, tv_exclude_vat_membership, promo_desc;
    public static String orderTypeStr;
    public static OrderFragment orderFragmentInstance;
    public static String shippingMethodStr = "free";
    public static String promoCode = "";
    public static double totalAmount = 0;
    public static double totalAmountSIM = 0;
    public VerifyPromoCodeOutputResponse verifyPromoCodeOutputResponse;
    private boolean fromNotification = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragmnet_order, container, false);
        if (getArguments() != null) {
            if (getArguments().containsKey(ConstantUtils.KEY_FROM_NOTIFICATION)) {
                fromNotification = getArguments().getBoolean(ConstantUtils.KEY_FROM_NOTIFICATION);
            }
        }
        initUI();
        setListeners();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
//        if (NewNumberFragment.isTimerActive && CommonMethods.isTimerUp(sharedPrefrencesManger.getTimerValue())) {
//            Fragment[] myFragments = {NewNumberFragment.newNumberFragment, orderFragmentInstance, NewNumberFragment.loadingFragmnet};
//            regActivity.removeFragment(myFragments);
//            regActivity.replaceFragmnet(new NewNumberFragment(), R.id.frameLayout, true);
//            NewNumberFragment.isTimerActive = false;
//        }
    }


    @Override
    public void initUI() {
        super.initUI();
        regActivity.showHeaderLayout();
        regActivity.setHeaderTitle("");
        orderFragmentInstance = this;
        nextBtn = (Button) view.findViewById(R.id.nextBtn);
        paidRBtn = (RadioButton) view.findViewById(R.id.paidRbtn);
        freeRBtn = (RadioButton) view.findViewById(R.id.freeBtn);
        et_promoCode = (EditText) view.findViewById(R.id.et_promoCode);
        promo_desc = (TextView) view.findViewById(R.id.promo_desc);
        if (fromNotification) {
            et_promoCode.setText("" + promoCode);
        }
        tv_membership_price = (TextView) view.findViewById(R.id.tv_membership_price);
        tv_sim_price = (TextView) view.findViewById(R.id.tv_sim_price);
        tv_exclude_vat_sim = (TextView) view.findViewById(R.id.tv_exclude_vat_sim);
        tv_exclude_vat_membership = (TextView) view.findViewById(R.id.tv_exclude_vat_membership);
        if (sharedPrefrencesManger.getMembershipPrice().equalsIgnoreCase("0")) {
            tv_membership_price.setText(getActivity().getResources().getString(R.string.free));
            tv_exclude_vat_membership.setVisibility(View.GONE);
            et_promoCode.setEnabled(false);
            et_promoCode.setAlpha(0.5f);
            promo_desc.setVisibility(View.VISIBLE);
        } else {
            et_promoCode.setEnabled(true);
            et_promoCode.setAlpha(1f);
            promo_desc.setVisibility(View.GONE);
            tv_membership_price.setText(getString(R.string.aed) + " " + sharedPrefrencesManger.getMembershipPrice());
        }
        if (sharedPrefrencesManger.getSIMPrice().equalsIgnoreCase("0")) {
            tv_exclude_vat_sim.setVisibility(View.GONE);
            tv_sim_price.setText(getActivity().getResources().getString(R.string.free));
        } else {
            tv_sim_price.setText(getString(R.string.aed) + " " + sharedPrefrencesManger.getSIMPrice());
        }

        paidRBtn.setChecked(true);
        if (!fromNotification) {
            orderTypeStr = "1";
        }
//        if (sharedPrefrencesManger.isVATEnabled()) {
//            ((TextView) view.findViewById(R.id.tv_vat_tax)).setVisibility(View.VISIBLE);
//        } else {
//
//            ((TextView) view.findViewById(R.id.tv_vat_tax)).setVisibility(View.GONE);
//        }

    }


    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        if (responseModel != null && responseModel.getServiceType() != null && responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.VERIFY_PROMO_CODE)) {
//            if (NewNumberFragment.isTimerActive && CommonMethods.isTimerUp(sharedPrefrencesManger.getTimerValue())) {
//                Fragment[] myFragments = {new LoadingFragmnet()};
//                regActivity.removeFragment(myFragments);
////            regActivity.replaceFragmnet(new NewNumberFragment(), R.id.frameLayout, true);
////            NewNumberFragment.isTimerActive = false;
//            }
            regActivity.onBackPressed();
        }
        if (responseModel != null && responseModel.getResultObj() != null && responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.VERIFY_PROMO_CODE)) {

            verifyPromoCodeOutputResponse = (VerifyPromoCodeOutputResponse) responseModel.getResultObj();
            if (verifyPromoCodeOutputResponse != null && verifyPromoCodeOutputResponse.getValid()) {

                PromoCodeVerificationSuccessfulFragment promoCodeVerificationSuccessfulFragment = new PromoCodeVerificationSuccessfulFragment();
                Bundle bundle = new Bundle();
                bundle.putString(ConstantUtils.KEY_PROMO_CODE_DESCRIPTION, verifyPromoCodeOutputResponse.getPromoCodeDescrition());
                bundle.putInt(ConstantUtils.KEY_PROMO_CODE_TYPE, verifyPromoCodeOutputResponse.getPromoCodeType());
                bundle.putString(ConstantUtils.KEY_PROMO_CODE, verifyPromoCodeOutputResponse.getPromoCode());
                if (verifyPromoCodeOutputResponse.getPromoCodeType() == 1) {
                    totalAmount = Double.valueOf(sharedPrefrencesManger.getMembershipPrice());
                    totalAmountSIM = Double.valueOf(sharedPrefrencesManger.getSIMPrice());
                } else if (verifyPromoCodeOutputResponse.getPromoCodeType() == 2) {
                    totalAmount = 0;
                    totalAmountSIM = 0;
                }
                promoCodeVerificationSuccessfulFragment.setArguments(bundle);
                promoCode = et_promoCode.getText().toString().trim();
                regActivity.replaceFragmnet(promoCodeVerificationSuccessfulFragment, R.id.frameLayout, true);

            } else {
                showError();
            }

        }
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        if (responseModel != null && responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.VERIFY_PROMO_CODE)) {
            regActivity.onBackPressed();
            showError();
        }
    }

    private void showError() {
        Bundle bundle = new Bundle();
        if (verifyPromoCodeOutputResponse != null && verifyPromoCodeOutputResponse.getErrorMsgEn() != null
                && verifyPromoCodeOutputResponse.getErrorMsgEn().length() > 0) {
            if (sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)) {
                bundle.putString(ConstantUtils.errorString, verifyPromoCodeOutputResponse.getErrorMsgEn());
            } else {
                bundle.putString(ConstantUtils.errorString, verifyPromoCodeOutputResponse.getErrorMsgAr());
            }
        } else {
            bundle.putString(ConstantUtils.errorString, getString(R.string.error_txt));
        }
        bundle.putString(ConstantUtils.buttonTitle, getString(R.string.try_again));
        ErrorFragment errorFragment = new ErrorFragment();
        errorFragment.setArguments(bundle);

        regActivity.addFragmnet(errorFragment, R.id.frameLayout, true);
    }

    void setListeners() {
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()) {
                    if (paidRBtn.isChecked()) {
                        orderTypeStr = "1";
                        Bundle bundle2 = new Bundle();

                        bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_package_selected_sim_membership);
                        bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_package_selected_sim_membership);
                        mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_package_selected_sim_membership, bundle2);
                        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), TAGGING_package_selected);
                    }
                    if (freeRBtn.isChecked()) {
                        orderTypeStr = "2";
                        Bundle bundle2 = new Bundle();

                        bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_package_selected_simonly);
                        bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_package_selected_simonly);
                        mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_package_selected_simonly, bundle2);
                    }
                    //
                    logEvent();

                    if (et_promoCode.getText().toString().length() > 0) {
                        verifyPromoCode();
                    } else {
                        LogPromoCodeDetailsRequestModel logPromoCodeDetailsRequestModel = new LogPromoCodeDetailsRequestModel();
                        logPromoCodeDetailsRequestModel.setPromoCode("");
                        logPromoCodeDetailsRequestModel.setPromoCodeDescription("");
                        logPromoCodeDetailsRequestModel.setPromoCodeType("");
                        logPromoCodeDetailsRequestModel.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
                        new LogPromoCodeService(OrderFragment.this, logPromoCodeDetailsRequestModel);
//                        regActivity.replaceFragmnet(new DeliveryDetailsAreaFragment(), R.id.frameLayout, true);
                        sharedPrefrencesManger.setIsFromMigration(false);
                        totalAmount = Double.valueOf(sharedPrefrencesManger.getMembershipPrice());
                        totalAmountSIM = Double.valueOf(sharedPrefrencesManger.getSIMPrice());
                        promoCode = "";
                        logEvent(AdjustEvents.ORDER_NEXT, new Bundle());
                        regActivity.replaceFragmnet(new GetNodIDFragment(), R.id.frameLayout, true);
                    }
//
//                    DeliveryMethodFragment deliveryMethodFragment = new DeliveryMethodFragment();
//                    regActivity.replaceFragmnet(deliveryMethodFragment, R.id.frameLayout, true);
                }
            }
        });

        paidRBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!paidRBtn.isChecked())
                    paidRBtn.setChecked(true);
                freeRBtn.setChecked(false);

            }
        });
        freeRBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!freeRBtn.isChecked())
                    freeRBtn.setChecked(true);
                paidRBtn.setChecked(false);
            }
        });
    }

    private void verifyPromoCode() {
        regActivity.addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        VerifyPromoCodeInputModel verifyPromoCodeInputModel = new VerifyPromoCodeInputModel();
        verifyPromoCodeInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
        verifyPromoCodeInputModel.setOsVersion(osVersion);
        verifyPromoCodeInputModel.setDeviceId(deviceId);
        verifyPromoCodeInputModel.setAppVersion(appVersion);
        verifyPromoCodeInputModel.setChannel(channel);
        verifyPromoCodeInputModel.setLang(currentLanguage);
        verifyPromoCodeInputModel.setToken(token);
        verifyPromoCodeInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        verifyPromoCodeInputModel.setAuthToken(authToken);
        verifyPromoCodeInputModel.setPromoCode(et_promoCode.getText().toString().trim());

        new VerifyPromoCodeService(this, verifyPromoCodeInputModel);
    }

    private boolean isValid() {
        if (paidRBtn != null && freeRBtn != null) {
            return paidRBtn.isChecked() || freeRBtn.isChecked();
        } else {
            return false;
        }
    }

    private void logEvent() {

        logEventInputModel.setActionTransactionId(sharedPrefrencesManger.getTempAppId());
        logEventInputModel.setScreenId(ConstantUtils.SCREEN_ID_1001);
        logEventInputModel.setPackageSelected(((TextView) view.findViewById(R.id.simId)).getText().toString());

        new LogApplicationFlowService(this, logEventInputModel);
    }

}
