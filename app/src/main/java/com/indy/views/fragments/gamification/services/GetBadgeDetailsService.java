package com.indy.views.fragments.gamification.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.BaseServiceManger;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.gamification.models.getbadgesdetails.GetBadgeDetailsInputModel;
import com.indy.views.fragments.gamification.models.getbadgesdetails.GetBadgeDetailsOutputModel;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Tohamy on 10/1/2017.
 */

public class GetBadgeDetailsService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private GetBadgeDetailsInputModel getBadgeDetailsInputModel;


    public GetBadgeDetailsService(BaseInterface baseInterface, GetBadgeDetailsInputModel getBadgeDetailsInputModel) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.getBadgeDetailsInputModel = getBadgeDetailsInputModel;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<GetBadgeDetailsOutputModel> call = apiService.getBadgeDetails(getBadgeDetailsInputModel);
        call.enqueue(new Callback<GetBadgeDetailsOutputModel>() {
            @Override
            public void onResponse(Call<GetBadgeDetailsOutputModel> call, Response<GetBadgeDetailsOutputModel> response) {
                mBaseResponseModel.setServiceType(ServiceUtils.GET_BADGES_DETAILS);
//                try{
//                    Log.d("Message",new String(response.body().getBadgeDetails().getDescription2().getBytes(),"UTF-8"));
//                }catch (Exception ex){
//
//                }
                mBaseResponseModel.setResultObj(response.body());
//                String errorUnAuthorize = null;
//                if(((MasterErrorResponse) mBaseResponseModel.getResultObj()).getErrorCode()!=null){
//                    errorUnAuthorize = ((MasterErrorResponse) mBaseResponseModel.getResultObj()).getErrorCode();
//                }
                if (response.code() == ConstantUtils.unAuthorizeToken){
//                    ||
//                }(errorUnAuthorize!=null &&
//                        errorUnAuthorize.equalsIgnoreCase("token_003"))) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (response.body().getErrorMsgEn() != null) {
//                        String urlForTag = call.request().url().toString();
//                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
//                        urlForTag = urlForTag.replace("/", "_");
//                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<GetBadgeDetailsOutputModel> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.GET_BADGES_DETAILS);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}

