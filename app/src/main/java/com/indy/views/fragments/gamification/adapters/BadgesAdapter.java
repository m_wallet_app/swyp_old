package com.indy.views.fragments.gamification.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.indy.R;
import com.indy.utils.SharedPrefrencesManger;
import com.indy.views.fragments.gamification.OnBadgeListClicked;
import com.indy.views.fragments.gamification.models.getbadgeslist.Badge;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

public class BadgesAdapter extends RecyclerView.Adapter<BadgesAdapter.ViewHolder> {

    private Context mContext;
    private SharedPrefrencesManger sharedPrefrencesManger;
    private List<Badge> badges;
    private OnBadgeListClicked onBadgeListClicked;

    public BadgesAdapter(List<Badge> badges, Context context, OnBadgeListClicked onBadgeListClicked) {

        this.mContext = context;
        this.sharedPrefrencesManger = new SharedPrefrencesManger(context);
        this.badges = badges;
        this.onBadgeListClicked = onBadgeListClicked;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.item_badge, viewGroup, false);
        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        Badge badge = badges.get(position);
        holder.iv_badge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBadgeListClicked.onBadgeClicked(badges.get(position),position);
            }
        });

        if (badge.isCompleted()) {
            ImageLoader.getInstance().displayImage(badge.getImageUrlCompleted(), holder.iv_badge);
        } else {
            ImageLoader.getInstance().displayImage(badge.getImageUrlInProgress(), holder.iv_badge);
        }
        holder.tv_name.setText(badge.getTitle());

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return badges.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView iv_badge;
        TextView tv_name;

        public ViewHolder(View v) {
            super(v);

            iv_badge = (ImageView) v.findViewById(R.id.iv_badge);
            tv_name = (TextView) v.findViewById(R.id.tv_badge_name);
        }
    }


}
