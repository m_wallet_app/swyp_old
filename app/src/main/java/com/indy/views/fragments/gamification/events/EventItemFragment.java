package com.indy.views.fragments.gamification.events;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.indy.R;
import com.indy.models.rewards.StoreLocation;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.utils.SharedPrefrencesManger;
import com.indy.views.fragments.gamification.models.EventPhoto;
import com.indy.views.fragments.gamification.models.EventsList;
import com.indy.views.fragments.utils.MasterFragment;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;


public class EventItemFragment extends MasterFragment {
    private ImageView deal_image;
    TextView tv_title, tv_subTitle, tv_pageCounter, tv_expiry;

    private String imageUrl;
    private String tv_indicates;
    int position, total;

    EventsList mEventsListItem;
    CardView CardView;
    ArrayList<StoreLocation> mStoreLocationsList;
    String icon_url;
    View rootView;
    private SharedPrefrencesManger sharedPrefrencesManger;
    List<EventPhoto> eventPhotosList;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            super.onCreateView(inflater, container, savedInstanceState);
            rootView = inflater.inflate(R.layout.item_event, container, false);


            if (getArguments() != null && getArguments().getParcelable(ConstantUtils.item_events_pager) != null) {
                mEventsListItem = getArguments().getParcelable(ConstantUtils.item_events_pager);
            }
            if (getArguments() != null && getArguments().getInt("position") != -1) {
                position = getArguments().getInt("position");
            }
            if (getArguments() != null && getArguments().getInt("total") != -1) {
                total = getArguments().getInt("total");
            }

            if (getArguments() != null && getArguments().getString("icon_url") != null) {
                icon_url = getArguments().getString("icon_url");
            }


            deal_image = (ImageView) rootView.findViewById(R.id.deal_image);
            sharedPrefrencesManger = new SharedPrefrencesManger(getActivity().getApplicationContext());

            tv_title = (TextView) rootView.findViewById(R.id.tv_event_name);
            tv_pageCounter = (TextView) rootView.findViewById(R.id.page_counter);
            tv_subTitle = (TextView) rootView.findViewById(R.id.tv_event_subtitle);
            tv_expiry = (TextView) rootView.findViewById(R.id.deal_expire);
            CardView = (CardView) rootView.findViewById(R.id.card_view);
            CardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mEventsListItem.getActive()) {
                        Intent intent = new Intent(getActivity(), EventDetailActivity.class);
                        intent.putExtra("total", total);
                        intent.putExtra("position", position);
                        intent.putExtra(ConstantUtils.KEY_EVENT_DETAIL, mEventsListItem);
                        intent.putExtra("icon_url", icon_url);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivityForResult(intent, 100);
                    } else {
                        Intent intent = new Intent(getActivity(), PreviousEventDetailActivity.class);
                        intent.putExtra("total", total);
                        intent.putExtra("position", position);
                        intent.putExtra(ConstantUtils.KEY_EVENT_DETAIL, mEventsListItem);
                        intent.putExtra("icon_url", icon_url);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivityForResult(intent, 100);
                    }
                }
            });

//            mStoreLocationsList = new ArrayList<StoreLocation>();
//            if (getArguments() != null && getArguments().getParcelableArrayList("storeLocation") != null) {
//                mStoreLocationsList = getArguments().getParcelableArrayList("storeLocation");
//            }
            ImageLoader.getInstance().displayImage(mEventsListItem.getImageUrl(), deal_image);
            if (sharedPrefrencesManger.getLanguage().equalsIgnoreCase("en")) {
                tv_title.setText(mEventsListItem.getNameEn());
                tv_subTitle.setText(mEventsListItem.getLocation());
                tv_expiry.setText(CommonMethods.parseDateWithHours(mEventsListItem.getTournamentActualDate(), sharedPrefrencesManger.getLanguage()));
//                tv_expiry.setText(mEventsListItem.getDate());

            } else {
                tv_title.setText(mEventsListItem.getNameAr());
                tv_subTitle.setText(mEventsListItem.getLocation());
                tv_expiry.setText(CommonMethods.parseDateWithHours(mEventsListItem.getTournamentActualDate(), sharedPrefrencesManger.getLanguage()));
//                tv_expiry.setText(mEventsListItem.getDate());
            }
//            tv_expiry.setText(mEventsListItem.getDate());

            tv_pageCounter.setText(position + "/" + total);


        } catch (Exception ex) {
            //CommonMethods.logException(ex);
        }
        return rootView;
    }

//    private void createDummyPhotosList() {
//        eventPhotosList = new ArrayList<>();
//        for(int i=0;i<10;i++){
//            EventPhoto eventPhoto = new EventPhoto();
//            eventPhoto.setSelected(false);
//            eventPhoto.setDescription("Last nights concert was amazing");
//            eventPhoto.setId(i+"");
//            eventPhoto.setImageURL(ConstantUtils.DUMMY_IMAGE_URL);
//            eventPhoto.setOwnedByUser(false);
//            eventPhoto.setUploadedBy("Carl Anderson "+i);
//            eventPhoto.setVotes(12000);
//            eventPhotosList.add(eventPhoto);
//        }
//        mEventsListItem.setEventPhotosList(eventPhotosList);
//    }


    //    public void bindData(String imageUrl,String deal_name,String deal_price_discount,String deal_expire ,String tv_indicator) {
//        this.imageUrl = imageUrl;
//        this.addressc = tv_indicator;
//    }
}
