package com.indy.views.fragments.gamification.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.BaseServiceManger;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.gamification.models.socialshare.SocialShareInput;
import com.indy.views.fragments.gamification.models.socialshare.SocialShareOutput;
import com.indy.views.fragments.gamification.models.viewluckydealservice.ViewLuckyDealInput;
import com.indy.views.fragments.gamification.models.viewluckydealservice.ViewLuckyDealOutput;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Tohamy on 10/9/2017.
 */

public class ViewLuckyDealService  extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private ViewLuckyDealInput viewLuckyDealInput;


    public ViewLuckyDealService(BaseInterface baseInterface, ViewLuckyDealInput viewLuckyDealInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.viewLuckyDealInput = viewLuckyDealInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<ViewLuckyDealOutput> call = apiService.viewLuckyDeal(viewLuckyDealInput);
        call.enqueue(new Callback<ViewLuckyDealOutput>() {
            @Override
            public void onResponse(Call<ViewLuckyDealOutput> call, Response<ViewLuckyDealOutput> response) {
                mBaseResponseModel.setServiceType(ServiceUtils.VIEW_LUCKY_DEAL);
                mBaseResponseModel.setResultObj(response.body());

                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (response.body().getErrorMsgEn() != null) {
//                        String urlForTag = call.request().url().toString();
//                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
//                        urlForTag = urlForTag.replace("/", "_");
//                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<ViewLuckyDealOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.VIEW_LUCKY_DEAL);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}