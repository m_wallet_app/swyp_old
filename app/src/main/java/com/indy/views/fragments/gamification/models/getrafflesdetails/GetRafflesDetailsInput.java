package com.indy.views.fragments.gamification.models.getrafflesdetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GamificationBaseInputModel;

/**
 * Created by mobile on 12/10/2017.
 */

public class GetRafflesDetailsInput extends GamificationBaseInputModel {

    @SerializedName("raffleEventId")
    @Expose
    String raffleEventId;

    public String getRaffleEventId() {
        return raffleEventId;
    }

    public void setRaffleEventId(String raffleEventId) {
        this.raffleEventId = raffleEventId;
    }
}
