package com.indy.views.fragments.gamification.models.getnotificationpopupdetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GamificationBaseInputModel;

/**
 * Created by mobile on 10/10/2017.
 */

public class GetNotificationPopupDetailsInput extends GamificationBaseInputModel {
    @SerializedName("popupId")
    @Expose
    String notificationId;

    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }
}
