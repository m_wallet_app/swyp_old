package com.indy.views.fragments.gamification.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by mobile on 23/10/2017.
 */

public class GetNotificationSettingsOutput extends MasterErrorResponse {

    @SerializedName("inAppNotifications")
    @Expose
    private boolean inAppNotifications;

    public boolean getInAppNotifications() {
        return inAppNotifications;
    }

    public void setInAppNotifications(boolean inAppNotifications) {
        this.inAppNotifications = inAppNotifications;
    }
}
