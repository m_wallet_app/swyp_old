package com.indy.views.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.indy.R;
import com.indy.models.balance.BalanceInputModel;
import com.indy.models.benefits.BenefitsOutputModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterInputResponse;
import com.indy.services.BenefitsService;
import com.indy.services.GetUserBalanceService;
import com.indy.utils.CommonMethods;
import com.indy.views.activites.RechargeActivity;
import com.indy.views.activites.SwpeMainActivity;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.indy.views.fragments.utils.MasterFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class BenefitsFragment extends MasterFragment {

    View view;
    TextView bt_cancel;
    Button bt_proceed;
    TextView tv_benefits;
    BenefitsOutputModel benefitsOutputModel;

    TextView tv_header, tv_footer, tv_title1, tv_description1, tv_title2, tv_description2, tv_title3, tv_description3;

    public BenefitsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_benefits, container, false);
        initUI();
        onConsumeService();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();

        bt_proceed = (Button) view.findViewById(R.id.btn_proceed);
        bt_cancel = (TextView) view.findViewById(R.id.btn_cancel);
        tv_header = (TextView) view.findViewById(R.id.tv_header);
        tv_footer = (TextView) view.findViewById(R.id.tv_footer);
        tv_title1 = (TextView) view.findViewById(R.id.title_1);
        tv_title2 = (TextView) view.findViewById(R.id.title_2);
        tv_title3 = (TextView) view.findViewById(R.id.title_3);
        tv_description1 = (TextView) view.findViewById(R.id.description_1);
        tv_description2 = (TextView) view.findViewById(R.id.description_2);
        tv_description3 = (TextView) view.findViewById(R.id.description_3);

        bt_proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), RechargeActivity.class);
                startActivity(intent);
            }
        });

        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        ((SwpeMainActivity) getActivity()).addFragmnet(new LoadingFragmnet(),R.id.frameLayout,true);
        MasterInputResponse masterInputResponse = new MasterInputResponse();
        masterInputResponse.setAppVersion(appVersion);
        masterInputResponse.setImsi(sharedPrefrencesManger.getMobileNo());
        masterInputResponse.setDeviceId(deviceId);
        masterInputResponse.setToken(token);
        masterInputResponse.setLang(currentLanguage);
        masterInputResponse.setChannel(channel);
        masterInputResponse.setMsisdn(sharedPrefrencesManger.getMobileNo());
        masterInputResponse.setOsVersion(osVersion);
        new BenefitsService(this,masterInputResponse);
    }



    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            if (responseModel != null && responseModel.getResultObj() != null) {
                benefitsOutputModel = (BenefitsOutputModel) responseModel.getResultObj();
                if (benefitsOutputModel != null && benefitsOutputModel.getBenefitsText() != null
                        && benefitsOutputModel.getBenefitsText().size() > 0) {
                    setupView();
                } else {
                    failure();
                }
            }
            getActivity().onBackPressed();
        }catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        try {
            getActivity().onBackPressed();
        }catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    public void setupView() {
        tv_header.setText(benefitsOutputModel.getHeader());
        tv_footer.setText(benefitsOutputModel.getFooter());
        for (int i = 0; i < benefitsOutputModel.getBenefitsText().size(); i++) {
            if (i == 0) {

                tv_title1.setText(benefitsOutputModel.getBenefitsText().get(i).getTitle());
                tv_description1.setText(benefitsOutputModel.getBenefitsText().get(i).getDescription());

            }else if (i == 1) {

                tv_title2.setText(benefitsOutputModel.getBenefitsText().get(i).getTitle());
                tv_description2.setText(benefitsOutputModel.getBenefitsText().get(i).getDescription());

            } else if (i == 2) {

                tv_title3.setText(benefitsOutputModel.getBenefitsText().get(i).getTitle());
                tv_description3.setText(benefitsOutputModel.getBenefitsText().get(i).getDescription());
                return;
            }
        }
    }

    public void failure() {

    }
}
