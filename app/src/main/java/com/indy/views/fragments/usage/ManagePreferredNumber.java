package com.indy.views.fragments.usage;


import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.indy.R;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.UnlimitedMinutesPackageActivity;
import com.indy.views.fragments.utils.MasterFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class ManagePreferredNumber extends MasterFragment {

    public View mView;
    private TextView tv_currentPreferredNumber, tv_numberConfirmation, tv_cancel;
    private EditText et_preferredNumber;
    private Button bt_change;
    private TextInputLayout til_number;
    private String preferredNumberOld;
    public ManagePreferredNumber() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_manage_preferred_number, container, false);
        initUI();
        return mView;
    }

    @Override
    public void initUI() {
        super.initUI();

        tv_currentPreferredNumber = (TextView) mView.findViewById(R.id.tv_old_preferred_number);
        et_preferredNumber = (EditText) mView.findViewById(R.id.et_preferredNumber);
        tv_cancel = (TextView) mView.findViewById(R.id.cancelBtn);
        bt_change = (Button) mView.findViewById(R.id.changeBtn);
        til_number = (TextInputLayout) mView.findViewById(R.id.number_input_layout);
        disableChangeBtn();
        if (getArguments() != null && getArguments().getString(ConstantUtils.oldPreferredNumber) != null) {
            tv_currentPreferredNumber.setText(getString(R.string.current_preferred_number) + "\n" + getArguments().getString(ConstantUtils.oldPreferredNumber));
            preferredNumberOld = getArguments().getString(ConstantUtils.oldPreferredNumber);

        } else {
            tv_currentPreferredNumber.setText("");
        }

        et_preferredNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (et_preferredNumber.getText().toString().trim().length() > 0) {
                    enableChangeBtn();
                } else {
                    disableChangeBtn();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        bt_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValid()) {
                    Bundle bundle = new Bundle();
                    bundle.putString(ConstantUtils.oldPreferredNumber,preferredNumberOld);
                    bundle.putString(ConstantUtils.mobileNo, et_preferredNumber.getText().toString().trim());
                    ChangePreferredNumber changePreferredNumber = new ChangePreferredNumber();
                    changePreferredNumber.setArguments(bundle);
                    ((UnlimitedMinutesPackageActivity) getActivity()).replaceFragmnet(changePreferredNumber, R.id.frameLayout, false);
                }
            }
        });
    }

    public boolean isValid() {
        boolean isValid = true;
        if (et_preferredNumber.getText().toString().length() != 10) {

            isValid = false;
            setTextInputLayoutError(til_number, getString(R.string.invalid_delivery_number));

        } else {
            removeTextInputLayoutError(til_number);
        }

        if(et_preferredNumber.getText().toString().equals(preferredNumberOld)){
            isValid = false;
            setTextInputLayoutError(til_number,getString(R.string.this_is_already_your_preferred_number));
        }else{
            removeTextInputLayoutError(til_number);
        }

        if(et_preferredNumber.getText().toString().equals(sharedPrefrencesManger.getMobileNo())){
            isValid = false;
            setTextInputLayoutError(til_number,getString(R.string.your_own_number_cannot_be_your_preferred_number));
        }else{
            removeTextInputLayoutError(til_number);
        }
        return isValid;
    }

    private void disableChangeBtn() {
        bt_change.setAlpha(0.5f);
        bt_change.setEnabled(false);
    }

    private void enableChangeBtn() {
        bt_change.setAlpha(1.0f);
        bt_change.setEnabled(true);
    }

}
