package com.indy.views.fragments.rewards.ItemsCarousel;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.ViewGroup;


/**
 * Created by Amir.jehangir on 10/30/2016.
 */
public class AspectRatioCardView extends CardView {

    private float ratio = 1.23f;
    Context context;

    public AspectRatioCardView(Context context) {
        this(context, null);
        this.context = context;
    }

    public AspectRatioCardView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);

    }

    public AspectRatioCardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (ratio > 0) {
            int ratioHeight;
            int width = getMeasuredWidth();
            if(getMeasuredWidth()>800) {
                width = width -100;
            }
//                ratioHeight = (int) (getMeasuredWidth() * 1.0);
//            }else{
                ratioHeight = (int) (width * ratio);
//            }
            setMeasuredDimension(getMeasuredWidth(), ratioHeight);
            ViewGroup.LayoutParams lp = getLayoutParams();
            lp.height = ratioHeight;
            setLayoutParams(lp);
        }
    }
}
