package com.indy.views.fragments.gamification.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.indy.views.fragments.gamification.events.EventsFragment;

/**
 * Created by emad on 8/9/16.
 */
public class EventsPagerAdapter extends FragmentStatePagerAdapter {
    private int mNumOfTabs;

    public EventsPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }


    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return EventsFragment.newInstance(true);
            case 1:
                return EventsFragment.newInstance(false);
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }


}

