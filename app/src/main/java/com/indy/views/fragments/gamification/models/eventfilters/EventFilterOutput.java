package com.indy.views.fragments.gamification.models.eventfilters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

import java.util.List;

/**
 * Created by mobile on 01/10/2017.
 */

public class EventFilterOutput extends MasterErrorResponse {
    @SerializedName("controlType")
    @Expose
    private String controlType;
    @SerializedName("options")
    @Expose
    private List<EventFilterOptions> options = null;

    public String getControlType() {
        return controlType;
    }

    public void setControlType(String controlType) {
        this.controlType = controlType;
    }

    public List<EventFilterOptions> getOptions() {
        return options;
    }

    public void setOptions(List<EventFilterOptions> options) {
        this.options = options;
    }
}
