package com.indy.views.fragments.gamification.challenges;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.controls.ServiceUtils;
import com.indy.models.faqs.FaqsInputModel;
import com.indy.models.faqs.FaqsResponseModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.FaqsService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.MasterActivity;
import com.indy.views.fragments.gamification.models.serviceInputOutput.LogShareEventInputModel;
import com.indy.views.fragments.gamification.services.RegisterShareEventService;
import com.indy.views.fragments.utils.ErrorFragment;

public class EarnRaffleTicketsActivity extends MasterActivity {

    LinearLayout ll_invite_friend, ll_store, ll_checkins, ll_badges, ll_share, ll_recharge_account;
    RelativeLayout rl_back, rl_help;
    TextView tv_header_title;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_earn_raffle_tickets);
        initUI();
    }

    @Override
    public void initUI() {
        super.initUI();

        ll_invite_friend = (LinearLayout) findViewById(R.id.ll_invite_friends);
        ll_store = (LinearLayout) findViewById(R.id.ll_buy_packages);
        ll_checkins = (LinearLayout) findViewById(R.id.ll_check_in);
        ll_badges = (LinearLayout) findViewById(R.id.ll_unlock_badges);
        ll_recharge_account = (LinearLayout) findViewById(R.id.ll_recharge_account);
        ll_share = (LinearLayout) findViewById(R.id.ll_share);
        rl_back = (RelativeLayout) findViewById(R.id.backLayout);
        rl_help = (RelativeLayout) findViewById(R.id.helpLayout);

        ll_recharge_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(ConstantUtils.RAFFLES_RECHARGE_ACCOUNT);
                finish();
            }
        });

        ll_invite_friend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(ConstantUtils.RAFFLES_INVITE_FRIEND);
                finish();
            }
        });

        ll_store.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(ConstantUtils.RAFFLES_STORE);
                finish();
            }
        });

        ll_checkins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(ConstantUtils.RAFFLES_CHECKIN);
                finish();
            }
        });

        ll_badges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(ConstantUtils.RAFFLES_BADGES);
                finish();
            }
        });

        ll_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onConsumeService();
            }
        });
        rl_help.setVisibility(View.INVISIBLE);
        rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tv_header_title = (TextView) findViewById(R.id.titleTxt);
        tv_header_title.setText(getString(R.string.swypees));
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
//        findViewById(R.id.shareID).setEnabled(false);
        FaqsInputModel faqsInputModel = new FaqsInputModel();
        faqsInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
        faqsInputModel.setLang(currentLanguage);
        faqsInputModel.setAppVersion(appVersion);
        faqsInputModel.setToken(token);
        faqsInputModel.setChannel(channel);
        faqsInputModel.setSignificant(ConstantUtils.share_significant);
        faqsInputModel.setDeviceId(deviceId);
        faqsInputModel.setOsVersion(osVersion);
        faqsInputModel.setAuthToken(authToken);
        new FaqsService(this, faqsInputModel);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        if (responseModel != null && responseModel.getServiceType() != null && responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.faqsService)) {
            FaqsResponseModel faqsResponseModel = (FaqsResponseModel) responseModel.getResultObj();
            if (faqsResponseModel != null && faqsResponseModel.getSrcUrlEn() != null) {
                if (currentLanguage.equals("en")) {
                    onShareLinkSuccess(faqsResponseModel.getSrcUrlEn());
                } else {
                    onShareLinkSuccess(faqsResponseModel.getSrcUrlAr());
                }
            } else if (faqsResponseModel != null && faqsResponseModel.getErrorMsgEn() != null) {
                if (currentLanguage.equals("en")) {
                    onShareLinkFailure(faqsResponseModel.getErrorMsgEn());
                } else {
                    onShareLinkFailure(faqsResponseModel.getErrorMsgAr());
                }
            } else {
                onShareLinkFailure(getString(R.string.generice_error));
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sharedPrefrencesManger.getNavigationIndex().length() > 0 || sharedPrefrencesManger.getNavigationBadgeIndex().length() > 0) {

            finish();
        }
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
    }

    public void onShareLinkSuccess(String urlToShare) {

        Bundle bundle2 = new Bundle();

        bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_share_button);
        bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_share_button);
        mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_share_button, bundle2);
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.share_swyp));
        String textToShare = getString(R.string.share_swype_app) + "\n" + urlToShare;
        share.putExtra(Intent.EXTRA_TEXT, textToShare);

        startActivityForResult(Intent.createChooser(share, "Share Invite"), 120);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 120) {
            if (CommonMethods.isMembershipValid(this)) {
                logShareEvent();
            }
        }
    }

    private void logShareEvent() {

        LogShareEventInputModel logShareEventInputModel = new LogShareEventInputModel();
        logShareEventInputModel.setChannel(channel);
        logShareEventInputModel.setLang(currentLanguage);
        logShareEventInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        logShareEventInputModel.setAppVersion(appVersion);
        logShareEventInputModel.setAuthToken(authToken);
        logShareEventInputModel.setDeviceId(deviceId);
        logShareEventInputModel.setToken(token);
        logShareEventInputModel.setUserSessionId(sharedPrefrencesManger.getUserSessionId());
        logShareEventInputModel.setUserId(sharedPrefrencesManger.getUserId());
        logShareEventInputModel.setApplicationId(ConstantUtils.INDY_TALOS_ID);
        logShareEventInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
        logShareEventInputModel.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        logShareEventInputModel.setOsVersion(osVersion);
        logShareEventInputModel.setEventTypeId(ConstantUtils.LOG_SHARE_EVENT);

        new RegisterShareEventService(this, logShareEventInputModel);
    }


    public void onShareLinkFailure(String message) {

        Fragment errorFragmnet = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, message);
        errorFragmnet.setArguments(bundle);
        replaceFragmnet(errorFragmnet, R.id.contentFrameLayout, true);

    }
}
