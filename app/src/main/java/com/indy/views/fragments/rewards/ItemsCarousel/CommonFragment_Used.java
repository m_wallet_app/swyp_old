package com.indy.views.fragments.rewards.ItemsCarousel;

import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.indy.R;
import com.indy.models.rewards.UsedRewardsList;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.rewards.UsedRewardsFragment;
import com.indy.views.fragments.utils.MasterFragment;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by Amir.jehangir on 10/30/2016.
 */
public class CommonFragment_Used extends MasterFragment {
    private ImageView deal_image;
    TextView DealName, page_counter, deal_price_Discount, deal_expire;
    private View address1, address2, address3, address4, address5;
    private RatingBar ratingBar;
    private View head1, head2, head3, head4;
    private String imageUrl;
    private String tv_indicates;
    int position, total;
    TextView tv_addressc;
    UsedRewardsList mUsedRewardsListItem;
    android.support.v7.widget.CardView CardView;

    String addressc;
    View rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        rootView = inflater.inflate(R.layout.fragment_common_used, null);
//        DragLayout dragLayout = (DragLayout) rootView.findViewById(R.id.drag_layout);
        initUI();


        return rootView;
    }

    @Override
    public void initUI() {
        super.initUI();


        if (getArguments() != null && getArguments().getInt("position") != -1) {
            position = getArguments().getInt("position");
        }
        if (getArguments() != null && getArguments().getInt("total") != -1) {
            total = getArguments().getInt("total");
        }

        if (getArguments() != null && getArguments().getParcelable(ConstantUtils.item_USED_reward_view_pager) != null) {
            mUsedRewardsListItem = getArguments().getParcelable(ConstantUtils.item_USED_reward_view_pager);
        }

        deal_image = (ImageView) rootView.findViewById(R.id.deal_image);
        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);

        ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
        deal_image.setColorFilter(filter);

        //  tv_addressc = (TextView) rootView.findViewById(R.id.page_counter);

        DealName = (TextView) rootView.findViewById(R.id.DealName);
        page_counter = (TextView) rootView.findViewById(R.id.page_counter);
        deal_price_Discount = (TextView) rootView.findViewById(R.id.deal_price_Discount);
        deal_expire = (TextView) rootView.findViewById(R.id.deal_expire);
        CardView = (CardView) rootView.findViewById(R.id.card_view);
//        CardView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getActivity(), RewardDetailsActivity.class);
//                intent.putExtra(ConstantUtils.RewardsDetails, mUsedRewardsListItem);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(intent);
//            }
//        });
        ImageLoader.getInstance().displayImage(mUsedRewardsListItem.getImageUrl(), deal_image);
//        deal_price_Discount.setText(getString(R.string.aed) + " " + mUsedRewardsListItem.getAverageSaving() + " " + getString(R.string.saved));

        if (currentLanguage != null) {
            if (currentLanguage.equals("en")) {
                deal_price_Discount.setText(getString(R.string.aed) + " " + mUsedRewardsListItem.getAverageSaving() + " " + getString(R.string.discount));
                DealName.setText(mUsedRewardsListItem.getNameEn());
                deal_expire.setText(getString(R.string.used_on) + " " + UsedRewardsFragment.expiryDateEn);

            }else{
                deal_price_Discount.setText(getString(R.string.discount) + " " + mUsedRewardsListItem.getAverageSaving() + " " + getString(R.string.aed));
                DealName.setText(mUsedRewardsListItem.getNameAr());
                deal_expire.setText(getString(R.string.used_on) + " " + UsedRewardsFragment.expiryDateAr);
            }
        }else{
            if (sharedPrefrencesManger.getLanguage().equals("en")) {
                deal_price_Discount.setText(getString(R.string.aed) + " " + mUsedRewardsListItem.getAverageSaving() + " " + getString(R.string.discount));
                DealName.setText(mUsedRewardsListItem.getNameEn());
                deal_expire.setText(getString(R.string.used_on) + " " + UsedRewardsFragment.expiryDateEn);

            }else{
                deal_price_Discount.setText(getString(R.string.discount) + " " + mUsedRewardsListItem.getAverageSaving() + " " + getString(R.string.aed));
                DealName.setText(mUsedRewardsListItem.getNameAr());
                deal_expire.setText(getString(R.string.used_on) + " " + UsedRewardsFragment.expiryDateAr);
            }
        }
        page_counter.setText(position + "/" + total);

    }

    public void bindData(String imageUrl, String deal_name, String deal_price_discount, String deal_expire, String tv_indicator) {
        this.imageUrl = imageUrl;
        this.addressc = tv_indicator;
    }
}
