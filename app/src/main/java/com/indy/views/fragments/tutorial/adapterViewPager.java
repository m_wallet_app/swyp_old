package com.indy.views.fragments.tutorial;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

public class adapterViewPager extends FragmentStatePagerAdapter {
    List<Fragment> fragmentList;
    Context mContext;
    public adapterViewPager(FragmentManager fm, List<Fragment> fragmentList, Context context) {
        super(fm);
        this.fragmentList = fragmentList;
        this.mContext = context;
    }

    @Override
    public Fragment getItem(int position) {


//        if (position == 0) {
//            TutorialActivity.ll_fourth.setVisibility(View.INVISIBLE);
//            TutorialActivity.tv_content.setText(mContext.getString(R.string.enjoy_str_txt));
//            TutorialActivity.tv_title.setText(mContext.getString(R.string.enjoy_str));
//        } else if (position == 1) {
//
//            TutorialActivity.ll_fourth.setVisibility(View.INVISIBLE);
//            TutorialActivity.tv_content.setText(mContext.getString(R.string.enjoy_rewards_txt));
//            TutorialActivity.tv_title.setText(mContext.getString(R.string.enjoy_rewards));
//           } else if (position == 2) {
//                TutorialActivity.ll_fourth.setVisibility(View.INVISIBLE);
//                TutorialActivity.tv_content.setText(mContext.getString(R.string.track_your_usage_str));
//                TutorialActivity.tv_title.setText(mContext.getString(R.string.track_your_usage));
//            } else if (position == 3) {
//                TutorialActivity.ll_fourth.setVisibility(View.VISIBLE);
//                TutorialActivity.tv_content.setText(mContext.getString(R.string.enjoy_packages_str));
//                TutorialActivity.tv_title.setText(mContext.getString(R.string.enjoy_packages));
//            }
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size(); //the number of paggers on ViewPager -_-
    }
}

