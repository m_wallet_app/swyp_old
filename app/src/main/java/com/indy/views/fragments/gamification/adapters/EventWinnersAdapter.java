package com.indy.views.fragments.gamification.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.indy.R;
import com.indy.utils.SharedPrefrencesManger;
import com.indy.views.fragments.gamification.models.EventWinnerModel;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.List;

public class EventWinnersAdapter extends RecyclerView.Adapter<EventWinnersAdapter.ViewHolder> {
    private List<EventWinnerModel> eventsList;
    private Context mContext;
    private SharedPrefrencesManger sharedPrefrencesManger;


    public EventWinnersAdapter(List<EventWinnerModel> mRewardsList, Context context){
        eventsList = mRewardsList;
        this.mContext = context;
        this.sharedPrefrencesManger = new SharedPrefrencesManger(context);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.item_event_winner, viewGroup, false);
        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final EventWinnerModel eventListItem = eventsList.get(position);
        ImageLoader.getInstance().displayImage(eventListItem.getImageURL(), holder.iv_photo, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        });

        holder.iv_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                onPhotoItemClick.onPhotoClicked(position);
            }
        });

        holder.tv_description.setText(eventListItem.getTitle());
        holder.tv_votes.setText(eventListItem.getVotes() + " " + mContext.getString(R.string.votes));
        if(eventListItem.getRank()==1) {
            holder.tv_rank.setText(eventListItem.getRank()+"st");
        }else if(eventListItem.getRank()==2){
            holder.tv_rank.setText(eventListItem.getRank()+"nd");
        }else if(eventListItem.getRank()==3){
            holder.tv_rank.setText(eventListItem.getRank()+"rd");
        }else{
            holder.tv_description.setText(eventListItem.getRank()+"th");
        }
        holder.tv_title.setText(eventListItem.getContestantName());

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return eventsList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView iv_photo;
        TextView tv_votes, tv_title, tv_description, tv_rank;

        public ViewHolder(View v) {
            super(v);

            iv_photo = (ImageView) v.findViewById(R.id.iv_photo_details);
            tv_votes = (TextView) v.findViewById(R.id.tv_vote_counter);
            tv_title = (TextView) v.findViewById(R.id.tv_photo_username);
            tv_rank = (TextView) v.findViewById(R.id.tv_rank);
            tv_description = (TextView) v.findViewById(R.id.tv_photo_description);

        }
    }


}
