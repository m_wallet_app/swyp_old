package com.indy.views.fragments.gamification.models.viewluckydealservice;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by Tohamy on 10/9/2017.
 */

public class ViewLuckyDealOutput extends MasterErrorResponse {
    @SerializedName("dealviewed")
    @Expose
    private boolean dealViewed;

    public boolean isDealViewed() {
        return dealViewed;
    }

    public void setDealViewed(boolean dealViewed) {
        this.dealViewed = dealViewed;
    }
}
