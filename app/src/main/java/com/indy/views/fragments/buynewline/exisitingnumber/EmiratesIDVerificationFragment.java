package com.indy.views.fragments.buynewline.exisitingnumber;


import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.indy.R;
import com.indy.controls.AdjustEvents;
import com.indy.controls.ServiceUtils;
import com.indy.customviews.CustomEditText;
import com.indy.models.emiratesIDVerificationService.EmiratesIDVerificationInputModel;
import com.indy.models.emiratesIdVerification.VerifyEmiratesIdInput;
import com.indy.models.emiratesIdVerification.VerifyEmiratesIdOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.LogApplicationFlowService;
import com.indy.services.VerifyEmiratesIdOCRNewService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.buynewline.DeliveryDetailsAddress;
import com.indy.views.fragments.buynewline.DeliveryDetailsAreaFragment;
import com.indy.views.fragments.buynewline.DeliveryMethodFragment;
import com.indy.views.fragments.buynewline.NameDOBFragment;
import com.indy.views.fragments.buynewline.NewNumberFragment;
import com.indy.views.fragments.buynewline.OrderFragment;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.indy.views.fragments.utils.MasterFragment;

import java.util.ArrayList;

import static com.indy.utils.ConstantUtils.TAGGING_eid_number_verify;
import static com.indy.utils.ConstantUtils.TAGGING_manual_eid_verify;
import static com.indy.views.activites.RegisterationActivity.logEventInputModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class EmiratesIDVerificationFragment extends MasterFragment {

    EditText eIDTxt;
    View mView;
    private VerifyEmiratesIdInput verifyEmiratesIdInput;
    private VerifyEmiratesIdOutput verifyEmiratesIdOutput;
    private EmiratesIDVerificationInputModel emiratesIDVerificationInputModel;
    private int status;
    Button verfyBtn;
    public static String emiratesID;
    private TextInputLayout mobileNoInputLayout;
    private CustomEditText code1, code2, code3, code4;
    public int NO_OF_DIGITS = 15;
    int[] tvListIds = {R.id.code_1, R.id.code_2, R.id.code_3, R.id.code_4};
    ArrayList<EditText> tv_digits;
    public static EmiratesIDVerificationFragment emiratesIDVerificationFragment;

    public EmiratesIDVerificationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_emirates_idverification, container, false);

        initUI();
        setListeners();
        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_manual_eid_entry);
        return mView;
    }


    @Override
    public void initUI() {
        super.initUI();
        regActivity.setHeaderTitle("");
        regActivity.showHeaderLayout();
        eIDTxt = (EditText) mView.findViewById(R.id.eid_number);
        verfyBtn = (Button) mView.findViewById(R.id.verfyBtn);
        mobileNoInputLayout = (TextInputLayout) mView.findViewById(R.id.mobileNoInputLayout);
        code1 = (CustomEditText) mView.findViewById(R.id.code_1);
        code2 = (CustomEditText) mView.findViewById(R.id.code_2);
        code3 = (CustomEditText) mView.findViewById(R.id.code_3);
        code4 = (CustomEditText) mView.findViewById(R.id.code_4);
        disableVerifyButton();
        emiratesIDVerificationFragment = this;

    }


    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            if (isVisible()) {
                if (responseModel != null && responseModel.getServiceType() != null && responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.LOG_USER_EVENT)) {
                } else {
                    regActivity.onBackPressed();
                    if (isVisible()) {
                        if (responseModel != null && responseModel.getResultObj() != null) {
                            verifyEmiratesIdOutput = (VerifyEmiratesIdOutput) responseModel.getResultObj();
                            if (verifyEmiratesIdOutput != null) {
                                if (verifyEmiratesIdOutput.getStatus() == null)
                                    onEmiratesIDVerificationFail();
                                else
                                    onEmiratesIDVerificationSuccess();
                            }
                        }
                    }
                }

            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

    }

    private void onEmiratesIDVerificationSuccess() {
        logEvent(AdjustEvents.EID_PRECESS_COMPLETE, new Bundle());
        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), TAGGING_eid_number_verify);
        status = verifyEmiratesIdOutput.getStatus();

        switch (status) {

            case 1: // eligable
//                onEligable();
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), TAGGING_manual_eid_verify);
                logEvent(AdjustEvents.AGE_ELIGIBILITY_SUCCESS, new Bundle());
                emiratesID = getCode(true);


                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_delivery_info_eid_entered);
                logEvent(AdjustEvents.REVIEW_ORDER_SUMMARY, new Bundle());
                regActivity.replaceFragmnet(new NameDOBFragment(), R.id.frameLayout, true);
                break;
            case 6:
                logEvent(AdjustEvents.ALREADY_SWYP_CUSTOMER, new Bundle());
                CommonMethods.logFirebaseEvent(mFirebaseAnalytics, ConstantUtils.TAGGING_onboarding_error_existing_user);

//                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_onboarding_invalid_eid_number);
                regActivity.replaceFragmnet(new RegisteredNonEmirateIDDocumentFragment(), R.id.frameLayout, true);
                break;
            default:
                logEvent(AdjustEvents.AGE_ELIGIBILITY_FAILURE, new Bundle());
//                CommonMethods.logFirebaseEvent(mFirebaseAnalytics,ConstantUtils.TAGGING_onboarding_error_existing_user);
                CommonMethods.logFirebaseEvent(mFirebaseAnalytics, ConstantUtils.TAGGING_onboarding_error_eid_failure);

                if (verifyEmiratesIdOutput != null && verifyEmiratesIdOutput.getErrorMsgEn() != null) {
                    showErrorFragment(verifyEmiratesIdOutput.getErrorMsgEn());
                } else {
                    showErrorFragment(getString(R.string.generice_error));

                }
                break;
        }
    }


    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        if (responseModel != null && responseModel.getServiceType() != null && !responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.LOG_USER_EVENT)) {
            super.onErrorListener(responseModel);
            try {
                if (isVisible())
                    regActivity.onBackPressed();
            } catch (Exception ex) {
                if (ex != null) {
                    ex.printStackTrace();
                }
            }
            showErrorFragment("Error in service.");
        }
    }

    private void showLoadingFragment() {
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.loadingString, getString(R.string.verifying_eid));
        LoadingFragmnet loadingFragmnet = new LoadingFragmnet();
        loadingFragmnet.setArguments(bundle);
        regActivity.addFragmnet(loadingFragmnet, R.id.loadingFragmeLayout, true);
    }

    private void showErrorFragment(String error) {
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, error);
        ErrorFragment errorFragment = new ErrorFragment();
        errorFragment.setArguments(bundle);
        regActivity.replaceFragmnet(errorFragment, R.id.frameLayout, true);
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        showLoadingFragment();
        logEvent();
//        progressID.setVisibility(View.VISIBLE);
        emiratesIDVerificationInputModel = new EmiratesIDVerificationInputModel();
        emiratesIDVerificationInputModel.setLang(currentLanguage);
        emiratesIDVerificationInputModel.setAppVersion(appVersion);
        emiratesIDVerificationInputModel.setOsVersion(osVersion);
        emiratesIDVerificationInputModel.setToken(token);
        emiratesIDVerificationInputModel.setChannel(channel);
        emiratesIDVerificationInputModel.setDeviceId(token);
        emiratesIDVerificationInputModel.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        emiratesIDVerificationInputModel.setEmiratesId(getCode(true));
        new VerifyEmiratesIdOCRNewService(this, emiratesIDVerificationInputModel);

    }

    //    @Override
//    public void onErrorListener(BaseResponseModel responseModel) {
//        super.onErrorListener(responseModel);
////        progressID.setVisibility(View.GONE);
//        regActivity.onBackPressed();
//    }
    private boolean isValid() {
        if (getCode(false).length() != NO_OF_DIGITS) {
            return false;
        } else
            return true;
    }

    private String getCode(boolean forService) {
        String codeEntered = "";
        if (forService) {
            codeEntered += code1.getText().toString();
            codeEntered += "-";
            codeEntered += code2.getText().toString();
            codeEntered += "-";
            codeEntered += code3.getText().toString();
            codeEntered += "-";
            codeEntered += code4.getText().toString();
        } else {
            codeEntered += code1.getText().toString();
            codeEntered += code2.getText().toString();
            codeEntered += code3.getText().toString();
            codeEntered += code4.getText().toString();
        }
        return codeEntered;
    }

    private void setListeners() {

        tv_digits = new ArrayList<EditText>();
        for (int i = 0; i < tvListIds.length; i++) {
            EditText et_temp = (EditText) mView.findViewById(tvListIds[i]);
            et_temp.addTextChangedListener(new CustomTextWatcher(et_temp));
            tv_digits.add(et_temp);
        }
        verfyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()) {
//                    onNotEligableDueToPaidAmount();
//                }
//                    regActivity.replaceFragmnet(new Registeration4Fragment(),R.id.frameLayout,true);
                    onConsumeService();
                } else
//                    showErrorFragment(getString(R.string.EID_NO));
                    mobileNoInputLayout.setError(getString(R.string.EID_NO));

            }
        });

        eIDTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (eIDTxt.getText().toString().trim().length() > 0) {
                    if (eIDTxt.getText().toString().trim().length() == NO_OF_DIGITS)
                        disableVerifyButton();
                    else
                        enableVerifyButton();
                } else {
                    disableVerifyButton();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void onEmiratesIDVerificationFail() {
        logEvent(AdjustEvents.EID_PRECESS_COMPLETE, new Bundle());
//        emiratesID = getCode(true);
//        Bundle bundle2 = new Bundle();
//
//        bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_delivery_info_eid_entered);
//        bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_delivery_info_eid_entered);
//        mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_delivery_info_eid_entered, bundle2);
//        regActivity.replaceFragmnet(new OrderSummaryFragment(), R.id.frameLayout, true);
        CommonMethods.logFirebaseEvent(mFirebaseAnalytics, ConstantUtils.TAGGING_onboarding_error_eid_failure);
        if (verifyEmiratesIdOutput.getErrorMsgEn() != null) {
            if (currentLanguage.equalsIgnoreCase("en"))
                showErrorFragment(verifyEmiratesIdOutput.getErrorMsgEn());
            else
                showErrorFragment(verifyEmiratesIdOutput.getErrorMsgAr());
        } else {
            showErrorFragment(getString(R.string.generice_error));
        }


    }


    private void enableVerifyButton() {
        verfyBtn.setAlpha(1.0f);
        verfyBtn.setEnabled(true);
    }

    private void disableVerifyButton() {
        verfyBtn.setAlpha(0.5f);
        verfyBtn.setEnabled(false);
    }

    public class CustomTextWatcher implements TextWatcher {
        private EditText view;

        private CustomTextWatcher(EditText view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

            String text = editable.toString();
            if ((tv_digits.indexOf(view) == 0) && text.length() == 3) {
                moveToNext();
            } else if ((tv_digits.indexOf(view) == 1) && text.length() == 4) {
                moveToNext();
            } else if ((tv_digits.indexOf(view) == 2) && text.length() == 7) {
                moveToNext();
            } else if ((tv_digits.indexOf(view) == 3) && text.length() == 1) {
                moveToNext();
            } else
                disableVerifyButton();
        }

        public void moveToNext() {
            view.clearFocus();

            int currentIndex = tv_digits.indexOf(view);
            if (getCode(false).length() == NO_OF_DIGITS) {
                enableVerifyButton();
            }
            if (currentIndex == 3) {
                hideKeyBoard();
                if (getCode(false).length() == NO_OF_DIGITS) {
                    enableVerifyButton();
                }
            } else {
                for (int i = currentIndex + 1; i < tv_digits.size(); i++) {
                    tv_digits.get(i).requestFocus();

                    return;

                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (NewNumberFragment.isTimerActive && CommonMethods.isTimerUp(sharedPrefrencesManger.getTimerValue())) {
            Fragment[] myFragments = {OrderFragment.orderFragmentInstance,
                    DeliveryMethodFragment.deliveryFragmentInstance,
                    DeliveryDetailsAreaFragment.deliveryDetailsAreaFragment,
                    DeliveryDetailsAddress.deliveryDetailsAddress,
                    NewNumberFragment.newNumberFragment,
                    NewNumberFragment.loadingFragmnet,
                    EmiratesIDVerificationFragment.emiratesIDVerificationFragment
            };
            regActivity.removeFragment(myFragments);
            regActivity.replaceFragmnet(new NewNumberFragment(), R.id.frameLayout, true);
            NewNumberFragment.isTimerActive = false;

        }
    }

    public void logEvent() {
        logEventInputModel.setScreenId(ConstantUtils.SCREEN_ID_3003);

        logEventInputModel.setEmiratesId(getCode(true));

        new LogApplicationFlowService(this, logEventInputModel);
    }
}
