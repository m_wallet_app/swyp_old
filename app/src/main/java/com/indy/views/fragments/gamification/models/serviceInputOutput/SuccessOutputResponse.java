package com.indy.views.fragments.gamification.models.serviceInputOutput;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by mobile on 26/10/2017.
 */

public class SuccessOutputResponse extends MasterErrorResponse {

    @SerializedName("success")
    @Expose
    private boolean success;

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
