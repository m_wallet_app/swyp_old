package com.indy.views.fragments.gamification.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterInputResponse;
import com.indy.services.BaseServiceManger;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.gamification.models.GetNotificationSettingsOutput;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**

 */
public class GetUserNotificationStatusService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private MasterInputResponse masterInputResponse;


    public GetUserNotificationStatusService(BaseInterface baseInterface, MasterInputResponse masterInputResponse) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.masterInputResponse = masterInputResponse;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<GetNotificationSettingsOutput> call = apiService.getUserNotificationSetting(masterInputResponse);
        call.enqueue(new Callback<GetNotificationSettingsOutput>() {
            @Override
            public void onResponse(Call<GetNotificationSettingsOutput> call, Response<GetNotificationSettingsOutput> response) {
                mBaseResponseModel.setServiceType(ServiceUtils.GET_USER_NOTIFICATION_SETTINGS);
                mBaseResponseModel.setResultObj(response.body());

                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (response.body().getErrorMsgEn() != null) {
//                        String urlForTag = call.request().url().toString();
//                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
//                        urlForTag = urlForTag.replace("/", "_");
//                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<GetNotificationSettingsOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.GET_USER_NOTIFICATION_SETTINGS);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
