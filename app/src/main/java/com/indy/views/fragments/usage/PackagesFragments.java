package com.indy.views.fragments.usage;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.adapters.PackagesListAdapter;
import com.indy.controls.ServiceUtils;
import com.indy.models.esimActivation.CheckESIMEligibilityRequestModel;
import com.indy.models.esimActivation.CheckESIMEligibiltyResponseModel;
import com.indy.models.packages.NewMemberShipPack;
import com.indy.models.packages.UsagePackageList;
import com.indy.models.packages.UsagePackagesoutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.models.utils.MasterInputResponse;
import com.indy.services.GetESIMEligibilityService;
import com.indy.services.GetNewPackagesListService;
import com.indy.services.GetPackageListService;
import com.indy.squad_number.listeners.UpdateSquadNumberListener;
import com.indy.squad_number.models.Squad;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.SwpeMainActivity;
import com.indy.views.fragments.AboutUs.InviteFriendFragment;
import com.indy.views.fragments.BenefitsFragment;
import com.indy.views.fragments.NewMembershipBenefitsFragment;
import com.indy.views.fragments.NewMembershipUpgradeBenefitsFragment;
import com.indy.views.fragments.esim.ActivateYourESIMFragment;
import com.indy.views.fragments.store.mainstore.MyStoreFragment;
import com.indy.views.fragments.utils.MasterFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by emad on 8/9/16.
 */
public class PackagesFragments extends MasterFragment {
    private View view;
    private RecyclerView mRecyclerView;
    private GridLayoutManager mGridLayoutManger;
    private List<UsagePackageList> usagePackageList;
    private NewMemberShipPack newMemberPackageList;
    private MasterInputResponse masterInputResponse;
    private UsagePackagesoutput usagePackagesoutput;
    private PackagesListAdapter packagesListAdapter;

    private UsagePackageList emptyUsagePackageListlItem;
    LinearLayout Lv_packagesView, ll_no_membership, ll_bottom_packages;
    TextView tv_invitFriendsm;
    TextView tv_overlay;
    TextView tv_addPackages, tv_membership_cycle;
    ProgressBar progressBar;
    public boolean isMembershipValid = true;
    private Button bt_overLay, bt_esim_overlay;
    private TextView btn_membership;
    private LinearLayout rl_membership;
    private LinearLayout ll_progressLinear;
    private LinearLayout ll_activate_esim;
    private LinearLayout ll_new_membership;

    private ImageView iv_removeBox;
    boolean showBlackBox = false;
    boolean showESIMBox = false;
    boolean showNewMembershipBox = false;
    boolean showMembershipPackBox = false;
    //---------
    LinearLayout ll_dummyContainer;
    TextView bt_retry;
    TextView tv_error;
    //------
    public int status = 0;
    public static boolean packagesDisabled = false;
    ImageView iv_overlay;
    public static boolean isAnimationAllowed = false;
    private String sim_status = "failure";
    public static PackagesFragments instance;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_packages_layout, container, false);
        initUI();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void onResume() {
        super.onResume();
        if (sharedPrefrencesManger.getESIMBitmapCachedFlag()) {
            if (usagePackageList != null) {
                usagePackageList.clear();
                if (packagesListAdapter != null)
                    packagesListAdapter.notifyDataSetChanged();
            }
            ll_activate_esim.setVisibility(View.GONE);
            sim_status = "failure";
            showESIMBox = false;
            getPackagesList();
        }
    }

    public void refresh() {

        onResume();
    }

    public void setSquadNumber(){

    }

    @Override
    public void initUI() {
        super.initUI();
        btn_membership = (TextView) view.findViewById(R.id.membershipBtn);
        ll_no_membership = (LinearLayout) view.findViewById(R.id.ll_no_membership);
        tv_membership_cycle = (TextView) view.findViewById(R.id.tv_membership_cycle);
        Lv_packagesView = (LinearLayout) view.findViewById(R.id.Lv_packagesView);
        rl_membership = (LinearLayout) view.findViewById(R.id.rl_get_membership);
        ll_activate_esim = (LinearLayout) view.findViewById(R.id.ll_activate_esim);
        ll_new_membership = (LinearLayout) view.findViewById(R.id.ll_new_membership);
        ll_bottom_packages = (LinearLayout) view.findViewById(R.id.ll_bottom_packages);
        tv_overlay = (TextView) view.findViewById(R.id.tv_overlay);
        bt_overLay = (Button) view.findViewById(R.id.bt_overlay);
        bt_esim_overlay = (Button) view.findViewById(R.id.bt_esim_overlay);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        ll_dummyContainer = (LinearLayout) view.findViewById(R.id.ll_dummy_error_container);
        ll_dummyContainer.setVisibility(View.GONE);
        bt_retry = (TextView) view.findViewById(R.id.retryBtn);
        bt_retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onConsumeService();
            }
        });
        tv_error = (TextView) view.findViewById(R.id.tv_error);
//        iv_removeBox.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                rl_membership.setVisibility(View.GONE);
//                mRecyclerView.setVisibility(View.VISIBLE);
//                ll_bottom_packages.setVisibility(View.VISIBLE);
//            }
//        });
//        isMembershipValid = false;
        packagesDisabled = false;
        instance = this;
        rl_membership.setVisibility(View.GONE);
        ll_activate_esim.setVisibility(View.GONE);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mGridLayoutManger = new GridLayoutManager(getActivity(), 2);
        mGridLayoutManger.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (usagePackageList != null) {
                    if ((usagePackageList.get(position).getPackageType() == -2) || usagePackageList.get(position).getPackageType() == -3 || usagePackageList.get(position).getPackageType() == -4 || usagePackageList.get(position).getPackageType() == 4) {
                        return 2;
                    } else {
                        return 1;
                    }
                } else {
                    return 1;
                }
            }
        });
        mRecyclerView.setLayoutManager(mGridLayoutManger);
        mRecyclerView.setHasFixedSize(true);

        btn_membership.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //dont happen yet
//                Intent intent = new Intent(getActivity(), BenefitsFragment.class);
//                startActivity(intent);


                ((SwpeMainActivity) getActivity()).replaceFragmnet(new NewMembershipUpgradeBenefitsFragment(), R.id.contentFrameLayout, true);
            }
        });
        tv_addPackages = (TextView) view.findViewById(R.id.confirmBtn);
        tv_addPackages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle2 = new Bundle();

                bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_usage_shop_more);
                bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_usage_shop_more);
                mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_usage_shop_more, bundle2);
                ((SwpeMainActivity) getActivity()).replaceFragmnet(new MyStoreFragment(), R.id.contentFrameLayout, true);
                SwpeMainActivity.usageTxtID.setText(getString(R.string.store_capital_header));
            }
        });

        tv_invitFriendsm = (TextView) view.findViewById(R.id.tv_inviteFriends);
        tv_invitFriendsm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle2 = new Bundle();

                bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_usage_invite_friend);
                bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_usage_invite_friend);
                mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_usage_invite_friend, bundle2);
                startActivity(new Intent(getActivity(), InviteFriendFragment.class));

            }
        });
        ll_progressLinear = (LinearLayout) view.findViewById(R.id.ll_progressBar);

        if (sharedPrefrencesManger.getUserProfileObject() != null && sharedPrefrencesManger.getUserProfileObject().getMembershipData() != null && sharedPrefrencesManger.getUserProfileObject().getMembershipData().getStatus() != null) {
            status = sharedPrefrencesManger.getUserProfileObject().getMembershipData().getStatus();
//            status = 2;
            isMembershipValid = (status != 1 || status != 3);
        }

        if (isMembershipValid) {
            onConsumeService();
//            onMockSuccessListener();
        } else {
//            onMockSuccessListener();
            getESIMEligibility();
            initMembershipLayout();
        }
        showConfirmView(status);
    }

    private void initMembershipLayout() {
//        ll_noMembershipView.setVisibility(View.VISIBLE);
//        ll_viewPager.setVisibility(View.GONE);
        ll_no_membership.setVisibility(View.VISIBLE);
        if (sharedPrefrencesManger.isVATEnabled()) {
            Double price = Double.parseDouble(sharedPrefrencesManger.getAfterLoginPrice());
            tv_membership_cycle.setText(getString(R.string.you_dont_have_a_swyp_membership));
        } else {
            tv_membership_cycle.setText(getString(R.string.you_dont_have_a_swyp_membership));

        }
        Lv_packagesView.setVisibility(View.GONE);
        ll_bottom_packages.setVisibility(View.GONE);
    }

    public void getPackagesList() {
        masterInputResponse = new MasterInputResponse();
        masterInputResponse.setAppVersion(appVersion);
        masterInputResponse.setToken(token);
        masterInputResponse.setOsVersion(osVersion);
        masterInputResponse.setLang(currentLanguage);
        masterInputResponse.setChannel(channel);
        masterInputResponse.setDeviceId(deviceId);
        masterInputResponse.setMsisdn(sharedPrefrencesManger.getMobileNo());
        masterInputResponse.setAuthToken(authToken);
        new GetNewPackagesListService(this, masterInputResponse);
    }

    private void onMockSuccessListener() {

        try {
            ll_progressLinear.setVisibility(View.GONE);
            ll_dummyContainer.setVisibility(View.GONE);

            sim_status = "success";

            if (isMembershipValid && !packagesDisabled) {
                if (!(ll_no_membership.getVisibility() == View.VISIBLE))
                    getPackagesList();
                if (sim_status != null && sim_status.length() > 0 && sim_status.equalsIgnoreCase("success")) {
                    showESIMBox = true;
                } else {
                    showESIMBox = false;
                }
            }
            if (ll_no_membership.getVisibility() == View.VISIBLE) {
                if (sim_status != null && sim_status.length() > 0 && sim_status.equalsIgnoreCase("success")) {
                    ll_activate_esim.setVisibility(View.VISIBLE);
                    bt_esim_overlay.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ((SwpeMainActivity) getActivity()).replaceFragmnet(new ActivateYourESIMFragment(), R.id.contentFrameLayout, true);
                        }
                    });
                } else {
                    ll_activate_esim.setVisibility(View.GONE);
                }
            } else {
                if (sim_status != null && sim_status.length() > 0 && sim_status.equalsIgnoreCase("success")) {
                    if (!showESIMBox)
                        ll_activate_esim.setVisibility(View.VISIBLE);
                    else

                        ll_activate_esim.setVisibility(View.GONE);
                    bt_esim_overlay.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ((SwpeMainActivity) getActivity()).replaceFragmnet(new ActivateYourESIMFragment(), R.id.contentFrameLayout, true);
                        }
                    });
                } else {
                    ll_activate_esim.setVisibility(View.GONE);
                }
            }

        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);

        try {
            ll_progressLinear.setVisibility(View.GONE);
            ll_dummyContainer.setVisibility(View.GONE);
            if (responseModel != null && responseModel.getResultObj() != null) {
                if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.packageServiceType)) {
                    getPackagesList(responseModel);
                }

                if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.get_esim_eligibility_service)) {
                    CheckESIMEligibiltyResponseModel faqsResponseModel = (CheckESIMEligibiltyResponseModel) responseModel.getResultObj();
                    sim_status = faqsResponseModel.getResponseMsg();
                    if (isMembershipValid && !packagesDisabled) {
                        if (!(ll_no_membership.getVisibility() == View.VISIBLE)) {
                            getPackagesList();
                        }
                        if (sim_status != null && sim_status.length() > 0 && sim_status.equalsIgnoreCase("success")) {
                            showESIMBox = true;
                        } else {
                            showESIMBox = false;
                        }
                    }
                    if (ll_no_membership.getVisibility() == View.VISIBLE) {
                        if (sim_status != null && sim_status.length() > 0 && sim_status.equalsIgnoreCase("success")) {
                            if (!showESIMBox)
                                ll_activate_esim.setVisibility(View.VISIBLE);
                            else

                                ll_activate_esim.setVisibility(View.GONE);
                            bt_esim_overlay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    ((SwpeMainActivity) getActivity()).replaceFragmnet(new ActivateYourESIMFragment(), R.id.contentFrameLayout, true);
                                }
                            });
                        } else {
                            ll_activate_esim.setVisibility(View.GONE);
                        }
                    } else {
                        if (sim_status != null && sim_status.length() > 0 && sim_status.equalsIgnoreCase("success")) {
                            if (!showESIMBox)
                                ll_activate_esim.setVisibility(View.VISIBLE);
                            else

                                ll_activate_esim.setVisibility(View.GONE);
                            bt_esim_overlay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    ((SwpeMainActivity) getActivity()).replaceFragmnet(new ActivateYourESIMFragment(), R.id.contentFrameLayout, true);
                                }
                            });
                        } else {
                            ll_activate_esim.setVisibility(View.GONE);
                        }
                    }
                }
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        ll_progressLinear.setVisibility(View.GONE);
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
//        super.onErrorListener(responseModel);
        ll_progressLinear.setVisibility(View.GONE);
        ll_dummyContainer.setVisibility(View.GONE);
        try {
            if (responseModel != null && responseModel.getResultObj() != null) {
                if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.get_esim_eligibility_service)) {
                    getPackagesList();
                } else if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.packageServiceType)) {
                    ll_progressLinear.setVisibility(View.GONE);
                    onGetPackagesError();
                }
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    private void getPackagesList(BaseResponseModel responseModel) {
        usagePackagesoutput = (UsagePackagesoutput) responseModel.getResultObj();
        if (usagePackagesoutput != null) {
            if (usagePackagesoutput.getErrorCode() != null)
                onGetPackagesError();
            else
                onGetPackageSucess();
        }
    }

    private void onGetPackageSucess() {
        if (isVisible()) {
            usagePackageList = usagePackagesoutput.getUsagePackageList();
            if (usagePackagesoutput.getNewMemberShipPack() != null && usagePackagesoutput.getNewMemberShipPack().getUsagePackageList() != null && usagePackagesoutput.getNewMemberShipPack().getUsagePackageList().size() > 0) {
                newMemberPackageList = usagePackagesoutput.getNewMemberShipPack();
            }
            if (usagePackagesoutput.getNewMemberShipPack() == null) {
                showNewMembershipBox = true;
            } else {
                showNewMembershipBox = false;
            }

            if (usagePackageList != null)
                if (usagePackageList.size() > 0 || newMemberPackageList != null) {

                    Lv_packagesView.setVisibility(View.VISIBLE);
                    getOptimizedArray();
                    isAnimationAllowed = true;

//                    addDummyPackages();

                    for (int i = 0; i < ((usagePackageList.size() > 5) ? 5 : usagePackageList.size()); i++) {
                        if (usagePackageList.get(i).getPackageType() == -2 || usagePackageList.get(i).getPackageType() == -3 || usagePackageList.get(i).getPackageType() == -4 || usagePackageList.get(i).getPackageType() == 4) {
                            usagePackageList.remove(i);
                        }
                    }

                    if (showBlackBox) {
                        UsagePackageList usagePackageListObj = new UsagePackageList();
                        usagePackageListObj.setPackageType(-2);
                        usagePackageListObj.setId("-2");
                        usagePackageListObj.setNameEn("dummy");
                        usagePackageListObj.setDescriptionAr("desc");
                        usagePackageListObj.setDescriptionEn("desc");
                        usagePackageListObj.setEndDate("sdas");
                        usagePackageListObj.setIconUrl("google.com");
                        usagePackageListObj.setImageUrl("google.com");
                        usagePackageListObj.setIsAnimated(0);
                        usagePackageListObj.setOfferList(null);
                        usagePackageList.add(0, usagePackageListObj);
                    } else {
                        if (showNewMembershipBox) {
                            UsagePackageList usagePackageListObj = new UsagePackageList();
                            usagePackageListObj.setPackageType(-4);
                            usagePackageListObj.setId("-4");
                            usagePackageListObj.setNameEn("dummy");
                            usagePackageListObj.setDescriptionAr("desc");
                            usagePackageListObj.setDescriptionEn("desc");
                            usagePackageListObj.setEndDate("sdas");
                            usagePackageListObj.setIconUrl("google.com");
                            usagePackageListObj.setImageUrl("google.com");
                            usagePackageListObj.setIsAnimated(0);
                            usagePackageListObj.setOfferList(null);
                            usagePackageList.add(0, usagePackageListObj);
                        } else {
                            UsagePackageList usagePackageListObj = new UsagePackageList();
                            usagePackageListObj.setPackageType(4);
                            usagePackageListObj.setId("4");
                            usagePackageListObj.setNameEn("dummy");
                            usagePackageListObj.setDescriptionAr("desc");
                            usagePackageListObj.setDescriptionEn("desc");
                            usagePackageListObj.setEndDate("sdas");
                            usagePackageListObj.setIconUrl("google.com");
                            usagePackageListObj.setImageUrl("google.com");
                            usagePackageListObj.setIsAnimated(0);
                            usagePackageListObj.setOfferList(null);
                            usagePackageList.add(0, usagePackageListObj);
                        }
                    }
                    if (showESIMBox) {
                        UsagePackageList usagePackageListObj = new UsagePackageList();
                        usagePackageListObj.setPackageType(-3);
                        usagePackageListObj.setId("-3");
                        usagePackageListObj.setNameEn("dummy");
                        usagePackageListObj.setDescriptionAr("desc");
                        usagePackageListObj.setDescriptionEn("desc");
                        usagePackageListObj.setEndDate("sdas");
                        usagePackageListObj.setIconUrl("google.com");
                        usagePackageListObj.setImageUrl("google.com");
                        usagePackageListObj.setIsAnimated(0);
                        usagePackageListObj.setOfferList(null);
                        usagePackageList.add(0, usagePackageListObj);
                    }


//                    if (!showESIMBox) {
//                        if (usagePackageList.get(0).getPackageType() != -2 && showBlackBox) {
//                            UsagePackageList usagePackageListObj = new UsagePackageList();
//                            usagePackageListObj.setPackageType(-2);
//                            usagePackageListObj.setId("-2");
//                            usagePackageListObj.setNameEn("dummy");
//                            usagePackageListObj.setDescriptionAr("desc");
//                            usagePackageListObj.setDescriptionEn("desc");
//                            usagePackageListObj.setEndDate("sdas");
//                            usagePackageListObj.setIconUrl("google.com");
//                            usagePackageListObj.setImageUrl("google.com");
//                            usagePackageListObj.setIsAnimated(0);
//                            usagePackageListObj.setOfferList(null);
//                            usagePackageList.add(0, usagePackageListObj);
//                        }
//                    }
// else {
//                        if (usagePackageList.get(0).getPackageType() == -2 || usagePackageList.get(0).getPackageType() == -3) {
//                            usagePackageList.remove(0);
//                        }
//                        if (usagePackageList.get(0).getPackageType() == -2 || usagePackageList.get(0).getPackageType() == -3) {
//                            usagePackageList.remove(0);
//                        }
//
//                        if (showBlackBox) {
//                            UsagePackageList usagePackageListObj = new UsagePackageList();
//                            usagePackageListObj.setPackageType(-3);
//                            usagePackageListObj.setId("-3");
//                            usagePackageListObj.setNameEn("dummy");
//                            usagePackageListObj.setDescriptionAr("desc");
//                            usagePackageListObj.setDescriptionEn("desc");
//                            usagePackageListObj.setEndDate("sdas");
//                            usagePackageListObj.setIconUrl("google.com");
//                            usagePackageListObj.setImageUrl("google.com");
//                            usagePackageListObj.setIsAnimated(0);
//                            usagePackageListObj.setOfferList(null);
//                            usagePackageList.add(0, usagePackageListObj);
//
//                            UsagePackageList usagePackageListObj2 = new UsagePackageList();
//                            usagePackageListObj2.setPackageType(-2);
//                            usagePackageListObj2.setId("-2");
//                            usagePackageListObj2.setNameEn("dummy");
//                            usagePackageListObj2.setDescriptionAr("desc");
//                            usagePackageListObj2.setDescriptionEn("desc");
//                            usagePackageListObj2.setEndDate("sdas");
//                            usagePackageListObj2.setIconUrl("google.com");
//                            usagePackageListObj2.setImageUrl("google.com");
//                            usagePackageListObj2.setIsAnimated(0);
//                            usagePackageListObj2.setOfferList(null);
//                            usagePackageList.add(1, usagePackageListObj2);
//                        } else {
//                            if (usagePackageList.get(0).getPackageType() == -3 || usagePackageList.get(0).getPackageType() == -4) {
//                                usagePackageList.remove(0);
//                            }
//                            if (usagePackageList.get(0).getPackageType() == -3 || usagePackageList.get(0).getPackageType() == -4) {
//                                usagePackageList.remove(0);
//                            }
//                            if (showNewMembershipBox) {
//                                UsagePackageList usagePackageListObj = new UsagePackageList();
//                                usagePackageListObj.setPackageType(-4);
//                                usagePackageListObj.setId("-4");
//                                usagePackageListObj.setNameEn("dummy");
//                                usagePackageListObj.setDescriptionAr("desc");
//                                usagePackageListObj.setDescriptionEn("desc");
//                                usagePackageListObj.setEndDate("sdas");
//                                usagePackageListObj.setIconUrl("google.com");
//                                usagePackageListObj.setImageUrl("google.com");
//                                usagePackageListObj.setIsAnimated(0);
//                                usagePackageListObj.setOfferList(null);
//                                usagePackageList.add(0, usagePackageListObj);
//
//                                UsagePackageList usagePackageListObj2 = new UsagePackageList();
//                                usagePackageListObj2.setPackageType(-3);
//                                usagePackageListObj2.setId("-3");
//                                usagePackageListObj2.setNameEn("dummy");
//                                usagePackageListObj2.setDescriptionAr("desc");
//                                usagePackageListObj2.setDescriptionEn("desc");
//                                usagePackageListObj2.setEndDate("sdas");
//                                usagePackageListObj2.setIconUrl("google.com");
//                                usagePackageListObj2.setImageUrl("google.com");
//                                usagePackageListObj2.setIsAnimated(0);
//                                usagePackageListObj2.setOfferList(null);
//                                usagePackageList.add(1, usagePackageListObj2);
//
//
//                            } else {
//                                UsagePackageList usagePackageListObj = new UsagePackageList();
//                                usagePackageListObj.setPackageType(-3);
//                                usagePackageListObj.setId("-3");
//                                usagePackageListObj.setNameEn("dummy");
//                                usagePackageListObj.setDescriptionAr("desc");
//                                usagePackageListObj.setDescriptionEn("desc");
//                                usagePackageListObj.setEndDate("sdas");
//                                usagePackageListObj.setIconUrl("google.com");
//                                usagePackageListObj.setImageUrl("google.com");
//                                usagePackageListObj.setIsAnimated(0);
//                                usagePackageListObj.setOfferList(null);
//                                usagePackageList.add(0, usagePackageListObj);
//                            }
//                        }
//                    }

                    packagesListAdapter = new PackagesListAdapter(usagePackageList, newMemberPackageList, getActivity(), sharedPrefrencesManger, (SwpeMainActivity) getActivity(), showESIMBox, showNewMembershipBox);

                    mRecyclerView.setAdapter(packagesListAdapter);
                } else {
                    noMemberShip(getString(R.string.get_membership_small));
                }
        }
    }

    private void noMemberShip(String message) {
        rl_membership.setVisibility(View.GONE);
        initMembershipLayout();
        String text = getString(R.string.current_packages_will_not_be_renewed);
        tv_overlay.setText(text);
        bt_overLay.setText(message);
        bt_overLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((SwpeMainActivity) getActivity()).replaceFragmnet(new NewMembershipUpgradeBenefitsFragment(), R.id.contentFrameLayout, true);
            }
        });

        packagesDisabled = true;
        mRecyclerView.setEnabled(false);
        mRecyclerView.setAlpha(0.4f);
        mRecyclerView.setClickable(false);
        tv_addPackages.setEnabled(false);
        tv_addPackages.setAlpha(0.4f);
        showBlackBox = false;
        if (sim_status.equalsIgnoreCase("success")) {
            ll_activate_esim.setVisibility(View.VISIBLE);
        }
    }

    private void onGetPackagesError() {
        Lv_packagesView.setVisibility(View.GONE);
        if (usagePackagesoutput != null) {
            if (usagePackagesoutput.getErrorCode() != null && usagePackagesoutput.getErrorCode().equals("3000031")) {
                noMemberShip(sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english) ?
                        usagePackagesoutput.getErrorMsgEn() : usagePackagesoutput.getErrorMsgAr());
                return;
            } else {
                tv_error.setText(sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english) ?
                        usagePackagesoutput.getErrorMsgEn() : usagePackagesoutput.getErrorMsgAr());
            }
        } else {
            tv_error.setText(getString(R.string.generice_error));
        }
        ll_dummyContainer.setVisibility(View.VISIBLE);
    }

    public void getOptimizedArray() {

//        usagePackageList;//
        ArrayList<UsagePackageList> temp = new ArrayList<UsagePackageList>();
        ArrayList<Integer> counts = new ArrayList<Integer>();
        for (int i = 0; i < usagePackageList.size(); i++) {
            if (usagePackageList.get(i).getPackageType() == 1) {
                temp.add(usagePackageList.get(i));
            } else {
                counts.add(i);
            }
            if (counts.size() == 2) {
                temp.add(usagePackageList.get(counts.get(0)));
                temp.add(usagePackageList.get(counts.get(1)));
                counts.clear();
            }
        }
        for (int i = 0; i < counts.size(); i++) {
            temp.add(usagePackageList.get(counts.get(i)));
        }

//        Collections.sort(temp, new Comparator<UsagePackageList>() {
//            @Override
//            public int compare(UsagePackageList usagePackageList, UsagePackageList t1) {
//                if(usagePackageList.getPackageType()<t1.getPackageType()){
//                    return 1;
//                }else{
//                    return 0;
//                }
//            }
//        });

        for (int i = 0; i < temp.size(); i++) {
            if (temp.get(i).getPackageType() == 1) {
                UsagePackageList item = temp.get(i);
                UsagePackageList item2 = temp.get(0);
                temp.set(i, item2);
                temp.set(0, item);
                break;
            }
        }
        usagePackageList = temp;
    }


    @Override
    public void onConsumeService() {
        super.onConsumeService();
        ll_progressLinear.setVisibility(View.VISIBLE);
        CheckESIMEligibilityRequestModel checkESIMEligibilityRequestModel = new CheckESIMEligibilityRequestModel();
        checkESIMEligibilityRequestModel.setAppVersion(appVersion);
        checkESIMEligibilityRequestModel.setToken(token);
        checkESIMEligibilityRequestModel.setOsVersion(osVersion);
        checkESIMEligibilityRequestModel.setLang(currentLanguage);
        checkESIMEligibilityRequestModel.setChannel(channel);
        checkESIMEligibilityRequestModel.setDeviceId(deviceId);
        checkESIMEligibilityRequestModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        checkESIMEligibilityRequestModel.setAuthToken(authToken);
        checkESIMEligibilityRequestModel.setAdditionalInfo(ConstantUtils.FIREBASE_TOKEN_KEY, sharedPrefrencesManger.getToken());
        new GetESIMEligibilityService(this, checkESIMEligibilityRequestModel);
    }

    private void getESIMEligibility() {
        ll_progressLinear.setVisibility(View.VISIBLE);
        CheckESIMEligibilityRequestModel checkESIMEligibilityRequestModel = new CheckESIMEligibilityRequestModel();
        checkESIMEligibilityRequestModel.setAppVersion(appVersion);
        checkESIMEligibilityRequestModel.setToken(token);
        checkESIMEligibilityRequestModel.setOsVersion(osVersion);
        checkESIMEligibilityRequestModel.setLang(currentLanguage);
        checkESIMEligibilityRequestModel.setChannel(channel);
        checkESIMEligibilityRequestModel.setDeviceId(deviceId);
        checkESIMEligibilityRequestModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        checkESIMEligibilityRequestModel.setAuthToken(authToken);
        checkESIMEligibilityRequestModel.setAdditionalInfo(ConstantUtils.FIREBASE_TOKEN_KEY, sharedPrefrencesManger.getToken());
        new GetESIMEligibilityService(this, checkESIMEligibilityRequestModel);
    }

    public void showConfirmView(int mode) {
        String text = "";

        switch (mode) {
            case 4://packages: true, membership: false
                rl_membership.setVisibility(View.GONE);
                ll_no_membership.setVisibility(View.GONE);
                mRecyclerView.setVisibility(View.VISIBLE);
                text = getString(R.string.current_packages_will_not_be_renewed);
                tv_overlay.setText(text);
                bt_overLay.setText(getString(R.string.renew_now));
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_usage_membership_failed);

                bt_overLay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ((SwpeMainActivity) getActivity()).replaceFragmnet(new NewMembershipUpgradeBenefitsFragment(), R.id.contentFrameLayout, true);
                    }
                });
                ll_bottom_packages.setVisibility(View.GONE);
                packagesDisabled = false;
                mRecyclerView.setEnabled(false);
                mRecyclerView.setAlpha(1.0f);
                mRecyclerView.setClickable(false);
//                tv_addPackages.setEnabled(false);
                tv_addPackages.setAlpha(1.0f);
                showBlackBox = true;
                break;

            case 3://packages:false, membership: false
                rl_membership.setVisibility(View.VISIBLE);
                ll_no_membership.setVisibility(View.GONE);
                Lv_packagesView.setVisibility(View.GONE);
                if (sim_status != null && sim_status.equalsIgnoreCase("success"))
                    ll_activate_esim.setVisibility(View.VISIBLE);
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_usage_membership_failed);

                text = getString(R.string.current_packages_will_not_be_renewed);
                tv_overlay.setText(text);
                bt_overLay.setText(getString(R.string.get_membership_small));
                bt_overLay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ((SwpeMainActivity) getActivity()).replaceFragmnet(new NewMembershipUpgradeBenefitsFragment(), R.id.contentFrameLayout, true);
                    }
                });

                packagesDisabled = true;
                mRecyclerView.setEnabled(false);
                mRecyclerView.setAlpha(0.4f);
                mRecyclerView.setClickable(false);
                tv_addPackages.setEnabled(false);
                tv_addPackages.setAlpha(0.4f);
                showBlackBox = true;
                break;
            default:
                break;
        }

    }

    public void onSetNewSquadNumber(ArrayList<Squad> squads) {
        if (usagePackagesoutput.getNewMemberShipPack() != null && usagePackagesoutput.getNewMemberShipPack().getSquadNumber() != null) {
            usagePackagesoutput.getNewMemberShipPack().getSquadNumber().setSquad(squads);
        }
        onGetPackageSucess();
    }

    public void onDeleteSquadNumber() {
        if (usagePackagesoutput.getNewMemberShipPack() != null  && usagePackagesoutput.getNewMemberShipPack().getSquadNumber() != null) {
            usagePackagesoutput.getNewMemberShipPack().getSquadNumber().getSquad().remove(0);
        }
        onGetPackageSucess();
    }
}
