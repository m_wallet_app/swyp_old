package com.indy.views.fragments.gamification.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by mobile on 23/10/2017.
 */

public class UpdateUserNotificationSettingOutputResponse extends MasterErrorResponse {

    @SerializedName("flagUpdated")
    @Expose
    private boolean flagUpdated;

    public boolean isFlagUpdated() {
        return flagUpdated;
    }

    public void setFlagUpdated(boolean flagUpdated) {
        this.flagUpdated = flagUpdated;
    }
}
