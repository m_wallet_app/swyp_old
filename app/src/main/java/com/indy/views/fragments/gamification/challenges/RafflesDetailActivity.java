package com.indy.views.fragments.gamification.challenges;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.MasterActivity;
import com.indy.views.fragments.gamification.adapters.RafflesDetailAdapter;
import com.indy.views.fragments.gamification.models.getrafflesdetails.GetRafflesDetailsInput;
import com.indy.views.fragments.gamification.models.getrafflesdetails.GetRafflesDetailsOutputResponse;
import com.indy.views.fragments.gamification.models.getraffleslist.Raffle;
import com.indy.views.fragments.gamification.services.GetRaffleDetailsService;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;

public class RafflesDetailActivity extends MasterActivity {
    RelativeLayout rl_back, rl_help;
    WebView wv_raffleDetails;
    private TextView tv_header;
    private Raffle raffle;
    private TextView tv_title, tv_subTitle;
    private GetRafflesDetailsOutputResponse getRafflesDetailsOutputResponse;
    RecyclerView rv_winners;
    private FrameLayout frameLayout;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_raffles_detail);
        raffle = (Raffle) getIntent().getParcelableExtra(ConstantUtils.KEY_RAFFLE_MONTH);
        initUI();
    }

    @Override
    public void initUI() {
        super.initUI();
        tv_header = (TextView) findViewById(R.id.titleTxt);
        tv_header.setText(getResources().getString(R.string.results));
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_subTitle = (TextView) findViewById(R.id.tv_winning_text) ;
        rl_back = (RelativeLayout) findViewById(R.id.backLayout);
        rl_help = (RelativeLayout) findViewById(R.id.helpLayout);
        rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        rl_help.setVisibility(View.INVISIBLE);
        rv_winners = (RecyclerView) findViewById(R.id.rv_winners);
        frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
        onConsumeService();
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        addFragmnet(new LoadingFragmnet(), R.id.frameLayout,true);
        frameLayout.setVisibility(View.VISIBLE);
        GetRafflesDetailsInput getRafflesDetailsInput= new GetRafflesDetailsInput();
        getRafflesDetailsInput.setChannel(channel);
        getRafflesDetailsInput.setLang(currentLanguage);
        getRafflesDetailsInput.setMsisdn(sharedPrefrencesManger.getMobileNo());
        getRafflesDetailsInput.setAppVersion(appVersion);
        getRafflesDetailsInput.setAuthToken(authToken);
        getRafflesDetailsInput.setDeviceId(deviceId);
        getRafflesDetailsInput.setToken(token);
        getRafflesDetailsInput.setUserSessionId(sharedPrefrencesManger.getUserSessionId());
        getRafflesDetailsInput.setUserId(sharedPrefrencesManger.getUserId());
        getRafflesDetailsInput.setApplicationId(ConstantUtils.INDY_TALOS_ID);
        getRafflesDetailsInput.setImsi(sharedPrefrencesManger.getMobileNo());
        getRafflesDetailsInput.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        getRafflesDetailsInput.setOsVersion(osVersion);
        getRafflesDetailsInput.setRaffleEventId(raffle.getIndyRaffleEventId());
        new GetRaffleDetailsService(this, getRafflesDetailsInput);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        onBackPressed();
        if(responseModel!=null && responseModel.getResultObj()!=null) {
            getRafflesDetailsOutputResponse = (GetRafflesDetailsOutputResponse) responseModel.getResultObj();
            if(getRafflesDetailsOutputResponse!=null){
                onGetRaffleDetailsSuccess();
            }else{
                onGetRaffleDetailsFailure();
            }
        }else{
            onGetRaffleDetailsFailure();
        }
    }



    private void onGetRaffleDetailsFailure() {
        showErrorFragment();
    }

    public void showErrorFragment(){
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        if(getRafflesDetailsOutputResponse!=null && getRafflesDetailsOutputResponse.getErrorMsgEn()!=null){
            if(currentLanguage.equalsIgnoreCase(ConstantUtils.lang_english)){
                bundle.putString(ConstantUtils.errorString,getRafflesDetailsOutputResponse.getErrorMsgEn());

            }else{
                bundle.putString(ConstantUtils.errorString,getRafflesDetailsOutputResponse.getErrorMsgAr());
            }
        }else{
            bundle.putString(ConstantUtils.errorString,getString(R.string.generice_error));
        }
        errorFragment.setArguments(bundle);
        addFragmnet(errorFragment,R.id.frameLayout,true);
    }

    private void onGetRaffleDetailsSuccess() {
        tv_title.setText(getRafflesDetailsOutputResponse.getRaffleName());
        if(getRafflesDetailsOutputResponse.isUserWon()){
            tv_subTitle.setText(getString(R.string.you_are_a_winner));
        }else{

            tv_subTitle.setText(getString(R.string.sorry_good_luck_next_time));
        }

        if(getRafflesDetailsOutputResponse.getPrizes()!=null && getRafflesDetailsOutputResponse.getPrizes().size()>0){
            RafflesDetailAdapter rafflesDetailAdapter = new RafflesDetailAdapter(getRafflesDetailsOutputResponse.getPrizes(),this);
            rv_winners.setLayoutManager(new LinearLayoutManager(this));
            rv_winners.setHasFixedSize(true);
            rv_winners.setAdapter(rafflesDetailAdapter);
        }else{
            tv_subTitle.setText(getString(R.string.winners_will_be_announced_soon));
        }
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sharedPrefrencesManger.getNavigationIndex().length() > 0 || sharedPrefrencesManger.getNavigationBadgeIndex().length() >0) {

            finish();
        }
    }

    @Override
    public void onBackPressed() {
        if(frameLayout.getVisibility()==View.VISIBLE){
            frameLayout.setVisibility(View.GONE);
            super.onBackPressed();
        }else{
            super.onBackPressed();
        }
    }
}
