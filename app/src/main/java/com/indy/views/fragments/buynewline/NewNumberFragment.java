package com.indy.views.fragments.buynewline;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.adapters.MsisdenListAdapter;
import com.indy.controls.AdjustEvents;
import com.indy.controls.OnLockNumberInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.faqs.FaqsInputModel;
import com.indy.models.faqs.FaqsResponseModel;
import com.indy.models.inilizeregisteration.InilizeRegisteratonOutput;
import com.indy.models.locaknumber.LockMobileNumberInput;
import com.indy.models.locaknumber.LockMobileNumberOutput;
import com.indy.models.msisdn.MsisdenListInput;
import com.indy.models.msisdn.MsisdenListResponse;
import com.indy.models.msisdn.MsisdnList;
import com.indy.models.onboarding_push_notifications.InitializeServiceRequestModel;
import com.indy.models.search_msisdn_model.SearchMsisdnInputModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterInputResponse;
import com.indy.services.FaqsService;
import com.indy.services.LockNumberService;
import com.indy.services.LogApplicationFlowService;
import com.indy.services.MsisdnService;
import com.indy.services.NewGetRegistrationIDService;
import com.indy.services.SearchMsisdnService;
import com.indy.services.UnLockNumberService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.buynewline.exisitingnumber.NumberTransferFragment;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.indy.views.fragments.utils.MasterFragment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import it.gmariotti.recyclerview.adapter.AlphaAnimatorAdapter;

import static com.indy.R.id.iv_deleteText;
import static com.indy.utils.ConstantUtils.TAGGING_migration_selected;
import static com.indy.utils.ConstantUtils.TAGGING_on_boarding_select_number;
import static com.indy.utils.ConstantUtils.TAGGING_onboarding_select_number;
import static com.indy.views.activites.RegisterationActivity.logEventInputModel;

//import it.gmariotti.recyclerview.adapter.AlphaAnimatorAdapter;


/**
 * Created by emad on 8/2/16.
 */
public class NewNumberFragment extends MasterFragment implements OnLockNumberInterface {
    private View view;
    private RecyclerView my_recycler_view;
    MsisdenListAdapter msisdenListAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<MsisdnList> msisdnLists;
    //    private ImageView freshNo;
    private int startIndex = 0, endIndex = 5;
    //    private ProgressBar progressID;
    LockMobileNumberInput lockMobileNumberInput;
    LockMobileNumberOutput lockMobileNumberOutput;
    private MsisdenListResponse msisdenListResponse;
    private TextView emptyItem;
    private String selectedMobileNo;
    private BaseResponseModel baseResponseModel;
    private MsisdenListInput msisdenListInput;
    private TextView keepexistingnumberTxt;
    public static String msisdnNumber = "-1";
    private ImageView ic_arrow;
    public static NewNumberFragment newNumberFragment;
    public static LoadingFragmnet loadingFragmnet;
    private MsisdenListResponse searchNumberResponse;
    public static boolean isTimerActive = false;
    private RelativeLayout rl_keepNumberRoot, rl_searchNumber;
    private EditText et_searchNumber;
    private ImageView iv_clearSearch, iv_search;
    private LinearLayout ll_searchHeader, ll_emptyState;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        if (view == null) {
        view = inflater.inflate(R.layout.fragment_new_number, container, false);
        initUI();
//        } else {
        if (msisdnNumber != null && !msisdnNumber.equalsIgnoreCase("-1")) //
            // in case number is loced so call un lockservice to this number
            onUnLockNumberService();

//        }
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
        regActivity.showHeaderLayout();
        regActivity.setHeaderTitle("");
        regActivity.showBackBtn();
        my_recycler_view = (RecyclerView) view.findViewById(R.id.my_recycler_view);
//        freshNo = (ImageView) view.findViewById(R.id.freshNo);
        emptyItem = (TextView) view.findViewById(R.id.emptyItem);
        keepexistingnumberTxt = (TextView) view.findViewById(R.id.keep_existing_number);
        ic_arrow = (ImageView) view.findViewById(R.id.ic_arrow);
        if (currentLanguage.equalsIgnoreCase("en")) {
            ic_arrow.setBackground(getResources().getDrawable(R.drawable.ic_arrow_right));
        } else {
            ic_arrow.setBackground(getResources().getDrawable(R.drawable.ic_arrow_left));
        }

        rl_searchNumber = (RelativeLayout) view.findViewById(R.id.rl_searchNumber);
        iv_clearSearch = (ImageView) view.findViewById(iv_deleteText);
        iv_search = (ImageView) view.findViewById(R.id.iv_search_number);
        ll_searchHeader = (LinearLayout) view.findViewById(R.id.ll_headerSearch);
        et_searchNumber = (EditText) view.findViewById(R.id.mobileNo);
        loadingFragmnet = new LoadingFragmnet();
        my_recycler_view.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
//        progressID = (ProgressBar) view.findViewById(R.id.progressID);
        my_recycler_view.setLayoutManager(mLayoutManager);
        newNumberFragment = this;
        msisdnLists = new ArrayList<>();
//        progressID.setVisibility(View.VISIBLE);
        ll_searchHeader.setVisibility(View.VISIBLE);
        rl_searchNumber.setVisibility(View.GONE);
        iv_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_searchNumber.setText("");
                rl_searchNumber.setVisibility(View.VISIBLE);
                ll_searchHeader.setVisibility(View.GONE);
            }
        });

        iv_clearSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rl_searchNumber.setVisibility(View.GONE);
                ll_searchHeader.setVisibility(View.VISIBLE);

                et_searchNumber.setText("");
                hideKeyBoard();

                onConsumeService();
            }
        });
        et_searchNumber.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                try {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        hideKeyBoard();

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                onCallSearchNumberService(et_searchNumber.getText().toString());

                            }
                        }, 300);


                        return true;
                    }
                } catch (Exception ex) {

                }
                return false;
            }
        });

        ll_emptyState = (LinearLayout) view.findViewById(R.id.ll_empty_state);
        ll_emptyState.setVisibility(View.GONE);
//        freshNo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), TAGGING_onboarding_refresh_number_list);
//
//                if (msisdnLists.size() >= 5) {
//                    refreshAdapter();
//                }
//            }
//        });
//        keepexistingnumberTxt.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(),TAGGING_migration_selected);
//
//
//                regActivity.addFragmnet(new NumberTransferFragment(), R.id.frameLayout, true);
//
//            }
//        });

        rl_keepNumberRoot = (RelativeLayout) view.findViewById(R.id.rl_keep_number_root);
        rl_keepNumberRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), TAGGING_migration_selected);

//                    logEvent(3);
                    regActivity.addFragmnet(new NumberTransferFragment(), R.id.frameLayout, true);
                } catch (Exception ex) {
                    CommonMethods.logException(ex);
                }
            }
        });
        rl_keepNumberRoot.setVisibility(View.GONE);

        consumeGetRegistrationIDService();


    }

    private void consumeGetRegistrationIDService() {

        regActivity.addFragmnet(loadingFragmnet, R.id.loadingFragmeLayout, true);
        InitializeServiceRequestModel masterInputResponse = new InitializeServiceRequestModel();
        masterInputResponse.setAppVersion(appVersion);
        masterInputResponse.setChannel(channel);
        masterInputResponse.setLang(currentLanguage);
        masterInputResponse.setOsVersion(osVersion);
        masterInputResponse.setDeviceId(deviceId);
        masterInputResponse.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        new NewGetRegistrationIDService(this, masterInputResponse);
    }


    private boolean shouldRefresh = false;

    @Override
    public void onResume() {
        super.onResume();
        regActivity.showBackBtn();
        isTimerActive = false;
        if (shouldRefresh) {
            onConsumeService();
        }


    }


    public void onCallSearchNumberService(String searchString) {
//        logEvent(2);
        regActivity.addFragmnet(loadingFragmnet, R.id.loadingFragmeLayout, true);
        SearchMsisdnInputModel searchMsisdnInputModel = new SearchMsisdnInputModel();
        searchMsisdnInputModel.setAppVersion(appVersion);
        searchMsisdnInputModel.setToken(token);
        searchMsisdnInputModel.setOsVersion(osVersion);
        searchMsisdnInputModel.setChannel(channel);
        searchMsisdnInputModel.setDeviceId(deviceId);
        searchMsisdnInputModel.setReservationId(sharedPrefrencesManger.getRegisterationID());
        searchMsisdnInputModel.setStoreId(storeId);
        searchMsisdnInputModel.setSearchString(searchString);
        searchMsisdnInputModel.setLang(sharedPrefrencesManger.getLanguage());
        new SearchMsisdnService(this, searchMsisdnInputModel);
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        if (sharedPrefrencesManger.getRegisterationID().length() > 0) {
            shouldRefresh = false;

//        progressID.setVisibility(View.VISIBLE);
            msisdenListInput = new MsisdenListInput();
            msisdenListInput.setAppVersion(appVersion);
            msisdenListInput.setToken(token);
            msisdenListInput.setOsVersion(osVersion);
            msisdenListInput.setChannel(channel);
            msisdenListInput.setDeviceId(deviceId);
            msisdenListInput.setReservationId(sharedPrefrencesManger.getRegisterationID());
            msisdenListInput.setStoreId(storeId);
            new MsisdnService(this, msisdenListInput);
        }

        FaqsInputModel faqsInputModel = new FaqsInputModel();
        faqsInputModel.setAppVersion(appVersion);
        faqsInputModel.setDeviceId(deviceId);
        faqsInputModel.setToken(token);
        faqsInputModel.setOsVersion(osVersion);
        faqsInputModel.setChannel(channel);
        faqsInputModel.setLang(currentLanguage);
        //0562237176
//        faqsInputModel.setSignificant("REG_FAQ");
        faqsInputModel.setSignificant(ConstantUtils.is_migration_enabled);
        new FaqsService(this, faqsInputModel);

    }

    private void onUnLockNumberService() {
//        regActivity.addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        lockMobileNumberInput = new LockMobileNumberInput();
        lockMobileNumberInput.setAppVersion(appVersion);
        lockMobileNumberInput.setToken(token);
        lockMobileNumberInput.setOsVersion(osVersion);
        lockMobileNumberInput.setChannel(channel);
        lockMobileNumberInput.setDeviceId(deviceId);
        lockMobileNumberInput.setMsisdn(selectedMobileNo);
        lockMobileNumberInput.setTargetMsisdn(selectedMobileNo);
        lockMobileNumberInput.setReservationId(sharedPrefrencesManger.getRegisterationID());
        lockMobileNumberInput.setStoreId(storeId);
        new UnLockNumberService(this, lockMobileNumberInput);
    }

    private void onLockNumberService() {
        lockMobileNumberInput = new LockMobileNumberInput();
        lockMobileNumberInput.setAppVersion(appVersion);
        lockMobileNumberInput.setToken(token);
        lockMobileNumberInput.setOsVersion(osVersion);
        lockMobileNumberInput.setChannel(channel);
        lockMobileNumberInput.setDeviceId(deviceId);
        lockMobileNumberInput.setMsisdn(selectedMobileNo);
        lockMobileNumberInput.setTargetMsisdn(selectedMobileNo);
        lockMobileNumberInput.setReservationId(sharedPrefrencesManger.getRegisterationID());
        lockMobileNumberInput.setStoreId(storeId);
        lockMobileNumberInput.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        new LockNumberService(this, lockMobileNumberInput);

    }


    public void hideKeyBoard() {
        View view = et_searchNumber;
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
//            if (isVisible()) {
            my_recycler_view.setEnabled(true);
            if (responseModel != null && responseModel.getResultObj() != null) {
                this.baseResponseModel = responseModel;
                if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.new_get_registration_id_service)) {
//                    regActivity.onBackPressed();
                    inilizeRegisteratonOutput = (InilizeRegisteratonOutput) responseModel.getResultObj();
                    if (inilizeRegisteratonOutput.getErrorCode() != null) {
                        regActivity.onBackPressed();
                        onGetInitializeError();
                    } else
                        onGetRegistrationIDSuccess();
                } else if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.lockNumberService)) {// in case lock numbers

                    regActivity.onBackPressed();
                    onLockNumber();
                } else if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.msisdnService)) { // in case get numbers
                    regActivity.onBackPressed();
                    my_recycler_view.setVisibility(View.VISIBLE);
                    int j = 0;
                    for (int i = 0; i < regActivity.getSupportFragmentManager().getFragments().size(); i++) {

                        if (regActivity.getSupportFragmentManager().getFragments().get(i) instanceof LoadingFragmnet) {
                            j++;
                        }

                    }
                    Fragment[] loadingFragments = new Fragment[j];
                    for (int i = 0; i < j; i++) {
                        loadingFragments[i] = new LoadingFragmnet();
                    }
                    regActivity.removeFragment(loadingFragments);
                    msisdenListResponse = (MsisdenListResponse) responseModel.getResultObj();
                    onGetMsisdnNumbers(msisdenListResponse);
                } else if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.unLockNumberService)) { // in case un locknumber
//                onUnLockNumber();
                } else if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.SEARCH_MSISDN)) { // in case un locknumber
//                onUnLockNumber();
                    regActivity.onBackPressed();
                    searchNumberResponse = (MsisdenListResponse) responseModel.getResultObj();

                    if (searchNumberResponse != null && searchNumberResponse.getMsisdnList() != null && searchNumberResponse.getMsisdnList().size() > 0) {
                        onGetMsisdnNumbers(searchNumberResponse);
                    } else {
                        if (isVisible()) {
                            my_recycler_view.setAdapter(null);
                            my_recycler_view.setVisibility(View.GONE);

                            emptyItem.setVisibility(View.VISIBLE);
                            ll_emptyState.setVisibility(View.VISIBLE);
//                                emptyItem.setText(getString(R.string.no_numbers));
                            if (searchNumberResponse != null && searchNumberResponse.getErrorMsgEn() != null) {
                                if (sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)) {
                                    emptyItem.setText(searchNumberResponse.getErrorMsgEn());

                                } else {
                                    emptyItem.setText(searchNumberResponse.getErrorMsgAr());

                                }
                            } else {
                                emptyItem.setText(getString(R.string.no_numbers_error));
                            }
                        }


                    }
                }
            } else if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.faqsService)) { // migration enabled/disabled
                FaqsResponseModel faqsResponseModel = (FaqsResponseModel) responseModel.getResultObj();
                if (faqsResponseModel != null && !faqsResponseModel.isMigrationEnabled()) {
                    rl_keepNumberRoot.setVisibility(View.GONE);
                } else {
                    rl_keepNumberRoot.setVisibility(View.VISIBLE);
                }
            }
//            }
        } catch (Exception ex) {
            CommonMethods.logException(ex);

        }

    }

    InilizeRegisteratonOutput inilizeRegisteratonOutput;

    private void onGetRegistrationIDSuccess() {
        if (inilizeRegisteratonOutput != null && inilizeRegisteratonOutput.getRegistrationId() != null
                && !inilizeRegisteratonOutput.getRegistrationId().isEmpty()) {
            sharedPrefrencesManger.setRegisterationID(inilizeRegisteratonOutput.getRegistrationId());
            onConsumeService();
        }

    }

    private void onGetMsisdnNumbers(MsisdenListResponse msisdenListResponseService) {
//        msisdenListResponse = (MsisdenListResponse) baseResponseModel.getResultObj();
        if (msisdenListResponseService != null) {
            if (msisdenListResponseService.getErrorCode() != null)
                onGetMsisdnListError();
            else {
                msisdnLists = msisdenListResponseService.getMsisdnList();
                onGetMsisdnListSucess(msisdnLists);
            }
        }

    }


    private void onLockNumber() {
        lockMobileNumberOutput = (LockMobileNumberOutput) baseResponseModel.getResultObj();
        if (lockMobileNumberOutput != null)
            if (lockMobileNumberOutput.getErrorCode() != null)
                onLockNumberError();
            else
                onLockNumberSucess();
    }

    private void onUnLockNumber() {
        lockMobileNumberOutput = (LockMobileNumberOutput) baseResponseModel.getResultObj();
        if (lockMobileNumberOutput != null)
            if (lockMobileNumberOutput.getErrorCode() != null)
                onLockNumberError();
//            else {
//                onUmlockNumberSucess();
//            }

    }

    private void onUmlockNumberSucess() {
        if (lockMobileNumberOutput.getStatus() == 3) {// un loced sucessfully
        } else {
            onLockNumberError();

        }
    }

    private void onLockNumberError() {
        Fragment errorFragmnet = new ErrorFragment();
        Bundle bundle = new Bundle();
        if (currentLanguage != null) {
            if (currentLanguage.equalsIgnoreCase("en"))
                bundle.putString(ConstantUtils.errorString, lockMobileNumberOutput.getErrorMsgEn());
            else
                bundle.putString(ConstantUtils.errorString, lockMobileNumberOutput.getErrorMsgAr());
        }
        errorFragmnet.setArguments(bundle);
        regActivity.addFragmnet(errorFragmnet, R.id.frameLayout, true);
//        showErrorDalioge(lockMobileNumberOutput.getErrorMsgEn());
    }


    private void onLockNumberSucess() {
        if (lockMobileNumberOutput.getStatus() == 1) { // number is un locked
            isTimerActive = true;
            sharedPrefrencesManger.setTimerValue(new Date().getTime());
            shouldRefresh = true;
            regActivity.replaceFragmnet(new OrderFragment(), R.id.frameLayout, true);
            logEvent(AdjustEvents.NUMBER_SELECTION_COMPLETE,new Bundle());
        } else {
            try {

                showErrorFragment(regActivity.getString(R.string.generice_error));

            } catch (Exception ex) {
                showErrorFragment("Sorry this number has been reserved, please select another number.");
            }
        }

    }


    private void showErrorFragment(String error) {
        try {
            Bundle bundle = new Bundle();
            bundle.putString(ConstantUtils.errorString, error);
            ErrorFragment errorFragment = new ErrorFragment();
            errorFragment.setArguments(bundle);
            regActivity.addFragmnet(errorFragment, R.id.frameLayout, false);
        } catch (Exception ex) {

        }
    }

    private void onGetMsisdnListSucess(List<MsisdnList> mLists) {
        if (isVisible()) {

            if (mLists.size() > 0) {
                emptyItem.setVisibility(View.GONE);
                msisdenListAdapter = new MsisdenListAdapter(mLists, getContext(), this);
                AlphaAnimatorAdapter animatorAdapter = new AlphaAnimatorAdapter(msisdenListAdapter, my_recycler_view);
                my_recycler_view.setAdapter(animatorAdapter);
                my_recycler_view.setVisibility(View.VISIBLE);
                ll_emptyState.setVisibility(View.GONE);
            } else {
                if (isVisible()) {
                    my_recycler_view.setAdapter(null);
                    my_recycler_view.setVisibility(View.GONE);
                    ll_emptyState.setVisibility(View.VISIBLE);
                    emptyItem.setVisibility(View.VISIBLE);
                    emptyItem.setText(getString(R.string.no_numbers));
                }
            }
        }
    }

    private void onGetInitializeError() {
        Fragment errorFragmnet = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.buttonTitle, getString(R.string.ok));

        if (currentLanguage != null) {
            if (currentLanguage.equalsIgnoreCase("en"))
                bundle.putString(ConstantUtils.errorString, inilizeRegisteratonOutput.getErrorMsgEn());
            else
                bundle.putString(ConstantUtils.errorString, inilizeRegisteratonOutput.getErrorMsgAr());
        }
        errorFragmnet.setArguments(bundle);
        regActivity.onBackPressed();
        regActivity.addFragmnet(errorFragmnet, R.id.frameLayout, true);
    }

    private void onGetMsisdnListError() {
        Fragment errorFragmnet = new ErrorFragment();
        Bundle bundle = new Bundle();
        if (currentLanguage != null) {
            if (currentLanguage.equalsIgnoreCase("en"))
                bundle.putString(ConstantUtils.errorString, msisdenListResponse.getErrorMsgEn());
            else
                bundle.putString(ConstantUtils.errorString, msisdenListResponse.getErrorMsgAr());
        }
        errorFragmnet.setArguments(bundle);
        regActivity.addFragmnet(errorFragmnet, R.id.frameLayout, true);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        try {
            if (responseModel != null && responseModel.getResultObj() != null) {
                my_recycler_view.setEnabled(true);
                if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.new_get_registration_id_service)) {
                    regActivity.onBackPressed();
                    inilizeRegisteratonOutput = (InilizeRegisteratonOutput) responseModel.getResultObj();
                    if (inilizeRegisteratonOutput != null && inilizeRegisteratonOutput.getErrorMsgEn() != null) {
                        if (sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)) {
                            showErrorFragment(inilizeRegisteratonOutput.getErrorMsgEn());
                        } else {
                            showErrorFragment(inilizeRegisteratonOutput.getErrorMsgAr());
                        }
                    } else {
                        showErrorFragment(getString(R.string.error_));
                    }
                }
                if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.unLockNumberService)) { // in case un locknumber
                } else if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.SEARCH_MSISDN)) {
                    regActivity.onBackPressed();
                    searchNumberResponse = (MsisdenListResponse) responseModel.getResultObj();
                    if (searchNumberResponse != null && searchNumberResponse.getErrorMsgEn() != null) {
                        if (sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)) {
                            showErrorFragment(searchNumberResponse.getErrorMsgEn());
                        } else {
                            showErrorFragment(searchNumberResponse.getErrorMsgAr());
                        }
                    } else {
                        showErrorFragment(getString(R.string.error_));
                    }
                } else {
                    super.onErrorListener(responseModel);
//        progressID.setVisibility(View.GONE);
                    if (isVisible()) {
                        if (responseModel != null && responseModel.getServiceType() != null && responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.LOG_USER_EVENT)) {

                        } else {
                            regActivity.onBackPressed();
                        }
                    }
                }
            } else {
                super.onErrorListener(responseModel);
            }
        } catch (Exception ex) {

            if (ex.getMessage().contains("SSL")) {
                showErrorFragment(getString(R.string.error_));
                my_recycler_view.setAdapter(null);
                my_recycler_view.setVisibility(View.GONE);

                ll_emptyState.setVisibility(View.VISIBLE);
            }
            CommonMethods.logException(ex);

            super.onErrorListener(responseModel);
        }

    }

    private List<MsisdnList> getFiveNumbers() {
        List<MsisdnList> subItems = null;
        if (msisdnLists.size() > 5) {
            if (msisdnLists.size() - endIndex >= 5 || endIndex == 5) {
                subItems = msisdnLists.subList(startIndex, endIndex);
                startIndex = endIndex;
                endIndex = startIndex + 5;
            } else {
                if (endIndex > 5) {
                    subItems = msisdnLists.subList(startIndex, msisdnLists.size());
                    startIndex = 0;
                    endIndex = 5;
                } else {
                    subItems = msisdnLists;
                }
            }
        } else {
            subItems = msisdnLists;
        }
        return subItems;
    }


    private void refreshAdapter() {
        msisdenListAdapter.notifyDataSetChanged();
        msisdenListAdapter = new MsisdenListAdapter(getFiveNumbers(), getContext(), this);
        AlphaAnimatorAdapter animatorAdapter = new AlphaAnimatorAdapter(msisdenListAdapter, my_recycler_view);
        my_recycler_view.setAdapter(animatorAdapter);

    }

    @Override
    public void onSelectedNumber(String mobileNo) {
        selectedMobileNo = mobileNo;
        msisdnNumber = selectedMobileNo;
        msisdnNumber = selectedMobileNo;
        my_recycler_view.setEnabled(false);
        try {
            regActivity.addFragmnet(loadingFragmnet, R.id.frameLayout, true);
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }
        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), TAGGING_on_boarding_select_number);
//        logEvent(1);
        onLockNumberService();
    }

    private void logEvent(int index) {

        logEventInputModel.setActionTransactionId(sharedPrefrencesManger.getTempAppId());
        logEventInputModel.setScreenId(ConstantUtils.SCREEN_ID_0006);

        switch (index) {
            case 1:
                logEventInputModel.setSelectedNumber(selectedMobileNo);
                break;
            case 2:
                logEventInputModel.setSearchNumber(et_searchNumber.getText().toString());
                break;
            case 3:
                logEventInputModel.setKeepExistingNumber(((TextView) view.findViewById(R.id.keep_existing_number)).getText().toString());
                break;
        }
        new LogApplicationFlowService(this, logEventInputModel);
    }
}
