package com.indy.views.fragments.gamification.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.BaseServiceManger;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.gamification.models.getimagefilters.GetImageFiltersInput;
import com.indy.views.fragments.gamification.models.getimagefilters.GetImageFiltersOutput;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Tohamy on 10/3/2017.
 */

public class GetImageFiltersService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private GetImageFiltersInput getImageFiltersInput;


    public GetImageFiltersService(BaseInterface baseInterface, GetImageFiltersInput getImageFiltersInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.getImageFiltersInput = getImageFiltersInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<GetImageFiltersOutput> call = apiService.getImageFilters(getImageFiltersInput);
        call.enqueue(new Callback<GetImageFiltersOutput>() {
            @Override
            public void onResponse(Call<GetImageFiltersOutput> call, Response<GetImageFiltersOutput> response) {
                mBaseResponseModel.setServiceType(ServiceUtils.GET_IMAGES_FILTERS);
                mBaseResponseModel.setResultObj(response.body());

                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (response.body().getErrorMsgEn() != null) {
//                        String urlForTag = call.request().url().toString();
//                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
//                        urlForTag = urlForTag.replace("/", "_");
//                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<GetImageFiltersOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.GET_IMAGES_FILTERS);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}

