package com.indy.views.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.indy.R;
import com.indy.controls.ServiceUtils;
import com.indy.dbt.activities.DBTPagerActivity;
import com.indy.models.balance.BalanceInputModel;
import com.indy.models.balance.BalanceResponseModel;
import com.indy.models.benefits.BenefitsOutputModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.models.utils.MasterInputResponse;
import com.indy.services.BenefitsService;
import com.indy.services.GetUserBalanceService;
import com.indy.services.PurchaseNewMembershipService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.RechargeActivity;
import com.indy.views.activites.SwpeMainActivity;
import com.indy.views.fragments.usage.PackagesFragments;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.indy.views.fragments.utils.MasterFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewMembershipBenefitsFragment extends MasterFragment {

    View view;
    TextView bt_cancel;
    Button bt_proceed;
    TextView tv_benefits;
    BenefitsOutputModel benefitsOutputModel;

    TextView tv_header, tv_footer, tv_title1, tv_description1, tv_title2, tv_description2, tv_title3, tv_description3, tv_title4, tv_description4, tv_title5, tv_description5, tv_title7, tv_description7, tv_title6, tv_description6;
    private BalanceResponseModel balanceResponseModel;
    private Double availableBalance = 0.0;
    private Double newMembershipPrice;

    public NewMembershipBenefitsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_benefits, container, false);
        initUI();
        onConsumeService();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();

        bt_proceed = (Button) view.findViewById(R.id.btn_proceed);
        bt_proceed.setEnabled(false);
        bt_cancel = (TextView) view.findViewById(R.id.btn_cancel);
        tv_header = (TextView) view.findViewById(R.id.tv_header);
        tv_footer = (TextView) view.findViewById(R.id.tv_footer);
        tv_footer.setVisibility(View.GONE);
        tv_title1 = (TextView) view.findViewById(R.id.title_1);
        tv_title2 = (TextView) view.findViewById(R.id.title_2);
        tv_title3 = (TextView) view.findViewById(R.id.title_3);
        tv_title4 = (TextView) view.findViewById(R.id.title_4);
        tv_title5 = (TextView) view.findViewById(R.id.title_5);
        tv_title6 = (TextView) view.findViewById(R.id.title_6);
        tv_title7 = (TextView) view.findViewById(R.id.title_7);
        tv_description1 = (TextView) view.findViewById(R.id.description_1);
        tv_description2 = (TextView) view.findViewById(R.id.description_2);
        tv_description3 = (TextView) view.findViewById(R.id.description_3);
        tv_description4 = (TextView) view.findViewById(R.id.description_4);
        tv_description5 = (TextView) view.findViewById(R.id.description_5);
        tv_description6 = (TextView) view.findViewById(R.id.description_6);
        tv_description7 = (TextView) view.findViewById(R.id.description_7);

        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        newMembershipPrice = Double.parseDouble(sharedPrefrencesManger.getNewMembershipPrice());
        if(sharedPrefrencesManger.isVATEnabled()){
            newMembershipPrice = newMembershipPrice + ((newMembershipPrice/100)*5);
        }
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        ((SwpeMainActivity) getActivity()).addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        MasterInputResponse masterInputResponse = new MasterInputResponse();
        masterInputResponse.setAppVersion(appVersion);
        masterInputResponse.setImsi(sharedPrefrencesManger.getMobileNo());
        masterInputResponse.setDeviceId(deviceId);
        masterInputResponse.setToken(token);
        masterInputResponse.setLang(currentLanguage);
        masterInputResponse.setChannel(channel);
        masterInputResponse.setMsisdn(sharedPrefrencesManger.getMobileNo());
        masterInputResponse.setOsVersion(osVersion);
        new BenefitsService(this, masterInputResponse);
    }

    private void getAvailableBalance() {
        BalanceInputModel balanceInputModel = new BalanceInputModel();
        balanceInputModel.setAppVersion(appVersion);
        balanceInputModel.setToken(token);
        balanceInputModel.setOsVersion(osVersion);
        balanceInputModel.setChannel(channel);
        balanceInputModel.setDeviceId(deviceId);
        balanceInputModel.setLang(currentLanguage);
        balanceInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
        balanceInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        balanceInputModel.setAuthToken(authToken);
        balanceInputModel.setBillingPeriod(CommonMethods.getBillingPeriod());
        new GetUserBalanceService(this, balanceInputModel, getActivity());
    }

    private void purchaseNewMembership() {
        MasterInputResponse masterInputResponse = new MasterInputResponse();
        masterInputResponse.setAppVersion(appVersion);
        masterInputResponse.setToken(token);
        masterInputResponse.setOsVersion(osVersion);
        masterInputResponse.setChannel(channel);
        masterInputResponse.setDeviceId(deviceId);
        masterInputResponse.setLang(currentLanguage);
        masterInputResponse.setImsi(sharedPrefrencesManger.getMobileNo());
        masterInputResponse.setMsisdn(sharedPrefrencesManger.getMobileNo());
        masterInputResponse.setAuthToken(authToken);
        new PurchaseNewMembershipService(this, masterInputResponse);
        ((SwpeMainActivity) getActivity()).replaceFragmnet(new LoadingFragmnet(), R.id.contentFrameLayout, true);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);

        if (responseModel.getServiceType() != null && responseModel.getServiceType().equals(ServiceUtils.balanceServiceType)) {
            balanceResponseModel = (BalanceResponseModel) responseModel.getResultObj();
            if (balanceResponseModel != null) {
                availableBalance = balanceResponseModel.getAmount();
                setupEligibility();
            }
        } else if (responseModel.getServiceType() != null && responseModel.getServiceType().equals(ServiceUtils.purchase_new_membership_service)) {
            int j = 0;
            for (int i = 0; i < ((SwpeMainActivity) getActivity()).getSupportFragmentManager().getFragments().size(); i++) {

                if (((SwpeMainActivity) getActivity()).getSupportFragmentManager().getFragments().get(i) instanceof LoadingFragmnet) {
                    j++;
                }

            }
            Fragment[] loadingFragments = new Fragment[j];
            for (int i = 0; i < j; i++) {
                loadingFragments[i] = new LoadingFragmnet();
            }
            ((SwpeMainActivity) getActivity()).removeFragment(loadingFragments);

            MasterErrorResponse masterErrorResponse = (MasterErrorResponse) responseModel.getResultObj();
            if (masterErrorResponse.getErrorCode() == null) {
                if (PackagesFragments.instance != null) {
                    PackagesFragments.instance.getPackagesList();
                    ((SwpeMainActivity) getActivity()).onBackPressed();
                }
            } else {
                if (masterErrorResponse != null && masterErrorResponse.getErrorMsgEn() != null) {
                    if (currentLanguage.equals("en")) {
                        showErrorFragment(masterErrorResponse.getErrorMsgEn());
                    } else {

                        showErrorFragment(masterErrorResponse.getErrorMsgEn());
                    }
                } else {
                    showErrorFragment(getString(R.string.generice_error));
                }
            }
        } else {
            try {
                if (responseModel != null && responseModel.getResultObj() != null) {
                    benefitsOutputModel = (BenefitsOutputModel) responseModel.getResultObj();
                    if (benefitsOutputModel != null && benefitsOutputModel.getBenefitsText() != null
                            && benefitsOutputModel.getBenefitsText().size() > 0) {
                        setupView();
                    } else {
                        failure();
                    }
                }
                getActivity().onBackPressed();
            } catch (Exception ex) {
                if (ex != null) {
                    ex.printStackTrace();
                }
            }
        }
    }

    private void setupEligibility() {

        bt_proceed.setEnabled(true);
        if (availableBalance > newMembershipPrice) {
            bt_proceed.setText(R.string.proceed);
            bt_proceed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    purchaseNewMembership();
                }
            });
        } else {
            bt_proceed.setText(R.string.recharge);
            tv_footer.setVisibility(View.VISIBLE);
            bt_proceed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), RechargeActivity.class);
                    startActivity(intent);
                }
            });
        }
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        try {
            getActivity().onBackPressed();
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    public void setupView() {
        getAvailableBalance();
        tv_header.setText(benefitsOutputModel.getHeader());
        tv_footer.setText(benefitsOutputModel.getFooter());
        for (int i = 0; i < benefitsOutputModel.getBenefitsText().size(); i++) {
            if (i == 0) {

                tv_title1.setText(benefitsOutputModel.getBenefitsText().get(i).getTitle());
                tv_description1.setText(benefitsOutputModel.getBenefitsText().get(i).getDescription());

            } else if (i == 1) {

                tv_title2.setText(benefitsOutputModel.getBenefitsText().get(i).getTitle());
                tv_description2.setText(benefitsOutputModel.getBenefitsText().get(i).getDescription());

            } else if (i == 2) {

                tv_title3.setText(benefitsOutputModel.getBenefitsText().get(i).getTitle());
                tv_description3.setText(benefitsOutputModel.getBenefitsText().get(i).getDescription());
            } else if (i == 3) {

                tv_title4.setText(benefitsOutputModel.getBenefitsText().get(i).getTitle());
                tv_description4.setText(benefitsOutputModel.getBenefitsText().get(i).getDescription());
            } else if (i == 4) {

                tv_title5.setText(benefitsOutputModel.getBenefitsText().get(i).getTitle());
                tv_description5.setText(benefitsOutputModel.getBenefitsText().get(i).getDescription());
            } else if (i == 5) {

                tv_title6.setText(benefitsOutputModel.getBenefitsText().get(i).getTitle());
                tv_description6.setText(benefitsOutputModel.getBenefitsText().get(i).getDescription());
            } else if (i == 6) {

                tv_title7.setText(benefitsOutputModel.getBenefitsText().get(i).getTitle());
                tv_description7.setText(benefitsOutputModel.getBenefitsText().get(i).getDescription());
            }
        }
    }

    private void showErrorFragment(String error) {
        ((SwpeMainActivity) getActivity()).showFrame();
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, error);
        errorFragment.setArguments(bundle);
        ((SwpeMainActivity) getActivity()).replaceFragmnet(errorFragment, R.id.contentFrameLayout, true);
    }

    public void failure() {

    }
}
