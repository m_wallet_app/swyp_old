package com.indy.views.fragments.rewards;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indy.R;
import com.indy.models.utils.BaseResponseModel;
import com.indy.views.activites.RewardsDetailsActivity;
import com.indy.views.fragments.utils.MasterFragment;

/**
 * Created by emad on 9/28/16.
 */

public class UseVoucherFragment extends MasterFragment {
    private View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_use_voucher, container, false);
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.useVoucherID:
                onUseVoucher();
                Log.v("oooooooops", "oooops");
                break;
        }
    }

    private void onUseVoucher() {
        ((RewardsDetailsActivity) getActivity()).replaceFragmnet(new VoucherCodeFragment(), R.id.frameLayout, true);
    }
}
