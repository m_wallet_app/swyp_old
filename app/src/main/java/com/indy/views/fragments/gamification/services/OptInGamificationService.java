package com.indy.views.fragments.gamification.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.BaseServiceManger;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.gamification.models.serviceInputOutput.OptInGamificationInputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.OptInGamificationOutputResponse;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**

 */
public class OptInGamificationService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private OptInGamificationInputModel optInGamificationInputModel;


    public OptInGamificationService(BaseInterface baseInterface, OptInGamificationInputModel optInGamificationInputModel) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.optInGamificationInputModel = optInGamificationInputModel;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<OptInGamificationOutputResponse> call = apiService.optInGamification(optInGamificationInputModel);
        call.enqueue(new Callback<OptInGamificationOutputResponse>() {
            @Override
            public void onResponse(Call<OptInGamificationOutputResponse> call, Response<OptInGamificationOutputResponse> response) {
                mBaseResponseModel.setServiceType(ServiceUtils.optInGamification);
                mBaseResponseModel.setResultObj(response.body());

                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (response.body().getErrorMsgEn() != null) {
//                        String urlForTag = call.request().url().toString();
//                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
//                        urlForTag = urlForTag.replace("/", "_");
//                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<OptInGamificationOutputResponse> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.optInGamification);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
