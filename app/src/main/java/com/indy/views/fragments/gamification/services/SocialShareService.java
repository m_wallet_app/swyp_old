package com.indy.views.fragments.gamification.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.BaseServiceManger;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.gamification.models.socialshare.SocialShareInput;
import com.indy.views.fragments.gamification.models.socialshare.SocialShareOutput;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Tohamy on 10/4/2017.
 */

public class SocialShareService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private SocialShareInput socialShareInput;


    public SocialShareService(BaseInterface baseInterface, SocialShareInput socialShareInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.socialShareInput = socialShareInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<SocialShareOutput> call = apiService.socialShare(socialShareInput);
        call.enqueue(new Callback<SocialShareOutput>() {
            @Override
            public void onResponse(Call<SocialShareOutput> call, Response<SocialShareOutput> response) {
                mBaseResponseModel.setServiceType(ServiceUtils.SOCIAL_SHARE);
                mBaseResponseModel.setResultObj(response.body());

                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (response.body().getErrorMsgEn() != null) {
//                        String urlForTag = call.request().url().toString();
//                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
//                        urlForTag = urlForTag.replace("/", "_");
//                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<SocialShareOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.SOCIAL_SHARE);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
