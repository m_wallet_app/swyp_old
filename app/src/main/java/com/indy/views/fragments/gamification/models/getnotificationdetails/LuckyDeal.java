package com.indy.views.fragments.gamification.models.getnotificationdetails;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mobile on 04/01/2018.
 */

public class LuckyDeal implements Parcelable
{

    @SerializedName("rewardId")
    @Expose
    private String rewardId;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("totalDescription")
    @Expose
    private String totalDescription;
    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("action")
    @Expose
    private Object action;
    @SerializedName("name")
    @Expose
    private Object name;
    @SerializedName("packageCode")
    @Expose
    private String packageCode;
    @SerializedName("packageName")
    @Expose
    private String packageName;
    @SerializedName("packageNameAr")
    @Expose
    private String packageNameAr;
    @SerializedName("packageDescription")
    @Expose
    private String packageDescription;
    @SerializedName("packageDescriptionAr")
    @Expose
    private String packageDescriptionAr;
    @SerializedName("previousPrice")
    @Expose
    private Integer previousPrice;
    @SerializedName("currentPrice")
    @Expose
    private Integer currentPrice;
    public final static Parcelable.Creator<LuckyDeal> CREATOR = new Creator<LuckyDeal>() {


        @SuppressWarnings({
                "unchecked"
        })
        public LuckyDeal createFromParcel(Parcel in) {
            return new LuckyDeal(in);
        }

        public LuckyDeal[] newArray(int size) {
            return (new LuckyDeal[size]);
        }

    }
            ;

    protected LuckyDeal(Parcel in) {
        this.rewardId = ((String) in.readValue((String.class.getClassLoader())));
        this.type = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.totalDescription = ((String) in.readValue((String.class.getClassLoader())));
        this.description = ((Object) in.readValue((Object.class.getClassLoader())));
        this.action = ((Object) in.readValue((Object.class.getClassLoader())));
        this.name = ((Object) in.readValue((Object.class.getClassLoader())));
        this.packageCode = ((String) in.readValue((String.class.getClassLoader())));
        this.packageName = ((String) in.readValue((String.class.getClassLoader())));
        this.packageNameAr = ((String) in.readValue((String.class.getClassLoader())));
        this.packageDescription = ((String) in.readValue((String.class.getClassLoader())));
        this.packageDescriptionAr = ((String) in.readValue((String.class.getClassLoader())));
        this.previousPrice = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.currentPrice = ((Integer) in.readValue((Integer.class.getClassLoader())));
    }

    public LuckyDeal() {
    }

    public String getRewardId() {
        return rewardId;
    }

    public void setRewardId(String rewardId) {
        this.rewardId = rewardId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getTotalDescription() {
        return totalDescription;
    }

    public void setTotalDescription(String totalDescription) {
        this.totalDescription = totalDescription;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public Object getAction() {
        return action;
    }

    public void setAction(Object action) {
        this.action = action;
    }

    public Object getName() {
        return name;
    }

    public void setName(Object name) {
        this.name = name;
    }

    public String getPackageCode() {
        return packageCode;
    }

    public void setPackageCode(String packageCode) {
        this.packageCode = packageCode;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getPackageNameAr() {
        return packageNameAr;
    }

    public void setPackageNameAr(String packageNameAr) {
        this.packageNameAr = packageNameAr;
    }

    public String getPackageDescription() {
        return packageDescription;
    }

    public void setPackageDescription(String packageDescription) {
        this.packageDescription = packageDescription;
    }

    public String getPackageDescriptionAr() {
        return packageDescriptionAr;
    }

    public void setPackageDescriptionAr(String packageDescriptionAr) {
        this.packageDescriptionAr = packageDescriptionAr;
    }

    public Integer getPreviousPrice() {
        return previousPrice;
    }

    public void setPreviousPrice(Integer previousPrice) {
        this.previousPrice = previousPrice;
    }

    public Integer getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(Integer currentPrice) {
        this.currentPrice = currentPrice;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(rewardId);
        dest.writeValue(type);
        dest.writeValue(totalDescription);
        dest.writeValue(description);
        dest.writeValue(action);
        dest.writeValue(name);
        dest.writeValue(packageCode);
        dest.writeValue(packageName);
        dest.writeValue(packageNameAr);
        dest.writeValue(packageDescription);
        dest.writeValue(packageDescriptionAr);
        dest.writeValue(previousPrice);
        dest.writeValue(currentPrice);
    }

    public int describeContents() {
        return 0;
    }

}
