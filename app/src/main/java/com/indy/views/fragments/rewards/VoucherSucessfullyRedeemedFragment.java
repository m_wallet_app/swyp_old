package com.indy.views.fragments.rewards;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.indy.R;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;
import com.indy.utils.MaterialCheckBox;
import com.indy.views.fragments.utils.MasterFragment;

/**
 * Created by emad on 9/28/16.
 */

public class VoucherSucessfullyRedeemedFragment extends MasterFragment {
    private View view;
    TextView voucherID;
    String voucherCode = "";
    Button okBtn;

    SharedPreferences pref;
    SharedPreferences.Editor editor;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_voucher_redeem_sucessfully, container, false);
        if (getArguments() != null)
            voucherCode = getArguments().getString("VOUCHERCODE");
        else
            voucherCode = "";
        initUI();

        //  String strtext = getArguments().getString("edttext");

        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
        voucherID = (TextView) view.findViewById(R.id.voucherID);
        okBtn = (Button) view.findViewById(R.id.okBtn);

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//              RewardsFragment rv = new RewardsFragment();
//                rv.Result(100);
//                Intent i = new Intent();
//                i.putExtra("hello",100);
//                getActivity().setResult(100,i);
                // sharedPrefrencesManger.
                sharedPrefrencesManger.setUsedActivity("1400");
                RewardsFragment.reloadAgain=true;

//                getActivity().finishActivity(100);
                getActivity().finish();
//                if (RewardsFragment.rewardsFragmentInstance != null)
//                    RewardsFragment.rewardsFragmentInstance.onReedemedSucessfully();

            }
        });
        voucherID.setText(voucherCode);
        MaterialCheckBox materialCheckBox = (MaterialCheckBox) view.findViewById(R.id.fl_tick_animation);
        ImageView iv_tick = (ImageView) view.findViewById(R.id.iv_tick);
        CommonMethods.drawCircle(materialCheckBox,iv_tick);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                CommonMethods.createAppRatingDialog(getActivity(),mFirebaseAnalytics);
            }
        },2000);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
    }
}
