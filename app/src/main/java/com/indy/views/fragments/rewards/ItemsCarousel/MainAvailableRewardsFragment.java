package com.indy.views.fragments.rewards.ItemsCarousel;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.indy.R;
import com.indy.adapters.CategoriesListAdapter;
import com.indy.helpers.GoogleMapsHelper;
import com.indy.models.rewards.AvilableRewardsoutput;
import com.indy.models.rewards.RewardsList;
import com.indy.models.rewards.StoreLocation;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterInputResponse;
import com.indy.services.AvilableRewardsService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.utils.GPSTracker;
import com.indy.views.activites.RewardDetailsActivity;
import com.indy.views.fragments.utils.MasterFragment;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.indy.views.fragments.rewards.RewardsFragment.bottomSheet;
import static com.indy.views.fragments.rewards.RewardsFragment.categoriesList;
import static com.indy.views.fragments.rewards.RewardsFragment.categoryIcons;
import static com.indy.views.fragments.rewards.RewardsFragment.resetBtn;
import static com.indy.views.fragments.rewards.RewardsFragment.rv_categories;
import static com.indy.views.fragments.rewards.RewardsFragment.tv_apply;

/**
 * Created by Amir.jehangir on 10/30/2016.
 */
public class MainAvailableRewardsFragment extends MasterFragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
        GoogleMap.OnInfoWindowClickListener, LocationListener {
    List<RewardsList> rewardsList;
    public static boolean shouldRefresh = false;
    View rootView;
    //---------
    LinearLayout ll_dummyContainer;
    TextView bt_retry;
    TextView tv_error;
    LinearLayout ll_noVouchers;
    //-------------

    //--- Map objects

    View view;
    public GoogleMap mMap;
    public MarkerOptions marker;
    public static HashMap<Marker, RewardsList> mMarkerList;
    public static ArrayList<Marker> mMarkersForSettingList;

    LocationManager locationManager;
    GPSTracker gpsTracker;
    private Button locateMe;
    public TextView tv_logoutFromHotSpots;
    public String locationName = null;
    private Double mLatitude, mLongitude;
    Location mlocation;
    LinearLayout ll_progress;
    public static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    public static final int LOCATION_REQUEST = 1340;
    GoogleMapsHelper googleMapsHelper;


    //-------------------
    private ViewPager viewPager;
    private List<CommonFragment> fragments = new ArrayList<>(); // ViewPager
    private LinearLayoutManager mLayoutManager;
    private MasterInputResponse masterInputResponse;
    private ProgressBar progressBar;
    private AvilableRewardsoutput avilableRewardsoutput;
    private AvilableRewardsService avilableRewardsService;
    public static BottomSheetBehavior behavior;
    public LinearLayout ll_filter, ll_mapList, ll_map;

    public static String expiryDateEn = "";
    public static String expiryDateAr = "";
    public static List<String> selectedCategories;
    //bottom sheet filter and map view

    public boolean isMap = false;
    public ImageView iv_mapListIcon;
    TextView tv_filterCount;
    public CategoriesListAdapter categoriesListAdapter;
    public static TextView tv_filter;

    public ImageView iv_filter, iv_locateMe;
    private Bundle getArguments;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_avilable_rewards, container, false);
            shouldRefresh = true;
            initUI();
        }
//         onConsumeService();
        //   positionView = rootView.findViewById(R.id.position_view);
        //  dealStatusBar(); //

        // 2. ImageLoader
//            initImageLoader();
//            callService();

        // 3. ViewPager
        //  fillViewPager();

        return rootView;
    }

    public void callService() {
        if (shouldRefresh && !isMap)
            onConsumeService();
    }

    @Override
    public void initUI() {
        super.initUI();
        try {
            mLayoutManager = new LinearLayoutManager(getActivity());
            progressBar = (ProgressBar) rootView.findViewById(R.id.progressID);
            viewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
            ll_noVouchers = (LinearLayout) rootView.findViewById(R.id.ll_no_available_rewards);
            mMarkerList = new HashMap<Marker, RewardsList>();
            mMarkersForSettingList = new ArrayList<Marker>();
            ll_filter = (LinearLayout) rootView.findViewById(R.id.ll_filter_list);
            ll_mapList = (LinearLayout) rootView.findViewById(R.id.ll_map_list);
            ll_filter.setEnabled(false);
            ll_mapList.setEnabled(false);
            ll_noVouchers.setVisibility(View.GONE);
            iv_filter = (ImageView) rootView.findViewById(R.id.iv_filter);
            selectedCategories = new ArrayList<String>();
            getArguments = getArguments();
            behavior = BottomSheetBehavior.from(bottomSheet);
            behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    // React to state change
                    if (newState == BottomSheetBehavior.STATE_COLLAPSED) {

                    } else if (newState == BottomSheetBehavior.STATE_EXPANDED) {

                    }
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                    // React to dragging events
                }
            });


            iv_locateMe = (ImageView) rootView.findViewById(R.id.iv_locateMe);
            iv_locateMe.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mlocation != null) {
                        LatLng mLatLng = new LatLng(mlocation.getLatitude(), mlocation.getLongitude());
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mLatLng,
                                13));
                    }
                }
            });
            tv_filterCount = ((TextView) rootView.findViewById(R.id.tv_filter_count));
            tv_filterCount.setVisibility(View.GONE);
            Typeface tf = Typeface.createFromAsset(getResources().getAssets(), "fonts/OpenSans-Regular.ttf");
            tv_filterCount.setTypeface(tf);
            SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.map);
            if (mapFragment != null) {
                mapFragment.getMapAsync(this);
            }
            tv_filter = (TextView) rootView.findViewById(R.id.tv_filter);

            tv_filter.setTextColor(getResources().getColor(R.color.grey_shopping_cart));
            iv_filter.setImageResource(R.drawable.filter);
            tv_apply.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    filterRewards();
                    CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_reward_filter_applied);
                    for (int i = 0; i < selectedCategories.size(); i++) {
                        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_reward_filter_applied + "_" + selectedCategories.get(i));

                    }
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
            });


            rv_categories.setLayoutManager(new LinearLayoutManager(getActivity()));
            rv_categories.setHasFixedSize(true);
            if (categoriesList != null) {
                categoriesListAdapter = new CategoriesListAdapter(categoriesList, getActivity());
                rv_categories.setAdapter(categoriesListAdapter);

            }
            ll_filter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (categoriesList != null && categoriesList.size() > 0)
                        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
            });
            resetBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectedCategories.clear();
                    CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_reward_filter_cleared);

//                categoriesListAdapter.clearChecks();
                    if (categoriesList != null) {
                        categoriesListAdapter = new CategoriesListAdapter(categoriesList, getActivity());
                        rv_categories.setAdapter(categoriesListAdapter);
                    }

                }
            });
            resetBtn.setEnabled(false);
            resetBtn.setAlpha(0.3f);
            ll_map = (LinearLayout) rootView.findViewById(R.id.ll_map);
            ll_map.setVisibility(View.GONE);
            iv_mapListIcon = (ImageView) rootView.findViewById(R.id.iv_map_list_button);
            ll_mapList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!isMap) {

                        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_reward_map_opened);

                        checkPermission();
                        iv_mapListIcon.setImageResource(R.drawable.rewards_list_icon);
                        if (ll_noVouchers.getVisibility() != View.VISIBLE) {
                            ll_map.setVisibility(View.VISIBLE);
                            viewPager.setVisibility(View.GONE);
                        }
                        ((TextView) rootView.findViewById(R.id.tv_map_list_label)).setText(getString(R.string.view_list));
                        isMap = true;
                    } else {
                        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_reward_list_opened);
                        iv_mapListIcon.setImageResource(R.drawable.rewards_map_icon);
                        if (ll_noVouchers.getVisibility() != View.VISIBLE) {

                            ll_map.setVisibility(View.GONE);
                            ll_dummyContainer.setVisibility(View.GONE);
                            viewPager.setVisibility(View.VISIBLE);
                        }
                        ((TextView) rootView.findViewById(R.id.tv_map_list_label)).setText(getString(R.string.map_view));

                        isMap = false;
                    }
                }
            });
            initErrorLayout();


            googleMapsHelper = new GoogleMapsHelper(getContext());
//        locateMe = (Button) view.findViewById(R.id.btn_get_directions);
//
//        locateMe.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (mLatitude != null && mLongitude != null)
//                    googleMapsHelper.onGoogleMpasOpen(mLatitude, mLongitude, locationName);
//            }
//        });

//            if(getArguments()!=null && getArguments().containsKey(ConstantUtils.KEY_NOTIFICATION_ITEM_ID)){
//                Intent intent = new Intent(getActivity(),RewardDetailsActivity.class);
////                intent.putExtra()
//            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public void filterRewards() {

        ArrayList<RewardsList> filteredRewards = new ArrayList<RewardsList>();
        if (rewardsList != null) {
            for (int i = 0; i < rewardsList.size(); i++) {
                for (int j = 0; j < selectedCategories.size(); j++) {
                    if (rewardsList.get(i).getCategoryId().equalsIgnoreCase(selectedCategories.get(j))) {
                        filteredRewards.add(rewardsList.get(i));
                    }
                }
            }
        }
        if (filteredRewards != null && filteredRewards.size() > 0) {

            tv_filterCount.setVisibility(View.VISIBLE);
            tv_filterCount.setText(selectedCategories.size() + "");
            ll_noVouchers.setVisibility(View.GONE);
            fillViewPager(filteredRewards);

            drawMapPins(filteredRewards);
            if (isMap) {
                viewPager.setVisibility(View.GONE);
                ll_map.setVisibility(View.VISIBLE);
            } else {
                viewPager.setVisibility(View.VISIBLE);
                ll_map.setVisibility(View.GONE);
            }
            iv_filter.setImageResource(R.drawable.filter_pink);
            tv_filter.setTextColor(getResources().getColor(R.color.pink_apply));
        } else if (selectedCategories != null && selectedCategories.size() > 0) {
            tv_filterCount.setVisibility(View.VISIBLE);
            tv_filterCount.setText(selectedCategories.size() + "");

            iv_filter.setImageResource(R.drawable.filter_pink);
            tv_filter.setTextColor(getResources().getColor(R.color.pink_apply));
            ll_noVouchers.setVisibility(View.VISIBLE);
            viewPager.setVisibility(View.GONE);
            ll_dummyContainer.setVisibility(View.GONE);
            ((TextView) rootView.findViewById(R.id.tv_membership_cycle)).setText(getString(R.string.no_available_rewards_category));
            drawMapPins(filteredRewards);
            ll_map.setVisibility(View.GONE);
        } else {
            ll_noVouchers.setVisibility(View.GONE);
            tv_filter.setTextColor(getResources().getColor(R.color.grey_shopping_cart));
            iv_filter.setImageResource(R.drawable.filter);
            drawMapPins(filteredRewards);
            tv_filterCount.setVisibility(View.GONE);
            if (isMap) {
                viewPager.setVisibility(View.GONE);
                ll_map.setVisibility(View.VISIBLE);
            } else {
                viewPager.setVisibility(View.VISIBLE);
                ll_map.setVisibility(View.GONE);
            }
            fillViewPager(rewardsList);
            drawMapPins(rewardsList);
        }

    }


    @Override
    public void onResume() {
        super.onResume();
        if (shouldRefresh && !isMap) {
            callService();
        }
    }

    private void initErrorLayout() {
        ll_dummyContainer = (LinearLayout) rootView.findViewById(R.id.ll_dummy_error_container);

        ll_dummyContainer.setVisibility(View.GONE);
        bt_retry = (TextView) rootView.findViewById(R.id.retryBtn);
        bt_retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onConsumeService();
                ll_dummyContainer.setVisibility(View.GONE);
            }
        });
        tv_error = (TextView) rootView.findViewById(R.id.tv_error);
    }

    int detailItemIndex = -1;
    String detailItemId = "";

    private void fillViewPager(final List<RewardsList> rewardsListFiltered) {
        // indicatorTv = (TextView) findViewById(R.id.indicator_tv);
        viewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
        fragments.clear();
        // 1. viewPager parallax，PageTransformer
        viewPager.setPageTransformer(false, new CustPagerTransformer(getActivity()));
        if (getArguments != null) {
            detailItemId = getArguments.getString(ConstantUtils.KEY_NOTIFICATION_ITEM_ID);
        }
        // 2. viewPageradapter
        for (int i = 0; i < rewardsListFiltered.size(); i++) {
            // 10个fragment

            RewardsList rewardsListTemp = rewardsListFiltered.get(i);
            if (rewardsListTemp.getOfferID().equalsIgnoreCase(detailItemId)) {
                detailItemIndex = i;
            }
            CommonFragment commonFragment = new CommonFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable(ConstantUtils.item_reward_view_pager, rewardsListTemp);
            bundle.putInt("position", i + 1);
            bundle.putInt("total", rewardsListFiltered.size());
            bundle.putString("icon_url", getCategoryIconUrl(rewardsListFiltered.get(i).getCategoryId()));
            ArrayList<StoreLocation> passing = new ArrayList<StoreLocation>(rewardsListTemp.getStoreLocations());
            bundle.putParcelableArrayList("storeLocation", passing);
            commonFragment.setArguments(bundle);
            fragments.add(commonFragment);
        }

        viewPager.setAdapter(new FragmentStatePagerAdapter(getFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
//                CommonFragment fragment = fragments.get(position % 10);
//                int totalNum = viewPager.getAdapter().getCount();
//                int currentItem = viewPager.getCurrentItem() + 1;
//                Spanned Indicators = Html.fromHtml("<font color='#12edf0'>" + currentItem + "</font>  /  " + totalNum);
//                fragment.bindData(rewardsList, Indicators.toString());
                return fragments.get(position);
            }

            @Override
            public int getCount() {
                return rewardsListFiltered.size();
            }
        });


        // 3. viewPager
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                updateIndicatorTv();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        updateIndicatorTv();

        if (detailItemIndex >= 0) {
            getArguments = null;
            CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_reward_detail_opened);
//            CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_reward_detail_opened+"_"+mRewardsListItem.getCategoryId());
            RewardsList rewardsListTemp = rewardsListFiltered.get(detailItemIndex);
            Intent intent = new Intent(getActivity(), RewardDetailsActivity.class);
            intent.putExtra(ConstantUtils.RewardsDetails, rewardsListTemp);
            intent.putExtra("total", rewardsListFiltered.size());
            intent.putExtra("position", detailItemIndex + 1);
            intent.putExtra("icon_url", getCategoryIconUrl(rewardsListFiltered.get(detailItemIndex).getCategoryId()));
            ArrayList<StoreLocation> passing = new ArrayList<StoreLocation>(rewardsListTemp.getStoreLocations());
            intent.putParcelableArrayListExtra("storeLocation", passing);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    public String getCategoryIconUrl(String category) {
        for (int i = 0; i < categoriesList.size(); i++) {
            if (categoriesList.get(i).getCategoryName().equalsIgnoreCase(category)) {
                return categoriesList.get(i).getCategoryImageUrl();
            }
        }
        return "ht";
    }

    private void updateIndicatorTv() {
        int totalNum = viewPager.getAdapter().getCount();
        int currentItem = viewPager.getCurrentItem() + 1;
        // indicatorTv.setText(Html.fromHtml("<font color='#12edf0'>" + currentItem + "</font>  /  " + totalNum));
    }
//    private void dealStatusBar() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            int statusBarHeight = getStatusBarHeight();
//            ViewGroup.LayoutParams lp = positionView.getLayoutParams();
//            lp.height = statusBarHeight;
//            positionView.setLayoutParams(lp);
//        }
//    }


    @Override
    public void onConsumeService() {
        super.onConsumeService();

//        do{
//            if(!RewardsFragment.lock){
//                RewardsFragment.lock = true;
//            }
//        }while (RewardsFragment.lock);
        progressBar.setVisibility(View.VISIBLE);
        viewPager.setVisibility(View.GONE);
        masterInputResponse = new MasterInputResponse();
        masterInputResponse.setAppVersion(appVersion);
        masterInputResponse.setToken(token);
        masterInputResponse.setOsVersion(osVersion);
        masterInputResponse.setChannel(channel);
        masterInputResponse.setDeviceId(deviceId);
        masterInputResponse.setMsisdn(sharedPrefrencesManger.getMobileNo());
        masterInputResponse.setAuthToken(authToken);
        avilableRewardsService = new AvilableRewardsService(this, masterInputResponse);

    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        // RewardsFragment.lock = false;
        super.onSucessListener(responseModel);
        try {
            if (isVisible()) {

                progressBar.setVisibility(View.GONE);
                viewPager.setVisibility(View.VISIBLE);
                if (responseModel != null && responseModel.getResultObj() != null) {
                    avilableRewardsoutput = (AvilableRewardsoutput) responseModel.getResultObj();
                    if (avilableRewardsoutput != null) {
                        if (avilableRewardsoutput.getErrorCode() != null)
                            onGetRewardsFailure();
                        else
                            onGetRewardsSucess();
                    }
                }
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }


    private void onGetRewardsSucess() {

        if (avilableRewardsoutput.getRewardList() != null && avilableRewardsoutput.getRewardList().size() > 0) {
            shouldRefresh = false;
//            RewardsFragment.setSavedValue(avilableRewardsoutput.getMonthlySavings()+"");

            rewardsList = avilableRewardsoutput.getRewardList();
            expiryDateEn = avilableRewardsoutput.getExpiryDateEn();
            expiryDateAr = avilableRewardsoutput.getExpiryDateAr();

            if (mlocation != null) {

                for (int i = 0; i < rewardsList.size(); i++) {
                    for (int j = 0; j < rewardsList.get(i).getStoreLocations().size(); j++) {
                        rewardsList.get(i).getStoreLocations().get(j).setDistanceFromUser(CommonMethods.calculateDistanceBetween(new LatLng(mlocation.getLatitude(), mlocation.getLongitude()),
                                new LatLng(rewardsList.get(i).getStoreLocations().get(j).getGeoLocation().getLatitude(),
                                        rewardsList.get(i).getStoreLocations().get(j).getGeoLocation().getLongitude())));

                    }
                }

            }

            //  prepareFragmentsList();
            //   initPager();
//                avilableRewardsAdapter = new AvilableRewardsAdapter(rewardsList, getContext());
//                rewardsRecyclerView.setAdapter(avilableRewardsAdapter);
            if (isVisible()) {

                ll_filter.setEnabled(true);
                ll_mapList.setEnabled(true);
                fillViewPager(rewardsList);
                drawMapPins(rewardsList);
            }

        } else {
            ll_noVouchers.setVisibility(View.VISIBLE);
            ll_dummyContainer.setVisibility(View.GONE);
        }
    }

    private void onGetRewardsFailure() {

        if (currentLanguage != null && avilableRewardsoutput != null && avilableRewardsoutput.getErrorMsgEn() != null) {
            if (currentLanguage.equalsIgnoreCase("en"))
                tv_error.setText(avilableRewardsoutput.getErrorMsgEn());
            else
                tv_error.setText(avilableRewardsoutput.getErrorMsgAr());

        } else {
            tv_error.setText(getString(R.string.generice_error));
        }

        ll_dummyContainer.setVisibility(View.GONE);
        bt_retry.setText(getString(R.string.retry));
        bt_retry.setOnClickListener(new View.OnClickListener()

                                    {
                                        @Override
                                        public void onClick(View view) {
                                            onConsumeService();
                                            ll_dummyContainer.setVisibility(View.GONE);
                                        }
                                    }

        );
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
//        super.onErrorListener(responseModel);
        try {
            if (isVisible())
                progressBar.setVisibility(View.GONE);
            onGetRewardsFailure();
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (avilableRewardsService != null)
            avilableRewardsService.onWeServiceCallCanceled();
        Log.v("ooops", "ondestory");
        return;

    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (avilableRewardsService != null)
            avilableRewardsService.onWeServiceCallCanceled();
        Log.v("ooops", "onDetach");
        return;
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        RewardsList rewardItem = mMarkerList.get(marker);
        try {
            if (rewardItem != null) {
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_reward_reward_detail_opened_from_pin);
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_reward_detail_opened + "_" + rewardItem.getCategoryId());

                Intent intent = new Intent(getActivity(), RewardDetailsActivity.class);
                intent.putExtra(ConstantUtils.RewardsDetails, rewardItem);
                intent.putExtra("total", rewardsList.size());
                intent.putExtra("position", rewardItem.getTempPosition());
                ArrayList<StoreLocation> passing = new ArrayList<StoreLocation>(rewardItem.getStoreLocations());
                intent.putExtra("icon_url", getCategoryIconUrl(rewardItem.getCategoryId()));
                intent.putParcelableArrayListExtra("storeLocation", passing);


                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
//
            }
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }
//        GoogleMapsHelper openGoogleMaps = new GoogleMapsHelper(getContext());
//        openGoogleMaps.onGoogleMpasOpen(marker.getPosition().latitude, marker.getPosition().longitude, locationName);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        int index = -1;
        try {
            RewardsList rewardItem = mMarkerList.get(marker);

            if (rewardItem != null) {
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_reward_map_pin_clicked);

                if (currentLanguage.equals("en")) {
                    locationName = rewardItem.getNameEn();
                } else {
                    locationName = rewardItem.getNameAr();
                }
                if (rewardItem.getStoreLocations() != null && rewardItem.getStoreLocations().size() > 0) {

                    mLatitude = rewardItem.getStoreLocations().get(rewardItem.getSelectedMarker()).getGeoLocation().getLatitude();
                    //getNearestLocationList().get(index).getGeoLocation().getLatitude();
                    mLongitude = rewardItem.getStoreLocations().get(rewardItem.getSelectedMarker()).getGeoLocation().getLongitude();
//            locationNameId.setText(locationName);
                    LatLng latLng = new LatLng(mLatitude, mLongitude);
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                }
            }
        } catch (Exception ex) {
            CommonMethods.logException(ex);

        }

        return false;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//        checkPermission();
//        isGPSEnabled();
    }

    private boolean canAccessLocation() {
        if (!hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
            return false;
        } else {
            return true;
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == getActivity().checkSelfPermission(perm));
    }

    private void isGPSEnabled() {
        try {
            LocationManager locationManager = (LocationManager)
                    getActivity().getSystemService(getActivity().LOCATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (canAccessLocation()) {// gps is allowed
//                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && canAccessLocation()) {// gps is enable
                    if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {// gps is disable
                        checkForGps();
                    } else {
                        gpsTracker = new GPSTracker(getActivity());
                        if (gpsTracker.canGetLocation()) {
                            mlocation = gpsTracker.getLocation();
                            if (mlocation != null) {
                                addMarkerAtCurrentPosition(mlocation);
                                populateDistanceInList();

                            } else {
                                Toast.makeText(getActivity(), "Fetching Location, please wait..", Toast.LENGTH_SHORT).show();
                                try {
                                    Thread.sleep(1500);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                mlocation = gpsTracker.getLocation();
                                if (mlocation != null) {
                                    addMarkerAtCurrentPosition(mlocation);
                                    populateDistanceInList();


                                }
                            }
                        }
//                    chec/kLocation();
                    }
//                checkLocation();
                } else {
                    requestPermissions(LOCATION_PERMS, 0);
                }

            } else {
                gpsTracker = new GPSTracker(getActivity());

                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) { // gps is disable
                    checkForGps();
                } else { // gps is enable
                    if (gpsTracker.canGetLocation()) {
                        mlocation = gpsTracker.getLocation();
                        if (mlocation != null) {
                            addMarkerAtCurrentPosition(mlocation);
                            populateDistanceInList();


                        } else {
                            Toast.makeText(getActivity(), "Fetching Location, please wait..", Toast.LENGTH_SHORT).show();
                            try {
                                Thread.sleep(1500);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            mlocation = gpsTracker.getLocation();
                            if (mlocation != null) {
                                addMarkerAtCurrentPosition(mlocation);
                                populateDistanceInList();


                            }

                        }
                    }
                }

            }
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }

    }

    private void populateDistanceInList() {
        try {
            if (rewardsList != null) {
                for (int i = 0; i < rewardsList.size(); i++) {

                    for (int j = 0; j < rewardsList.get(i).getStoreLocations().size(); j++) {
                        rewardsList.get(i).getStoreLocations().get(j).setDistanceFromUser(CommonMethods.calculateDistanceBetween(new LatLng(mlocation.getLatitude(), mlocation.getLongitude()),
                                new LatLng(rewardsList.get(i).getStoreLocations().get(j).getGeoLocation().getLatitude(),
                                        rewardsList.get(i).getStoreLocations().get(j).getGeoLocation().getLongitude())));

                    }
                }
            }

            for (int i = 0; i < mMarkersForSettingList.size(); i++) {
                mMarkersForSettingList.get(i).setSnippet(CommonMethods.getTwoDecimalPointsValueFromDouble(CommonMethods.calculateDistanceBetween(new LatLng(mlocation.getLatitude(), mlocation.getLongitude()),
                        mMarkersForSettingList.get(i).getPosition())) + " " + getString(R.string.km));
            }
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }
    }


    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            } else {
                isGPSEnabled();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 0:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    isGPSEnabled();
                    // permission was granted, yay! Do the

                } else {
                    checkForGps();
//                    Toast.makeText(this, "To use this feature, kindly allow Ding access to your location.", Toast.LENGTH_LONG).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        ll_dummyContainer.setVisibility(View.GONE);
        switch (requestCode) {
            case 100:
                LocationManager locationManager = (LocationManager)
                        getActivity().getSystemService(getActivity().LOCATION_SERVICE);
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    checkForGps();
                } else {
                    isGPSEnabled();
                }
                break;
            default:
                break;
        }

    }

    public String getCategoryURL(String cataegoryId) {
        try {
            for (int kx = 0; kx < categoriesList.size(); kx++) {
                if (categoriesList.get(kx).getCategoryName().equalsIgnoreCase(cataegoryId)) {
                    return categoriesList.get(kx).getCategoryImageUrl();
                }
            }
        } catch (Exception ex) {
            CommonMethods.logException(ex);

        }
        return "https://www.aau.org/wp-content/uploads/sites/9/2017/02/placeholder.jpg";
    }

    public Marker temp;

    public void drawMapPins(List<RewardsList> rewardsListForDrawMaps) {
        try {
            mMap.clear();
            LatLng fLatLng = null;
            if (mlocation != null) {
                addMarkerAtCurrentPosition(mlocation);
            }
            temp = null;
            ll_dummyContainer.setVisibility(View.GONE);
            if (rewardsListForDrawMaps != null) {
                for (int i = 0; i < rewardsListForDrawMaps.size(); i++) {
                    for (int j = 0; j < rewardsListForDrawMaps.get(i).getStoreLocations().size(); j++) {
                        StoreLocation tempStoreLocation = rewardsListForDrawMaps.get(i).getStoreLocations().get(j);
                        fLatLng = new LatLng(tempStoreLocation.getGeoLocation().getLatitude(),
                                tempStoreLocation.getGeoLocation().getLongitude());
                        final LatLng mLatLng = new LatLng(tempStoreLocation.getGeoLocation().getLatitude(),
                                tempStoreLocation.getGeoLocation().getLongitude());
                        marker = new MarkerOptions().position(mLatLng);


//             marker.icon(BitmapDescriptorFactory.fromBitmap(etisaltLogo));
                        String title = "";
                        if (currentLanguage.equals("en")) {
                            title = rewardsListForDrawMaps.get(i).getNameEn();
                        } else {
                            title = rewardsListForDrawMaps.get(i).getNameAr();
                        }
                        if (mlocation != null) {
                            try {
                                temp = mMap.addMarker(
                                        marker.position(mLatLng)
                                                .title(title)
                                                .icon(BitmapDescriptorFactory.fromBitmap(categoryIcons.get(rewardsListForDrawMaps.get(i).getCategoryId())))
                                                .snippet(CommonMethods.getTwoDecimalPointsValueFromDouble(tempStoreLocation.getDistanceFromUser()) + getString(R.string.km)));
                            } catch (Exception ex) {

                            }
                        } else {
                            try {
                                temp = mMap.addMarker(
                                        marker.position(mLatLng)
                                                .icon(BitmapDescriptorFactory.fromBitmap(categoryIcons.get(rewardsListForDrawMaps.get(i).getCategoryId())))
                                                .title(title));
                            } catch (Exception ex) {

                            }
                        }


//                        PicassoMarker markerTarget = new PicassoMarker(temp);
//                        Picasso.with(getActivity())
//                                .load(getCategoryURL(rewardsListForDrawMaps.get(i).getCategoryId()))
//                                .error(R.drawable.marker_dummy)
//
//                                .resize(CommonMethods.dip2px(getActivity(), 20), CommonMethods.dip2px(getActivity(), 30))
//
//                                .into(markerTarget);
////                                    .snippet(rewardsListForDrawMaps.get(i).getOfferTnCEn())
//                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.location_icon_fill)));
                        rewardsListForDrawMaps.get(i).setSelectedMarker(j);
                        rewardsListForDrawMaps.get(i).setTempPosition(i + 1);
                        mMarkerList.put(temp, rewardsListForDrawMaps.get(i));
                        mMarkersForSettingList.add(temp);

//            if(i==0){
//                tempMarker=temp;
//            }
//        distance = mLocationModelResponse.getNearestLocationList().get(0).getDistance().toString();
                        if (currentLanguage.equals("en")) {
                            locationName = avilableRewardsoutput.getRewardList().get(i).getNameEn();
                        } else {
                            locationName = avilableRewardsoutput.getRewardList().get(i).getNameAr();
                        }
//         distanceId.setText( getString(R.string.nearest_shop) + " " + distance + "  " +  getString(R.string.away));
                        if (rewardsListForDrawMaps.get(0).getStoreLocations() != null && rewardsListForDrawMaps.get(0).getStoreLocations().size() > 0) {
                            mLatitude = rewardsListForDrawMaps.get(0).getStoreLocations().get(0).getGeoLocation().getLatitude();
                            mLongitude = rewardsListForDrawMaps.get(0).getStoreLocations().get(0).getGeoLocation().getLongitude();
                        }
//         locationNameId.setText( locationName);
                        mMap.setOnMarkerClickListener(this);
                        mMap.setOnInfoWindowClickListener(this);
                        mMap.setInfoWindowAdapter(new MyInfoWindowAdapter());
//        if(tempMarker!=null){
//            tempMarker.showInfoWindow();
//        }
                    }
                }
            }
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }
    }


    class MyInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private final View myContentsView;

        MyInfoWindowAdapter() {
            myContentsView = getActivity().getLayoutInflater().inflate(R.layout.my_rewards_info_window, null);
        }

        @Override
        public View getInfoContents(Marker marker) {

            try {
                TextView tvTitle = ((TextView) myContentsView.findViewById(R.id.tv_name));
                tvTitle.setText(marker.getTitle());
                TextView tvSnippet = ((TextView) myContentsView.findViewById(R.id.tv_distance));
                tvSnippet.setText(marker.getSnippet());
                ImageView iv_icon = (ImageView) myContentsView.findViewById(R.id.iv_content_info);
                iv_icon.setVisibility(View.VISIBLE);
                RewardsList rewardsListTemp = mMarkerList.get(marker);
                if (rewardsListTemp != null) {
                    Picasso.with(getActivity())
                            .load(rewardsListTemp.getImageUrl())
                            .into(iv_icon);
                }
            } catch (Exception ex) {
                CommonMethods.logException(ex);

            }
            return myContentsView;
        }

        @Override
        public View getInfoWindow(Marker marker) {
            // TODO Auto-generated method stub
            return null;
        }

    }

    public void checkForGps() {
        showErrorFragment();
//        showErrorFragment();AlertDailogueHelper.showDaiogue(this, NearestStoreActivity.this, "Alert",
//
    }

    private void showErrorFragment() {
//        frameLayout.setVisibility(View.VISIBLE);
//        ErrorFragment errorFragment = new ErrorFragment();
//        Bundle bundle = new Bundle();
//        bundle.putString(ConstantUtils.errorString, "Cannot get Location please enable GPS.");
//        bundle.putString(ConstantUtils.settingsButtonEnabled, "true");
//        bundle.putString(ConstantUtils.buttonTitle, "Settings");
//        errorFragment.setArguments(bundle);
//        ((SwpeMainActivity) getActivity()).replaceFragmnet(errorFragment, R.id.contentFrameLayout, true);
        try {
            tv_error.setText(getString(R.string.enable_gps));
            ll_dummyContainer.setVisibility(View.VISIBLE);
            bt_retry.setText(getString(R.string.settings));
            bt_retry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivityForResult(intent, 100);
                }
            });
        } catch (Exception ex) {
            CommonMethods.logException(ex);

        }
    }

    public void addMarkerAtCurrentPosition(Location mLocation) {

        try {
            LatLng mLatLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
            MarkerOptions tempMarker = new MarkerOptions().position(mLatLng);
            mMap.addMarker(
                    tempMarker.position(mLatLng)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.locate_me_marker)));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mLatLng,
                    13));
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }

    }
}

