package com.indy.views.fragments.gamification.events;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.indy.R;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.gamification.models.EventsList;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetEventsListInputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetEventsListOutputModel;
import com.indy.views.fragments.gamification.services.GetEventsListService;
import com.indy.views.fragments.rewards.ItemsCarousel.CustPagerTransformer;
import com.indy.views.fragments.utils.MasterFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventsFragment extends MasterFragment {
    private static final String ACTIVE_EVENTS = "ACTIVE_EVENTS";
    private static final int PAGE_SIZE = 9;
    private List<EventsList> eventsList = new ArrayList<EventsList>();
    private View rootView;
    //---------
    private LinearLayout ll_dummyContainer;
    private TextView bt_retry;
    private TextView tv_error;
    private LinearLayout ll_noVouchers;
    //-------------

    //-------------------
    private ViewPager viewPager;
    private List<EventItemFragment> fragments = new ArrayList<>(); // ViewPager
    private ProgressBar progressBar;
    private boolean shouldLoadMore = false;
    private int pageNo = 1;
    private GetEventsListOutputModel getEventsListOutputModel;
    private boolean activeEvents;
    private int position;


    public static EventsFragment newInstance(boolean activeEvents) {
        EventsFragment fragment = new EventsFragment();
        Bundle args = new Bundle();
        args.putBoolean(ACTIVE_EVENTS, activeEvents);
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_current_events, container, false);
        activeEvents = getArguments().getBoolean(ACTIVE_EVENTS, false);
        initUI();
        return rootView;
    }

    @Override
    public void initUI() {
        super.initUI();
        pageNo = 1;
        shouldLoadMore = false;
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressID);
        try {
            viewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
//            ll_noVouchers = (LinearLayout) rootView.findViewById(R.id.ll_no_available_rewards);
//
//            ll_noVouchers.setVisibility(View.GONE);
//            createDummyEventsList();
//            fillViewPager(eventsList);

            initErrorLayout();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
//        callService();

//        createDummyEventsList();
        progressBar.setVisibility(View.VISIBLE);
        onConsumeService();
    }

    private void initErrorLayout() {
        ll_dummyContainer = (LinearLayout) rootView.findViewById(R.id.ll_dummy_error_container);

        ll_dummyContainer.setVisibility(View.GONE);
        bt_retry = (TextView) rootView.findViewById(R.id.retryBtn);
        bt_retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onConsumeService();
                ll_dummyContainer.setVisibility(View.GONE);
            }
        });
        tv_error = (TextView) rootView.findViewById(R.id.tv_error);
    }

    private void fillViewPager() {
        // indicatorTv = (TextView) findViewById(R.id.indicator_tv);
        viewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
        // 1. viewPager parallax，PageTransformer
        viewPager.setPageTransformer(false, new CustPagerTransformer(getActivity()));

        viewPager.setAdapter(new FragmentPagerAdapter(getChildFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
//                CommonFragment fragment = fragments.get(position % 10);
//                int totalNum = viewPager.getAdapter().getCount();
//                int currentItem = viewPager.getCurrentItem() + 1;
//                Spanned Indicators = Html.fromHtml("<font color='#12edf0'>" + currentItem + "</font>  /  " + totalNum);
//                fragment.bindData(eventsList, Indicators.toString());
                return fragments.get(position);
            }

            @Override
            public int getCount() {
                return fragments.size();
            }
        });

        // 3. viewPager
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    @Override
    public void onConsumeService() {
        super.onConsumeService();

        GetEventsListInputModel getEventsListInputModel = new GetEventsListInputModel();
        getEventsListInputModel.setChannel(channel);
        getEventsListInputModel.setLang(currentLanguage);
        getEventsListInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        getEventsListInputModel.setAppVersion(appVersion);
        getEventsListInputModel.setAuthToken(authToken);
        getEventsListInputModel.setDeviceId(deviceId);
        getEventsListInputModel.setToken(token);
        getEventsListInputModel.setUserSessionId(sharedPrefrencesManger.getUserSessionId());
        getEventsListInputModel.setUserId(sharedPrefrencesManger.getUserId());
        getEventsListInputModel.setApplicationId(ConstantUtils.INDY_TALOS_ID);
        getEventsListInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
        getEventsListInputModel.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        getEventsListInputModel.setOsVersion(osVersion);
        getEventsListInputModel.setPageFrom(pageNo);
        getEventsListInputModel.setPageTo(pageNo + PAGE_SIZE);
        new GetEventsListService(this, getEventsListInputModel, activeEvents);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            if (isVisible()) {
                progressBar.setVisibility(View.GONE);
                viewPager.setVisibility(View.VISIBLE);
                if (responseModel != null && responseModel.getResultObj() != null) {
                    getEventsListOutputModel = (GetEventsListOutputModel) responseModel.getResultObj();
                    if (getEventsListOutputModel != null) {
                        if (getEventsListOutputModel.getIndyEvents() != null) {
                            if (getEventsListOutputModel.getIndyEvents().size() > 0) {
                                getEventsSuccess();
                            } else {
                                getEventsError(getString(R.string.oops_you_dont_have_any_current_events_yet));
                            }
                        } else {
                            getEventsError(sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)
                                    ? getEventsListOutputModel.getErrorMsgEn() : getEventsListOutputModel.getErrorMsgAr());
                        }
                    }
                }
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    private void getEventsSuccess() {
        if (isVisible()) {
            if (getEventsListOutputModel.getIndyEvents() != null) {
                if (!getEventsListOutputModel.getIndyEvents().isEmpty()) {
                    shouldLoadMore = true;
                    for (int i = 0; i < getEventsListOutputModel.getIndyEvents().size(); i++) {

                        EventsList events = getEventsListOutputModel.getIndyEvents().get(i);
                        if (activeEvents) {
                            events.setActive(true);
                        } else {
                            events.setActive(false);
                        }
                        eventsList.add(events);
                        position = eventsList.size();
                        EventsList eventsListTemp = eventsList.get(i);
                        EventItemFragment commonFragment = new EventItemFragment();
                        Bundle bundle = new Bundle();
                        bundle.putParcelable(ConstantUtils.item_events_pager, eventsListTemp);
                        bundle.putInt("position", position);
                        bundle.putInt("total", getEventsListOutputModel.getTotalEventsCount());
                        if (events.getImageUrl() != null && !events.getImageUrl().isEmpty())
                            bundle.putString("icon_url", events.getImageUrl());
//            ArrayList<StoreLocation> passing = new ArrayList<StoreLocation>(rewardsListTemp.getStoreLocations());
//            bundle.putParcelableArrayList("storeLocation", passing);
                        commonFragment.setArguments(bundle);
                        fragments.add(commonFragment);
                    }
                    if (pageNo == 1) {
                        fillViewPager();
                    }
                    if (viewPager != null)
                        viewPager.getAdapter().notifyDataSetChanged();
                    if (getEventsListOutputModel.getTotalEventsCount() > position) {
                        pageNo += PAGE_SIZE + 1;
                        onConsumeService();
                    }
                } else {
                    if (pageNo == 1) {
                        ll_dummyContainer.setVisibility(View.GONE);
                    } else {
                        shouldLoadMore = false;
                    }
                }
            } else {
//            ll_noVouchers.setVisibility(View.VISIBLE);
                ll_dummyContainer.setVisibility(View.GONE);
            }
        }
    }

    private void getEventsError(String error) {
        if (error != null && !error.isEmpty()) {
            tv_error.setText(error);
        } else {
            tv_error.setText(getString(R.string.generice_error));
        }
        ll_dummyContainer.setVisibility(View.VISIBLE);
        bt_retry.setText(getString(R.string.retry));
        bt_retry.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            onConsumeService();
                                            ll_dummyContainer.setVisibility(View.GONE);
                                        }
                                    }
        );
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        try {
            progressBar.setVisibility(View.GONE);
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }


    private void showErrorFragment() {
//        frameLayout.setVisibility(View.VISIBLE);
//        ErrorFragment errorFragment = new ErrorFragment();
//        Bundle bundle = new Bundle();
//        bundle.putString(ConstantUtils.errorString, "Cannot get Location please enable GPS.");
//        bundle.putString(ConstantUtils.settingsButtonEnabled, "true");
//        bundle.putString(ConstantUtils.buttonTitle, "Settings");
//        errorFragment.setArguments(bundle);
//        ((SwpeMainActivity) getActivity()).replaceFragmnet(errorFragment, R.id.contentFrameLayout, true);

        tv_error.setText(getString(R.string.enable_gps));
        ll_dummyContainer.setVisibility(View.VISIBLE);
        bt_retry.setText(getString(R.string.settings));
        bt_retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, 100);
            }
        });
    }

}
