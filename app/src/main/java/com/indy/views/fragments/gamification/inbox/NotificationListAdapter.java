package com.indy.views.fragments.gamification.inbox;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.gamification.models.getnotificationslist.NotificationModel;

import java.util.List;

public class NotificationListAdapter extends RecyclerView.Adapter<NotificationListAdapter.ViewHolder> {
    private List<NotificationModel> rewardsLists;
    private Context mContext;
    private OnNotificationClickInterface onNotificationClickInterface;

    public NotificationListAdapter(List<NotificationModel> mRewardsList, Context context, OnNotificationClickInterface onNotificationClickInterface) {
        rewardsLists = mRewardsList;
        this.mContext = context;
        this.onNotificationClickInterface = onNotificationClickInterface;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.item_notification_list, viewGroup, false);
        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final NotificationModel rewardsListItem = rewardsLists.get(position);
        if (rewardsListItem.getNotificationTitle() != null && !rewardsListItem.getNotificationTitle().isEmpty())
            holder.tv_title.setText(rewardsListItem.getNotificationTitle());
        if (rewardsListItem.getNotificationDate() != null && !rewardsListItem.getNotificationDate().isEmpty())
            holder.tv_date.setText(CommonMethods.parseDate(rewardsListItem.getNotificationDate(), ConstantUtils.lang_english));
        holder.ll_cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onNotificationClickInterface.onNotificationClick(rewardsListItem, position);
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return rewardsLists.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_title, tv_date;
        LinearLayout ll_cardView;

        public ViewHolder(View v) {
            super(v);
            tv_title = (TextView) v.findViewById(R.id.tv_title_notification);
            tv_date = (TextView) v.findViewById(R.id.tv_date_notification);
            ll_cardView = (LinearLayout) v.findViewById(R.id.card_view);
        }
    }


}
