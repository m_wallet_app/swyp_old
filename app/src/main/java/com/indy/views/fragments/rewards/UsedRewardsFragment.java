package com.indy.views.fragments.rewards;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.indy.R;
import com.indy.adapters.AvilableRewardsAdapter;
import com.indy.models.rewards.UsedRewardsList;
import com.indy.models.rewards.UsedRewardsoutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.models.utils.MasterInputResponse;
import com.indy.services.UsedRewardsService;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.rewards.ItemsCarousel.CommonFragment_Used;
import com.indy.views.fragments.rewards.ItemsCarousel.CustPagerTransformer;
import com.indy.views.fragments.utils.MasterFragment;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by emad on 9/27/16.
 */

public class UsedRewardsFragment extends MasterFragment {

    private TextView indicatorTv;
    private View positionView;
    private final String[] imageArray = {"assets://burgerking.png", "assets://burgerking.png", "assets://burgerking.png", "assets://burgerking.png", "assets://burgerking.png"};
    private RecyclerView rewardsRecyclerView;
    private LinearLayoutManager mLayoutManager;

    private ViewPager viewPager;
    private List<CommonFragment_Used> fragments = new ArrayList<>(); // ViewPager
    private LinearLayout ll_error;
    private MasterInputResponse masterInputResponse;
    private ProgressBar progressBar;
    private UsedRewardsoutput usedRewardsoutput;
    public static boolean shouldRefresh = true;

    private AvilableRewardsAdapter avilableRewardsAdapter;
    ViewPager mViewPager;
    List<AvailableRewardsItemFragment> availableRewardsItemFragmentList;

    List<UsedRewardsList> rewardsList;

    //---------
    LinearLayout ll_dummyContainer;
    TextView bt_retry;
    TextView tv_error;

    public static String expiryDateEn = "";
    public static String expiryDateAr = "";
    //-------------
    View rootView;
    public static UsedRewardsFragment usedRewardsFragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

//        if (rootView == null) {
        rootView = inflater.inflate(R.layout.fragment_used_rewards, container, false);
        initUI();
        ll_error.setVisibility(View.GONE);

//        if(sharedPrefrencesManger.getUsedActivity().equals("1400")){
//            sharedPrefrencesManger.getUsedActivity().equals("");
//        }else {
//            onConsumeService();
//        }

        //   positionView = rootView.findViewById(R.id.position_view);
        //  dealStatusBar(); //

        // 2. ImageLoader
        initImageLoader();
        // onConsumeService();

        // 3. ViewPager
        //  fillViewPager();
//        }
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (shouldRefresh) {
            callService();
        }
    }

    public void callService() {
        if (shouldRefresh)
            onConsumeService();
    }

    @Override
    public void initUI() {
        super.initUI();
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressID);
        viewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
        ll_error = (LinearLayout) rootView.findViewById(R.id.ll_no_available_rewards);
        usedRewardsFragment = this;
        initErrorLayout();
    }


    private void initErrorLayout() {
        ll_dummyContainer = (LinearLayout) rootView.findViewById(R.id.ll_dummy_error_container);

        ll_dummyContainer.setVisibility(View.GONE);
        bt_retry = (TextView) rootView.findViewById(R.id.retryBtn);
        bt_retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onConsumeService();
                ll_dummyContainer.setVisibility(View.GONE);
            }
        });
        tv_error = (TextView) rootView.findViewById(R.id.tv_error);
    }

    private void fillViewPager() {
        // indicatorTv = (TextView) findViewById(R.id.indicator_tv);
        viewPager = (ViewPager) rootView.findViewById(R.id.viewpager);

        // 1. viewPager parallax，PageTransformer
        viewPager.setPageTransformer(false, new CustPagerTransformer(getActivity()));

        // 2. viewPageradapter
        for (int i = 0; i < rewardsList.size(); i++) {

            UsedRewardsList rewardsListTemp = rewardsList.get(i);
            CommonFragment_Used commonFragment_used = new CommonFragment_Used();
            Bundle bundle = new Bundle();
            bundle.putParcelable(ConstantUtils.item_USED_reward_view_pager, rewardsListTemp);
            bundle.putInt("position", i + 1);
            bundle.putInt("total", rewardsList.size());
            commonFragment_used.setArguments(bundle);
            fragments.add(commonFragment_used);
        }

        viewPager.setAdapter(new FragmentStatePagerAdapter(getFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                CommonFragment_Used fragment = fragments.get(position % 10);
//                int totalNum = viewPager.getAdapter().getCount();
//                int currentItem = viewPager.getCurrentItem() + 1;
//                Spanned Indicators = Html.fromHtml("<font color='#12edf0'>" + currentItem + "</font>  /  " + totalNum);
//             //   fragment.bindData(rewardsList, Indicators.toString());
                return fragments.get(position);
            }

            @Override
            public int getCount() {
                return rewardsList.size();
            }
        });


        // 3. viewPager
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                updateIndicatorTv();
                viewPager.setCurrentItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        updateIndicatorTv();
    }

    private void updateIndicatorTv() {
        int totalNum = viewPager.getAdapter().getCount();
        int currentItem = viewPager.getCurrentItem() + 1;
        // indicatorTv.setText(Html.fromHtml("<font color='#12edf0'>" + currentItem + "</font>  /  " + totalNum));
    }
//    private void dealStatusBar() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            int statusBarHeight = getStatusBarHeight();
//            ViewGroup.LayoutParams lp = positionView.getLayoutParams();
//            lp.height = statusBarHeight;
//            positionView.setLayoutParams(lp);
//        }
//    }

    private int getStatusBarHeight() {
        Class<?> c = null;
        Object obj = null;
        Field field = null;
        int x = 0, statusBarHeight = 0;
        try {
            c = Class.forName("com.android.internal.R$dimen");
            obj = c.newInstance();
            field = c.getField("status_bar_height");
            x = Integer.parseInt(field.get(obj).toString());
            statusBarHeight = getResources().getDimensionPixelSize(x);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return statusBarHeight;
    }

    @SuppressWarnings("deprecation")
    private void initImageLoader() {
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                getActivity())
                .memoryCacheExtraOptions(480, 800)
                // default = device screen dimensions
                .threadPoolSize(3)
                // default
                .threadPriority(Thread.NORM_PRIORITY - 1)
                // default
                .tasksProcessingOrder(QueueProcessingType.FIFO)
                // default
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .memoryCacheSize(2 * 1024 * 1024).memoryCacheSizePercentage(13) // default
                .discCacheSize(50 * 1024 * 1024) //
                .discCacheFileCount(100) //
                .discCacheFileNameGenerator(new HashCodeFileNameGenerator()) // default
                .imageDownloader(new BaseImageDownloader(getActivity())) // default
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple()) // default
                .writeDebugLogs().build();

        // 2.ImageLoader
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);
    }


    @Override
    public void onConsumeService() {
        super.onConsumeService();
        progressBar.setVisibility(View.VISIBLE);
        viewPager.setVisibility(View.GONE);
        masterInputResponse = new MasterInputResponse();
        masterInputResponse.setAppVersion(appVersion);
        masterInputResponse.setToken(token);
        masterInputResponse.setOsVersion(osVersion);
        masterInputResponse.setChannel(channel);
        masterInputResponse.setLang(currentLanguage);
        masterInputResponse.setImsi(sharedPrefrencesManger.getMobileNo());
        masterInputResponse.setDeviceId(deviceId);
        masterInputResponse.setMsisdn(sharedPrefrencesManger.getMobileNo());
        masterInputResponse.setAuthToken(authToken);
        new UsedRewardsService(this, masterInputResponse);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            if (isVisible())
                progressBar.setVisibility(View.GONE);

            viewPager.setVisibility(View.VISIBLE);
            if (responseModel != null && responseModel.getResultObj() != null) {
                usedRewardsoutput = (UsedRewardsoutput) responseModel.getResultObj();
                if (usedRewardsoutput != null && usedRewardsoutput.getRewardList() != null) {
                    if (usedRewardsoutput.getMonthlySavings() != null) {
                        if (RewardsFragment.rewardsFragmentInstance != null)
                            RewardsFragment.rewardsFragmentInstance.setSavedValue(usedRewardsoutput.getMonthlySavings() + "");
                    } else {
                        if (RewardsFragment.rewardsFragmentInstance != null)
                            RewardsFragment.rewardsFragmentInstance.setSavedValue("0");
                    }
                    if (usedRewardsoutput.getErrorCode() != null) {
                        onGetRewardsFailure();
                    } else {
                        onGetRewardsSucess();
                    }
                }
            } else {
                ll_error.setVisibility(View.VISIBLE);
                RewardsFragment.rewardsFragmentInstance.setSavedValue("0");
//            viewPager.setVisibility(View.GONE);
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        progressBar.setVisibility(View.GONE);
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    private void onGetRewardsSucess() {
        if (isVisible())
            ll_error.setVisibility(View.GONE);
        viewPager.setVisibility(View.VISIBLE);

        if (usedRewardsoutput.getRewardList() != null) {
            try {
//                ((TextView) getParentFragment().getView().findViewById(R.id.tv_saved_rewards)).setText("))))");
                if (usedRewardsoutput.getMonthlySavings() != null) {
                    RewardsFragment.rewardsFragmentInstance.setSavedValue(usedRewardsoutput.getMonthlySavings() + "");
                } else {
                    RewardsFragment.rewardsFragmentInstance.setSavedValue("0");
                }
            } catch (Exception ex) {

            }
            if (usedRewardsoutput.getRewardList().size() > 0) {
                rewardsList = usedRewardsoutput.getRewardList();
                expiryDateEn = usedRewardsoutput.getExpiryDateEn();
                expiryDateAr = usedRewardsoutput.getExpiryDateAr();
                //  prepareFragmentsList();
                //   initPager();
//                avilableRewardsAdapter = new AvilableRewardsAdapter(rewardsList, getContext());
//                rewardsRecyclerView.setAdapter(avilableRewardsAdapter);
                if (isVisible())
                    fillViewPager();
            } else {
                ll_error.setVisibility(View.VISIBLE);
//                viewPager.setVisibility(View.GONE);
            }
        } else {
            ll_error.setVisibility(View.VISIBLE);
            RewardsFragment.rewardsFragmentInstance.setSavedValue("0");
//            viewPager.setVisibility(View.GONE);
        }
    }

    private void showErrorFragmmet() {
        ll_error.setVisibility(View.VISIBLE);
//        viewPager.setVisibility(View.GONE);
    }

    private void onGetRewardsFailure() {

        showErrorView();
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
//        super.onErrorListener(responseModel);
        try {
            if (isVisible())
                progressBar.setVisibility(View.GONE);
            showErrorView();

        }catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    public void showErrorView() {
        if (isVisible())
            if (currentLanguage != null && usedRewardsoutput != null && usedRewardsoutput.getErrorMsgEn() != null) {
                if (currentLanguage.equalsIgnoreCase("en"))
                    tv_error.setText(usedRewardsoutput.getErrorMsgEn());
                else
                    tv_error.setText(usedRewardsoutput.getErrorMsgAr());

            } else {
                tv_error.setText(getString(R.string.generice_error));
            }

        ll_dummyContainer.setVisibility(View.VISIBLE);
        if (isVisible())
            bt_retry.setText(getString(R.string.retry));
        if (isVisible())
            bt_retry.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                onConsumeService();
                                                ll_dummyContainer.setVisibility(View.GONE);
                                            }
                                        }
            );
    }

    public void refreshView() {
        onConsumeService();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
