package com.indy.views.fragments.usage;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.adapters.UsagePagerAdapter;
import com.indy.controls.ServiceUtils;
import com.indy.customviews.CustomTabLayout;
import com.indy.models.balance.BalanceInputModel;
import com.indy.models.balance.BalanceResponseModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.services.GetUserBalanceService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.SwpeMainActivity;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.MasterFragment;

import java.util.Date;

/**
 * Created by emad on 8/9/16.
 */
public class UsageFragment extends MasterFragment {

    private View view;
    public ViewPager viewPager;
    private TextView expiryTxt;
    private BaseResponseModel mResponseModel;
    private BalanceInputModel balanceInputModel;
    private BalanceResponseModel balanceResponseModel;
    private TextView usageTxtID;
    private ImageView ic_scroll_up;
    public static  TextView tv_lastUpdated,  balanceID;
    public static UsageFragment usageFragmentInsatnce;

    public static LinearLayout ll_transition;
    public static TextView tv_balanceID;
    public static int tabLayoutIndex = -1;
    public static UsageFragment usageFragmentInstance;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_usage, container, false);
            initUI();
            sharedPrefrencesManger.setLastUpdated(new Date().getTime()+"");
        }
        if (usageTxtID != null)
            usageTxtID.setText(getString(R.string.usage_spaced));
        if (ic_scroll_up != null)
            ic_scroll_up.setBackground(getResources().getDrawable(R.drawable.ic_scroll));
        Bundle bundle = new Bundle();

        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "USAGESCREEN");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "USAGESCREEN");
        mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_USAGE, bundle);
        if(tabLayoutIndex>0){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(viewPager!=null && tabLayout !=null) {
                        viewPager.setCurrentItem(tabLayoutIndex, true);
                        tabLayout.getTabAt(tabLayoutIndex).select();
                        tabLayoutIndex = -1;
                    }
                }
            },5);


        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        onConsumeService();
        if (tv_lastUpdated != null)
            tv_lastUpdated.setText(getString(R.string.last_updated) + " " + sharedPrefrencesManger.getLastUpdatedBalance() +
                    " " + getString(R.string.min_ago));
    }

    public void setSelectedTab(int index){
        viewPager.setCurrentItem(index);
    }

    @Override
    public void initUI() {
        super.initUI();
        usageFragmentInstance  = this;
//        expiryTxt = (TextView) view.findViewById(R.id.expiryTxt);
        balanceID = (TextView) view.findViewById(R.id.balanceID);
        usageTxtID = (TextView) view.findViewById(R.id.usageTxtID);
        ic_scroll_up = (ImageView) view.findViewById(R.id.ic_scroll_up);
        ic_scroll_up.setBackground(getResources().getDrawable(R.drawable.ic_scroll));
        tv_lastUpdated = (TextView) view.findViewById(R.id.tv_last_updated);
        tv_lastUpdated.setText(getString(R.string.last_updated) + " " + sharedPrefrencesManger.getLastUpdatedBalance() +
                " " + getString(R.string.min_ago));
        usageFragmentInsatnce = this;

        ll_transition = (LinearLayout) view.findViewById(R.id.ll_transition);
        tv_balanceID = (TextView) view.findViewById(R.id.balanceID);
        initTabLayout();
    }
    public CustomTabLayout tabLayout;
    private void initTabLayout() {
        tabLayout = (CustomTabLayout) view.findViewById(R.id.tab_layout);
        tabLayout.setSharedPrefrencesManger(sharedPrefrencesManger);
        if (sharedPrefrencesManger.getUsageStaus() == true) {
            tabLayout.addTab(tabLayout.newTab().setText("     "+ (getString(R.string.packages))+"    "));
        }
        tabLayout.addTab(tabLayout.newTab().setText("     "+ (getString(R.string.pay_per_use))+"         "));
        tabLayout.addTab(tabLayout.newTab().setText(" "+ (getString(R.string.wifi_hot_spot))+ " "));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        viewPager = (ViewPager) view.findViewById(R.id.pager);
        final UsagePagerAdapter adapter = new UsagePagerAdapter
                (getActivity().getSupportFragmentManager(), tabLayout.getTabCount(),getActivity().getApplicationContext(), this);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(0, true);
        viewPager.setSelected(true);
        viewPager.setFocusable(true);
        tabLayout.getTabAt(0).select();
        tabLayout.setSelectedTabIndicatorColor(Color.WHITE);
        tabLayout.setFillViewport(true);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                tabLayout.setSelectedTabIndicatorColor(Color.WHITE);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        if(tabLayoutIndex>0) {
            viewPager.setCurrentItem(tabLayoutIndex, true);
            tabLayout.getTabAt(tabLayoutIndex).select();
            tabLayoutIndex = -1;
        }
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            if (isVisible()) {
                if(responseModel!=null && responseModel.getResultObj()!=null) {
                    this.mResponseModel = responseModel;
                    if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.balanceServiceType)) {
                        onGetBalance(mResponseModel);
                    }
                }

            }
        }catch (Exception ex){
            if(ex!=null){
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        try{
        super.onErrorListener(responseModel);

        }catch (Exception ex){
            if(ex!=null){
                ex.printStackTrace();
            }
        }
    }

    /*
    on get balance response
    incase of sucess get packges and payperus
     */
    private void onGetBalance(BaseResponseModel responseModel) {
        balanceResponseModel = (BalanceResponseModel) responseModel.getResultObj();
        if (balanceResponseModel != null) {
            if (balanceResponseModel.getErrorCode() != null) {
                if (currentLanguage != null)
                    if (currentLanguage.equalsIgnoreCase("en"))
                        balanceID.setText(getResources().getString(R.string.your_balance_is_only) + "0.0");
                    else
                        balanceID.setText(getResources().getString(R.string.your_balance_is_only) + "0.0");

            } else
                onGetBalanceSucess();
//                onGetBalanceError();


        }
    }

    private void onGetBalanceSucess() {
        if (currentLanguage != null)
            if (currentLanguage.equalsIgnoreCase("en"))
                balanceID.setText(getResources().getString(R.string.your_balance_is_only) + " " + getResources().getString(R.string.aed)+ " " + balanceResponseModel.getAmount()+ " " + getString(R.string.aed_arabic_only));
            else
                balanceID.setText(getResources().getString(R.string.your_balance_is_only) + " " + balanceResponseModel.getAmount()+ " " + getString(R.string.aed_arabic_only));
        tv_lastUpdated.setText(getString(R.string.last_updated) + " " + sharedPrefrencesManger.getLastUpdatedBalance() +
                " " + getString(R.string.min_ago));
        CommonMethods.updateBalanceAndLastUpdated(getString(R.string.your_balance_is_aed) + " " + balanceResponseModel.getAmount()+ " " + getString(R.string.aed_arabic_only)
                ,getString(R.string.last_updated) + " " + sharedPrefrencesManger.getLastUpdatedBalance() +
                        " " + getString(R.string.min_ago));
        // expiryTxt.setText(balanceResponseModel.getExpiryTextEn() + " " + balanceResponseModel.getExpiryIntervalEn());

    }

    private void onGetBalanceError() {

        Fragment errorFragmnet = new ErrorFragment();
        Bundle bundle = new Bundle();
        if (currentLanguage != null) {
            if (currentLanguage.equalsIgnoreCase("en"))
                bundle.putString(ConstantUtils.errorString, balanceResponseModel.getErrorMsgEn());
            else
                bundle.putString(ConstantUtils.errorString, balanceResponseModel.getErrorMsgAr());
        }
        errorFragmnet.setArguments(bundle);
        ((SwpeMainActivity) getActivity()).replaceFragmnet(errorFragmnet, R.id.contentFrameLayout, true);

    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        balanceInputModel = new BalanceInputModel();
        balanceInputModel.setAppVersion(appVersion);
        balanceInputModel.setToken(token);
        balanceInputModel.setOsVersion(osVersion);
        balanceInputModel.setChannel(channel);
        balanceInputModel.setDeviceId(deviceId);
        balanceInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        balanceInputModel.setBillingPeriod(CommonMethods.getBillingPeriod());//cureent month
        balanceInputModel.setAuthToken(authToken);
        new GetUserBalanceService(this, balanceInputModel, getActivity());
    }

    public void onSlideUp() {
        if (usageTxtID != null) {
            usageTxtID.setVisibility(View.VISIBLE);
            if (isVisible())
                ic_scroll_up.setBackground(getResources().getDrawable(R.drawable.ic_scroll));
        }
    }

    public void onSlideDown() {
        if (usageTxtID != null) {
            usageTxtID.setVisibility(View.INVISIBLE);
            if (isVisible())
                ic_scroll_up.setBackground(getResources().getDrawable(R.drawable.ic_up));
        }
    }

    public static void setWifiHotspotPage(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(usageFragmentInsatnce.viewPager!=null && usageFragmentInsatnce.tabLayout !=null) {
                    usageFragmentInsatnce.viewPager.setCurrentItem(2, true);
                    usageFragmentInsatnce.tabLayout.getTabAt(2).select();
                    tabLayoutIndex = -1;
                }
            }
        },50);
    }

}
