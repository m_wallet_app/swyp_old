package com.indy.views.fragments.gamification.challenges;

import android.Manifest;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.indy.R;
import com.indy.helpers.GoogleMapsHelper;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.ConstantUtils;
import com.indy.utils.GPSTracker;
import com.indy.views.activites.MasterActivity;
import com.indy.views.fragments.gamification.models.serviceInputOutput.DoCheckInInputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetCheckinLocationsOutputModel;
import com.indy.views.fragments.gamification.services.GetCheckInLocationsListService;

import java.util.HashMap;

public class CheckInLocationsActivity extends MasterActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
        GoogleMap.OnInfoWindowClickListener, LocationListener {


    LocationManager locationManager;
    FrameLayout frameLayout;
    Location mLocation;
    Double mLatitude, mLongitude;
    GoogleMap mMap;
    public MarkerOptions marker;
    ImageView iv_locateMe;

    GoogleMapsHelper googleMapsHelper;
    public static HashMap<String, Integer> mMarkerList;
    public static HashMap<LatLng, Marker> mMarkersForSettingList;
    Marker highlightedMarker;
    String locationName;
    //---------
    LinearLayout ll_dummyContainer;
    TextView bt_retry;
    TextView tv_error;
    //------
    GetCheckinLocationsOutputModel getCheckinLocationsOutputModel;
    GPSTracker gpsTracker;
    public static final int LOCATION_REQUEST = 1340;
    public static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_in_locations);
        gpsTracker = new GPSTracker(this);

        mMarkerList = new HashMap<String, Integer>();
        mMarkersForSettingList = new HashMap<>();
        initUI();

    }

    @Override
    public void initUI() {
        super.initUI();


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
        initErrorLayout();
        TextView headerText = (TextView) findViewById(R.id.titleTxt);
        headerText.setText(getString(R.string.map));
        googleMapsHelper = new GoogleMapsHelper(getApplicationContext());
        iv_locateMe = (ImageView) findViewById(R.id.iv_locateMe);
        iv_locateMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMap != null && mLocation != null) {
                    LatLng latLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, ConstantUtils.zoomLevel));
                }
            }
        });

        //..............header Buttons............
        RelativeLayout backLayout = (RelativeLayout) findViewById(R.id.backLayout);
        RelativeLayout helpLayout = (RelativeLayout) findViewById(R.id.helpLayout);
        helpLayout.setVisibility(View.INVISIBLE);

        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard();
                onBackPressed();
            }
        });

        //..............header Buttons............
    }

    private void showErrorLayout(){
        ll_dummyContainer.setVisibility(View.VISIBLE);
        if(getCheckinLocationsOutputModel!=null && getCheckinLocationsOutputModel.getErrorMsgEn()!=null) {
            if (sharedPrefrencesManger != null && sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.enLng)) {
                tv_error.setText(getCheckinLocationsOutputModel.getErrorMsgEn());
            } else {
                tv_error.setText(getCheckinLocationsOutputModel.getErrorMsgAr());
            }
        }else{
            tv_error.setText(getString(R.string.error_txt));
        }
    }

    private void initErrorLayout() {
        ll_dummyContainer = (LinearLayout) findViewById(R.id.ll_dummy_error_container);
        ll_dummyContainer.setVisibility(View.GONE);
        bt_retry = (TextView) findViewById(R.id.retryBtn);
        bt_retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onConsumeService();
            }
        });
        tv_error = (TextView) findViewById(R.id.tv_error);
        if(getCheckinLocationsOutputModel!=null && getCheckinLocationsOutputModel.getErrorMsgEn()!=null) {
            if (sharedPrefrencesManger != null && sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.enLanguage)) {
                tv_error.setText(getCheckinLocationsOutputModel.getErrorMsgEn());
            } else {
                tv_error.setText(getCheckinLocationsOutputModel.getErrorMsgAr());
            }
        }else{
            tv_error.setText(getString(R.string.error_txt));
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if(location!=null){
            mLatitude = location.getLatitude();
            mLongitude = location.getLongitude();
            mLocation = location;
            iv_locateMe.callOnClick();
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        GoogleMapsHelper openGoogleMaps = new GoogleMapsHelper(this);
        openGoogleMaps.onGoogleMpasOpen(marker.getPosition().latitude, marker.getPosition().longitude, locationName);

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        isGPSEnabled();
    }

    int counter = 0;

    private void isGPSEnabled() {
        counter ++;
        LocationManager locationManager = (LocationManager)
                getSystemService(LOCATION_SERVICE);

//                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && canAccessLocation()) {// gps is enable
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {// gps is disable
//                    showErrorFragment();
                    showGPSError();
                } else {
                    if (gpsTracker.canGetLocation()) {
                        mLocation = gpsTracker.getLocation();
                        if (mLocation != null) {

                            onConsumeService();
                            iv_locateMe.callOnClick();
                            addMarkerAtCurrentPosition(mLocation);
                        }

                    }else{
                        try{
                            Thread.sleep(2000);
                        }catch (InterruptedException ex){
                            ex.printStackTrace();
                        }
                        if(counter<5)
                        isGPSEnabled();
                    }
//                    checkLocation();
                }


    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();

        DoCheckInInputModel gamificationBaseInputModel = new DoCheckInInputModel();
        gamificationBaseInputModel.setChannel(channel);
        gamificationBaseInputModel.setLang(currentLanguage);
        gamificationBaseInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        gamificationBaseInputModel.setAppVersion(appVersion);
        gamificationBaseInputModel.setAuthToken(authToken);
        gamificationBaseInputModel.setDeviceId(deviceId);
        gamificationBaseInputModel.setToken(token);
        gamificationBaseInputModel.setUserSessionId(sharedPrefrencesManger.getUserSessionId());
        gamificationBaseInputModel.setUserId(sharedPrefrencesManger.getUserId());
        gamificationBaseInputModel.setApplicationId(ConstantUtils.INDY_TALOS_ID);
        gamificationBaseInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
        gamificationBaseInputModel.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        gamificationBaseInputModel.setOsVersion(osVersion);
        gamificationBaseInputModel.setLongitude(mLocation.getLongitude());
        gamificationBaseInputModel.setLatitude(mLocation.getLatitude());
        new GetCheckInLocationsListService(this,gamificationBaseInputModel);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        if(responseModel!=null && responseModel.getResultObj()!=null){
            getCheckinLocationsOutputModel = (GetCheckinLocationsOutputModel) responseModel.getResultObj();
            if(getCheckinLocationsOutputModel!=null && getCheckinLocationsOutputModel.getLocations()!=null &&
                    getCheckinLocationsOutputModel.getLocations().size()>0){
                onCheckinListSuccess();
            }else{
                showErrorLayout();
            }
        }
    }

    private void onCheckinListSuccess() {
            drawMapPins();    
    }

    public void drawMapPins() {
        mMap.clear();
        LatLng fLatLng = null;
        addMarkerAtCurrentPosition(mLocation);

        Marker temp = null;
        ll_dummyContainer.setVisibility(View.GONE);

        for (int i = 0; i < getCheckinLocationsOutputModel.getLocations().size(); i++) {

            LatLng mLatLng = new LatLng(getCheckinLocationsOutputModel.getLocations().get(i).getLatitude(),
                    getCheckinLocationsOutputModel.getLocations().get(i).getLongitude());
            marker = new MarkerOptions().position(mLatLng);


//             marker.icon(BitmapDescriptorFactory.fromBitmap(etisaltLogo));
            String title = "";
            if (currentLanguage.equals("en")) {
                title = getCheckinLocationsOutputModel.getLocations().get(i).getName();
            } else {
                title = getCheckinLocationsOutputModel.getLocations().get(i).getName();
            }
            String dist = "-";
            if (getCheckinLocationsOutputModel.getLocations().get(i).getDistance() != null) {
                dist = getCheckinLocationsOutputModel.getLocations().get(i).getDistance() + " Km";
            }

            String address = "";
            if (getCheckinLocationsOutputModel.getLocations().get(i).getBusinessName() != null) {
                address = getCheckinLocationsOutputModel.getLocations().get(i).getBusinessName() ;
            }
            if (i == 0) {
                temp = mMap.addMarker(
                        marker.position(mLatLng)
                                .title(title)
                                .snippet(address)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.location_icon_fill)));
//                tv_name.setText(title);
//                tv_distance.setText(dist);
//                ll_marker.setVisibility(View.VISIBLE);
                highlightedMarker = temp;

            } else {
                temp = mMap.addMarker(
                        marker.position(mLatLng)
                                .title(title)
                                .snippet(address)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.location_orange)));

            }
            mMarkerList.put(temp.getId(), i);
            mMarkersForSettingList.put(mLatLng,temp);


//            if(i==0){
//                tempMarker=temp;
//            }
        }
        fLatLng = new LatLng(getCheckinLocationsOutputModel.getLocations().get(0).getLatitude(),
                getCheckinLocationsOutputModel.getLocations().get(0).getLongitude());

        if (currentLanguage.equals("en")) {
            locationName = getCheckinLocationsOutputModel.getLocations().get(0).getBusinessName();
        } else {
            locationName = getCheckinLocationsOutputModel.getLocations().get(0).getBusinessName();
        }
//         distanceId.setText( getString(R.string.nearest_shop) + " " + distance + "  " +  getString(R.string.away));
        mLatitude = getCheckinLocationsOutputModel.getLocations().get(0).getLatitude();
        mLongitude = getCheckinLocationsOutputModel.getLocations().get(0).getLongitude();
//         locationNameId.setText( locationName);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(fLatLng,
                ConstantUtils.zoomLevel));

        mMap.setOnMarkerClickListener(this);
        mMap.setOnInfoWindowClickListener(this);
        mMap.setInfoWindowAdapter(new MyInfoWindowAdapter());
//        if(tempMarker!=null){
//            tempMarker.showInfoWindow();
//        }

    }

    private class MyInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private final View myContentsView;

        MyInfoWindowAdapter() {
            myContentsView = getLayoutInflater().inflate(R.layout.my_info_window, null);
        }

        @Override
        public View getInfoContents(Marker marker) {

            TextView tvTitle = ((TextView) myContentsView.findViewById(R.id.tv_name));
            tvTitle.setText(marker.getTitle());
            TextView tvSnippet = ((TextView) myContentsView.findViewById(R.id.tv_distance));
            tvSnippet.setText(marker.getSnippet());

            return myContentsView;
        }

        @Override
        public View getInfoWindow(Marker marker) {
            // TODO Auto-generated method stub
            return null;
        }

    }

    public void showGPSError(){

        tv_error.setText(getString(R.string.enable_gps));
        ll_dummyContainer.setVisibility(View.VISIBLE);

        bt_retry.setText(getString(R.string.settings));
        bt_retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, 100);
            }
        });    }

    public void addMarkerAtCurrentPosition(Location mLocation) {
        if (mLocation != null) {
            LatLng mLatLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
            MarkerOptions tempMarker = new MarkerOptions().position(mLatLng);
            mMap.addMarker(
                    tempMarker.position(mLatLng)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.locate_me_marker)));
        }
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        ll_dummyContainer.setVisibility(View.GONE);
        switch (requestCode) {
            case 100:
                try{
                    Thread.sleep(2000);
                }catch (InterruptedException ex){
                    ex.printStackTrace();
                }
                LocationManager locationManager = (LocationManager)getSystemService(LOCATION_SERVICE);
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    showGPSError();
                } else {
                    isGPSEnabled();
                }
                break;
            default:
                break;
        }

    }


}



