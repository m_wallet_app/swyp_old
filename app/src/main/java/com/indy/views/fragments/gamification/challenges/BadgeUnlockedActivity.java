package com.indy.views.fragments.gamification.challenges;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.indy.R;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.GetRewardForSharingService;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.ShareMasterActivity;
import com.indy.views.fragments.gamification.models.getnotificationpopupdetails.GetNotificationPopupDetailsInput;
import com.indy.views.fragments.gamification.models.getnotificationpopupdetails.GetNotificationPopupDetailsOutput;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetRewardForSharingInputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetRewardForSharingOutputResponse;
import com.indy.views.fragments.gamification.services.GetNotificationPopupDetailsService;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class BadgeUnlockedActivity extends ShareMasterActivity {
    private String notificationId;
    GetNotificationPopupDetailsOutput getNotificationPopupDetailsOutput;
    private TextView tv_title, tv_subtitle, tv_rewardAmount, tv_rewardText, tv_close, tv_viewBadges;
    private ImageView iv_badge_unlock, iv_badgeDetail2;
    private FrameLayout frameLayout;
    private TextView fbShare;
    private TextView twitterShare;

    private CoordinatorLayout coordinatorLayout;
    private BottomSheetBehavior behavior;
    private View bottomSheet;
    private Button shareButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_badge_unlocked);
        initUI();
    }

    @Override
    public void initUI() {
        super.initUI();

        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_subtitle = (TextView) findViewById(R.id.tv_badge_detail);
        tv_rewardAmount = (TextView) findViewById(R.id.tv_rewards_count);
        tv_rewardText = (TextView) findViewById(R.id.tv_motivation_text);
        iv_badge_unlock = (ImageView) findViewById(R.id.iv_badge_unlock);
        tv_close = (TextView) findViewById(R.id.tv_close);
        frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
        tv_viewBadges = (TextView) findViewById(R.id.tv_view_badges);
        iv_badgeDetail2 = (ImageView) findViewById(R.id.iv_badge_detail_dummy) ;
        tv_viewBadges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                sharedPrefrencesManger.setNavigationBadgeIndex("0");
//                Intent intent = new Intent(BadgeUnlockedActivity.this,SwpeMainActivity.class);
//                intent.putExtra(ConstantUtils.CHALLENGES_TAB_NAVIGATION_INDEX,"0");
//                startActivity(intent);
//                ChallengesMainFragment challengesMainFragment = new ChallengesMainFragment();
//                Bundle bundle = new Bundle();
//                bundle.putString(ConstantUtils.CHALLENGES_TAB_NAVIGATION_INDEX, "0");
//                challengesMainFragment.setArguments(bundle);
//                SwpeMainActivity.swpeMainActivityInstance.replaceFragmnet(challengesMainFragment, R.id.contentFrameLayout, true);
//                SwpeMainActivity.clickedItem = SwpeMainActivity.rewardsClicked;
            }
        });
        if (getIntent() != null && getIntent().getExtras() != null) {
            notificationId = getIntent().getExtras().getString(ConstantUtils.KEY_NOTIFICATION_ID_FOR_SERVICE, "");

        }
        tv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

//        Glide.with(this) // replace with 'this' if it's in activity
//                .load("https://uat-swypapp.etisalat.ae/Indy/assets/images/badges/90Friends-&-Gems-flatflat128.gif")
//                .asGif()
//                .skipMemoryCache(true)
//                .diskCacheStrategy(DiskCacheStrategy.NONE)
//                .error(R.drawable.bitmap_default_image) // show error drawable if the image is not a gif
//                .into(iv_badge_unlock);
        if (notificationId != null && !notificationId.isEmpty()) {
            onConsumeService();
        } else {
            finish();
        }

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinateLayoutID);
        bottomSheet = coordinatorLayout.findViewById(R.id.share_photo_bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {

                } else if (newState == BottomSheetBehavior.STATE_EXPANDED) {

                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });
        fbShare = (TextView) findViewById(R.id.fb_share);
        twitterShare = (TextView) findViewById(R.id.twitter_share);
        fbShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                referenceID = getNotificationPopupDetailsOutput.getId();
//                BitmapDrawable drawable = (BitmapDrawable) iv_badge_unlock.getDrawable();
//                Bitmap bitmap = drawable.getBitmap();
//                setBitmapToShare(bitmap);

                ImageLoader.getInstance().displayImage(getNotificationPopupDetailsOutput.getImageToShareUrl(), iv_badgeDetail2, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {

                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        setBitmapToShare(null);
                        facebookLogin(true,getNotificationPopupDetailsOutput.getId(),"");

                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        setBitmapToShare(loadedImage);
                        facebookLogin(true,getNotificationPopupDetailsOutput.getId(),"");
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {

                    }
                });


            }
        });
        twitterShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                referenceID = getNotificationPopupDetailsOutput.getId();
                ImageLoader.getInstance().displayImage(getNotificationPopupDetailsOutput.getImageToShareUrl(), iv_badgeDetail2, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {

                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        setBitmapToShare(null);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (checkSelfPermission(Manifest.permission_group.STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                String[] newPerms = {Manifest.permission.READ_EXTERNAL_STORAGE,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE};
                                requestPermissions(newPerms, 2000);
                            }
                            ;
                        } else {
                            twitterLogin(true, getNotificationPopupDetailsOutput.getId(),"");

                        }
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        setBitmapToShare(loadedImage);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (checkSelfPermission(Manifest.permission_group.STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                String[] newPerms = {Manifest.permission.READ_EXTERNAL_STORAGE,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE};
                                requestPermissions(newPerms, 2000);
                            }
                            ;
                        } else {
                            twitterLogin(true, getNotificationPopupDetailsOutput.getId(),"");

                        }
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {

                    }
                });
            }
        });
        shareButton = (Button) findViewById(R.id.btn_share);
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onConsumeGetRewardService();

            }
        });
        if (getIntent() != null && getIntent().getExtras() != null) {
            if(getIntent().getExtras().getString(ConstantUtils.notification_type,"0").equalsIgnoreCase("1")){
                shareButton.setVisibility(View.INVISIBLE);
            }else{
                shareButton.setVisibility(View.VISIBLE);
            }
        }

    }

    private void onGetShareRewardSuccess(String rewardString){
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        if(rewardString.length()>0) {
            ((TextView) findViewById(R.id.tv_reward_text)).setText(getString(R.string.share_your_achievement) + " " +
                    getString(R.string.and_get_rewarded_with) + rewardString);
        }else{
            ((TextView) findViewById(R.id.tv_reward_text)).setText(getString(R.string.share_your_achievement));
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        if(sharedPrefrencesManger.getNavigationBadgeIndex().length()>0){
            finish();
        }

    }

    public void onConsumeGetRewardService(){
        addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        GetRewardForSharingInputModel getBadgeDetailsInputModel = new GetRewardForSharingInputModel();
        getBadgeDetailsInputModel.setChannel(channel);
        getBadgeDetailsInputModel.setLang(currentLanguage);
        getBadgeDetailsInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        getBadgeDetailsInputModel.setAppVersion(appVersion);
        getBadgeDetailsInputModel.setAuthToken(authToken);
        getBadgeDetailsInputModel.setDeviceId(deviceId);
        getBadgeDetailsInputModel.setToken(token);
        getBadgeDetailsInputModel.setUserSessionId(sharedPrefrencesManger.getUserSessionId());
        getBadgeDetailsInputModel.setUserId(sharedPrefrencesManger.getUserId());
        getBadgeDetailsInputModel.setApplicationId(ConstantUtils.INDY_TALOS_ID);
        getBadgeDetailsInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
        getBadgeDetailsInputModel.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        getBadgeDetailsInputModel.setOsVersion(osVersion);
        getBadgeDetailsInputModel.setActionName(ConstantUtils.badge_share);
        new GetRewardForSharingService(this, getBadgeDetailsInputModel);
    }


    @Override
    public void onConsumeService() {
        super.onConsumeService();
        frameLayout.setVisibility(View.VISIBLE);
        addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        GetNotificationPopupDetailsInput getNotificationPopupDetailsInput = new GetNotificationPopupDetailsInput();
        getNotificationPopupDetailsInput.setChannel(channel);
        getNotificationPopupDetailsInput.setLang(currentLanguage);
        getNotificationPopupDetailsInput.setMsisdn(sharedPrefrencesManger.getMobileNo());
        getNotificationPopupDetailsInput.setAppVersion(appVersion);
        getNotificationPopupDetailsInput.setAuthToken(authToken);
        getNotificationPopupDetailsInput.setDeviceId(deviceId);
        getNotificationPopupDetailsInput.setToken(token);
        getNotificationPopupDetailsInput.setImsi(sharedPrefrencesManger.getMobileNo());
        getNotificationPopupDetailsInput.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        getNotificationPopupDetailsInput.setOsVersion(osVersion);

        getNotificationPopupDetailsInput.setApplicationId(ConstantUtils.INDY_TALOS_ID);
        getNotificationPopupDetailsInput.setUserId(sharedPrefrencesManger.getUserId());
        getNotificationPopupDetailsInput.setUserSessionId(sharedPrefrencesManger.getUserSessionId());
        getNotificationPopupDetailsInput.setNotificationId(notificationId);
        new GetNotificationPopupDetailsService(this, getNotificationPopupDetailsInput);

    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        onBackPressed();
        if (responseModel != null && responseModel.getResultObj() != null) {
            if(responseModel.getServiceType()!=null && responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.GET_NOTIFICATION_POPUP_DETAILS)) {
                getNotificationPopupDetailsOutput = (GetNotificationPopupDetailsOutput) responseModel.getResultObj();
                if (getNotificationPopupDetailsOutput != null) {
                    onGetNotificationSuccess();
                } else {
                    onGetNotificationFailure();
                }
            } else if(responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.getRewardForSharingService)){
                GetRewardForSharingOutputResponse getRewardForSharingOutputResponse = (GetRewardForSharingOutputResponse) responseModel.getResultObj();
                if(getRewardForSharingOutputResponse.getRewardAmount()!=null){
                    onGetShareRewardSuccess(getRewardForSharingOutputResponse.getRewardAmount());
                }else{
                    onGetShareRewardSuccess("");
                }
            }
        } else {
            onGetNotificationFailure();
        }
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        onBackPressed();
        if(responseModel.getServiceType()!=null && responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.getRewardForSharingService)){
            onGetShareRewardSuccess("");
        }
    }

    private void onGetNotificationFailure() {
        showErrorFragment();
    }

    public void showErrorFragment() {
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        if (getNotificationPopupDetailsOutput != null && getNotificationPopupDetailsOutput.getErrorMsgEn() != null) {
            if (sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)) {
                bundle.putString(ConstantUtils.errorString, getNotificationPopupDetailsOutput.getErrorMsgEn());
            } else {
                bundle.putString(ConstantUtils.errorString, getNotificationPopupDetailsOutput.getErrorMsgAr());
            }
        } else {
            bundle.putString(ConstantUtils.errorString, getString(R.string.generice_error));
        }

        errorFragment.setArguments(bundle);
        frameLayout.setVisibility(View.VISIBLE);
        addFragmnet(errorFragment, R.id.frameLayout, true);

    }

    private void onGetNotificationSuccess() {
        if(getNotificationPopupDetailsOutput!=null) {

            if(getNotificationPopupDetailsOutput.getTitle()!=null)
            tv_title.setText(getNotificationPopupDetailsOutput.getTitle());

            if(getNotificationPopupDetailsOutput.getText()!=null)
            tv_subtitle.setText(getNotificationPopupDetailsOutput.getText());

            if(getNotificationPopupDetailsOutput.getImageUrl()!=null && getNotificationPopupDetailsOutput.getImageUrl().length()>0) {
//            ImageLoader.getInstance().displayImage(getNotificationPopupDetailsOutput.getImageUrl(), iv_badge_unlock);
                Glide.with(this).asGif().
                        load(getNotificationPopupDetailsOutput.getImageUrl())
                        .into(iv_badge_unlock)

                        .onLoadFailed(getResources().getDrawable(R.drawable.bitmap_default_image));

            }
            if(getNotificationPopupDetailsOutput.getReward()!=null)
            tv_rewardAmount.setText(getString(R.string.reward) + " " +getNotificationPopupDetailsOutput.getReward()+
                    " " + getString(R.string.swypees));

//            if(getNotificationPopupDetailsOutput.getRewardText()!=null)
//            tv_rewardText.setText(getNotificationPopupDetailsOutput.getRewardText());
//        Glide.with(context) // replace with 'this' if it's in activity
//                .load("https://uat-swypapp.etisalat.ae/Indy/assets/images/badges/90Friends-&-Gems-flatflat128.gif")
//                .asGif()
//                .error(R.drawable.bitmap_default_image) // show error drawable if the image is not a gif
//                .into(iv_badge_unlock);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (frameLayout.getVisibility() == View.VISIBLE) {
            frameLayout.setVisibility(View.GONE);
        } else {
            finish();
        }
    }
}
