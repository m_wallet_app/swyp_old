package com.indy.views.fragments.findus;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.adapters.FindUsPagerAdapter;
import com.indy.customviews.*;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.HelpActivity;
import com.indy.views.activites.MasterActivity;
import com.indy.views.fragments.utils.ErrorFragment;

import static com.indy.R.id.langBtn;

/**
 * Created by hadi on 11/10/16.
 */
public class FindUsFragment extends MasterActivity {

    ViewPager viewPager;
    private TextView titleTxt, headerTxt;
    private Button backImg, helpImg;
    FrameLayout frameLayout;
    public static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
    };
    public static final int LOCATION_REQUEST = 1340;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_find_us);
        initUI();
        onConsumeService();
    }

    @Override
    public void initUI() {
        super.initUI();
        titleTxt = (TextView) findViewById(R.id.tv_title_findus);
        headerTxt = (TextView) findViewById(R.id.titleTxt);
        headerTxt.setText(getString(R.string.find_us_spaced));
        backImg = (Button) findViewById(R.id.backImg);
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
//..............header Buttons............
        RelativeLayout backLayout = (RelativeLayout) findViewById(R.id.backLayout);
        RelativeLayout helpLayout = (RelativeLayout) findViewById(R.id.helpLayout);

        helpLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FindUsFragment.this, HelpActivity.class);
                startActivity(intent);
            }
        });

        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard();
                onBackPressed();
            }
        });
        //..............header Buttons............
        helpImg = (Button) findViewById(R.id.helpBtn);
        helpImg.setVisibility(View.INVISIBLE);
        frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
        frameLayout.setVisibility(View.GONE);
        checkPermission();

//        isGPSEnabled();

    }

    private void checkPermission() {
//        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M || canAccessLocation();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                this.requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            } else {
                LocationManager locationManager = (LocationManager)
                        getSystemService(LOCATION_SERVICE);
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    showErrorFragment(true, false);
                    initTabLayout();

//            initTabLayout();
                } else {
                    if (CommonMethods.isConnected(this)) {
                        initTabLayout();
                    } else {
                        showErrorFragment(false, false);
                    }
                }
            }
        } else {
            LocationManager locationManager = (LocationManager)
                    getSystemService(LOCATION_SERVICE);
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                showErrorFragment(true, false);
                initTabLayout();

//            initTabLayout();
            } else {
                if (CommonMethods.isConnected(this)) {
                    initTabLayout();
                } else {
                    showErrorFragment(false, false);
                }
            }
        }
    }

    private boolean canAccessLocation() {
        if (!hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
            return false;
        } else {
            return true;
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == checkSelfPermission(perm));
    }

    private void initTabLayout() {
        final CustomTabLayout tabLayout = (CustomTabLayout) findViewById(R.id.tab_layout);
        tabLayout.setSharedPrefrencesManger(sharedPrefrencesManger);
        tabLayout.addTab(tabLayout.newTab().setText((getString(R.string.payment_machine_uppercase))));
        tabLayout.addTab(tabLayout.newTab().setText(("          " + getString(R.string.shops_uppercase)) + "          "));
        tabLayout.addTab(tabLayout.newTab().setText(("          " + getString(R.string.wifi_hotspots_uppercase) + "          ")));
        tabLayout.setTabGravity(TabLayout.GRAVITY_CENTER);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        viewPager = (ViewPager) findViewById(R.id.pager);
        final FindUsPagerAdapter adapter = new FindUsPagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(0, true);
        viewPager.setSelected(true);
        viewPager.setFocusable(true);
        tabLayout.getTabAt(0).select();
        titleTxt.setText(getString(R.string.locate_our_shop_now));
        tabLayout.setFillViewport(true);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

                if (tab.getPosition() == 2) {
                    titleTxt.setText(getString(R.string.find_your_hotspot));
                    CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_support_find_us_wifi_hotspots_tab);

                } else {
                    if(tab.getPosition()==0){
                        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_support_find_us_payment_machines_tab);

                    }else if(tab.getPosition()==1){
                        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_support_find_us_shops_tab);

                    }
                    titleTxt.setText(getString(R.string.locate_our_shop_now));
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

    }

    private void showErrorFragment(boolean isGPSError, boolean showMessage) {
        try {
            frameLayout.setVisibility(View.VISIBLE);
            frameLayout.bringToFront();
            ErrorFragment errorFragment = new ErrorFragment();
            Bundle bundle = new Bundle();
            if (isGPSError) {
                bundle.putString(ConstantUtils.errorString, getString(R.string.enable_gps));
                bundle.putString(ConstantUtils.settingsButtonEnabled, "true");
                bundle.putString(ConstantUtils.buttonTitle, getString(R.string.settings));
                bundle.putBoolean("cancel", true);
            } else if (showMessage) {
                bundle.putString(ConstantUtils.errorString, "You have chosen to never allow location. Please turn on location permission from Settings.");

            } else {
                bundle.putBoolean("finish", true);
                bundle.putString(ConstantUtils.errorString, getString(R.string.connection_error));
            }

            errorFragment.setArguments(bundle);

            replaceFragmnet(errorFragment, R.id.frameLayout, true);
        } catch (Exception ex) {
            finish();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        frameLayout.setVisibility(View.GONE);
        switch (requestCode) {
            case 100:
                LocationManager locationManager = (LocationManager)
                        getSystemService(LOCATION_SERVICE);
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    checkForGps();
                } else {

                    final TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
                    if (tabLayout.getTabCount() > 0)
                        tabLayout.removeAllTabs();
                    initTabLayout();
                }
                break;
            default:
                break;
        }

    }

    public void checkForGps() {
        showErrorFragment(true, false);

    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case LOCATION_REQUEST:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkPermission();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                        checkPermission();
                    } else {
                        showErrorFragment(false, true);
                        initTabLayout();
                    }
//                    Toast.makeText(this, "To use this feature, kindly allow Ding access to your location.", Toast.LENGTH_LONG).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            default:
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }


}
