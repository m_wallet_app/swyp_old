package com.indy.views.fragments.buynewline;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.controls.ServiceUtils;
import com.indy.models.getShipmentStatus.GetShipmentStatusInput;
import com.indy.models.getShipmentStatus.GetShipmentStatusOutput;
import com.indy.models.profilestatus.NewProileStautsOutput;
import com.indy.models.profilestatus.ProfileStatusInput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.GetShipmentStatusService;
import com.indy.services.ProfileStatusService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.utils.MaterialCheckBox;
import com.indy.views.activites.HelpActivity;
import com.indy.views.activites.LiveChatActivity;
import com.indy.views.fragments.StartScreenFragment;
import com.indy.views.fragments.login.LoginFragment;
import com.indy.views.fragments.login.StaticPackagesFragments;
import com.indy.views.fragments.utils.MasterFragment;

/**
 * Created by emad on 10/8/16.
 */

public class PaymentSucessfulllyFragment extends MasterFragment {
    private View view;
    private TextView refrenceNumberTxt, viewPackagesID;
    private Button helpBtn, liveChatBtn;
    private GetShipmentStatusOutput getShipmentStatusOutput;
    public static PaymentSucessfulllyFragment paymentSucessfulllyFragmentInstance;
    private ImageView iv_deliveryStatusOne, iv_deliveryStatusTwo, iv_deliveryStatusThree, iv_deliveryStatusline1, iv_deliveryStatusline2;
    private ProfileStatusInput profileStatusInput;
    private NewProileStautsOutput profileStatusOutput;
    private TextView tv_numberDelivery;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_sucessfully, container, false);
        paymentSucessfulllyFragmentInstance = this;
        initUI();

        return view;

    }

    @Override
    public void initUI() {
        super.initUI();
        refrenceNumberTxt = (TextView) view.findViewById(R.id.refrenceNoID);

        if (getArguments() != null && getArguments().getBoolean("isCOD", false)) {
            refrenceNumberTxt.setText(sharedPrefrencesManger.getTransactionID());
        } else {
            if (sharedPrefrencesManger.getRegisterationID() != null && !sharedPrefrencesManger.getRegisterationID().isEmpty()) {
                refrenceNumberTxt.setText(sharedPrefrencesManger.getTransactionID());
            }
        }

        tv_numberDelivery = (TextView) view.findViewById(R.id.tv_number_delivery);

        if (sharedPrefrencesManger.getMobileNo() != null && !sharedPrefrencesManger.getMobileNo().isEmpty()) {
            if (sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)) {
                tv_numberDelivery.setText(getString(R.string.the_sim_card) + " " + sharedPrefrencesManger.getMobileNo()
                        + " " + getString(R.string.will_be_delivered_soon) + " " + getString(R.string.wait_for_our_call));
            } else {
                tv_numberDelivery.setText(getString(R.string.the_sim_card) + " SIM " + sharedPrefrencesManger.getMobileNo()
                        + " " + getString(R.string.will_be_delivered_soon));
            }
        } else {
            tv_numberDelivery.setText(getString(R.string.the_sim_card)
                    + " " + getString(R.string.will_be_delivered_soon));
        }

        helpBtn = (Button) view.findViewById(R.id.helpBtn);
        liveChatBtn = (Button) view.findViewById(R.id.liveChatBtn);

        helpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), HelpActivity.class);
                startActivity(intent);
            }
        });


        viewPackagesID = (TextView) view.findViewById(R.id.viewPackagesID);
        viewPackagesID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle2 = new Bundle();

                bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_payment_success_viewpackages);
                bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_payment_success_viewpackages);
                mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_payment_success_viewpackages, bundle2);

                Intent intent = new Intent(getActivity(), StaticPackagesFragments.class);
                startActivity(intent);
            }
        });
        iv_deliveryStatusOne = (ImageView) view.findViewById(R.id.iv_step_one);
        iv_deliveryStatusTwo = (ImageView) view.findViewById(R.id.iv_step_two);
        iv_deliveryStatusThree = (ImageView) view.findViewById(R.id.iv_step_three);
        iv_deliveryStatusline1 = (ImageView) view.findViewById(R.id.iv_order_tracking_one);
        iv_deliveryStatusline2 = (ImageView) view.findViewById(R.id.iv_order_tracking_two);
        if (sharedPrefrencesManger != null)
            if (sharedPrefrencesManger.getLiveChatStaus() == true)
                enableLiveChatBtn();
            else
                disableLiveChatBtn();


        MaterialCheckBox materialCheckBox = (MaterialCheckBox) view.findViewById(R.id.fl_tick_animation);
        ImageView iv_tick = (ImageView) view.findViewById(R.id.iv_tick);
        CommonMethods.drawCircle(materialCheckBox, iv_tick);
    }

    @Override
    public void onResume() {
        super.onResume();
        onConsumeService();

    }

    private void disableLiveChatBtn() {
        liveChatBtn.setAlpha(0.5f);
        liveChatBtn.setEnabled(false);
        liveChatBtn.setOnClickListener(null);
    }

    private void enableLiveChatBtn() {
        liveChatBtn.setAlpha(1.0f);
        liveChatBtn.setEnabled(true);
        liveChatBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle bundle2 = new Bundle();

                bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_delivery_info_success_livechat);
                bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_delivery_info_success_livechat);
                mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_delivery_info_success_livechat, bundle2);

                startActivity(new Intent(getActivity(), LiveChatActivity.class));
            }
        });
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        getProfileStatus();

    }

    public void getShipmentStatus() {
        GetShipmentStatusInput getShipmentStatusInput = new GetShipmentStatusInput();
//        getShipmentStatusInput.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        getShipmentStatusInput.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
//        getShipmentStatusInput.setRegistrationId("A150573828212157");
        getShipmentStatusInput.setChannel(channel);
        new GetShipmentStatusService(this, getShipmentStatusInput);
    }

    public void setDeliverStatus(int result) {
        switch (result) {
            case 0:
                iv_deliveryStatusOne.setImageResource(R.drawable.oval_white);
                iv_deliveryStatusTwo.setImageResource(R.drawable.oval_orange);
                iv_deliveryStatusThree.setImageResource(R.drawable.oval_orange);
                iv_deliveryStatusline1.setImageResource(R.drawable.line_orange);
                iv_deliveryStatusline2.setImageResource(R.drawable.line_orange);
                break;

            case 1:
                iv_deliveryStatusOne.setImageResource(R.drawable.oval_white);
                iv_deliveryStatusTwo.setImageResource(R.drawable.oval_white);
                iv_deliveryStatusThree.setImageResource(R.drawable.oval_orange);
                iv_deliveryStatusline1.setImageResource(R.drawable.line_white);
                iv_deliveryStatusline2.setImageResource(R.drawable.line_orange);
                break;

            case 2:
                iv_deliveryStatusOne.setImageResource(R.drawable.oval_white);
                iv_deliveryStatusTwo.setImageResource(R.drawable.oval_white);
                iv_deliveryStatusThree.setImageResource(R.drawable.oval_white);
                iv_deliveryStatusline1.setImageResource(R.drawable.line_white);
                iv_deliveryStatusline2.setImageResource(R.drawable.line_white);
                break;
            default:
                break;
        }
    }

    public void onGetShipmentStatusSuccess() {
        if (getShipmentStatusOutput.getShipmentStatus() != null && !getShipmentStatusOutput.getShipmentStatus().isEmpty()) {
//            getShipmentStatusOutput.setShipmentStatus("CXL");
            processStatus(getShipmentStatusOutput.getShipmentStatus());
        } else {
            if (sharedPrefrencesManger.getLastStatus() != null && !sharedPrefrencesManger.getLastStatus().isEmpty()) {
                processStatus(sharedPrefrencesManger.getLastStatus());
            } else {
                setDeliverStatus(0);
            }
//            iv_deliveryStatus.setImageResource(R.drawable.tracker_processed);
        }
    }

    public void onGetShipmentStatusFailure() {
        //processing

        if (sharedPrefrencesManger.getLastStatus() != null && !sharedPrefrencesManger.getLastStatus().isEmpty()) {
            processStatus(sharedPrefrencesManger.getLastStatus());
        } else {
            setDeliverStatus(0);
        }
    }


    public void processStatus(String status) {
        if (status.equalsIgnoreCase("UPL")) {
            setDeliverStatus(0);
            sharedPrefrencesManger.setLastStatus(status);

        } else if (status.equalsIgnoreCase("NDLV")
                || status.equalsIgnoreCase("RFD")
                || status.equalsIgnoreCase("RETD")
                || status.equalsIgnoreCase("PKD")
                || status.equalsIgnoreCase("NPKD")
                || status.equalsIgnoreCase("REC")
                || status.equalsIgnoreCase("SLV")
            //        || status.equalsIgnoreCase("CXL"))
                ) {
            //processing
            setDeliverStatus(1);
            sharedPrefrencesManger.setLastStatus(status);

        } else if (status.equalsIgnoreCase("DLV")) {
            //|| status.equalsIgnoreCase("CXL"))

            setDeliverStatus(2);
            sharedPrefrencesManger.setLastStatus(status);


        } else if (status.equalsIgnoreCase("CXL")) {
            paymentSucessfulllyFragmentInstance = null;
            regActivity.addFragmnet(new StartScreenFragment(), R.id.frameLayout, false);
            sharedPrefrencesManger.setLastStatus(status);
        } else {
            String newStatus = sharedPrefrencesManger.getLastStatus();
            processStatus(newStatus);
        }
    }


    public void onSucessListener(BaseResponseModel responseModel) {
        try {

            if (responseModel != null && responseModel.getResultObj() != null) {
                if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.profileStatusServiceType)) {
                    profileStatusOutput = (NewProileStautsOutput) responseModel.getResultObj();
                    if (profileStatusOutput != null) {
                        if (profileStatusOutput.getErrorCode() != null)
                            onProfileStatusErrror(responseModel);
                        else {

                            if (profileStatusOutput.getUserProfile() == null) {
                                onProfileStatusSucess();// user didnot has authtoken.
                            }
                        }
                    } else {
//            Toast.makeText(getApplicationContext(), getString(R.string.generice_error), Toast.LENGTH_SHORT).show();
                        onProfileStatusErrror(null);
                    }
                } else if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.getShipmentStatus)) {
                    getShipmentStatusOutput = (GetShipmentStatusOutput) responseModel.getResultObj();
                    if (getShipmentStatusOutput != null && getShipmentStatusOutput.getErrorMsgEn() == null) {
                        onGetShipmentStatusSuccess();
                    } else {
                        onGetShipmentStatusFailure();
                    }
                }
            } else {
                getShipmentStatus();
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    private void onProfileStatusErrror(BaseResponseModel responseModel) {
        getShipmentStatus();
    }

    private void onProfileStatusSucess() {
        displayNextFragment();
    }

    private void getProfileStatus() {// 4

        appVersion = sharedPrefrencesManger.getAppVersion();
        token = sharedPrefrencesManger.getToken();
        if (token == null)
            token = "DEVICETOKEN";
        osVersion = ConstantUtils.osVersion;
        channel = ConstantUtils.channel;
        deviceId = sharedPrefrencesManger.getToken();
        profileStatusInput = new ProfileStatusInput();
        profileStatusInput.setAppVersion(appVersion);
        profileStatusInput.setToken(token);
        profileStatusInput.setOsVersion(osVersion);
        profileStatusInput.setDeviceId(deviceId);
//        profileStatusInput.setRegistrationId("A150573828212157");
        profileStatusInput.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        profileStatusInput.setChannel(channel);
        profileStatusInput.setAuthToken(sharedPrefrencesManger.getAuthToken());
        new ProfileStatusService(this, profileStatusInput);
    }

    private void displayNextFragment() {

        if (profileStatusOutput.getProfileStatus() != null) {

            switch (profileStatusOutput.getProfileStatus()) {
                case 0:
                case -1:

                    regActivity.addFragmnet(new StartScreenFragment(), R.id.frameLayout, false);

                    break;

                case 4:
                    getShipmentStatus();
                    break;
                case 7:
                case 5:
                    if (sharedPrefrencesManger != null)
                        sharedPrefrencesManger.clearSharedPref();
                    Fragment loginFrag = new LoginFragment();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(ConstantUtils.showBackBtn, false);
                    loginFrag.setArguments(bundle);
                    regActivity.addFragmnet(loginFrag, R.id.frameLayout, false);
                    break;

                default:
                    regActivity.addFragmnet(new StartScreenFragment(), R.id.frameLayout, false);
                    break;

            }
        } else {
            getShipmentStatus();
        }
//        }
    }
}