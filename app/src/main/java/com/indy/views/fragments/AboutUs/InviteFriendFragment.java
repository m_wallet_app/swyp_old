package com.indy.views.fragments.AboutUs;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.indy.R;
import com.indy.controls.ServiceUtils;
import com.indy.models.faqs.FaqsInputModel;
import com.indy.models.faqs.FaqsResponseModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.FaqsService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.HelpActivity;
import com.indy.views.activites.MasterActivity;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GamificationBaseInputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.SuccessOutputResponse;
import com.indy.views.fragments.gamification.services.RegisterInviteFriendEventService;

import org.jetbrains.annotations.Nullable;

/**
 * A simple {@link Fragment} subclass.
 */
public class InviteFriendFragment extends MasterActivity {

    //    View view;
    Button backImg, btn_help;
    TextView btn_invite;
    TextView tv_title;
    TextView et_code;
    TextInputLayout til_code;
    String urlToShare;
    Context mContext;
    public InviteFriendFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_invite_friend);
        initUI();
        setListeners();
        onConsumeService();
    }


    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            if (responseModel != null && responseModel.getResultObj() != null && responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.faqsService)) {
                {

                    FaqsResponseModel faqsResponseModel = (FaqsResponseModel) responseModel.getResultObj();
                    if (faqsResponseModel != null && faqsResponseModel.getSrcUrlEn() != null) {
                        if (currentLanguage.equals("en")) {
                            urlToShare = faqsResponseModel.getSrcUrlEn();
                        } else {
                            urlToShare = faqsResponseModel.getSrcUrlAr();
                        }
                    } else {
                        onShareLinkFailure();
                    }
                }
            }else if(responseModel!=null && responseModel.getResultObj()!=null && responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.REGISTER_EVENT_INVITE_FRIEND)){
                SuccessOutputResponse successOutputResponse = (SuccessOutputResponse) responseModel.getResultObj();
                if(successOutputResponse!=null && successOutputResponse.getSuccess()){

                }
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();

        FaqsInputModel faqsInputModel = new FaqsInputModel();
        faqsInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
        faqsInputModel.setLang(currentLanguage);
        faqsInputModel.setAppVersion(appVersion);
        faqsInputModel.setToken(token);
        faqsInputModel.setChannel(channel);
        faqsInputModel.setSignificant(ConstantUtils.invite_significant);
        faqsInputModel.setDeviceId(deviceId);
        faqsInputModel.setOsVersion(osVersion);
        faqsInputModel.setAuthToken(authToken);
        faqsInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        new FaqsService(this, faqsInputModel);
    }

    //    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        // Inflate the layout for this fragment
//        view = inflater.inflate(R.layout.fragment_invite_friend, container, false);
//        initUI();
//        setListeners();
//        return view;
//    }

    @Override
    public void initUI() {
        super.initUI();

        et_code = (TextView) findViewById(R.id.et_invite_friend_code);

        btn_invite = (TextView) findViewById(R.id.btn_invite);
//        til_code = (TextInputLayout) findViewById(R.id.invite_code_text_input_layout);
        backImg = (Button) findViewById(R.id.backImg);
        tv_title = (TextView) findViewById(R.id.titleTxt);
        tv_title.setText(getString(R.string.invite_spaced));
        btn_help = (Button) findViewById(R.id.helpBtn);
        btn_help.setVisibility(View.GONE);
        if (sharedPrefrencesManger != null)
            if (sharedPrefrencesManger.getInvitaion() != null)
                et_code.setText(sharedPrefrencesManger.getInvitaion());

        disableContinueButton();
    }

    public void setListeners() {
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
//                ChangePasswordActivity changePasswordActivity = (ChangePasswordActivity) getActivity();
//                changePasswordActivity.onBackPressed();
            }
        });

//        et_code.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (et_code.getText().toString().length() > 0) {
            enableContinueButton();
        } else {
            disableContinueButton();
        }
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//
//            }
//        });

        btn_invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_invite_friend_button);

                onShareLinkSuccess(urlToShare);
            }
        });

        btn_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(InviteFriendFragment.this, HelpActivity.class));
            }
        });


    }

    private void enableContinueButton() {
        btn_invite.setAlpha(1.0f);
        btn_invite.setEnabled(true);
    }

    private void disableContinueButton() {
        btn_invite.setAlpha(0.5f);
        btn_invite.setEnabled(false);
    }

    public void onShareLinkSuccess(String urlToShare) {
        disableContinueButton();
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.putExtra(Intent.EXTRA_SUBJECT, "");
        if(currentLanguage.equalsIgnoreCase("en")) {
            share.putExtra(Intent.EXTRA_TEXT, getString(R.string.invites_friend_str_1) + " " + urlToShare + " " + getString(R.string.and_use_my_code) + " " + et_code.getText().toString() + ". " + getString(R.string.invites_friend_str_2));
        }else{
            share.putExtra(Intent.EXTRA_TEXT, getString(R.string.invites_friend_str_1) + " " + urlToShare + " " + getString(R.string.and_use_my_code) + " " + et_code.getText().toString() + ". " + getString(R.string.invites_friend_str_2));

        }
        startActivityForResult(Intent.createChooser(share, getString(R.string.invite_friends)),1024);
        enableContinueButton();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1024){// && resultCode == RESULT_OK){
            if(CommonMethods.isMembershipValid(this)) {
                registerInviteFriendEvent();
            }
            CommonMethods.createAppRatingDialog(this,mFirebaseAnalytics);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (sharedPrefrencesManger.getNavigationIndex().length() > 0 || sharedPrefrencesManger.getNavigationBadgeIndex().length() >0) {
            finish();
        }
    }

    public void registerInviteFriendEvent(){
        GamificationBaseInputModel gamificationBaseInputModel = new GamificationBaseInputModel();
        gamificationBaseInputModel.setChannel(channel);
        gamificationBaseInputModel.setLang(currentLanguage);
        gamificationBaseInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        gamificationBaseInputModel.setAppVersion(appVersion);
        gamificationBaseInputModel.setAuthToken(authToken);
        gamificationBaseInputModel.setDeviceId(deviceId);
        gamificationBaseInputModel.setToken(token);
        gamificationBaseInputModel.setUserSessionId(sharedPrefrencesManger.getUserSessionId());
        gamificationBaseInputModel.setUserId(sharedPrefrencesManger.getUserId());
        gamificationBaseInputModel.setApplicationId(ConstantUtils.INDY_TALOS_ID);
        gamificationBaseInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
        gamificationBaseInputModel.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        gamificationBaseInputModel.setOsVersion(osVersion);
        new RegisterInviteFriendEventService(this, gamificationBaseInputModel);
    }

    public void onShareLinkFailure() {

    }



}


/*

public class InviteFriendFragment extends MasterActivity {

    //    View view;
    Button backImg, btn_help;
    TextView btn_invite;
    TextView tv_title;
    TextView et_code;
    TextInputLayout til_code;
    String urlToShare;

    public InviteFriendFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_invite_friend);
        initUI();
        setListeners();
        onConsumeService();
    }


    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            if (responseModel != null && responseModel.getResultObj() != null) {

                FaqsResponseModel faqsResponseModel = (FaqsResponseModel) responseModel.getResultObj();
                if (faqsResponseModel != null && faqsResponseModel.getSrcUrlEn() != null) {
                    if (currentLanguage.equals("en")) {
                        urlToShare = faqsResponseModel.getSrcUrlEn();
                    } else {
                        urlToShare = faqsResponseModel.getSrcUrlAr();
                    }
                } else {
                    onShareLinkFailure();
                }
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();

        FaqsInputModel faqsInputModel = new FaqsInputModel();
        faqsInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
        faqsInputModel.setLang(currentLanguage);
        faqsInputModel.setAppVersion(appVersion);
        faqsInputModel.setToken(token);
        faqsInputModel.setChannel(channel);
        faqsInputModel.setSignificant(ConstantUtils.invite_significant);
        faqsInputModel.setDeviceId(deviceId);
        faqsInputModel.setOsVersion(osVersion);
        faqsInputModel.setAuthToken(authToken);
        faqsInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        new FaqsService(this, faqsInputModel);
    }

    //    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        // Inflate the layout for this fragment
//        view = inflater.inflate(R.layout.fragment_invite_friend, container, false);
//        initUI();
//        setListeners();
//        return view;
//    }

    @Override
    public void initUI() {
        super.initUI();

        et_code = (TextView) findViewById(R.id.et_invite_friend_code);

        btn_invite = (TextView) findViewById(R.id.btn_invite);
//        til_code = (TextInputLayout) findViewById(R.id.invite_code_text_input_layout);
        backImg = (Button) findViewById(R.id.backImg);
        tv_title = (TextView) findViewById(R.id.titleTxt);
        tv_title.setText(getString(R.string.invite_spaced));
        btn_help = (Button) findViewById(R.id.helpBtn);
        btn_help.setVisibility(View.GONE);
        if (sharedPrefrencesManger != null)
            if (sharedPrefrencesManger.getInvitaion() != null)
                et_code.setText(sharedPrefrencesManger.getInvitaion());

        disableContinueButton();
    }

    public void setListeners() {
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
//                ChangePasswordActivity changePasswordActivity = (ChangePasswordActivity) getActivity();
//                changePasswordActivity.onBackPressed();
            }
        });

//        et_code.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (et_code.getText().toString().length() > 0) {
            enableContinueButton();
        } else {
            disableContinueButton();
        }
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//
//            }
//        });

        btn_invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_invite_friend_button);

                onShareLinkSuccess(urlToShare);
            }
        });

        btn_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(InviteFriendFragment.this, HelpActivity.class),120);
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 120){
//            AlertDialog alertDialog = CommonMethods.createAppRatingDialog("Rate us!","Please rate the application",this);
            CommonMethods.createAppRatingDialog(this);
//            alertDialog.show();
        }
    }

    private void enableContinueButton() {
        btn_invite.setAlpha(1.0f);
        btn_invite.setEnabled(true);
    }

    private void disableContinueButton() {
        btn_invite.setAlpha(0.5f);
        btn_invite.setEnabled(false);
    }

    public void onShareLinkSuccess(String urlToShare) {
        disableContinueButton();
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.putExtra(Intent.EXTRA_SUBJECT, "");
        share.putExtra(Intent.EXTRA_TEXT, getString(R.string.invites_friend_str_1) + " " + urlToShare + " " + getString(R.string.and_use_my_code) + " " + et_code.getText().toString() + ". " + getString(R.string.invites_friend_str_2));
        startActivityForResult(Intent.createChooser(share, getString(R.string.invite_friends)),120);
        if(currentLanguage.equalsIgnoreCase("en")) {
            share.putExtra(Intent.EXTRA_TEXT, getString(R.string.invites_friend_str_1) + " " + urlToShare + " " + getString(R.string.and_use_my_code) + " " + et_code.getText().toString() + ". " + getString(R.string.invites_friend_str_2));
        }else{
            share.putExtra(Intent.EXTRA_TEXT, getString(R.string.invites_friend_str_1) + " " + urlToShare + " " + getString(R.string.and_use_my_code) + " " + et_code.getText().toString() + ". " + getString(R.string.invites_friend_str_2));

        }
        startActivityForResult(Intent.createChooser(share, getString(R.string.invite_friends)),120);

        enableContinueButton();
    }


    public void onShareLinkFailure() {

    }
}




 */