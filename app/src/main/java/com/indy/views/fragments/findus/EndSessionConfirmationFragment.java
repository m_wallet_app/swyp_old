package com.indy.views.fragments.findus;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.models.unsubscribe.UnsubscribeHotspotOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.models.utils.MasterInputResponse;
import com.indy.services.UnsubscribeService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.MasterActivity;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;

/**
 * A simple {@link Fragment} subclass.
 */
public class EndSessionConfirmationFragment extends MasterActivity {

    View view;
    private MasterInputResponse masterInputResponse;
    private UnsubscribeHotspotOutput unsubscribeHotspotOutput;
    FrameLayout frameLayout;
    Button logout;
    TextView cancel;

    public EndSessionConfirmationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_end_session_confirmation);
        initUI();


    }

    @Override
    public void initUI() {
        super.initUI();

        logout = (Button) findViewById(R.id.btn_logout);
        cancel = (TextView) findViewById(R.id.btn_cancel);
        frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
        frameLayout.setVisibility(View.GONE);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_support_wifi_hotspots_confirm_logout_from_hotspots);

                onConsumeService();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(),ConstantUtils.TAGGING_usage_cancel_logout_from_hotspots);

            }
        });
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        frameLayout.setVisibility(View.VISIBLE);
        addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        masterInputResponse = new MasterInputResponse();
        masterInputResponse.setAppVersion(appVersion);
        masterInputResponse.setToken(token);
        masterInputResponse.setOsVersion(osVersion);
        masterInputResponse.setChannel(channel);
        masterInputResponse.setDeviceId(deviceId);
        masterInputResponse.setMsisdn(sharedPrefrencesManger.getMobileNo());
        masterInputResponse.setLang(currentLanguage);
        masterInputResponse.setImsi(sharedPrefrencesManger.getMobileNo());
        masterInputResponse.setAuthToken(authToken);
        new UnsubscribeService(this, masterInputResponse);
    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        frameLayout.setVisibility(View.GONE);
        super.onSucessListener(responseModel);
//        progressBar.setVisibility(View.GONE);
        try {
            onBackPressed();
            if(responseModel!=null && responseModel.getResultObj()!=null) {
                unsubscribeHotspotOutput = (UnsubscribeHotspotOutput) responseModel.getResultObj();
                if (unsubscribeHotspotOutput != null) {
                    if (unsubscribeHotspotOutput.getErrorCode() != null)
                        onError();
                    else
                        onContinue();
                }
            }
        }catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

    }

    public void onError() {

        if (currentLanguage.equals("en")) {
            showErrorFragment(unsubscribeHotspotOutput.getErrorMsgEn());
        } else {
            showErrorFragment(unsubscribeHotspotOutput.getErrorMsgAr());
        }

    }

    public void onContinue() {
        if (unsubscribeHotspotOutput != null) {
            if (unsubscribeHotspotOutput.getUnsubscribed() != null && unsubscribeHotspotOutput.getUnsubscribed()) {
                frameLayout.setVisibility(View.VISIBLE);
                addFragmnet(new SessionEndedFragment(),R.id.frameLayout,true);
            } else {
                replaceFragmnet(new SessionEndedFragment(), R.id.frameLayout, false);
                if (currentLanguage.equals("en")) {
                    showErrorFragment(unsubscribeHotspotOutput.getErrorMsgEn());
                } else {
                    showErrorFragment(unsubscribeHotspotOutput.getErrorMsgAr());
                }
            }
        } else {
            showErrorFragment(getString(R.string.generice_error));
        }
    }

    private void showErrorFragment(String error) {
        frameLayout.setVisibility(View.VISIBLE);
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, error);
        errorFragment.setArguments(bundle);
        replaceFragmnet(errorFragment, R.id.frameLayout, false);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        frameLayout.setVisibility(View.GONE);
        super.onErrorListener(responseModel);
        try {
            if(unsubscribeHotspotOutput!=null && unsubscribeHotspotOutput.getErrorMsgEn()!=null) {
                if (currentLanguage.equals("en")) {
                    showErrorFragment(unsubscribeHotspotOutput.getErrorMsgEn());
                } else {
                    showErrorFragment(unsubscribeHotspotOutput.getErrorMsgAr());
                }
            }else{
                showErrorFragment(getString(R.string.error_));
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }

}
