package com.indy.views.fragments.gamification.models.serviceInputOutput;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.views.fragments.gamification.models.EventsList;

import java.util.List;

/**
 * Created by mobile on 24/09/2017.
 */

public class GetEventsListOutputModel extends MasterErrorResponse {
    @SerializedName("indyEvents")
    @Expose
    private List<EventsList> indyEvents = null;
    @SerializedName("totalEventsCount")
    @Expose
    private int totalEventsCount;
    public List<EventsList> getIndyEvents() {
        return indyEvents;
    }

    public void setIndyEvents(List<EventsList> indyEvents) {
        this.indyEvents = indyEvents;
    }

    public int getTotalEventsCount() {
        return totalEventsCount;
    }

    public void setTotalEventsCount(int totalEventsCount) {
        this.totalEventsCount = totalEventsCount;
    }
}
