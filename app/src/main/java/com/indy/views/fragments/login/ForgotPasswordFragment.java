package com.indy.views.fragments.login;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.models.ForgotPassword.ForgotPasswordInputModel;
import com.indy.models.ForgotPassword.ForgotPasswordOutputModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.ForgotPasswordService;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.HelpActivity;
import com.indy.views.activites.UserProfileActivity;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.indy.views.fragments.utils.MasterFragment;

/**
 * Created by emad on 9/7/16.
 */
public class ForgotPasswordFragment extends MasterFragment {
    private View view;
    private Button continueBtn, backImg;

    private EditText emailAddress, mobileNo;
    private TextView help;
    private ForgotPasswordInputModel forgotPasswordInputModel;
    //    private ProgressBar progressBar;
    private ForgotPasswordOutputModel forgotPasswordOutputModel;
    private String vCtag = "VCTAG";
    private String vMobiletag = "VMOBILETAG";
    private String fragmentType;
    private TextInputLayout til_email, til_mobileNumber;
    private BroadcastReceiver mIntentReceiver;
    public String mmobileNo;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_forgot_password, container, false);
        initUI();
        setListeners();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
        emailAddress = (EditText) view.findViewById(R.id.emailAddress);
        mobileNo = (EditText) view.findViewById(R.id.mobileNo);
        continueBtn = (Button) view.findViewById(R.id.continueBtn);
        help = (TextView) view.findViewById(R.id.help);
//        progressBar = (ProgressBar) view.findViewById(R.id.progressID);
        fragmentType = getArguments().getString(ConstantUtils.fragmentType);
        til_email = (TextInputLayout) view.findViewById(R.id.email_address_text_input_layout);
        til_mobileNumber = (TextInputLayout) view.findViewById(R.id.cell_number_text_input_layout);
        if (fragmentType.equalsIgnoreCase(ConstantUtils.registerationActivity))
            continueBtn.setAlpha(0.5f);
        continueBtn.setEnabled(false);

//..............header Buttons............
        RelativeLayout backLayout = (RelativeLayout) view.findViewById(R.id.backLayout);
        RelativeLayout helpLayout = (RelativeLayout) view.findViewById(R.id.helpLayout);

        helpLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), HelpActivity.class);
                startActivity(intent);
            }
        });

        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard();
                getActivity().onBackPressed();
            }
        });
        //..............header Buttons............

    }

    private void setListeners() {
        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid())
                    onConsumeService();
                Bundle bundle2 = new Bundle();

                bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_forgot_password_enter_number);
                bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_forgot_password_enter_number);
                mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_forgot_password_enter_number, bundle2);
//                    onContinue("1234");
            }
        });
        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onHelp();
            }
        });


        mobileNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (mobileNo.getText().toString().trim().length() > 0) {
                    enableNextBtn();
                } else if (emailAddress.getText().toString().trim().length() > 0) {
                    enableNextBtn();
                } else {
                    disableNextBtn();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        emailAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (emailAddress.getText().toString().trim().length() > 0) {
                    enableNextBtn();
                } else if (mobileNo.getText().toString().trim().length() > 0) {
                    enableNextBtn();
                } else {
                    disableNextBtn();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void disableNextBtn() {
        continueBtn.setAlpha(0.5f);
        continueBtn.setEnabled(false);
    }

    private void enableNextBtn() {
        continueBtn.setAlpha(1.0f);
        continueBtn.setEnabled(true);
    }

    private void onContinue(String vcTag) {
        mmobileNo = mobileNo.getText().toString().trim();
        ForgotPasswordCodeFragment forgotPasswordCodeFragment = new ForgotPasswordCodeFragment();
        Bundle bundle = new Bundle();
        bundle.putString(vCtag, vcTag);
        bundle.putString(vMobiletag, mobileNo.getText().toString().trim());
        forgotPasswordCodeFragment.setArguments(bundle);
        if (fragmentType.equalsIgnoreCase(ConstantUtils.registerationActivity))
            regActivity.replaceFragmnet(forgotPasswordCodeFragment, R.id.frameLayout, true);
        else
            ((UserProfileActivity) getActivity()).replaceFragmnet(forgotPasswordCodeFragment, R.id.frameLayout, true);

    }


    private void onHelp() {
        Intent intent = new Intent(getActivity(), HelpActivity.class);
        startActivity(intent);
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        regActivity.addFragmnet(new LoadingFragmnet(), R.id.loadingFragmeLayout, true);
        forgotPasswordInputModel = new ForgotPasswordInputModel();
        forgotPasswordInputModel.setAppVersion(appVersion);
        forgotPasswordInputModel.setToken(token);
        forgotPasswordInputModel.setOsVersion(osVersion);
        forgotPasswordInputModel.setChannel(channel);
        forgotPasswordInputModel.setDeviceId(deviceId);
        forgotPasswordInputModel.setLang(currentLanguage);

        forgotPasswordInputModel.setMsisdn(mobileNo.getText().toString().trim());
        forgotPasswordInputModel.setContact(mobileNo.getText().toString().trim());
        forgotPasswordInputModel.setLang(currentLanguage);
        new ForgotPasswordService(this, forgotPasswordInputModel);
    }

    //7625
    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
//        progressBar.setVisibility(View.GONE);
        try {
            if (isVisible()) {

                regActivity.onBackPressed();
                if(responseModel!=null && responseModel.getResultObj()!=null) {
                    forgotPasswordOutputModel = (ForgotPasswordOutputModel) responseModel.getResultObj();
                    if (forgotPasswordOutputModel != null) {
                        if (forgotPasswordOutputModel.getErrorCode() != null)
                            onError();
                        else
                            onContinue(forgotPasswordOutputModel.getGeneratedCode());
                    }
                }
            }
        }catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

    }

    private void onError() {
        Fragment errorFragmnet = new ErrorFragment();
        Bundle bundle = new Bundle();
        if (currentLanguage != null) {
            if (currentLanguage.equalsIgnoreCase("en"))
                bundle.putString(ConstantUtils.errorString, forgotPasswordOutputModel.getErrorMsgEn());
            else
                bundle.putString(ConstantUtils.errorString, forgotPasswordOutputModel.getErrorMsgAr());
        }
        errorFragmnet.setArguments(bundle);
        if (fragmentType.equalsIgnoreCase(ConstantUtils.registerationActivity))
            regActivity.replaceFragmnet(errorFragmnet, R.id.frameLayout, true);
        else
            ((UserProfileActivity) getActivity()).replaceFragmnet(errorFragmnet, R.id.frameLayout, true);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        try {
            regActivity.onBackPressed();
        }catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    private boolean isValid() {
        boolean isValid = true;
        if (mobileNo.getText().toString().length() < 10) {
            isValid = false;
            til_mobileNumber.setError(getString(R.string.invalid_delivery_number));
        } else {
            String noStr = mobileNo.getText().toString().substring(0, 2);
            if (!noStr.equalsIgnoreCase("05")) {
                isValid = false;
                til_mobileNumber.setError(getString(R.string.invalid_delivery_number));
            } else {
                removeTextInputLayoutError(til_mobileNumber);
            }
        }
        return isValid;
    }

    private void showErrorFragment() {
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, "Passwords do not match. Please try again.");
        errorFragment.setArguments(bundle);
        regActivity.replaceFragmnet(errorFragment, R.id.frameLayout, true);
    }
}
