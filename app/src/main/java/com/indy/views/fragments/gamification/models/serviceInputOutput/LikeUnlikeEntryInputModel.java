package com.indy.views.fragments.gamification.models.serviceInputOutput;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mobile on 24/09/2017.
 */

public class LikeUnlikeEntryInputModel extends GamificationBaseInputModel {

    @SerializedName("indyEventId")
    @Expose
    private String indyEventId;

    @SerializedName("like")
    @Expose
    private boolean like;

    @SerializedName("indyEventEntryId")
    @Expose
    private String indyEventEntryId;


    public String getIndyEventId() {
        return indyEventId;
    }

    public void setIndyEventId(String indyEventId) {
        this.indyEventId = indyEventId;
    }

    public boolean isLike() {
        return like;
    }

    public void setLike(boolean like) {
        this.like = like;
    }

    public String getIndyEventEntryId() {
        return indyEventEntryId;
    }

    public void setIndyEventEntryId(String indyEventEntryId) {
        this.indyEventEntryId = indyEventEntryId;
    }
}
