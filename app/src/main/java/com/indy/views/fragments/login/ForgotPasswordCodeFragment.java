package com.indy.views.fragments.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.customviews.CustomButton;
import com.indy.customviews.CustomEditText;
import com.indy.customviews.CustomTextView;
import com.indy.models.ForgotPassword.ForgotPasswordInputModel;
import com.indy.models.ForgotPassword.ForgotPasswordOutputModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.validateotp.ValidateOtpInput;
import com.indy.models.validateotp.ValidateOtpOutput;
import com.indy.services.ForgotPasswordService;
import com.indy.services.ValidateOtpService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.HelpActivity;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.indy.views.fragments.utils.MasterFragment;

import java.util.ArrayList;

/**
 * Created by emad on 9/7/16.
 */
public class ForgotPasswordCodeFragment extends MasterFragment {
    private View view;
    private CustomTextView resendBtn;
    private CustomButton continueButton;
    private CustomEditText code1, code2, code3, code4;
    private TextView help;
    private String vCtag = "VCTAG";
    private String vMobiletag = "VMOBILETAG";
    private String codeGenerated;
    private Button backImg;
    private CountDownTimer countDownTimer;
    int[] tvListIds = {R.id.code_1, R.id.code_2, R.id.code_3, R.id.code_4};
    ArrayList<EditText> tv_digits;
    private ForgotPasswordInputModel forgotPasswordInputModel;
    private ForgotPasswordOutputModel forgotPasswordOutputModel;
    private ValidateOtpOutput validateOtpOutputModel;
    private ProgressBar progressID;
    public static String mobileNo;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_forgot_password_code, container, false);
        initUI();
        startTimer();
        setListeners();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
        code1 = (CustomEditText) view.findViewById(R.id.code_1);
        code2 = (CustomEditText) view.findViewById(R.id.code_2);
        code3 = (CustomEditText) view.findViewById(R.id.code_3);
        code4 = (CustomEditText) view.findViewById(R.id.code_4);
        progressID = (ProgressBar) view.findViewById(R.id.progressID);
        resendBtn = (CustomTextView) view.findViewById(R.id.resendBtn);
        help = (TextView) view.findViewById(R.id.help);
        continueButton = (CustomButton) view.findViewById(R.id.continueBtn);
        showKeyBoard();
        if (getArguments() != null && !getArguments().getString(vMobiletag, "").isEmpty()) {
//            codeGenerated = getArguments().getString(vCtag);
            mobileNo = getArguments().getString(vMobiletag);
        }

        tv_digits = new ArrayList<EditText>();
        for (int i = 0; i < tvListIds.length; i++) {
            EditText et_temp = (EditText) view.findViewById(tvListIds[i]);
            et_temp.addTextChangedListener(new CustomTextWatcher(et_temp));
            tv_digits.add(et_temp);
        }

        backImg = (Button) view.findViewById(R.id.backImg);
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regActivity.onBackPressed();
            }
        });

        TextView headerTxt = (TextView) view.findViewById(R.id.titleTxt);
        headerTxt.setText("");
        disableContinueBtn();
        disableResebdCodeBtn();

    }


    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();

    }

    private void startTimer() {
//        if (countDownTimer == null) {
        countDownTimer = new CountDownTimer(61000, 1000) {

            public void onTick(long millisUntilFinished) {
                if (isVisible()) {
                    int minutes = (int) millisUntilFinished / (60 * 1000);
                    int seconds = (int) (millisUntilFinished / 1000) % 60;
                    String str = String.format("%d:%02d", minutes, seconds);
                    resendBtn.setText(getResources().getString(R.string.recent_code) + " " + str);
                    continueButton.setEnabled(true);
                } else {
                    cancel();
                }
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                if (isVisible()) {
                    resendBtn.setText(getString(R.string.resend_code_txt));
                    enableResendCodeBtn();
                    resendBtn.setEnabled(true);
                    continueButton.setEnabled(false);
                    cancel();
                }
            }

        }.start();
//        }
        resendBtn.setEnabled(false);
    }

    private void setListeners() {
        //..............header Buttons............
        RelativeLayout backLayout = (RelativeLayout) view.findViewById(R.id.backLayout);
        RelativeLayout helpLayout = (RelativeLayout) view.findViewById(R.id.helpLayout);

        helpLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), HelpActivity.class);
                startActivity(intent);
            }
        });

        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard();
                getActivity().onBackPressed();
            }
        });
        //..............header Buttons............

        resendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //// TODO: 10/3/2016 : resend service to be called here
                startTimer();
                onConsumeService();
            }
        });
        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onHelp();
            }
        });
    }

    private String getCode() {
        String codeEntered = "";
        codeEntered += code1.getText().toString();
        codeEntered += code2.getText().toString();
        codeEntered += code3.getText().toString();
        codeEntered += code4.getText().toString();
        return codeEntered;
    }

    private void onContinue() {
        try {
            if (countDownTimer != null)
                countDownTimer.onFinish();
            countDownTimer = null;

            CommonMethods.logFirebaseEvent(mFirebaseAnalytics,ConstantUtils.TAGGING_forgot_password_validation_code_entered);

            regActivity.addFragmnet(new LoadingFragmnet(), R.id.loadingFragmeLayout, true);
            ValidateOtpInput validateOtpInput = new ValidateOtpInput();
            validateOtpInput.setAuthToken(authToken);
            validateOtpInput.setAppVersion(appVersion);
            validateOtpInput.setChannel(channel);
            validateOtpInput.setDeviceId(deviceId);
            validateOtpInput.setImsi(sharedPrefrencesManger.getMobileNo());
            validateOtpInput.setLang(currentLanguage);
            validateOtpInput.setOtpCode(getCode());
            validateOtpInput.setOsVersion(osVersion);
            validateOtpInput.setToken(token);
            validateOtpInput.setMsisdn(mobileNo);
            new ValidateOtpService(this, validateOtpInput);
        }catch (Exception ex){
            CommonMethods.logException(ex);
        }
    }

    private void onHelp() {
        Intent intent = new Intent(getActivity(), HelpActivity.class);
        startActivity(intent);
    }

    public void showKeyBoard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, 0);
        }
    }


    public class CustomTextWatcher implements TextWatcher {
        private EditText view;

        private CustomTextWatcher(EditText view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

            String text = editable.toString();
            if (getCode().length() == 4)
                enableContinueBtn();
            else
                disableContinueBtn();
            if (text.length() == 1) {
                view.clearFocus();
                int currentIndex = tv_digits.indexOf(view);
                if (currentIndex == 3) {
                    hideKeyBoard();
                } else {
                    for (int i = currentIndex + 1; i < tv_digits.size(); i++) {
                        tv_digits.get(i).requestFocus();
                        return;
                    }
                }

            }

        }
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        regActivity.addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        forgotPasswordInputModel = new ForgotPasswordInputModel();
        forgotPasswordInputModel.setAppVersion(appVersion);
        forgotPasswordInputModel.setToken(token);
        forgotPasswordInputModel.setOsVersion(osVersion);
        forgotPasswordInputModel.setChannel(channel);
        forgotPasswordInputModel.setDeviceId(deviceId);
        forgotPasswordInputModel.setMsisdn(mobileNo);
        forgotPasswordInputModel.setLang(currentLanguage);

        forgotPasswordInputModel.setContact(mobileNo);
        forgotPasswordInputModel.setLang(currentLanguage);
        new ForgotPasswordService(this, forgotPasswordInputModel);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            regActivity.onBackPressed();
            if (isVisible()) {
                if(responseModel!=null && responseModel.getResultObj()!=null) {
                    if (responseModel.getServiceType().equalsIgnoreCase(ConstantUtils.validateOtp)) {
                        this.validateOtpOutputModel = (ValidateOtpOutput) responseModel.getResultObj();
                        if (validateOtpOutputModel.getErrorCode() == null && validateOtpOutputModel.getIsValid() != null && validateOtpOutputModel.getIsValid())
                            onValidateOtpSucess();
                        else
                            onvalidateOtpError();
                    } else {
                        forgotPasswordOutputModel = (ForgotPasswordOutputModel) responseModel.getResultObj();
                        if (forgotPasswordOutputModel != null)
                            if (forgotPasswordOutputModel.getErrorCode() != null)
                                onResendCodeError();
                            else
                                onResendCodeSucess();
                    }
                }
            }
        }catch (Exception ex) {
            CommonMethods.logException(ex);
        }
    }

    private void onValidateOtpSucess() {

        Bundle bundle2 = new Bundle();

        bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_forgot_password_validation_code_valid);
        bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_forgot_password_validation_code_valid);
        mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_forgot_password_validation_code_valid, bundle2);

        Fragment fragment = new SetNewPasswordFragment();
        Bundle bundle = new Bundle();
        bundle.putString(vCtag, vMobiletag);
        fragment.setArguments(bundle);
        regActivity.replaceFragmnet(fragment, R.id.frameLayout, true);
        regActivity.replaceFragmnet(fragment, R.id.frameLayout, true);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {

        try {
            regActivity.onBackPressed();
        }catch (Exception ex) {
            CommonMethods.logException(ex);

        }
    }

    private void onResendCodeError() {
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        if (currentLanguage != null) {
            if (currentLanguage.equalsIgnoreCase("en"))
                bundle.putString(ConstantUtils.errorString, forgotPasswordOutputModel.getErrorMsgEn());
            else
                bundle.putString(ConstantUtils.errorString, forgotPasswordOutputModel.getErrorMsgAr());
        }
        errorFragment.setArguments(bundle);
        regActivity.replaceFragmnet(errorFragment, R.id.frameLayout, true);
    }

    private void onvalidateOtpError() {
        Bundle bundle2 = new Bundle();

        bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_forgot_password_validation_code_invalid);
        bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_forgot_password_validation_code_invalid);
        mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_forgot_password_validation_code_invalid, bundle2);

        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        if (currentLanguage != null) {
            if (currentLanguage.equalsIgnoreCase("en"))
                bundle.putString(ConstantUtils.errorString, validateOtpOutputModel.getErrorMsgEn());
            else
                bundle.putString(ConstantUtils.errorString, validateOtpOutputModel.getErrorMsgAr());
        }
        errorFragment.setArguments(bundle);
        regActivity.replaceFragmnet(errorFragment, R.id.frameLayout, true);
    }

    private void onResendCodeSucess() {
        startTimer();
    }

    private void disableContinueBtn() {
        continueButton.setAlpha(0.5f);
        continueButton.setEnabled(false);
        continueButton.setOnClickListener(null);

    }

    private void disableResebdCodeBtn() {
        resendBtn.setAlpha(0.20f);
        resendBtn.setEnabled(false);

    }

    private void enableResendCodeBtn() {
        resendBtn.setAlpha(1.0f);
        resendBtn.setEnabled(true);

    }

    private void enableContinueBtn() {
        continueButton.setAlpha(1.0f);
        continueButton.setEnabled(true);
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onContinue();
            }
        });
    }

}
