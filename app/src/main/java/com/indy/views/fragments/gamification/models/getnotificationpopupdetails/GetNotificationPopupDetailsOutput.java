package com.indy.views.fragments.gamification.models.getnotificationpopupdetails;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by mobile on 10/10/2017.
 */

public class GetNotificationPopupDetailsOutput extends MasterErrorResponse {

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("rewardText")
    @Expose
    private String rewardText;

    @SerializedName("reward")
    @Expose
    private String reward;

    @SerializedName("subTitle1")
    @Expose
    private String subTitle1;
    @SerializedName("subTitle2")
    @Expose
    private String subTitle2;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("rewardAmount")
    @Expose
    private String rewardAmount;
    @SerializedName("rewardStatement")
    @Expose
    private String rewardStatement;
    @SerializedName("popupType")
    @Expose
    private Integer popupType;

    @SerializedName("totalRaffleTickets")
    @Expose
    private String totalRaffleTickets;

    @SerializedName("text")
    @Expose
    private String text;

    @SerializedName("imageToShareUrl")
    @Expose
    private String imageToShareUrl;

    String id;

    public final static Parcelable.Creator<GetNotificationPopupDetailsOutput> CREATOR = new Parcelable.Creator<GetNotificationPopupDetailsOutput>() {


        @SuppressWarnings({
                "unchecked"
        })
        public GetNotificationPopupDetailsOutput createFromParcel(Parcel in) {
            return new GetNotificationPopupDetailsOutput(in);
        }

        public GetNotificationPopupDetailsOutput[] newArray(int size) {
            return (new GetNotificationPopupDetailsOutput[size]);
        }

    };

    protected GetNotificationPopupDetailsOutput(Parcel in) {
        this.title = ((String) in.readValue((String.class.getClassLoader())));
        this.subTitle1 = ((String) in.readValue((String.class.getClassLoader())));
        this.subTitle2 = ((String) in.readValue((String.class.getClassLoader())));
        this.imageUrl = ((String) in.readValue((String.class.getClassLoader())));
        this.rewardAmount = ((String) in.readValue((String.class.getClassLoader())));
        this.rewardStatement = ((String) in.readValue((String.class.getClassLoader())));

        this.popupType = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.reward= ((String) in.readValue((String.class.getClassLoader())));
        this.rewardText = ((String) in.readValue((String.class.getClassLoader())));
        this.totalRaffleTickets = ((String) in.readValue((String.class.getClassLoader())));
        this.text = (String) in.readValue((String.class.getClassLoader()));
        this.imageToShareUrl = ((String) in.readValue(String.class.getClassLoader()));

    }

    public GetNotificationPopupDetailsOutput() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle2() {
        return subTitle2;
    }

    public void setSubTitle2(String subTitle2) {
        this.subTitle2 = subTitle2;
    }

    public String getSubTitle1() {
        return subTitle1;
    }

    public void setSubTitle1(String subTitle1) {
        this.subTitle1 = subTitle1;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getRewardAmount() {
        return rewardAmount;
    }

    public void setRewardAmount(String rewardAmount) {
        this.rewardAmount = rewardAmount;
    }

    public String getRewardStatement() {
        return rewardStatement;
    }

    public void setRewardStatement(String rewardStatement) {
        this.rewardStatement = rewardStatement;
    }

    public Integer getPopupType() {
        return popupType;
    }

    public void setPopupType(Integer popupType) {
        this.popupType = popupType;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(title);
        dest.writeValue(subTitle1);
        dest.writeValue(subTitle2);
        dest.writeValue(imageUrl);
        dest.writeValue(rewardAmount);
        dest.writeValue(rewardStatement);
        dest.writeValue(popupType);
        dest.writeString(reward);
        dest.writeString(rewardText);
        dest.writeString(totalRaffleTickets);
        dest.writeString(text);
        dest.writeString(imageToShareUrl);
    }

    public int describeContents() {
        return 0;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRewardText() {
        return rewardText;
    }

    public void setRewardText(String rewardText) {
        this.rewardText = rewardText;
    }

    public String getReward() {
        return reward;
    }

    public void setReward(String reward) {
        this.reward = reward;
    }

    public String getTotalRaffleTickets() {
        return totalRaffleTickets;
    }

    public void setTotalRaffleTickets(String totalRaffleTickets) {
        this.totalRaffleTickets = totalRaffleTickets;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImageToShareUrl() {
        return imageToShareUrl;
    }

    public void setImageToShareUrl(String imageToShareUrl) {
        this.imageToShareUrl = imageToShareUrl;
    }
}
