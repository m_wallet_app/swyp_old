package com.indy.views.fragments.gamification.challenges;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.indy.R;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.SwpeMainActivity;
import com.indy.views.fragments.gamification.OnRaffleListClick;
import com.indy.views.fragments.gamification.adapters.RaffleMonthsAdapter;
import com.indy.views.fragments.gamification.models.getraffleslist.GetRafflesListInput;
import com.indy.views.fragments.gamification.models.getraffleslist.GetRafflesListOutput;
import com.indy.views.fragments.gamification.models.getraffleslist.Raffle;
import com.indy.views.fragments.gamification.services.GetRafflesListService;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.MasterFragment;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class RafflesFragment extends MasterFragment implements OnRaffleListClick {

    View rootView;
    RecyclerView rv_raffles;
    TextView tv_earnMore;
    List<Raffle> raffles;
    RaffleMonthsAdapter raffleMonthsAdapter;
    private ProgressBar progressBar;
    private GetRafflesListOutput getRafflesListOutput;

    public RafflesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_raffles, container, false);
        initUI();
        return rootView;
    }

    @Override
    public void initUI() {
        super.initUI();

        rv_raffles = (RecyclerView) rootView.findViewById(R.id.rv_raffles);
        tv_earnMore = (TextView) rootView.findViewById(R.id.btn_saveProfile);

        rv_raffles.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_raffles.setHasFixedSize(true);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressID);
        onConsumeService();
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        progressBar.setVisibility(View.VISIBLE);
        GetRafflesListInput getRafflesListInput = new GetRafflesListInput();
        getRafflesListInput.setChannel(channel);
        getRafflesListInput.setLang(currentLanguage);
        getRafflesListInput.setMsisdn(sharedPrefrencesManger.getMobileNo());
        getRafflesListInput.setAppVersion(appVersion);
        getRafflesListInput.setAuthToken(authToken);
        getRafflesListInput.setDeviceId(deviceId);
        getRafflesListInput.setToken(token);
        getRafflesListInput.setUserSessionId(sharedPrefrencesManger.getUserSessionId());
        getRafflesListInput.setUserId(sharedPrefrencesManger.getUserId());
        getRafflesListInput.setApplicationId(ConstantUtils.INDY_TALOS_ID);
        getRafflesListInput.setImsi(sharedPrefrencesManger.getMobileNo());
        getRafflesListInput.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        getRafflesListInput.setOsVersion(osVersion);
        new GetRafflesListService(this, getRafflesListInput);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            progressBar.setVisibility(View.GONE);
            switch (responseModel.getServiceType()) {
                case ServiceUtils.GET_RAFFLES_LIST: {
                    if (responseModel != null) {
                        getRafflesListOutput = (GetRafflesListOutput) responseModel.getResultObj();
                        if (getRafflesListOutput != null)
                            if (getRafflesListOutput.getRaffles() != null
                                    && !getRafflesListOutput.getRaffles().isEmpty()) {
                                getRafflesListSuccess();
                            } else {
                                getRafflesListError();
                            }
                    }
                    break;
                }
            }
        }catch (Exception ex){
//            CommonMethods.logException(ex);
        }
    }

    private void getRafflesListSuccess() {
        raffleMonthsAdapter = new RaffleMonthsAdapter(getRafflesListOutput.getRaffles(), getActivity(), this);
        rv_raffles.setAdapter(raffleMonthsAdapter);
    }

    private void getRafflesListError() {
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)
                ? getRafflesListOutput.getErrorMsgEn() : getRafflesListOutput.getErrorMsgAr());

        errorFragment.setArguments(bundle);
        ((SwpeMainActivity) getActivity()).replaceFragmnet(errorFragment, R.id.frameLayout, true);
    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onRaffleClicked(Raffle raffle) {
        Intent intent = new Intent(getActivity(), RafflesDetailActivity.class);
        intent.putExtra(ConstantUtils.KEY_RAFFLE_MONTH, raffle);
        startActivity(intent);
    }
}
