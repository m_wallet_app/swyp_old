package com.indy.views.fragments.gamification.models.getrafflesdetails;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

import java.util.List;

/**
 * Created by mobile on 12/10/2017.
 */

public class GetRafflesDetailsOutputResponse extends MasterErrorResponse {

    @SerializedName("raffleName")
    @Expose
    private String raffleName;
    @SerializedName("prizes")
    @Expose
    private List<Prize> prizes = null;
    @SerializedName("userWon")
    @Expose
    private boolean userWon ;

    public final static Parcelable.Creator<GetRafflesDetailsOutputResponse> CREATOR = new Parcelable.Creator<GetRafflesDetailsOutputResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public GetRafflesDetailsOutputResponse createFromParcel(Parcel in) {
            return new GetRafflesDetailsOutputResponse(in);
        }

        public GetRafflesDetailsOutputResponse[] newArray(int size) {
            return (new GetRafflesDetailsOutputResponse[size]);
        }

    }
            ;

    protected GetRafflesDetailsOutputResponse(Parcel in) {
        this.raffleName = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.prizes, (Prize.class.getClassLoader()));
    }

    public GetRafflesDetailsOutputResponse() {
    }

    public String getRaffleName() {
        return raffleName;
    }

    public void setRaffleName(String raffleName) {
        this.raffleName = raffleName;
    }

    public List<Prize> getPrizes() {
        return prizes;
    }

    public void setPrizes(List<Prize> prizes) {
        this.prizes = prizes;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(raffleName);
        dest.writeList(prizes);
    }

    public int describeContents() {
        return 0;
    }

    public boolean isUserWon() {
        return userWon;
    }

    public void setUserWon(boolean userWon) {
        this.userWon = userWon;
    }
}