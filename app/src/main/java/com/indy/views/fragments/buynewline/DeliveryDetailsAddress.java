package com.indy.views.fragments.buynewline;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.controls.AdjustEvents;
import com.indy.controls.ServiceUtils;
import com.indy.helpers.GoogleMapsHelper;
import com.indy.helpers.LocationServiceHelper;
import com.indy.models.emiratesIdVerification.VerifyEmiratesIdInput;
import com.indy.models.emiratesIdVerification.VerifyEmiratesIdOutput;
import com.indy.models.finalizeregisteration.DeliveryInfo;
import com.indy.models.locations.LocationModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.ocr.screens.EmailAddressFragment;
import com.indy.ocr.screens.EmiratesIdDetailsFragment;
import com.indy.services.LogApplicationFlowService;
import com.indy.services.VerifyEmiratesIdForNewOrderService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.utils.GPSTracker;
import com.indy.views.fragments.buynewline.exisitingnumber.EmiratesIDVerificationFragment;
import com.indy.views.fragments.buynewline.exisitingnumber.PromoCodeVerificationSuccessfulFragment;
import com.indy.views.fragments.buynewline.exisitingnumber.RegisteredNonEmirateIDDocumentFragment;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.indy.views.fragments.utils.MasterFragment;

import java.util.List;

import static com.indy.R.id.frameLayout;
import static com.indy.utils.ConstantUtils.TAGGING_map;
import static com.indy.views.activites.RegisterationActivity.logEventInputModel;

/**
 * Created by emad on 9/21/16.
 */

public class DeliveryDetailsAddress extends MasterFragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleMap.OnInfoWindowClickListener {
    private View view = null;
    private Button nextBtn, okBtn, cancelBtn;
    public GoogleMap mMap;
    public MarkerOptions marker;
    EditText addresstxt, et_deliveryNotes;
    public TextView tv_errorTxt, tv_errorTitle;
    public static String addressLine2 = "";
    public static String deliveryNotes = "";
    TextInputLayout mobileNoInputLayout;
    private CharSequence addressTxt;
    LinearLayout ll_error;
    ImageView iv_search;
    private static String emiratesID;
    public static DeliveryDetailsAddress deliveryDetailsAddress;
    public static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    public static final int LOCATION_REQUEST = 1340;
    LocationManager locationManager;
    GPSTracker gpsTracker;
    public String locationName = null;
    private Double mLatitude, mLongitude;
    Location mlocation;
    private VerifyEmiratesIdOutput verifyEmiratesIdOutput;
    GoogleMapsHelper googleMapsHelper;

    // private Button backImg;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_deleivery_details_adddress, container, false);
            initUI();
            disableNextBtn();
            setListenesrs();
        }
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
        regActivity.showHeaderLayout();
        regActivity.setHeaderTitle(getString(R.string.enter_delivery_address_spaced));
        ll_error = (LinearLayout) view.findViewById(R.id.ll_error);
        ll_error.setVisibility(View.GONE);
        et_deliveryNotes = (EditText) view.findViewById(R.id.tv_additionalText);
        nextBtn = (Button) view.findViewById(R.id.nextBtn);
        addresstxt = (EditText) view.findViewById(R.id.addressID);

        mobileNoInputLayout = (TextInputLayout) view.findViewById(R.id.mobileNoInputLayout);
        SupportMapFragment mapFrag = SupportMapFragment.newInstance();
        getChildFragmentManager().beginTransaction().replace(R.id.map, mapFrag).commit();
        mapFrag.getMapAsync(this);

        addresstxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
                addressTxt = s;
                if (addressTxt != null) {
                    if (s.length() == 0)
                        disableNextBtn();
                    else
                        enableNextBtn();
                } else {
                    disableNextBtn();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
            }
        });
        ImageView iv_locateMe = (ImageView) view.findViewById(R.id.iv_locateMe);
        iv_locateMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mMap != null && mlocation != null) {
                    LatLng latLng = new LatLng(mlocation.getLatitude(), mlocation.getLongitude());
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, ConstantUtils.zoomLevel));
                }
            }
        });
        deliveryDetailsAddress = this;
        iv_search = (ImageView) view.findViewById(R.id.iv_search);
        iv_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyBoard();
                Geocoder gc = new Geocoder(getActivity());
                Bundle bundle2 = new Bundle();

                bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_delivery_info_enter_address);
                bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_delivery_info_enter_address);
//                mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_delivery_info_enter_address, bundle2);
                CommonMethods.logFirebaseEvent(mFirebaseAnalytics, ConstantUtils.TAGGING_delivery_info_enter_address, bundle2);
                if (Geocoder.isPresent()) {
                    List<Address> list = null;
                    try {
                        list = gc.getFromLocationName(addresstxt.getText().toString() + ", UAE", 1);
                        Address address = list.get(0);
                        double lat = address.getLatitude();
                        double lng = address.getLongitude();
                        String addressToDisplay = "";
                        for (int i = 0; i < address.getMaxAddressLineIndex() + 1; i++) {
                            addressToDisplay += address.getAddressLine(i) + " ";
                        }
                        if (mMap != null) {
                            mMap.clear();
                            LatLng fLatLng = new LatLng(lat, lng);

                            marker = new MarkerOptions().position(fLatLng);
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(fLatLng,
                                    13));
                            mMap.addMarker(
                                    marker.position(fLatLng)
                                            .title(addressToDisplay)
                                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.location_icon_fill)));

                            addresstxt.setText(addressToDisplay);
                            mMap.setOnMarkerClickListener(DeliveryDetailsAddress.this);
                            mMap.setOnInfoWindowClickListener(DeliveryDetailsAddress.this);
                            addMarkerAtCurrentPosition(mlocation, false);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), getString(R.string.sorry_address_invalid), Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

        tv_errorTitle = (TextView) view.findViewById(R.id.titleTxtView);
        tv_errorTxt = (TextView) view.findViewById(R.id.errorTxt);
        okBtn = (Button) view.findViewById(R.id.okBtn);
        cancelBtn = (Button) view.findViewById(R.id.cancelBtn);

        tv_errorTitle.setText("");
        tv_errorTxt.setText(getString(R.string.enable_gps));
        okBtn.setText(getString(R.string.settings));
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, LOCATION_REQUEST);
            }
        });
        cancelBtn.setVisibility(View.INVISIBLE);
    }

//    @Override
//    public void onSucessListener(BaseResponseModel responseModel) {
//        super.onSucessListener(responseModel);
//        if (isVisible()) {
//            StringBuilder cityAddress = (StringBuilder) responseModel.getResultObj();
//            if (cityAddress != null)
//                addresstxt.setText(cityAddress.toString());
//
//        }
//    }


    @Override
    public void onConsumeService() {
        super.onConsumeService();
        regActivity.addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        VerifyEmiratesIdInput verifyEmiratesIdInput;
        verifyEmiratesIdInput = new VerifyEmiratesIdInput();
        verifyEmiratesIdInput.setLang(currentLanguage);
        verifyEmiratesIdInput.setAppVersion(appVersion);
        verifyEmiratesIdInput.setOsVersion(osVersion);
        verifyEmiratesIdInput.setToken(token);
        verifyEmiratesIdInput.setChannel(channel);
        verifyEmiratesIdInput.setDeviceId(token);
        verifyEmiratesIdInput.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        verifyEmiratesIdInput.setMsisdn(NewNumberFragment.msisdnNumber);
        verifyEmiratesIdInput.setAmount(String.valueOf(OrderFragment.totalAmount));
        verifyEmiratesIdInput.setShippingMethod(OrderFragment.shippingMethodStr);
        verifyEmiratesIdInput.setOrderType("registrationOrder");
        verifyEmiratesIdInput.setEmiratesId(EmiratesIDVerificationFragment.emiratesID);
        verifyEmiratesIdInput.setPackageType(OrderFragment.orderTypeStr);
        if (PromoCodeVerificationSuccessfulFragment.promoCodeType == 1) {
            verifyEmiratesIdInput.setPackageType("4");

        } else if (PromoCodeVerificationSuccessfulFragment.promoCodeType == 2) {
            verifyEmiratesIdInput.setPackageType("3");
        }


        DeliveryInfo deliveryInfo = new DeliveryInfo();
        deliveryInfo.setEmirate(DeliveryDetailsAreaFragment.emirateStr);
        deliveryInfo.setCityCode(DeliveryDetailsAreaFragment.cityCodeStr);
        deliveryInfo.setArea(DeliveryDetailsAreaFragment.areaStr);
        deliveryInfo.setContactNumber(EmailAddressFragment.mobileNoStr);
        deliveryInfo.setAddressLine1(DeliveryDetailsAddress.addressLine2);
        deliveryInfo.setAddressLine2(DeliveryDetailsAddress.addressLine2);
        deliveryInfo.setDeliveryNotes(deliveryNotes);
        deliveryInfo.setLatitude(selectedLat);
        deliveryInfo.setLongitude(selectedLong);
        deliveryInfo.setDeliveryNotes(et_deliveryNotes.getText().toString());
        verifyEmiratesIdInput.setDeliveryInfo(deliveryInfo);
        verifyEmiratesIdInput.setDeviceId(deviceId);
        if (NameDOBFragment.fullName != null) {
            verifyEmiratesIdInput.setFullName(NameDOBFragment.fullName);
        } else {
            verifyEmiratesIdInput.setFullName(EmiratesIdDetailsFragment.fullName);
        }
        new VerifyEmiratesIdForNewOrderService(this, verifyEmiratesIdInput);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            if (isVisible()) {
                if (responseModel != null && responseModel.getServiceType() != null && responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.LOG_USER_EVENT)) {
                } else if (responseModel.getServiceType() != null && responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.emirates_id_verify_old)) {

                    regActivity.onBackPressed();

                    if (responseModel != null && responseModel.getResultObj() != null) {
                        verifyEmiratesIdOutput = (VerifyEmiratesIdOutput) responseModel.getResultObj();
                        if (verifyEmiratesIdOutput != null) {
                            if (verifyEmiratesIdOutput.getStatus() == null)
                                onEmiratesIDVerificationFail();
                            else
                                onEmiratesIDVerificationSuccess();
                        }
                    }

                } else {
                    cityAddress = (StringBuilder) responseModel.getResultObj();
                    if (cityAddress != null) {
                        addresstxt.setText(cityAddress.toString());
                    }
                }

            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

    }


    StringBuilder cityAddress;
    int status = 0;

    private void onEmiratesIDVerificationSuccess() {
        logEvent(AdjustEvents.EID_PRECESS_COMPLETE, new Bundle());
        status = verifyEmiratesIdOutput.getStatus();

        switch (status) {

            case 1: // eligable
//                onEligable();
                logEvent(AdjustEvents.AGE_ELIGIBILITY_SUCCESS, new Bundle());
                emiratesID = EmiratesIDVerificationFragment.emiratesID;

                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_delivery_info_eid_entered);
                logEvent(AdjustEvents.REVIEW_ORDER_SUMMARY, new Bundle());
                regActivity.replaceFragmnet(new OrderSummaryFragment(), R.id.frameLayout, true);
                break;
            case 6:
                logEvent(AdjustEvents.ALREADY_SWYP_CUSTOMER, new Bundle());
                CommonMethods.logFirebaseEvent(mFirebaseAnalytics, ConstantUtils.TAGGING_onboarding_error_existing_user);

//                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_onboarding_invalid_eid_number);
                regActivity.replaceFragmnet(new RegisteredNonEmirateIDDocumentFragment(), R.id.frameLayout, true);
                break;
            default:
                logEvent(AdjustEvents.AGE_ELIGIBILITY_FAILURE, new Bundle());
//                CommonMethods.logFirebaseEvent(mFirebaseAnalytics, ConstantUtils.TAGGING_onboarding_error_eid_failure);
//
//                if (verifyEmiratesIdOutput != null && verifyEmiratesIdOutput.getErrorMsgEn() != null) {
//                    showErrorFragment(false, verifyEmiratesIdOutput.getErrorMsgEn());
//                } else {
//                    showErrorFragment(false, getString(R.string.generice_error));
//
//                }

                emiratesID = EmiratesIDVerificationFragment.emiratesID;

                CommonMethods.logFirebaseEvent(mFirebaseAnalytics, ConstantUtils.TAGGING_onboarding_error_eid_failure);

                if (verifyEmiratesIdOutput.getErrorMsgEn() != null) {
                    if (currentLanguage.equalsIgnoreCase("en"))
                        showErrorFragment(verifyEmiratesIdOutput.getErrorMsgEn());
                    else
                        showErrorFragment(verifyEmiratesIdOutput.getErrorMsgAr());
                } else {
                    showErrorFragment(getString(R.string.generice_error));
                }
                break;


                /*
                 * Commented 20/12/2019
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_delivery_info_eid_entered);
                logEvent(AdjustEvents.REVIEW_ORDER_SUMMARY, new Bundle());
                regActivity.replaceFragmnet(new OrderSummaryFragment(), R.id.frameLayout, true);
                break;*/
        }
    }

    private void onEmiratesIDVerificationFail() {
        logEvent(AdjustEvents.EID_PRECESS_COMPLETE, new Bundle());
//        emiratesID = getCode(true);
//        Bundle bundle2 = new Bundle();
//
//        bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_delivery_info_eid_entered);
//        bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_delivery_info_eid_entered);
//        mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_delivery_info_eid_entered, bundle2);
//        regActivity.replaceFragmnet(new OrderSummaryFragment(), R.id.frameLayout, true);
        CommonMethods.logFirebaseEvent(mFirebaseAnalytics, ConstantUtils.TAGGING_onboarding_error_eid_failure);
        if (verifyEmiratesIdOutput.getErrorMsgEn() != null) {
            if (currentLanguage.equalsIgnoreCase("en"))
                showErrorFragment(false, verifyEmiratesIdOutput.getErrorMsgEn());
            else
                showErrorFragment(false, verifyEmiratesIdOutput.getErrorMsgAr());
        } else {
            showErrorFragment(false, getString(R.string.generice_error));
        }


    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        if (responseModel != null && responseModel.getServiceType() != null && !responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.LOG_USER_EVENT)) {
            super.onErrorListener(responseModel);
            try {
                if (isVisible())
                    regActivity.onBackPressed();
            } catch (Exception ex) {
                if (ex != null) {
                    ex.printStackTrace();
                }
            }
            showErrorFragment(false, "Error in service.");
        }
    }

    private void showErrorFragment(boolean isGPSError, String error) {

        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        if (isGPSError) {
            bundle.putString(ConstantUtils.errorString, getString(R.string.enable_gps));
            bundle.putString(ConstantUtils.settingsButtonEnabled, "true");
            bundle.putString(ConstantUtils.buttonTitle, getString(R.string.settings));
            bundle.putString("check", "true");
        } else {
//            bundle.putBoolean("finish", true);
            bundle.putString(error, getString(R.string.connection_error));
        }
        errorFragment.setArguments(bundle);
        regActivity.addFragmnet(errorFragment, frameLayout, true);


    }



    void setListenesrs() {
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), TAGGING_map);
                addressLine2 = addresstxt.getText().toString().trim();
                deliveryNotes = et_deliveryNotes.getText().toString().trim();
//                regActivity.replaceFragmnet(new OrderSummaryFragment(), R.id.frameLayout, true);
                logEvent(AdjustEvents.ADJUST_ADDRESS_MAP, new Bundle());
                if (!NewNumberFragment.msisdnNumber.equalsIgnoreCase("-1")) {
                    logEvent(AdjustEvents.EID_PRECESS_INITIATED, new Bundle());
                    onConsumeService();
//                    regActivity.replaceFragmnet(new EmiratesIDVerificationFragment(), frameLayout, true);

                } else {
                    logEvent(AdjustEvents.REVIEW_ORDER_SUMMARY, new Bundle());
                    regActivity.replaceFragmnet(new OrderSummaryFragment(), frameLayout, true);
                }
//              Intent intent =new Intent(getActivity(),PaymentActivity.class);
//                startActivity(intent);

            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        checkPermission();
        isGPSEnabled();
//        LatLng fLatLng = new LatLng(24.326190, 54.753607);
//        marker = new MarkerOptions().position(fLatLng);
//        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(fLatLng,
//                10));
////        mMap.addMarker(marker.position(fLatLng).title(""));
//        mMap.addMarker(
//                marker.position(fLatLng)
//                        .title("")
//                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.location_icon_fill)));

        mMap.setOnMarkerClickListener(this);
        mMap.setOnInfoWindowClickListener(this);


//          marker.icon(BitmapDescriptorFactory.fromBitmap(R.drawable.location_icon_fill));


//        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
//            @Override
//            public void onMapClick(LatLng latlng) {
//                try {
//                    marker = new MarkerOptions().position(latlng);
//                    Bundle bundle2 = new Bundle();
//
//                    bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_delivery_info_longpress_address);
//                    bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_delivery_info_longpress_address);
//                    mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_delivery_info_longpress_address, bundle2);
//                    if (marker != null) {
//                        mMap.clear();
////                    mMap.addMarker(marker.position(latlng).title(""));
//                        mMap.addMarker(
//                                marker.position(latlng)
//                                        .title("")
//                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.location_icon_fill)));
//                        Location location = new Location("");
//                        location.setLatitude(latlng.latitude);
//                        location.setLongitude(latlng.longitude);
//                        LocationModel locationModel = new LocationModel();
//                        locationModel.setLocation(location);
//                        locationModel.setActivity(getActivity());
//                        addMarkerAtCurrentPosition(mlocation, false);
//
//                        new LocationServiceHelper(locationModel, DeliveryDetailsAddress.this);
//                    }
//                } catch (Exception ex) {
//                    CommonMethods.logException(ex);
//                }
//            }
//        });
        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                selectedLat = mMap.getCameraPosition().target.latitude;
                selectedLong = mMap.getCameraPosition().target.longitude;
                Location location = new Location("");
                location.setLatitude(selectedLat);
                location.setLongitude(selectedLong);
                LocationModel locationModel = new LocationModel();
                locationModel.setLocation(location);
                locationModel.setActivity(getActivity());
                new LocationServiceHelper(locationModel, DeliveryDetailsAddress.this);
//                Toast.makeText(getActivity(),"lat:"+mMap.getCameraPosition().target.latitude + " long:"
//                        + mMap.getCameraPosition().target.longitude,Toast.LENGTH_LONG).show();
            }
        });
    }

    public static double selectedLat, selectedLong;

    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                this.requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            }
        }
    }

    private boolean canAccessLocation() {
        if (!hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
            return false;
        } else {
            return true;
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == getActivity().checkSelfPermission(perm));
    }

    public void checkForGps() {
//        regActivity.hideHeaderLayout();
//        ll_error.setVisibility(View.VISIBLE);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(ConstantUtils.DUBAI_LAT_LONG, 10));
    }

    private void isGPSEnabled() {
        LocationManager locationManager = (LocationManager)
                getActivity().getSystemService(getActivity().LOCATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (canAccessLocation()) {// gps is allowed
//                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && canAccessLocation()) {// gps is enable
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {// gps is disable
                    checkForGps();
                } else {
                    gpsTracker = new GPSTracker(getActivity());
                    if (gpsTracker.canGetLocation()) {
                        mlocation = gpsTracker.getLocation();
                        if (mlocation != null) {
                            try {
                                addMarkerAtCurrentPosition(mlocation, true);
                            } catch (SecurityException ex) {
                                ex.printStackTrace();
                            }
                        } else {
                            Toast.makeText(getActivity(), "Fetching Location, please wait..", Toast.LENGTH_SHORT).show();
                            try {
                                Thread.sleep(1500);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            mlocation = gpsTracker.getLocation();
                            if (mlocation != null) {
                                try {
                                    addMarkerAtCurrentPosition(mlocation, true);

                                } catch (SecurityException ex) {
                                    ex.printStackTrace();
                                }
                            }
                        }
                    } else {
                        checkForGps();
                    }
//                    chec/kLocation();
                }
//                checkLocation();
            } else {
                requestPermissions(LOCATION_PERMS, 0);
            }

        } else {
            gpsTracker = new GPSTracker(getActivity());

            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) { // gps is disable
                checkForGps();
            } else { // gps is enable
                if (gpsTracker.canGetLocation()) {
                    mlocation = gpsTracker.getLocation();
                    if (mlocation != null) {
                        onConsumeService();
                    } else {
                        Toast.makeText(getActivity(), "Fetching Location, please wait..", Toast.LENGTH_SHORT).show();
                        try {
                            Thread.sleep(1500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        mlocation = gpsTracker.getLocation();
                        onConsumeService();
                    }
                }
            }

        }

    }

//    public void addCurrentLocationMarker(Location mLocation) {
//        if (mMap != null && mLocation != null) {
//            final LatLng mLatLng = new LatLng(mLocation.getLatitude(),
//                    mLocation.getLongitude());
//            marker = new MarkerOptions().position(mLatLng);
//
//
////             marker.icon(BitmapDescriptorFactory.fromBitmap(etisaltLogo));
//
//            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mLatLng,
//                    8));
//
//        }
//    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 0:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    isGPSEnabled();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    checkForGps();
//                    Toast.makeText(this, "To use this feature, kindly allow Ding access to your location.", Toast.LENGTH_LONG).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        regActivity.showHeaderLayout();
        regActivity.setHeaderTitle(getString(R.string.enter_delivery_address_spaced));
        ll_error.setVisibility(View.GONE);
        switch (requestCode) {
            case LOCATION_REQUEST:
                LocationManager locationManager = (LocationManager)
                        getActivity().getSystemService(getActivity().LOCATION_SERVICE);
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    checkForGps();
                } else {
                    isGPSEnabled();
                }
                break;
            default:
                break;
        }

    }

    private void showErrorFragment(boolean isGPSError) {

        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        if (isGPSError) {
            bundle.putString(ConstantUtils.errorString, getString(R.string.enable_gps));
            bundle.putString(ConstantUtils.settingsButtonEnabled, "true");
            bundle.putString(ConstantUtils.buttonTitle, getString(R.string.settings));
            bundle.putString("check", "true");
        } else {
            bundle.putBoolean("finish", true);
            bundle.putString(ConstantUtils.errorString, getString(R.string.connection_error));
        }
        errorFragment.setArguments(bundle);
        regActivity.replaceFragmnet(errorFragment, frameLayout, true);


    }



    private void showErrorFragment(String error) {
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, error);
        ErrorFragment errorFragment = new ErrorFragment();
        errorFragment.setArguments(bundle);
        regActivity.replaceFragmnet(errorFragment, R.id.frameLayout, true);
    }


    @Override
    public void onInfoWindowClick(Marker marker) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    private void enableNextBtn() {
        nextBtn.setAlpha(1f);
        nextBtn.setEnabled(true);
    }

    private void disableNextBtn() {
        nextBtn.setAlpha(.5f);
        nextBtn.setEnabled(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (NewNumberFragment.isTimerActive && CommonMethods.isTimerUp(sharedPrefrencesManger.getTimerValue())) {
            Fragment[] myFragments = {OrderFragment.orderFragmentInstance,
                    DeliveryMethodFragment.deliveryFragmentInstance,
                    DeliveryDetailsAreaFragment.deliveryDetailsAreaFragment,
                    DeliveryDetailsAddress.deliveryDetailsAddress,
                    NewNumberFragment.newNumberFragment,
                    NewNumberFragment.loadingFragmnet
            };
            regActivity.removeFragment(myFragments);
            regActivity.replaceFragmnet(new NewNumberFragment(), frameLayout, true);

        }
    }


    public void addMarkerAtCurrentPosition(Location mLocation, boolean zoomIn) {
        try {
            if (mLocation != null) {
                LatLng mLatLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
                MarkerOptions tempMarker = new MarkerOptions().position(mLatLng);
                mMap.addMarker(
                        tempMarker.position(mLatLng)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.locate_me_marker)));
                if (zoomIn) {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mLatLng, ConstantUtils.zoomLevel));
                }
            }
        } catch (Exception ex) {

        }
    }

    private void logEvent() {
        logEventInputModel.setActionTransactionId(sharedPrefrencesManger.getTempAppId());
        logEventInputModel.setScreenId(ConstantUtils.SCREEN_ID_1004);
        logEventInputModel.setAddress(addresstxt.getText().toString());
        new LogApplicationFlowService(this, logEventInputModel);
    }
}