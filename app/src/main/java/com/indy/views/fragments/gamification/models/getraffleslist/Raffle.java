package com.indy.views.fragments.gamification.models.getraffleslist;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Tohamy on 10/4/2017.
 */

public class Raffle implements Parcelable {

    @SerializedName("indyEventId")
    @Expose
    private String indyEventId;
    @SerializedName("active")
    @Expose
    private boolean active;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("thumbNailUrl")
    @Expose
    private String thumbNailUrl;
    @SerializedName("shortDescription")
    @Expose
    private String shortDescription;
    @SerializedName("titleEn")
    @Expose
    private String titleEn;
    @SerializedName("titleAr")
    @Expose
    private String titleAr;
    @SerializedName("descriptionEn")
    @Expose
    private String descriptionEn;
    @SerializedName("descriptionAr")
    @Expose
    private String descriptionAr;
    @SerializedName("endsOnDate")
    @Expose
    private String endsOnDate;
    @SerializedName("indyRaffleEventId")
    @Expose
    private String indyRaffleEventId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("userRaffleTickets")
    @Expose
    private int userRaffleTickets;
    @SerializedName("userParticipant")
    @Expose
    private boolean userParticipant;

    protected Raffle(Parcel in) {
        indyEventId = in.readString();
        active = in.readInt() == 1;
        imageUrl = in.readString();
        thumbNailUrl = in.readString();
        shortDescription = in.readString();
        titleEn = in.readString();
        titleAr = in.readString();
        descriptionEn = in.readString();
        descriptionAr = in.readString();
        endsOnDate = in.readString();
        indyRaffleEventId = in.readString();
        title = in.readString();
        description = in.readString();
        userRaffleTickets = in.readInt();
        userParticipant = in.readInt() == 1;
    }

    public static final Creator<Raffle> CREATOR = new Creator<Raffle>() {
        @Override
        public Raffle createFromParcel(Parcel in) {
            return new Raffle(in);
        }

        @Override
        public Raffle[] newArray(int size) {
            return new Raffle[size];
        }
    };

    public String getIndyEventId() {
        return indyEventId;
    }

    public void setIndyEventId(String indyEventId) {
        this.indyEventId = indyEventId;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getThumbNailUrl() {
        return thumbNailUrl;
    }

    public void setThumbNailUrl(String thumbNailUrl) {
        this.thumbNailUrl = thumbNailUrl;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getTitleEn() {
        return titleEn;
    }

    public void setTitleEn(String titleEn) {
        this.titleEn = titleEn;
    }

    public String getTitleAr() {
        return titleAr;
    }

    public void setTitleAr(String titleAr) {
        this.titleAr = titleAr;
    }

    public String getDescriptionEn() {
        return descriptionEn;
    }

    public void setDescriptionEn(String descriptionEn) {
        this.descriptionEn = descriptionEn;
    }

    public String getDescriptionAr() {
        return descriptionAr;
    }

    public void setDescriptionAr(String descriptionAr) {
        this.descriptionAr = descriptionAr;
    }

    public String getEndsOnDate() {
        return endsOnDate;
    }

    public void setEndsOnDate(String endsOnDate) {
        this.endsOnDate = endsOnDate;
    }

    public String getIndyRaffleEventId() {
        return indyRaffleEventId;
    }

    public void setIndyRaffleEventId(String indyRaffleEventId) {
        this.indyRaffleEventId = indyRaffleEventId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getUserRaffleTickets() {
        return userRaffleTickets;
    }

    public void setUserRaffleTickets(int userRaffleTickets) {
        this.userRaffleTickets = userRaffleTickets;
    }

    public boolean getUserParticipant() {
        return userParticipant;
    }

    public void setUserParticipant(boolean userParticipant) {
        this.userParticipant = userParticipant;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(indyEventId);
        parcel.writeInt(active ? 1 : 0);
        parcel.writeString(imageUrl);
        parcel.writeString(thumbNailUrl);
        parcel.writeString(shortDescription);
        parcel.writeString(titleEn);
        parcel.writeString(titleAr);
        parcel.writeString(descriptionEn);
        parcel.writeString(descriptionEn);
        parcel.writeString(endsOnDate);
        parcel.writeString(indyRaffleEventId);
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeInt(userRaffleTickets);
        parcel.writeInt(userParticipant ? 1 : 0);
    }
}
