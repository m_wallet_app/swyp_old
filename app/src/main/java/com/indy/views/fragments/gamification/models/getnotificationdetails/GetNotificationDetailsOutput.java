package com.indy.views.fragments.gamification.models.getnotificationdetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by Tohamy on 10/4/2017.
 */

public class GetNotificationDetailsOutput extends MasterErrorResponse {

    @SerializedName("notificationDetails")
    @Expose
    private NotificationDetails notificationDetails;

    public NotificationDetails getNotificationDetails() {
        return notificationDetails;
    }

    public void setNotificationDetails(NotificationDetails notificationDetails) {
        this.notificationDetails = notificationDetails;
    }

}