package com.indy.views.fragments.store.recharge;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.models.rechargeList.RechargeListInput;
import com.indy.models.rechargeList.RechargeListOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.models.utils.MasterInputResponse;
import com.indy.services.RechargeListService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.CreditCardPaymentActivity;
import com.indy.views.activites.RechargeActivity;
import com.indy.views.fragments.store.recharge.RechargeAnimations.TimerView;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.indy.views.fragments.utils.MasterFragment;

/**
 * Created by Amir.jehangir on 10/14/2016.
 */
public class RechargeCreditCardFragment extends MasterFragment implements View.OnClickListener {
    private View view;
    TextView tv_lastUpdated;
    public TextView titleview;
    Button btn_proceed;
    RechargeActivity rechargeActivity;
    RechargeListInput rechargeListInput;
    MasterInputResponse masterInputResponse;
    RechargeListOutput rechargeListOutput;
    Context context = getActivity();
    private static final int TIMER_LENGTH25 = 1;
    private static final int TIMER_LENGTH50 = 2;
    private static final int TIMER_LENGTH100 = 1;
    private static final int TIMER_LENGTH200 = 1;
    private static final int TIMER_LENGTH500 = 2;
    FrameLayout frame25, frame50, frame100, frame200, frame500;
    private TimerView mTimerView25, mTimerView50, mTimerView100, mTimerView200, mTimerView500;
    TextView tv25, tv50, tv100, tv200, tv500;
    private String status;
    EditText rechargenumberAmount;
    private CharSequence emailCharSeq, fullNameCharSeq;
    String[] RechargeAmount;
    int Counter25 = 0, Counter50 = 0, Counter100 = 0, Counter200 = 0, Counter500 = 0;
    public double amountBalance = 0;
    public String amountSelected = "";
    LinearLayout lv_frames;
    public TextInputLayout til_amount;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_selectamount_recharge, container, false);
        rechargeActivity = (RechargeActivity) getActivity();
        view.bringToFront();
        initUI();
        if (getArguments() != null && getArguments().getString("balance") != null) {
            try {
                amountBalance = Double.parseDouble(getArguments().getString("balance"));
            } catch (Exception ex) {
                amountBalance = 0;
            }
        }
        titleview.setText(getString(R.string.your_balance_is_aed) + " " + amountBalance+ " " + getString(R.string.aed_arabic_only));
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        frame25.setOnClickListener(this);
        frame50.setOnClickListener(this);
        frame100.setOnClickListener(this);
        frame200.setOnClickListener(this);
        frame500.setOnClickListener(this);

    }

    @Override
    public void initUI() {
        super.initUI();


        rechargenumberAmount = (EditText) view.findViewById(R.id.rechargenumberAmount);
        titleview = (TextView) view.findViewById(R.id.user_balance_amount);
        titleview.setVisibility(View.INVISIBLE);
        //   titleview.setText(getResources().getString(R.string.payment_title));
        mTimerView25 = (TimerView) view.findViewById(R.id.timer25);
        mTimerView50 = (TimerView) view.findViewById(R.id.timer50);
        mTimerView100 = (TimerView) view.findViewById(R.id.timer100);
        mTimerView200 = (TimerView) view.findViewById(R.id.timer200);
        mTimerView500 = (TimerView) view.findViewById(R.id.timer500);
        frame25 = (FrameLayout) view.findViewById(R.id.frame25);
        frame50 = (FrameLayout) view.findViewById(R.id.frame50);
        frame100 = (FrameLayout) view.findViewById(R.id.frame100);
        frame200 = (FrameLayout) view.findViewById(R.id.frame200);
        frame500 = (FrameLayout) view.findViewById(R.id.frame500);
        tv25 = (TextView) view.findViewById(R.id.tv25);
        tv50 = (TextView) view.findViewById(R.id.tv50);
        tv100 = (TextView) view.findViewById(R.id.tv100);
        tv200 = (TextView) view.findViewById(R.id.tv200);
        tv500 = (TextView) view.findViewById(R.id.tv500);
        lv_frames = (LinearLayout) view.findViewById(R.id.lv_frames);
        lv_frames.setEnabled(false);
        lv_frames.setAlpha(0.4f);
        til_amount = (TextInputLayout) view.findViewById(R.id.rechargenumberTxtInput);

        mTimerView25.start(TIMER_LENGTH25);
        mTimerView50.start(TIMER_LENGTH50);
        mTimerView100.start(TIMER_LENGTH100);
        mTimerView200.start(TIMER_LENGTH200);
        mTimerView500.start(TIMER_LENGTH500);

        btn_proceed = (Button) view.findViewById(R.id.btn_proceed);
        btn_proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayNextFragment();
            }
        });
//
        onConsumeService();
        changeNextBtnState();
        btn_proceed.setAlpha(.5f);
        btn_proceed.setClickable(false);

        rechargenumberAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    resetPredefinedLayouts();
                    disableNextBtn();
                } else {
                    isValidAmount(rechargenumberAmount.getText().toString());
                }
            }
        });

        rechargenumberAmount.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    isValidAmount(rechargenumberAmount.getText().toString());
                    hideKeyBoard();
                    return true;
                }
                return true;
            }
        });

        tv_lastUpdated = (TextView) view.findViewById(R.id.tv_last_updated);
        tv_lastUpdated.setText(getString(R.string.last_updated) + " " + sharedPrefrencesManger.getLastUpdatedBalance() +
                " " + getString(R.string.min_ago));
        tv_lastUpdated.setVisibility(View.INVISIBLE);
        removeTextInputLayoutError(til_amount);

        if(sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)){
            tv25.setPaddingRelative(0,0,0,0);
            tv50.setPaddingRelative(0,0,0,0);
            tv100.setPaddingRelative(0,0,0,0);
            tv200.setPaddingRelative(0,0,0,0);
            tv500.setPaddingRelative(0,0,0,0);

        }else{
            tv25.setPaddingRelative(0,6,0,0);
            tv50.setPaddingRelative(0,6,0,0);
            tv100.setPaddingRelative(0,6,0,0);
            tv200.setPaddingRelative(0,6,0,0);
            tv500.setPaddingRelative(0,6,0,0);
        }
    }


    private void displayNextFragment() {
//        CongratulationsFragmentRecharge congratulationsFragmentRecharge = new CongratulationsFragmentRecharge();
        Bundle bundle = new Bundle();
        bundle.putString("amount", amountSelected.toString());
//        congratulationsFragmentRecharge.setArguments(bundle);
//        rechargeActivity.replaceFragmnet(congratulationsFragmentRecharge, R.id.frameLayout, true);
        Intent intent = new Intent(getActivity(), CreditCardPaymentActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_recharge_credit_card_pay);

        //rechargeActivity.replaceFragmnet(new PaymentBeneficiaryAndDescriptionFragment(), R.id.frameLayout, true);
    }

    @Override
    public void onClick(View v) {

        setFrameAttributes(v.getId());
    }


    public void changeNextBtnState() {
//

//        rechargenumberAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View view, boolean b) {
//                if(!b){
//
//                    isValidAmount(rechargenumberAmount.getText().toString());
//                }
//            }
//        });
//        rechargenumberAmount.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//                // TODO Auto-generated method stub
//                fullNameCharSeq = s;
//                isValidAmount(s.toString());
//            }
//
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//                // TODO Auto-generated method stub
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//                // TODO Auto-generated method stub
//            }
//        });
    }


    private void enableNextBtn() {
        btn_proceed.setAlpha(1f);
        btn_proceed.setClickable(true);
        removeTextInputLayoutError(til_amount);
        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_recharge_credit_card_select_amount+"_"+amountSelected);


    }

    private void disableNextBtn() {
        btn_proceed.setAlpha(.5f);
        btn_proceed.setClickable(false);

    }


    private void setFrameAttributes(int id) {
        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_recharge_credit_card_select_amount);

        switch (id) {
            case R.id.frame25: {

                if (Counter25 > 0) {
                    mTimerView25.setBackground(getResources().getDrawable(R.drawable.rbtn_selector));
                    tv25.setTextColor(getResources().getColor(R.color.title_usage_pink));
                    disableNextBtn();
                    amountSelected = "0";
                    Counter25 = 0;
                    rechargenumberAmount.setEnabled(true);
                } else {
                    Counter25 = 1;
                    rechargenumberAmount.setText("");
                    Counter50 = 0;
                    Counter100 = 0;
                    Counter200 = 0;
                    Counter500 = 0;
                    amountSelected = tv25.getText().toString();
                    enableNextBtn();
                    mTimerView25.setBackground(getResources().getDrawable(R.drawable.rbtn_textcolor_two));
                    mTimerView50.setBackground(getResources().getDrawable(R.drawable.rbtn_selector));
                    mTimerView100.setBackground(getResources().getDrawable(R.drawable.rbtn_selector));
                    mTimerView200.setBackground(getResources().getDrawable(R.drawable.rbtn_selector));
                    mTimerView500.setBackground(getResources().getDrawable(R.drawable.rbtn_selector));


                    tv25.setTextColor(getResources().getColor(R.color.color_white));
                    tv50.setTextColor(getResources().getColor(R.color.title_usage_pink));
                    tv100.setTextColor(getResources().getColor(R.color.title_usage_pink));
                    tv200.setTextColor(getResources().getColor(R.color.title_usage_pink));
                    tv500.setTextColor(getResources().getColor(R.color.title_usage_pink));

                }
            }
            break;
            case R.id.frame50: {

                if (Counter50 > 0) {
                    mTimerView50.setBackground(getResources().getDrawable(R.drawable.rbtn_selector));
                    tv50.setTextColor(getResources().getColor(R.color.title_usage_pink));
                    Counter50 = 0;
                    disableNextBtn();
                    amountSelected = "0";
                    rechargenumberAmount.setEnabled(true);
                } else {
                    Counter50 = 1;
                    rechargenumberAmount.setText("");
                    Counter25 = 0;
                    Counter100 = 0;
                    Counter200 = 0;
                    Counter500 = 0;
                    amountSelected = tv50.getText().toString();
                    enableNextBtn();
                    mTimerView25.setBackground(getResources().getDrawable(R.drawable.rbtn_selector));
                    mTimerView50.setBackground(getResources().getDrawable(R.drawable.rbtn_textcolor_two));
                    mTimerView100.setBackground(getResources().getDrawable(R.drawable.rbtn_selector));
                    mTimerView200.setBackground(getResources().getDrawable(R.drawable.rbtn_selector));
                    mTimerView500.setBackground(getResources().getDrawable(R.drawable.rbtn_selector));


                    tv25.setTextColor(getResources().getColor(R.color.title_usage_pink));
                    tv50.setTextColor(getResources().getColor(R.color.color_white));
                    tv100.setTextColor(getResources().getColor(R.color.title_usage_pink));
                    tv200.setTextColor(getResources().getColor(R.color.title_usage_pink));
                    tv500.setTextColor(getResources().getColor(R.color.title_usage_pink));

                }

            }
            break;
            case R.id.frame100: {
                if (Counter100 > 0) {
                    mTimerView100.setBackground(getResources().getDrawable(R.drawable.rbtn_selector));
                    tv100.setTextColor(getResources().getColor(R.color.title_usage_pink));
                    Counter100 = 0;
                    disableNextBtn();
                    amountSelected = "0";
                    rechargenumberAmount.setEnabled(true);
                } else {
                    Counter100 = 1;
                    Counter50 = 0;
                    Counter25 = 0;
                    Counter200 = 0;
                    Counter500 = 0;
                    rechargenumberAmount.setText("");

                    amountSelected = tv100.getText().toString();
                    enableNextBtn();

                    mTimerView25.setBackground(getResources().getDrawable(R.drawable.rbtn_selector));
                    mTimerView50.setBackground(getResources().getDrawable(R.drawable.rbtn_selector));
                    mTimerView100.setBackground(getResources().getDrawable(R.drawable.rbtn_textcolor_two));
                    mTimerView200.setBackground(getResources().getDrawable(R.drawable.rbtn_selector));
                    mTimerView500.setBackground(getResources().getDrawable(R.drawable.rbtn_selector));


                    tv25.setTextColor(getResources().getColor(R.color.title_usage_pink));
                    tv50.setTextColor(getResources().getColor(R.color.title_usage_pink));
                    tv100.setTextColor(getResources().getColor(R.color.color_white));
                    tv200.setTextColor(getResources().getColor(R.color.title_usage_pink));
                    tv500.setTextColor(getResources().getColor(R.color.title_usage_pink));

                }

            }
            break;
            case R.id.frame200: {
                if (Counter200 > 0) {
                    mTimerView200.setBackground(getResources().getDrawable(R.drawable.rbtn_selector));
                    tv200.setTextColor(getResources().getColor(R.color.title_usage_pink));
                    Counter200 = 0;
                    disableNextBtn();
                    amountSelected = "0";
                    rechargenumberAmount.setEnabled(true);
                } else {
                    Counter200 = 1;
                    rechargenumberAmount.setText("");
                    Counter50 = 0;
                    Counter100 = 0;
                    Counter25 = 0;
                    Counter500 = 0;
                    amountSelected = tv200.getText().toString();
                    enableNextBtn();
                    mTimerView25.setBackground(getResources().getDrawable(R.drawable.rbtn_selector));
                    mTimerView50.setBackground(getResources().getDrawable(R.drawable.rbtn_selector));
                    mTimerView100.setBackground(getResources().getDrawable(R.drawable.rbtn_selector));
                    mTimerView200.setBackground(getResources().getDrawable(R.drawable.rbtn_textcolor_two));
                    mTimerView500.setBackground(getResources().getDrawable(R.drawable.rbtn_selector));


                    tv25.setTextColor(getResources().getColor(R.color.title_usage_pink));
                    tv50.setTextColor(getResources().getColor(R.color.title_usage_pink));
                    tv100.setTextColor(getResources().getColor(R.color.title_usage_pink));
                    tv200.setTextColor(getResources().getColor(R.color.color_white));
                    tv500.setTextColor(getResources().getColor(R.color.title_usage_pink));

                }

            }
            break;
            case R.id.frame500: {
                if (Counter500 > 0) {
                    mTimerView500.setBackground(getResources().getDrawable(R.drawable.rbtn_selector));
                    tv500.setTextColor(getResources().getColor(R.color.title_usage_pink));
                    Counter500 = 0;
                    disableNextBtn();
                    amountSelected = "0";
                    rechargenumberAmount.setEnabled(true);
                } else {
                    Counter500 = 1;

                    rechargenumberAmount.setText("");
                    rechargenumberAmount.setEnabled(false);
                    amountSelected = tv500.getText().toString();
                    enableNextBtn();
                    mTimerView25.setBackground(getResources().getDrawable(R.drawable.rbtn_selector));
                    mTimerView50.setBackground(getResources().getDrawable(R.drawable.rbtn_selector));
                    mTimerView100.setBackground(getResources().getDrawable(R.drawable.rbtn_selector));
                    mTimerView200.setBackground(getResources().getDrawable(R.drawable.rbtn_selector));
                    mTimerView500.setBackground(getResources().getDrawable(R.drawable.rbtn_textcolor_two));

                    Counter50 = 0;
                    Counter100 = 0;
                    Counter200 = 0;
                    Counter25 = 0;
                    tv25.setTextColor(getResources().getColor(R.color.title_usage_pink));
                    tv50.setTextColor(getResources().getColor(R.color.title_usage_pink));
                    tv100.setTextColor(getResources().getColor(R.color.title_usage_pink));
                    tv200.setTextColor(getResources().getColor(R.color.title_usage_pink));
                    tv500.setTextColor(getResources().getColor(R.color.color_white));

                }

            }
            break;
        }

    }


    @Override
    public void onConsumeService() {
        super.onConsumeService();
        masterInputResponse = new MasterInputResponse();
        masterInputResponse.setLang(currentLanguage);
        masterInputResponse.setAppVersion(appVersion);
        masterInputResponse.setOsVersion(osVersion);
        masterInputResponse.setToken(token);
        masterInputResponse.setChannel(channel);
        masterInputResponse.setDeviceId(deviceId);
        masterInputResponse.setMsisdn(sharedPrefrencesManger.getMobileNo());
        masterInputResponse.setImsi(sharedPrefrencesManger.getMobileNo());
        masterInputResponse.setAuthToken(authToken);
        new RechargeListService(this, masterInputResponse);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            if (isVisible()) {
                //  rechargeActivity.onBackPressed();

                if (isVisible()) {
                    if(responseModel!=null && responseModel.getResultObj()!=null) {
                        rechargeListOutput = (RechargeListOutput) responseModel.getResultObj();
                        if (rechargeListOutput != null) {
                            if (rechargeListOutput.getErrorCode() != null && rechargeListOutput.getRechargeList() == null) {
                                onRechargeListIDVerificationFail();
                                lv_frames.setEnabled(false);
                                lv_frames.setAlpha(0.4f);
                            } else if (rechargeListOutput != null && rechargeListOutput.getRechargeList() != null &&
                                    rechargeListOutput.getRechargeList().length > 0) {
                                lv_frames.setEnabled(true);
                                lv_frames.setAlpha(1.0f);
                                onRechargeListIDVerificationSuccess();
                            } else if (rechargeListOutput != null && rechargeListOutput.getRechargeList() != null &&
                                    rechargeListOutput.getRechargeList().length == 0) {
                                lv_frames.setEnabled(false);
                                lv_frames.setAlpha(0.4f);
                            }
                        }
                    }
                }

            }
        }catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

    }

    private void onRechargeListIDVerificationFail() {
//        Toast.makeText(getActivity(), "fail" + status, Toast.LENGTH_SHORT).show();
        if (currentLanguage.equalsIgnoreCase("en"))
            showErrorFragment(rechargeListOutput.getErrorMsgEn());
        else
            showErrorFragment(rechargeListOutput.getErrorMsgAr());

    }


    private void onRechargeListIDVerificationSuccess() {
        status = rechargeListOutput.getRechargeList().toString();
        SetRechargeValues();

    }

    private void SetRechargeValues() {
        mTimerView25 = (TimerView) view.findViewById(R.id.timer25);
        mTimerView50 = (TimerView) view.findViewById(R.id.timer50);
        mTimerView100 = (TimerView) view.findViewById(R.id.timer100);
        mTimerView200 = (TimerView) view.findViewById(R.id.timer200);
        mTimerView500 = (TimerView) view.findViewById(R.id.timer500);
        frame25 = (FrameLayout) view.findViewById(R.id.frame25);
        frame50 = (FrameLayout) view.findViewById(R.id.frame50);
        frame100 = (FrameLayout) view.findViewById(R.id.frame100);
        frame200 = (FrameLayout) view.findViewById(R.id.frame200);
        frame500 = (FrameLayout) view.findViewById(R.id.frame500);
        tv25 = (TextView) view.findViewById(R.id.tv25);
        tv50 = (TextView) view.findViewById(R.id.tv50);
        tv100 = (TextView) view.findViewById(R.id.tv100);
        tv200 = (TextView) view.findViewById(R.id.tv200);
        tv500 = (TextView) view.findViewById(R.id.tv500);
        RechargeAmount = new String[rechargeListOutput.getRechargeList().length];
        RechargeAmount[0] = String.valueOf(rechargeListOutput.getRechargeList()[0]);
        RechargeAmount[1] = String.valueOf(rechargeListOutput.getRechargeList()[1]);
        RechargeAmount[2] = String.valueOf(rechargeListOutput.getRechargeList()[2]);
        RechargeAmount[3] = String.valueOf(rechargeListOutput.getRechargeList()[3]);
        RechargeAmount[4] = String.valueOf(rechargeListOutput.getRechargeList()[4]);

        tv25.setText(RechargeAmount[0].toString());
        tv50.setText(RechargeAmount[1].toString());
        tv100.setText(RechargeAmount[2].toString());
        tv200.setText(RechargeAmount[3].toString());
        tv500.setText(RechargeAmount[4].toString());
    }


    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
//        if (isVisible())
//            Toast.makeText(getActivity(), "fail" + status, Toast.LENGTH_SHORT).show();
        //  regActivity.onBackPressed();

//        showErrorFragment("Error in service.");
    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    private void showLoadingFragment() {
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.loadingString, getString(R.string.verifying_eid));
        LoadingFragmnet loadingFragmnet = new LoadingFragmnet();
        loadingFragmnet.setArguments(bundle);
        rechargeActivity.addFragmnet(loadingFragmnet, R.id.frameLayout, true);
    }

    private void showErrorFragment(String error) {
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, error);
        ErrorFragment errorFragment = new ErrorFragment();
        errorFragment.setArguments(bundle);
        rechargeActivity.replaceFragmnet(errorFragment, R.id.frameLayout, true);
    }


    public void resetPredefinedLayouts() {
        mTimerView25.setBackground(getResources().getDrawable(R.drawable.rbtn_selector));
        mTimerView50.setBackground(getResources().getDrawable(R.drawable.rbtn_selector));
        mTimerView100.setBackground(getResources().getDrawable(R.drawable.rbtn_selector));
        mTimerView200.setBackground(getResources().getDrawable(R.drawable.rbtn_selector));
        mTimerView500.setBackground(getResources().getDrawable(R.drawable.rbtn_selector));
        tv25.setTextColor(getResources().getColor(R.color.title_usage_pink));
        tv50.setTextColor(getResources().getColor(R.color.title_usage_pink));
        tv100.setTextColor(getResources().getColor(R.color.title_usage_pink));
        tv200.setTextColor(getResources().getColor(R.color.title_usage_pink));
        tv500.setTextColor(getResources().getColor(R.color.title_usage_pink));
        Counter25 = 0;
        Counter50 = 0;
        Counter100 = 0;
        Counter200 = 0;
        Counter500 = 0;

    }

    public void isValidAmount(String s) {
        double amountToSend = 0;

        try {
            amountToSend = Double.parseDouble(s.trim());
            if (s.length()>0&&amountToSend >= 25 && amountToSend <= 500) {
                enableNextBtn();
                amountSelected = amountToSend + "";
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_recharge_credit_card_enter_amount);

            } else {
                disableNextBtn();
                amountSelected = "0";
                setTextInputLayoutError(til_amount, getString(R.string.amount_must_be_from_25_aed_till_500_aed));

            }
        } catch (Exception ex) {
//            if (fullNameCharSeq != null)
//                if (s.length() < 2 || fullNameCharSeq.length() < 2) {
                    disableNextBtn();
                    amountSelected = "0";
            if(s.length()>0)
                    setTextInputLayoutError(til_amount, getString(R.string.amount_must_be_from_25_aed_till_500_aed));
//
//                } else {
//                    amountSelected = fullNameCharSeq + "";
//                    enableNextBtn();
//                }
        }

    }
}
