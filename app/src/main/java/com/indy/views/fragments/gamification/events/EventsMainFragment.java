package com.indy.views.fragments.gamification.events;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.indy.R;
import com.indy.customviews.CustomTabLayout;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.MasterActivity;
import com.indy.views.activites.SwpeMainActivity;
import com.indy.views.fragments.gamification.adapters.EventsPagerAdapter;
import com.indy.views.fragments.gamification.models.EventsList;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetEventsListInputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetEventsListOutputModel;
import com.indy.views.fragments.gamification.services.GetEventsListService;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.MasterFragment;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by emad on 8/9/16.
 */
public class EventsMainFragment extends MasterFragment {

    private View view;
    private ViewPager viewPager;
    private TextView tv_title, tv_headerTitle, tv_membership_cycle;
    private GetEventsListInputModel getEventsListInputModel;
    private GetEventsListOutputModel getEventsListOutputModel;
    private ImageView ic_scroll_up;
    public static EventsMainFragment eventsFragmentInstance;
    private ProgressBar progressBar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_events, container, false);
            initUI();
            eventsFragmentInstance = this;
        }
        return view;
    }


    @Override
    public void initUI() {
        super.initUI();
//        expiryTxt = (TextView) view.findViewById(R.id.expiryTxt);

        viewPager = (ViewPager) view.findViewById(R.id.pager);
        tv_headerTitle = (TextView) view.findViewById(R.id.usageTxtID);
        tv_headerTitle.setText(getString(R.string.events));
//        tv_membershipDate.setText(getString(R.string.membership_expires) + "30" + getString(R.string.membership_cycle_days) + getString(R.string.membership_cycle_daysLeft));


        LinearLayout lv_main_swip = (LinearLayout) view.findViewById(R.id.lv_main_swip);
        lv_main_swip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((SwpeMainActivity) getActivity()).mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED)
                    ((SwpeMainActivity) getActivity()).expandMenu();
                else if (((SwpeMainActivity) getActivity()).mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED)
                    ((SwpeMainActivity) getActivity()).collapseMenu();
            }
        });

        tv_title = (TextView) view.findViewById(R.id.tv_events_header);
        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) view.findViewById(R.id.coordinateLayoutID);
        progressBar = (ProgressBar) view.findViewById(R.id.progressID);
        if (sharedPrefrencesManger != null)
            if (sharedPrefrencesManger.getUserProfileObject() != null) {
                if (sharedPrefrencesManger.getUserProfileObject().getMembershipData() != null) {
                    if (sharedPrefrencesManger.getUserProfileObject().getMembershipData().getStatus() == ConstantUtils.validMembership) {
                        onHasMembership();
                    } else {
                        onNoMembership();

                    }
                }

            }

        ic_scroll_up = (ImageView) view.findViewById(R.id.ic_scroll_up);
        ic_scroll_up.setBackground(getResources().getDrawable(R.drawable.ic_scroll));

//        btn_membership.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                //dont happen yet
////                Intent intent = new Intent(getActivity(), BenefitsFragment.class);
////                startActivity(intent);
//                ((SwpeMainActivity) getActivity()).replaceFragmnet(new BenefitsFragment(), R.id.contentFrameLayout, true);
//            }
//        });
        initTabLayout();
    }


    private void onHasMembership() {
    }

    private void onNoMembership() {
    }



    private void initTabLayout() {
        final CustomTabLayout tabLayout = (CustomTabLayout) view.findViewById(R.id.tab_layout);
        tabLayout.setSharedPrefrencesManger(sharedPrefrencesManger);
        tabLayout.addTab(tabLayout.newTab().setText((getString(R.string.current))));
        tabLayout.addTab(tabLayout.newTab().setText((getString(R.string.previous))));
//        tabLayout.addTab(tabLayout.newTab().setText((getString(R.string.previous))));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);

        final EventsPagerAdapter adapter = new EventsPagerAdapter
                (getChildFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(0, true);
        viewPager.setSelected(true);
        viewPager.setFocusable(true);
        tabLayout.getTabAt(0).select();
        tv_title.setText(getString(R.string.participate_in_event_photo));
        LinearLayout ll_viewPager = (LinearLayout) view.findViewById(R.id.ll_viewPager);
        ll_viewPager.setVisibility(View.VISIBLE);
//        tabLayout.setFillViewport(true);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                if (tab.getPosition() == 0) {
                    tv_title.setText(getString(R.string.participate_in_event_photo));
                } else if (tab.getPosition() == 1) {
                    tv_title.setText(getString(R.string.check_out_the_past_events_and_winners));
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        progressBar.setVisibility(View.GONE);
    }

    public void onSlideUp() {
        if (tv_headerTitle != null) {
            tv_headerTitle.setVisibility(View.VISIBLE);
            if (isVisible()) {
                ic_scroll_up.setBackground(getResources().getDrawable(R.drawable.ic_scroll));

            }
        }
    }

    public void onSlideDown() {
        if (tv_headerTitle != null) {
            tv_headerTitle.setVisibility(View.INVISIBLE);
            if (isVisible()) {
                ic_scroll_up.setBackground(getResources().getDrawable(R.drawable.ic_up));

            }
        }
    }
}