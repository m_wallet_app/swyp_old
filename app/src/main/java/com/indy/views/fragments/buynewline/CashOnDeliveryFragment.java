package com.indy.views.fragments.buynewline;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;

import com.indy.R;
import com.indy.controls.AdjustEvents;
import com.indy.models.finalizeregisteration.DeliveryInfo;
import com.indy.models.finalizeregisteration.FinalizeRegisterationRequestInput;
import com.indy.models.finalizeregisteration.FinalizeRegisterationRequestOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.FinalizeRegisteraionRequestService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.buynewline.exisitingnumber.EmiratesIDVerificationFragment;
import com.indy.views.fragments.buynewline.exisitingnumber.NumberTransferFragment;
import com.indy.views.fragments.buynewline.exisitingnumber.PromoCodeVerificationSuccessfulFragment;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.indy.views.fragments.utils.MasterFragment;


/**
 * Created by hadi on 7/25/17.
 */

public class CashOnDeliveryFragment extends MasterFragment {
    private View view;
    private Button nextBtn;
    private RadioButton paidRBtn, freeRBtn;
    public static String orderTypeStr;
    private FinalizeRegisterationRequestInput finalizeRegisterationRequestInput;
    private FinalizeRegisterationRequestOutput finalizeRegisterationRequestOutput;
    public static CashOnDeliveryFragment orderFragmentInstance;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragmnet_cash_on_delivery, container, false);
        initUI();
        setListeners();
        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_tell_us_how_you_want_to_pay_screen);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
//        Fragment[] myFragments = {NewNumberFragment.newNumberFragment,orderFragmentInstance, NewNumberFragment.loadingFragmnet};
//            regActivity.removeFragment(myFragments);
//            regActivity.replaceFragmnet(new NewNumberFragment(),R.id.frameLayout,true);
//            NewNumberFragment.isTimerActive=false;

    }

    @Override
    public void initUI() {
        super.initUI();
        regActivity.showHeaderLayout();
        regActivity.setHeaderTitle("");
        orderFragmentInstance = this;
        nextBtn = (Button) view.findViewById(R.id.nextBtn);
        paidRBtn = (RadioButton) view.findViewById(R.id.paidRbtn);
        freeRBtn = (RadioButton) view.findViewById(R.id.freeBtn);
        paidRBtn.setChecked(true);
        orderTypeStr = "1";
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
//        progressID.setVisibility(View.GONE);
        regActivity.onBackPressed();
        try {
            if (responseModel != null && responseModel.getResultObj() != null) {
                finalizeRegisterationRequestOutput = (FinalizeRegisterationRequestOutput) responseModel.getResultObj();
                if (finalizeRegisterationRequestOutput != null) {

                    if (finalizeRegisterationRequestOutput.getErrorCode() != null) {
                        onFinalizeRegisterationError();
                    } else {
                        onFinalizeRegisterationSucess();
                    }
                } else {
                    onFinalizeRegisterationError();
                }
            } else {
                onFinalizeRegisterationError();
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

    }

    private void onFinalizeRegisterationError() {
//        showErrorDalioge(finalizeRegisterationRequestOutput.getErrorMsgEn());
        CommonMethods.logFirebaseEvent(mFirebaseAnalytics, ConstantUtils.TAGGING_onboarding_error_cod_order_fail);
        if (finalizeRegisterationRequestOutput != null && finalizeRegisterationRequestOutput.getErrorMsgEn() != null) {
            if (currentLanguage.equals("en")) {
                showErrorFragment(finalizeRegisterationRequestOutput.getErrorMsgEn());
            } else {
                showErrorFragment(finalizeRegisterationRequestOutput.getErrorMsgAr());
            }
        } else {
            showErrorFragment(getString(R.string.error_));
        }
    }

    private void showErrorFragment(String error) {
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, error);
        ErrorFragment errorFragment = new ErrorFragment();
        errorFragment.setArguments(bundle);
        regActivity.replaceFragmnet(errorFragment, R.id.frameLayout, true);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
//            CommonMethods.logFirebaseEvent(mFirebaseAnalytics, ConstantUtils.TAGGING_onboarding_error_cod_order_fail);
        onFinalizeRegisterationError();
        CommonMethods.logFirebaseEvent(mFirebaseAnalytics, ConstantUtils.TAGGING_onboarding_error_cod_order_fail);

    }

    private void onFinalizeRegisterationSucess() {
        logEvent(AdjustEvents.ORDER_COMPLETE_COD, new Bundle());
        logEvent(AdjustEvents.ORDER_COMPLETE, new Bundle());
        CommonMethods.logCompletedRegistrationEvent(ConstantUtils.ACCOUNT_NUMBER);
        CommonMethods.logFirebaseEvent(mFirebaseAnalytics, ConstantUtils.  TAGGING_SUCCESSFUL_ORDER);
        if (finalizeRegisterationRequestOutput != null) {
            if (finalizeRegisterationRequestOutput.getAdditionalInfo() != null && finalizeRegisterationRequestOutput.getAdditionalInfo().size() > 0) {

                for (int i = 0; i < finalizeRegisterationRequestOutput.getAdditionalInfo().size(); i++) {
                    if (finalizeRegisterationRequestOutput.getAdditionalInfo().get(i).getName().equalsIgnoreCase("OrderId")
                            && finalizeRegisterationRequestOutput.getAdditionalInfo().get(i).getValue() != null &&
                            finalizeRegisterationRequestOutput.getAdditionalInfo().get(i).getValue().length() > 0) {
                        sharedPrefrencesManger.setTransactionID(finalizeRegisterationRequestOutput.getAdditionalInfo().get(i).getValue());

                        break;

                    }
                }

                if (finalizeRegisterationRequestOutput.getRegistrationId() != null) {
                    sharedPrefrencesManger.setRegisterationID(finalizeRegisterationRequestOutput.getRegistrationId());
                    PaymentSucessfulllyFragment paymentSucessfulllyFragment = new PaymentSucessfulllyFragment();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("isCOD", true);
                    paymentSucessfulllyFragment.setArguments(bundle);
                    sharedPrefrencesManger.setLastStatus("UPL");
                    if (!NewNumberFragment.msisdnNumber.equalsIgnoreCase("-1")) {
                        sharedPrefrencesManger.setMobileNo(NewNumberFragment.msisdnNumber);
                    } else {
                        sharedPrefrencesManger.setMobileNo(NumberTransferFragment.mobileNoTxt);
                    }

                    regActivity.replaceFragmnet(paymentSucessfulllyFragment, R.id.frameLayout, true);
                } else {
                    CommonMethods.logFirebaseEvent(mFirebaseAnalytics, ConstantUtils.TAGGING_onboarding_error_cod_order_fail);
                }


            }
        }
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        regActivity.addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        finalizeRegisterationRequestInput = new FinalizeRegisterationRequestInput();
        finalizeRegisterationRequestInput.setLang(currentLanguage);
        finalizeRegisterationRequestInput.setAppVersion(appVersion);
        finalizeRegisterationRequestInput.setOsVersion(osVersion);
        finalizeRegisterationRequestInput.setToken(sharedPrefrencesManger.getToken());
        finalizeRegisterationRequestInput.setChannel(channel);
        finalizeRegisterationRequestInput.setDeviceId(deviceId);
        finalizeRegisterationRequestInput.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        if (!NewNumberFragment.msisdnNumber.equalsIgnoreCase("-1")) {
            finalizeRegisterationRequestInput.setMsisdn(NewNumberFragment.msisdnNumber);
        } else {
            finalizeRegisterationRequestInput.setMsisdn(NumberTransferFragment.mobileNoTxt);
        }
        finalizeRegisterationRequestInput.setAmount(String.valueOf(OrderSummaryFragment.totalAmount));
        finalizeRegisterationRequestInput.setShippingMethod(OrderFragment.shippingMethodStr);
        if (!NewNumberFragment.msisdnNumber.equalsIgnoreCase("-1")) {
            finalizeRegisterationRequestInput.setOrderType("registrationOrder");//in migration the value will be migrationOrder
        } else {
            finalizeRegisterationRequestInput.setOrderType("migrationOrder");//in migration the value will be migrationOrder
        }
        if (!NewNumberFragment.msisdnNumber.equalsIgnoreCase("-1")) {
            finalizeRegisterationRequestInput.setEmiratesId(EmiratesIDVerificationFragment.emiratesID);
        } else {
            finalizeRegisterationRequestInput.setEmiratesId(NameDOBFragment.emiratedID);
        }
        finalizeRegisterationRequestInput.setPackageType(OrderFragment.orderTypeStr);

        if (PromoCodeVerificationSuccessfulFragment.promoCodeType == 1) {
            finalizeRegisterationRequestInput.setPackageType("4");

        } else if (PromoCodeVerificationSuccessfulFragment.promoCodeType == 2) {
            finalizeRegisterationRequestInput.setPackageType("3");
        }
        finalizeRegisterationRequestInput.setPaymentMethod("CashOnDelivery");
        DeliveryInfo deliveryInfo = new DeliveryInfo();
        deliveryInfo.setEmirate(DeliveryDetailsAreaFragment.emirateStr);
        deliveryInfo.setCityCode(DeliveryDetailsAreaFragment.cityCodeStr);
        deliveryInfo.setArea(DeliveryDetailsAreaFragment.areaStr);
        deliveryInfo.setContactNumber(DeliveryDetailsAreaFragment.contactNoStr);
        deliveryInfo.setAddressLine1(DeliveryDetailsAddress.addressLine2);
        deliveryInfo.setAddressLine2(DeliveryDetailsAddress.addressLine2);
        finalizeRegisterationRequestInput.setDeliveryInfo(deliveryInfo);
        if (OrderFragment.promoCode != null && OrderFragment.promoCode.length() > 0) {
            finalizeRegisterationRequestInput.setPromoCode(OrderFragment.promoCode);
        }
        new FinalizeRegisteraionRequestService(this, finalizeRegisterationRequestInput);
    }

    void setListeners() {
        if (sharedPrefrencesManger.getMembershipPrice().equalsIgnoreCase("0")) {
            paidRBtn.setEnabled(false);
            freeRBtn.setEnabled(false);
            paidRBtn.setChecked(false);
            freeRBtn.setChecked(true);
        }
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()) {
                    logEvent(AdjustEvents.PAYMENT_SELECTION_COMPLETE, new Bundle());
                    if (paidRBtn.isChecked()) {
                        orderTypeStr = "1";
                        CommonMethods.logFirebaseEvent(mFirebaseAnalytics, ConstantUtils.TAGGING_onboarding_credit_card);
                        logEvent(AdjustEvents.ADJUST_CC, new Bundle());
//                        Intent intent = new Intent(getActivity(), PaymentActivity.class);
//                        startActivity(intent);
                    }
                    if (freeRBtn.isChecked()) {

                        orderTypeStr = "2";
                        CommonMethods.logFirebaseEvent(mFirebaseAnalytics, ConstantUtils.TAGGING_CASH_ON_DELIVERY_SELECTED);
                        logEvent(AdjustEvents.ADJUST_COD, new Bundle());

                    }
                    regActivity.replaceFragmnet(new DeliveryDetailsAreaFragment(), R.id.frameLayout, true);
//                        onConsumeService();
//                        PaymentSucessfulllyFragment paymentSucessfulllyFragment = new PaymentSucessfulllyFragment();
//                        Bundle bundle = new Bundle();
//                        bundle.putBoolean("isCOD",true);
//                        paymentSucessfulllyFragment.setArguments(bundle);
//                        sharedPrefrencesManger.setLastStatus("UPL");
//                        regActivity.replaceFragmnet(paymentSucessfulllyFragment, R.id.frameLayout, true);


                }
            }
        });

        paidRBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!paidRBtn.isChecked())
                    paidRBtn.setChecked(true);
                freeRBtn.setChecked(false);

            }
        });
        freeRBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!freeRBtn.isChecked())
                    freeRBtn.setChecked(true);
                paidRBtn.setChecked(false);
            }
        });
    }


    private boolean isValid() {
        if (paidRBtn.isChecked() || freeRBtn.isChecked())
            return true;
        else
            return false;
    }


}
