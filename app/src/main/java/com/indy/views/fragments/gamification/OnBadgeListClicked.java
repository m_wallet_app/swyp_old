package com.indy.views.fragments.gamification;

import com.indy.views.fragments.gamification.models.getbadgeslist.Badge;

/**
 * Created by mobile on 04/09/2017.
 */

public interface OnBadgeListClicked {

    void onBadgeClicked(Badge badge,int position);
}
