package com.indy.views.fragments.rewards;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.indy.R;
import com.indy.adapters.AvilableRewardsAdapter;
import com.indy.adapters.RewardsListItemPagerAdapter;
import com.indy.models.rewards.AvilableRewardsoutput;
import com.indy.models.rewards.RewardsList;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.models.utils.MasterInputResponse;
import com.indy.services.AvilableRewardsService;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.MasterFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by emad on 9/27/16.
 */

public class AvilableRewardsFragment extends MasterFragment {
    private View view;
    private RecyclerView rewardsRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private MasterInputResponse masterInputResponse;
    private ProgressBar progressBar;
    private AvilableRewardsoutput avilableRewardsoutput;
    private AvilableRewardsAdapter avilableRewardsAdapter;
    ViewPager mViewPager;
    List<RewardsList> rewardsList;
    List<AvailableRewardsItemFragment> availableRewardsItemFragmentList;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_avilable_rewards, container, false);
        initUI();
        onConsumeService();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
        rewardsRecyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        rewardsRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        rewardsRecyclerView.setLayoutManager(mLayoutManager);
        progressBar = (ProgressBar) view.findViewById(R.id.progressID);
        mViewPager = (ViewPager) view.findViewById(R.id.pager);

    }

    private void initPager(){
        final RewardsListItemPagerAdapter adapter = new RewardsListItemPagerAdapter(
                getActivity().getSupportFragmentManager(), availableRewardsItemFragmentList);
        mViewPager.setAdapter(adapter);
        mViewPager.setCurrentItem(0, true);
        mViewPager.setSelected(true);
        mViewPager.setFocusable(true);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                Toast.makeText(getActivity(), "Page scrolled to " +position , Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        progressBar.setVisibility(View.VISIBLE);
        masterInputResponse = new MasterInputResponse();
        masterInputResponse.setAppVersion(appVersion);
        masterInputResponse.setToken(token);
        masterInputResponse.setOsVersion(osVersion);
        masterInputResponse.setChannel(channel);
        masterInputResponse.setDeviceId(deviceId);
        masterInputResponse.setMsisdn(sharedPrefrencesManger.getMobileNo());
        masterInputResponse.setAuthToken(authToken);
        new AvilableRewardsService(this, masterInputResponse);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            progressBar.setVisibility(View.GONE);
            if(responseModel!=null && responseModel.getResultObj()!=null) {
                avilableRewardsoutput = (AvilableRewardsoutput) responseModel.getResultObj();
                if (avilableRewardsoutput != null)
                    if (avilableRewardsoutput.getErrorCode() != null)
                        onGetRewardsFailure();
                    else
                        onGetRewardsSucess();
            }
        }catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        progressBar.setVisibility(View.GONE);
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    private void onGetRewardsSucess() {
        if (avilableRewardsoutput.getRewardList() != null)
            if (avilableRewardsoutput.getRewardList().size() > 0) {
                rewardsList = avilableRewardsoutput.getRewardList();
                prepareFragmentsList();
                initPager();
//                avilableRewardsAdapter = new AvilableRewardsAdapter(rewardsList, getContext());
//                rewardsRecyclerView.setAdapter(avilableRewardsAdapter);
            }
    }

    private void onGetRewardsFailure() {
        Fragment errorFragmnet = new ErrorFragment();
        Bundle bundle = new Bundle();
        if (currentLanguage != null) {
            if (currentLanguage.equalsIgnoreCase("en"))
                bundle.putString(ConstantUtils.errorString, avilableRewardsoutput.getErrorMsgEn());
            else
                bundle.putString(ConstantUtils.errorString, avilableRewardsoutput.getErrorMsgAr());

        }
        errorFragmnet.setArguments(bundle);
        regActivity.replaceFragmnet(errorFragmnet, R.id.frameLayout, true);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        try {
            progressBar.setVisibility(View.GONE);
        }catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

    }


    private void prepareFragmentsList(){

        availableRewardsItemFragmentList = new ArrayList<AvailableRewardsItemFragment>();
        for(int i=0;i<rewardsList.size();i++) {
            RewardsList rewardsListTemp = rewardsList.get(i);
            AvailableRewardsItemFragment item = new AvailableRewardsItemFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable(ConstantUtils.item_reward_view_pager,rewardsListTemp);
            bundle.putInt("position",i+1);
            bundle.putInt("total",rewardsList.size());
            item.setArguments(bundle);
            availableRewardsItemFragmentList.add(item);
        }

    }
}
