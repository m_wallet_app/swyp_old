package com.indy.views.fragments.usage;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.helpers.ValidationHelper;
import com.indy.models.createnewaccount.CreateNewAccountInput;
import com.indy.models.createnewaccount.CreateNewAccountOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.services.SetUserCredinalsService;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.ChangePasswordActivity;
import com.indy.views.activites.RegisterationActivity;
import com.indy.views.activites.SwpeMainActivity;
import com.indy.views.activites.UserProfileActivity;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.indy.views.fragments.utils.MasterFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class SetNewPasswordSettingsFragment extends MasterFragment {
    private CreateNewAccountInput changePasswordInput;
    private EditText New_password_user_profile, ReType_password_user_profile;
    private TextInputLayout ReType_password_text_input_layout, New_password_text_input_layout;
    View view;
    private Button ConformSetNewPasswordbtn;
    private CreateNewAccountOutput changePasswordOutput;
    private Button bt_okConfirmation;
    LinearLayout ll_main, ll_confirmation;

    public SetNewPasswordSettingsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_set_new_password_settings, container, false);
        initUI();
        setListeners();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();

        TextView headerTxt = (TextView) view.findViewById(R.id.titleTxt);
        New_password_user_profile = (EditText) view.findViewById(R.id.New_password_user_profile);
        ReType_password_user_profile = (EditText) view.findViewById(R.id.ReType_password_user_profile);
        ReType_password_text_input_layout = (TextInputLayout) view.findViewById(R.id.ReType_password_text_input_layout);
        New_password_text_input_layout = (TextInputLayout) view.findViewById(R.id.New_password_text_input_layout);
        ConformSetNewPasswordbtn = (Button) view.findViewById(R.id.ConformSetNewPasswordbtn);
        headerTxt.setText(getString(R.string.forgot_password_spaced));
        ConformSetNewPasswordbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid())
                    onConsumeService();
            }
        });
        disableContinueBtn();
        ll_confirmation = (LinearLayout) view.findViewById(R.id.ll_confirmation_message);
        ll_confirmation.setVisibility(View.GONE);
        bt_okConfirmation = (Button) view.findViewById(R.id.okBtn);
        bt_okConfirmation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //
//                Intent intent = new Intent(getActivity(), RegisterationActivity.class);
//                intent.putExtra("fragIndex", "7");
//                startActivity(intent);
                onLogin();
            }
        });
        ll_main = (LinearLayout) view.findViewById(R.id.ll_main_content_view);
        ll_main.setVisibility(View.VISIBLE);
        ll_confirmation.setVisibility(View.GONE);

    }

    private void setListeners() {
        New_password_user_profile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (New_password_user_profile.getText().toString().length() > 0 && ReType_password_user_profile.getText().toString().length() > 0) {
                    enableContinueBtn();
                } else {
                    disableContinueBtn();
                }
            }
        });
        ReType_password_user_profile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (New_password_user_profile.getText().toString().length() > 0 && ReType_password_user_profile.getText().toString().length() > 0) {
                    enableContinueBtn();
                } else {
                    disableContinueBtn();
                }
            }
        });
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        ((ChangePasswordActivity) getActivity()).addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        changePasswordInput = new CreateNewAccountInput();
        changePasswordInput.setAppVersion(appVersion);
        changePasswordInput.setToken(token);
        changePasswordInput.setOsVersion(osVersion);
        changePasswordInput.setChannel(channel);
        changePasswordInput.setDeviceId(deviceId);
        changePasswordInput.setMsisdn(ChangePasswordCodeFragment.mobileNO);
        String newPass = ValidationHelper.getMd5(New_password_user_profile.getText().toString().trim());
        String confirmPass = ValidationHelper.getMd5(ReType_password_user_profile.getText().toString().trim());
        changePasswordInput.setPassword(newPass.toUpperCase());
        changePasswordInput.setPasswordConfirmation(confirmPass.toUpperCase());
        changePasswordInput.setAuthToken(authToken);
//        changePasswordInput.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        new SetUserCredinalsService(this, changePasswordInput);
    }


    private boolean isValid() {
        boolean isValid = true;
        if (New_password_user_profile.getText().toString().trim().length() == 0) {
            setTextInputLayoutError(New_password_text_input_layout, getString(R.string.please_enter_password));
            isValid = false;
        } else {
            removeTextInputLayoutError(New_password_text_input_layout);
        }

        if (ReType_password_user_profile.getText().toString().trim().length() == 0) {
            setTextInputLayoutError(ReType_password_text_input_layout, getString(R.string.please_confirm_password));
            isValid = false;
        } else {
            removeTextInputLayoutError(ReType_password_text_input_layout);
        }

        if (isValid && (!ValidationHelper.isValidPassword(New_password_user_profile.getText().toString().trim()) || New_password_user_profile.getText().toString().trim().length() < 6)) {
            setTextInputLayoutError(New_password_text_input_layout, getString(R.string.password_criteria_not_matched));
            isValid = false;
        } else if (isValid) {
            removeTextInputLayoutError(New_password_text_input_layout);
        }

        if (isValid) {
            if (!New_password_user_profile.getText().toString().trim().equals(ReType_password_user_profile.getText().toString().trim())) {
                //error fragment handling here
                setTextInputLayoutError(ReType_password_text_input_layout, getString(R.string.password_didnot_match));

//                ErrorFragment errorFragment = new ErrorFragment();
//                Bundle bundle = new Bundle();
//                isValid = false;
//                bundle.putString(ConstantUtils.errorString, getString(R.string.password_didnot_match));
//                errorFragment.setArguments(bundle);
//                ((ChangePasswordActivity) getActivity()).replaceFragmnet(errorFragment, R.id.frameLayout, true);
            } else {
                removeTextInputLayoutError(ReType_password_text_input_layout);
            }
        }

        return isValid;
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            if (isVisible()) {
                if(responseModel!=null && responseModel.getResultObj()!=null) {
                    ((ChangePasswordActivity) getActivity()).onBackPressed();
                    changePasswordOutput = (CreateNewAccountOutput) responseModel.getResultObj();
                    if (changePasswordOutput != null) {
                        if (changePasswordOutput.getErrorCode() != null) {
                            if (currentLanguage.equalsIgnoreCase("en"))
                                showErrorFragment(changePasswordOutput.getErrorMsgEn());
                            else
                                showErrorFragment(changePasswordOutput.getErrorMsgAr());
                        } else {
//                    sharedPrefrencesManger.setPassword(New_password_user_profile.getText().toString().trim());
                            showConfirmationView();
                        }
                    }
                }
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    public void showConfirmationView() {
        ll_confirmation.setVisibility(View.VISIBLE);
        ll_main.setVisibility(View.GONE);
    }

    public void onLogin() {
        getActivity().finish();
        if (UserProfileActivity.userProfileActivityInstance != null)
            UserProfileActivity.userProfileActivityInstance.finish();
        if (SwpeMainActivity.swpeMainActivityInstance != null)
            SwpeMainActivity.swpeMainActivityInstance.finish();
        Intent intent = new Intent(getActivity(), RegisterationActivity.class);
        intent.putExtra("fragIndex", "7");
        startActivity(intent);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {

        super.onErrorListener(responseModel);
    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    private void showErrorFragment(String error) {
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, error);
        errorFragment.setArguments(bundle);
        ((ChangePasswordActivity) getActivity()).replaceFragmnet(errorFragment, R.id.frameLayout, true);
    }

    private void disableContinueBtn() {
        ConformSetNewPasswordbtn.setAlpha(0.5f);
        ConformSetNewPasswordbtn.setEnabled(false);
    }

    private void enableContinueBtn() {
        ConformSetNewPasswordbtn.setAlpha(1.0f);
        ConformSetNewPasswordbtn.setEnabled(true);
    }
}
