package com.indy.views.fragments.buynewline.exisitingnumber;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.LogApplicationFlowService;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.NearestStoreActivity;
import com.indy.views.fragments.buynewline.NewNumberFragment;
import com.indy.views.fragments.utils.MasterFragment;

import static com.indy.views.activites.RegisterationActivity.logEventInputModel;

/**
 * Created by emad on 9/25/16.
 */

public class InvalidEIDFrgament extends MasterFragment {
    private View view;
    private Button reviewDetailsBtn, locateNearestShop, newNumber;
    private TextView tv_mobileNumber;
    private String mobileNO = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_invalid_eid, container, false);
        initUI();
        setListeners();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
        regActivity.setHeaderTitle("");
        regActivity.showHeaderLayout();
        regActivity.hideBackBtn();
        newNumber = (Button) view.findViewById(R.id.newNoID);
        reviewDetailsBtn = (Button) view.findViewById(R.id.reviewd_detailsBtn);
        locateNearestShop = (Button) view.findViewById(R.id.locate_nearest_ID);
        tv_mobileNumber = (TextView) view.findViewById(R.id.mobileNo);
//        ((RegisterationActivity) getActivity()).hideBackBtn();
        if (getArguments() != null && getArguments().getString(ConstantUtils.mobileNo) != null) {
            mobileNO = getArguments().getString(ConstantUtils.mobileNo);
            tv_mobileNumber.setText(mobileNO);
        }

    }


    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
    }

    private void setListeners() {

        reviewDetailsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard();
                Bundle bundle2 = new Bundle();

                bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_review_details);
                bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_review_details);
                mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_review_details, bundle2);


                ReviewDetailsFragment reviewDetailsFragment = new ReviewDetailsFragment();
                Bundle bundle = new Bundle();
                if (getArguments() != null && getArguments().getString(ConstantUtils.eidNO) != null)
                    bundle.putString(ConstantUtils.eidNO, getArguments().getString(ConstantUtils.eidNO));
                reviewDetailsFragment.setArguments(bundle);
                if (getArguments() != null && getArguments().getString(ConstantUtils.mobileNo) != null)
                    bundle.putString(ConstantUtils.mobileNo, getArguments().getString(ConstantUtils.mobileNo));
                reviewDetailsFragment.setArguments(bundle);
                logEvent(1);
                regActivity.replaceFragmnet(reviewDetailsFragment, R.id.frameLayout, true);

            }
        });
        locateNearestShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logEvent(2);
                Intent intent = new Intent(getActivity(), NearestStoreActivity.class);
                startActivity(intent);
            }
        });

        newNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logEvent(3);
                regActivity.replaceFragmnet(new NewNumberFragment(), R.id.frameLayout, true);
            }
        });
    }

    private void logEvent(int index) {

      logEventInputModel.setScreenId(ConstantUtils.SCREEN_ID_3031);

        switch (index) {
            case 1:
                logEventInputModel.setEidFailureReviewDetails(reviewDetailsBtn.getText().toString());
                break;
            case 2:
                logEventInputModel.setEidFailureLocateShop(locateNearestShop.getText().toString());
                break;
            case 3:
                logEventInputModel.setEidFailureNewNumber(newNumber.getText().toString());
                break;
        }
        new LogApplicationFlowService(this, logEventInputModel);
    }

}
