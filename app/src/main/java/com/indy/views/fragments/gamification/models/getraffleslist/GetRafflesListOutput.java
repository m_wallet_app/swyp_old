package com.indy.views.fragments.gamification.models.getraffleslist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

import java.util.List;

/**
 * Created by Tohamy on 10/4/2017.
 */

public class GetRafflesListOutput extends MasterErrorResponse {

    @SerializedName("raffles")
    @Expose
    private List<Raffle> raffles = null;

    public List<Raffle> getRaffles() {
        return raffles;
    }

    public void setRaffles(List<Raffle> raffles) {
        this.raffles = raffles;
    }

}

