package com.indy.views.fragments.gamification.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.BaseServiceManger;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.gamification.models.getraffleslist.GetRafflesListInput;
import com.indy.views.fragments.gamification.models.getraffleslist.GetRafflesListOutput;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Tohamy on 10/4/2017.
 */

public class GetRafflesListService extends BaseServiceManger {

    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private GetRafflesListInput getRafflesListInput;

    public GetRafflesListService(BaseInterface baseInterface, GetRafflesListInput getRafflesListInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.getRafflesListInput = getRafflesListInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<GetRafflesListOutput> call = apiService.getRafflesList(getRafflesListInput);
        call.enqueue(new Callback<GetRafflesListOutput>() {
            @Override
            public void onResponse(Call<GetRafflesListOutput> call, Response<GetRafflesListOutput> response) {
                mBaseResponseModel.setServiceType(ServiceUtils.GET_RAFFLES_LIST);
                mBaseResponseModel.setResultObj(response.body());

                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
//                    if (response.body().getErrorMsgEn() != null) {
//                        String urlForTag = call.request().url().toString();
//                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
//                        urlForTag = urlForTag.replace("/", "_");
//                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
//                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<GetRafflesListOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.GET_RAFFLES_LIST);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}

