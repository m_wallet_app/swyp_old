package com.indy.views.fragments.gamification.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.BaseServiceManger;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GamificationBaseInputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.SuccessOutputResponse;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**

 */
public class RegisterInviteFriendEventService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private GamificationBaseInputModel gamificationBaseInputModel;


    public RegisterInviteFriendEventService(BaseInterface baseInterface, GamificationBaseInputModel gamificationBaseInputModel) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.gamificationBaseInputModel = gamificationBaseInputModel;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<SuccessOutputResponse> call = apiService.registerInviteFriendEvent(gamificationBaseInputModel);
        call.enqueue(new Callback<SuccessOutputResponse>() {
            @Override
            public void onResponse(Call<SuccessOutputResponse> call, Response<SuccessOutputResponse> response) {
                mBaseResponseModel.setServiceType(ServiceUtils.REGISTER_EVENT_INVITE_FRIEND);
                mBaseResponseModel.setResultObj(response.body());

                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (response.body().getErrorMsgEn() != null) {
//                        String urlForTag = call.request().url().toString();
//                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
//                        urlForTag = urlForTag.replace("/", "_");
//                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<SuccessOutputResponse> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.REGISTER_EVENT_INVITE_FRIEND);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
