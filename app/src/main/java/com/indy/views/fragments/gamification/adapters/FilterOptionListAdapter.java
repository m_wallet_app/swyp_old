package com.indy.views.fragments.gamification.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.LinearLayout;

import com.indy.R;
import com.indy.utils.ConstantUtils;
import com.indy.utils.SharedPrefrencesManger;
import com.indy.views.fragments.gamification.OnSortSelect;
import com.indy.views.fragments.gamification.models.eventfilters.EventFilterOptions;

import java.util.List;

public class FilterOptionListAdapter extends RecyclerView.Adapter<FilterOptionListAdapter.ViewHolder> {
    private final OnSortSelect onSortSelect;
    private List<EventFilterOptions> filterList;
    private Context mContext;
    private SharedPrefrencesManger sharedPrefrencesManger;
    private int selectedIndex = -1;

    public FilterOptionListAdapter(List<EventFilterOptions> filterList, Context context) {
        this.filterList = filterList;
        this.mContext = context;
        this.sharedPrefrencesManger = new SharedPrefrencesManger(context);
        try {
            onSortSelect = (OnSortSelect) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(mContext.toString()
                    + " must implement OnSortSelect");
        }
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.order_item, viewGroup, false);
        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        final EventFilterOptions eventFilter = filterList.get(position);
        if (eventFilter != null) {
            if (sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)) {
                viewHolder.checkedText.setText(eventFilter.getLabel());
            } else {
                viewHolder.checkedText.setText(eventFilter.getLabel());
            }
            viewHolder.checkedText.setChecked(selectedIndex == viewHolder.getAdapterPosition());
            viewHolder.layout
                    .setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            viewHolder.checkedText.toggle();
                            if (viewHolder.checkedText.isChecked()) {
                                selectedIndex = viewHolder.getAdapterPosition();
                                if (onSortSelect != null) {
                                    onSortSelect.selectSort(eventFilter.getValue(), selectedIndex);
                                }
                            } else {
                                selectedIndex = -1;
                            }
                            notifyDataSetChanged();
                        }
                    });
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return filterList.size();
    }

    public void reset() {
        selectedIndex = -1;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout layout;
        public CheckedTextView checkedText;

        public ViewHolder(View itemView) {
            super(itemView);
            checkedText = (CheckedTextView) itemView.findViewById(R.id.checkedText);
            checkedText.setTextColor(ContextCompat.getColorStateList(mContext, R.color.sort_by_checked_text_back));
            layout = (LinearLayout) itemView.findViewById(R.id.layout);
        }
    }
}
