package com.indy.views.fragments.gamification.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mobile on 10/09/2017.
 */

public class EventPhoto implements Parcelable{



    @SerializedName("entryId")
    @Expose
    private String entryId;
    @SerializedName("indyEventId")
    @Expose
    private String indyEventId;
    @SerializedName("imageUrl")
    @Expose
    private String imageURL;
    @SerializedName("votes")
    @Expose
    private int votes;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("contestantName")
    @Expose
    private String contestantName;
    @SerializedName("liked")
    @Expose
    private boolean selected;
    ///////////////////

    private boolean isOwnedByUser;
    public static final Creator<EventPhoto> CREATOR = new Creator<EventPhoto>() {
        @Override
        public EventPhoto createFromParcel(Parcel in) {
            return new EventPhoto(in);
        }

        @Override
        public EventPhoto[] newArray(int size) {
            return new EventPhoto[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public int getVotes() {
        return votes;
    }

    public void setVotes(int votes) {
        this.votes = votes;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(entryId);
        parcel.writeString(imageURL);
        parcel.writeInt(votes);
        parcel.writeString(contestantName);
        parcel.writeString(description);
        parcel.writeString(indyEventId);
//        parcel.writeByte((byte) (selected ? 1 : 0));
//        parcel.writeByte((byte) (isOwnedByUser ? 1 : 0));
    }
    public EventPhoto(){

    }
    private EventPhoto(Parcel in){
        this.entryId = in.readString();
        this.imageURL = in.readString();
        this.votes = in.readInt();
        this.contestantName = in.readString();
        this.description = in.readString();
        this.indyEventId = in.readString();
//        this.selected = in.readByte() != 0;
//        this.isOwnedByUser = in.readByte() != 0;

    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getEntryId() {
        return entryId;
    }

    public void setEntryId(String entryId) {
        this.entryId = entryId;
    }

    public String getIndyEventId() {
        return indyEventId;
    }

    public void setIndyEventId(String indyEventId) {
        this.indyEventId = indyEventId;
    }

    public String getContestantName() {
        return contestantName;
    }

    public void setContestantName(String contestantName) {
        this.contestantName = contestantName;
    }

    public boolean isOwnedByUser() {
        return isOwnedByUser;
    }

    public void setOwnedByUser(boolean ownedByUser) {
        isOwnedByUser = ownedByUser;
    }
}
