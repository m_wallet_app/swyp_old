package com.indy.views.fragments.gamification.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.BaseServiceManger;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.gamification.models.UpdateUserNotificationSettingOutputResponse;
import com.indy.views.fragments.gamification.models.UpdateUserNotificationSettingsInput;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**

 */
public class UpdateUserNotificationSettingsService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private UpdateUserNotificationSettingsInput updateUserNotificationSettingsInput;


    public UpdateUserNotificationSettingsService(BaseInterface baseInterface, UpdateUserNotificationSettingsInput updateUserNotificationSettingsInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.updateUserNotificationSettingsInput = updateUserNotificationSettingsInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<UpdateUserNotificationSettingOutputResponse> call = apiService.updateUserNotificationSetting(updateUserNotificationSettingsInput);
        call.enqueue(new Callback<UpdateUserNotificationSettingOutputResponse>() {
            @Override
            public void onResponse(Call<UpdateUserNotificationSettingOutputResponse> call, Response<UpdateUserNotificationSettingOutputResponse> response) {
                mBaseResponseModel.setServiceType(ServiceUtils.UPDATE_USER_NOTIFICATION_SETTING);
                mBaseResponseModel.setResultObj(response.body());

                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (response.body().getErrorMsgEn() != null) {
//                        String urlForTag = call.request().url().toString();
//                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
//                        urlForTag = urlForTag.replace("/", "_");
//                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<UpdateUserNotificationSettingOutputResponse> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.UPDATE_USER_NOTIFICATION_SETTING);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
