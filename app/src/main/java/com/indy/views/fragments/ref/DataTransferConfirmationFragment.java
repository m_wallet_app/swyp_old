//package com.indy.views.fragments.ref;
//
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.TextView;
//
//import com.indy.R;
//import com.indy.dbt.activities.DBTPagerActivity;
//import com.indy.dbt.listeners.OnDataTransferConfirmedListener;
//import com.indy.views.fragments.utils.MasterFragment;
//
//public class DataTransferConfirmationFragment extends MasterFragment implements View.OnClickListener {
//
//    private View baseView;
//    private TextView tv_title;
//    private TextView tv_detail;
//    private TextView btn_cancel;
//    private Button btn_ok;
//    private OnDataTransferConfirmedListener onDataTransferConfirmedListener;
//    private String msisdn;
//    private String dataMB;
//    private String amountCharges;
//    public static final String KEY_MSISDN = "toMsisdn";
//    public static final String KEY_DATA_MB = "dataMB";
//    public static final String KEY_AMOUNT_CHARGES = "amountCharges";
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        baseView = inflater.inflate(R.layout.fragment_add_squad_confirmation, null, false);
//        initUI();
//        return baseView;
//    }
//
//    @Override
//    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//        if (getArguments() != null) {
//            msisdn = getArguments().getString(KEY_MSISDN);
//            dataMB = getArguments().getString(KEY_DATA_MB);
//            amountCharges = getArguments().getString(KEY_AMOUNT_CHARGES);
//            setData();
//        }
//    }
//
//    @Override
//    public void initUI() {
//        super.initUI();
//        tv_title = baseView.findViewById(R.id.tv_title);
//        tv_detail = baseView.findViewById(R.id.tv_warningText);
//        btn_cancel = baseView.findViewById(R.id.btn_cancel);
//        btn_ok = baseView.findViewById(R.id.btn_ok);
//        btn_cancel.setOnClickListener(this);
//        btn_ok.setOnClickListener(this);
//    }
//
//    private void setData() {
//        String detail = getString(R.string.you_are_about_to_transfer) + " " + dataMB +
//                " " + getString(R.string.mb_from_your_balance) + " " + msisdn + ". ";
//
//        if (sharedPrefrencesManger.isVATEnabled()) {
//            detail += "\n\n" + getString(R.string.you_ll_pay_) + " " + amountCharges + " " +
//                    getString(R.string.aed_with_the_transfer_fee);
//        } else {
//            detail += "\n\n" + getString(R.string.you_ll_pay_) + " " + amountCharges + " " +
//                    getString(R.string.aed_with_the_transfer_fee);
//        }
//        tv_title.setText(getString(R.string.send_data_to_friends));
//        tv_detail.setText(detail);
//    }
//
//    @Override
//    public void onClick(View v) {
//        switch (v.getId()) {
//
//            case R.id.btn_cancel:
//                if(onDataTransferConfirmedListener != null){
//                    onDataTransferConfirmedListener.onCancelled();
//                }
//                break;
//
//            case R.id.btn_ok:
//                if(onDataTransferConfirmedListener != null){
//                    onDataTransferConfirmedListener.onConfirmed();
//                }
//                break;
//
//        }
//    }
//
//    public void setOnDataTransferConfirmedListener(OnDataTransferConfirmedListener onDataTransferConfirmedListener) {
//        this.onDataTransferConfirmedListener = onDataTransferConfirmedListener;
//    }
//
//    @Override
//    public void onConsumeService() {
//        super.onConsumeService();
//    }
//}
