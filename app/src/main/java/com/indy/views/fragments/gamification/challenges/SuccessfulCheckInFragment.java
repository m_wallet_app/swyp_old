package com.indy.views.fragments.gamification.challenges;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.indy.R;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.utils.MaterialCheckBox;
import com.indy.views.activites.ShoppingCartActivity;
import com.indy.views.activites.SwpeMainActivity;
import com.indy.views.fragments.gamification.models.serviceInputOutput.CheckinRewardModel;
import com.indy.views.fragments.gamification.models.viewluckydealservice.ViewLuckyDealInput;
import com.indy.views.fragments.gamification.models.viewluckydealservice.ViewLuckyDealOutput;
import com.indy.views.fragments.gamification.services.ViewLuckyDealService;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.MasterFragment;

import static com.indy.R.id.continueBtn;

/**
 * A simple {@link Fragment} subclass.
 */
public class SuccessfulCheckInFragment extends MasterFragment {

    private View rootView;
    private Button shareButton;
    private TextView tv_cancel, tv_title, tv_subTitle, tv_rewards, tv_rewards_count, tv_total_raffle_tickets, tv_dealOldPrice, tv_dealNewPrice, tv_dealName, tv_dealDescription, tv_dealTimer
            ,tv_enAed1, tv_enAed2, tv_arAed1, tv_arAed2;
    private LinearLayout ll_raffle_tickets, ll_checkin_deal,ll_oldPrice,ll_newPrice;
    private CheckinRewardModel checkinRewardModel;
    private ProgressBar progressBar;
    private ViewLuckyDealOutput viewLuckyDealOutput;

    public SuccessfulCheckInFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_checkin_successful, container, false);

        initUI();

        return rootView;
    }

    @Override
    public void initUI() {
        super.initUI();
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressID);
        tv_title = (TextView) rootView.findViewById(R.id.tv_title);
        tv_subTitle = (TextView) rootView.findViewById(R.id.tv_subtitle);
        tv_rewards = (TextView) rootView.findViewById(R.id.tv_rewards_text);
        tv_rewards_count = (TextView) rootView.findViewById(R.id.tv_rewards_count);
        tv_total_raffle_tickets = (TextView) rootView.findViewById(R.id.tv_total_raffle_tickets);
        ll_raffle_tickets = (LinearLayout) rootView.findViewById(R.id.ll_raffle_tickets);
        ll_checkin_deal = (LinearLayout) rootView.findViewById(R.id.ll_checkin_deal);

        tv_dealDescription = (TextView) rootView.findViewById(R.id.tv_checkin_deal_description);
        tv_dealName = (TextView) rootView.findViewById(R.id.tv_checkin_deal_name);
        tv_dealNewPrice = (TextView) rootView.findViewById(R.id.tv_checkin_new_price);
        tv_dealOldPrice = (TextView) rootView.findViewById(R.id.tv_checkin_old_price);
        tv_dealTimer = (TextView) rootView.findViewById(R.id.tv_checkin_deal_timer);

        tv_arAed1 = (TextView) rootView.findViewById(R.id.tv_aed_ar);
        tv_arAed2 = (TextView) rootView.findViewById(R.id.tv_aed_ar_2);
        tv_enAed1 = (TextView) rootView.findViewById(R.id.tv_aed_en);
        tv_enAed2 = (TextView) rootView.findViewById(R.id.tv_aed_en_2);
        ll_oldPrice = (LinearLayout) rootView.findViewById(R.id.ll_old_price);
        ll_newPrice = (LinearLayout) rootView.findViewById(R.id.ll_new_price);
        shareButton = (Button) rootView.findViewById(continueBtn);
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
//                ((UploadEventPhotoActivity) getActivity()).replaceFragmnet(photoDetailFragment,R.id.frameLayout,true);
            }
        });


        tv_cancel = (TextView) rootView.findViewById(R.id.tv_cancel);
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        if (getArguments() != null) {
            Bundle bundle = getArguments();
            if (bundle.getString(ConstantUtils.title_checkin_success) != null) {
                tv_title.setText(bundle.getString(ConstantUtils.title_checkin_success));
            }
            if (bundle.getString(ConstantUtils.subtitle_checkin_success) != null) {
                tv_subTitle.setText(bundle.getString(ConstantUtils.subtitle_checkin_success));
            }

            if (bundle.getString(ConstantUtils.reward_type_checkin_success) != null) {
                setupRewardView(bundle.getString(ConstantUtils.reward_type_checkin_success));
            } else {
                setupRewardView("0");
            }
        }

        MaterialCheckBox materialCheckBox = (MaterialCheckBox) rootView.findViewById(R.id.fl_tick_animation);
        ImageView iv_tick = (ImageView) rootView.findViewById(R.id.iv_tick);
        CommonMethods.drawCircle(materialCheckBox, iv_tick);
    }

    public void setupRewardView(String upRewardView) {
        switch (upRewardView) {

            case "1":
//                if (getArguments().getString(ConstantUtils.raffle_tickets_won) != null) {
                if (getArguments() != null && getArguments().getParcelable(ConstantUtils.lottery_reward) != null) {
                    CheckinRewardModel checkinRewardModel1 = (CheckinRewardModel) getArguments().getParcelable(ConstantUtils.lottery_reward);

                    if (checkinRewardModel1 != null) {
                        if (checkinRewardModel1.getName() != null) {
                            tv_rewards.setText(checkinRewardModel1.getName());
                            tv_rewards.setVisibility(View.VISIBLE);
                        } else {
                            tv_rewards.setVisibility(View.INVISIBLE);
                        }
                        if (checkinRewardModel1.getDescription() != null) {
                            tv_rewards_count.setText(checkinRewardModel1.getDescription());
                        } else {
                            tv_rewards_count.setVisibility(View.INVISIBLE);
                        }
                        if (checkinRewardModel1.getTotalDescription() != null) {
                            tv_total_raffle_tickets.setText(checkinRewardModel1.getTotalDescription());
                        } else {
                            tv_total_raffle_tickets.setVisibility(View.INVISIBLE);
                        }

//                            if (getArguments().getString(ConstantUtils.raffle_tickets_total) != null) {
//                                tv_total_raffle_tickets.setText(getString(R.string.you_now_have) + " "
//                                        + getArguments().getString(ConstantUtils.raffle_tickets_total) + " " + getString(R.string.raffle_tickets));
//                            } else {
//                                tv_total_raffle_tickets.setVisibility(View.INVISIBLE);
//                            }
                    }
                } else {
                    tv_total_raffle_tickets.setVisibility(View.INVISIBLE);
                    tv_rewards_count.setVisibility(View.INVISIBLE);
                    tv_rewards.setVisibility(View.INVISIBLE);
                }
//                }
                ll_checkin_deal.setVisibility(View.GONE);
                ll_raffle_tickets.setVisibility(View.VISIBLE);
                shareButton.setText(getString(R.string.ok));
                tv_cancel.setVisibility(View.INVISIBLE);

                break;

            case "2":
                tv_rewards.setVisibility(View.VISIBLE);
                tv_rewards.setText(getString(R.string.you_found_a_deal));
                shareButton.setText(getString(R.string.get_now));
                tv_cancel.setText(getString(R.string.later));
                ll_checkin_deal.setVisibility(View.VISIBLE);
                ll_raffle_tickets.setVisibility(View.GONE);
                if (getArguments() != null && getArguments().getParcelable(ConstantUtils.lottery_reward) != null) {
                    checkinRewardModel = (CheckinRewardModel) getArguments().getParcelable(ConstantUtils.lottery_reward);
                    if (checkinRewardModel.getPreviousPrice() == 0) {
                        ll_newPrice.setVisibility(View.INVISIBLE);
                        rootView.findViewById(R.id.line).setVisibility(View.INVISIBLE);
                        if (sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)) {
                            tv_dealOldPrice.setText(checkinRewardModel.getCurrentPrice()+"");// + " " + getString(R.string.aed));
                            tv_enAed1.setVisibility(View.VISIBLE);
                            tv_arAed1.setVisibility(View.GONE);
                        } else {
                            tv_dealOldPrice.setText(checkinRewardModel.getCurrentPrice()+"");
                            tv_enAed1.setVisibility(View.GONE);
                            tv_arAed1.setVisibility(View.VISIBLE);
                        }
                    } else {


                        if (sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)) {
//                            tv_dealOldPrice.setText(checkinRewardModel.getPreviousPrice() + " " + getString(R.string.aed));
                            tv_dealOldPrice.setText(checkinRewardModel.getPreviousPrice());// + " " + getString(R.string.aed));
                            tv_enAed1.setVisibility(View.VISIBLE);
                            tv_arAed1.setVisibility(View.GONE);
                        } else {
//                            tv_dealOldPrice.setText(getString(R.string.aed) + " " + checkinRewardModel.getPreviousPrice());
                            tv_dealOldPrice.setText(checkinRewardModel.getPreviousPrice());
                            tv_enAed1.setVisibility(View.GONE);
                            tv_arAed1.setVisibility(View.VISIBLE);
                        }
                        rootView.findViewById(R.id.line).setVisibility(View.VISIBLE);
                        if (checkinRewardModel.getCurrentPrice() == 0) {
                            tv_dealOldPrice.setText(getString(R.string.free));
                        } else {
                            if (sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)) {
                                tv_dealNewPrice.setText(checkinRewardModel.getCurrentPrice());// + " " + getString(R.string.aed));
                                tv_enAed2.setVisibility(View.VISIBLE);
                                tv_arAed2.setVisibility(View.GONE);
                            } else {
                                tv_dealNewPrice.setText(checkinRewardModel.getCurrentPrice());
                                tv_enAed2.setVisibility(View.GONE);
                                tv_arAed2.setVisibility(View.VISIBLE);
                            }
                        }

                    }


                    tv_dealDescription.setText(checkinRewardModel.getPackageDescription() + "");
                    tv_dealName.setText(checkinRewardModel.getPackageName() + "");
                    tv_dealTimer.setText(checkinRewardModel.getTotalDescription());
                }
                shareButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onConsumeService();
                    }
                });
                break;

            default:
                tv_rewards.setText(getString(R.string.you_did_not_find_anything));
                tv_rewards.setVisibility(View.VISIBLE);
                tv_total_raffle_tickets.setVisibility(View.INVISIBLE);
                tv_rewards_count.setVisibility(View.INVISIBLE);
                shareButton.setText(getString(R.string.ok));
                ll_checkin_deal.setVisibility(View.GONE);
                ll_raffle_tickets.setVisibility(View.GONE);
                tv_cancel.setVisibility(View.INVISIBLE);
                break;
        }

    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        progressBar.setVisibility(View.VISIBLE);
        ViewLuckyDealInput viewLuckyDealInput = new ViewLuckyDealInput();
        viewLuckyDealInput.setChannel(channel);
        viewLuckyDealInput.setLang(currentLanguage);
        viewLuckyDealInput.setMsisdn(sharedPrefrencesManger.getMobileNo());
        viewLuckyDealInput.setAppVersion(appVersion);
        viewLuckyDealInput.setAuthToken(authToken);
        viewLuckyDealInput.setDeviceId(deviceId);
        viewLuckyDealInput.setToken(token);
        viewLuckyDealInput.setUserSessionId(sharedPrefrencesManger.getUserSessionId());
        viewLuckyDealInput.setUserId(sharedPrefrencesManger.getUserId());
        viewLuckyDealInput.setApplicationId(ConstantUtils.INDY_TALOS_ID);
        viewLuckyDealInput.setImsi(sharedPrefrencesManger.getMobileNo());
        viewLuckyDealInput.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        viewLuckyDealInput.setOsVersion(osVersion);
        if (checkinRewardModel != null && checkinRewardModel.getRewardId() != null) {
            viewLuckyDealInput.setRewardItemId(checkinRewardModel.getRewardId());
        }
        new ViewLuckyDealService(this, viewLuckyDealInput);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            progressBar.setVisibility(View.GONE);
            switch (responseModel.getServiceType()) {
                case ServiceUtils.VIEW_LUCKY_DEAL: {
                    if (responseModel != null) {
                        viewLuckyDealOutput = (ViewLuckyDealOutput) responseModel.getResultObj();
                        if (viewLuckyDealOutput != null)
                            if (viewLuckyDealOutput.isDealViewed()) {
                                viewLuckyDealSuccess();
                            } else {
                                viewLuckyDealError();
                            }
                    }
                    break;
                }
            }
        } catch (Exception ex) {

        }
    }

    private void viewLuckyDealSuccess() {
        //start shopping cart activity
        Intent intent = new Intent(getActivity(), ShoppingCartActivity.class);
        intent.putExtra(ConstantUtils.lottery_reward, checkinRewardModel);
        Fragment myFrag[] = {new SuccessfulCheckInFragment()};
        ((SwpeMainActivity) getActivity()).removeFragment(myFrag);
        startActivity(intent);
    }

    private void viewLuckyDealError() {
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)
                ? viewLuckyDealOutput.getErrorMsgEn() : viewLuckyDealOutput.getErrorMsgAr());
        errorFragment.setArguments(bundle);
        ((SwpeMainActivity) getActivity()).replaceFragmnet(errorFragment, R.id.contentFrameLayout, true);
    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        progressBar.setVisibility(View.GONE);
    }
}
