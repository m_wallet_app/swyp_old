package com.indy.views.fragments.gamification.challenges;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.indy.R;
import com.indy.controls.ServiceUtils;
import com.indy.customviews.CustomTabLayout;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.RechargeActivity;
import com.indy.views.activites.SwpeMainActivity;
import com.indy.views.fragments.AboutUs.InviteFriendFragment;
import com.indy.views.fragments.gamification.adapters.ChallengesPagerAdapter;
import com.indy.views.fragments.gamification.events.EventsFragment;
import com.indy.views.fragments.rewards.RewardsFragment;
import com.indy.views.fragments.store.mainstore.MyStoreFragment;
import com.indy.views.fragments.utils.MasterFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChallengesMainFragment extends MasterFragment {


    View rootView;
    TextView tv_title;
    ViewPager viewPager;

    public static ChallengesMainFragment challengesMainFragment;
    private TextView usageTxtID;
    int navigationIndex = -1;
    public ChallengesMainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_challenges_main, container, false);
        initUI();
        challengesMainFragment = this;
        return rootView;
    }

    @Override
    public void initUI() {
        super.initUI();

        TextView tv_headerTitle = (TextView) rootView.findViewById(R.id.usageTxtID);
        tv_headerTitle.setText(getString(R.string.challenges));
        usageTxtID = (TextView) rootView.findViewById(R.id.usageTxtID);
        usageTxtID.setText(getString(R.string.challenges));
        tv_title = (TextView) rootView.findViewById(R.id.tv_title);
        ic_scroll_up = (ImageView) rootView.findViewById(R.id.ic_scroll_up);
        ic_scroll_up.setBackground(getResources().getDrawable(R.drawable.ic_scroll));
        if(getArguments()!=null && getArguments().getString(ConstantUtils.CHALLENGES_TAB_NAVIGATION_INDEX)!=null){
            navigationIndex = Integer.parseInt(getArguments().getString(ConstantUtils.CHALLENGES_TAB_NAVIGATION_INDEX,"-1"));
        };
        onConsumeService();
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();

        initViewPager();
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        if (responseModel != null) {
            if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.optInGamification)) {

            }
        }
    }

    CustomTabLayout tabLayout;

    public void initViewPager() {
        tabLayout = (CustomTabLayout) rootView.findViewById(R.id.tab_layout);
        tabLayout.setSharedPrefrencesManger(sharedPrefrencesManger);
        tabLayout.addTab(tabLayout.newTab().setText((getString(R.string.badges))));
        tabLayout.addTab(tabLayout.newTab().setText((getString(R.string.checkin))));
        tabLayout.addTab(tabLayout.newTab().setText((getString(R.string.raffles))));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        viewPager = (ViewPager) rootView.findViewById(R.id.pager);
        final ChallengesPagerAdapter adapter = new ChallengesPagerAdapter(
                getChildFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);

        viewPager.setSelected(true);
        viewPager.setFocusable(true);
        viewPager.setOffscreenPageLimit(3);
        if(navigationIndex>-1){
            tabLayout.getTabAt(navigationIndex).select();
            viewPager.setCurrentItem(navigationIndex, true);
        }else {
            viewPager.setCurrentItem(1, true);
            tabLayout.getTabAt(1).select();
        }
        tabLayout.setFillViewport(true);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

                if (tab.getPosition() == 1) {
                    tv_title.setText(getString(R.string.find_out_whats_hidden));
                } else if (tab.getPosition() == 2) {
                    tv_title.setText(getString(R.string.take_your_chances));
                } else if (tab.getPosition() == 0   ) {
                    tv_title.setText(getString(R.string.scroll_like_you_mean_it));
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment[] fragArray = {new ChallengesMainFragment(),new EventsFragment(),new MyStoreFragment(), new RewardsFragment()};
        if (requestCode == ConstantUtils.RAFFLES_NAVIGATION) {
            switch (resultCode) {

                case ConstantUtils.RAFFLES_RECHARGE_ACCOUNT:
                    ((SwpeMainActivity) getActivity()).removeFragment(fragArray);
                    ((SwpeMainActivity) getActivity()).startActivity(new Intent(getActivity(), RechargeActivity.class));
                    break;

                case ConstantUtils.RAFFLES_BADGES:

                    tabLayout.getTabAt(0).select();

                    break;
                case ConstantUtils.RAFFLES_INVITE_FRIEND:
                    ((SwpeMainActivity) getActivity()).removeFragment(fragArray);
                    SwpeMainActivity.clickedItem = 2;
                    ((SwpeMainActivity) getActivity()).startActivity(new Intent(getActivity(), InviteFriendFragment.class));
                    break;

                case ConstantUtils.RAFFLES_CHECKIN:
                    tabLayout.getTabAt(1).select();

                    break;
                case ConstantUtils.RAFFLES_STORE:
                    ((SwpeMainActivity) getActivity()).removeFragment(fragArray);
                    SwpeMainActivity.clickedItem = 3;
                    ((SwpeMainActivity) getActivity()).addFragmnet(new MyStoreFragment(), R.id.contentFrameLayout, true);
                    break;
                default:
                    break;
            }
        }
    }

    private ImageView ic_scroll_up;

    public void onSlideUp() {
        if (usageTxtID != null) {
            usageTxtID.setVisibility(View.VISIBLE);
            if (isVisible())
                ic_scroll_up.setBackground(getResources().getDrawable(R.drawable.ic_scroll));
        }
    }

    public void onSlideDown() {
        if (usageTxtID != null) {
            usageTxtID.setVisibility(View.INVISIBLE);
            if (isVisible())
                ic_scroll_up.setBackground(getResources().getDrawable(R.drawable.ic_up));
        }
    }
}
