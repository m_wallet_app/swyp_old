package com.indy.views.fragments.buynewline;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.controls.AdjustEvents;
import com.indy.controls.DateSelectInterface;
import com.indy.controls.ServiceUtils;
import com.indy.helpers.DateHelpers;
import com.indy.models.onboarding_push_notifications.LogEIDDetailsRequestModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.verfiymsisdnusingeid.VerfyMsisidnUsingEidInput;
import com.indy.models.verfiymsisdnusingeid.VerfyMsisidnUsingEidOutput;
import com.indy.ocr.screens.EmailAddressFragment;
import com.indy.services.LogApplicationFlowService;
import com.indy.services.LogEIDDetailsService;
import com.indy.services.VerfiyMsisdByEidService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.RegisterationActivity;
import com.indy.views.fragments.buynewline.exisitingnumber.CoorporateNumberMigrationFragment;
import com.indy.views.fragments.buynewline.exisitingnumber.EligableNumberMigrationFragment;
import com.indy.views.fragments.buynewline.exisitingnumber.EmiratesIDVerificationFragment;
import com.indy.views.fragments.buynewline.exisitingnumber.InvalidEIDFrgament;
import com.indy.views.fragments.buynewline.exisitingnumber.NumberMigrationFragment;
import com.indy.views.fragments.buynewline.exisitingnumber.PendingPaymentFragment;
import com.indy.views.fragments.buynewline.exisitingnumber.RegisteredNonEmirateIDDocumentFragment;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.indy.views.fragments.utils.MasterFragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static com.indy.utils.ConstantUtils.TAGGING_manual_details;
import static com.indy.utils.ConstantUtils.TAGGING_enter_details;
import static com.indy.utils.ConstantUtils.eidNO;
import static com.indy.views.activites.RegisterationActivity.logEventInputModel;

/**
 * Created by emad on 9/7/16.
 */
public class NameDOBFragment extends MasterFragment {
    private View view;
    private Button dayBtn, monthBtn, yearBtn;
    TextView nextBtn;
    Animation dayAnim, monthAnim, yearAnim;
    public static DateSelectInterface dateSelectInterface;
    DateHelpers dateUtils;
    private int yearInt, monthInt, dayInt = 0;
    private String userName, email;
    private TextView daysSpinner, monthsSpinner, yearSpinner;
    private TextView dayTxtView, monthTxtView, yearTxtView;
    public static int dayVal, monthVal, yearVal = 0;
    public static String year;
    private double diffYears = 0;
    public static String emiratedID, fullName, dateOfBirth;
    public EditText et_fullName;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null)
            view = inflater.inflate(R.layout.fragment_enter_dob_name, container, false);
        initUI();
//        loadDaysSpinner();
//        loadMonthsSpinner();
//        loadYearsSpinner();
        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), TAGGING_manual_details);
        return view;
    }

    @Override
    public void initUI() {

        super.initUI();
        try {
            regActivity.showHeaderLayout();
            regActivity.setHeaderTitle("");

            dateUtils = new DateHelpers();
            yearInt = 0;
            monthInt = 0;
            dayInt = 0;
            dayBtn = (Button) view.findViewById(R.id.dayBtn);
            monthBtn = (Button) view.findViewById(R.id.monthBtn);
            yearBtn = (Button) view.findViewById(R.id.yearBtn);
            nextBtn = (TextView) view.findViewById(R.id.nextBtn);
            dayTxtView = (TextView) view.findViewById(R.id.dayTxt);
            monthTxtView = (TextView) view.findViewById(R.id.monthTxt);
            yearTxtView = (TextView) view.findViewById(R.id.yearTxt);
            if (getArguments() != null) {
                userName = getArguments().getString(ConstantUtils.userName);
                email = getArguments().getString(ConstantUtils.email);
            }

            monthsSpinner = (TextView) view.findViewById(R.id.monthSpin);
            daysSpinner = (TextView) view.findViewById(R.id.daySpin);
            yearSpinner = (TextView) view.findViewById(R.id.yearSpin);

            et_fullName = (EditText) view.findViewById(R.id.fullName);

            dayAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.zoom_anim);
            monthAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.zoom_anim);
            yearAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.zoom_anim);
            ((RegisterationActivity) getActivity()).showBackBtn();
            loadAnimation();
            disableNextBtn();
            daysSpinner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                DialogFragment newFragment = new DatePickerFragment();
//
//
//                newFragment.show(getActivity().getFragmentManager(), "datePicker");
                    showCalendar();
                }
            });
            monthsSpinner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                DialogFragment newFragment = new DatePickerFragment();
//                newFragment.show(getActivity().getFragmentManager(), "datePicker");
                    showCalendar();

                }
            });
            yearSpinner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                DialogFragment newFragment = new DatePickerFragment();
//                newFragment.show(getActivity().getFragmentManager(), "datePicker");
                    showCalendar();

                }
            });
            nextBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    logEvent(AdjustEvents.MANUAL_EID, new Bundle());
                    try {
                        hideKeyBoard();
//                long diffYears = dateUtils.validateDates(dateUtils.getCurrentDate(), getSelectedDate());

//                    dateUtils.getDiffYears(getSelectedDateForDifference(), dateUtils.getCurrentDateFor());
                        if (diffYears >= 30 || diffYears < 18) {
                            showErrorFragment(getString(R.string.ilgble_age));
                        } else {
//                    ((RegisterationActivity) getActivity()).replaceFragmnet(new Greater24Fragment(), R.id.frameLayout, true);
//                } else {
                            CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_manual_confirm_details);
                            if (sharedPrefrencesManger.isFromMigration()) {
                                onConsumeService();
                            } else {
                                fullName = et_fullName.getText().toString();
                                EmailAddressFragment emailAddressFragment = new EmailAddressFragment();

                                LogEIDDetailsRequestModel logEIDDetailsRequestModel = new LogEIDDetailsRequestModel();
                                logEIDDetailsRequestModel.setBirthDate(dateOfBirth);
                                logEIDDetailsRequestModel.setEmiratesid(EmiratesIDVerificationFragment.emiratesID);
                                logEIDDetailsRequestModel.setFullName(fullName);
                                logEIDDetailsRequestModel.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
                                new LogEIDDetailsService(NameDOBFragment.this, logEIDDetailsRequestModel);

                                regActivity.replaceFragmnet(emailAddressFragment, R.id.frameLayout, true);

                                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), TAGGING_enter_details);
                            }
//                            TermsAndConditionsFragment termsAndConditionsFragment = new TermsAndConditionsFragment();
//                            Bundle bundle = new Bundle();
//                            bundle.putString(ConstantUtils.userName, userName);
//                            bundle.putString(ConstantUtils.email, email);
//                            bundle.putString(ConstantUtils.birthdate, getSelectedDate());
//                            bundle.putString(ConstantUtils.newAccountMobileNo, getArguments().getString(ConstantUtils.newAccountMobileNo));
//
//                            termsAndConditionsFragment.setArguments(bundle);
//
//                            CommonMethods.logFirebaseEvent(mFirebaseAnalytics, ConstantUtils.TAGGING_dob_entered);
//                            logEvent();
//                            regActivity.replaceFragmnet(termsAndConditionsFragment, R.id.frameLayout, true);
                        }
//                }
                    } catch (Exception ex) {
                        CommonMethods.logException(ex);
                    }
                }

            });
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }
    }

    private void loadAnimation() {
        dayBtn.startAnimation(dayAnim);
        dayAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                monthBtn.setVisibility(View.VISIBLE);
                monthBtn.startAnimation(monthAnim);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        monthAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                yearBtn.setVisibility(View.VISIBLE);
                yearBtn.startAnimation(yearAnim);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public void showDatePickerFragment() {

    }


    public void onDateSelect(String day, String month, String year) {
        yearSpinner.setText(year);
        try {
            int monthValTemp = Integer.parseInt(month);
            monthValTemp += 1;
            monthsSpinner.setText(monthValTemp + "");
        } catch (Exception ex) {
            monthsSpinner.setText(month);
        }
        daysSpinner.setText(day);
        yearInt = Integer.valueOf(year);
        monthInt = Integer.valueOf(month);
        dayInt = Integer.valueOf(day);
        dayTxtView.setVisibility(View.VISIBLE);
        monthTxtView.setVisibility(View.VISIBLE);
        yearTxtView.setVisibility(View.VISIBLE);
        enableNextBtn();
        dateOfBirth = day + "/" + month + "/" + year;
        dateOfBirth = parseDate(dateOfBirth);
    }

    public static String parseDate(String date) {
        String returnString = "";
        SimpleDateFormat dateFormatter;
        try {
            Date formattedDate;
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            formattedDate = df.parse(date);
            dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
            return dateFormatter.format(formattedDate);
        } catch (Exception ex) {
            return returnString;

        }
    }

    private Date getSelectedDateForDifference(int day, int month, int year) {
        Calendar c = Calendar.getInstance();
        c.set(year, month + 1, day);
        return c.getTime();
    }

    private String getSelectedDate() {
        Calendar c = Calendar.getInstance();
        c.set(yearInt, monthInt - 1, dayInt);
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        String formattedDate = df.format(c.getTime());
        Log.v("selecteday", formattedDate + "  " + yearInt + " " + yearInt + " " + dayInt + " " + (monthInt - 1));
        return formattedDate;
    }

    private boolean isBirthValid() {
        boolean isvlaid = true;
        if (dayInt == 0 || yearInt == 0 || monthInt == 0) {
            isvlaid = false;
        }
        return isvlaid;
    }

    private void enableNextBtn() {
        nextBtn.setAlpha(ConstantUtils.ALPHA_BUTTON_ENABLED);
        nextBtn.setClickable(true);
        nextBtn.setEnabled(true);
    }

    private void disableNextBtn() {
        nextBtn.setAlpha(.5f);
        nextBtn.setClickable(false);
        nextBtn.setEnabled(false);
    }

    public void showErrorFragment(String error) {
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();

        bundle.putString(ConstantUtils.errorString, error);
        errorFragment.setArguments(bundle);
        regActivity.addFragmnet(errorFragment, R.id.frameLayout, true);
    }

//    private void checkNextStaus() {
//        if (dayVal == 0 || monthVal == 0 || yearVal == 0)
//            disableNextBtn();
//        else
//            enableNextBtn();
//
//    }

    public void showCalendar() {
        try {
            final Dialog dialog = new Dialog(getActivity());
            dialog.setContentView(R.layout.testing);

            final DatePicker datePicker = (DatePicker) dialog.findViewById(R.id.datePicker);
            Calendar cal1 = Calendar.getInstance();
            cal1 = Calendar.getInstance();

            cal1.setTimeInMillis(new Date().getTime());
            cal1.add(Calendar.YEAR, -18);
//        cal1.add(Calendar.DAY_OF_MONTH,-1);
            datePicker.setMaxDate(cal1.getTimeInMillis());
            cal1.setTime(new Date());
            cal1.add(Calendar.DAY_OF_MONTH, 1);
            cal1.add(Calendar.YEAR, -30);
            datePicker.setMinDate(cal1.getTimeInMillis());

            Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
            // if button is clicked, close the custom dialog
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int day = datePicker.getDayOfMonth();
                    int month = datePicker.getMonth();
                    int year = datePicker.getYear();
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(year, month, day);
                    SimpleDateFormat dateFormatter;

                    DateHelpers dateHelpers = new DateHelpers();
//                long diffYears = dateHelpers.validateDates(dateUtils.getCurrentDate(), getSelectedDate());

                    diffYears = dateUtils.getDiffYears(newDate.getTime(), dateUtils.getCurrentDateFor());
                    if (diffYears >= 30 || diffYears < 18) {
                        disableNextBtn();
//                    showErrorFragment(getString(R.string.ilgble_age));
                    } else {
//                    enableNextBtn();
                        dialog.dismiss();
                        onDateSelect(day + "", month + "", year + "");
                    }
                }
            });

            Button dialogCancelButton = (Button) dialog.findViewById(R.id.dialogButtonCancel);
            dialogCancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.cancel();
                }
            });

            dialog.show();
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }
    }

    private void logEvent() {


        logEventInputModel.setActionTransactionId(sharedPrefrencesManger.getTempAppId());
        logEventInputModel.setScreenId(ConstantUtils.SCREEN_ID_0003);

        logEventInputModel.setDay(daysSpinner.getText().toString());
        logEventInputModel.setMonth(monthsSpinner.getText().toString());
        logEventInputModel.setYear(yearSpinner.getText().toString());

        new LogApplicationFlowService(this, logEventInputModel);

    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        showLoadingFragment();
        logEvent();
        verfyMsisidnUsingEidInput = new VerfyMsisidnUsingEidInput();
        verfyMsisidnUsingEidInput.setAppVersion(appVersion);
        verfyMsisidnUsingEidInput.setToken(token);
        verfyMsisidnUsingEidInput.setOsVersion(osVersion);
        verfyMsisidnUsingEidInput.setChannel(channel);
        verfyMsisidnUsingEidInput.setDeviceId(deviceId);
        verfyMsisidnUsingEidInput.setMsisdn(NumberMigrationFragment.migrationMobileNumber);
        verfyMsisidnUsingEidInput.setEmiratesId(EmiratesIDVerificationFragment.emiratesID);
        verfyMsisidnUsingEidInput.setLang(currentLanguage);
        verfyMsisidnUsingEidInput.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        new VerfiyMsisdByEidService(this, verfyMsisidnUsingEidInput);
    }

    private VerfyMsisidnUsingEidInput verfyMsisidnUsingEidInput;
    private VerfyMsisidnUsingEidOutput verfyMsisidnUsingEidOutput;
    private int status;

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            if (isVisible()) {
                if (responseModel != null && responseModel.getServiceType() != null && responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.LOG_USER_EVENT)) {
                } else {
                    regActivity.onBackPressed();
                    if (responseModel != null && responseModel.getResultObj() != null) {
                        verfyMsisidnUsingEidOutput = (VerfyMsisidnUsingEidOutput) responseModel.getResultObj();
                        if (verfyMsisidnUsingEidOutput != null)
                            if (verfyMsisidnUsingEidOutput.getErrorCode() != null)
                                onNumberMigrationFail();
                            else
                                onNumberMigrationSucess();
                    }
                }
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        try {
            if (isVisible() && regActivity != null) {
                if (responseModel != null && responseModel.getServiceType() != null && responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.LOG_USER_EVENT)) {
                } else {
                    regActivity.onBackPressed();
                }
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

    }

    private void showLoadingFragment() {
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.loadingString, getString(R.string.verifying_eid));
        LoadingFragmnet loadingFragmnet = new LoadingFragmnet();
        loadingFragmnet.setArguments(bundle);
        regActivity.addFragmnet(loadingFragmnet, R.id.loadingFragmeLayout, true);
    }

    private void onNumberMigrationSucess() {
        try {
            status = verfyMsisidnUsingEidOutput.getStatus();

            emiratedID = EmiratesIDVerificationFragment.emiratesID;
            fullName = et_fullName.getText().toString();
            status = 1;
            switch (status) {
                case 1: // eligable
                    onEligable();
                    break;
                case 2: // not eligable due to un paid amount
                    onNotEligableDueToPaidAmount();
                    break;
                case 3: // Not eligible due to migrate to Indy
                    onNotEligableDueToMigrateIndy();
                    break;
                case 4: //Failure
                    Bundle bundle2 = new Bundle();

                    bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_migration_invalid_eid_number);
                    bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_migration_invalid_eid_number);
                    mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_migration_invalid_eid_number, bundle2);
                    onInValidEidResponse();
                    break;
                case 6:
                    onAlreadyCustomerResponse();
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void onAlreadyCustomerResponse() {


        regActivity.replaceFragmnet(new RegisteredNonEmirateIDDocumentFragment(), R.id.frameLayout, true);

    }

    private void onInValidEidResponse() {
        InvalidEIDFrgament invalidEIDFrgament = new InvalidEIDFrgament();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.eidNO, emiratedID);
        bundle.putString(ConstantUtils.mobileNo, NumberMigrationFragment.migrationMobileNumber);
        invalidEIDFrgament.setArguments(bundle);
        regActivity.replaceFragmnet(invalidEIDFrgament, R.id.frameLayout, true);

    }

    private void onNotEligableDueToMigrateIndy() {
        Fragment CoorporateNumberMigrationFragment = new CoorporateNumberMigrationFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.mobileNo, NumberMigrationFragment.migrationMobileNumber);
        CoorporateNumberMigrationFragment.setArguments(bundle);
        regActivity.replaceFragmnet(CoorporateNumberMigrationFragment, R.id.frameLayout, true);

    }

    private void onNotEligableDueToPaidAmount() {
        Bundle bundle = new Bundle();
        bundle.putString("AMOUNT", String.valueOf(verfyMsisidnUsingEidOutput.getAmount()));
        Fragment pendingFragmmnet = new PendingPaymentFragment();
        pendingFragmmnet.setArguments(bundle);
        regActivity.replaceFragmnet(pendingFragmmnet, R.id.frameLayout, true);
    }

    private void onEligable() {
        Bundle bundle2 = new Bundle();

        bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_migration_valid_eid_entered);
        bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_migration_valid_eid_entered);
        mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_migration_valid_eid_entered, bundle2);
        EligableNumberMigrationFragment eligableNumberMigrationFragment = new EligableNumberMigrationFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.mobileNo, NumberMigrationFragment.migrationMobileNumber);
        eligableNumberMigrationFragment.setArguments(bundle);
        regActivity.replaceFragmnet(eligableNumberMigrationFragment, R.id.frameLayout, true);
    }


    private void onNumberMigrationFail() {
        if (currentLanguage.equalsIgnoreCase("en"))
            showErrorFragment(verfyMsisidnUsingEidOutput.getErrorMsgEn());
        else
            showErrorFragment(verfyMsisidnUsingEidOutput.getErrorMsgAr());

//        ((RegisterationActivity) getActivity()).replaceFragmnet(new RegisteredNonEmirateIDDocumentFragment(), R.id.frameLayout, true);

    }
}
