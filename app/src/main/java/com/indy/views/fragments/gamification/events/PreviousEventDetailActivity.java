package com.indy.views.fragments.gamification.events;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.MasterActivity;
import com.indy.views.fragments.gamification.adapters.EventWinnersAdapter;
import com.indy.views.fragments.gamification.models.EventWinnerModel;
import com.indy.views.fragments.gamification.models.EventsList;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetWinnersListInputResponse;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetWinnersListOutputModel;
import com.indy.views.fragments.gamification.services.GetEventWinnersListService;
import com.indy.views.fragments.utils.LoadingFragmnet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class PreviousEventDetailActivity extends MasterActivity {

    RecyclerView rv_winners;
    TextView tv_title, tv_date, tv_result, tv_header;
    RelativeLayout rl_back, rl_help;
    List<EventWinnerModel> eventWinnerModels;
    EventWinnersAdapter eventWinnersAdapter;
    GetWinnersListOutputModel getWinnersListOutputModel;
    EventsList eventObject;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_previous_event_detail);
        initUI();
    }

    @Override
    public void initUI() {
        super.initUI();


        tv_header = (TextView) findViewById(R.id.titleTxt);
        tv_header.setText(getResources().getString(R.string.results));

        rl_back = (RelativeLayout) findViewById(R.id.backLayout);
        rl_help = (RelativeLayout) findViewById(R.id.helpLayout);
        rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        rl_help.setVisibility(View.INVISIBLE);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_result = (TextView) findViewById(R.id.tv_result);
        tv_date = (TextView) findViewById(R.id.tv_date);
        rv_winners = (RecyclerView) findViewById(R.id.rv_winners);
        if (getIntent() != null && getIntent().getExtras() != null) {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null && bundle.getParcelable(ConstantUtils.KEY_EVENT_DETAIL) != null) {
                eventObject = bundle.getParcelable(ConstantUtils.KEY_EVENT_DETAIL);
            } else {
                finish();
            }
        }

        onConsumeService();


//        createDummyList();

    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            onBackPressed();
            ((FrameLayout) findViewById(R.id.frameLayout)).setVisibility(View.GONE);
            if (responseModel != null) {
                getWinnersListOutputModel = (GetWinnersListOutputModel) responseModel.getResultObj();
                if (getWinnersListOutputModel != null) {
                    onEventDetailSuccess();

                }
            }
        }catch (Exception ex){

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sharedPrefrencesManger.getNavigationIndex().length() > 0 || sharedPrefrencesManger.getNavigationBadgeIndex().length() >0) {

            finish();
        }
    }

    public void onEventDetailSuccess() {
        tv_title.setText(eventObject.getNameEn());
        tv_date.setText(getString(R.string.event_on) + " " + CommonMethods.parseDateWithHours(eventObject.getTournamentActualDate(),currentLanguage));
//        tv_date.setText(getString(R.string.event_on) + " " +eventObject.getDate());
        if(getWinnersListOutputModel.getUserWon()){
            tv_result.setText(getString(R.string.you_are_a_winner));
        }else{
            tv_result.setText(getString(R.string.sorry_good_luck_next_time));
        }

        Collections.sort(getWinnersListOutputModel.getWinnersList(), new Comparator<EventWinnerModel>(){
            public int compare(EventWinnerModel obj1, EventWinnerModel obj2) {
                // ## Ascending order
//                return obj1.getRanking().compareToIgnoreCase(obj2.firstName); // To compare string values
                 return Integer.valueOf(obj1.getRank()).compareTo(obj2.getRank()); // To compare integer values

                // ## Descending order
                // return obj2.firstName.compareToIgnoreCase(obj1.firstName); // To compare string values
//                 return Integer.valueOf(obj2.getRank()).compareTo(obj1.getRank()); // To compare integer values
            }
        });

        if(getWinnersListOutputModel.getWinnersList() != null &&
                getWinnersListOutputModel.getWinnersList().size() > 0){
            eventWinnerModels = getWinnersListOutputModel.getWinnersList();
            eventWinnersAdapter = new EventWinnersAdapter(eventWinnerModels, this);
            rv_winners.setAdapter(eventWinnersAdapter);
            rv_winners.setLayoutManager(new LinearLayoutManager(this));
        }else{
            tv_result.setText(getString(R.string.winners_will_be_announced_soon));
        }
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        ((FrameLayout) findViewById(R.id.frameLayout)).setVisibility(View.VISIBLE);

        addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        GetWinnersListInputResponse getWinnersListInputResponse = new GetWinnersListInputResponse();
        getWinnersListInputResponse.setChannel(channel);
        getWinnersListInputResponse.setLang(currentLanguage);
        getWinnersListInputResponse.setMsisdn(sharedPrefrencesManger.getMobileNo());
        getWinnersListInputResponse.setAppVersion(appVersion);
        getWinnersListInputResponse.setAuthToken(authToken);
        getWinnersListInputResponse.setDeviceId(deviceId);
        getWinnersListInputResponse.setToken(token);
        getWinnersListInputResponse.setImsi(sharedPrefrencesManger.getMobileNo());
        getWinnersListInputResponse.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        getWinnersListInputResponse.setOsVersion(osVersion);

        getWinnersListInputResponse.setApplicationId(ConstantUtils.INDY_TALOS_ID);
        getWinnersListInputResponse.setUserId(sharedPrefrencesManger.getUserId());
        getWinnersListInputResponse.setUserSessionId(sharedPrefrencesManger.getUserSessionId());
        getWinnersListInputResponse.setIndyEventId(eventObject.getIndyEventId());
        new GetEventWinnersListService(this, getWinnersListInputResponse);
    }

    private void createDummyList() {
        eventWinnerModels = new ArrayList<>();

        EventWinnerModel eventWinnerModel = new EventWinnerModel();
        eventWinnerModel.setId("1");
        eventWinnerModel.setDescription("A great concert it was! #swyp #yolo");
        eventWinnerModel.setImageURL("");
        eventWinnerModel.setContestantName("Mark Henry");
        eventWinnerModel.setRank(1);
        eventWinnerModel.setVotes(12345);
        eventWinnerModels.add(eventWinnerModel);

        EventWinnerModel eventWinnerModel2 = new EventWinnerModel();
        eventWinnerModel2.setId("2");
        eventWinnerModel2.setDescription("This was a biggg show! #swyp #strongman #baahubali_USA");
        eventWinnerModel2.setImageURL("");
        eventWinnerModel2.setContestantName("Big Show");
        eventWinnerModel2.setRank(2);
        eventWinnerModel2.setVotes(12300);
        eventWinnerModels.add(eventWinnerModel2);


        EventWinnerModel eventWinnerModel3 = new EventWinnerModel();
        eventWinnerModel3.setId("3");
        eventWinnerModel3.setDescription("This was indeed a main event! #swyp #money #raw");
        eventWinnerModel3.setImageURL("");
        eventWinnerModel3.setContestantName("Triple H");
        eventWinnerModel3.setRank(3);
        eventWinnerModel3.setVotes(12235);
        eventWinnerModels.add(eventWinnerModel3);

    }
}
