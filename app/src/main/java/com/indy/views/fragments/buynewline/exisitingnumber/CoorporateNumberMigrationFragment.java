package com.indy.views.fragments.buynewline.exisitingnumber;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.indy.R;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.LogApplicationFlowService;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.LiveChatActivity;
import com.indy.views.fragments.buynewline.NewNumberFragment;
import com.indy.views.fragments.utils.MasterFragment;

import org.jetbrains.annotations.Nullable;

import static com.indy.views.activites.RegisterationActivity.logEventInputModel;

/**
 * Created by emad on 9/25/16.
 */

public class CoorporateNumberMigrationFragment extends MasterFragment {
    private View view;
    private Button newNoBtn, liveChatBtn;
    private TextView mobileNo;
    private String mobileNotxt;
    public static CoorporateNumberMigrationFragment coorporateNumberMigrationFragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_coorporate_number_migration_payment, container, false);
        initUI();
        setListeners();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
        regActivity.setHeaderTitle("");
        regActivity.showHeaderLayout();
        newNoBtn = (Button) view.findViewById(R.id.newNoID);
        liveChatBtn = (Button) view.findViewById(R.id.liveChatID);
        mobileNo = (TextView) view.findViewById(R.id.mobileNo);
        coorporateNumberMigrationFragment = this;
        if (getArguments().getString(ConstantUtils.mobileNo, "") != null)
            mobileNo.setText(getArguments().getString(ConstantUtils.mobileNo, ""));
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
    }

    private void setListeners() {
        newNoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment[] myFragments = {CoorporateNumberMigrationFragment.coorporateNumberMigrationFragment,
                        NumberMigrationFragment.numberMigrationFragmentInstance,
                        NumberTransferFragment.numberTransferFragment,
                        NewNumberFragment.newNumberFragment
                };
                regActivity.removeFragment(myFragments);
                logEvent(1);
                regActivity.replaceFragmnet(new NewNumberFragment(), R.id.frameLayout, true);
            }
        });
        liveChatBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logEvent(2);
                startActivity(new Intent(regActivity,LiveChatActivity.class));
            }
        });
    }

    private void logEvent(int index) {

       logEventInputModel.setScreenId(ConstantUtils.SCREEN_ID_3042);

        switch(index){
            case 1:
                logEventInputModel.setEligibilityFailureOutstandingAmountNewNumber(newNoBtn.getText().toString());

                break;

            case 2:
                logEventInputModel.setEligibilityFailureAccountRestrictionsLiveChat(liveChatBtn.getText().toString());
                break;
        }

        new LogApplicationFlowService(this, logEventInputModel);
    }
}

