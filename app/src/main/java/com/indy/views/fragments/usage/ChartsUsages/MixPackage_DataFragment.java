package com.indy.views.fragments.usage.ChartsUsages;

import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.Utils;
import com.indy.R;
import com.indy.adapters.PackagesListAdapter;
import com.indy.customviews.MyProgressBar;
import com.indy.models.packages.PackageConsumption;
import com.indy.models.packageusage.GraphPointList;
import com.indy.models.packageusage.UsageGraphList;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.MixedDataPackageGraphPagerActivity;
import com.indy.views.fragments.utils.MasterFragment;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Amir.jehangir on 11/6/2016.
 */
public class MixPackage_DataFragment extends MasterFragment {
    View view;

    private LineChart mChart;
    int xVal = 30;
    float yVal = 100f;
    float minVal;
    float upperLimt = 0f;
    float lowerLimit = 0f;
    private YAxis leftAxis;
    private LimitLine llXAxis;
    Typeface typeface;
//    private Button backImg, helpBtn;
//    UsagePackageList userPackgeListItem;
//    ArrayList<OfferList> offerList;

    //    private TextView expiresId;
    private TextView startDate, endDate;
    private ImageView iv_packageIcon;
    //    private TextView remaingID, titletext,
    TextView data_package_name, tv_balance_expire;

    //    TextView tv_user_balance_amount, tv_balance_expire, tv_additional_subscription;
    ArrayList<Entry> values = new ArrayList<Entry>();
    ArrayList<GraphPointList> GraphsPoints;
    UsageGraphList usageGraphListObject;
    Bundle extras;
    TextView tv_dataUsedToday, tv_dailyEstimatedUsage, tv_forecastUsage,tv_dailyUsedSubtitle;
    RelativeLayout rl_dataHeader, rl_progressBarHeader, rl_progressBarLayout;
    MyProgressBar myProgressBar;
    TextView tv_offerName, tv_offerPackageAmount;
    public MixPackage_DataFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_mixpackage_data, container, false);
        extras = getArguments();
        GraphsPoints = new ArrayList<>();
        usageGraphListObject = new UsageGraphList();
        if (extras != null && extras.getParcelable("DataGraphList") != null) {
            usageGraphListObject = getArguments().getParcelable("DataGraphList");
            if (usageGraphListObject != null && usageGraphListObject.getGraphPointList() != null)
                GraphsPoints = (ArrayList<GraphPointList>) usageGraphListObject.getGraphPointList();
            else
                GraphsPoints = new ArrayList<GraphPointList>();
        } else {

        }


        initUI();
        //  onConsumeService();
        return view;
    }


    @Override
    public void initUI() {
        super.initUI();
        try {
            mChart = (LineChart) view.findViewById(R.id.chart1);
            startDate = (TextView) view.findViewById(R.id.startDateID);
            endDate = (TextView) view.findViewById(R.id.endDateId);
            data_package_name = (TextView) view.findViewById(R.id.data_package_name);
            tv_balance_expire = (TextView) view.findViewById(R.id.tv_expire_days_left);
            iv_packageIcon = (ImageView) view.findViewById(R.id.numberofdeals);
            if (MixedDataPackageGraphPagerActivity.userPackgeListItem.getIconUrl() != null) {
                Picasso.with(getContext()).load(MixedDataPackageGraphPagerActivity.userPackgeListItem.getIconUrl()).
                        networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE).into(iv_packageIcon);
            }


            tv_dailyEstimatedUsage = (TextView) view.findViewById(R.id.tv_mbused_daily);
            tv_dataUsedToday = (TextView) view.findViewById(R.id.tv_mbused);
            tv_forecastUsage = (TextView) view.findViewById(R.id.tv_mbused_forecast);
            tv_dailyUsedSubtitle = (TextView) view.findViewById(R.id.tv_dailyEsimation);
            tv_balance_expire.setText("");
            if (currentLanguage.equals("en")) {
                data_package_name.setText(MixedDataPackageGraphPagerActivity.userPackgeListItem.getNameEn());
            } else {
                data_package_name.setText(MixedDataPackageGraphPagerActivity.userPackgeListItem.getNameAr());
            }
            if (extras != null && extras.getString("startDate") != null) {
                startDate.setText(parseDateFormatChart(getArguments().getString("startDate").toString()));
            } else {
                startDate.setText("");
            }
            if (extras != null && extras.getString("endDate") != null) {
                endDate.setText(parseDateFormatChart(getArguments().getString("endDate").toString()));
            } else {
                endDate.setText("");
            }
            if (currentLanguage.equals("en")) {
                typeface = Typeface.createFromAsset(getResources().getAssets(), "fonts/OpenSans-Regular.ttf");
            } else {
                typeface = Typeface.createFromAsset(getResources().getAssets(), "fonts/aktive_grotest_rg.ttf");
            }

            rl_dataHeader = (RelativeLayout) view.findViewById(R.id.rl_data);
            rl_progressBarHeader = (RelativeLayout) view.findViewById(R.id.rl_data_progress_bar);
            rl_progressBarLayout = (RelativeLayout) view.findViewById(R.id.rl_progress_bar);
            myProgressBar = (MyProgressBar) view.findViewById(R.id.myProgressBar);
            tv_offerName = (TextView) view.findViewById(R.id.tv_offer_name);
            tv_offerPackageAmount = (TextView) view.findViewById(R.id.tv_package_amount);

            if (GraphsPoints != null) {
//                AssembleChart();
                setupProgressBar();

            }


        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }
    }

    public void setupProgressBar() {
        rl_progressBarHeader.setVisibility(View.VISIBLE);
        rl_dataHeader.setVisibility(View.GONE);
        mChart.setVisibility(View.GONE);
        rl_progressBarLayout.setVisibility(View.VISIBLE);

        PackageConsumption packageConsumption;
//            if(PackagesListAdapter.usagePackageItem!=null && PackagesListAdapter.usagePackageItem.getOfferList()!=null)
        packageConsumption = PackagesListAdapter.usagePackageItem.getOfferList().get(0).getPackageConsumption();
        if(sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)) {
            tv_offerName.setText(PackagesListAdapter.usagePackageItem.getNameEn());
        }else{
            tv_offerName.setText(PackagesListAdapter.usagePackageItem.getNameAr());
        }
        myProgressBar.setDataLimitText(packageConsumption.getRemaining() + " " + packageConsumption.getConsumptionUnitEn() +
                " " + getString(R.string.data_left));
        Double remainingAllowance = (packageConsumption.getConsumption() / packageConsumption.getQuota()) * 100;
//        myProgressBar.setMax(100);

        int finalProgress = 100 -(int) Math.ceil(remainingAllowance);
//        remainingAllowance.intValue();

        myProgressBar.setProgress(finalProgress);

//        myProgressBar.setProgress(100 - 50);



        Double limitFromServiceValue = PackagesListAdapter.usagePackageItem.getOfferList().get(0).getPackageConsumption().getQuota();
        Double forecastedUsageValue = Double.parseDouble(usageGraphListObject.getForecastedUsage());

        if (forecastedUsageValue > limitFromServiceValue) {
            tv_forecastUsage.setTextColor(getResources().getColor(R.color.red_color));
        } else {
            tv_forecastUsage.setTextColor(getResources().getColor(R.color.colorBlack));
        }
        tv_forecastUsage.setText(PackagesListAdapter.daysDifferenceForDetail(
                MixedDataPackageGraphPagerActivity.userPackgeListItem.getOfferList().get(0).getInOfferEndDate()));//, getString(R.string.days_left)));

        if (sharedPrefrencesManger.getLanguage().equals("en")) {
            tv_dataUsedToday.setText(usageGraphListObject.getUsedToday() + " " + packageConsumption.getConsumptionUnitEn());
            tv_dailyEstimatedUsage.setText(packageConsumption.getConsumption()+ " " + packageConsumption.getConsumptionUnitEn());
            tv_dailyUsedSubtitle.setText(getString(R.string.of) + " " + packageConsumption.getQuota()+packageConsumption.getConsumptionUnitEn() + " " + getString(R.string.used_lower_case));
//            tv_forecastUsage.setText(usageGraphListObject.getForecastedUsage() + " " + packageConsumption.getConsumptionUnitEn());
//            tv_forecastUsage.setText(tv_balance_expire.getText().toString());
//            tv_dailyEstimatedUsage.setText(usageGraphListObject.getDailyBudget() + " " + packageConsumption.getConsumptionUnitEn());
//            tv_offerName.setText(packageConsumption.getTextEn());
            tv_offerPackageAmount.setText(packageConsumption.getQuota().toString() + " " +packageConsumption.getConsumptionUnitEn());
        } else {
            tv_dataUsedToday.setText(usageGraphListObject.getUsedToday() + " " + packageConsumption.getConsumptionUnitAr());
//            tv_forecastUsage.setText(usageGraphListObject.getForecastedUsage() + " " + packageConsumption.getConsumptionUnitAr());
            tv_dailyEstimatedUsage.setText(getString(R.string.of) + " " + packageConsumption.getQuota()+packageConsumption.getConsumptionUnitAr() + " " + getString(R.string.used_lower_case));

            tv_dailyEstimatedUsage.setText(usageGraphListObject.getDailyBudget() + " " + packageConsumption.getConsumptionUnitAr());
//            tv_offerName.setText(packageConsumption.getTextAr());
            tv_offerPackageAmount.setText(packageConsumption.getQuota().toString() + " " +packageConsumption.getConsumptionUnitAr());
        }
    }

    private void AssembleChart() {
//        mChart.setViewPortOffsets(0, 0, 0, 0);
        try {
            mChart.setDrawGridBackground(false);

            // no description text
            mChart.setDescription("");
            PackageConsumption packageConsumption;
//            if(PackagesListAdapter.usagePackageItem!=null && PackagesListAdapter.usagePackageItem.getOfferList()!=null)

            packageConsumption = PackagesListAdapter.usagePackageItem.getOfferList().get(0).getPackageConsumption();
            mChart.setNoDataTextDescription("You need to provide data for the chart.");


            // add data
//        PackagesListAdapter.usagePackageItem.getOfferList().get(0).getPackageConsumption().
            String limitFromService = PackagesListAdapter.usagePackageItem.getOfferList().get(0).getPackageConsumption().getQuota().toString();
            setData(xVal, Float.parseFloat(limitFromService));
            mChart.setTouchEnabled(false);
            mChart.setDragEnabled(false);
            mChart.setScaleEnabled(false);

            mChart.setScaleYEnabled(true);

            llXAxis = new LimitLine(Float.parseFloat(limitFromService), "Index 10");
            llXAxis.setLineWidth(1f);

            llXAxis.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
            llXAxis.setTextSize(10f);
            XAxis xAxis = mChart.getXAxis();
            xAxis.setEnabled(true);
//        xAxis.enableGridDashedLine(10f, 10f, 0f);
            xAxis.disableGridDashedLine();
            //xAxis.setValueFormatter(new MyCustomXAxisValueFormatter());
            //xAxis.addLimitLine(llXAxis); // add x-axis limit line
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            xAxis.setAxisLineWidth(1);
            upperLimt = Float.parseFloat(limitFromService);
            lowerLimit = 0f;
            LimitLine ll1;
            Double limitFromServiceValue = PackagesListAdapter.usagePackageItem.getOfferList().get(0).getPackageConsumption().getQuota();
            Double forecastedUsageValue = Double.parseDouble(usageGraphListObject.getForecastedUsage());

            if (forecastedUsageValue > limitFromServiceValue) {
                tv_forecastUsage.setTextColor(getResources().getColor(R.color.red_color));
            } else {
                tv_forecastUsage.setTextColor(getResources().getColor(R.color.colorBlack));
            }
            if (sharedPrefrencesManger.getLanguage().equals("en")) {
                ll1 = new LimitLine(upperLimt, upperLimt + " " + packageConsumption.getConsumptionUnitEn() + " " + getResources().getString(R.string.chart_limit));
                ll1.setLineWidth(1f);
                ll1.setTypeface(typeface);
                ll1.setLineColor(getResources().getColor(R.color.graph_line_color));
                tv_dataUsedToday.setText(usageGraphListObject.getUsedToday() + " " + packageConsumption.getConsumptionUnitEn());
                tv_forecastUsage.setText(usageGraphListObject.getForecastedUsage() + " " + packageConsumption.getConsumptionUnitEn());
//        tv_dataAllowanceRemaining.setText(packageUsageOutput.getUsageGraphList().get(0).getDailyBudget());
                tv_dailyEstimatedUsage.setText(usageGraphListObject.getDailyBudget() + " " + packageConsumption.getConsumptionUnitEn());
            } else {
                ll1 = new LimitLine(upperLimt, upperLimt + " " + packageConsumption.getConsumptionUnitAr() + " " + getResources().getString(R.string.chart_limit));
                ll1.setLineWidth(1f);
                ll1.setTypeface(typeface);
                ll1.setLineColor(getResources().getColor(R.color.graph_line_color));

                tv_dataUsedToday.setText(usageGraphListObject.getUsedToday() + " " + packageConsumption.getConsumptionUnitAr());
                tv_forecastUsage.setText(usageGraphListObject.getForecastedUsage() + " " + packageConsumption.getConsumptionUnitAr());
//        tv_dataAllowanceRemaining.setText(packageUsageOutput.getUsageGraphList().get(0).getDailyBudget());
                tv_dailyEstimatedUsage.setText(usageGraphListObject.getDailyBudget() + " " + packageConsumption.getConsumptionUnitAr());

            }

//        ll1.enableDashedLine(10f, 10f, 0f);
            ll1.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_TOP);
            ll1.setTextSize(14f);
            ll1.setTextColor(getResources().getColor(R.color.greyish_brown));

            ll1.setTypeface(typeface);
//        ll1.setTypeface(tf);

//        LimitLine ll2 = new LimitLine(lowerLimit, "Lower Limit");
//        ll2.setLineWidth(2f);
////        ll2.enableDashedLine(10f, 10f, 0f);
//        ll2.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
//        ll2.setTextSize(10f);
////        ll2.setTypeface(tf);
//        ll2.setLineColor(android.R.color.transparent);
            leftAxis = mChart.getAxisLeft();
            leftAxis.removeAllLimitLines(); // reset all limit lines to avoid overlapping lines
            if (ll1 != null && usageGraphListObject != null && usageGraphListObject.getGraphPointList() != null && usageGraphListObject.getGraphPointList().size() > 0)
                leftAxis.addLimitLine(ll1);
            //  leftAxis.addLimitLine(ll2);
            leftAxis.setAxisMaxValue(upperLimt);
            leftAxis.setAxisMinValue(lowerLimit);
            leftAxis.setYOffset(0f);
            //   leftAxis.enableGridDashedLine(10f, 10f, 0f);
            leftAxis.setDrawZeroLine(false);
            // limit lines are drawn behind data (and not on top)
            leftAxis.setDrawLimitLinesBehindData(false);
            leftAxis.setMaxWidth(1);
            leftAxis.setAxisLineWidth(1);
            mChart.getAxisRight().setEnabled(false);
            mChart.getXAxis().setAxisMinValue(-0.1f);
            mChart.getXAxis().setAxisLineColor(getResources().getColor(R.color.graph_line_color));
            mChart.getAxisLeft().setAxisLineColor(getResources().getColor(R.color.graph_line_color));
            //mChart.getViewPortHandler().setMaximumScaleY(2f);
            //mChart.getViewPortHandler().setMaximumScaleX(2f);

            mChart.animateX(2500);
            //mChart.invalidate();

            // get the legend (only possible after setting data)
            mChart.getLegend().setEnabled(false);
            mChart.setBorderColor(getResources().getColor(R.color.graph_line_color));

            // modify the legend ...
            // l.setPosition(LegendPosition.LEFT_OF_CHART);
            // l.setForm(Legend.LegendForm.LINE);

//        disableCordinaets();
//        enableCordinates();
            // // dont forget to refresh the drawing
            // mChart.invalidate();
//        toogleValues();
            toogleCubed();
            toogleCircles();
//        mChart.animateX(2500);

            animateXY();
            disableGridView();
            disableCordinaets();
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }

//        tv_mbused_daily.setText();
    }

    private void disableCordinaets() {
        mChart.getXAxis().setTextColor(android.R.color.transparent);
        leftAxis.setTextColor(android.R.color.transparent);
    }

    private void disableGridView() {
        mChart.getAxisLeft().setDrawGridLines(false);
        mChart.getXAxis().setDrawGridLines(false);
    }

    private void enableCordinates() {
        mChart.getXAxis().setTextColor(android.R.color.transparent);
        leftAxis.setTextColor(android.R.color.transparent);
    }


    private void toogleValues() {
        List<ILineDataSet> sets = mChart.getData()
                .getDataSets();

        for (ILineDataSet iSet : sets) {

            LineDataSet set = (LineDataSet) iSet;
            set.setDrawValues(!set.isDrawValuesEnabled());
        }

        mChart.invalidate();
    }

    private void toogleCubed() {
        try {
            List<ILineDataSet> sets = mChart.getData()
                    .getDataSets();

            for (ILineDataSet iSet : sets) {

                LineDataSet set = (LineDataSet) iSet;
                set.setMode(set.getMode() == LineDataSet.Mode.CUBIC_BEZIER
                        ? LineDataSet.Mode.LINEAR
                        : LineDataSet.Mode.CUBIC_BEZIER);
            }
            mChart.invalidate();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void toogleCircles() {
        try {
            List<ILineDataSet> sets = mChart.getData()
                    .getDataSets();


            for (ILineDataSet iSet : sets) {

                LineDataSet set = (LineDataSet) iSet;
                if (set.isDrawCirclesEnabled())
                    set.setDrawCircles(false);
                else
                    set.setDrawCircles(true);
            }
            mChart.invalidate();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void animateXY() {
        mChart.animateXY(3000, 3000);

    }


    private void setData(int count, float range) {


        //   List<GraphPointList> tempList = packageUsageOutput.getUsageGraphList().get(0).getGraphPointList();
        try {
            Collections.sort(GraphsPoints, new Comparator<GraphPointList>() {
                @Override
                public int compare(GraphPointList lhs, GraphPointList rhs) {
                    if (lhs.getYAxisValue() < rhs.getYAxisValue()) {
                        return -1;
                    } else {
                        return 1;
                    }
                }
            });

            float xValue = -1;
            if (usageGraphListObject != null && usageGraphListObject.getGraphPointList() != null &&
                    usageGraphListObject.getGraphPointList().size() > 0 &&
                    usageGraphListObject.getGraphPointList().get(0) != null && (
                    usageGraphListObject.getGraphPointList().get(0).getXAxisValue() > 0 ||
                            usageGraphListObject.getGraphPointList().get(0).getYAxisValue() > 0)) {


                values = new ArrayList<>();
                for (int i = 0; i < count; i++) {
                    values.add(new Entry(i, 0));
                }
                for (int i = 0; i < usageGraphListObject.getGraphPointList().size(); i++) {
//            if (i < GraphsPoints.size()) {
////                if (i < 10) {
                    String valToFloat = GraphsPoints.get(i).getYAxisValue().toString();
                    float val = Float.parseFloat(valToFloat);
                    upperLimt = (float) (GraphsPoints.get(i).getYAxisValue() + 5);
                    //   int abc = Integer.parseInt(Collections.max(tempList.get(i).getYAxisValue()/11));
                    String xValueToChange = GraphsPoints.get(i).getXAxisValue().toString();
                    xValue = Float.parseFloat(xValueToChange);

                    if (xValue >= 0 && xValue <= 30)
                        values.set(Math.round(xValue), new Entry(xValue, val));
//                } else {
//                    float val = (float) (tempList.get(i).getYAxisValue()/11);
//                    values.add(new Entry(i, val));
//
//                }
//
//
//            } else {
//                values.add(new Entry(i, 0));
//            }
                }
                if (xValue >= 0 && xValue <= 30) {

                    values.add(Math.round(xValue) + 1, new Entry(xValue + 0.001f, 0));
                }

            } else {
                values = new ArrayList<Entry>();
            }
            LineDataSet set1;
            mChart.setBorderColor(getResources().getColor(R.color.orange_color));

            if (mChart.getData() != null &&
                    mChart.getData().getDataSetCount() > 0) {
                set1 = (LineDataSet) mChart.getData().getDataSetByIndex(0);
                set1.setValues(values);
                set1.setDrawValues(false);
                set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                set1.setCubicIntensity(1.0f);
                mChart.getData().notifyDataChanged();
                mChart.notifyDataSetChanged();

            } else {
                // create a dataset and give it a type
//            set1 = new LineDataSet(values, "10 days Remaining");
                set1 = new LineDataSet(values, "");
                // set the line to be drawn like this "- - - - - -"
//            set1.enableDashedLine(10f, 5f, 0f);
                set1.setCubicIntensity(1.0f);

                set1.enableDashedHighlightLine(10f, 5f, 0f);
                set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                set1.setColor(getResources().getColor(R.color.orange_color));
                set1.setCircleColor(getResources().getColor(R.color.orange_color));
                set1.setLineWidth(1f);
                set1.setCircleRadius(3f);
                set1.setDrawCircleHole(false);
                set1.setValueTextSize(9f);
                set1.setDrawValues(false);
                set1.setDrawFilled(true);
//            set1.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);

                if (Utils.getSDKInt() >= 18) {
                    // fill drawable only supported on api level 18 and above
                    Drawable drawable = ContextCompat.getDrawable(getActivity(), R.drawable.fade_red);
                    set1.setFillDrawable(drawable);
                } else {
                    set1.setFillColor(getResources().getColor(R.color.graph_line_color));
                }

                ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
                dataSets.add(set1); // add the datasets

                // create a data object with the datasets
                LineData data = new LineData(dataSets);

                // set data

                mChart.setData(data);
            }
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }
    }

//
//    Date convertToDate(String receivedDate) throws ParseException {
//        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
//        Date date = formatter.parse(receivedDate);
//        return date;
//
//    }


    public static String parseDateFormat(String date) {
        String returnString = "";
        SimpleDateFormat dateFormatter;
        try {
            Date formattedDate;
            //28/09/2016 19:13:20
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
            formattedDate = df.parse(date);
            dateFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
            return dateFormatter.format(formattedDate);
        } catch (Exception ex) {
            return returnString;

        }
    }


    public static String parseDate(String date) {
        String returnString = "";
        SimpleDateFormat dateFormatter;
        try {
            Date formattedDate;
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
            formattedDate = df.parse(date);
            dateFormatter = new SimpleDateFormat("dd MMM", Locale.US);
            return dateFormatter.format(formattedDate);
        } catch (Exception ex) {
            return returnString;

        }
    }


    public static String parseDateFormatChart(String date) {
        String returnString = "";
        SimpleDateFormat dateFormatter;
        try {
            Date formattedDate;
            //28/09/2016 19:13:20
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
            formattedDate = df.parse(date);
            dateFormatter = new SimpleDateFormat("dd MMM", Locale.US);
            return dateFormatter.format(formattedDate);
        } catch (Exception ex) {
            return returnString;

        }
    }


}
