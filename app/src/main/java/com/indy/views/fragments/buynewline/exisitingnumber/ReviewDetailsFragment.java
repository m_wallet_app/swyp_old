package com.indy.views.fragments.buynewline.exisitingnumber;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.indy.R;
import com.indy.controls.ServiceUtils;
import com.indy.customviews.CustomEditText;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.verfiymsisdnusingeid.VerfyMsisidnUsingEidInput;
import com.indy.models.verfiymsisdnusingeid.VerfyMsisidnUsingEidOutput;
import com.indy.services.LogApplicationFlowService;
import com.indy.services.VerfiyMsisdByEidService;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.buynewline.Registeration3Fragment;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.indy.views.fragments.utils.MasterFragment;

import java.util.ArrayList;

import static com.indy.views.activites.RegisterationActivity.logEventInputModel;

/**
 * Created by emad on 9/25/16.
 */

public class ReviewDetailsFragment extends MasterFragment {
    private View view;
    private EditText fullnameTxt;
    private Button verfyBtn;
    private VerfyMsisidnUsingEidInput verfyMsisidnUsingEidInput;
    private VerfyMsisidnUsingEidOutput verfyMsisidnUsingEidOutput;
    private CustomEditText code1, code2, code3, code4;
    public int NO_OF_DIGITS = 15;
    int[] tvListIds = {R.id.code_1, R.id.code_2, R.id.code_3, R.id.code_4};
    ArrayList<EditText> tv_digits;

    String eidFromBundle = "";
    TextInputLayout til_name, til_ID;
    private int status;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_review_details, container, false);
        initUI();
        setListeners();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
        regActivity.showHeaderLayout();
        regActivity.setHeaderTitle("");
        fullnameTxt = (EditText) view.findViewById(R.id.fullnametxt);
//        eidTxt = (EditText) view.findViewById(R.id.eidTxt);
        verfyBtn = (Button) view.findViewById(R.id.verfyBtn);
        if (getArguments() != null && getArguments().getString(ConstantUtils.eidNO) != null)
            eidFromBundle = getArguments().getString(ConstantUtils.eidNO);
        fullnameTxt.setText(Registeration3Fragment.fullnameStr);
        til_name = (TextInputLayout) view.findViewById(R.id.name_input_layout);
        til_ID = (TextInputLayout) view.findViewById(R.id.emirtaes_id_input_layout);


        code1 = (CustomEditText) view.findViewById(R.id.code_1);
        code2 = (CustomEditText) view.findViewById(R.id.code_2);
        code3 = (CustomEditText) view.findViewById(R.id.code_3);
        code4 = (CustomEditText) view.findViewById(R.id.code_4);
        tv_digits = new ArrayList<EditText>();
        for (int i = 0; i < tvListIds.length; i++) {
            EditText et_temp = (EditText) view.findViewById(tvListIds[i]);
            et_temp.addTextChangedListener(new CustomTextWatcher(et_temp));
            tv_digits.add(et_temp);
        }

        if (eidFromBundle.length() == NO_OF_DIGITS) {
            code1.setText(eidFromBundle.substring(0, 3));
            code2.setText(eidFromBundle.substring(3, 7));
            code3.setText(eidFromBundle.substring(7, 14));
            code4.setText(eidFromBundle.substring(14, 15));
        }

        if (getCode(false).length() == 15 && fullnameTxt.getText().toString().trim().length() > 0) {
            enableVerifyBtn();
        } else {
            disableVerifyBtn();
        }

    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            if (isVisible()) {
                if(responseModel!=null && responseModel.getServiceType()!=null && responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.LOG_USER_EVENT)){

                }else {
                    regActivity.onBackPressed();
                    if (responseModel != null && responseModel.getResultObj() != null) {
                        verfyMsisidnUsingEidOutput = (VerfyMsisidnUsingEidOutput) responseModel.getResultObj();
                        if (verfyMsisidnUsingEidOutput != null)
                            if (verfyMsisidnUsingEidOutput.getErrorCode() != null)
                                onNumberMigrationFail();
                            else
                                onNumberMigrationSucess();
                    }
                }
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }


    }

    private String getCode(boolean forService) {
        String codeEntered = "";
        if (forService) {
            codeEntered += code1.getText().toString();
            codeEntered += "-";
            codeEntered += code2.getText().toString();
            codeEntered += "-";
            codeEntered += code3.getText().toString();
            codeEntered += "-";
            codeEntered += code4.getText().toString();
        } else {
            codeEntered += code1.getText().toString();
            codeEntered += code2.getText().toString();
            codeEntered += code3.getText().toString();
            codeEntered += code4.getText().toString();
        }
        return codeEntered;
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        try {
            regActivity.onBackPressed();
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        logEvent();
        regActivity.addFragmnet(new LoadingFragmnet(), R.id.loadingFragmeLayout, true);
        verfyMsisidnUsingEidInput = new VerfyMsisidnUsingEidInput();
        verfyMsisidnUsingEidInput.setAppVersion(appVersion);
        verfyMsisidnUsingEidInput.setToken(token);
        verfyMsisidnUsingEidInput.setOsVersion(osVersion);
        verfyMsisidnUsingEidInput.setChannel(channel);
        verfyMsisidnUsingEidInput.setDeviceId(deviceId);
        verfyMsisidnUsingEidInput.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        verfyMsisidnUsingEidInput.setLang(currentLanguage);
        if (getArguments() != null && getArguments().getString(ConstantUtils.mobileNo) != null)
            verfyMsisidnUsingEidInput.setMsisdn(getArguments().getString(ConstantUtils.mobileNo));

        verfyMsisidnUsingEidInput.setEmiratesId(getCode(true));
        new VerfiyMsisdByEidService(this, verfyMsisidnUsingEidInput);

    }

    private void setListeners() {
        verfyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()) {
                    onConsumeService();
                }
            }
        });

        fullnameTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (fullnameTxt.getText().toString().trim().length() > 0 && getCode(false).length() > 0) {
                    enableVerifyBtn();
                } else {
                    disableVerifyBtn();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


    }

    private void onNumberMigrationSucess() {
        status = verfyMsisidnUsingEidOutput.getStatus();
        switch (status) {
            case 1: // eligable
                break;
            case 2: // not eligable due to un paid amount
                break;
            case 3: // Not eligible due to migrate to Indy
                break;
            case 4: //Failure
                if (verfyMsisidnUsingEidOutput.getErrorMsgEn() != null) {
                    if (currentLanguage.equals("en")) {
                        showErrorFragment(verfyMsisidnUsingEidOutput.getErrorMsgEn());
                    } else {
                        showErrorFragment(verfyMsisidnUsingEidOutput.getErrorMsgAr());
                    }

                } else {
                    showErrorFragment(getString(R.string.error_));
                }
                break;
        }
    }

    private void onNumberMigrationFail() {
        showErrorFragment(verfyMsisidnUsingEidOutput.getErrorMsgEn());
    }

    private boolean isValid() {
        boolean isValid = true;
        if (fullnameTxt.getText().toString().trim().length() == 0) {
            setTextInputLayoutError(til_name, getString(R.string.enter_mobile_no));
            isValid = false;
        } else {
            removeTextInputLayoutError(til_name);
        }
//        if (getCode(false).length() == 0) {
//            setTextInputLayoutError(til_ID, getString(R.string.please_enter_password));
//            isValid = false;
//
//        }
        return isValid;
    }

    private void disableVerifyBtn() {
        verfyBtn.setAlpha(0.5f);
        verfyBtn.setEnabled(false);
    }

    private void enableVerifyBtn() {
        verfyBtn.setAlpha(1.0f);
        verfyBtn.setEnabled(true);
    }

    private void showErrorFragment(String error) {
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, error);
        ErrorFragment errorFragment = new ErrorFragment();
        regActivity.replaceFragmnet(errorFragment, R.id.frameLayout, false);
    }


    private void showLoadingFragment() {
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.loadingString, getString(R.string.verifying_code));
        LoadingFragmnet loadingFragmnet = new LoadingFragmnet();
        loadingFragmnet.setArguments(bundle);
        regActivity.addFragmnet(loadingFragmnet, R.id.frameLayout, true);
    }


    public class CustomTextWatcher implements TextWatcher {
        private EditText view;

        private CustomTextWatcher(EditText view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

            String text = editable.toString();
            if ((tv_digits.indexOf(view) == 0) && text.length() == 3) {
                moveToNext();
            } else if ((tv_digits.indexOf(view) == 1) && text.length() == 4) {
                moveToNext();
            } else if ((tv_digits.indexOf(view) == 2) && text.length() == 7) {
                moveToNext();
            } else if ((tv_digits.indexOf(view) == 3) && text.length() == 1) {
                moveToNext();
            } else
                disableVerifyBtn();
        }

        public void moveToNext() {
            view.clearFocus();

            int currentIndex = tv_digits.indexOf(view);
            if (getCode(false).length() == NO_OF_DIGITS) {
                enableVerifyBtn();
            }
            if (currentIndex == 3) {
                hideKeyBoard();
                if (getCode(false).length() == NO_OF_DIGITS) {
                    enableVerifyBtn();
                }
            } else {
                for (int i = currentIndex + 1; i < tv_digits.size(); i++) {
                    tv_digits.get(i).requestFocus();

                    return;

                }
            }
        }
    }


    private void logEvent() {
        logEventInputModel.setScreenId(ConstantUtils.SCREEN_ID_3032);

        logEventInputModel.setReviewFullName(fullnameTxt.getText().toString());
        logEventInputModel.setReviewEmiratesId(getCode(true));
        logEventInputModel.setReviewVerify(verfyBtn.getText().toString());

        new LogApplicationFlowService(this, logEventInputModel);
    }
}
