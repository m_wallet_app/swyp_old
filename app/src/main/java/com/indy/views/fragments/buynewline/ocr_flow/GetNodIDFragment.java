package com.indy.views.fragments.buynewline.ocr_flow;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.indy.R;
import com.indy.models.utils.BaseResponseModel;
import com.indy.ocr.scanning.CameraActivity;
import com.indy.ocr.scanning.EmiratesIdDataObject;
import com.indy.ocr.scanning.UploadEmiratesID;
import com.indy.ocr.screens.EmiratesIdDetailsFragment;
import com.indy.ocrStep2.ocr.UploadEmiratesIdOutput;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.buynewline.exisitingnumber.EmiratesIDVerificationFragment;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.indy.views.fragments.utils.MasterFragment;
import com.kofax.kmc.ken.engines.data.Image;
import com.kofax.kmc.kut.utilities.AppContextProvider;
import com.kofax.kmc.kut.utilities.Licensing;
import com.kofax.mobile.sdk.extract.server.IServerExtractor;

import static com.indy.controls.AdjustEvents.AGE_ELIGIBILITY_SUCCESS;
import static com.indy.ocr.scanning.Constants.CAPTURE_BACK;
import static com.indy.ocr.scanning.Constants.CAPTURE_FRONT;
import static com.indy.ocr.scanning.Constants.SIDE;
import static com.indy.ocr.scanning.Constants.back_image;
import static com.indy.ocr.scanning.Constants.front_image;
import static com.indy.utils.ConstantUtils.TAGGING_id_scan_back;
import static com.indy.utils.ConstantUtils.TAGGING_id_scan_front;

/**
 * A simple {@link Fragment} subclass.
 */
public class GetNodIDFragment extends MasterFragment {

    Button bt_nextBtn;
    View rootView;

    public GetNodIDFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_get_nod_id, container, false);
        initUI();
        return rootView;
    }

    @Override
    public void initUI() {
        super.initUI();
        // Initializing the SDK
        AppContextProvider.setContext(getActivity().getApplicationContext());
        Licensing.setMobileSDKLicense(ConstantUtils.KOFAX_LICENSE);
        bt_nextBtn = (Button) rootView.findViewById(R.id.nextBtn);
        bt_nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CameraActivity.class);
                intent.putExtra(SIDE, CAPTURE_FRONT);
                startActivityForResult(intent, 100);
                logEvent(AGE_ELIGIBILITY_SUCCESS, new Bundle());
//                regActivity.startActivity(new Intent(regActivity, CameraActivity.class));
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 100:
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), TAGGING_id_scan_front);
                Log.d("getmFirebaseAnalytics()", "TAGGING_id_scan_front");
                if (resultCode != 120 && resultCode != getActivity().RESULT_CANCELED) {
                    regActivity.addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            final Intent intent = new Intent(getActivity(), CameraActivity.class);
                            intent.putExtra(SIDE, CAPTURE_BACK);
                            startActivityForResult(intent, 200);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        regActivity.getSupportFragmentManager().popBackStack();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, 1000);
                        }
                    }, 1000);
                } else if (resultCode == 120) {
                    regActivity.replaceFragmnet(new EmiratesIDVerificationFragment(), R.id.frameLayout, true);

                }
                break;

            case 200:
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), TAGGING_id_scan_back);
                Log.d("getmFirebaseAnalytics()", "TAGGING_id_scan_back");
                if (resultCode == 100) {
//                    Toast.makeText(getActivity(), "good capture", Toast.LENGTH_LONG).show();

                    sendRequest();
                } else {
//                    Toast.makeText(getActivity(), "error in capture", Toast.LENGTH_LONG).show();
                }
                break;
            case 1001:
                break;
        }

    }

    public void sendRequest() {

        try {
//        addFragmnet(new LoadingFragmnet(),R.id.frameLayout,true);
            final IServerExtractor serverExtractor;
//        this.mContext = this.getApplicationContext();
//        mParameters = new HashMap<>();
//        certificateValidatorListener = new CertificateValidatorListenerImpl();
//        List<Image> images = new ArrayList<>();

            front_image.setImageDPI(500);
            back_image.setImageDPI(500);
            front_image.setImageMimeType(Image.ImageMimeType.MIMETYPE_JPEG);
            back_image.setImageMimeType(Image.ImageMimeType.MIMETYPE_JPEG);

//        images.add(front_image);
//        images.add(back_image);

            regActivity.addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
            new UploadEmiratesID(this, front_image, back_image, getActivity());


//            serverExtractor = ServerBuilder.build(this.mContext, ServerBuilder.ServerType.RTTI);
//            mParameters.put("xImageResize", "ID-1");
//            mParameters.put("ProcessCount", "2");
//            mParameters.put("xExtractSignatureImage", "True");
//            mParameters.put("processImage", "False");
//            mParameters.put("xRegion", "Asia");
//            mParameters.put("xIdType", "ID");
//            mParameters.put("xExtractFaceImage", "True");
//            ServerExtractionParameters extractionParameters = new ServerExtractionParameters(URL, images, null, certificateValidatorListener, mParameters,null);
//            serverExtractor.extractData(extractionParameters, this);
        } catch (Exception ex) {
            Log.d("indyocr", ex.getMessage());
        }
    }

    EmiratesIdDataObject emiratesIdDataObject;

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            if (responseModel != null) {
                regActivity.onBackPressed();
                UploadEmiratesIdOutput uploadEmiratesIdOutput = (UploadEmiratesIdOutput) responseModel.getResultObj();
                if (uploadEmiratesIdOutput != null && uploadEmiratesIdOutput.getErrorResult() != null &&
                        uploadEmiratesIdOutput.getErrorResult().getErrorDescription() == null) {
                    emiratesIdDataObject = CommonMethods.getEmiratesIdObject(uploadEmiratesIdOutput.getFields());

                    EmiratesIdDetailsFragment emiratesIdDetailsFragment = new EmiratesIdDetailsFragment();
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(ConstantUtils.EMIRATES_ID, emiratesIdDataObject);
                    emiratesIdDetailsFragment.setArguments(bundle);
                    regActivity.replaceFragmnet(emiratesIdDetailsFragment, R.id.frameLayout, true);
//                    Toast.makeText(regActivity, "no error received", Toast.LENGTH_SHORT).show();
//                uploadEmiratesIdOutput.getClassificationResult().get(i).
                } else {
                    ErrorFragment errorFragment = new ErrorFragment();
                    Bundle bundle = new Bundle();
                    if (uploadEmiratesIdOutput.getErrorResult() != null) {
                        bundle.putString(ConstantUtils.errorString, uploadEmiratesIdOutput.getErrorResult().getErrorDescription().toString());
                    } else {
                        bundle.putString(ConstantUtils.errorString, "Error!");
                    }
                    errorFragment.setArguments(bundle);
                    regActivity.addFragmnet(new ErrorFragment(), R.id.frameLayout, true);
                }
            } else {
                ErrorFragment errorFragment = new ErrorFragment();
                Bundle bundle = new Bundle();
                bundle.putString(ConstantUtils.errorString, "Error!");

                errorFragment.setArguments(bundle);
                regActivity.addFragmnet(new ErrorFragment(), R.id.frameLayout, true);
            }
        } catch (Exception ex) {
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        regActivity.onBackPressed();

    }

}
