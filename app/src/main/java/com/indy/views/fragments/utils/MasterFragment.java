package com.indy.views.fragments.utils;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.adjust.sdk.Adjust;
import com.adjust.sdk.AdjustEvent;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.controls.BaseInterface;
import com.indy.helpers.RetrofitErrorHandeler;
import com.indy.helpers.ValidationHelper;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.utils.IndyApplication;
import com.indy.utils.SharedPrefrencesManger;
import com.indy.views.activites.MasterActivity;
import com.indy.views.activites.RegisterationActivity;
import com.indy.views.activites.UnAuthorizedTokenActivity;

import java.util.Set;

/**
 * Created by emad on 8/2/16.
 */
public class MasterFragment extends Fragment implements BaseInterface {
    public RegisterationActivity regActivity;
    public String appVersion, token, osVersion, channel, deviceId, storeId, currentLanguage, authToken;
    public SharedPrefrencesManger sharedPrefrencesManger;
    private RetrofitErrorHandeler retrofitErrorHandeler;
    public ValidationHelper validationHelper;
    protected FirebaseAnalytics mFirebaseAnalytics;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        sharedPrefrencesManger = ((IndyApplication) getActivity().getApplicationContext()).sharedPrefrencesManger;
        ((MasterActivity) getActivity()).switchLocalizaion(sharedPrefrencesManger.getLanguage(), null);

        initUI();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {


    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        if (masterErrorResponse != null)
            if (masterErrorResponse.getErrorCode() != null) {
                if (getActivity() != null) {
                    Intent intent = new Intent(getActivity(), UnAuthorizedTokenActivity.class);
                    if (currentLanguage.equalsIgnoreCase("en"))
                        intent.putExtra("msg", masterErrorResponse.getErrorMsgEn());
                    else
                        intent.putExtra("msg", masterErrorResponse.getErrorMsgAr());
                    startActivity(intent);
                }

            }

    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        try {
            if (responseModel != null && responseModel.getResultObj() != null) {
                Throwable t = (Throwable) responseModel.getResultObj();
                retrofitErrorHandeler = new RetrofitErrorHandeler();
                retrofitErrorHandeler.handleError(t, getContext());
                if (RegisterationActivity.logEventInputModel != null) {
                    if (responseModel.getResultObj() != null) {
                        MasterErrorResponse masterErrorResponse = (MasterErrorResponse) responseModel.getResultObj();
                        if (masterErrorResponse != null && masterErrorResponse.getErrorMsgEn() != null) {

                            RegisterationActivity.logEventInputModel.setErrorCode(masterErrorResponse.getErrorMsgEn());

                        }
                    }

                }
            }
        } catch (Exception ex) {
//            ((MasterActivity) getActivity()).addFragmnet(new ErrorFragment(), R.id.frameLayout,true);
            if (ex != null) {
                ex.printStackTrace();
            }
        }

    }

    public FirebaseAnalytics getmFirebaseAnalytics() {
        return mFirebaseAnalytics;
    }

    public void setmFirebaseAnalytics(FirebaseAnalytics mFirebaseAnalytics) {
        this.mFirebaseAnalytics = mFirebaseAnalytics;
    }

    public void initUI() {

        sharedPrefrencesManger = new SharedPrefrencesManger(getActivity());
        validationHelper = new ValidationHelper();
        regActivity = RegisterationActivity.getRegInstance();
        CommonMethods.setStatusBarTransparent(getActivity());
        appVersion = sharedPrefrencesManger.getAppVersion();
        token = sharedPrefrencesManger.getToken();
        osVersion = ConstantUtils.osVersion;
        channel = ConstantUtils.channel;
        deviceId = sharedPrefrencesManger.getDeviceId();
        storeId = "indyStore";
        currentLanguage = sharedPrefrencesManger.getLanguage();
        authToken = sharedPrefrencesManger.getAuthToken();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        CommonMethods.firebaseAnalytics = mFirebaseAnalytics;

        Log.v("deviceID", deviceId + "");

    }


    @Override
    public void onConsumeService() {

        hideKeyBoard();

    }

    public void hideKeyBoard() {
        if (getActivity() != null) {
            View view = getActivity().getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    private Animation loadAnimation(int animType) {
        Animation animation = AnimationUtils.loadAnimation(getActivity(), animType);
        animation.setFillEnabled(true);
        animation.setFillAfter(true);
        return animation;
    }

//    public void showErrorDalioge(String msg) {
//        new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
//                .setTitleText(msg)
//                .setContentText("")
//                .show();
//
//    }

    public void setTextInputLayoutError(TextInputLayout textInputLayoutError, String error) {
        textInputLayoutError.setError(error);
    }

    public void removeTextInputLayoutError(TextInputLayout textInputLayoutError) {
        textInputLayoutError.setError(null);
    }

    public void removeEditTextLayoutError(EditText textInputLayoutError) {
        textInputLayoutError.setError(null);
    }

    /**
     * log event on Adjust
     *
     * @param eventTag event tag
     * @param bundle   bundle to set params
     */
    public void logEvent(String eventTag, Bundle bundle) {
        try {
            //Adjust Event
            AdjustEvent event = new AdjustEvent(eventTag);
            if (bundle != null && !bundle.isEmpty()) {
                Set<String> keys = bundle.keySet();
                for (String key : keys) {
                    if (bundle.getString(key) != null)
                        event.addPartnerParameter(key, bundle.getString(key));
                }
            }
            Adjust.trackEvent(event);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
