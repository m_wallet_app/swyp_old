package com.indy.views.fragments.buynewline.ocr_flow;


import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.indy.R;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.utils.MasterFragment;

import static com.indy.R.id.nextBtn;

/**
 * A simple {@link Fragment} subclass.
 */
public class EmailInputFragment extends MasterFragment {

    private View rootView;
    private Button bt_next;
    private EditText fullName, email;
    public static String fullnameStr, emailStr;
    private TextInputLayout fullNameTxtInput, emailTxtInput;
    private CharSequence emailCharSeq;
    public static String emailString;
    public EmailInputFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_email_input, container, false);
        initUI();
        return rootView;
    }

    @Override
    public void initUI() {
        super.initUI();
        fullName = (EditText) rootView.findViewById(R.id.fullName);
        email = (EditText) rootView.findViewById(R.id.email);
        emailTxtInput = (TextInputLayout) rootView.findViewById(R.id.emailTxtInput);

        fullNameTxtInput = (TextInputLayout) rootView.findViewById(R.id.fullNameTxtInput);
        bt_next = (Button) rootView.findViewById(nextBtn);

        bt_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailString = email.getText().toString();
                regActivity.replaceFragmnet(new GetNodIDFragment(), R.id.frameLayout, true);
            }
        });
        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
                emailCharSeq = s;
                if (emailCharSeq != null)
                    if (s.length() == 0 || emailCharSeq.length() == 0)
                        disableNextBtn();
                    else
                        enableNextBtn();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
            }
        });
    }

    private void enableNextBtn() {
        bt_next.setAlpha(ConstantUtils.ALPHA_BUTTON_ENABLED);
        bt_next.setClickable(true);
    }

    private void disableNextBtn() {
        bt_next.setAlpha(ConstantUtils.ALPHA_BUTTON_DISABLED);
        bt_next.setClickable(false);
    }
}
