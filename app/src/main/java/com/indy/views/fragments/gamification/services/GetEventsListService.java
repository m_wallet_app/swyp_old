package com.indy.views.fragments.gamification.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.BaseServiceManger;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetEventsListInputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetEventsListOutputModel;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**

 */
public class GetEventsListService extends BaseServiceManger {
    private final boolean activeEvents;
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private GetEventsListInputModel getEventsListInputModel;


    public GetEventsListService(BaseInterface baseInterface, GetEventsListInputModel getEventsListInputModel,boolean activeEvents) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.getEventsListInputModel = getEventsListInputModel;
        this.activeEvents = activeEvents;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<GetEventsListOutputModel> call =activeEvents?apiService.getEventsList(getEventsListInputModel):apiService.getPreviousEventsList(getEventsListInputModel);
        call.enqueue(new Callback<GetEventsListOutputModel>() {
            @Override
            public void onResponse(Call<GetEventsListOutputModel> call, Response<GetEventsListOutputModel> response) {
                mBaseResponseModel.setServiceType(ServiceUtils.getEventsListService);
                mBaseResponseModel.setResultObj(response.body());

                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (response.body().getErrorMsgEn() != null) {
//                        String urlForTag = call.request().url().toString();
//                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
//                        urlForTag = urlForTag.replace("/", "_");
//                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<GetEventsListOutputModel> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.getEventsListService);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
