package com.indy.views.fragments.gamification.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.indy.R;
import com.indy.utils.SharedPrefrencesManger;
import com.indy.views.fragments.gamification.OnBadgeListClicked;
import com.indy.views.fragments.gamification.models.getrafflesdetails.WinnersListModel;

import java.util.List;

public class RaffleWinnersAdapter extends RecyclerView.Adapter<RaffleWinnersAdapter.ViewHolder> {

    private Context mContext;
    private SharedPrefrencesManger sharedPrefrencesManger;
    private List<WinnersListModel> winnersListModels;
    private OnBadgeListClicked onBadgeListClicked;

    public RaffleWinnersAdapter(List<WinnersListModel> winnersListModels, Context context) {

        this.mContext = context;
        this.sharedPrefrencesManger = new SharedPrefrencesManger(context);
        this.winnersListModels = winnersListModels;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.item_winner_raffle, viewGroup, false);
        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        WinnersListModel winnersListModel= winnersListModels.get(position);

        holder.tv_name.setText(winnersListModel.getUsername());

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return winnersListModels.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_name;

        public ViewHolder(View v) {
            super(v);

            tv_name = (TextView) v.findViewById(R.id.tv_winner_name);
        }
    }


}
