package com.indy.views.fragments.gamification.models.serviceInputOutput;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mobile on 25/09/2017.
 */

public class GetWinnersListInputResponse extends GamificationBaseInputModel {


    @SerializedName("indyEventId")
    @Expose
    String indyEventId;

    public String getIndyEventId() {
        return indyEventId;
    }

    public void setIndyEventId(String indyEventId) {
        this.indyEventId = indyEventId;
    }
}
