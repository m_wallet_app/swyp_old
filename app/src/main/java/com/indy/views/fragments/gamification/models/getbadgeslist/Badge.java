package com.indy.views.fragments.gamification.models.getbadgeslist;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Tohamy on 10/1/2017.
 */

public class Badge implements Parcelable {

    @SerializedName("badgeId")
    @Expose
    private String badgeId;
    @SerializedName("imageUrlCompleted")
    @Expose
    private String imageUrlCompleted;
    @SerializedName("imageUrlInProgress")
    @Expose
    private String imageUrlInProgress;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description1")
    @Expose
    private String description1;
    @SerializedName("description2")
    @Expose
    private String description2;
    @SerializedName("description3")
    @Expose
    private String description3;
    @SerializedName("description4")
    @Expose
    private String description4;
    @SerializedName("completed")
    @Expose
    private boolean completed;
    @SerializedName("progress")
    @Expose
    private int progress;
    @SerializedName("completionCount")
    @Expose
    private int completionCount;
    @SerializedName("rewardAmount")
    @Expose
    private int rewardAmount;
    @SerializedName("rewardImage")
    @Expose
    private String rewardImage;

    public String getBadgeId() {
        return badgeId;
    }

    public void setBadgeId(String badgeId) {
        this.badgeId = badgeId;
    }

    public String getImageUrlCompleted() {
        return imageUrlCompleted;
    }

    public void setImageUrlCompleted(String imageUrlCompleted) {
        this.imageUrlCompleted = imageUrlCompleted;
    }

    public String getImageUrlInProgress() {
        return imageUrlInProgress;
    }

    public void setImageUrlInProgress(String imageUrlInProgress) {
        this.imageUrlInProgress = imageUrlInProgress;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription1() {
        return description1;
    }

    public void setDescription1(String description1) {
        this.description1 = description1;
    }

    public String getDescription2() {
        return description2;
    }

    public void setDescription2(String description2) {
        this.description2 = description2;
    }

    public String getDescription3() {
        return description3;
    }

    public void setDescription3(String description3) {
        this.description3 = description3;
    }

    public String getDescription4() {
        return description4;
    }

    public void setDescription4(String description4) {
        this.description4 = description4;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public int getCompletionCount() {
        return completionCount;
    }

    public void setCompletionCount(int completionCount) {
        this.completionCount = completionCount;
    }

    public int getRewardAmount() {
        return rewardAmount;
    }

    public void setRewardAmount(int rewardAmount) {
        this.rewardAmount = rewardAmount;
    }

    public String getRewardImage() {
        return rewardImage;
    }

    public void setRewardImage(String rewardImage) {
        this.rewardImage = rewardImage;
    }

    protected Badge(Parcel in) {
        completed = in.readByte() == 1;
        imageUrlCompleted = in.readString();
        imageUrlInProgress = in.readString();
        title = in.readString();
        description1 = in.readString();
        description2 = in.readString();
        description3 = in.readString();
        description4 = in.readString();
        progress = in.readInt();
        completionCount = in.readInt();
        rewardAmount = in.readInt();
        rewardImage = in.readString();
        badgeId = in.readString();
    }

    public static final Creator<Badge> CREATOR = new Creator<Badge>() {
        @Override
        public Badge createFromParcel(Parcel in) {
            return new Badge(in);
        }

        @Override
        public Badge[] newArray(int size) {
            return new Badge[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(completed ? 1 : 0);
        parcel.writeString(imageUrlCompleted);
        parcel.writeString(imageUrlInProgress);
        parcel.writeString(title);
        parcel.writeString(description1);
        parcel.writeString(description2);
        parcel.writeString(description3);
        parcel.writeString(description4);
        parcel.writeInt(progress);
        parcel.writeInt(completionCount);
        parcel.writeInt(rewardAmount);
        parcel.writeString(rewardImage);
        parcel.writeString(badgeId);
    }
}
