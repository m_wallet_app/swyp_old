package com.indy.views.fragments.buynewline.exisitingnumber;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.controls.ServiceUtils;
import com.indy.models.checkmisidenoperator.CheckMisidenOperatorOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterInputResponse;
import com.indy.services.CheckMsisdnOperatorService;
import com.indy.services.LogApplicationFlowService;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.indy.views.fragments.utils.MasterFragment;

import static com.indy.views.activites.RegisterationActivity.logEventInputModel;

/**
 * Created by emad on 9/25/16.
 */

public class NumberTransferFragment extends MasterFragment {

    private View view;
    private Button nextBtn;
    private EditText mobileNO;
    private MasterInputResponse masterInputResponse;
    private CheckMisidenOperatorOutput checkMisidenOperatorOutput;
    private int stausCode;
    public static NumberTransferFragment numberTransferFragment;
    private TextInputLayout til_number;
    public static String mobileNoTxt;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_number_transfer, container, false);
        initUI();
        setListeners();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
        regActivity.setHeaderTitle("");
        regActivity.showHeaderLayout();
        numberTransferFragment = this;
        nextBtn = (Button) view.findViewById(R.id.nextBtn);
        mobileNO = (EditText) view.findViewById(R.id.mobileNo);

        til_number = (TextInputLayout) view.findViewById(R.id.number_input_layout);
        mobileNO.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (mobileNO.getText().toString().trim().length() > 0) {
                    enableVerifyButton();
                } else {
                    disableVerifyButton();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        disableVerifyButton();
    }

    private void setListeners() {
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()) {


                    Bundle bundle2 = new Bundle();

                    bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_migration_number_entered);
                    bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_migration_number_entered);
                    mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_migration_number_entered, bundle2);

                    onConsumeService();
                }
//                else {
//                    showErrorFragment();
//                }
            }
        });


    }

    private void showErrorFragment(String error) {
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, error);
        ErrorFragment errorFragment = new ErrorFragment();
        errorFragment.setArguments(bundle);
        regActivity.replaceFragmnet(errorFragment, R.id.frameLayout, true);


    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        regActivity.addFragmnet(new LoadingFragmnet(), R.id.loadingFragmeLayout, true);
        masterInputResponse = new MasterInputResponse();
        masterInputResponse.setAppVersion(appVersion);
        masterInputResponse.setToken(token);
        masterInputResponse.setOsVersion(osVersion);
        masterInputResponse.setChannel(channel);
        masterInputResponse.setDeviceId(deviceId);
        masterInputResponse.setMsisdn(mobileNO.getText().toString().trim());
        new CheckMsisdnOperatorService(NumberTransferFragment.this, masterInputResponse);

    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            if (isVisible()) {
                if(responseModel!=null && responseModel.getServiceType()!=null && !responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.LOG_USER_EVENT)) {
                    regActivity.onBackPressed();
                    if (responseModel != null && responseModel.getResultObj() != null) {
                        checkMisidenOperatorOutput = (CheckMisidenOperatorOutput) responseModel.getResultObj();
                        if (checkMisidenOperatorOutput != null)
                            onCheckMsisdenNumberSucess();
                    }
                }

            }
        }catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    private void onCheckMsisdenNumberSucess() {
        if (checkMisidenOperatorOutput.getErrorCode() != null) {
            onCheckMsisdenNumberError();
        } else {
            mobileNoTxt = mobileNO.getText().toString().trim();
            logEvent();
            validateStuts();
        }
    }
    private void logEvent() {

       logEventInputModel.setScreenId(ConstantUtils.SCREEN_ID_3001);

        logEventInputModel.setMigrationNumber(mobileNoTxt);

        new LogApplicationFlowService(this, logEventInputModel);
    }

    private void onCheckMsisdenNumberError() {
        if (currentLanguage.equalsIgnoreCase("en"))
            showErrorFragment(checkMisidenOperatorOutput.getErrorMsgEn());
        else
            showErrorFragment(checkMisidenOperatorOutput.getErrorMsgAr());
//        onDueNumber();
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
//        if (isVisible()) {
//            regActivity.onBackPressed();
//            showErrorFragment(getString(R.string.error_));
//
//        }

    }

    private void validateStuts() {
        stausCode = checkMisidenOperatorOutput.getStatus();
        if (stausCode == 1) { // this number is etisalat number.
            onEtisalatNumber();
//            onDueNumber();
        }
        if (stausCode == 2) {// this number is Du number
            onDueNumber();
        }
    }

    private void onDueNumber() {
        NonEtisaaltFragment nonEtisaaltFragment = new NonEtisaaltFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.mobileNo, mobileNO.getText().toString().trim());
        nonEtisaaltFragment.setArguments(bundle);
        regActivity.replaceFragmnet(nonEtisaaltFragment, R.id.frameLayout, true);
        Bundle bundle2 = new Bundle();

        bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_migration_invalid_number_entered);
        bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_migration_invalid_number_entered);
        mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_migration_invalid_number_entered, bundle2);

    }

    private void onEtisalatNumber() {
        NumberMigrationFragment numberMigrationFragment = new NumberMigrationFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.mobileNo, mobileNO.getText().toString().trim());
        numberMigrationFragment.setArguments(bundle);
        regActivity.replaceFragmnet(numberMigrationFragment, R.id.frameLayout, true);
        Bundle bundle2 = new Bundle();

        bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_migration_valid_number_entered);
        bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_migration_valid_number_entered);
        mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_migration_valid_number_entered, bundle2);
    }

    private boolean isValid() {
        boolean isValid = true;
        if (mobileNO.getText().toString().length() < 10) {
            isValid = false;
            til_number.setError(getString(R.string.invalid_delivery_number));
        } else {
            String noStr = mobileNO.getText().toString().substring(0, 2);
            if (!noStr.equalsIgnoreCase("05")) {
                isValid = false;
                til_number.setError(getString(R.string.invalid_delivery_number));
            } else {
                removeTextInputLayoutError(til_number);
            }
        }
        return isValid;
    }

    private void enableVerifyButton() {
        nextBtn.setAlpha(1.0f);
        nextBtn.setEnabled(true);
    }

    private void disableVerifyButton() {
        nextBtn.setAlpha(0.5f);
        nextBtn.setEnabled(false);
    }
}
