package com.indy.views.fragments.gamification.models.serviceInputOutput;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mobile on 21/12/2017.
 */

public class GetRewardForSharingInputModel extends GamificationBaseInputModel {

    @SerializedName("actionName")
    @Expose
    private String actionName;


    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }
}
