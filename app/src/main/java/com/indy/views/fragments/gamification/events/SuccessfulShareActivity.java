package com.indy.views.fragments.gamification.events;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.ShareMasterActivity;
import com.indy.views.fragments.gamification.models.getnotificationpopupdetails.GetNotificationPopupDetailsInput;
import com.indy.views.fragments.gamification.models.getnotificationpopupdetails.GetNotificationPopupDetailsOutput;
import com.indy.views.fragments.gamification.services.GetNotificationPopupDetailsService;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;

import static com.indy.R.id.tv_subtitle;

public class SuccessfulShareActivity extends ShareMasterActivity {
    private TextView tv_title;
    private TextView tvSubtitle;
    //    private TextView tvRewardsText;
    private TextView tvRewardsCount;
    private TextView tvTotalRaffleTickets;
    private TextView tvCancel;
    private Button continueBtn;
    private String notificationId;
    FrameLayout frameLayout;

    private TextView fbShare;
    private TextView twitterShare;

    private CoordinatorLayout coordinatorLayout;
    private BottomSheetBehavior behavior;
    private View bottomSheet;
    GetNotificationPopupDetailsOutput getNotificationPopupDetailsOutput;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_successful_share);
        initUI();
    }


    @Override
    public void initUI() {
        super.initUI();
        tvSubtitle = (TextView) findViewById(tv_subtitle);
        frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
//        tvRewardsText = (TextView) findViewById(R.id.tv_rewards_text);
        tvRewardsCount = (TextView) findViewById(R.id.tv_rewards_count);
        tvTotalRaffleTickets = (TextView) findViewById(R.id.tv_total_raffle_tickets);
        tvCancel = (TextView) findViewById(R.id.tv_cancel);
        continueBtn = (Button) findViewById(R.id.continueBtn);
        tv_title = (TextView) findViewById(R.id.tv_title);
        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                sharedPrefrencesManger.setNavigationIndex("2");
//                ChallengesMainFragment challengesMainFragment = new ChallengesMainFragment();
//                Bundle bundle = new Bundle();
//                bundle.putString(ConstantUtils.CHALLENGES_TAB_NAVIGATION_INDEX, "3");
//                challengesMainFragment.setArguments(bundle);
//                SwpeMainActivity.swpeMainActivityInstance.replaceFragmnet(challengesMainFragment, R.id.contentFrameLayout, true);
//                SwpeMainActivity.clickedItem = SwpeMainActivity.challengesClicked;


            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        if (getIntent() != null && getIntent().getExtras() != null) {
            notificationId = getIntent().getExtras().getString(ConstantUtils.KEY_NOTIFICATION_ID_FOR_SERVICE, "");

        }
        if (notificationId != null && !notificationId.isEmpty()) {
            onConsumeService();
        } else {
            finish();
        }

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinateLayoutID);
        bottomSheet = coordinatorLayout.findViewById(R.id.share_photo_bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {

                } else if (newState == BottomSheetBehavior.STATE_EXPANDED) {

                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });
        fbShare = (TextView) findViewById(R.id.fb_share);
        twitterShare = (TextView) findViewById(R.id.twitter_share);
        fbShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                referenceID = getNotificationPopupDetailsOutput.getId();
                facebookShare(getNotificationPopupDetailsOutput.getId(),"");
            }
        });
        twitterShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                referenceID = getNotificationPopupDetailsOutput.getId();
                twitterLogin(true, getNotificationPopupDetailsOutput.getId(),"");
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
        if(sharedPrefrencesManger.getNavigationIndex().length() > 0 || sharedPrefrencesManger.getNavigationBadgeIndex().length() >0){
            finish();
        }
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        frameLayout.setVisibility(View.VISIBLE);
        addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        GetNotificationPopupDetailsInput getNotificationPopupDetailsInput = new GetNotificationPopupDetailsInput();
        getNotificationPopupDetailsInput.setChannel(channel);
        getNotificationPopupDetailsInput.setLang(currentLanguage);
        getNotificationPopupDetailsInput.setMsisdn(sharedPrefrencesManger.getMobileNo());
        getNotificationPopupDetailsInput.setAppVersion(appVersion);
        getNotificationPopupDetailsInput.setAuthToken(authToken);
        getNotificationPopupDetailsInput.setDeviceId(deviceId);
        getNotificationPopupDetailsInput.setToken(token);
        getNotificationPopupDetailsInput.setImsi(sharedPrefrencesManger.getMobileNo());
        getNotificationPopupDetailsInput.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        getNotificationPopupDetailsInput.setOsVersion(osVersion);

        getNotificationPopupDetailsInput.setApplicationId(ConstantUtils.INDY_TALOS_ID);
        getNotificationPopupDetailsInput.setUserId(sharedPrefrencesManger.getUserId());
        getNotificationPopupDetailsInput.setUserSessionId(sharedPrefrencesManger.getUserSessionId());
        getNotificationPopupDetailsInput.setNotificationId(notificationId);
        new GetNotificationPopupDetailsService(this, getNotificationPopupDetailsInput);

    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        onBackPressed();
        if (responseModel != null && responseModel.getResultObj() != null) {
            getNotificationPopupDetailsOutput = (GetNotificationPopupDetailsOutput) responseModel.getResultObj();
            if (getNotificationPopupDetailsOutput != null) {
                onGetNotificationSuccess();
            } else {
                onGetNotificationFailure();
            }
        } else {
            onGetNotificationFailure();
        }
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        onBackPressed();
    }

    private void onGetNotificationFailure() {
        showErrorFragment();
    }

    public void showErrorFragment() {
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        if (getNotificationPopupDetailsOutput != null && getNotificationPopupDetailsOutput.getErrorMsgEn() != null) {
            if (sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)) {
                bundle.putString(ConstantUtils.errorString, getNotificationPopupDetailsOutput.getErrorMsgEn());
            } else {
                bundle.putString(ConstantUtils.errorString, getNotificationPopupDetailsOutput.getErrorMsgAr());
            }
        } else {
            bundle.putString(ConstantUtils.errorString, getString(R.string.generice_error));
        }

        errorFragment.setArguments(bundle);
        frameLayout.setVisibility(View.VISIBLE);
        addFragmnet(errorFragment, R.id.frameLayout, true);

    }

    private void onGetNotificationSuccess() {

        if (getNotificationPopupDetailsOutput != null) {

            if(getNotificationPopupDetailsOutput.getTitle()!=null)
            tv_title.setText(getNotificationPopupDetailsOutput.getTitle());

            if(getNotificationPopupDetailsOutput.getText()!=null)
            tvSubtitle.setText(getNotificationPopupDetailsOutput.getText());

            if(getNotificationPopupDetailsOutput.getRewardText()!=null)
            tvRewardsCount.setText("+ " + getNotificationPopupDetailsOutput.getRewardText());//+ " " + getString(R.string.raffle_tickets));

            if(getNotificationPopupDetailsOutput.getTotalRaffleTickets()!=null)
            tvTotalRaffleTickets.setText(getNotificationPopupDetailsOutput.getTotalRaffleTickets());
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (frameLayout.getVisibility() == View.VISIBLE) {
            frameLayout.setVisibility(View.GONE);
        } else {
            finish();
        }
    }


}
