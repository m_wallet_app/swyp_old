package com.indy.views.fragments.buynewline;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.indy.R;
import com.indy.views.activites.HelpActivity;
import com.indy.views.fragments.utils.MasterFragment;

/**
 * Created by emad on 8/3/16.
 */
public class Greater24Fragment extends MasterFragment {
    private View view;
    private TextView help;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_24_layout, container, false);
        initUI();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
        help = (TextView) view.findViewById(R.id.help);
        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), HelpActivity.class);
                startActivity(intent);
            }
        });
    }

}
