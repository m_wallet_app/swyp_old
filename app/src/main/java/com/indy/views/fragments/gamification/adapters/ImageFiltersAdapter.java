package com.indy.views.fragments.gamification.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.indy.R;
import com.indy.utils.SharedPrefrencesManger;
import com.indy.views.fragments.gamification.OnImageFilterSelected;
import com.indy.views.fragments.gamification.models.getimagefilters.ImageFilter;

import java.util.List;

/**
 * Created by Tohamy on 10/3/2017.
 */

public class ImageFiltersAdapter extends RecyclerView.Adapter<ImageFiltersAdapter.ViewHolder> {
    private final OnImageFilterSelected onImageFilterSelected;
    private List<ImageFilter> filters;
    private Context mContext;
    private SharedPrefrencesManger sharedPrefrencesManger;

    public ImageFiltersAdapter(List<ImageFilter> filters, Context mContext, OnImageFilterSelected onImageFilterSelected) {
        this.filters = filters;
        this.mContext = mContext;
        this.onImageFilterSelected = onImageFilterSelected;
        this.sharedPrefrencesManger = new SharedPrefrencesManger(mContext);
    }


    @Override
    public ImageFiltersAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.item_image_filter, viewGroup, false);
        return new ImageFiltersAdapter.ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ImageFiltersAdapter.ViewHolder holder, final int position) {
        final ImageFilter filter = filters.get(position);
        if(position==0){
            holder.filterPhoto.setImageBitmap(filter.getBitmap());
        }else {
            holder.filterPhoto.setImageBitmap(filter.getBitmap());
//            if (filter.getUrl() != null && !filter.getUrl().isEmpty())
//                ImageLoader.getInstance().displayImage(filter.getUrl(), holder.filterPhoto);
        }
//        holder.filterPhoto.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onPhotoItemClick.onPhotoClicked(position);
//            }
//        });
        holder.filterPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onImageFilterSelected != null)
                    onImageFilterSelected.filterSelected(filter, position);
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return filters.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView filterPhoto;

        public ViewHolder(View v) {
            super(v);
            filterPhoto = (ImageView) v.findViewById(R.id.iv_filter_photo);
        }
    }
}
