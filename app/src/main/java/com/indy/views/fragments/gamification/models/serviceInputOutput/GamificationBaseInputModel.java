package com.indy.views.fragments.gamification.models.serviceInputOutput;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterInputResponse;

/**
 * Created by mobile on 24/09/2017.
 */

public class GamificationBaseInputModel extends MasterInputResponse {

    @SerializedName("userSessionId")
    @Expose
    private String userSessionId;
    @SerializedName("applicationId")
    @Expose
    private String applicationId;
    @SerializedName("userId")
    @Expose
    private String userId;

    public String getUserSessionId() {
        return userSessionId;
    }

    public void setUserSessionId(String userSessionId) {
        this.userSessionId = userSessionId;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
