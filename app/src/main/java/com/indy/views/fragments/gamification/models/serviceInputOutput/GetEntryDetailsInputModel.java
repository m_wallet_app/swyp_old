package com.indy.views.fragments.gamification.models.serviceInputOutput;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mobile on 24/09/2017.
 */

public class GetEntryDetailsInputModel extends GamificationBaseInputModel {


    @SerializedName("indyEventEntryId")
    @Expose
    private String indyEventEntryId;

    @SerializedName("indyEventId")
    @Expose
    private String indyEventId;




    public String getIndyEventEntryId() {
        return indyEventEntryId;
    }

    public void setIndyEventEntryId(String indyEventEntryId) {
        this.indyEventEntryId = indyEventEntryId;
    }

    public String getIndyEventId() {
        return indyEventId;
    }

    public void setIndyEventId(String indyEventId) {
        this.indyEventId = indyEventId;
    }
}
