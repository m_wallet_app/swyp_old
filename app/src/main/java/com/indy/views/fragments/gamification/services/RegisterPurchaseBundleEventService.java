package com.indy.views.fragments.gamification.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.BaseServiceManger;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.gamification.models.serviceInputOutput.SuccessOutputResponse;
import com.indy.views.fragments.gamification.models.serviceInputOutput.logPurchaseBundleEvent.LogPurchaseBundleEventInputModel;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**

 */
public class RegisterPurchaseBundleEventService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private LogPurchaseBundleEventInputModel logPurchaseBundleEventInputModel;


    public RegisterPurchaseBundleEventService(BaseInterface baseInterface, LogPurchaseBundleEventInputModel logPurchaseBundleEventInputModel) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.logPurchaseBundleEventInputModel = logPurchaseBundleEventInputModel;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<SuccessOutputResponse> call = apiService.logPurchaseBundleEvent(logPurchaseBundleEventInputModel);
        call.enqueue(new Callback<SuccessOutputResponse>() {
            @Override
            public void onResponse(Call<SuccessOutputResponse> call, Response<SuccessOutputResponse> response) {
                mBaseResponseModel.setServiceType(ServiceUtils.REGISTER_PURCHASE_STORE_SUCCESS);
                mBaseResponseModel.setResultObj(response.body());

                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (response.body().getErrorMsgEn() != null) {
//                        String urlForTag = call.request().url().toString();
//                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
//                        urlForTag = urlForTag.replace("/", "_");
//                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<SuccessOutputResponse> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.REGISTER_PURCHASE_STORE_SUCCESS);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
