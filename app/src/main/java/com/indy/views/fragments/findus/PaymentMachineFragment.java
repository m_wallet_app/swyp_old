package com.indy.views.fragments.findus;


import android.Manifest;
import android.annotation.TargetApi;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.indy.R;
import com.indy.helpers.GoogleMapsHelper;
import com.indy.helpers.autocomplete.AutoCompleteSearchAdapter;
import com.indy.helpers.autocomplete.Search;
import com.indy.models.location.GeoLocation;
import com.indy.models.location.LocationInputModel;
import com.indy.models.location.LocationModelResponse;
import com.indy.models.location.NearestLocationList;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.services.GetNearestLocationService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.utils.GPSTracker;
import com.indy.views.fragments.utils.MasterFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class PaymentMachineFragment extends MasterFragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
        GoogleMap.OnInfoWindowClickListener, LocationListener {

    View view;
    public GoogleMap mMap;
    public MarkerOptions marker;
    public static HashMap<String, Integer> mMarkerList;
    public static HashMap<LatLng, Marker> mMarkersForSettingList;
    Marker highlightedMarker;
    LocationManager locationManager;
    GPSTracker gpsTracker;
    private Button locateMe;
    public TextView distanceId, locationNameId, headerText;
    public LocationModelResponse mLocationModelResponse;
    public String locationName = null;
    private Double mLatitude, mLongitude;
    Location mlocation;
    public static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
    };
    public static final int LOCATION_REQUEST = 1340;
    GoogleMapsHelper googleMapsHelper;

    public PaymentMachineFragment() {
        // Required empty public constructor
    }

    //---------
    LinearLayout ll_dummyContainer;
    TextView bt_retry;
    TextView tv_error;
    //------

    //-----------AUTOCOMPLETE SEARCH------//
    AutoCompleteSearchAdapter autoCompleteSearchAdapter;
    ArrayList<Search> searches;
    AutoCompleteTextView et_autoComplete;
    TextView tv_change, tv_name, tv_distance;
    RelativeLayout ll_marker;
    //------------AUTOCOMPLETE SEARCH----------//

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_payment_machine, container, false);
        gpsTracker = new GPSTracker(getActivity());
        mMarkerList = new HashMap<String, Integer>();
        mMarkersForSettingList = new HashMap<>();
        initUI();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }


        googleMapsHelper = new GoogleMapsHelper(getContext());
        locateMe = (Button) view.findViewById(R.id.btn_get_directions);

        locateMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mLatitude != null && mLongitude != null) {
                    googleMapsHelper.onGoogleMpasOpen(mLatitude, mLongitude, locationName);
                    CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_support_payment_machines_position_me);

                }
            }
        });

        et_autoComplete = (AutoCompleteTextView) view.findViewById(R.id.et_search_all);
        tv_change = (TextView) view.findViewById(R.id.tv_change);
        tv_distance = (TextView) view.findViewById(R.id.tv_distance);
        tv_name = (TextView) view.findViewById(R.id.tv_name);

        tv_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_autoComplete.setVisibility(View.VISIBLE);
                ll_marker.setVisibility(View.GONE);
                et_autoComplete.requestFocus();
            }
        });
        ll_marker = (RelativeLayout) view.findViewById(R.id.ll_marker);
        ll_marker.setVisibility(View.GONE);
        et_autoComplete.setVisibility(View.GONE);
        ImageView iv_locateMe = (ImageView) view.findViewById(R.id.iv_locateMe);
        iv_locateMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mMap != null && mlocation != null) {
                    LatLng latLng = new LatLng(mlocation.getLatitude(), mlocation.getLongitude());
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, ConstantUtils.zoomLevel));
                    CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_support_payment_machines_position_me);
                }
            }
        });
        initErrorLayout();
    }

    private void initErrorLayout() {
        ll_dummyContainer = (LinearLayout) view.findViewById(R.id.ll_dummy_error_container);

        ll_dummyContainer.setVisibility(View.GONE);
        bt_retry = (TextView) view.findViewById(R.id.retryBtn);
        bt_retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onConsumeService();
                ll_dummyContainer.setVisibility(View.GONE);
            }
        });
        tv_error = (TextView) view.findViewById(R.id.tv_error);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

//        if (!checkPermission() || !isGPSEnabled()) {
//            onConsumeService();
//        } else {
//            onConsumeService();
//        }
        checkPermission();
        isGPSEnabled();
    }


    private void checkPermission() {
//        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M || canAccessLocation();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                this.requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            }
        }
    }


    @Override
    public boolean onMarkerClick(Marker marker) {
        int index = -1;
        try {
            if (marker != null && marker.getId() != null) {
                index = mMarkerList.get(marker.getId());
            }
            if (index != -1) {
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_support_payment_machines_click_marker);
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_support_payment_machines_click_marker + "_" + mLocationModelResponse.getNearestLocationList().get(index).getAddressEn());

                if (currentLanguage.equals("en")) {
                    locationName = mLocationModelResponse.getNearestLocationList().get(index).getAddressEn();
                } else {
                    locationName = mLocationModelResponse.getNearestLocationList().get(index).getAddressAr();
                }
                mLatitude = mLocationModelResponse.getNearestLocationList().get(index).getGeoLocation().getLatitude();
                mLongitude = mLocationModelResponse.getNearestLocationList().get(index).getGeoLocation().getLongitude();
//            locationNameId.setText(locationName);
                LatLng latLng = new LatLng(mLatitude, mLongitude);
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, ConstantUtils.zoomLevel));
                ll_marker.setVisibility(View.VISIBLE);
                et_autoComplete.setVisibility(View.GONE);
//            tv_name.setText(locationName);
//            tv_distance.setText(distance);
            }
        } catch (Exception ex) {
            
        }

        return false;
    }


    @Override
    public void onInfoWindowClick(Marker marker) {
        GoogleMapsHelper openGoogleMaps = new GoogleMapsHelper(getContext());
        openGoogleMaps.onGoogleMpasOpen(marker.getPosition().latitude, marker.getPosition().longitude, locationName);
    }


    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    private boolean canAccessLocation() {
        if (!hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
            return false;
        } else {
            return true;
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == getActivity().checkSelfPermission(perm));
    }

    public void checkForGps() {
        showErrorFragment();
//        showErrorFragment();AlertDailogueHelper.showDaiogue(this, NearestStoreActivity.this, "Alert",
//
    }

    private void isGPSEnabled() {
        LocationManager locationManager = (LocationManager)
                getActivity().getSystemService(getActivity().LOCATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (canAccessLocation()) {// gps is enable
//                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && canAccessLocation()) {// gps is enable
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {// gps is disable
//                    return false;
//                    checkForGps();
                    onConsumeService();

                } else {
                    if (gpsTracker.canGetLocation()) {
                        mlocation = gpsTracker.getLocation();
                        if (mlocation != null) {
                            onConsumeService();
                        } else {
                            Toast.makeText(getActivity(), "Fetching Location, please wait..", Toast.LENGTH_SHORT).show();
                            try {
                                Thread.sleep(1500);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            mlocation = gpsTracker.getLocation();
                            onConsumeService();
                        }
                    } else {
                        onConsumeService();
                    }
//                    chec/kLocation();
                }
//                checkLocation();
            } else {
                onConsumeService();

//                return false;
            }

        } else {
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) { // gps is disable
//                checkForGps();
                onConsumeService();
            } else { // gps is enable
                if (gpsTracker.canGetLocation()) {
                    mlocation = gpsTracker.getLocation();
                    if (mlocation != null) {
                        onConsumeService();
                    } else {
                        Toast.makeText(getActivity(), "Fetching Location, please wait..", Toast.LENGTH_SHORT).show();
                        try {
                            Thread.sleep(1500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        mlocation = gpsTracker.getLocation();
                        onConsumeService();
                    }
                } else {
                    onConsumeService();
                }
            }

        }
    }


    private void showErrorFragment() {
//        frameLayout.setVisibility(View.VISIBLE);
//        ErrorFragment errorFragment = new ErrorFragment();
//        Bundle bundle = new Bundle();
//        bundle.putString(ConstantUtils.errorString, "Cannot get Location please enable GPS.");
//        bundle.putString(ConstantUtils.settingsButtonEnabled, "true");
//        bundle.putString(ConstantUtils.buttonTitle, "Settings");
//        errorFragment.setArguments(bundle);
//        ((FindUsFragment) getActivity()).replaceFragmnet(errorFragment, R.id.frameLayout, true);


        //----------------------**************
//        if (mLocationModelResponse != null && mLocationModelResponse.getErrorMsgEn() != null) {
//            if (currentLanguage.equals("en")) {
//                tv_error.setText(mLocationModelResponse.getErrorMsgEn());
//            } else {
//                tv_error.setText(mLocationModelResponse.getErrorMsgAr());
//            }
//        } else {
//            tv_error.setText(getString(R.string.generice_error));
//        }
//        ll_dummyContainer.setVisibility(View.VISIBLE);
//        bt_retry.setText(getString(R.string.retry));
//        bt_retry.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onConsumeService();
//                ll_dummyContainer.setVisibility(View.GONE);
//            }
//        });
//------------------------*************
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
//        progressID.setVisibility(View.VISIBLE);
        LocationInputModel locationInputModel = new LocationInputModel();
        locationInputModel.setSignificant(ConstantUtils.payment_findus);
        locationInputModel.setSize(ConstantUtils.number_of_locations);
        GeoLocation geoLocation = new GeoLocation();
        geoLocation = new GeoLocation();
        if (mlocation != null) {


            geoLocation.setLatitude(mlocation.getLatitude());
            geoLocation.setLongitude(mlocation.getLongitude());

        } else {

            geoLocation.setLatitude(0d);
            geoLocation.setLongitude(0d);

        }
        locationInputModel.setGeoLocation(geoLocation);
        locationInputModel.setAppVersion(appVersion);
        locationInputModel.setToken(token);
        locationInputModel.setOsVersion(osVersion);
        locationInputModel.setChannel(channel);
        locationInputModel.setDeviceId(deviceId);
        locationInputModel.setAuthToken(authToken);
        new GetNearestLocationService(this, locationInputModel);

    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
//        progressID.setVisibility(View.GONE);
//        onBackPressed();
        try {
            if (responseModel != null && responseModel.getResultObj() != null) {
                if (isVisible()) {
                    mLocationModelResponse = (LocationModelResponse) responseModel.getResultObj();
                    if (mLocationModelResponse != null && mLocationModelResponse.getNearestLocationList().size() > 0) {
                        drawMapPins();
                        createLocationList(mLocationModelResponse.getNearestLocationList());
                        if (mlocation == null) {
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(24.980223, 55.265673),
                                    ConstantUtils.zoomLevelWithoutLocationPaymentMachine));
                        }
                    } else {
                        showErrorFragment();
                    }
                }
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        try {
            showErrorFragment();
        } catch (Exception ex) {

        }
    }


    public void addMarkerAtCurrentPosition(Location mLocation) {
        if (mLocation != null) {
            LatLng mLatLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
            MarkerOptions tempMarker = new MarkerOptions().position(mLatLng);
            mMap.addMarker(
                    tempMarker.position(mLatLng)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.locate_me_marker)));
        }
    }

    public void drawMapPins() {
        mMap.clear();
        addMarkerAtCurrentPosition(mlocation);
        LatLng fLatLng = null;
        String distance = null;
        Marker temp = null;
        for (int i = 0; i < mLocationModelResponse.getNearestLocationList().size(); i++) {
            fLatLng = new LatLng(mLocationModelResponse.getNearestLocationList().get(i).getGeoLocation().getLatitude(),
                    mLocationModelResponse.getNearestLocationList().get(i).getGeoLocation().getLongitude());
            LatLng mLatLng = new LatLng(mLocationModelResponse.getNearestLocationList().get(i).getGeoLocation().getLatitude(),
                    mLocationModelResponse.getNearestLocationList().get(i).getGeoLocation().getLongitude());
            marker = new MarkerOptions().position(mLatLng);
            String title = "";
            if (currentLanguage.equals("en")) {
                title = mLocationModelResponse.getNearestLocationList().get(i).getAddressEn();
            } else {
                title = mLocationModelResponse.getNearestLocationList().get(i).getAddressAr();
            }
//             marker.icon(BitmapDescriptorFactory.fromBitmap(etisaltLogo));
            String dist = "-";
            if (mLocationModelResponse.getNearestLocationList().get(i).getDistance() != null) {
                dist = mLocationModelResponse.getNearestLocationList().get(i).getDistance() + " Km";
            }

            if (i == 0) {
                temp = mMap.addMarker(
                        marker.position(mLatLng)
                                .title(mLocationModelResponse.getNearestLocationList().get(i).getAddressEn())
                                .snippet(dist)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.location_icon_fill)));
                et_autoComplete.setVisibility(View.GONE);
                tv_name.setText(title);
                tv_distance.setText(dist);
                ll_marker.setVisibility(View.VISIBLE);
                highlightedMarker = temp;
            } else {
                temp = mMap.addMarker(
                        marker.position(mLatLng)
                                .title(mLocationModelResponse.getNearestLocationList().get(i).getAddressEn())
                                .snippet(dist)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.location_orange)));

            }
            mMarkerList.put(temp.getId(), i);
            mMarkersForSettingList.put(mLatLng, temp);
        }
        fLatLng = new LatLng(mLocationModelResponse.getNearestLocationList().get(0).getGeoLocation().getLatitude(),
                mLocationModelResponse.getNearestLocationList().get(0).getGeoLocation().getLongitude());

        if (currentLanguage.equals("en")) {
            locationName = mLocationModelResponse.getNearestLocationList().get(0).getAddressEn();
        } else {
            locationName = mLocationModelResponse.getNearestLocationList().get(0).getAddressAr();
        }
//         distanceId.setText( getString(R.string.nearest_shop) + " " + distance + "  " +  getString(R.string.away));
        mLatitude = mLocationModelResponse.getNearestLocationList().get(0).getGeoLocation().getLatitude();
        mLongitude = mLocationModelResponse.getNearestLocationList().get(0).getGeoLocation().getLongitude();
//         locationNameId.setText( locationName);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(fLatLng,
                ConstantUtils.zoomLevel));
        mMap.setOnMarkerClickListener(this);
        mMap.setOnInfoWindowClickListener(this);
        mMap.setInfoWindowAdapter(new MyInfoWindowAdapter());
    }


    class MyInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private final View myContentsView;

        MyInfoWindowAdapter() {
            myContentsView = getActivity().getLayoutInflater().inflate(R.layout.my_info_window, null);
        }

        @Override
        public View getInfoContents(Marker marker) {

            TextView tvTitle = ((TextView) myContentsView.findViewById(R.id.tv_name));
            tvTitle.setText(marker.getTitle());
            TextView tvSnippet = ((TextView) myContentsView.findViewById(R.id.tv_distance));
            tvSnippet.setText(marker.getSnippet());

            return myContentsView;
        }

        @Override
        public View getInfoWindow(Marker marker) {
            // TODO Auto-generated method stub
            return null;
        }

    }

    private void createLocationList(final List<NearestLocationList> locationList) {

        if (locationList != null && !locationList.isEmpty()) {

            if (locationList.size() > 0) {
                searches = new ArrayList<Search>(locationList.size());
                if (sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)) {
                    for (NearestLocationList nearestLocationList : locationList) {
                        searches.add(new Search(nearestLocationList.getAddressEn(), nearestLocationList.getGeoLocation().getLatitude(),
                                nearestLocationList.getGeoLocation().getLongitude()));


                    }
                } else {
                    for (NearestLocationList nearestLocationList : locationList) {

                        searches.add(new Search(nearestLocationList.getAddressAr(), nearestLocationList.getGeoLocation().getLatitude(),
                                nearestLocationList.getGeoLocation().getLongitude()));

                    }
                }
                if (searches != null) {
                    if (getActivity() != null) {

                        autoCompleteSearchAdapter = new AutoCompleteSearchAdapter(getActivity(), R.layout.list_item_search, searches);

                        et_autoComplete.setAdapter(autoCompleteSearchAdapter);

                        et_autoComplete.setThreshold(1);

                    }
                }
            }
            autoCompleteSearchAdapter.setOnItemClickListener(new AutoCompleteSearchAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position, Search search) {
                    TextView charac = (TextView) view.findViewById(R.id.searchNameLabel);
                    String text = charac.getText().toString();
                    et_autoComplete.setText("");
                    et_autoComplete.clearListSelection();
                    et_autoComplete.dismissDropDown();
                    hideKeyBoard();
                    CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_support_payment_machines_search);

                    //HomeFragment.hideKeyboard(et_search);
                    NearestLocationList nearestLocationList = getLocationObject(search, locationList);
                    if (mMap != null) {
                        LatLng latLng = new LatLng(nearestLocationList.getGeoLocation().getLatitude(),
                                nearestLocationList.getGeoLocation().getLongitude());
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, ConstantUtils.zoomLevel));
                        ll_marker.setVisibility(View.VISIBLE);
                        et_autoComplete.setVisibility(View.GONE);
                        if (currentLanguage.equals("en")) {
                            tv_name.setText(nearestLocationList.getAddressEn());
                        } else {
                            tv_name.setText(nearestLocationList.getAddressAr());
                        }
                        if (nearestLocationList.getDistance() != null) {
                            tv_distance.setText(nearestLocationList.getDistance() + getString(R.string.km));
                        }
                        highlightedMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.location_orange));
                        mMarkersForSettingList.get(latLng).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.location_icon_fill));
                        highlightedMarker = mMarkersForSettingList.get(latLng);
                    }


                }
            });

        }
    }

    public NearestLocationList getLocationObject(Search search, List<NearestLocationList> locationList) {
        for (int i = 0; i < locationList.size(); i++) {
            if ((locationList.get(i).getAddressEn().equalsIgnoreCase(search.getName()) ||
                    locationList.get(i).getAddressAr().equalsIgnoreCase(search.getName()))
                    && locationList.get(i).getGeoLocation().getLatitude().equals(search.getLatitude())
                    && locationList.get(i).getGeoLocation().getLongitude().equals(search.getLongitude())) {

                return locationList.get(i);

            }
        }
        return null;

    }

}
