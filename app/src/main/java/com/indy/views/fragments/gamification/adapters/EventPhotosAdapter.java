package com.indy.views.fragments.gamification.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.ImageView;

import com.indy.R;
import com.indy.utils.SharedPrefrencesManger;
import com.indy.views.fragments.gamification.OnPhotoItemClick;
import com.indy.views.fragments.gamification.OnVoteUnVoteClick;
import com.indy.views.fragments.gamification.models.EventPhoto;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

public class EventPhotosAdapter extends RecyclerView.Adapter<EventPhotosAdapter.ViewHolder> {
    private List<EventPhoto> rewardsLists;
    private Context mContext;
    private SharedPrefrencesManger sharedPrefrencesManger;
    private OnVoteUnVoteClick onVoteUnVoteClick;
    private OnPhotoItemClick onPhotoItemClick;
    boolean isMembershipValid;

    public EventPhotosAdapter(List<EventPhoto> mRewardsList, Context context, OnVoteUnVoteClick onVoteUnVoteClick
            , OnPhotoItemClick onPhotoItemClick, boolean isMembershipValid) {
        rewardsLists = mRewardsList;
        this.mContext = context;
        this.sharedPrefrencesManger = new SharedPrefrencesManger(context);
        this.onVoteUnVoteClick = onVoteUnVoteClick;
        this.onPhotoItemClick = onPhotoItemClick;
        this.isMembershipValid = isMembershipValid;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.item_event_pictures_list, viewGroup, false);
        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final EventPhoto rewardsListItem = rewardsLists.get(position);
        ImageLoader.getInstance().displayImage(rewardsListItem.getImageURL(), holder.iv_photo);
        holder.iv_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPhotoItemClick.onPhotoClicked(position);
            }
        });
        holder.iv_voteStatus.setEnabled(isMembershipValid);
        holder.iv_voteStatus.setChecked(rewardsListItem.isSelected());
        holder.iv_voteStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onVoteUnVoteClick != null)
                    onVoteUnVoteClick.onVoteUnVoteClick(rewardsListItem, !holder.iv_voteStatus.isChecked(), position);
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return rewardsLists.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView iv_photo;
        CheckedTextView iv_voteStatus;

        public ViewHolder(View v) {
            super(v);
            iv_photo = (ImageView) v.findViewById(R.id.iv_event_photo);
            iv_voteStatus = (CheckedTextView) v.findViewById(R.id.vote_checked_text);

        }
    }


}
