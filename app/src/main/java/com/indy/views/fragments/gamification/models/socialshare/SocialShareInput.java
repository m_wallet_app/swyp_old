package com.indy.views.fragments.gamification.models.socialshare;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GamificationBaseInputModel;

/**
 * Created by Tohamy on 10/4/2017.
 */

public class SocialShareInput extends GamificationBaseInputModel {
    @SerializedName("mediaChannel")
    @Expose
    private String mediaChannel;
    @SerializedName("itemId")
    @Expose
    private String itemId;
    @SerializedName("itemType")
    @Expose
    private String itemType;

    @SerializedName("referenceId")
    @Expose
    private String referenceId;

    public String getMediaChannel() {
        return mediaChannel;
    }

    public void setMediaChannel(String mediaChannel) {
        this.mediaChannel = mediaChannel;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }
}

