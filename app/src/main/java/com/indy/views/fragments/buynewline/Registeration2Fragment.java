package com.indy.views.fragments.buynewline;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.models.utils.BaseResponseModel;
import com.indy.ocr.scanning.CameraBackScanningActivity;
import com.indy.ocr.scanning.EmiratesIdDataObject;
import com.indy.ocr.scanning.UploadEmiratesID;
import com.indy.ocr.screens.EmiratesIdDetailsFragment;
import com.indy.ocrStep2.ocr.UploadEmiratesIdOutput;
import com.indy.utils.CommonMethods;
import com.indy.services.LogApplicationFlowService;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.NearestStoreActivity;
import com.indy.views.fragments.buynewline.ocr_flow.EmailInputFragment;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.indy.views.fragments.utils.MasterFragment;
import com.kofax.kmc.ken.engines.data.Image;
import com.kofax.mobile.sdk.extract.server.IServerExtractor;

import static com.indy.ocr.scanning.Constants.CAPTURE_BACK;
import static com.indy.ocr.scanning.Constants.SIDE;
import static com.indy.ocr.scanning.Constants.back_image;
import static com.indy.ocr.scanning.Constants.front_image;

import static com.indy.views.activites.RegisterationActivity.logEventInputModel;

/**
 * Created by emad on 9/7/16.
 */
public class Registeration2Fragment extends MasterFragment {
    private View view;
    private Button locateNearestBtn, continueSignUpBtn;
    private TextView customTextView3;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_party, container, false);
        initUI();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
        regActivity.showHeaderLayout();
        regActivity.setHeaderTitle("");
        locateNearestBtn = (Button) view.findViewById(R.id.locateNearestId);
        continueSignUpBtn = (Button) view.findViewById(R.id.continueSignUpId);
        customTextView3 = (TextView) view.findViewById(R.id.customTextView3);
        customTextView3.setText(getString(R.string.listing_credit_card_1) + "\n" + getString(R.string.listing_credit_card_2));
        setListeners();
    }

    private void setListeners() {
        locateNearestBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();

                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_check_nearest_shop);
                bundle.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_check_nearest_shop);
                mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_check_nearest_shop, bundle);


//                Bundle bundle = new Bundle();
//
//                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_check_nearest_shop);
//                bundle.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_check_nearest_shop);
//                mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_check_nearest_shop, bundle);
                Intent intent = new Intent(getActivity(), NearestStoreActivity.class);
                Bundle bundle1 = new Bundle();
                logEvent(2);
                bundle1.putString(ConstantUtils.KEY_SCREEN_ID,ConstantUtils.SCREEN_ID_0001);
                bundle1.putSerializable("logging_object",logEventInputModel);
                intent.putExtras(bundle1);
                startActivity(intent);


//
//                Intent intent = new Intent(getActivity(), CameraActivity.class);
//                intent.putExtra(SIDE, CAPTURE_FRONT);
//                startActivityForResult(intent, 100);
            }
        });
        continueSignUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();

                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_continue_signup);
                bundle.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_continue_signup);mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_continue_signup, bundle);
                logEvent(1);
                regActivity.replaceFragmnet(new Registeration3Fragment(), R.id.frameLayout, true);


                bundle.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_continue_signup);
                mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_continue_signup, bundle);
//                regActivity.replaceFragmnet(new Registeration3Fragment(), R.id.frameLayout, true);
                regActivity.replaceFragmnet(new EmailInputFragment(), R.id.frameLayout, true);
//                CommonMethods.getDebitCreditCardScan(regActivity);
//                startActivity(new Intent(getActivity(),CreditCardScanningActivity.class));
//                bundle.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_continue_signup);mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_continue_signup, bundle);
//                regActivity.replaceFragmnet(new Registeration3Fragment(), R.id.frameLayout, true);
            }
        });

    }

    private void logEvent(int index) {

        logEventInputModel.setActionTransactionId(sharedPrefrencesManger.getTempAppId());
        logEventInputModel.setProgressStatus(ConstantUtils.APP_LOG_STATUS_IN_PROGRESS);
        if(index==1) {
            logEventInputModel.setScreenId(ConstantUtils.SCREEN_ID_0000);
            logEventInputModel.setSignUp(continueSignUpBtn.getText().toString());

        }else{
            logEventInputModel.setScreenId(ConstantUtils.SCREEN_ID_0000);
            logEventInputModel.setNearestLocation(locateNearestBtn.getText().toString());


        }


        new LogApplicationFlowService(this,logEventInputModel);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 100:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        final Intent intent = new Intent(getActivity(), CameraBackScanningActivity.class);
                        intent.putExtra(SIDE, CAPTURE_BACK);
                        startActivityForResult(intent, 200);

                    }
                }, 1000);

                break;

            case 200:
                if (resultCode == 100) {
//                    Toast.makeText(getActivity(), "good capture", Toast.LENGTH_LONG).show();

                    sendRequest();
                } else {
//                    Toast.makeText(getActivity(), "error in capture", Toast.LENGTH_LONG).show();
                }
                break;
            case 1001:
                break;
        }

    }

    public void sendRequest() {

        try {
//        addFragmnet(new LoadingFragmnet(),R.id.frameLayout,true);
            final IServerExtractor serverExtractor;
//        this.mContext = this.getApplicationContext();
//        mParameters = new HashMap<>();
//        certificateValidatorListener = new CertificateValidatorListenerImpl();
//        List<Image> images = new ArrayList<>();

            front_image.setImageDPI(500);
            back_image.setImageDPI(500);
            front_image.setImageMimeType(Image.ImageMimeType.MIMETYPE_JPEG);
            back_image.setImageMimeType(Image.ImageMimeType.MIMETYPE_JPEG);

//        images.add(front_image);
//        images.add(back_image);

            regActivity.addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
            new UploadEmiratesID(this, front_image, back_image, getActivity());


//            serverExtractor = ServerBuilder.build(this.mContext, ServerBuilder.ServerType.RTTI);
//            mParameters.put("xImageResize", "ID-1");
//            mParameters.put("ProcessCount", "2");
//            mParameters.put("xExtractSignatureImage", "True");
//            mParameters.put("processImage", "False");
//            mParameters.put("xRegion", "Asia");
//            mParameters.put("xIdType", "ID");
//            mParameters.put("xExtractFaceImage", "True");
//            ServerExtractionParameters extractionParameters = new ServerExtractionParameters(URL, images, null, certificateValidatorListener, mParameters,null);
//            serverExtractor.extractData(extractionParameters, this);
        } catch (Exception ex) {
            Log.d("indyocr", ex.getMessage());
        }
    }

    EmiratesIdDataObject emiratesIdDataObject;

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            if (responseModel != null) {
                regActivity.onBackPressed();
                UploadEmiratesIdOutput uploadEmiratesIdOutput = (UploadEmiratesIdOutput) responseModel.getResultObj();
                if (uploadEmiratesIdOutput != null && uploadEmiratesIdOutput.getErrorResult() != null &&
                        uploadEmiratesIdOutput.getErrorResult().getErrorDescription() == null) {
                    emiratesIdDataObject = CommonMethods.getEmiratesIdObject(uploadEmiratesIdOutput.getFields());

                    EmiratesIdDetailsFragment emiratesIdDetailsFragment = new EmiratesIdDetailsFragment();
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(ConstantUtils.EMIRATES_ID, emiratesIdDataObject);
                    emiratesIdDetailsFragment.setArguments(bundle);
                    regActivity.replaceFragmnet(emiratesIdDetailsFragment, R.id.frameLayout, true);
//                    Toast.makeText(regActivity, "no error received", Toast.LENGTH_SHORT).show();
//                uploadEmiratesIdOutput.getClassificationResult().get(i).
                } else {
                    ErrorFragment errorFragment = new ErrorFragment();
                    Bundle bundle = new Bundle();
                    if (uploadEmiratesIdOutput.getErrorResult() != null) {
                        bundle.putString(ConstantUtils.errorString, uploadEmiratesIdOutput.getErrorResult().getErrorDescription().toString());
                    } else {
                        bundle.putString(ConstantUtils.errorString, "Error!");
                    }
                    errorFragment.setArguments(bundle);
                    regActivity.addFragmnet(new ErrorFragment(), R.id.frameLayout, true);
                }
            } else {
                ErrorFragment errorFragment = new ErrorFragment();
                Bundle bundle = new Bundle();
                bundle.putString(ConstantUtils.errorString, "Error!");

                errorFragment.setArguments(bundle);
                regActivity.addFragmnet(new ErrorFragment(), R.id.frameLayout, true);
            }
        } catch (Exception ex) {
//            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        regActivity.onBackPressed();

    }
}
