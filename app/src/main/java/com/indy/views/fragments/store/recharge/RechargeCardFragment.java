package com.indy.views.fragments.store.recharge;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.controls.ServiceUtils;
import com.indy.models.balance.BalanceInputModel;
import com.indy.models.balance.BalanceResponseModel;
import com.indy.models.recharge.RechargeInput;
import com.indy.models.recharge.RechargeOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.services.GetUserBalanceService;
import com.indy.services.RechargeService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.HelpActivity;
import com.indy.views.activites.RechargeActivity;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.indy.views.fragments.utils.MasterFragment;

/**
 * Created by Amir.jehangir on 10/14/2016.
 */
public class RechargeCardFragment extends MasterFragment {
    private View view;
    TextView titleview, tv_pleaseEnterValid;
    public static TextView tv_lastUpdated;
    Button btn_proceed;
    RechargeActivity rechargeActivity;
    RechargeInput rechargeInput;
    RechargeOutput rechargeOutput;
    private String status;
    EditText txtInputScrachCardNo;
    TextInputLayout til_scratchCard;

    BalanceInputModel balanceInputModel;
    BalanceResponseModel balanceResponseModel;
    private CharSequence emailCharSeq, fullNameCharSeq;
    TextView title;
    public static TextView tv_userBalanceAmount;
    private Button backImg, helpBtn;
    FrameLayout frameLayout;
    String serviceType = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_payment, container, false);

        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
        title = (TextView) view.findViewById(R.id.titleTxt);
        title.setText(R.string.payment_title);
        backImg = (Button) view.findViewById(R.id.backImg);
        helpBtn = (Button) view.findViewById(R.id.helpBtn);
        helpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), HelpActivity.class);
                startActivity(intent);
            }
        });
        helpBtn.setVisibility(View.INVISIBLE);
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((RechargeActivity) getActivity()).onBackPressed();
            }
        });
        txtInputScrachCardNo = (EditText) view.findViewById(R.id.rechargenumber);
        btn_proceed = (Button) view.findViewById(R.id.btn_proceed);
        txtInputScrachCardNo.setText("");
        tv_userBalanceAmount = (TextView) view.findViewById(R.id.user_balance_amount);

        til_scratchCard = (TextInputLayout) view.findViewById(R.id.rechargenumberTxtInput);
        tv_pleaseEnterValid = (TextView) view.findViewById(R.id.tv_please_enter_valid);
        tv_pleaseEnterValid.setVisibility(View.GONE);
        tv_lastUpdated = (TextView) view.findViewById(R.id.tv_last_updated);
        tv_lastUpdated.setText(getString(R.string.last_updated) + " " + sharedPrefrencesManger.getLastUpdatedBalance() +
                " " + getString(R.string.min_ago));

        disableNextBtn();
        changeNextBtnState();
    }

    public boolean isValid() {
        boolean isValid = true;
        if (txtInputScrachCardNo.getText().toString().length() != 15) {
            isValid = false;
//            setTextInputLayoutError(til_scratchCard,);
            tv_pleaseEnterValid.setVisibility(View.VISIBLE);
        } else {
            tv_pleaseEnterValid.setVisibility(View.GONE);
        }

        return isValid;
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        showLoadingFragment();
//        progressID.setVisibility(View.VISIBLE);
        if (serviceType.equals(ServiceUtils.rechargeCard)) {
            rechargeInput = new RechargeInput();
            rechargeInput.setLang(currentLanguage);
            rechargeInput.setAppVersion(appVersion);
            rechargeInput.setOsVersion(osVersion);
            rechargeInput.setToken(token);
            rechargeInput.setChannel(channel);
            rechargeInput.setDeviceId(deviceId);
            rechargeInput.setMsisdn(sharedPrefrencesManger.getMobileNo());
            rechargeInput.setImsi(sharedPrefrencesManger.getMobileNo());
            rechargeInput.setScratchCardNo(txtInputScrachCardNo.getText().toString());
            rechargeInput.setAuthToken(authToken);
            new RechargeService(this, rechargeInput);
        } else if (serviceType.equals(ServiceUtils.balanceServiceType)) {
            balanceInputModel = new BalanceInputModel();
            balanceInputModel.setAppVersion(appVersion);
            balanceInputModel.setToken(token);
            balanceInputModel.setOsVersion(osVersion);
            balanceInputModel.setChannel(channel);
            balanceInputModel.setDeviceId(deviceId);
            balanceInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
            balanceInputModel.setBillingPeriod(CommonMethods.getBillingPeriod());
            balanceInputModel.setAuthToken(authToken);
            new GetUserBalanceService(this, balanceInputModel, getActivity());
        }
    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        rechargeActivity.onBackPressed();
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            if (isVisible()) {
                if (responseModel != null && responseModel.getResultObj() != null) {
                    rechargeActivity.onBackPressed();
                    if (isVisible()) {
                        if (responseModel.getServiceType().equals(ServiceUtils.rechargeCard)) {
                            rechargeOutput = (RechargeOutput) responseModel.getResultObj();
                            if (rechargeOutput != null) {
                                if (rechargeOutput.getErrorCode() != null) {
                                    onRechargeIDVerificationFail();
                                } else {
                                    if (rechargeOutput.getRecharged() != null && rechargeOutput.getRecharged().equals("true")) {
                                        onRechargeIDVerificationSuccess();
                                    } else {
                                        onRechargeIDVerificationFail();
                                    }
                                }
                            }
                        } else if (responseModel.getServiceType().equals(ServiceUtils.balanceServiceType)) {
                            balanceResponseModel = (BalanceResponseModel) responseModel.getResultObj();
                            if (balanceResponseModel != null) {
                                if (balanceResponseModel.getErrorCode() != null) {
                                    showErrorFragment(balanceResponseModel.getErrorMsgEn());
                                } else {
                                    tv_userBalanceAmount.setText(getString(R.string.your_balance_is_aed) + " " + balanceResponseModel.getAmount()+ " " + getString(R.string.aed_arabic_only));
                                    tv_lastUpdated.setText(getString(R.string.last_updated) + " " + sharedPrefrencesManger.getLastUpdatedBalance() +
                                            " " + getString(R.string.min_ago));

                                    CommonMethods.updateBalanceAndLastUpdated(getString(R.string.your_balance_is_aed) + " " + balanceResponseModel.getAmount()+ " " + getString(R.string.aed_arabic_only)
                                            , getString(R.string.last_updated) + " " + sharedPrefrencesManger.getLastUpdatedBalance() +
                                                    " " + getString(R.string.min_ago));

                                }
                            }
                        }
                    }

                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void onRechargeIDVerificationFail() {
//        Toast.makeText(getActivity(),"fail"+ status ,Toast.LENGTH_SHORT).show();
        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_recharge_card_failure);

        if (rechargeOutput != null && rechargeOutput.getErrorMsgEn() != null) {
            if (currentLanguage.equalsIgnoreCase("en"))
                showErrorFragment(rechargeOutput.getErrorMsgEn());
            else
                showErrorFragment(rechargeOutput.getErrorMsgAr());

        } else {
            showErrorFragment(getString(R.string.generice_error));
        }
    }
//


    private void onRechargeIDVerificationSuccess() {
        status = rechargeOutput.getRecharged();

        CommonMethods.logPurchaseEvent(0);
        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_recharge_card_success);

//        Toast.makeText(getActivity(),"success"+ status ,Toast.LENGTH_SHORT).show();
        displayNextFragment();

//        switch (status) {
//            case "false":{
//
//                Toast
//                // eligable
//
////                onEligable();
//              //  emiratesID = eIDTxt.getText().toString().trim();
//              //  regActivity.replaceFragmnet(new OrderSummaryFragment(), R.id.frameLayout, true);
//            }
//                break;
//            default:
//                showErrorFragment("Error message to be replaced.");
//                break;
//        }
    }


    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        try {
//            super.onErrorListener(responseModel);

            showErrorFragment(getString(R.string.error_));
        }catch (Exception ex){
            CommonMethods.logException(ex);
        }
//            Toast.makeText(getActivity(),"fail"+ status ,Toast.LENGTH_SHORT).show();
        //  regActivity.onBackPressed();

//        showErrorFragment("Error in service.");
    }
//    private void showErrorFragment(String error) {
//        Bundle bundle = new Bundle();
//        bundle.putString(ConstantUtils.errorString, error);
//        ErrorFragment errorFragment = new ErrorFragment();
//        errorFragment.setArguments(bundle);
//        rechargeActivity.replaceFragmnet(errorFragment, R.id.frameLayout, true);
//    }

    private void showLoadingFragment() {
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.loadingString, getString(R.string.proceed));
        LoadingFragmnet loadingFragmnet = new LoadingFragmnet();
        loadingFragmnet.setArguments(bundle);
        rechargeActivity.addFragmnet(loadingFragmnet, R.id.frameLayout, true);
    }

    private void displayNextFragment() {
        txtInputScrachCardNo.setText("");
        CongratulationsFragmentRecharge congratulationsFragmentRecharge = new CongratulationsFragmentRecharge();
        Bundle bundle = new Bundle();
        congratulationsFragmentRecharge.setArguments(bundle);
        rechargeActivity.replaceFragmnet(congratulationsFragmentRecharge, R.id.frameLayout, true);
    }

    public void changeNextBtnState() {
        txtInputScrachCardNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (txtInputScrachCardNo.getText().toString().trim().length() == 15) {
                    CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_recharge_card_enter_card_number);
                    enableNextBtn();
                } else {
                    disableNextBtn();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
            }
        });
//        txtInputScrachCardNo.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//                // TODO Auto-generated method stub
//                emailCharSeq = s;
//                if (fullNameCharSeq != null)
//                    if (s.length() <14  || fullNameCharSeq.length() < 14)
//                        disableNextBtn();
//                    else
//                        enableNextBtn();
//            }
//
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//                // TODO Auto-generated method stub
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//                // TODO Auto-generated method stub
//            }
//        });
    }

    private void enableNextBtn() {
        btn_proceed.setAlpha(1f);
        btn_proceed.setClickable(true);
        btn_proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()) {
                    serviceType = ServiceUtils.rechargeCard;
                    onConsumeService();
                    CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_recharge_card_recharge_button);

//                    displayNextFragment();
                }
            }
        });

    }


    private void disableNextBtn() {
        btn_proceed.setAlpha(.5f);
        btn_proceed.setClickable(false);
        btn_proceed.setOnClickListener(null);
    }

    private void showErrorFragment(String error) {
//        if(frameLayout!=null) {
//            frameLayout.setVisibility(View.VISIBLE);
//        frameLayout.bringToFront();
            ErrorFragment errorFragment = new ErrorFragment();
            Bundle bundle = new Bundle();
            bundle.putString(ConstantUtils.errorString, error);
            errorFragment.setArguments(bundle);
            ((RechargeActivity) getActivity()).addFragmnet(errorFragment, R.id.frameLayout, true);

    }

    @Override
    public void onResume() {
        super.onResume();
        rechargeActivity = (RechargeActivity) getActivity();
        view.bringToFront();
        initUI();
        serviceType = ServiceUtils.balanceServiceType;
        onConsumeService();
    }
}
