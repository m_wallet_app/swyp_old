package com.indy.views.fragments.buynewline;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.controls.AdjustEvents;
import com.indy.controls.ServiceUtils;
import com.indy.models.finalizeregisteration.DeliveryInfo;
import com.indy.models.finalizeregisteration.FinalizeRegisterationRequestInput;
import com.indy.models.finalizeregisteration.FinalizeRegisterationRequestOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.ocr.screens.EmailAddressFragment;
import com.indy.ocr.screens.EmiratesIdDetailsFragment;
import com.indy.services.FinalizeRegisteraionRequestService;
import com.indy.services.LogApplicationFlowService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.PaymentActivity;
import com.indy.views.fragments.buynewline.exisitingnumber.EmiratesIDVerificationFragment;
import com.indy.views.fragments.buynewline.exisitingnumber.NumberTransferFragment;
import com.indy.views.fragments.buynewline.exisitingnumber.PromoCodeVerificationSuccessfulFragment;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.indy.views.fragments.utils.MasterFragment;

import static com.indy.views.activites.RegisterationActivity.logEventInputModel;

/**
 * Created by emad on 9/22/16.
 */

public class OrderSummaryFragment extends MasterFragment implements SeekBar.OnSeekBarChangeListener {
    private View view;
    private TextView name, alternativeName, packageType, simPrice, deliveryInformation,
            deliveryMethod, deliveryMethodValue, totalamount, tv_packageName, tv_membershipFee, tv_deliveryFee;
    private FinalizeRegisterationRequestInput finalizeRegisterationRequestInput;
    private FinalizeRegisterationRequestOutput finalizeRegisterationRequestOutput;
    private Button proceedToPayment;
    private double shippingMethodAmount;
    private double orderTypeAmount, vatAmount = 0;
    public static double totalAmount;
    public RelativeLayout rl_extraData;
    SeekBar sb;
    boolean flag = false;
    TextView seekbartest;
    RelativeLayout page_background;
    public static OrderSummaryFragment orderSummaryFragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_order_summery, container, false);
        initUI();
        setListeners();
        setValues();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (sb != null) {
            sb.setProgress(5);
            seekbartest.setText(getString(R.string.swyp_to_checkout));
//            seekbartest.setText(getString(R.string.swyp_to_checkout));
            seekbartest.setVisibility(View.VISIBLE);

        }

        if (NewNumberFragment.isTimerActive && CommonMethods.isTimerUp(sharedPrefrencesManger.getTimerValue())) {
            Fragment[] myFragments = {OrderFragment.orderFragmentInstance,
//                    DeliveryMethodFragment.deliveryFragmentInstance,
                    DeliveryDetailsAreaFragment.deliveryDetailsAreaFragment,
                    DeliveryDetailsAddress.deliveryDetailsAddress,
                    NewNumberFragment.newNumberFragment,
                    NewNumberFragment.loadingFragmnet,
                    EmiratesIDVerificationFragment.emiratesIDVerificationFragment,
                    OrderSummaryFragment.orderSummaryFragment
            };
            regActivity.removeFragment(myFragments);
            regActivity.replaceFragmnet(new NewNumberFragment(), R.id.frameLayout, true);
            NewNumberFragment.isTimerActive = false;
        }

    }

    @Override
    public void initUI() {
        super.initUI();
        regActivity.showHeaderLayout();
        regActivity.setHeaderTitle("");
        name = (TextView) view.findViewById(R.id.name);
        alternativeName = (TextView) view.findViewById(R.id.alternativename);
        packageType = (TextView) view.findViewById(R.id.simText);
        simPrice = (TextView) view.findViewById(R.id.sim_price);

        deliveryInformation = (TextView) view.findViewById(R.id.deliveryInformation);
        deliveryMethod = (TextView) view.findViewById(R.id.deliveryMethod);
        deliveryMethodValue = (TextView) view.findViewById(R.id.deliveryMethodValue);
        totalamount = (TextView) view.findViewById(R.id.totalamount);
        proceedToPayment = (Button) view.findViewById(R.id.proceddBtn);
        sb = (SeekBar) view.findViewById(R.id.myseek);
        seekbartest = (TextView) view.findViewById(R.id.slider_text);
        tv_packageName = (TextView) view.findViewById(R.id.tv_packageDescription);
        tv_membershipFee = (TextView) view.findViewById(R.id.membership_price);
//        tv_deliveryFee = (TextView) view.findViewById(R.id.tv_deliveryFee);
        rl_extraData = (RelativeLayout) view.findViewById(R.id.rl_extra_data);

        seekbartest.setText(getString(R.string.swyp_to_checkout));
        sb.setVisibility(View.VISIBLE);
        sb.setProgress(5);
        proceedToPayment.setVisibility(View.INVISIBLE);
        seekbartest.setVisibility(View.VISIBLE);
        orderSummaryFragment = this;

        if (sharedPrefrencesManger.isVATEnabled()) {
            ((TextView) view.findViewById(R.id.tv_vat_text)).setVisibility(View.VISIBLE);
        } else {
            ((TextView) view.findViewById(R.id.tv_vat_text)).setVisibility(View.GONE);

        }
    }

    private void setValues() {
        if (NameDOBFragment.fullName != null) {
            name.setText(NameDOBFragment.fullName);
        } else {
            name.setText(EmiratesIdDetailsFragment.fullName);
        }

        alternativeName.setText(EmailAddressFragment.mobileNoStr);
        deliveryInformation.setText(DeliveryDetailsAddress.addressLine2);


        if (OrderFragment.orderTypeStr != null) {
            if (OrderFragment.orderTypeStr.equalsIgnoreCase("1")) { // in case order type
                packageType.setText(getString(R.string.sim));
                orderTypeAmount = OrderFragment.totalAmount + OrderFragment.totalAmountSIM;
                if (OrderFragment.totalAmountSIM == 0) {
                    simPrice.setText(getString(R.string.free));
                } else {
                    simPrice.setText(getString(R.string.aed) + " " + OrderFragment.totalAmountSIM);
                }
                if (OrderFragment.totalAmount == 0) {
                    tv_membershipFee.setText(getString(R.string.free));
                } else {
                    tv_membershipFee.setText(getString(R.string.aed) + " " + OrderFragment.totalAmount);
                }
            } else {
                packageType.setText(getString(R.string.sim_only));
                orderTypeAmount = OrderFragment.totalAmountSIM;
                if (OrderFragment.totalAmountSIM == 0) {
                    simPrice.setText(getString(R.string.free));
                } else {
                    simPrice.setText(getString(R.string.aed) + " " + OrderFragment.totalAmountSIM);
                }
            }
        }

        if (sharedPrefrencesManger.isVATEnabled()) {
            vatAmount = orderTypeAmount * 0.05;
            totalAmount = shippingMethodAmount + orderTypeAmount + vatAmount;
        } else {
            totalAmount = shippingMethodAmount + orderTypeAmount;
        }
        totalAmount = (double) Math.round(totalAmount * 100) / 100;
        totalamount.setText(getString(R.string.aed) + " " + totalAmount);


        if (OrderFragment.orderTypeStr != null) {
//            if (OrderFragment.orderTypeStr.equalsIgnoreCase("1")) { // in case order type
//                packageType.setText(getString(R.string.sim_membership));
//                packageTypeValue.setText(getString(R.string.paid_aed));
//                orderTypeAmount = 50;
//            } else {
//                packageType.setText(getString(R.string.sim_only));
//                packageTypeValue.setText(getString(R.string.free));
//                orderTypeAmount = 0;
//            }

//            packageType.setText(getString(R.string.sim));
//            tv_membershipFee.setText(getString(R.string.AED_50));
//            packageTypeValue.setText(getString(R.string.free));
//            orderTypeAmount = 50;
        }
        if (OrderFragment.shippingMethodStr != null) {
            if (OrderFragment.shippingMethodStr.equalsIgnoreCase("free")) {  // in case shipping method.
                deliveryMethod.setText(getString(R.string.standered) + " " + getString(R.string.seventy_two_hours));
                deliveryMethodValue.setText(getString(R.string.free));
                shippingMethodAmount = 0;
            } else {
                deliveryMethod.setText(getString(R.string.bullet_txt));
                deliveryMethodValue.setText(getString(R.string.AED_50));
                shippingMethodAmount = 50;
            }
        }

        if (OrderFragment.promoCode != null && OrderFragment.promoCode.length() > 0 &&
                PromoCodeVerificationSuccessfulFragment.promoCodeType > 0) {
            if (PromoCodeVerificationSuccessfulFragment.promoCodeType == 2) {
                tv_membershipFee.setText(getString(R.string.free));
                tv_packageName.setVisibility(View.VISIBLE);
                rl_extraData.setVisibility(View.GONE);

                orderTypeAmount = OrderFragment.totalAmountSIM;
                if (OrderFragment.totalAmountSIM == 0) {
                    simPrice.setText(getString(R.string.free));
                } else {
                    simPrice.setText(getString(R.string.aed) + " " + OrderFragment.totalAmountSIM);
                }
                tv_packageName.setText(PromoCodeVerificationSuccessfulFragment.promoCodeDescription);

            } else if (PromoCodeVerificationSuccessfulFragment.promoCodeType == 1) {
                orderTypeAmount = OrderFragment.totalAmount + OrderFragment.totalAmountSIM;
                if (OrderFragment.totalAmountSIM == 0) {
                    simPrice.setText(getString(R.string.free));
                } else {
                    simPrice.setText(getString(R.string.aed) + " " + OrderFragment.totalAmountSIM);
                }
                if (OrderFragment.totalAmount == 0) {
                    tv_membershipFee.setText(getString(R.string.free));
                } else {
                    tv_membershipFee.setText(getString(R.string.aed) + " " + OrderFragment.totalAmount);
                }
                rl_extraData.setVisibility(View.VISIBLE);
                tv_packageName.setVisibility(View.VISIBLE);
                tv_packageName.setText(PromoCodeVerificationSuccessfulFragment.promoCodeDescription);
            }
        }

    }

    private void setListeners() {
//        proceedToPayment.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                hideKeyBoard();
////                Intent intent = new Intent(getActivity(), PaymentActivity.class);
////                startActivity(intent);
//                regActivity.replaceFragmnet(new CashOnDeliveryFragment(),R.id.frameLayout,true);
//
//            }
//        });
        sb.setOnSeekBarChangeListener(this);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int arg1, boolean arg2) {
        if (seekBar.getProgress() < 5) {
            seekBar.setProgress(5);
        } else if (seekBar.getProgress() > 94) {
            seekBar.setProgress(95);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar arg0) {

        seekbartest.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onStopTrackingTouch(SeekBar arg0) {
        Log.d("onStopTrackingTouch", "onStopTrackingTouch");
        if (arg0.getProgress() < 80) {
            arg0.setProgress(5);
            seekbartest.setVisibility(View.VISIBLE);
            seekbartest.setText(getString(R.string.swyp_to_checkout));

        } else {
            arg0.setProgress(95);
            hideKeyBoard();
            Bundle bundle2 = new Bundle();

            bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_delivery_info_order_checkout);
            bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_delivery_info_order_checkout);
            mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_delivery_info_order_checkout, bundle2);
//            Intent intent = new Intent(getActivity(), PaymentActivity.class);
//            startActivity(intent);
            logEvent();
//            if (totalAmount == 0) {
//                onFinalizeRegisteration();
//            } else {
//                regActivity.replaceFragmnet(new CashOnDeliveryFragment(), R.id.frameLayout, true);
//            }
            if (CashOnDeliveryFragment.orderTypeStr.equalsIgnoreCase("1")) {
                Intent intent = new Intent(getActivity(), PaymentActivity.class);
                startActivity(intent);
            } else if (CashOnDeliveryFragment.orderTypeStr.equalsIgnoreCase("2")) {
                onFinalizeRegisteration();
            }

//            seekbartest.setVisibility(View.VISIBLE);
//            seekbartest.setText("Press to confirm");
//            sb.setVisibility(View.INVISIBLE);
//           proceedToPayment.setVisibility(View.VISIBLE);

        }
    }

    private void onFinalizeRegisteration() {
        regActivity.addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        finalizeRegisterationRequestInput = new FinalizeRegisterationRequestInput();
        finalizeRegisterationRequestInput.setLang(currentLanguage);
        finalizeRegisterationRequestInput.setAppVersion(appVersion);
        finalizeRegisterationRequestInput.setOsVersion(osVersion);
        finalizeRegisterationRequestInput.setToken(sharedPrefrencesManger.getToken());
        finalizeRegisterationRequestInput.setChannel(channel);
        finalizeRegisterationRequestInput.setDeviceId(deviceId);

        finalizeRegisterationRequestInput.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        if (!NewNumberFragment.msisdnNumber.equalsIgnoreCase("-1")) {
            finalizeRegisterationRequestInput.setMsisdn(NewNumberFragment.msisdnNumber);
        } else {
            finalizeRegisterationRequestInput.setMsisdn(NumberTransferFragment.mobileNoTxt);
        }
        finalizeRegisterationRequestInput.setAmount(String.valueOf(OrderSummaryFragment.totalAmount));
        finalizeRegisterationRequestInput.setShippingMethod(OrderFragment.shippingMethodStr);
        if (!NewNumberFragment.msisdnNumber.equalsIgnoreCase("-1")) {
            finalizeRegisterationRequestInput.setOrderType("registrationOrder");//in migration the value will be migrationOrder
        } else {
            finalizeRegisterationRequestInput.setOrderType("migrationOrder");//in migration the value will be migrationOrder
        }
        if (!NewNumberFragment.msisdnNumber.equalsIgnoreCase("-1")) {
            finalizeRegisterationRequestInput.setEmiratesId(EmiratesIDVerificationFragment.emiratesID);
        } else {
            finalizeRegisterationRequestInput.setEmiratesId(NameDOBFragment.emiratedID);
        }
        finalizeRegisterationRequestInput.setPackageType(OrderFragment.orderTypeStr);

        if (PromoCodeVerificationSuccessfulFragment.promoCodeType == 1) {
            finalizeRegisterationRequestInput.setPackageType("4");

        } else if (PromoCodeVerificationSuccessfulFragment.promoCodeType == 2) {
            finalizeRegisterationRequestInput.setPackageType("3");
        }
        finalizeRegisterationRequestInput.setPaymentMethod("CashOnDelivery");
        DeliveryInfo deliveryInfo = new DeliveryInfo();
        deliveryInfo.setEmirate(DeliveryDetailsAreaFragment.emirateStr);
        deliveryInfo.setCityCode(DeliveryDetailsAreaFragment.cityCodeStr);
        deliveryInfo.setArea(DeliveryDetailsAreaFragment.areaStr);
        deliveryInfo.setContactNumber(EmailAddressFragment.mobileNoStr);
        deliveryInfo.setAddressLine1(DeliveryDetailsAddress.addressLine2);
        deliveryInfo.setAddressLine2(DeliveryDetailsAddress.addressLine2);
        finalizeRegisterationRequestInput.setDeliveryInfo(deliveryInfo);
        if (OrderFragment.promoCode != null && OrderFragment.promoCode.length() > 0) {
            finalizeRegisterationRequestInput.setPromoCode(OrderFragment.promoCode);
        }
        new FinalizeRegisteraionRequestService(this, finalizeRegisterationRequestInput);
    }

    private void logEvent() {

        logEventInputModel.setActionTransactionId(sharedPrefrencesManger.getTempAppId());
        logEventInputModel.setScreenId(ConstantUtils.SCREEN_ID_1005);

        logEventInputModel.setCheckout(((TextView) view.findViewById(R.id.slider_text)).getText().toString());

        new LogApplicationFlowService(this, logEventInputModel);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
//        progressID.setVisibility(View.GONE);

        try {
            if (responseModel != null && responseModel.getResultObj() != null) {
                if (responseModel.getServiceType() != null && responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.finalize_registration)) {

                    regActivity.onBackPressed();
                    finalizeRegisterationRequestOutput = (FinalizeRegisterationRequestOutput) responseModel.getResultObj();
                    if (finalizeRegisterationRequestOutput != null) {

                        if (finalizeRegisterationRequestOutput.getErrorCode() != null || finalizeRegisterationRequestOutput.getRegistrationId() == null) {
                            onFinalizeRegisterationError();
                        } else {
                            onFinalizeRegisterationSucess();
                        }
                    } else {
                        onFinalizeRegisterationError();
                    }
                }
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

    }


    private void onFinalizeRegisterationError() {
//        showErrorDalioge(finalizeRegisterationRequestOutput.getErrorMsgEn());
//        CommonMethods.logFirebaseEvent(mFirebaseAnalytics, ConstantUtils.TAGGING_onboarding_error_cod_order_fail);
        if (finalizeRegisterationRequestOutput != null && finalizeRegisterationRequestOutput.getErrorMsgEn() != null) {
            if (currentLanguage.equals("en")) {
                showErrorFragment(finalizeRegisterationRequestOutput.getErrorMsgEn());
            } else {
                showErrorFragment(finalizeRegisterationRequestOutput.getErrorMsgAr());
            }
        } else {
            showErrorFragment(getString(R.string.error_));
        }
    }

    private void showErrorFragment(String error) {
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, error);
        ErrorFragment errorFragment = new ErrorFragment();
        errorFragment.setArguments(bundle);
        regActivity.replaceFragmnet(errorFragment, R.id.frameLayout, true);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        CommonMethods.logFirebaseEvent(mFirebaseAnalytics, ConstantUtils.TAGGING_onboarding_error_cod_order_fail);
        if (responseModel != null && responseModel.getResultObj() != null) {
            if (responseModel.getServiceType() != null && responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.finalize_registration)) {
                regActivity.onBackPressed();
                onFinalizeRegisterationError();
            }
        }
    }

    private void onFinalizeRegisterationSucess() {
        logEvent(AdjustEvents.ORDER_COMPLETE_COD, new Bundle());
        logEvent(AdjustEvents.ORDER_COMPLETE, new Bundle());
        CommonMethods.logCompletedRegistrationEvent(ConstantUtils.ACCOUNT_NUMBER);
        CommonMethods.logFirebaseEvent(mFirebaseAnalytics, ConstantUtils.TAGGING_SUCCESSFUL_ORDER);
        if (finalizeRegisterationRequestOutput != null) {
            if (finalizeRegisterationRequestOutput.getAdditionalInfo() != null && finalizeRegisterationRequestOutput.getAdditionalInfo().size() > 0) {

                for (int i = 0; i < finalizeRegisterationRequestOutput.getAdditionalInfo().size(); i++) {
                    if (finalizeRegisterationRequestOutput.getAdditionalInfo().get(i).getName().equalsIgnoreCase("OrderId")
                            && finalizeRegisterationRequestOutput.getAdditionalInfo().get(i).getValue() != null &&
                            finalizeRegisterationRequestOutput.getAdditionalInfo().get(i).getValue().length() > 0) {
                        sharedPrefrencesManger.setTransactionID(finalizeRegisterationRequestOutput.getAdditionalInfo().get(i).getValue());

                        break;

                    }
                }

                if (finalizeRegisterationRequestOutput.getRegistrationId() != null) {
                    sharedPrefrencesManger.setRegisterationID(finalizeRegisterationRequestOutput.getRegistrationId());
                    PaymentSucessfulllyFragment paymentSucessfulllyFragment = new PaymentSucessfulllyFragment();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("isCOD", true);
                    paymentSucessfulllyFragment.setArguments(bundle);
                    sharedPrefrencesManger.setLastStatus("UPL");
                    if (!NewNumberFragment.msisdnNumber.equalsIgnoreCase("-1")) {
                        sharedPrefrencesManger.setMobileNo(NewNumberFragment.msisdnNumber);
                    } else {
                        sharedPrefrencesManger.setMobileNo(NumberTransferFragment.mobileNoTxt);
                    }

                    regActivity.replaceFragmnet(paymentSucessfulllyFragment, R.id.frameLayout, true);
                } else {
//                    CommonMethods.logFirebaseEvent(mFirebaseAnalytics, ConstantUtils.TAGGING_onboarding_error_cod_order_fail);
                }

            }
        }
    }

}
