package com.indy.views.fragments.utils;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.widget.DatePicker;

import com.indy.R;
import com.indy.controls.DailogeInterface;
import com.indy.views.fragments.buynewline.NameDOBFragment;
import com.indy.views.fragments.buynewline.Registeration4Fragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Eiman on 1/26/2016 AD.
 */
public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener, DailogeInterface {
    String dateSet, dateFormatter;
    Date selctedDate;
    long datePickerLong;
    Date calendarDate;
    Date currenDate;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
//        updateEnglishLocalization();
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), R.style.DateTimePickers, this, year, month, day);
        Calendar cal1 = Calendar.getInstance();

        cal1.setTimeInMillis(new Date().getTime());
        cal1.add(Calendar.YEAR,-18);
//        cal1.add(Calendar.DAY_OF_MONTH,-1);
        datePickerDialog.getDatePicker().setMaxDate(cal1.getTimeInMillis());
        cal1.setTime(new Date());
        cal1.add(Calendar.DAY_OF_MONTH,1);
        cal1.add(Calendar.YEAR,-25);
        datePickerDialog.getDatePicker().setMinDate(cal1.getTimeInMillis());
        datePickerDialog.getDatePicker().init(cal1.get(Calendar.YEAR), cal1.get(Calendar.MONTH), cal1.get(Calendar.DAY_OF_MONTH) - 1, new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker datePicker, int i, int i1, int i2) {



            }
        });

        return datePickerDialog;
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        //TODO something with the date chosen by the user
        month = month + 1;
        Log.w("DatePicker", day + "/" + month + "/" + year);
        NameDOBFragment.dateSelectInterface.onDateSelect(String.valueOf(day), String.valueOf(month), String.valueOf(year));
        dateSet = day + "/" + month + "/" + year;
        Calendar cal = Calendar.getInstance();
        cal.set(year, month - 1, day);
        selctedDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd")/*.format(cal.getTime())*/;
        selctedDate = cal.getTime();
        datePickerLong = cal.getTime().getTime();

        Calendar currentDate = Calendar.getInstance();
        int mYear = currentDate.get(Calendar.YEAR);
        int mMonth = currentDate.get(Calendar.MONTH);
        int mDay = currentDate.get(Calendar.DAY_OF_MONTH);
        currentDate.set(mYear, mMonth + 1, mDay);
        Long currentDateLong = currentDate.getTime().getTime();

        String selectedDate = sdf.format(cal.getTime());
        try {
            calendarDate = sdf.parse(year + "-" + month + "-" + day);
            currenDate = sdf.parse(mYear + "-" + (mMonth + 1) + "-" + mDay);
        } catch (ParseException e) {
            e.printStackTrace();
        }
//        Log.v("date is", calendarDate + " " + year+" "+month+ day);
//        if (!MainActivity.isFilterFromMain) {
//            AppointmentFilterFragmnet.selectTimeId.setText(selectedDate);
//        } else {
//            TextView txtView = ReserveAppointmentFragmnet.dateLayoutId;
//            SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
//            try {
//                Date date1 = sd.parse("2016-7-1");
//                Date date2 = sd.parse("2010-6-6");
//                Log.v("dates", calendarDate + " " + currentDate);
//                if (calendarDate.compareTo(currenDate) < 0) {
//                    AlertDailogeHelper.showDaiogue(getActivity(), this, getString(R.string.error), getString(R.string.select_date_error), getString(R.string.ok), "", " ");
//                } else {
//                    txtView.setText(selectedDate);
//                    ReserveAppointmentFragmnet.dateTextId.setVisibility(View.VISIBLE);
//                }
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//
////            if (calendarDate.compareTo(calendarDate) > 0) {
////                AlertDailogeHelper.showDaiogue(getActivity(), this, getString(R.string.error), getString(R.string.select_date_error), getString(R.string.ok), "", " ");
////            } else {
////                txtView.setText(selectedDate);
////                ReserveAppointmentFragmnet.dateTextId.setVisibility(View.VISIBLE);
////
////            }
//        }

        //....................................

//

        //....................................
//        updateEnglishLocalization();
        //.........................
    }

    public void updateArabicLocalization() {
        Locale locale = new Locale("ar");
        Locale.setDefault(locale);        // Create a new instance of DatePickerDialog and return it
        Configuration config = new Configuration();
        config.locale = locale;
        getActivity().getApplicationContext().getResources().updateConfiguration(config, getActivity().getResources().getDisplayMetrics());
    }

    public void updateEnglishLocalization() {
        Locale locale = new Locale("en");
        Locale.setDefault(locale);        // Create a new instance of DatePickerDialog and return it
        Configuration config = new Configuration();
        config.locale = locale;
        getActivity().getApplicationContext().getResources().updateConfiguration(config,
                getActivity().getResources().getDisplayMetrics());
    }

    @Override
    public void onAgreeListener(String alertType) {

    }

    @Override
    public void onCancelListener(String alertType) {

    }
}

