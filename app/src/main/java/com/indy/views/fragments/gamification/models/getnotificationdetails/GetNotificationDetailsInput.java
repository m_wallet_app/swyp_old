package com.indy.views.fragments.gamification.models.getnotificationdetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GamificationBaseInputModel;

/**
 * Created by Tohamy on 10/4/2017.
 */

public class GetNotificationDetailsInput extends GamificationBaseInputModel {
    @SerializedName("notificationId")
    @Expose
    private String notificationId;

    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }
}
