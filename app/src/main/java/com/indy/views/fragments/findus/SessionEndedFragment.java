package com.indy.views.fragments.findus;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.indy.R;
import com.indy.views.fragments.utils.MasterFragment;


public class SessionEndedFragment extends MasterFragment {

    View view;
    Button button;
    public SessionEndedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_session_ended, container, false);
        initUI();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
        button =(Button) view.findViewById(R.id.btn_ok);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });
    }
}
