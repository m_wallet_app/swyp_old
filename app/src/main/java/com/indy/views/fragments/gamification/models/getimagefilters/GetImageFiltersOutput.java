package com.indy.views.fragments.gamification.models.getimagefilters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

import java.util.List;

/**
 * Created by Tohamy on 10/3/2017.
 */

public class GetImageFiltersOutput extends MasterErrorResponse {

    @SerializedName("filters")
    @Expose
    private List<ImageFilter> filters = null;

    public List<ImageFilter> getFilters() {
        return filters;
    }

    public void setFilters(List<ImageFilter> filters) {
        this.filters = filters;
    }

}
