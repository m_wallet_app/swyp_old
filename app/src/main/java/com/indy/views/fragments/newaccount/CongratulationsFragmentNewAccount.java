package com.indy.views.fragments.newaccount;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.indy.R;
import com.indy.customviews.CustomTextView;
import com.indy.models.getUserMessage.GetUserMessageInputModel;
import com.indy.models.getUserMessage.GetUserMessageOutputModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.GetMessageForUserService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.utils.MaterialCheckBox;
import com.indy.views.activites.SwypeToReachUsFragmnet;
import com.indy.views.fragments.login.InviteCodeFragment;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.MasterFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class CongratulationsFragmentNewAccount extends MasterFragment {

    private View view;
    private Button button;
    private CustomTextView tv_message;
    public CongratulationsFragmentNewAccount() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_congratulations_fragment_new_account, container, false);
        initUI();
        onConsumeService();
        setListeners();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
        button = (Button) view.findViewById(R.id.okBtn);
        tv_message = (CustomTextView) view.findViewById(R.id.tv_success_create_account);

        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_create_account_success_page);
        MaterialCheckBox materialCheckBox = (MaterialCheckBox) view.findViewById(R.id.fl_tick_animation);
        ImageView iv_tick = (ImageView) view.findViewById(R.id.iv_tick);
        CommonMethods.drawCircle(materialCheckBox, iv_tick);
        regActivity.hideHeaderLayout();
    }

    private void setListeners() {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSkip();
                regActivity.replaceFragmnet(new SwypeToReachUsFragmnet(), R.id.frameLayout, true);
            }
        });
    }

    private void onSkip() {
        Fragment[] myFragments = {new CongratulationsFragmentNewAccount(), new NewAccountFragment(),
                new NewAccountCodeFragment(),
                new SetPasswordFragment(),
                new InviteCodeFragment()
        };

        regActivity.removeFragment(myFragments);
    }


    @Override
    public void onConsumeService() {
        super.onConsumeService();
        GetUserMessageInputModel getUserMessageInputModel = new GetUserMessageInputModel();
        getUserMessageInputModel.setMessageKey(ConstantUtils.KEY_INVITE_FRIEND_SERVICE);
        getUserMessageInputModel.setAppVersion(appVersion);
        getUserMessageInputModel.setChannel(channel);
        getUserMessageInputModel.setLang(currentLanguage);
        getUserMessageInputModel.setToken(token);
        getUserMessageInputModel.setOsVersion(osVersion);
        getUserMessageInputModel.setDeviceId(deviceId);
        if(getArguments()!=null && getArguments().getString(ConstantUtils.newAccountMobileNo)!=null) {
            getUserMessageInputModel.setMsisdn(getArguments().getString(ConstantUtils.newAccountMobileNo));
        }else{
            getUserMessageInputModel.setMsisdn("0544485993");
        }
        getUserMessageInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
        new GetMessageForUserService(this,getUserMessageInputModel);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            if(responseModel!=null && responseModel.getResultObj()!=null) {
                GetUserMessageOutputModel getUserMessageOutputModel = (GetUserMessageOutputModel) responseModel.getResultObj();
                if (getUserMessageOutputModel != null && getUserMessageOutputModel.getMessage() != null && getUserMessageOutputModel.getMessage().length() > 0) {
                    tv_message.setText(getUserMessageOutputModel.getMessage());
                } else {
                    if (getUserMessageOutputModel != null && getUserMessageOutputModel.getErrorCode() != null && getUserMessageOutputModel.getErrorMsgEn() != null) {
                        if (currentLanguage.equals("en")) {
                            showErrorFragment(getUserMessageOutputModel.getErrorMsgEn());
                        } else {
                            showErrorFragment(getUserMessageOutputModel.getErrorMsgAr());
                        }
                    } else {
                        showErrorFragment(getString(R.string.generice_error));
                    }
                }
            }
        }catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
    }

    private void showErrorFragment(String error) {
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, error);
        errorFragment.setArguments(bundle);
        regActivity.replaceFragmnet(errorFragment, R.id.frameLayout, true);
    }
}
