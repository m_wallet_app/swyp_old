package com.indy.views.fragments.buynewline;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.helpers.ValidationHelper;
import com.indy.services.LogApplicationFlowService;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.utils.MasterFragment;

import static com.indy.views.activites.RegisterationActivity.logEventInputModel;

/**
 * Created by emad on 9/7/16.
 */
public class Registeration3Fragment extends MasterFragment {
    private View view;
    private Button nextBtn;
    private EditText fullName, email;
    public static String fullnameStr, emailStr;
    private TextInputLayout fullNameTxtInput, emailTxtInput;
    private CharSequence emailCharSeq, fullNameCharSeq;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_3_registeration, container, false);
            initUI();
        }
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
        regActivity.showHeaderLayout();
        regActivity.setHeaderTitle("");
        nextBtn = (Button) view.findViewById(R.id.nextBtn);
        fullName = (EditText) view.findViewById(R.id.fullName);
        email = (EditText) view.findViewById(R.id.email);
        emailTxtInput = (TextInputLayout) view.findViewById(R.id.emailTxtInput);

        fullNameTxtInput = (TextInputLayout) view.findViewById(R.id.fullNameTxtInput);
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()) {
                    hideKeyBoard();
                    Bundle bundle2 = new Bundle();

                    bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_name_email_entered);
                    bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_name_email_entered);
                    mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_name_email_entered, bundle2);

                    Registeration4Fragment registeration4Fragment = new Registeration4Fragment();
                    Bundle bundle = new Bundle();
                    bundle.putString(ConstantUtils.userName, fullName.getText().toString());
                    bundle.putString(ConstantUtils.email, email.getText().toString());
                    fullnameStr = fullName.getText().toString();
                    emailStr = email.getText().toString();
                    registeration4Fragment.setArguments(bundle);
                    logEvent();
                    regActivity.replaceFragmnet(registeration4Fragment, R.id.frameLayout, true);
                }
            }
        });

        disableNextBtn();
        changeNextBtnState();
    }

    private void logEvent() {
        logEventInputModel.setActionTransactionId(sharedPrefrencesManger.getTempAppId());
        logEventInputModel.setScreenId(ConstantUtils.SCREEN_ID_0002);

        logEventInputModel.setFullName(fullName.getText().toString());
        logEventInputModel.setEmail(email.getText().toString());

        new LogApplicationFlowService(this, logEventInputModel);

    }

    private boolean isValid() {
        boolean isVaild = true;


        if (fullName.getText().toString().trim().length() == 0) {
            setTextInputLayoutError(fullNameTxtInput, getString(R.string.enter_full_name));
            isVaild = false;
        } else if (!ValidationHelper.isAlphabetical(fullName.getText().toString().trim())) {
            isVaild = false;
            setTextInputLayoutError(fullNameTxtInput, getString(R.string.only_alphabets_are_allowed));
        } else {
            removeTextInputLayoutError(fullNameTxtInput);
        }
        if (email.getText().toString().trim().length() == 0) {
            setTextInputLayoutError(emailTxtInput, getString(R.string.enter_email));
            isVaild = false;

        } else {
            if ((validationHelper.isEmailValid(email.getText().toString().trim()))) {
                removeTextInputLayoutError(emailTxtInput);
            } else {
                setTextInputLayoutError(emailTxtInput, getString(R.string.enter_valid_email));
                isVaild = false;
                Bundle bundle2 = new Bundle();

                bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_incorrect_email_entererd);
                bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_incorrect_email_entererd);
                mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_incorrect_email_entererd, bundle2);
            }
        }
        return isVaild;
    }

    public void changeNextBtnState() {
        fullName.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
                fullNameCharSeq = s;
                if (emailCharSeq != null)
                    if (s.length() == 0 || emailCharSeq.length() == 0)
                        disableNextBtn();
                    else
                        enableNextBtn();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
            }
        });
        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
                emailCharSeq = s;
                if (fullNameCharSeq != null)
                    if (s.length() == 0 || fullNameCharSeq.length() == 0)
                        disableNextBtn();
                    else
                        enableNextBtn();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
            }
        });
    }

    private void enableNextBtn() {
        nextBtn.setAlpha(ConstantUtils.ALPHA_BUTTON_ENABLED);
        nextBtn.setClickable(true);
    }

    private void disableNextBtn() {
        nextBtn.setAlpha(ConstantUtils.ALPHA_BUTTON_DISABLED);
        nextBtn.setClickable(false);
    }
}
