package com.indy.views.fragments.rewards;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.adapters.RewardsPagerAdapter;
import com.indy.controls.ServiceUtils;
import com.indy.customviews.CustomTabLayout;
import com.indy.customviews.CustomTextView;
import com.indy.models.balance.BalanceResponseModel;
import com.indy.models.rewards.Category;
import com.indy.models.rewards.GetCategoriesListInput;
import com.indy.models.rewards.GetCategoriesOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.new_membership.NewMembershipSuccessListener;
import com.indy.services.GetCategoriesService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.SwpeMainActivity;
import com.indy.views.fragments.BenefitsFragment;
import com.indy.views.fragments.NewMembershipBenefitsFragment;
import com.indy.views.fragments.NewMembershipUpgradeBenefitsFragment;
import com.indy.views.fragments.rewards.ItemsCarousel.MainAvailableRewardsFragment;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.MasterFragment;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.HashMap;
import java.util.List;

/**
 * Created by emad on 8/9/16.
 */
public class RewardsFragment extends MasterFragment implements NewMembershipSuccessListener {

    private View view;
    private ViewPager viewPager;
    private TextView tv_membershipDate;
    public static TextView balanceID;
    private BaseResponseModel mResponseModel;
    private BalanceResponseModel balanceResponseModel;
    private LinearLayout ll_noMembershipView, ll_viewPager;
    public boolean isMembershipValid = true;
    private CustomTextView btn_membership;
    private TextView tv_membershipCycleText, tv_membershipCycle;
    public static String you_have_saved, aed, this_month;
    LinearLayout used, avilable, ll_tabs;
    FrameLayout avilableFrameLayout, usedFrameLayout;
    private View usedView, ailableView;
    private boolean isUsedClicked, isAvilabledClicked;
    public static RewardsFragment rewardsFragmentInstance;
    public static boolean reloadAgain = false;
    private TextView usageTxtID;
    private ImageView ic_scroll_up;
    private RelativeLayout valid_member_ship;
    public static String expiryDateEn;
    public static String expiryDateAr;
    public GetCategoriesOutput getCategoriesOutput;
    public static List<Category> categoriesList;
    public static RecyclerView rv_categories;

    //Bottom sheet for categories
    public static TextView tv_apply;
    public static Button resetBtn;
    public static View bottomSheet;
    private Bundle getArguments;

    // test git
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_rewards, container, false);
            initUI();
        }
        return view;

    }


    @Override
    public void initUI() {
        super.initUI();
//        expiryTxt = (TextView) view.findViewById(R.id.expiryTxt);
        balanceID = (TextView) view.findViewById(R.id.tv_saved_rewards);

        ll_noMembershipView = (LinearLayout) view.findViewById(R.id.ll_no_membership);
        valid_member_ship = (RelativeLayout) view.findViewById(R.id.valid_member_ship);
        ll_viewPager = (LinearLayout) view.findViewById(R.id.ll_viewPager);
        btn_membership = (CustomTextView) view.findViewById(R.id.membershipBtnNoMembership);
        viewPager = (ViewPager) view.findViewById(R.id.pager);
        tv_membershipCycle = (TextView) view.findViewById(R.id.tv_membership_cycle);
        tv_membershipDate = (TextView) view.findViewById(R.id.tv_membership_date);
        tv_membershipCycleText = (TextView) view.findViewById(R.id.tv_membership_cycle_text);
//        tv_membershipDate.setText(getString(R.string.membership_expires) + "30" + getString(R.string.membership_cycle_days) + getString(R.string.membership_cycle_daysLeft));
        used = (LinearLayout) view.findViewById(R.id.used);
        avilable = (LinearLayout) view.findViewById(R.id.avilable);
        usedView = (View) view.findViewById(R.id.usedView);
        ailableView = (View) view.findViewById(R.id.ailableView);
        avilableFrameLayout = (FrameLayout) view.findViewById(R.id.avilableFrameLayout);
        usedFrameLayout = (FrameLayout) view.findViewById(R.id.usedFrameLayout);
        ll_tabs = (LinearLayout) view.findViewById(R.id.ll);
        usageTxtID = (TextView) view.findViewById(R.id.usageTxtID);
        ic_scroll_up = (ImageView) view.findViewById(R.id.ic_scroll_up);
        ic_scroll_up.setBackground(getResources().getDrawable(R.drawable.ic_scroll));

        usageTxtID.setText(getString(R.string.rewards_capital_header_spaced));

        LinearLayout lv_main_swip = (LinearLayout) view.findViewById(R.id.lv_main_swip);
        lv_main_swip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((SwpeMainActivity) getActivity()).mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED)
                    ((SwpeMainActivity) getActivity()).expandMenu();
                else if (((SwpeMainActivity) getActivity()).mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED)
                    ((SwpeMainActivity) getActivity()).collapseMenu();
            }
        });
        isMembershipValid = true;
        rewardsFragmentInstance = this;
        reloadAgain = false;
        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) view.findViewById(R.id.coordinateLayoutID);

        bottomSheet = coordinatorLayout.findViewById(R.id.bottom_sheet_linear_layout);
        tv_apply = (TextView) view.findViewById(R.id.tv_apply);
        resetBtn = (Button) view.findViewById(R.id.resetBtn);
        rv_categories = (RecyclerView) view.findViewById(R.id.rv_categories);

        if (sharedPrefrencesManger != null) {
//            Fri Dec 02 11:11:46 GST 2016
            // EEE MMM dd HH:mm:ss T yyyy
            // dd MMMM, HH:mm
            if (sharedPrefrencesManger.isVATEnabled()) {
                Double price = Double.parseDouble(sharedPrefrencesManger.getAfterLoginPrice());
                tv_membershipCycle.setText(getString(R.string.you_dont_have_a_swyp_membership));
            } else {
                tv_membershipCycle.setText(getString(R.string.you_dont_have_a_swyp_membership));
            }
            if (sharedPrefrencesManger.getUserProfileObject() != null) {
                if (sharedPrefrencesManger.getUserProfileObject().getMembershipData() != null) {
                    if (sharedPrefrencesManger.getUserProfileObject().getMembershipData().getStatus() == ConstantUtils.validMembership) {
                        tv_membershipCycleText.setText(" " + /*DateHelpers.getExpirtyDate(*/sharedPrefrencesManger.getUserProfileObject().getMembershipData().getExpiryDate()/*)*/);
                        isMembershipValid = true;
//                        tv_membershipDate.setVisibility(View.VISIBLE);
                    } else {
                        tv_membershipDate.setText(getString(R.string.no_valid_membership));
                        isMembershipValid = false;
                        tv_membershipCycleText.setVisibility(View.GONE);
                        ll_viewPager.setVisibility(View.GONE);
                        ll_noMembershipView.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
        you_have_saved = getString(R.string.you_have_saved);
        aed = getString(R.string.aed);
        this_month = getString(R.string.this_month);
//        new GetUserBalanceService(this,null);


        if (sharedPrefrencesManger != null) {
////            Fri Dec 02 11:11:46 GST 2016
//            // EEE MMM dd HH:mm:ss T yyyy
//            // dd MMMM, HH:mm
            if (sharedPrefrencesManger != null)
                if (sharedPrefrencesManger.getUserProfileObject() != null) {
                    if (sharedPrefrencesManger.getUserProfileObject().getMembershipData() != null) {
                        if (sharedPrefrencesManger.getUserProfileObject().getMembershipData().getStatus() == ConstantUtils.validMembership) {
                            tv_membershipDate.setVisibility(View.VISIBLE);

                        } else {
                            tv_membershipDate.setVisibility(View.GONE);

                        }
                    }

                }
        }
        btn_membership.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //dont happen yet
//                Intent intent = new Intent(getActivity(), BenefitsFragment.class);
//                startActivity(intent);
                NewMembershipUpgradeBenefitsFragment newMembershipUpgradeBenefitsFragment = new NewMembershipUpgradeBenefitsFragment();
                newMembershipUpgradeBenefitsFragment.setNewMembershipSuccessListener(RewardsFragment.this);
                ((SwpeMainActivity) getActivity()).replaceFragmnet(newMembershipUpgradeBenefitsFragment, R.id.contentFrameLayout, true);
            }
        });
        if (isMembershipValid) {
//            valid_member_ship.setVisibility(View.VISIBLE);
            //TODO added line below
            valid_member_ship.setVisibility(View.GONE);
            onConsumeService();
//            showUsedAndAvilableFragmnets();

        } else {
            valid_member_ship.setVisibility(View.GONE);
            initMembershipLayout();
        }
        Bundle bundle = new Bundle();

        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "RewardsScreen");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "RewardsScreen");
        mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_REWARDS
                , bundle);


        setSavedValue("0");

    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        GetCategoriesListInput listInput = new GetCategoriesListInput();
        listInput.setLang(currentLanguage);
        listInput.setMsisdn(sharedPrefrencesManger.getMobileNo());
        new GetCategoriesService(this, listInput);
    }


    private void initMembershipLayout() {
        ll_noMembershipView.setVisibility(View.VISIBLE);
        ll_viewPager.setVisibility(View.GONE);
        used.setVisibility(View.GONE);
        avilable.setVisibility(View.GONE);
        avilableFrameLayout.setVisibility(View.GONE);
        usedFrameLayout.setVisibility(View.GONE);
        ll_tabs.setVisibility(View.VISIBLE);
        balanceID.setText(getString(R.string.you_have_not_saved_yet));
        used.setVisibility(View.VISIBLE);
        used.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(0);
                //TODO ABID
//                usedView.setVisibility(View.VISIBLE);
//                ailableView.setVisibility(View.INVISIBLE);
            }
        });

        avilable.setVisibility(View.VISIBLE);
        avilable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(1);
                //TODO ABID
//                usedView.setVisibility(View.INVISIBLE);
//                ailableView.setVisibility(View.VISIBLE);
            }
        });
        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_rewards_no_membership);


    }

    private void showUsedAndAvilableFragmnets() {
        try {

            avilableFrameLayout.setVisibility(View.VISIBLE);
            ll_noMembershipView.setVisibility(View.GONE);
            ll_viewPager.setVisibility(View.GONE);
            if ((SwpeMainActivity) getActivity() != null) {
                if (getArguments() != null && getArguments().containsKey(ConstantUtils.KEY_NOTIFICATION_ITEM_ID)) {
                    MainAvailableRewardsFragment mainAvailableRewardsFragment = new MainAvailableRewardsFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString(ConstantUtils.KEY_NOTIFICATION_ITEM_ID, getArguments().getString(ConstantUtils.KEY_NOTIFICATION_ITEM_ID));
                    mainAvailableRewardsFragment.setArguments(bundle);
                    ((SwpeMainActivity) getActivity()).replaceFragmnet(mainAvailableRewardsFragment, R.id.avilableFrameLayout, false);

                } else {
                    ((SwpeMainActivity) getActivity()).replaceFragmnet(new MainAvailableRewardsFragment(), R.id.avilableFrameLayout, false);

                }
            }
            used.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!isUsedClicked) {
                        usedView.setVisibility(View.VISIBLE);
                        ailableView.setVisibility(View.INVISIBLE);
//                avilableFrameLayout.setVisibility(View.INVISIBLE);
//                usedFrameLayout.setVisibility(View.VISIBLE);
                        Fragment usedRewardsFrag = new UsedRewardsFragment();
                        ((SwpeMainActivity) getActivity()).replaceFragmnet(usedRewardsFrag, R.id.avilableFrameLayout, false);
                        isUsedClicked = true;
                        isAvilabledClicked = false;
                        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_reward_available_rewards_clicked);

                    }
                }
            });

            avilable.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!isAvilabledClicked) {
                        usedView.setVisibility(View.INVISIBLE);
                        ailableView.setVisibility(View.VISIBLE);
//                usedFrameLayout.setVisibility(View.INVISIBLE);
//                avilableFrameLayout.setVisibility(View.VISIBLE);
                        ((SwpeMainActivity) getActivity()).replaceFragmnet(new MainAvailableRewardsFragment(), R.id.avilableFrameLayout, false);
                        isUsedClicked = false;
                        isAvilabledClicked = true;
                        CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_reward_used_rewards_clicked);

                    }
                }
            });
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }

    }

    public void selectUsedRewards() {
        try {
            avilableFrameLayout.setVisibility(View.VISIBLE);
//        usedFrameLayout.setVisibility(View.VISIBLE);
            ll_noMembershipView.setVisibility(View.GONE);
            ll_viewPager.setVisibility(View.GONE);
            usedView.setVisibility(View.VISIBLE);
            ailableView.setVisibility(View.INVISIBLE);
//        UsedRewardsFragment.usedRewardsFragment.onDetach();
            SwpeMainActivity.swpeMainActivityInstance.replaceFragmnet(new UsedRewardsFragment(), R.id.avilableFrameLayout, false);
//        UsedRewardsFragment.usedRewardsFragment.refreshView();
            used.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!isUsedClicked) {
                        usedView.setVisibility(View.VISIBLE);
                        ailableView.setVisibility(View.INVISIBLE);
//                avilableFrameLayout.setVisibility(View.INVISIBLE);
//                usedFrameLayout.setVisibility(View.VISIBLE);
                        ((SwpeMainActivity) getActivity()).replaceFragmnet(new UsedRewardsFragment(), R.id.avilableFrameLayout, false);
                        isUsedClicked = true;
                        isAvilabledClicked = false;
                    }
                }
            });

            avilable.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!isAvilabledClicked) {
                        usedView.setVisibility(View.INVISIBLE);
                        ailableView.setVisibility(View.VISIBLE);
//                usedFrameLayout.setVisibility(View.INVISIBLE);
//                avilableFrameLayout.setVisibility(View.VISIBLE);
                        ((SwpeMainActivity) getActivity()).replaceFragmnet(new MainAvailableRewardsFragment(), R.id.avilableFrameLayout, false);
                        isUsedClicked = false;
                        isAvilabledClicked = true;
                    }
                }
            });
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }
    }

    private void initTabLayout() {
        final CustomTabLayout tabLayout = (CustomTabLayout) view.findViewById(R.id.tab_layout);
        tabLayout.setSharedPrefrencesManger(sharedPrefrencesManger);
        tabLayout.addTab(tabLayout.newTab().setText((getString(R.string.avilable_rewards))));
        tabLayout.addTab(tabLayout.newTab().setText((getString(R.string.used_rewards))));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        if (isMembershipValid) {
            getArguments = getArguments();
            final RewardsPagerAdapter adapter = new RewardsPagerAdapter
                    (getActivity().getSupportFragmentManager(), tabLayout.getTabCount(), getArguments);
            viewPager.setAdapter(adapter);
            viewPager.setCurrentItem(0, true);
            viewPager.setSelected(true);
            viewPager.setFocusable(true);
            tabLayout.getTabAt(0).select();
            ll_viewPager.setVisibility(View.VISIBLE);
            avilableFrameLayout.setVisibility(View.GONE);
            ll_noMembershipView.setVisibility(View.GONE);
//        tabLayout.setFillViewport(true);
            viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
            tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    viewPager.setCurrentItem(tab.getPosition());

                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });
            getArguments = null;
        }
    }

    public void loadIcons() {
        for (final Category category : categoriesList) {
            Picasso.with(getActivity())
                    .load(category.getCategoryImageUrl())
                    .resize(CommonMethods.dip2px(getActivity(), 20), CommonMethods.dip2px(getActivity(), 30))
                    .into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            categoryIcons.put(category.getCategoryName(), bitmap);
                        }

                        @Override
                        public void onBitmapFailed(Drawable errorDrawable) {

                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {

                        }
                    });
        }
    }

    public static HashMap<String, Bitmap> categoryIcons = new HashMap<>();

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            this.mResponseModel = responseModel;
            if (responseModel != null && responseModel.getResultObj() != null) {
                if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.balanceServiceType)) {
                    onGetBalance(mResponseModel);
                } else if (responseModel.getResultObj() != null && responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.getCategories)) {
                    getCategoriesOutput = (GetCategoriesOutput) responseModel.getResultObj();
                    if (getCategoriesOutput != null && getCategoriesOutput.getCategories() != null && getCategoriesOutput.getCategories().size() > 0) {
                        categoriesList = getCategoriesOutput.getCategories();
                        loadIcons();
                        initTabLayout();
//                        showUsedAndAvilableFragmnets();
                    }
                }
            } else {
                super.onErrorListener(responseModel);
            }
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }
    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        try {
            if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.getCategories)) {

//                showUsedAndAvilableFragmnets();
            }
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }
    }

    /*
    on get balance response
    incase of sucess get packges and payperus
     */
    private void onGetBalance(BaseResponseModel responseModel) {
        balanceResponseModel = (BalanceResponseModel) responseModel.getResultObj();
        if (balanceResponseModel != null) {
            if (balanceResponseModel.getErrorCode() != null)
//            expiryTxt.setText(balanceResponseModel.getExpiryTextEn() + " " + balanceResponseModel.getExpiryIntervalEn());
                onGetBalanceError();
            else
                onGetBalanceSucess();
        }
    }

    private void onGetBalanceError() {
        Fragment errorFragmnet = new ErrorFragment();
        Bundle bundle = new Bundle();
        if (currentLanguage != null) {
            if (currentLanguage.equalsIgnoreCase("en"))
                bundle.putString(ConstantUtils.errorString, balanceResponseModel.getErrorMsgEn());
            else
                bundle.putString(ConstantUtils.errorString, balanceResponseModel.getErrorMsgAr());
        }
        errorFragmnet.setArguments(bundle);
        ((SwpeMainActivity) getActivity()).replaceFragmnet(errorFragmnet, R.id.contentFrameLayout, true);
    }

    private void onGetBalanceSucess() {
        balanceID.setText(balanceResponseModel.getAmount() + " " + balanceResponseModel.getAmountUnitEn());

    }

    public void setSavedValue(String amount) {

        if (Integer.parseInt(amount) > 0) {

            if (currentLanguage.equalsIgnoreCase("en"))
                balanceID.setText(you_have_saved + " " + aed + " " +
                        amount + " " + this_month);
            else
                balanceID.setText(you_have_saved + " " + amount + " " +
                        aed + " " + this_month);
        } else {
            balanceID.setText(getString(R.string.you_have_not_saved_yet));
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (reloadAgain) {
            if (isVisible())
                selectUsedRewards();
        }
    }

    public void onSlideUp() {
        if (usageTxtID != null) {
            usageTxtID.setVisibility(View.VISIBLE);
            if (isVisible())
                ic_scroll_up.setBackground(getResources().getDrawable(R.drawable.ic_scroll));
        }
    }

    public void onSlideDown() {
        if (usageTxtID != null) {
            usageTxtID.setVisibility(View.INVISIBLE);
            if (isVisible())
                ic_scroll_up.setBackground(getResources().getDrawable(R.drawable.ic_up));
        }
    }

    @Override
    public void onOKPressed() {
        ((SwpeMainActivity) getActivity()).onBackPressed();
    }
}