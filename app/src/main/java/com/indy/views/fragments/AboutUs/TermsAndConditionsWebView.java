package com.indy.views.fragments.AboutUs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;

import com.indy.R;
import com.indy.controls.BaseInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.faqs.FaqsInputModel;
import com.indy.models.faqs.FaqsResponseModel;
import com.indy.models.inilizeregisteration.InilizeRegisterationInput;
import com.indy.models.inilizeregisteration.InilizeRegisteratonOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.services.FaqsService;
import com.indy.utils.ConstantUtils;
import com.indy.utils.SharedPrefrencesManger;
import com.indy.views.activites.AboutAppActivity;
import com.indy.views.activites.MasterActivity;

/**
 * Created by Amir.jehangir on 10/10/2016.
 */
public class TermsAndConditionsWebView extends MasterActivity implements BaseInterface {

    private View view;
    AboutAppActivity aboutActivity;
    private WebView webView;
    private InilizeRegisterationInput inilizeRegisterationInput;
    FaqsResponseModel faqsResponseModel;
    InilizeRegisteratonOutput inilizeRegisteratonOutput;
    BaseResponseModel mResponseModel;
    private Button backImg;
    public SharedPrefrencesManger sharedPrefrencesManger;
    public String appVersion, token, osVersion, channel, deviceId,authToken;

//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        view = inflater.inflate(R.layout.fragment_termsconditions_webview, container, false);
//        initUI();
//        onConsumeService();
//        return view;
//    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_termsconditions_webview);
        initUI();
        onConsumeService();
    }

    @Override
    public void initUI() {
        sharedPrefrencesManger=new SharedPrefrencesManger(getApplicationContext());
        super.initUI();
//        aboutActivity = (AboutAppActivity)getActivity();
        webView = (WebView) findViewById(R.id.webView);
        backImg = (Button) findViewById(R.id.backImg);
        WebSettings settings = webView.getSettings();
        webView.setBackgroundColor(0);
//        progressID.setVisibility(View.VISIBLE);
        settings.setJavaScriptEnabled(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                getActivity().onBackPressed();
                finish();
            }
        });

        //..............header Buttons............
//        ImageView helpLayout = (ImageView) view.findViewById(R.id.helpBtn);
//
//        helpLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getActivity(), HelpActivity.class);
//                startActivity(intent);
//            }
//        });

//        backLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                hideKeyBoard();
//                aboutActivity.onBackPressed();
//            }
//        });
        //..............header Buttons............
    }

    @Override
    public void onConsumeService() {
//        super.onConsumeService();
      //  ((AboutAppActivity) getActivity()).addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        FaqsInputModel faqsInputModel = new FaqsInputModel();
        faqsInputModel.setAppVersion(sharedPrefrencesManger.getAppVersion());
        faqsInputModel.setDeviceId(sharedPrefrencesManger.getDeviceId());
        faqsInputModel.setToken(sharedPrefrencesManger.getToken());
        faqsInputModel.setOsVersion(ConstantUtils.osVersion);
        faqsInputModel.setChannel(ConstantUtils.channel);
        faqsInputModel.setLang(currentLanguage);
        //0562237176
//        faqsInputModel.setSignificant("REG_FAQ");
        faqsInputModel.setSignificant(ConstantUtils.REG_TERMSANDCONDITIONS);
        faqsInputModel.setAuthToken(sharedPrefrencesManger.getAuthToken());
        new FaqsService(this, faqsInputModel);

    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
//        super.onSucessListener(responseModel);
//        if (isVisible()) {
        try {
            if(responseModel!=null && responseModel.getResultObj()!=null) {
                mResponseModel = responseModel;
//        progressID.setVisibility(View.GONE);
                if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.faqsService))
                    onTermsAndCoditionService();
            }
//        }
        }catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
//        super.onUnAuthorizeToken(masterErrorResponse);
    }

    private void onTermsAndCoditionService() {
        faqsResponseModel = (FaqsResponseModel) mResponseModel.getResultObj();
        if (faqsResponseModel != null) {
            if (faqsResponseModel.getErrorCode() != null)
                onTermsAndCondtionsError();
            else
                onTermsAndCondtionsSucess();

        }

    }

    private void onTermsAndCondtionsSucess() {

        if (sharedPrefrencesManger.getLanguage().equals(ConstantUtils.lang_english)) {
            getTermsAndConditions(faqsResponseModel.getSrcUrlEn());
        } else {
            getTermsAndConditions(faqsResponseModel.getSrcUrlAr());
        }

    }

    private void onTermsAndCondtionsError() {
      onBackPressed();
        if (sharedPrefrencesManger.getLanguage().equals(ConstantUtils.lang_english)) {

            Toast.makeText(TermsAndConditionsWebView.this, faqsResponseModel.getErrorMsgEn(), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(TermsAndConditionsWebView.this, faqsResponseModel.getErrorMsgAr(), Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
//     onBackPressed();

//        progressID.setVisibility(View.GONE);
    }


    private void getTermsAndConditions(String termsLinks) {
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
//                progressID.setVisibility(View.GONE);
              //  aboutActivity.onBackPressed();


            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
               onBackPressed();
            }
        });
        webView.loadUrl(termsLinks);
    }


}
