package com.indy.views.fragments.gamification.models.serviceInputOutput;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

import java.util.List;

/**
 * Created by mobile on 27/09/2017.
 */

public class GetCheckinLocationsOutputModel extends MasterErrorResponse {
    @SerializedName("checkinLocations")
    @Expose
    private List<CheckinLocation> locations = null;

    public List<CheckinLocation> getLocations() {
        return locations;
    }

    public void setLocations(List<CheckinLocation> locations) {
        this.locations = locations;
    }
}
