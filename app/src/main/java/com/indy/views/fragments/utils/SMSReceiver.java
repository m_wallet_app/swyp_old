//package com.indy.views.fragments.utils;
//
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.os.Bundle;
//import android.telephony.SmsMessage;
//import android.util.Log;
//import android.widget.Toast;
//
//import com.google.common.base.CharMatcher;
//
///**
// * Created by Amir.jehangir on 10/24/2016.
// */
//public class SMSReceiver extends BroadcastReceiver {
//    @Override
//    public void onReceive(Context context, Intent intent) {
//
//        Bundle extras = intent.getExtras();
//        if (extras == null)
//            return;
//
//
//        Object[] pdus = (Object[]) extras.get("pdus");
//        for (int i = 0; i <pdus.length; i++) {
//            SmsMessage SMessage = SmsMessage.createFromPdu((byte[]) pdus[i]);
//            String sender_ = SMessage.getOriginatingAddress();
//            String body_ = SMessage.getMessageBody().toString();
//
//            try {
//
//                if (extras != null) {
//                    String msg = sender_+":"+body_;
//                    msg = msg.replace("\n", "");
//                    String body = msg.substring(msg.lastIndexOf(":") + 1, msg.length());
//                    String pNumber = msg.substring(0, msg.lastIndexOf(":"));
//
//                    String phoneNumber = pNumber;
//                    String senderNum = phoneNumber;
//                    String message = body;
//
//
//                    if (senderNum.equals("etisalatINF") && message.contains("OTP")) {
//                        //  Toast.makeText(context, "Your Code is : " + output, Toast.LENGTH_SHORT).show();
//                        String output = CharMatcher.is('-').or(CharMatcher.DIGIT).retainFrom(message);
//                        //  Log.i("SmsReceiver", "senderNum: " + senderNum + "; message: " + message + "; MyCode is : " + output);
//                        if (output != null) {
//                            String[] OTPCODE = output.split("");
//
//                            // A custom Intent that will used as another Broadcast
//                            Intent in = new Intent("SmsMessage.intent.MAIN").
//                                    putExtra("get_msg", OTPCODE);
//
//                            context.sendBroadcast(in);
//                        } else {
//                            Toast.makeText(context, "Enter Number Manualy", Toast.LENGTH_SHORT).show();
//                        }
//
//                    } else {
//                        Toast.makeText(context, "Enter Number Manualy", Toast.LENGTH_SHORT).show();
//                        //  progressBar.setVisibility(View.GONE);
//                    }
//                    // Show Alert
//                    int duration = Toast.LENGTH_SHORT;
//                    Toast toast = Toast.makeText(context,
//                            "senderNum: " + senderNum + ", message: " + message, duration);
//                    //   toast.show();
//
//                }
//
//            } catch (Exception e) {
//                Log.e("SmsReceiver", "Exception smsReceiver" + e);
//
//            }
//
//
//        }
//    }
//}
