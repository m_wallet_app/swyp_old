package com.indy.views.fragments.gamification.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.BaseServiceManger;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.gamification.models.getrafflesdetails.GetRafflesDetailsInput;
import com.indy.views.fragments.gamification.models.getrafflesdetails.GetRafflesDetailsOutputResponse;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**

 */
public class GetRaffleDetailsService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private GetRafflesDetailsInput getRafflesDetailsInput;


    public GetRaffleDetailsService(BaseInterface baseInterface, GetRafflesDetailsInput getRafflesDetailsInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.getRafflesDetailsInput = getRafflesDetailsInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<GetRafflesDetailsOutputResponse> call = apiService.getRaffleDetails(getRafflesDetailsInput);
        call.enqueue(new Callback<GetRafflesDetailsOutputResponse>() {
            @Override
            public void onResponse(Call<GetRafflesDetailsOutputResponse> call, Response<GetRafflesDetailsOutputResponse> response) {
                mBaseResponseModel.setServiceType(ServiceUtils.GET_RAFFLE_DETAILS);
                mBaseResponseModel.setResultObj(response.body());

                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (response.body().getErrorMsgEn() != null) {
//                        String urlForTag = call.request().url().toString();
//                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
//                        urlForTag = urlForTag.replace("/", "_");
//                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<GetRafflesDetailsOutputResponse> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.GET_RAFFLE_DETAILS);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
