package com.indy.views.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.indy.R;
import com.indy.new_membership.NewMembershipSuccessListener;
import com.indy.views.fragments.utils.MasterFragment;

public class NewMembershipSuccessfulFragment extends MasterFragment {


    private View baseView;
    private Button okBtn;
    private NewMembershipSuccessListener newMembershipSuccessListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        baseView = inflater.inflate(R.layout.fragment_new_membership_success, null, false);
        return baseView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initUI();
    }

    public void setNewMembershipSuccessListener(NewMembershipSuccessListener newMembershipSuccessListener) {
        this.newMembershipSuccessListener = newMembershipSuccessListener;
    }

    @Override
    public void initUI() {
        super.initUI();

        okBtn = baseView.findViewById(R.id.okBtn);
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
                if (newMembershipSuccessListener != null)
                    newMembershipSuccessListener.onOKPressed();
            }
        });
    }
}
