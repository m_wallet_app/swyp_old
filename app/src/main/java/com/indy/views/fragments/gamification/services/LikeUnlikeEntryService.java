package com.indy.views.fragments.gamification.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.BaseServiceManger;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.gamification.models.serviceInputOutput.LikeUnlikeEntryInputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.LikeUnlikeEntryOutputModel;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**

 */
public class LikeUnlikeEntryService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private LikeUnlikeEntryInputModel likeUnlikeEntryInputModel;


    public LikeUnlikeEntryService(BaseInterface baseInterface, LikeUnlikeEntryInputModel likeUnlikeEntryInputModel) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.likeUnlikeEntryInputModel = likeUnlikeEntryInputModel;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<LikeUnlikeEntryOutputModel> call = apiService.likeUnlikeEntry(likeUnlikeEntryInputModel);
        call.enqueue(new Callback<LikeUnlikeEntryOutputModel>() {
            @Override
            public void onResponse(Call<LikeUnlikeEntryOutputModel> call, Response<LikeUnlikeEntryOutputModel> response) {
                mBaseResponseModel.setServiceType(ServiceUtils.likeUnlikeEntryService);
                mBaseResponseModel.setResultObj(response.body());

                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (response.body().getErrorMsgEn() != null) {
//                        String urlForTag = call.request().url().toString();
//                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
//                        urlForTag = urlForTag.replace("/", "_");
//                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<LikeUnlikeEntryOutputModel> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.likeUnlikeEntryService);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
