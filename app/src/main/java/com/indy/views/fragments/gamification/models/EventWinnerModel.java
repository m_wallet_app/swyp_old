package com.indy.views.fragments.gamification.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mobile on 10/09/2017.
 */

public class EventWinnerModel implements Parcelable{




    @SerializedName("indyEventId")
    @Expose
    private String indyEventId;


    @SerializedName("title")
    @Expose
    private String title;


    @SerializedName("contestantName")
    @Expose
    private String contestantName;
    @SerializedName("liked")
    @Expose
    private Boolean liked;


    /////////////////
    @SerializedName("imageUrl")
    @Expose
    private String imageURL;

    @SerializedName("entryId")
    @Expose
    private String id;
    @SerializedName("votes")
    @Expose
    private int votes;
    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("rank")
    @Expose
    private int rank;
    public static final Creator<EventWinnerModel> CREATOR = new Creator<EventWinnerModel>() {
        @Override
        public EventWinnerModel createFromParcel(Parcel in) {
            return new EventWinnerModel(in);
        }

        @Override
        public EventWinnerModel[] newArray(int size) {
            return new EventWinnerModel[size];
        }
    };

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getVotes() {
        return votes;
    }

    public void setVotes(int votes) {
        this.votes = votes;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(imageURL);
        parcel.writeInt(votes);
        parcel.writeString(contestantName);
        parcel.writeString(description);
        parcel.writeInt(rank);
        parcel.writeString(title);
    }

    public EventWinnerModel(){

    }

    private EventWinnerModel(Parcel in){
        this.id = in.readString();
        this.imageURL = in.readString();
        this.votes = in.readInt();
        this.contestantName = in.readString();
        this.description = in.readString();
        this.rank = in.readInt();
        this.title = in.readString();
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public String getIndyEventId() {
        return indyEventId;
    }

    public void setIndyEventId(String indyEventId) {
        this.indyEventId = indyEventId;
    }

    public String getContestantName() {
        return contestantName;
    }

    public void setContestantName(String contestantName) {
        this.contestantName = contestantName;
    }

    public Boolean getLiked() {
        return liked;
    }

    public void setLiked(Boolean liked) {
        this.liked = liked;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
