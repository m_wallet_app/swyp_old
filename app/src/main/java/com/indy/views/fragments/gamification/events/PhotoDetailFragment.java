package com.indy.views.fragments.gamification.events;


import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.GetRewardForSharingService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.ShareMasterActivity;
import com.indy.views.fragments.gamification.models.EventPhoto;
import com.indy.views.fragments.gamification.models.EventsList;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetEntryDetailsInputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetEntryDetailsOutputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetRewardForSharingInputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetRewardForSharingOutputResponse;
import com.indy.views.fragments.gamification.models.serviceInputOutput.LikeUnlikeEntryInputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.LikeUnlikeEntryOutputModel;
import com.indy.views.fragments.gamification.models.socialshare.SocialShareInput;
import com.indy.views.fragments.gamification.models.socialshare.SocialShareOutput;
import com.indy.views.fragments.gamification.services.GetEntryDetailsService;
import com.indy.views.fragments.gamification.services.LikeUnlikeEntryService;
import com.indy.views.fragments.gamification.services.SocialShareService;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhotoDetailFragment extends ShareMasterActivity {

    private RelativeLayout rl_help, rl_back;
    private TextView tv_title, tv_votes, tv_photoDescription, tv_spreadThWord, tv_headerText;
    private ImageView iv_photo;
    private EventPhoto detailPhotoObject;
    private Button shareButton;
    private LinearLayout shareLayout;
    private CoordinatorLayout coordinatorLayout;
    private BottomSheetBehavior behavior;
    private View bottomSheet;
    private GetEntryDetailsOutputModel getEntryDetailsOutputModel;
    private TextView fbShare;
    private TextView twitterShare;
    private String uploadedEntryId;
    private EventsList eventObject;
    private SocialShareOutput socialShareOutput;
    private ProgressBar progressBar;
    private EventPhoto eventPhoto;
    private CheckedTextView iv_voteUnvote;
    private boolean isOwner = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_event_photo_detail);
        uploadedEntryId = getIntent().getStringExtra(ConstantUtils.UPLOADED_ENTRY_ID);
        eventObject = (EventsList) getIntent().getParcelableExtra(ConstantUtils.EVENT_KEY);
        eventPhoto = (EventPhoto) getIntent().getParcelableExtra(ConstantUtils.EVENT_PHOTO);
        isOwner = (boolean) getIntent().getBooleanExtra(ConstantUtils.IS_OWNER,false);
        initUI();
        onConsumeService();
    }

    @Override
    public void initUI() {
        super.initUI();
        progressBar = (ProgressBar) findViewById(R.id.progressID);
        tv_headerText = (TextView) findViewById(R.id.titleTxt);
        tv_headerText.setText(getString(R.string.photo));
        tv_title = (TextView) findViewById(R.id.tv_photo_username);
        tv_photoDescription = (TextView) findViewById(R.id.tv_photo_description);
        tv_votes = (TextView) findViewById(R.id.tv_vote_counter);
        iv_photo = (ImageView) findViewById(R.id.iv_photo_details);
        rl_back = (RelativeLayout) findViewById(R.id.backLayout);
        rl_help = (RelativeLayout) findViewById(R.id.helpLayout);
        tv_spreadThWord = (TextView) findViewById(R.id.tv_spread_the_word);
        tv_spreadThWord.setVisibility(View.GONE);
        rl_help.setVisibility(View.INVISIBLE);
        rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        shareLayout = (LinearLayout) findViewById(R.id.share_layout);
        shareButton = (Button) findViewById(R.id.okBtn);
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onConsumeGetRewardService();


            }
        });
        iv_voteUnvote = (CheckedTextView) findViewById(R.id.iv_voteUnvote);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinateLayoutID);
        bottomSheet = coordinatorLayout.findViewById(R.id.share_photo_bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {

                } else if (newState == BottomSheetBehavior.STATE_EXPANDED) {

                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });
        fbShare = (TextView) findViewById(R.id.fb_share);
        twitterShare = (TextView) findViewById(R.id.twitter_share);
        fbShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                referenceID = getEntryDetailsOutputModel.getEntryDetails().getIndyEventId();
                facebookShare(uploadedEntryId,"");
            }
        });
        twitterShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                referenceID = getEntryDetailsOutputModel.getEntryDetails().getIndyEventId();

                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                twitterLogin(true,uploadedEntryId,"");
            }
        });

        iv_voteUnvote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(CommonMethods.isMembershipValid(PhotoDetailFragment.this)){
                if (getEntryDetailsOutputModel.getEntryDetails().isSelected()) {
                    onCallLikeUnlikeService(getEntryDetailsOutputModel.getEntryDetails().getEntryId(), false);
                }else{
                    onCallLikeUnlikeService(getEntryDetailsOutputModel.getEntryDetails().getEntryId(), true);

                }
                }
            }
        });
        iv_voteUnvote.setEnabled(false);
    }
    private void onGetShareRewardSuccess(String rewardString){
//        int rewardAmount = 10;
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        if(rewardString.length()>0) {
            ((TextView) findViewById(R.id.tv_reward_text)).setText(getString(R.string.share_your_photo_entry) + " " +
                    getString(R.string.and_get_rewarded_with) +rewardString);
        }else{
            ((TextView) findViewById(R.id.tv_reward_text)).setText(getString(R.string.share_your_photo_entry));
        }
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        progressBar.setVisibility(View.VISIBLE);
        GetEntryDetailsInputModel getEntryDetailsInputModel = new GetEntryDetailsInputModel();
        getEntryDetailsInputModel.setChannel(channel);
        getEntryDetailsInputModel.setLang(currentLanguage);
        getEntryDetailsInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        getEntryDetailsInputModel.setAppVersion(appVersion);
        getEntryDetailsInputModel.setAuthToken(authToken);
        getEntryDetailsInputModel.setDeviceId(deviceId);
        getEntryDetailsInputModel.setToken(token);
        getEntryDetailsInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
        getEntryDetailsInputModel.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        getEntryDetailsInputModel.setOsVersion(osVersion);
        getEntryDetailsInputModel.setIndyEventId(eventObject != null ? eventObject.getIndyEventId() : eventPhoto.getIndyEventId());
        getEntryDetailsInputModel.setUserSessionId(sharedPrefrencesManger.getUserSessionId());
        getEntryDetailsInputModel.setUserId(sharedPrefrencesManger.getUserId());
        getEntryDetailsInputModel.setIndyEventEntryId(uploadedEntryId != null ? uploadedEntryId : eventPhoto.getEntryId());
        getEntryDetailsInputModel.setApplicationId(ConstantUtils.INDY_TALOS_ID);
        new GetEntryDetailsService(this, getEntryDetailsInputModel);
    }


    public void onConsumeGetRewardService(){
        progressBar.setVisibility(View.VISIBLE);
        GetRewardForSharingInputModel getBadgeDetailsInputModel = new GetRewardForSharingInputModel();
        getBadgeDetailsInputModel.setChannel(channel);
        getBadgeDetailsInputModel.setLang(currentLanguage);
        getBadgeDetailsInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        getBadgeDetailsInputModel.setAppVersion(appVersion);
        getBadgeDetailsInputModel.setAuthToken(authToken);
        getBadgeDetailsInputModel.setDeviceId(deviceId);
        getBadgeDetailsInputModel.setToken(token);
        getBadgeDetailsInputModel.setUserSessionId(sharedPrefrencesManger.getUserSessionId());
        getBadgeDetailsInputModel.setUserId(sharedPrefrencesManger.getUserId());
        getBadgeDetailsInputModel.setApplicationId(ConstantUtils.INDY_TALOS_ID);
        getBadgeDetailsInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
        getBadgeDetailsInputModel.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        getBadgeDetailsInputModel.setOsVersion(osVersion);
        getBadgeDetailsInputModel.setActionName(ConstantUtils.social_share);
        new GetRewardForSharingService(this, getBadgeDetailsInputModel);
    }

    private void onSocialShare() {
        progressBar.setVisibility(View.VISIBLE);
        super.onConsumeService();
        SocialShareInput socialShareInput = new SocialShareInput();
        socialShareInput.setChannel(channel);
        socialShareInput.setLang(currentLanguage);
        socialShareInput.setMsisdn(sharedPrefrencesManger.getMobileNo());
        socialShareInput.setAppVersion(appVersion);
        socialShareInput.setAuthToken(authToken);
        socialShareInput.setDeviceId(deviceId);
        socialShareInput.setToken(token);
        socialShareInput.setImsi(sharedPrefrencesManger.getMobileNo());
        socialShareInput.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        socialShareInput.setOsVersion(osVersion);
        socialShareInput.setUserSessionId(sharedPrefrencesManger.getUserSessionId());
        socialShareInput.setUserId(sharedPrefrencesManger.getUserId());
        socialShareInput.setApplicationId(ConstantUtils.INDY_TALOS_ID);
        socialShareInput.setMediaChannel(faceBookLogin ? ConstantUtils.FACEBOOK : ConstantUtils.TWITTER);
        socialShareInput.setItemId(detailPhotoObject.getEntryId());
        socialShareInput.setItemType(ConstantUtils.IMAGE_TYPE);
        socialShareInput.setReferenceId(referenceID);
        new SocialShareService(this, socialShareInput);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            progressBar.setVisibility(View.GONE);
            if (responseModel != null)
                switch (responseModel.getServiceType()) {
                    case ServiceUtils.getEntryDetailsService: {
                        getEntryDetailsOutputModel = (GetEntryDetailsOutputModel) responseModel.getResultObj();
                        detailPhotoObject = getEntryDetailsOutputModel.getEntryDetails();
                        if (detailPhotoObject != null) {
                            onGetEntryDetailsSuccess();
                        }
                        break;
                    }
                    case ServiceUtils.SOCIAL_SHARE: {
                        socialShareOutput = (SocialShareOutput) responseModel.getResultObj();
                        if (socialShareOutput != null && socialShareOutput.isSuccess()) {
                            onSocialShareSuccess();
                        } else {
                            onSocialShareFail();
                        }
                        break;
                    }

                    case ServiceUtils.likeUnlikeEntryService:
                        iv_voteUnvote.setEnabled(true);
                        getLikeAndUnLikeEntryService(responseModel);
                        break;

                    case ServiceUtils.getRewardForSharingService:
                        GetRewardForSharingOutputResponse getRewardForSharingOutputResponse = (GetRewardForSharingOutputResponse) responseModel.getResultObj();
                        if(getRewardForSharingOutputResponse!=null && getRewardForSharingOutputResponse.getRewardAmount()!=null){
                            onGetShareRewardSuccess(getRewardForSharingOutputResponse.getRewardAmount());
                        }else{
                            onGetShareRewardSuccess("");
                        }
                        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                        break;
                }
        } catch (Exception ex) {

        }
    }
    LikeUnlikeEntryOutputModel likeUnlikeEntryOutputModel;
    boolean selectedValueParam = false;
    public void getLikeAndUnLikeEntryService(BaseResponseModel responseModel) {
        likeUnlikeEntryOutputModel = (LikeUnlikeEntryOutputModel) responseModel.getResultObj();
        if (likeUnlikeEntryOutputModel != null) {
            if (likeUnlikeEntryOutputModel.isSuccess()) {

                tv_votes.setText(likeUnlikeEntryOutputModel.getEntryDetails().getVotes() + " " + getString(R.string.votes));
                iv_voteUnvote.setChecked(likeUnlikeEntryOutputModel.getEntryDetails().isSelected());
                iv_voteUnvote.setSelected(likeUnlikeEntryOutputModel.getEntryDetails().isSelected());
                getEntryDetailsOutputModel.getEntryDetails().setSelected(likeUnlikeEntryOutputModel.getEntryDetails().isSelected());
            }
        }
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        try {
            iv_voteUnvote.setEnabled(true);
            progressBar.setVisibility(View.GONE);
            if(responseModel.getServiceType()!=null && responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.getRewardForSharingService)){
                onGetShareRewardSuccess("");
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    private void onSocialShareSuccess() {
        Intent intent = new Intent(PhotoDetailFragment.this, SuccessfulShareActivity.class);
        startActivity(intent);
    }

    private void onSocialShareFail() {

    }

    private void onGetEntryDetailsSuccess() {
        if (!isOwner) {
            tv_title.setText(detailPhotoObject.getContestantName());
            tv_spreadThWord.setVisibility(View.GONE);
            shareLayout.setVisibility(View.GONE);
            iv_voteUnvote.setEnabled(true);
        } else {
            iv_voteUnvote.setEnabled(false);

            tv_title.setText(getString(R.string.you));
            tv_spreadThWord.setVisibility(View.VISIBLE);
            shareLayout.setVisibility(View.VISIBLE);
            iv_voteUnvote.setEnabled(false);
        }
        tv_votes.setText(detailPhotoObject.getVotes() + " " + getString(R.string.votes));
        if (detailPhotoObject.getTitle() != null)
            tv_photoDescription.setText(detailPhotoObject.getTitle());
        if (detailPhotoObject.getImageURL() != null && !detailPhotoObject.getImageURL().isEmpty())
            ImageLoader.getInstance().loadImage(detailPhotoObject.getImageURL(), new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    bitmap = loadedImage;
                    iv_photo.setImageBitmap(loadedImage);
                }
            });

        if(detailPhotoObject.isSelected()){
            iv_voteUnvote.setChecked(true);
        }else{
            iv_voteUnvote.setChecked(false);
        }
    }

    @Override
    public void shareSuccess() {
        onSocialShare();
    }

    @Override
    public void shareFail() {

    }

    public void onCallLikeUnlikeService(String entryId, boolean like) {
        progressBar.setVisibility(View.VISIBLE);
        iv_voteUnvote.setEnabled(false);
        LikeUnlikeEntryInputModel likeUnlikeEntryInputModel = new LikeUnlikeEntryInputModel();
        likeUnlikeEntryInputModel.setChannel(channel);
        likeUnlikeEntryInputModel.setLang(currentLanguage);
        likeUnlikeEntryInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        likeUnlikeEntryInputModel.setAppVersion(appVersion);
        likeUnlikeEntryInputModel.setAuthToken(authToken);
        likeUnlikeEntryInputModel.setDeviceId(deviceId);
        likeUnlikeEntryInputModel.setToken(token);
        likeUnlikeEntryInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
        likeUnlikeEntryInputModel.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        likeUnlikeEntryInputModel.setOsVersion(osVersion);
        likeUnlikeEntryInputModel.setApplicationId(ConstantUtils.INDY_TALOS_ID);
        likeUnlikeEntryInputModel.setUserId(sharedPrefrencesManger.getUserId());
        likeUnlikeEntryInputModel.setUserSessionId(sharedPrefrencesManger.getUserSessionId());
        likeUnlikeEntryInputModel.setIndyEventId(eventObject != null ? eventObject.getIndyEventId() : eventPhoto.getIndyEventId());
        likeUnlikeEntryInputModel.setIndyEventEntryId(uploadedEntryId != null ? uploadedEntryId : eventPhoto.getEntryId());
        likeUnlikeEntryInputModel.setLike(like);
        new LikeUnlikeEntryService(this, likeUnlikeEntryInputModel);
    }

}
