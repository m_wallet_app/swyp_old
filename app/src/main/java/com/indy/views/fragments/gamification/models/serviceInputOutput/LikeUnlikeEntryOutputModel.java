package com.indy.views.fragments.gamification.models.serviceInputOutput;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.views.fragments.gamification.models.EventPhoto;

/**
 * Created by mobile on 24/09/2017.
 */

public class LikeUnlikeEntryOutputModel extends MasterErrorResponse {

    @SerializedName("entryDetails")
    @Expose
    EventPhoto entryDetails;

    @SerializedName("action")
    @Expose
    String action;

    @SerializedName("success")
    @Expose
    boolean success;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public EventPhoto getEntryDetails() {
        return entryDetails;
    }

    public void setEntryDetails(EventPhoto entryDetails) {
        this.entryDetails = entryDetails;
    }
}
