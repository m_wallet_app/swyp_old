package com.indy.views.fragments.usage;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indy.R;
import com.indy.views.fragments.utils.MasterFragment;

/**
 * Created by Amir.jehangir on 10/10/2016.
 */
public class ChangePasswordFragment extends MasterFragment {
    private View view;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_changepassword, container, false);
        initUI();
        onConsumeService();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();

    }



}
