package com.indy.views.fragments.rewards.ItemsCarousel;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.indy.R;
import com.indy.models.rewards.RewardsList;
import com.indy.models.rewards.StoreLocation;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.utils.SharedPrefrencesManger;
import com.indy.views.activites.RewardDetailsActivity;
import com.indy.views.fragments.utils.MasterFragment;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;

/**
 * Created by Amir.jehangir on 10/30/2016.
 */
public class CommonFragment extends MasterFragment {
    private ImageView deal_image;
    TextView DealName, page_counter, deal_price_Discount, deal_expire;
    private View address1, address2, address3, address4, address5;
    private RatingBar ratingBar;
    private View head1, head2, head3, head4;
    private String imageUrl;
    private String tv_indicates;
    int position, total;
    TextView tv_addressc;
    RewardsList mRewardsListItem;
    CardView CardView;
    ArrayList<StoreLocation> mStoreLocationsList;
    String icon_url;
    View rootView;
    private SharedPrefrencesManger sharedPrefrencesManger;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .resetViewBeforeLoading(false) // default
                .delayBeforeLoading(1000)
                .cacheInMemory(false) // default
                .cacheOnDisk(true) // default
                .build();

        try {
            super.onCreateView(inflater, container, savedInstanceState);
            rootView = inflater.inflate(R.layout.fragment_common, container, false);
//        DragLayout dragLayout = (DragLayout) rootView.findViewById(R.id.drag_layout);

            if (getArguments() != null && getArguments().getParcelable(ConstantUtils.item_reward_view_pager) != null) {
                mRewardsListItem = getArguments().getParcelable(ConstantUtils.item_reward_view_pager);
            }
            if (getArguments() != null && getArguments().getInt("position") != -1) {
                position = getArguments().getInt("position");
            }
            if (getArguments() != null && getArguments().getInt("total") != -1) {
                total = getArguments().getInt("total");
            }

            if (getArguments() != null && getArguments().getString("icon_url") != null) {
                icon_url = getArguments().getString("icon_url");
            }
            deal_image = (ImageView) rootView.findViewById(R.id.deal_image);
            sharedPrefrencesManger = new SharedPrefrencesManger(getActivity().getApplicationContext());
            //  tv_addressc = (TextView) rootView.findViewById(R.id.page_counter);

            DealName = (TextView) rootView.findViewById(R.id.DealName);
            page_counter = (TextView) rootView.findViewById(R.id.page_counter);
            deal_price_Discount = (TextView) rootView.findViewById(R.id.deal_price_Discount);
            deal_expire = (TextView) rootView.findViewById(R.id.deal_expire);
            CardView = (CardView) rootView.findViewById(R.id.card_view);
            CardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_reward_detail_opened);
//                    CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_reward_detail_opened+"_"+mRewardsListItem.getCategoryId());
                    CommonMethods.logViewedContentEvent(mRewardsListItem.getOfferID(), mRewardsListItem.getAverageSaving());

                    Intent intent = new Intent(getActivity(), RewardDetailsActivity.class);
                    intent.putExtra(ConstantUtils.RewardsDetails, mRewardsListItem);
                    intent.putExtra("total", total);
                    intent.putExtra("position", position);
                    intent.putParcelableArrayListExtra("storeLocation", mStoreLocationsList);
                    intent.putExtra("icon_url", icon_url);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivityForResult(intent, 100);
                }
            });

            mStoreLocationsList = new ArrayList<StoreLocation>();
            if (getArguments() != null && getArguments().getParcelableArrayList("storeLocation") != null) {
                mStoreLocationsList = getArguments().getParcelableArrayList("storeLocation");
            }
            ImageLoader.getInstance().displayImage(mRewardsListItem.getImageUrl(), deal_image, options);
            if (sharedPrefrencesManger.getLanguage().equalsIgnoreCase("en")) {
                DealName.setText(mRewardsListItem.getNameEn());
                deal_price_Discount.setText(getString(R.string.aed) + " " + mRewardsListItem.getAverageSaving() + " " + getString(R.string.discount));
//                deal_expire.setText(getString(R.string.expires) + " " + MainAvailableRewardsFragment.expiryDateEn);// + " " + CommonMethods.parseDate(mRewardsListItem.getExpiryDate()));
            } else {
                DealName.setText(mRewardsListItem.getNameAr());
                deal_price_Discount.setText(getString(R.string.discount) + " " + mRewardsListItem.getAverageSaving() + " " + getString(R.string.aed));
//                deal_expire.setText(MainAvailableRewardsFragment.expiryDateAr + " " + getString(R.string.expires));// + " " + CommonMethods.parseDate(mRewardsListItem.getExpiryDate()));

            }
            page_counter.setText(position + "/" + total);


        }catch (Exception ex){

        }
        return rootView;
    }


    //    public void bindData(String imageUrl,String deal_name,String deal_price_discount,String deal_expire ,String tv_indicator) {
//        this.imageUrl = imageUrl;
//        this.addressc = tv_indicator;
//    }
}
