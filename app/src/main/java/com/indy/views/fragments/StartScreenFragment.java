package com.indy.views.fragments;


import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.adapters.StaticPackagesListAdapter;
import com.indy.controls.AdjustEvents;
import com.indy.controls.ServiceUtils;
import com.indy.models.getStaticBundles.BundleList;
import com.indy.models.getStaticBundles.StaticBundleOutput;
import com.indy.models.onboarding_push_notifications.LandingPageRequestModel;
import com.indy.models.onboarding_push_notifications.LandingPageResponseModel;
import com.indy.models.packages.UsagePackageList;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterInputResponse;
import com.indy.services.GetStaticPackageListService;
import com.indy.services.LogLandingPageService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.HelpActivity;
import com.indy.views.activites.RegisterationActivity;
import com.indy.views.activites.SplashActivity;
import com.indy.views.fragments.buynewline.NewNumberFragment;
import com.indy.views.fragments.buynewline.PaymentSucessfulllyFragment;
import com.indy.views.fragments.buynewline.Registeration2Fragment;
import com.indy.views.fragments.login.LoginFragment;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.indy.views.fragments.utils.MasterFragment;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class StartScreenFragment extends MasterFragment {

    public View view;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mGridLayoutManger;
    private List<BundleList> usagePackageList;
    private MasterInputResponse masterInputResponse;
    private StaticBundleOutput usagePackagesoutput;
    private StaticPackagesListAdapter packagesListAdapter;
    private UsagePackageList emptyUsagePackageListlItem;
    private RelativeLayout rl_help, rl_back;
    private Button bt_getSim, langBtn, helpBtn;
    private TextView tv_loginBtn;

    public StartScreenFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_start_screen, container, false);
            initUI();
            PaymentSucessfulllyFragment.paymentSucessfulllyFragmentInstance = null;
            setListeners();
            onConsumeService();
            CommonMethods.logFirebaseEvent(mFirebaseAnalytics, ConstantUtils.TAGGING_what_is_swyp);
//            loadingAnimation();
//            startService();
        } else {
            regActivity.hideHeaderLayout();
            regActivity.setHeaderTitle("");
        }
        try {
            PaymentSucessfulllyFragment.paymentSucessfulllyFragmentInstance = null;
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        return view;
    }

    @Override
    public void initUI() {
        super.initUI();

        mRecyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        rl_back = (RelativeLayout) view.findViewById(R.id.backLayout);
        langBtn = (Button) view.findViewById(R.id.langBtn);
        helpBtn = (Button) view.findViewById(R.id.helpBtn);
        bt_getSim = (Button) view.findViewById(R.id.bt_get_sim);
        tv_loginBtn = (TextView) view.findViewById(R.id.tv_login);

        if (getArguments() != null && !getArguments().getBoolean(ConstantUtils.showBackBtn, true)) {
            rl_back.setVisibility(View.GONE);
        } else {
            rl_back.setVisibility(View.VISIBLE);
        }

        mRecyclerView.setHasFixedSize(true);
        regActivity.hideHeaderLayout();
        regActivity.setHeaderTitle("");
        mGridLayoutManger = new LinearLayoutManager(getContext());
        mGridLayoutManger.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mGridLayoutManger);
        mRecyclerView.setHasFixedSize(true);

        if (sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)) {
            ((TextView) view.findViewById(R.id.tv_unlimited_wifi_english)).setVisibility(View.VISIBLE);
            ((LinearLayout) view.findViewById(R.id.ll_wifi_unlimited_arabic)).setVisibility(View.GONE);

            ((TextView) view.findViewById(R.id.tv_unlimited_wifi_description_arabic)).setVisibility(View.VISIBLE);
            ((TextView) view.findViewById(R.id.tv_unlimited_wifi_description_arabic)).setText(getString(R.string.unlimited_wifi_description));

            ((TextView) view.findViewById(R.id.tv_unlimited_wifi_description_english)).setVisibility(View.GONE);
        } else {
            ((TextView) view.findViewById(R.id.tv_unlimited_wifi_english)).setVisibility(View.GONE);
            ((LinearLayout) view.findViewById(R.id.ll_wifi_unlimited_arabic)).setVisibility(View.VISIBLE);


            ((TextView) view.findViewById(R.id.tv_unlimited_wifi_description_arabic)).setVisibility(View.VISIBLE);
            ((TextView) view.findViewById(R.id.tv_unlimited_wifi_description_arabic)).setText(getString(R.string.unlimited_wifi_description));
            ((TextView) view.findViewById(R.id.tv_unlimited_wifi_description_english)).setVisibility(View.VISIBLE);
        }


        rl_back.setVisibility(View.GONE);
        helpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), HelpActivity.class));
//                stopService();

            }
        });
        if (sharedPrefrencesManger.getLanguage().equals(ConstantUtils.lang_english)) {
            langBtn.setText(getString(R.string.arabic));
        } else {
            langBtn.setText(getString(R.string.english));
        }
        bt_getSim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logEvent(AdjustEvents.CLICKS_GET_SIM, new Bundle());
                regActivity.replaceFragmnet(new NewNumberFragment(), R.id.frameLayout, true);
                // regActivity.replaceFragmnet(new SwypeToReachUsFragmnet(), R.id.frameLayout, true);
                // startActivity(new Intent(getActivity(), DataPackageChartActivity.class));
//                startActivity(new Intent(getActivity(), SwypeToReachUsFragmnet.class));
                //TODO

                CommonMethods.logFirebaseEvent(mFirebaseAnalytics, ConstantUtils.TAGGING_get_sim);

            }
        });
        tv_loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment loginFrag = new LoginFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean(ConstantUtils.showBackBtn, true);
                loginFrag.setArguments(bundle);
                regActivity.replaceFragmnet(loginFrag, R.id.frameLayout, true);

                CommonMethods.logFirebaseEvent(mFirebaseAnalytics, ConstantUtils.TAGGING_login_button);


            }
        });
        if (sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)) {
            Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/aktive_grotesk_rg.ttf");
            langBtn.setTypeface(tf);
        } else {
            Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/aller_rg.ttf");
            langBtn.setTypeface(tf);
        }

        langBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CommonMethods.logFirebaseEvent(mFirebaseAnalytics, ConstantUtils.TAGGING_change_language);


                if (langBtn.getText().toString().equals(getString(R.string.arabic))) {

//                    langBtn.setText(getString(R.string.english));
                    sharedPrefrencesManger.setLanguage(ConstantUtils.arLng);
//                    ((MasterActivity) getActivity()).switchLocalizaion(sharedPrefrencesManger.getLanguage(),(MasterActivity)getActivity());
//                    LocalizationUtils.switchLocalizaion(sharedPrefrencesManger.getLanguage(), (MasterActivity) getActivity());
                    regActivity.finish();
                    regActivity.startActivity(new Intent(getActivity(), RegisterationActivity.class));
                } else if (langBtn.getText().toString().equals(getString(R.string.english))) {
//                    langBtn.setText(getString(R.string.arabic));

                    sharedPrefrencesManger.setLanguage(ConstantUtils.enLng);
//                    ((MasterActivity) getActivity()).switchLocalizaion(sharedPrefrencesManger.getLanguage(),(MasterActivity)getActivity());
//
//                    LocalizationUtils.switchLocalizaion(sharedPrefrencesManger.getLanguage(), (MasterActivity) getActivity());
                    regActivity.finish();
                    regActivity.startActivity(new Intent(getActivity(), RegisterationActivity.class));
                }
            }
        });

        if (SplashActivity.isTutorialRunnung)
            rl_back.setVisibility(View.VISIBLE);
        else
            rl_back.setVisibility(View.GONE);

        regActivity.addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            if (responseModel != null && responseModel.getResultObj() != null) {
                if (responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.packageServiceType)) {
                    getActivity().onBackPressed();
                    getPackagesList(responseModel);
                }
                if(responseModel.getServiceType().equalsIgnoreCase(ServiceUtils.log_landing_page_service)){
                    LandingPageResponseModel landingPageResponseModel = (LandingPageResponseModel) responseModel.getResultObj();
                    if(landingPageResponseModel != null){
                        Log.d("registrationIDSet:", "" + landingPageResponseModel.getRegistrationId());
                        sharedPrefrencesManger.setRegisterationID(landingPageResponseModel.getRegistrationId());
                    }
                }
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }


    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        try {
            getActivity().onBackPressed();
            if (responseModel != null) {
                showErrorFragment(responseModel.toString());
            } else {
                showErrorFragment("Error from service.");
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    private void getPackagesList(BaseResponseModel responseModel) {
        usagePackagesoutput = (StaticBundleOutput) responseModel.getResultObj();
        if (usagePackagesoutput != null) {
            if (usagePackagesoutput.getErrorCode() != null)
                onGetPackagesError();
            else
                onGetPackageSucess();
        }
    }


    private void onGetPackageSucess() {
        usagePackageList = usagePackagesoutput.getBundleList();
        if (usagePackageList != null)
            if (usagePackageList.size() > 0) {
//                BundleList bundleList = new BundleList();
//                bundleList.setBundleName("dummy element");
//                bundleList.setNameEn("dummy element");
//                usagePackageList.add(bundleList);
                packagesListAdapter = new StaticPackagesListAdapter(usagePackageList, getContext());
                mRecyclerView.setAdapter(packagesListAdapter);
            }

    }

    public void setListeners() {
//        continueBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(StaticPackagesFragments.this, RegisterationActivity.class);
//                startActivity(intent);
//                finish();
//            }
//        });
    }

    private void onGetPackagesError() {
        showErrorFragment(usagePackagesoutput.getErrorMsgEn());
    }

    public void showErrorFragment(String error) {
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();

        bundle.putString(ConstantUtils.errorString, error);
        errorFragment.setArguments(bundle);
        regActivity.addFragmnet(errorFragment, R.id.frameLayout, true);
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        masterInputResponse = new MasterInputResponse();
        masterInputResponse.setAppVersion(appVersion);
        masterInputResponse.setToken(sharedPrefrencesManger.getToken());
        masterInputResponse.setOsVersion(osVersion);
        masterInputResponse.setChannel(channel);
        masterInputResponse.setDeviceId(deviceId);
        new GetStaticPackageListService(this, masterInputResponse);

        LandingPageRequestModel landingPageRequestModel = new LandingPageRequestModel();
        landingPageRequestModel.setAppVersion(appVersion);
        landingPageRequestModel.setToken(sharedPrefrencesManger.getToken());
        landingPageRequestModel.setOsVersion(osVersion);
        landingPageRequestModel.setChannel(channel);
        landingPageRequestModel.setDeviceId(deviceId);
        landingPageRequestModel.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        new LogLandingPageService(this, landingPageRequestModel);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ConstantUtils.SHOW_BUY_NOW_CODE) {
            if (resultCode == 1) {
                regActivity.addFragmnet(new Registeration2Fragment(), R.id.frameLayout, true);
            }
        }
    }
}
