package com.indy.views.fragments.utils;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.indy.R;
import com.indy.dbt.listeners.OnDataTransferConfirmedListener;
import com.indy.models.utils.SwypeErrorResponse;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.ConfirmStorePurchaseFragment;
import com.indy.views.fragments.findus.EndSessionConfirmationFragment;
import com.indy.views.fragments.gamification.challenges.CheckInLocationsActivity;

import static com.indy.views.fragments.buynewline.DeliveryDetailsAddress.LOCATION_REQUEST;

/**
 * Created by emad on 10/3/16.
 */

public class ErrorFragment extends MasterFragment {
    private TextView errorTxt, titleTxt;
    private View view;
    private Button okBtn, cancelBtn;
    SwypeErrorResponse masterErrorResponse;
    public static String KEY_DBT_LISTENER = "KEY_DBT_LISTENER";
    OnDataTransferConfirmedListener onDataTransferConfirmedListener = null;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().overridePendingTransition(R.anim.buttom_up, R.anim.buttom_down);
        view = inflater.inflate(R.layout.fragment_error, container, false);
        initUI();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
        try {
            if (getArguments() != null && getArguments().getSerializable(ConstantUtils.master_error_response) != null) {
                masterErrorResponse = (SwypeErrorResponse) getArguments().getSerializable(ConstantUtils.master_error_response);
            }
            errorTxt = (TextView) view.findViewById(R.id.errorTxt);
            titleTxt = (TextView) view.findViewById(R.id.titleTxtView);

//        if (masterErrorResponse != null && masterErrorResponse.getSwyperErrorString() != null) {
//            errorTxt.setText(masterErrorResponse.getSwyperErrorString());
//        } else if(masterErrorResponse!=null){
//
//            if (currentLanguage.equals("en")) {
//                errorTxt.setText(masterErrorResponse.getErrorMsgEn());
//            } else {
//                errorTxt.setText(masterErrorResponse.getErrorMsgAr());
//            }


            if (getArguments() != null && getArguments().getString(ConstantUtils.errorString) != null) {
                errorTxt.setText(getArguments().getString(ConstantUtils.errorString));
            } else {
                errorTxt.setText(getString(R.string.error_));
            }


            if (getArguments() != null && getArguments().getString(ConstantUtils.errorTitle) != null) {
                titleTxt.setText(getArguments().getString(ConstantUtils.errorTitle));
            } else {
//            errorTxt.setText("Error");
            }


//        }else{
//            errorTxt.setText("Error");
//        }
            okBtn = (Button) view.findViewById(R.id.okBtn);
            cancelBtn = (Button) view.findViewById(R.id.cancelBtn);

            if (getArguments() != null && getArguments().getString(ConstantUtils.NEGATIVE_BUTTON_TEXT) != null) {
                cancelBtn.setText(getArguments().getString(ConstantUtils.NEGATIVE_BUTTON_TEXT));
            }
            if (getArguments() != null && getArguments().getBoolean("cancel", false)) {
                cancelBtn.setVisibility(View.VISIBLE);
                cancelBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getActivity().onBackPressed();
                    }
                });
            } else if (getArguments() != null && getArguments().getBoolean(ConstantUtils.VIEW_MAP_VISIBLE, false)) {

                cancelBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        startActivity(new Intent(getActivity(), CheckInLocationsActivity.class));
                        getActivity().onBackPressed();
                    }
                });

            } else {
                cancelBtn.setVisibility(View.INVISIBLE);
            }
            if (regActivity != null) {
                regActivity.hideHeaderLayout();
            }
            if (getArguments() != null) {
                if (getArguments().getString(ConstantUtils.settingsButtonEnabled) != null
                        && getArguments().getString(ConstantUtils.settingsButtonEnabled).equals("true")) {
                    okBtn.setText(getArguments().getString(ConstantUtils.buttonTitle));
                    okBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivityForResult(intent, 100);
                        }
                    });
                }
                else if(getArguments().getBoolean(KEY_DBT_LISTENER)){
                    okBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(ErrorFragment.this.onDataTransferConfirmedListener != null){
                                ErrorFragment.this.onDataTransferConfirmedListener.onCancelled();
                            }
                        }
                    });
                }
                else if (getArguments().getBoolean("finish")) {
                    okBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getActivity().finish();
                        }
                    });
                } else if (getArguments().containsKey(ConstantUtils.buttonTitle)) {
                    okBtn.setText(getArguments().getString(ConstantUtils.buttonTitle));
                    okBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getActivity().onBackPressed();
                        }
                    });
                } else {
                    okBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (getActivity() instanceof EndSessionConfirmationFragment || getActivity() instanceof ConfirmStorePurchaseFragment) {
                                getActivity().finish();
                            } else {
                                getActivity().onBackPressed();

                            }
                        }
                    });
                }

            } else {

                okBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            getActivity().onBackPressed();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                });
            }
        } catch (Exception ex) {
            CommonMethods.logException(ex);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case LOCATION_REQUEST:


                break;
            default:
                break;
        }
    }

    public void setOnDataTransferConfirmedListener(OnDataTransferConfirmedListener onDataTransferConfirmedListener){
        this.onDataTransferConfirmedListener = onDataTransferConfirmedListener;
    }
}
