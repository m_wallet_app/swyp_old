package com.indy.views.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.indy.R;
import com.indy.adapters.BenefitsAdapter;
import com.indy.controls.ServiceUtils;
import com.indy.models.balance.BalanceInputModel;
import com.indy.models.balance.BalanceResponseModel;
import com.indy.models.benefits.NewBenefitsModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.models.utils.MasterInputResponse;
import com.indy.new_membership.NewMembershipSuccessListener;
import com.indy.services.GetUserBalanceService;
import com.indy.services.NewBenefitsService;
import com.indy.services.PurchaseNewMembershipService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.SwpeMainActivity;
import com.indy.views.fragments.usage.PackagesFragments;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.LoadingFragmnet;
import com.indy.views.fragments.utils.MasterFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewMembershipUpgradeBenefitsFragment extends MasterFragment implements NewMembershipSuccessListener {

    View view;
    TextView bt_cancel;
    Button bt_proceed;
    TextView tv_benefits;
    NewBenefitsModel benefitsOutputModel = new NewBenefitsModel();
    RecyclerView rv_benefits;
    TextView tv_header, footer_description, footer_title, footer_subtitle;
    private BalanceResponseModel balanceResponseModel;
    private Double availableBalance = 0.0;
    private Double newMembershipPrice;
    BenefitsAdapter benefitsAdapter;
    private NewMembershipSuccessListener newMembershipSuccessListener;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_new_benefits, container, false);
        initUI();
        onConsumeService();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();

        bt_proceed = (Button) view.findViewById(R.id.btn_proceed);
        bt_cancel = (TextView) view.findViewById(R.id.btn_cancel);
        tv_header = (TextView) view.findViewById(R.id.tv_header);
        footer_title = (TextView) view.findViewById(R.id.footer_title);
        footer_subtitle = (TextView) view.findViewById(R.id.footer_subtitle);
        footer_description = (TextView) view.findViewById(R.id.footer_description);
        rv_benefits = view.findViewById(R.id.rv_benefits);

        bt_proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                purchaseNewMembership();
            }
        });

        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        newMembershipPrice = Double.parseDouble(sharedPrefrencesManger.getNewMembershipPrice());
        if (sharedPrefrencesManger.isVATEnabled()) {
            newMembershipPrice = newMembershipPrice + ((newMembershipPrice / 100) * 5);
        }
        benefitsAdapter = new BenefitsAdapter(benefitsOutputModel.getBenefitsSummary(), getActivity(), (SwpeMainActivity) getActivity());
        rv_benefits.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_benefits.setAdapter(benefitsAdapter);
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        ((SwpeMainActivity) getActivity()).addFragmnet(new LoadingFragmnet(), R.id.frameLayout, true);
        MasterInputResponse masterInputResponse = new MasterInputResponse();
        masterInputResponse.setAppVersion(appVersion);
        masterInputResponse.setImsi(sharedPrefrencesManger.getMobileNo());
        masterInputResponse.setDeviceId(deviceId);
        masterInputResponse.setToken(token);
        masterInputResponse.setLang(currentLanguage);
        masterInputResponse.setChannel(channel);
        masterInputResponse.setMsisdn(sharedPrefrencesManger.getMobileNo());
        masterInputResponse.setOsVersion(osVersion);
        new NewBenefitsService(this, masterInputResponse);
    }

    private void getAvailableBalance() {
        BalanceInputModel balanceInputModel = new BalanceInputModel();
        balanceInputModel.setAppVersion(appVersion);
        balanceInputModel.setToken(token);
        balanceInputModel.setOsVersion(osVersion);
        balanceInputModel.setChannel(channel);
        balanceInputModel.setDeviceId(deviceId);
        balanceInputModel.setLang(currentLanguage);
        balanceInputModel.setImsi(sharedPrefrencesManger.getMobileNo());
        balanceInputModel.setMsisdn(sharedPrefrencesManger.getMobileNo());
        balanceInputModel.setAuthToken(authToken);
        balanceInputModel.setBillingPeriod(CommonMethods.getBillingPeriod());
        new GetUserBalanceService(this, balanceInputModel, getActivity());
    }

    private void purchaseNewMembership() {
        MasterInputResponse masterInputResponse = new MasterInputResponse();
        masterInputResponse.setAppVersion(appVersion);
        masterInputResponse.setToken(token);
        masterInputResponse.setOsVersion(osVersion);
        masterInputResponse.setChannel(channel);
        masterInputResponse.setDeviceId(deviceId);
        masterInputResponse.setLang(currentLanguage);
        masterInputResponse.setImsi(sharedPrefrencesManger.getMobileNo());
        masterInputResponse.setMsisdn(sharedPrefrencesManger.getMobileNo());
        masterInputResponse.setAuthToken(authToken);
        new PurchaseNewMembershipService(this, masterInputResponse);
        ((SwpeMainActivity) getActivity()).replaceFragmnet(new LoadingFragmnet(), R.id.contentFrameLayout, true);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);

        if (responseModel.getServiceType() != null && responseModel.getServiceType().equals(ServiceUtils.purchase_new_membership_service)) {
            int j = 0;
            for (int i = 0; i < ((SwpeMainActivity) getActivity()).getSupportFragmentManager().getFragments().size(); i++) {

                if (((SwpeMainActivity) getActivity()).getSupportFragmentManager().getFragments().get(i) instanceof LoadingFragmnet) {
                    j++;
                }

            }
            Fragment[] loadingFragments = new Fragment[j];
            for (int i = 0; i < j; i++) {
                loadingFragments[i] = new LoadingFragmnet();
            }
            ((SwpeMainActivity) getActivity()).removeFragment(loadingFragments);

            MasterErrorResponse masterErrorResponse = (MasterErrorResponse) responseModel.getResultObj();
            if (masterErrorResponse.getErrorCode() == null) {
                if (PackagesFragments.instance != null) {
                    PackagesFragments.instance.getPackagesList();
                }
                onTransferSuccess();
            } else {
                // if (No Sufficient balance)
//                if (true) {
//                    Intent intent = new Intent(getActivity(), RechargeActivity.class);
//                    startActivity(intent);
//                }
//                else
                if (masterErrorResponse != null && masterErrorResponse.getErrorMsgEn() != null) {
                    if (currentLanguage.equals("en")) {
                        showErrorFragment(masterErrorResponse.getErrorMsgEn());
                    } else {

                        showErrorFragment(masterErrorResponse.getErrorMsgAr());
                    }
                } else {
                    showErrorFragment(getString(R.string.generice_error));
                }
            }
        } else {
            try {
                if (responseModel != null && responseModel.getResultObj() != null) {
                    benefitsOutputModel = (NewBenefitsModel) responseModel.getResultObj();
                    if (benefitsOutputModel != null && benefitsOutputModel.getBenefitsSummary() != null
                            && benefitsOutputModel.getBenefitsSummary().size() > 0) {
                        setupView();
                    } else {
                        failure();
                    }
                }
                getActivity().onBackPressed();
            } catch (Exception ex) {
                if (ex != null) {
                    ex.printStackTrace();
                }
            }
        }
    }

    private void setupEligibility() {

        //PROMO
        if (benefitsOutputModel.getFooter().getSubtitle() == null) {

        }
        //NON PROMO
        else {

        }
//        bt_proceed.setEnabled(true);
//        if (availableBalance > newMembershipPrice) {
//            bt_proceed.setText(R.string.proceed);
//            bt_proceed.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    purchaseNewMembership();
//                }
//            });
//        } else {
//            bt_proceed.setText(R.string.recharge);
//            tv_footer.setVisibility(View.VISIBLE);
//            bt_proceed.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = new Intent(getActivity(), RechargeActivity.class);
//                    startActivity(intent);
//                }
//            });
//        }
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        try {
            getActivity().onBackPressed();
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    public void setupView() {
//        getAvailableBalance();
        tv_header.setText(benefitsOutputModel.getHeader());
        if (benefitsOutputModel.getFooter() != null) {
            footer_title.setText(benefitsOutputModel.getFooter().getTitle());
            footer_description.setText(benefitsOutputModel.getFooter().getDescription());
            if (benefitsOutputModel.getFooter().getSubtitle() != null) {
                footer_subtitle.setVisibility(View.VISIBLE);
                footer_subtitle.setText(benefitsOutputModel.getFooter().getSubtitle());
            }
        }
        benefitsAdapter.setData(benefitsOutputModel.getBenefitsSummary());
    }

    private void showErrorFragment(String error) {
        ((SwpeMainActivity) getActivity()).showFrame();
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, error);
        errorFragment.setArguments(bundle);
        ((SwpeMainActivity) getActivity()).replaceFragmnet(errorFragment, R.id.contentFrameLayout, true);
    }

    private void onTransferSuccess() {
        NewMembershipSuccessfulFragment newMembershipSuccessfulFragment = new NewMembershipSuccessfulFragment();
        newMembershipSuccessfulFragment.setNewMembershipSuccessListener(this);
        ((SwpeMainActivity) getActivity()).replaceFragmnet(newMembershipSuccessfulFragment, R.id.contentFrameLayout, true);
    }
    public void failure() {

    }

    public void setNewMembershipSuccessListener(NewMembershipSuccessListener newMembershipSuccessListener){
        this.newMembershipSuccessListener = newMembershipSuccessListener;
    }

    @Override
    public void onOKPressed() {
        ((SwpeMainActivity) getActivity()).onBackPressed();
        if (newMembershipSuccessListener != null)
            newMembershipSuccessListener.onOKPressed();
    }
}
