package com.indy.views.fragments.usage;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.customviews.CustomButton;
import com.indy.customviews.CustomEditText;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.models.validateotp.ValidateOtpInput;
import com.indy.models.validateotp.ValidateOtpOutput;
import com.indy.services.ValidateOtpService;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.ChangePasswordActivity;
import com.indy.views.activites.HelpActivity;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.MasterFragment;

import java.util.ArrayList;

/**
 * Created by hadi on 10/7/16.
 */
public class ChangePasswordCodeFragment extends MasterFragment {
    private View view;
    private CustomButton continueButton;
    private CustomEditText code1, code2, code3, code4;
    private TextView help;
    private String mobileNoTag = "MOBILENOTAG";
    private String codeTag = "CODETAG";
    public static String codeGenerated;
    public static String mobileNO;
    private Button backImg;


    int[] tvListIds = {R.id.code_1, R.id.code_2, R.id.code_3, R.id.code_4};
    ArrayList<EditText> tv_digits;
    TextView errorTxt;
    ProgressBar progressID;
    BaseResponseModel mBaseResponse;
    ValidateOtpOutput validateOtpOutput;
    private RelativeLayout helpLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_change_password_code, container, false);
        initUI();
        setListeners();
        disableNextBtn();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
//        regActivity.appBarId.setVisibility(View.VISIBLE);
        progressID = (ProgressBar) view.findViewById(R.id.progressID);
        code1 = (CustomEditText) view.findViewById(R.id.code_1);
        code2 = (CustomEditText) view.findViewById(R.id.code_2);
        code3 = (CustomEditText) view.findViewById(R.id.code_3);
        code4 = (CustomEditText) view.findViewById(R.id.code_4);

        errorTxt = (TextView) view.findViewById(R.id.errorTxt);
        help = (TextView) view.findViewById(R.id.help);
        continueButton = (CustomButton) view.findViewById(R.id.continueBtn);
        helpLayout = (RelativeLayout) view.findViewById(R.id.helpLayout);

//        regActivity.appBarId.setVisibility(View.GONE);
//        showKeyBoard();
        codeGenerated = null;
        if (getArguments() != null && !getArguments().getString(codeTag, "").isEmpty()) {
            codeGenerated = getArguments().getString(codeTag);
        }
        if (getArguments() != null && !getArguments().getString(mobileNoTag, "").isEmpty()) {
            mobileNO = getArguments().getString(mobileNoTag);
        }
        tv_digits = new ArrayList<EditText>();
        for (int i = 0; i < tvListIds.length; i++) {
            EditText et_temp = (EditText) view.findViewById(tvListIds[i]);
            et_temp.addTextChangedListener(new CustomTextWatcher(et_temp));
            tv_digits.add(et_temp);
        }

        backImg = (Button) view.findViewById(R.id.backImg);
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ChangePasswordActivity) getActivity()).onBackPressed();
            }
        });

        helpLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), HelpActivity.class);
                startActivity(intent);
            }
        });
        TextView headerTxt = (TextView) view.findViewById(R.id.titleTxt);
        headerTxt.setText(getString(R.string.forgot_password_spaced));

    }

    private void setListeners() {


        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onContinue();
            }
        });
    }


    private void disableNextBtn() {
        continueButton.setAlpha(0.5f);
        continueButton.setEnabled(false);
    }

    private void enableNextBtn() {
        continueButton.setAlpha(1.0f);
        continueButton.setEnabled(true);
    }

    private String getCode() {
        String codeEntered = "";
        codeEntered += code1.getText().toString();
        codeEntered += code2.getText().toString();
        codeEntered += code3.getText().toString();
        codeEntered += code4.getText().toString();
        return codeEntered;
    }

    private void onContinue() {

        progressID.setVisibility(View.VISIBLE);

        ValidateOtpInput validateOtpInput = new ValidateOtpInput();
        validateOtpInput.setAuthToken(authToken);
        validateOtpInput.setAppVersion(appVersion);
        validateOtpInput.setChannel(channel);
        validateOtpInput.setDeviceId(deviceId);
        validateOtpInput.setImsi(sharedPrefrencesManger.getMobileNo());
        validateOtpInput.setLang(currentLanguage);
        validateOtpInput.setOtpCode(getCode());
        validateOtpInput.setOsVersion(osVersion);
        validateOtpInput.setToken(token);
        validateOtpInput.setMsisdn(sharedPrefrencesManger.getMobileNo());
        new ValidateOtpService(this, validateOtpInput);

    }


    private void onHelp() {
        Intent intent = new Intent(getActivity(), HelpActivity.class);
        startActivity(intent);
    }

    public void showKeyBoard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, 0);
        }
    }


    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
    }


    public class CustomTextWatcher implements TextWatcher {
        private EditText view;

        private CustomTextWatcher(EditText view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

            String text = editable.toString();
            if (getCode().length() == 4)
                enableNextBtn();
            else
                disableNextBtn();
            if (text.length() == 1) {
                view.clearFocus();
                enableNextBtn();
                int currentIndex = tv_digits.indexOf(view);
                if (currentIndex == 3) {
                    hideKeyBoard();
                } else {
                    for (int i = currentIndex + 1; i < tv_digits.size(); i++) {
                        tv_digits.get(i).requestFocus();

                        return;

                    }
                }
            } else
                disableNextBtn();
        }
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            progressID.setVisibility(View.GONE);
            mBaseResponse = responseModel;
            if(responseModel!=null && responseModel.getResultObj()!=null) {
                validateOtpOutput = (ValidateOtpOutput) responseModel.getResultObj();
                if (validateOtpOutput != null && validateOtpOutput.getIsValid() != null && validateOtpOutput.getIsValid())
                    onValidateOtpSucess();
                else
                    onvalidateOtpError();
            }
        }catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        try {
            progressID.setVisibility(View.GONE);
        }catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        super.onUnAuthorizeToken(masterErrorResponse);
        progressID.setVisibility(View.GONE);

    }

    private void onValidateOtpSucess() {
        ((ChangePasswordActivity) getActivity()).replaceFragmnet(new SetNewPasswordSettingsFragment(), R.id.frameLayout, true);

    }

    private void onvalidateOtpError() {
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        if(validateOtpOutput!=null && validateOtpOutput.getErrorMsgEn()!=null) {
            if (currentLanguage != null && validateOtpOutput != null && validateOtpOutput.getErrorMsgEn() != null) {
                if (currentLanguage.equalsIgnoreCase("en"))
                    bundle.putString(ConstantUtils.errorString, validateOtpOutput.getErrorMsgEn());
                else
                    bundle.putString(ConstantUtils.errorString, validateOtpOutput.getErrorMsgAr());
            } else {
                if (sharedPrefrencesManger != null && sharedPrefrencesManger.getLanguage().equals("en") && validateOtpOutput != null) {
                    bundle.putString(ConstantUtils.errorString, validateOtpOutput.getErrorMsgEn());
                } else {
                    bundle.putString(ConstantUtils.errorString, validateOtpOutput.getErrorMsgAr());
                }
            }
        }else{
            bundle.putString(ConstantUtils.errorString,getString(R.string.generice_error));
        }
        errorFragment.setArguments(bundle);
        ((ChangePasswordActivity) getActivity()).replaceFragmnet(errorFragment, R.id.frameLayout, true);
    }
}
