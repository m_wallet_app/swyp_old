package com.indy.views.fragments.login;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.widget.ProgressBar;

public class MyProgressBar extends ProgressBar {

    Paint p = new Paint();
    int progress;
    public MyProgressBar(Context context) {
        super(context);
        p.setColor(Color.GREEN);
    }
    // Resizing Behavior
    public enum ResizingBehavior {
        AspectFit, //!< The content is proportionally resized to fit into the target rectangle.
        AspectFill, //!< The content is proportionally resized to completely fill the target rectangle.
        Stretch, //!< The content is stretched to match the entire target rectangle.
        Center, //!< The content is centered in the target rectangle, but it is NOT resized.
    }

    @Override
    protected synchronized void onDraw(Canvas canvas) {
        canvas.drawLine(0, 0, getWidth(), 0, p);
        canvas.drawLine(0, 0, 0, getHeight(), p);
        canvas.drawArc(new RectF(0, 0, getWidth(),  getHeight()), 40,40, true, p);
        /**
         * Whatever drawing is needed for your Layout
         * But make sure you use relative lenghts and heights
         * So you can reuse your Widget.
         * Also use the Progress Variable to draw the filled part
         * corresponding to getMax()
         */


        Paint paint = CacheForCanvas1.paint;

        // Resize to Target Frame
        canvas.save();
        RectF resizedFrame = CacheForCanvas1.resizedFrame;
//        MyProgressBar.resizingBehaviorApply(resizing, CacheForCanvas1.originalFrame, targetFrame, resizedFrame);
        canvas.translate(resizedFrame.left, resizedFrame.top);
        canvas.scale(resizedFrame.width() / 466f, resizedFrame.height() / 156f);

        // Bezier
        RectF bezierRect = CacheForCanvas1.bezierRect;
        bezierRect.set(20.42f, 20.5f, 435.5f, 121.5f);
        Path bezierPath = CacheForCanvas1.bezierPath;
        bezierPath.reset();
        bezierPath.moveTo(435.5f, 45.5f);
        bezierPath.lineTo(435.5f, 65.5f);
        bezierPath.lineTo(435.5f, 90.5f);
        bezierPath.cubicTo(435.5f, 90.5f, 429f, 103.75f, 420.5f, 111.5f);
        bezierPath.cubicTo(412f, 119.25f, 401.5f, 121.5f, 401.5f, 121.5f);
        bezierPath.lineTo(52.5f, 111.5f);
        bezierPath.cubicTo(52.5f, 111.5f, 22.5f, 120.5f, 20.5f, 77.5f);
        bezierPath.cubicTo(18.5f, 34.5f, 52.5f, 35.5f, 52.5f, 35.5f);
        bezierPath.lineTo(401.5f, 20.5f);
        bezierPath.cubicTo(401.5f, 20.5f, 412f, 21.5f, 420.5f, 28.5f);
        bezierPath.cubicTo(429f, 35.5f, 435.5f, 45.5f, 435.5f, 45.5f);
        bezierPath.close();

        paint.reset();
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.GRAY);
        canvas.drawPath(bezierPath, paint);

        paint.reset();
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        paint.setStrokeWidth(1f);
        paint.setStrokeMiter(10f);
        canvas.save();

        RectF bezierRect2 = new RectF();

        paint.reset();
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.RED);

        double percentageValue = 435.5 * (progress/100);
        bezierRect2.set(20.42f, 20.5f, (float)percentageValue, 121.5f);
        canvas.drawRect(bezierRect2,paint);
//        paint.setStyle(Paint.Style.STROKE);
//        paint.setColor(Color.BLACK);
//        canvas.drawPath(bezierPath, paint);
        canvas.restore();

        canvas.restore();

    }

    protected void onValueChanged(){
        /**
         * Update your progress Variable and draw again
         */
        progress = getProgress();
        this.invalidate();
    }

    @Override
    public synchronized void setMax(int max) {
        onValueChanged();
        super.setMax(max);
    }

    @Override
    public synchronized void setProgress(int progress) {
        onValueChanged();
        super.setProgress(progress);
    }

    @Override
    public synchronized void setSecondaryProgress(int secondaryProgress) {
        onValueChanged();
        super.setSecondaryProgress(secondaryProgress);
    }

    private static class CacheForCanvas1 {
        private static Paint paint = new Paint();
        private static RectF originalFrame = new RectF(0f, 0f, 466f, 156f);
        private static RectF resizedFrame = new RectF();
        private static RectF bezierRect = new RectF();
        private static Path bezierPath = new Path();
    }
    // Resizing Behavior
    public static void resizingBehaviorApply(ResizingBehavior behavior, RectF rect, RectF target, RectF result) {
        if (rect.equals(target) || target == null) {
            result.set(rect);
            return;
        }

        if (behavior == ResizingBehavior.Stretch) {
            result.set(target);
            return;
        }

        float xRatio = Math.abs(target.width() / rect.width());
        float yRatio = Math.abs(target.height() / rect.height());
        float scale = 0f;

        switch (behavior) {
            case AspectFit: {
                scale = Math.min(xRatio, yRatio);
                break;
            }
            case AspectFill: {
                scale = Math.max(xRatio, yRatio);
                break;
            }
            case Center: {
                scale = 1f;
                break;
            }
        }

        float newWidth = Math.abs(rect.width() * scale);
        float newHeight = Math.abs(rect.height() * scale);
        result.set(target.centerX() - newWidth / 2,
                target.centerY() - newHeight / 2,
                target.centerX() + newWidth / 2,
                target.centerY() + newHeight / 2);
    }
}