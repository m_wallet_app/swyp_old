package com.indy.views.fragments.gamification.inbox;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.ShoppingCartActivity;
import com.indy.views.activites.SwpeMainActivity;
import com.indy.views.fragments.gamification.challenges.ChallengesMainFragment;
import com.indy.views.fragments.gamification.models.getnotificationdetails.GetNotificationDetailsInput;
import com.indy.views.fragments.gamification.models.getnotificationdetails.GetNotificationDetailsOutput;
import com.indy.views.fragments.gamification.models.getnotificationdetails.NotificationDetails;
import com.indy.views.fragments.gamification.models.serviceInputOutput.CheckinRewardModel;
import com.indy.views.fragments.gamification.services.GetNotificationDetailsService;
import com.indy.views.fragments.rewards.RewardsFragment;
import com.indy.views.fragments.utils.ErrorFragment;
import com.indy.views.fragments.utils.MasterFragment;
import com.squareup.picasso.Picasso;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationDetailFragment extends MasterFragment {


    private TextView tv_date, tv_title, tv_detail, tv_btn;
    private View rootView;
    private ImageView iv_notification;
    private String notificationId;
    private ProgressBar progressBar;
    private GetNotificationDetailsOutput getNotificationDetailsOutput;
    private LinearLayout layout;
    private RelativeLayout rl_back;
    private RelativeLayout rl_help;
    private TextView tv_headerTitle;


    public NotificationDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_notification_detail, container, false);
        initUI();
        if (getArguments() != null) {
            notificationId = getArguments().getString(ConstantUtils.NOTIFICATIONS_ID, "");
            if (notificationId != null && !notificationId.isEmpty()) {
                //call servcei for detail here
                onConsumeService();
            }
        }
        return rootView;
    }

    @Override
    public void initUI() {
        super.initUI();
        rl_back = (RelativeLayout) rootView.findViewById(R.id.backLayout);
        rl_help = (RelativeLayout) rootView.findViewById(R.id.helpLayout);
        rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        tv_headerTitle = (TextView) rootView.findViewById(R.id.titleTxt);
        tv_headerTitle.setText(getResources().getString(R.string.message));
        rl_help.setVisibility(View.INVISIBLE);
        tv_btn = (TextView) rootView.findViewById(R.id.btn_action_notiication);
        tv_date = (TextView) rootView.findViewById(R.id.tv_date_notification);
        tv_detail = (TextView) rootView.findViewById(R.id.tv_detail_notification);
        tv_title = (TextView) rootView.findViewById(R.id.tv_title_notification);
        iv_notification = (ImageView) rootView.findViewById(R.id.iv_notification);
        layout = (LinearLayout) rootView.findViewById(R.id.layout);
        layout.setVisibility(View.GONE);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressID);
        onConsumeService();
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        progressBar.setVisibility(View.VISIBLE);
        GetNotificationDetailsInput getNotificationDetailsInput = new GetNotificationDetailsInput();
        getNotificationDetailsInput.setChannel(channel);
        getNotificationDetailsInput.setMsisdn(sharedPrefrencesManger.getMobileNo());
        getNotificationDetailsInput.setAppVersion(appVersion);
        getNotificationDetailsInput.setAuthToken(authToken);
        getNotificationDetailsInput.setDeviceId(deviceId);
        getNotificationDetailsInput.setToken(token);
        getNotificationDetailsInput.setUserSessionId(sharedPrefrencesManger.getUserSessionId());
        getNotificationDetailsInput.setUserId(sharedPrefrencesManger.getUserId());
        getNotificationDetailsInput.setApplicationId(ConstantUtils.INDY_TALOS_ID);
        getNotificationDetailsInput.setImsi(sharedPrefrencesManger.getMobileNo());
        getNotificationDetailsInput.setRegistrationId(sharedPrefrencesManger.getRegisterationID());
        getNotificationDetailsInput.setOsVersion(osVersion);
        getNotificationDetailsInput.setNotificationId(notificationId);
        new GetNotificationDetailsService(this, getNotificationDetailsInput);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            progressBar.setVisibility(View.GONE);
            layout.setVisibility(View.VISIBLE);
            switch (responseModel.getServiceType()) {
                case ServiceUtils.GET_NOTIFICATION_DETAILS: {
                    if (responseModel != null) {
                        getNotificationDetailsOutput = (GetNotificationDetailsOutput) responseModel.getResultObj();
                        if (getNotificationDetailsOutput != null)
                            if (getNotificationDetailsOutput.getNotificationDetails() != null) {
                                getNotificationDetailsSuccess();
                            } else {
                                getNotificationDetailsError();
                            }
                    }
                    break;
                }
            }
        } catch (Exception ex) {

        }
    }

    public void getNotificationDetailsSuccess() {
        NotificationDetails notificationDetails = getNotificationDetailsOutput.getNotificationDetails();
        if (notificationDetails.getNotificationTitle() != null && !notificationDetails.getNotificationTitle().isEmpty())
            tv_title.setText(notificationDetails.getNotificationTitle());
        if (notificationDetails.getNotificationText() != null && !notificationDetails.getNotificationText().isEmpty())
            tv_detail.setText(notificationDetails.getNotificationText());
        if (notificationDetails.getNotificationDate() != null && !notificationDetails.getNotificationDate().isEmpty())
            tv_date.setText(CommonMethods.parseDate(notificationDetails.getNotificationDate(), ConstantUtils.lang_english));
        if (notificationDetails.getNotificationImage() != null && !notificationDetails.getNotificationImage().isEmpty()) {
            Picasso.with(getActivity()).load(notificationDetails.getNotificationImage()).into(iv_notification);
        } else {
            iv_notification.setVisibility(View.GONE);
        }
        tv_btn.setVisibility(View.VISIBLE);
        setClickListenerOnButton(notificationDetails.getNotificationActionType());

    }

    private void getNotificationDetailsError() {
        ErrorFragment errorFragment = new ErrorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ConstantUtils.errorString, sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)
                ? getNotificationDetailsOutput.getErrorMsgEn() : getNotificationDetailsOutput.getErrorMsgAr());
        errorFragment.setArguments(bundle);
        ((SwpeMainActivity) getActivity()).replaceFragmnet(errorFragment, R.id.frameLayout, true);
    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
        progressBar.setVisibility(View.GONE);
    }

    public void setClickListenerOnButton(int type) {
        switch (type) {
            case 4:
                //navigate to perks
                tv_btn.setText(getString(R.string.go_see_perks));
                tv_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Fragment[] frag = {new NotificationDetailFragment(), new InboxFragment()};
                        ((SwpeMainActivity) getActivity()).removeFragment(frag);
                        ((SwpeMainActivity) getActivity()).replaceFragmnet(new RewardsFragment(), R.id.contentFrameLayout, true);
                        SwpeMainActivity.clickedItem = SwpeMainActivity.rewardsClicked;

                    }
                });
                break;

            case 2:
                tv_btn.setText(getString(R.string.go_see_raffles));
                tv_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Fragment[] frag = {new NotificationDetailFragment(), new InboxFragment()};
                        ((SwpeMainActivity) getActivity()).removeFragment(frag);
                        ChallengesMainFragment challengesMainFragment = new ChallengesMainFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString(ConstantUtils.CHALLENGES_TAB_NAVIGATION_INDEX, "2");
                        challengesMainFragment.setArguments(bundle);
                        ((SwpeMainActivity) getActivity()).replaceFragmnet(challengesMainFragment, R.id.contentFrameLayout, true);
                        SwpeMainActivity.clickedItem = SwpeMainActivity.rewardsClicked;

                    }
                });

                break;

            case 3:
            case 1:
                tv_btn.setText(getString(R.string.go_see_badges));
                tv_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Fragment[] frag = {new NotificationDetailFragment(), new InboxFragment()};
                        ((SwpeMainActivity) getActivity()).removeFragment(frag);
                        ChallengesMainFragment challengesMainFragment = new ChallengesMainFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString(ConstantUtils.CHALLENGES_TAB_NAVIGATION_INDEX, "0");
                        challengesMainFragment.setArguments(bundle);
                        ((SwpeMainActivity) getActivity()).replaceFragmnet(challengesMainFragment, R.id.contentFrameLayout, true);
                        SwpeMainActivity.clickedItem = SwpeMainActivity.rewardsClicked;

                    }
                });

                break;

            case 5:
                tv_btn.setText(getString(R.string.get_it_now));
                tv_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), ShoppingCartActivity.class);
                        CheckinRewardModel checkinRewardModel = new CheckinRewardModel();
                        checkinRewardModel.setCurrentPrice(getNotificationDetailsOutput.getNotificationDetails().getLuckyDeal().getCurrentPrice());
                        if(sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)) {
                            checkinRewardModel.setPackageDescription(getNotificationDetailsOutput.getNotificationDetails().getLuckyDeal().getPackageDescription());
                            checkinRewardModel.setPackageName(getNotificationDetailsOutput.getNotificationDetails().getLuckyDeal().getPackageName());

                        }else{
                            checkinRewardModel.setPackageDescription(getNotificationDetailsOutput.getNotificationDetails().getLuckyDeal().getPackageDescriptionAr());
                            checkinRewardModel.setPackageName(getNotificationDetailsOutput.getNotificationDetails().getLuckyDeal().getPackageNameAr());

                        }
                        checkinRewardModel.setTotalDescription(getNotificationDetailsOutput.getNotificationDetails().getLuckyDeal().getTotalDescription());
                        checkinRewardModel.setPackageCode(getNotificationDetailsOutput.getNotificationDetails().getLuckyDeal().getPackageCode());
                        checkinRewardModel.setPreviousPrice(getNotificationDetailsOutput.getNotificationDetails().getLuckyDeal().getPreviousPrice());
                        checkinRewardModel.setRewardId(getNotificationDetailsOutput.getNotificationDetails().getLuckyDeal().getRewardId());
                        checkinRewardModel.setType(getNotificationDetailsOutput.getNotificationDetails().getLuckyDeal().getType());
                        checkinRewardModel.setNotificationResponseId(getNotificationDetailsOutput.getNotificationDetails().getNotificationId());
                        intent.putExtra(ConstantUtils.lottery_reward, checkinRewardModel);
                        startActivity(intent);
                    }
                });
                break;
            default:
                break;
        }
    }

    public void onGetDetailFailure() {

    }
}
