package com.indy.views.fragments.buynewline.exisitingnumber;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.LogApplicationFlowService;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.NearestStoreActivity;
import com.indy.views.fragments.buynewline.NewNumberFragment;
import com.indy.views.fragments.utils.MasterFragment;

import static com.indy.views.activites.RegisterationActivity.logEventInputModel;

/**
 * Created by emad on 9/25/16.
 */

public class NonEtisaaltFragment extends MasterFragment {
    private Button newNumberID, nearestShopBtn;
    private View view;
    private TextView tv_title;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_non_etisalat, container, false);
        initUI();
        setListeners();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
        regActivity.setHeaderTitle("");
        regActivity.showHeaderLayout();
        newNumberID = (Button) view.findViewById(R.id.newNumberID);
        nearestShopBtn = (Button) view.findViewById(R.id.btn_locate_nearest_shop);
        tv_title = (TextView) view.findViewById(R.id.tv_title_portability);
        String textToReplace = tv_title.getText().toString().trim();
        String mobileNumber = "";
        if(getArguments()!=null && getArguments().getString(ConstantUtils.mobileNo)!=null){
            mobileNumber = getArguments().getString(ConstantUtils.mobileNo);
            textToReplace = textToReplace +"\n"+mobileNumber;
            tv_title.setText(textToReplace);

        }

        nearestShopBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();

                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_check_nearest_shop);
                bundle.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_check_nearest_shop);
                mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_check_nearest_shop, bundle);
                Intent intent = new Intent(getActivity(), NearestStoreActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
    }

    private void logEvent() {
logEventInputModel.setActionTransactionId(sharedPrefrencesManger.getTempAppId());
        logEventInputModel.setScreenId(ConstantUtils.SCREEN_ID_3002);

        logEventInputModel.setNonEtisalatNewNumber(newNumberID.getText().toString());

        new LogApplicationFlowService(this, logEventInputModel);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
    }

    private void setListeners() {
        newNumberID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logEvent();
                regActivity.replaceFragmnet(new NewNumberFragment(), R.id.frameLayout, true);
            }
        });
    }
}
