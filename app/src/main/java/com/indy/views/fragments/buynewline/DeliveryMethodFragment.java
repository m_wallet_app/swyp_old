package com.indy.views.fragments.buynewline;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.R;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.utils.MasterFragment;


/**
 * Created by emad on 8/2/16.
 */
public class DeliveryMethodFragment extends MasterFragment {
    private View view;
    private Button nextBtn;
    private RadioButton standeredRBtn, bulletRBtn;
    public static String shippingMethodStr;
    public static DeliveryMethodFragment deliveryFragmentInstance;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragmnet_delivery, container, false);
        initUI();
        setListeners();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
        regActivity.showHeaderLayout();
        regActivity.setHeaderTitle("");
        nextBtn = (Button) view.findViewById(R.id.nextBtn);
        standeredRBtn = (RadioButton) view.findViewById(R.id.standeredRbtn);
        bulletRBtn = (RadioButton) view.findViewById(R.id.bulletBtn);
        standeredRBtn.setChecked(true);
        shippingMethodStr = "free";
        deliveryFragmentInstance = this;


    }

    @Override
    public void onResume() {
        super.onResume();
        if(NewNumberFragment.isTimerActive && CommonMethods.isTimerUp(sharedPrefrencesManger.getTimerValue())){
            Fragment[] myFragments = {OrderFragment.orderFragmentInstance,NewNumberFragment.loadingFragmnet, deliveryFragmentInstance, NewNumberFragment.newNumberFragment};
            regActivity.removeFragment(myFragments);
            regActivity.replaceFragmnet(new NewNumberFragment(),R.id.frameLayout,true);
            NewNumberFragment.isTimerActive=false;
        }
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
        super.onErrorListener(responseModel);
    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
    }

    void setListeners() {
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()) {
                    if (standeredRBtn.isChecked()) {
                        shippingMethodStr = "free";
                    }
                    if (bulletRBtn.isChecked()) {
                        shippingMethodStr = "bullet";
                    }

                    Bundle bundle2 = new Bundle();

                    bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_selected_standard_delivery);
                    bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_selected_standard_delivery);
                    mFirebaseAnalytics.logEvent(ConstantUtils.TAGGING_selected_standard_delivery, bundle2);

                    regActivity.replaceFragmnet(new DeliveryDetailsAreaFragment(), R.id.frameLayout, true);
                }

            }
        });

        standeredRBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!standeredRBtn.isChecked())
                    standeredRBtn.setChecked(true);
                bulletRBtn.setChecked(false);

            }
        });
        bulletRBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!bulletRBtn.isChecked())
                    bulletRBtn.setChecked(true);
                standeredRBtn.setChecked(false);
            }
        });
    }

    private boolean isValid() {
        if (standeredRBtn.isChecked() || bulletRBtn.isChecked())
            return true;
        else
            return false;
    }
}
