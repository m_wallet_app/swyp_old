package com.indy.views.fragments.gamification.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.services.BaseServiceManger;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetWinnersListInputResponse;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetWinnersListOutputModel;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**

 */
public class GetEventWinnersListService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private GetWinnersListInputResponse getWinnersListInputResponse;


    public GetEventWinnersListService(BaseInterface baseInterface, GetWinnersListInputResponse getWinnersListInputResponse) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.getWinnersListInputResponse = getWinnersListInputResponse;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<GetWinnersListOutputModel> call = apiService.getEventWinnersList(getWinnersListInputResponse);
        call.enqueue(new Callback<GetWinnersListOutputModel>() {
            @Override
            public void onResponse(Call<GetWinnersListOutputModel> call, Response<GetWinnersListOutputModel> response) {
                mBaseResponseModel.setServiceType(ServiceUtils.getWinnerListService);
                mBaseResponseModel.setResultObj(response.body());

                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (response.body().getErrorMsgEn() != null) {
//                        String urlForTag = call.request().url().toString();
//                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
//                        urlForTag = urlForTag.replace("/", "_");
//                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<GetWinnersListOutputModel> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.getWinnerListService);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
