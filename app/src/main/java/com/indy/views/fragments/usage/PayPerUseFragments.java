package com.indy.views.fragments.usage;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.adapters.PayPerUseAdapter;
import com.indy.models.payperuse.PayPeruseOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.models.utils.MasterInputResponse;
import com.indy.services.PayPerUseService;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.HistoryActivity;
import com.indy.views.fragments.store.recharge.ItemDecoration.DividerItemDecoration;
import com.indy.views.fragments.utils.MasterFragment;

/**
 * Created by emad on 8/9/16.
 */
public class PayPerUseFragments extends MasterFragment {
    private View view;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager linearLayoutManager;
    private MasterInputResponse masterInputResponse;
    private PayPeruseOutput payPeruseOutput;
    private PayPerUseAdapter payPerUseAdapter;
//    private ProgressBar progressBar;
    private TextView intervalID;
    LinearLayout tv_transactionHistorty, ll_progress;

    LinearLayout Lv_packagesView, ll_no_membership;

    //---------
    LinearLayout ll_dummyContainer;
    TextView bt_retry;
    TextView tv_error;
    //------

    public boolean isMembershipValid = true;
    private Button btn_membership;
    private ImageView leftArrow, rightArrow;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_pay_per_use, container, false);
        initUI();
        onConsumeService();
        return view;
    }

    @Override
    public void initUI() {
        super.initUI();
//        btn_membership = (Button) view.findViewById(R.id.membershipBtn);
//        ll_no_membership = (LinearLayout) view.findViewById(R.id.ll_no_membership);
        //  Lv_packagesView = (LinearLayout) view.findViewById(R.id.Lv_packagesView);
        isMembershipValid = true;
        if (sharedPrefrencesManger.getUserProfileObject() != null) {
            if (sharedPrefrencesManger.getUserProfileObject().getMembershipData() != null && sharedPrefrencesManger.getUserProfileObject().getMembershipData().getSubscribtionDaysLeft() != null) {
                // tv_membershipDate.setText(getString(R.string.membership_cycle) + ":  " + LoginFragment.loginOutputModel.getUserProfile().getMembershipData().getSubscribtionDaysLeft() + " "
                // + getString(R.string.days_left));
                isMembershipValid = true;
            } else {
                //tv_membershipDate.setText(getString(R.string.no_valid_membership));
                isMembershipValid = false;
            }

        }

        ll_dummyContainer = (LinearLayout) view.findViewById(R.id.ll_dummy_error_container);
        tv_error = (TextView) view.findViewById(R.id.tv_error);
        ll_dummyContainer.setVisibility(View.GONE);
        bt_retry = (TextView) view.findViewById(R.id.retryBtn);
        bt_retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onConsumeService();
            }
        });
        tv_transactionHistorty = (LinearLayout) view.findViewById(R.id.tv_transactionHistorty);
        leftArrow = (ImageView) view.findViewById(R.id.left_arrow);
        rightArrow = (ImageView) view.findViewById(R.id.right_arrow);
        tv_transactionHistorty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_usage_transaction_history);
                startActivity(new Intent(getActivity(), HistoryActivity.class));
            }
        });
        mRecyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        Lv_packagesView = (LinearLayout) view.findViewById(R.id.Lv_packagesView);
        ll_progress = (LinearLayout) view.findViewById(R.id.ll_progress);
        intervalID = (TextView) view.findViewById(R.id.intervalID);
        mRecyclerView.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), null, false, false));
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setHasFixedSize(true);

    }

    @Override
    public void onConsumeService() {
        super.onConsumeService();
        ll_progress.setVisibility(View.VISIBLE);
        Lv_packagesView.setVisibility(View.GONE);
        ll_dummyContainer.setVisibility(View.GONE);
        masterInputResponse = new MasterInputResponse();
        masterInputResponse.setLang(currentLanguage);
        masterInputResponse.setAppVersion(appVersion);
        masterInputResponse.setToken(token);
        masterInputResponse.setOsVersion(osVersion);
        masterInputResponse.setChannel(channel);
        masterInputResponse.setDeviceId(deviceId);
        masterInputResponse.setMsisdn(sharedPrefrencesManger.getMobileNo());
        masterInputResponse.setAuthToken(authToken);
        new PayPerUseService(this, masterInputResponse);

    }

    @Override
    public void onUnAuthorizeToken(MasterErrorResponse masterErrorResponse) {
        ll_progress.setVisibility(View.GONE);
        super.onUnAuthorizeToken(masterErrorResponse);
    }

    @Override
    public void onSucessListener(BaseResponseModel responseModel) {
        super.onSucessListener(responseModel);
        try {
            ll_progress.setVisibility(View.GONE);
            CommonMethods.logFirebaseEvent(getmFirebaseAnalytics(), ConstantUtils.TAGGING_usage_pay_per_use);
            if(responseModel!=null && responseModel.getResultObj()!=null) {
                payPeruseOutput = (PayPeruseOutput) responseModel.getResultObj();
                if (payPeruseOutput != null) {
                    if (payPeruseOutput.getErrorCode() != null)
                        onGetPayPerUseError();
                    else
                        onGetPayPeruserSucess();
                } else {
                    onGetPayPerUseError();
                }
            }else{
                onGetPayPerUseError();

            }
        }catch (Exception ex){
            if(ex!=null){
                ex.printStackTrace();
            }
        }
    }

    private void onGetPayPeruserSucess() {
        if (payPeruseOutput != null && payPeruseOutput.getPaymentPackageList() != null
                && payPeruseOutput.getPaymentPackageList().size() > 0) {
            Lv_packagesView.setVisibility(View.VISIBLE);

            if(sharedPrefrencesManger.getLanguage().equals("en")) {
                intervalID.setText(payPeruseOutput.getIntervalEn());
            }else{
                intervalID.setText(payPeruseOutput.getIntervalAr());
            }
            payPerUseAdapter = new PayPerUseAdapter(payPeruseOutput.getPaymentPackageList(), getContext());
            mRecyclerView.setAdapter(payPerUseAdapter);
        } else {
            onGetPayPerUseError();
        }
    }

    private void onGetPayPerUseError() {
        ll_progress.setVisibility(View.GONE);
        if (currentLanguage != null) {
            if (currentLanguage.equalsIgnoreCase("en"))
                tv_error.setText(payPeruseOutput.getErrorMsgEn());
            else
                tv_error.setText(payPeruseOutput.getErrorMsgAr());
        } else {
            tv_error.setText(getString(R.string.generice_error));

        }
        ll_dummyContainer.setVisibility(View.VISIBLE);
//        Fragment errorFragmnet = new ErrorFragment();
//        Bundle bundle = new Bundle();
//        if (currentLanguage != null) {
//            if (currentLanguage.equalsIgnoreCase("en"))
//                bundle.putString(ConstantUtils.errorString, payPeruseOutput.getErrorMsgEn());
//            else
//                bundle.putString(ConstantUtils.errorString, payPeruseOutput.getErrorMsgAr());
//        }
//        errorFragmnet.setArguments(bundle);
//        ((SwpeMainActivity) getActivity()).replaceFragmnet(errorFragmnet, R.id.contentFrameLayout, true);

    }

    @Override
    public void onErrorListener(BaseResponseModel responseModel) {
//        super.onErrorListener(responseModel);
        try {
            ll_progress.setVisibility(View.GONE);
            ll_dummyContainer.setVisibility(View.VISIBLE);
//            ll_no_membership.setVisibility(View.GONE);
        }catch (Exception ex){
          CommonMethods.logException(ex);
        }
    }
}
