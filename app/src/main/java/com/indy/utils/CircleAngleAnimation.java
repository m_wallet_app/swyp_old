package com.indy.utils;

import android.view.animation.Animation;
import android.view.animation.Transformation;

/**
 * Created by mobile on 17/04/2017.
 */

public class CircleAngleAnimation extends Animation {

    private MaterialCheckBox circle;

    private float oldAngle;
    private float newAngle;

    public CircleAngleAnimation(MaterialCheckBox circle, int newAngle) {
        this.oldAngle = circle.getOldAngle();
        this.newAngle = newAngle;
        this.circle = circle;
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation transformation) {
        float angle = oldAngle + ((newAngle - oldAngle) * interpolatedTime);

        circle.setNewAngle(angle);
        circle.requestLayout();
    }
}
