package com.indy.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.indy.BuildConfig;
import com.indy.R;
import com.indy.helpers.DateHelpers;
import com.indy.ocr.scanning.EmiratesIdDataObject;
import com.indy.ocrStep2.ocr.CardFields;
import com.indy.views.activites.RechargeActivity;
import com.indy.views.activites.RequestMainPagerActivity;
import com.indy.views.fragments.store.mainstore.MyStoreFragment;
import com.indy.views.fragments.store.recharge.RechargeCardFragment;
import com.indy.views.fragments.usage.UsageFragment;
import com.kofax.mobile.sdk.capture.creditcard.CreditCardParameters;
import com.kofax.mobile.sdk.capture.creditcard.CreditCardWorkflowActivity;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by hadi.mehmood on 10/19/2016.
 */
public class CommonMethods {
    public static FirebaseAnalytics firebaseAnalytics;

    public static boolean isVatEnabled = true;


    public static boolean isConnected(Object context) {
        boolean HaveConnectedWifi = false;
        boolean HaveConnectedMobile = false;
        Context localContextObject = (Context) context;
        ConnectivityManager cm = (ConnectivityManager) localContextObject.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    HaveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    HaveConnectedMobile = true;
        }
        return HaveConnectedWifi || HaveConnectedMobile;
    }


    public static String parseDate(String date) {
        String returnString = "";
        SimpleDateFormat dateFormatter;
        try {
            Date formattedDate;
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            formattedDate = df.parse(date);
            dateFormatter = new SimpleDateFormat("dd MMMM yyyy", Locale.US);
            return dateFormatter.format(formattedDate);
        } catch (Exception ex) {
            return returnString;

        }
    }

    public static String parseDate(String date, String language) {
        String returnString = "";
        SimpleDateFormat dateFormatter;
        try {
            Date formattedDate;
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            formattedDate = df.parse(date);
            dateFormatter = new SimpleDateFormat("dd MMMM yyyy", new Locale(language));
            return dateFormatter.format(formattedDate);
        } catch (Exception ex) {
            return returnString;

        }
    }

    public static String daysDifference(String date, String toAppendAtEnd) {
        Date dateToPass;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        try {

            dateToPass = simpleDateFormat.parse(date);

        } catch (Exception ex) {
            dateToPass = null;
        }
        if (dateToPass != null) {
            long diffInMils = dateToPass.getTime() - DateHelpers.getCurrentDateFor().getTime();
            long daysDiff = TimeUnit.MILLISECONDS.toDays(diffInMils);
            return daysDiff + " " + toAppendAtEnd;
        } else {
            return toAppendAtEnd + " " + date;
        }
    }

    public static Double getFormattedDouble(Double valueToFormat) {
        BigDecimal bd = new BigDecimal(valueToFormat);
        bd = bd.setScale(2, RoundingMode.DOWN);
        return bd.doubleValue();

    }

    public static String getBillingPeriod() {
        String dateToPass;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMyyyy", Locale.US);
        try {

            dateToPass = simpleDateFormat.format(new Date());

        } catch (Exception ex) {
            dateToPass = null;
        }
        return dateToPass;

    }

    public boolean isValidPassword(String password) {

        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=\\S+$).{8,25}$";

        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
        Matcher matcher = pattern.matcher(password);
        if (!matcher.matches()) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean isTimerUp(long oldTime) {
        long currentTime = new Date().getTime();

        long difference = currentTime - oldTime;
        long minutes = TimeUnit.MILLISECONDS.toMinutes(difference);
//        return minutes >= ConstantUtils.timeout_new_number;
        return false;
    }

    public static void updateBalanceAndLastUpdated(String amount, String lastUpdated) {

        if (UsageFragment.balanceID != null && UsageFragment.tv_lastUpdated != null) {
            UsageFragment.balanceID.setText(amount);
            UsageFragment.tv_lastUpdated.setText(lastUpdated);
        }

        if (RequestMainPagerActivity.titleTxt != null && RequestMainPagerActivity.tv_lastUpdated != null) {
            RequestMainPagerActivity.titleTxt.setText(amount);
            RequestMainPagerActivity.tv_lastUpdated.setText(lastUpdated);
        }

        if (RechargeActivity.user_balance_amount != null && RechargeActivity.tv_lastUpdated != null) {
            RechargeActivity.user_balance_amount.setText(amount);
            RechargeActivity.tv_lastUpdated.setText(lastUpdated);
        }

        if (MyStoreFragment.tv_titleTxt != null && MyStoreFragment.tv_lastUpdated != null) {
            MyStoreFragment.tv_titleTxt.setText(amount);
            MyStoreFragment.tv_lastUpdated.setText(lastUpdated);
        }

        if (RechargeCardFragment.tv_userBalanceAmount != null && RechargeCardFragment.tv_lastUpdated != null) {
            RechargeCardFragment.tv_lastUpdated.setText(lastUpdated);
            RechargeCardFragment.tv_userBalanceAmount.setText(amount);
        }

    }

    public static MaterialCheckBox mCircle;
    public static ImageView iv_tick;

    public static void drawCircle(MaterialCheckBox circle, ImageView tick) {
        mCircle = circle;
        mCircle.setRotation(30);
        iv_tick = tick;
        CircleAngleAnimation animation = new CircleAngleAnimation(mCircle, -340);
        animation.setDuration(1000);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

//                mCircle.setChecked(true);
                AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
                alphaAnimation.setDuration(500);
                alphaAnimation.setFillAfter(true);
                alphaAnimation.setFillEnabled(true);
                iv_tick.setVisibility(View.VISIBLE);
                iv_tick.startAnimation(alphaAnimation);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        mCircle.startAnimation(animation);
    }

    public static int dip2px(Context context, float dpValue) {
//        if(mDensity == -1) {
        try {
            Float mDensity = context.getResources().getDisplayMetrics().density;
//        }
            return (int) (dpValue * mDensity + 0.5f);
        } catch (Exception ex) {
            return (int) (dpValue * 3 + 0.5f);
        }
    }

    public static void setTextViewWithNull(TextView viewToSet, String textToSet) {
        if (viewToSet == null) {
            return;
        } else {
            if (textToSet != null && textToSet.length() > 0) {
                viewToSet.setText(textToSet);
                viewToSet.clearFocus();
            } else {
                viewToSet.setText("-");
                viewToSet.clearFocus();
            }
        }
    }

    public static void setStatusBarTransparent(Activity activity) {
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
//            return;
//        }
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
//            activity.getWindow().setStatusBarColor(Color.TRANSPARENT);
//        } else {
//            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//        }
//
//        ViewGroup rootView = (ViewGroup) ((ViewGroup) activity.findViewById(android.R.id.content)).getChildAt(0);
//        rootView.setFitsSystemWindows(true);
//        rootView.setClipToPadding(true);
//
//        ViewGroup contentView = (ViewGroup) activity.findViewById(android.R.id.content);
//        if (contentView.getChildCount() > 1) {
//            contentView.removeViewAt(1);
//        }
//
//        int res = activity.getResources().getIdentifier("status_bar_height", "dimen", "android");
//        int height = 0;
//        if (res != 0)
//            height = activity.getResources().getDimensionPixelSize(res);
//
//        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) contentView.getLayoutParams();
//        lp.setMargins(lp.getMarginStart(), -height, lp.getMarginEnd(), 0);
//        View view = contentView.findViewById(R.id.headerLayoutID);
//        if (view != null) {
//            view.setPadding(0, height, 0, 0);
//        }h
    }

    public static void resetStatusBar(Activity activity) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            return;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            activity.getWindow().setStatusBarColor(ContextCompat.getColor(activity, R.color.colorAccent));
        } else {
            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        ViewGroup rootView = (ViewGroup) ((ViewGroup) activity.findViewById(android.R.id.content)).getChildAt(0);
        rootView.setFitsSystemWindows(true);
        rootView.setClipToPadding(true);

        ViewGroup contentView = (ViewGroup) activity.findViewById(android.R.id.content);
        if (contentView.getChildCount() > 1) {
            contentView.removeViewAt(1);
        }
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) contentView.getLayoutParams();
        lp.setMargins(lp.getMarginStart(), 0, lp.getMarginEnd(), 0);
        View view = contentView.findViewById(R.id.headerLayoutID);
        if (view != null) {
            view.setPadding(0, 0, 0, 0);
        }
    }

    public static void logFirebaseEvent(FirebaseAnalytics mFirebaseAnalytics, String tag) {
        Bundle bundle = new Bundle();
//
        if (BuildConfig.FLAVOR.equalsIgnoreCase("prod") && BuildConfig.BUILD_TYPE.equalsIgnoreCase("release")) {
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, tag);
            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, tag);
        }
        mFirebaseAnalytics.logEvent(tag, bundle);

        AppEventsLogger logger = AppEventsLogger.newLogger(IndyApplication.context);
        logger.logEvent(tag);
    }

    public static void logFirebaseEvent(FirebaseAnalytics mFirebaseAnalytics, String tag, Bundle bundle) {
        if (bundle == null)
            bundle = new Bundle();

        mFirebaseAnalytics.logEvent(tag, bundle);
        AppEventsLogger logger = AppEventsLogger.newLogger(IndyApplication.context);
        logger.logEvent(tag);
    }

    public static double calculateDistanceBetween(LatLng startPoint, LatLng endPoint) {
        double distance = 0;
        float[] results = new float[1];
        Location.distanceBetween(startPoint.latitude, startPoint.longitude, endPoint.latitude, endPoint.longitude, results);
        distance = (double) results[0];
        distance /= 1000;//result in meteres

        return distance;
    }

    public static String getTwoDecimalPointsValueFromDouble(double val) {
        try {
            DecimalFormat df = new DecimalFormat("#.##");
            df.setRoundingMode(RoundingMode.CEILING);
            return df.format(val);
        } catch (Exception ex) {
            return val + "";
        }
    }

    public static void logException(Exception ex) {
        if (ex != null) {
            Crashlytics.logException(ex);
            if (ex instanceof IllegalStateException) {

            } else {
//                Crashlytics.r(ex);
            }

            ex.printStackTrace();
        }
    }

    //return application version code
    public static int getApplicationVersionCode(Context mContext) {
        int versionCode = 1;
        try {
            PackageManager manager = mContext.getPackageManager();
            PackageInfo info = manager.getPackageInfo(
                    mContext.getPackageName(), 0);
            versionCode = info.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionCode;
    }


    public static Bitmap getFilterAppliedBitmap(Bitmap sourceBitmap, String color, Context context) {
        Bitmap bottomImage = sourceBitmap;
        Bitmap mutableBitmap1 = bottomImage.copy(Bitmap.Config.ARGB_8888, true);
        Bitmap topImage;
        if (color.equalsIgnoreCase("orange")) {
            topImage = BitmapFactory.decodeResource(context.getResources(),
                    R.drawable.filter_final);
        } else// (color.equalsIgnoreCase("purple"))
        {
            topImage = BitmapFactory.decodeResource(context.getResources(),
                    R.drawable.filter_orange);
        }
        Bitmap mutableBitmap2 = topImage.copy(Bitmap.Config.ARGB_8888, true);
        Canvas comboImage = new Canvas(mutableBitmap1);
        // Then draw the second on top of that
        comboImage.drawBitmap(mutableBitmap2, 0f, 0f, null);
        return mutableBitmap1;
    }

    public static Bitmap reduceBitmapAlpha(Bitmap sourceBitmap) {
        // lets create a new empty bitmap
        Bitmap newBitmap = Bitmap.createBitmap(sourceBitmap.getWidth(), sourceBitmap.getHeight(), Bitmap.Config.ARGB_8888);
// create a canvas where we can draw on
        Canvas canvas = new Canvas(newBitmap);
// create a paint instance with alpha
        Paint alphaPaint = new Paint();
        alphaPaint.setAlpha(75);
// now lets draw using alphaPaint instance
        canvas.drawBitmap(sourceBitmap, 0, 0, alphaPaint);
        return newBitmap;
    }

    public static Bitmap getFilterAppliedBitmap(Bitmap sourceBitmap, Bitmap topImage) {
        Bitmap bottomImage = sourceBitmap;
        Bitmap mutableBitmap1 = bottomImage.copy(Bitmap.Config.ARGB_8888, true);
        Bitmap mutableBitmap2 = reduceBitmapAlpha(topImage);
        Canvas comboImage = new Canvas(mutableBitmap1);
        // Then draw the second on top of that
        comboImage.drawBitmap(mutableBitmap2, 0f, 0f, null);
        return mutableBitmap1;
    }


    //compute days difference between today and future date
    public static int getDaysDifference(String dateS) {
        int daysDiff = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); // Set your date format
        Date date; // Get Date String according to date format
        try {
            date = sdf.parse(dateS);
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());
            Date today = calendar.getTime();
            daysDiff = (int) ((date.getTime() - today.getTime()) / (1000 * 60 * 60 * 24));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return daysDiff;
    }

    public static String parseDateWithHours(String date, String language) {
        String returnString = "";
        SimpleDateFormat dateFormatter;
        try {
            Date formattedDate;
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            formattedDate = df.parse(date);
            dateFormatter = new SimpleDateFormat("dd MMMM yyyy", new Locale(language));
            return dateFormatter.format(formattedDate);
        } catch (Exception ex) {
            return returnString;

        }
    }

    public static String parseDateToMonthYear(String endsOnDate, String language) {
        String returnString = "";
        SimpleDateFormat dateFormatter;
        try {
            Date formattedDate;
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            formattedDate = df.parse(endsOnDate);
            Locale locale = new Locale(language);
            dateFormatter = new SimpleDateFormat("MMM yyyy", locale);
            return dateFormatter.format(formattedDate);
        } catch (Exception ex) {
            return returnString;

        }
    }

    public static boolean isMembershipValid(Context context) {
        boolean membershipStatus = true;
        SharedPrefrencesManger sharedPrefrencesManger = new SharedPrefrencesManger(context);

        int status = sharedPrefrencesManger.getUserProfileObject().getMembershipData().getStatus();
        if (status != ConstantUtils.inValidMembership && status != 4) {
            membershipStatus = true;
        } else {
            membershipStatus = false;
        }
        return membershipStatus;
    }

    public static void openExternalWebView(String url, Context context) {
        if (url != null) {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            context.startActivity(i);
        }
    }

    public static List<String> fieldsToCapture = new ArrayList<>();


    public static EmiratesIdDataObject getEmiratesIdObject(List<CardFields> fields) {
        fieldsToCapture = new ArrayList<>();
        fieldsToCapture.add("FullName");
        fieldsToCapture.add("IDNumber");
        fieldsToCapture.add("License");
        fieldsToCapture.add("DateOfBirth");
        fieldsToCapture.add("Gender");
        fieldsToCapture.add("ExpirationDate");
        fieldsToCapture.add("Nationality");
        fieldsToCapture.add("FullNameArabic");

        EmiratesIdDataObject emiratesIdDataObject = new EmiratesIdDataObject();
        for (int i = 0; i < fields.size(); i++) {
            for (int j = 0; j < fieldsToCapture.size(); j++) {
                if (fields.get(i).getName().equalsIgnoreCase(fieldsToCapture.get(j))) {
                    if (j == 0) {
                        emiratesIdDataObject.setFullName(fields.get(i).getText());

                        break;
                    } else if (j == 1) {
                        emiratesIdDataObject.setEmiratesIdNumber(fields.get(i).getText());
                        break;
                    } else if (j == 2) {
                        emiratesIdDataObject.setCardNumber(fields.get(i).getText());
                        break;
                    } else if (j == 3) {
                        emiratesIdDataObject.setDateOfBirth(fields.get(i).getText());
                        break;
                    } else if (j == 4) {
                        emiratesIdDataObject.setGender(fields.get(i).getText());
                        break;
                    } else if (j == 5) {
                        emiratesIdDataObject.setExpiryDate(fields.get(i).getText());
                        break;
                    } else if (j == 6) {
                        emiratesIdDataObject.setNationality(fields.get(i).getText());
                        break;
                    } else if (j == 7) {
                        emiratesIdDataObject.setFullNameArabic(fields.get(i).getText());
                        break;
                    }
                }

            }

        }

        return emiratesIdDataObject;
    }

    public static void getDebitCreditCardScan(Activity mActivity) {


        Intent scanIntent = new Intent(mActivity.getApplicationContext(),
                CreditCardWorkflowActivity.class);
        CreditCardParameters parameters = new CreditCardParameters(mActivity);
        parameters.extraTimeForExpiry = 3000L;

        scanIntent.putExtra(CreditCardWorkflowActivity.CREDIT_CARD_PARAMETERS, parameters);
        mActivity.startActivityForResult(scanIntent, 1001);
    }


    public static void createAppRatingDialog(final Activity mActivty) {
        String rateAppTitle = mActivty.getResources().getString(R.string.rate_title);
        String rateAppMessage = mActivty.getResources().getString(R.string.rate_message);
        AlertDialog dialog = new AlertDialog.Builder(mActivty).setPositiveButton(mActivty.getResources().getString(R.string.rate_positive_button), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt) {
                openAppInPlayStore(mActivty);
//                AppPreferences.getInstance(MainActivity.this.getApplicationContext()).setAppRate(false);
            }
        })
                .setNegativeButton(mActivty.getResources().getString(R.string.rate_negative_button), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt) {
                        paramAnonymousDialogInterface.dismiss();
//                openFeedback(MainActivity.this);
//                AppPreferences.getInstance(MainActivity.this.getApplicationContext()).setAppRate(false);
                    }
                })
//                .setNeutralButton(mActivty.getString(R.string.dialog_ask_later), new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt) {
//                paramAnonymousDialogInterface.dismiss();
//                AppPreferences.getInstance(MainActivity.this.getApplicationContext()).resetLaunchCount();
//            }
//        })
                .setMessage(rateAppMessage).setTitle(rateAppTitle).create();
        dialog.show();
    }


    public static void createAppRatingDialog(final Activity mActivty, final FirebaseAnalytics firebaseAnalytics) {
        final SharedPrefrencesManger sharedPrefrencesManger = new SharedPrefrencesManger(mActivty);
//        sharedPrefrencesManger.setIsAppRatingAllowed(true);
//        sharedPrefrencesManger.setIsAppRated(false);
        if (sharedPrefrencesManger.isAppRatingAllowed() && !sharedPrefrencesManger.isAppRated()) {
            String rateAppTitle = mActivty.getResources().getString(R.string.rate_title);
            String rateAppMessage = mActivty.getResources().getString(R.string.rate_message);
            final Dialog ratingDialog = new Dialog(mActivty);
            ratingDialog.setContentView(R.layout.rating_dialog);
            ((TextView) ratingDialog.findViewById(R.id.tv_rating_title)).setText(mActivty.getString(R.string.rate_title));
            ((TextView) ratingDialog.findViewById(R.id.tv_rating_text)).setText(mActivty.getString(R.string.rate_message));
            ((TextView) ratingDialog.findViewById(R.id.tv_negative_button)).setText(mActivty.getString(R.string.rate_negative_button));
            ((TextView) ratingDialog.findViewById(R.id.tv_negative_button)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ratingDialog.dismiss();
                    sharedPrefrencesManger.setIsAppRated(true);
                    logFirebaseEvent(firebaseAnalytics, ConstantUtils.TAGGING_cancel_rate);
                }
            });

            ((TextView) ratingDialog.findViewById(R.id.tv_positive_button)).setText(mActivty.getString(R.string.rate_positive_button));
            ((TextView) ratingDialog.findViewById(R.id.tv_positive_button)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ratingDialog.dismiss();

                    openAppInPlayStore(mActivty);
                    sharedPrefrencesManger.setIsAppRated(true);
                    logFirebaseEvent(firebaseAnalytics, ConstantUtils.TAGGING_rate_now);

                }
            });
            ratingDialog.show();
//            AlertDialog dialog = new AlertDialog.Builder(mActivty).setPositiveButton(mActivty.getResources().getString(R.string.rate_positive_button), new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt) {
//
////                AppPreferences.getInstance(MainActivity.this.getApplicationContext()).setAppRate(false);
//                }
//            })
//                    .setNegativeButton(mActivty.getResources().getString(R.string.rate_negative_button), new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt) {
//                            paramAnonymousDialogInterface.dismiss();
////                openFeedback(MainActivity.this);
////                AppPreferences.getInstance(MainActivity.this.getApplicationContext()).setAppRate(false);
//                        }
//                    })
////                .setNeutralButton(mActivty.getString(R.string.dialog_ask_later), new DialogInterface.OnClickListener() {
////            public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt) {
////                paramAnonymousDialogInterface.dismiss();
////                AppPreferences.getInstance(MainActivity.this.getApplicationContext()).resetLaunchCount();
////            }
////        })
//                    .setMessage(rateAppMessage).setTitle(rateAppTitle).create();
//            dialog.show();
        }
    }

    private static void openAppInPlayStore(Context paramContext) {
        paramContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.indy")));
    }

    public static int storeLatestVersion(String versionNameString) {
        String[] versionNames = versionNameString.split(",");
        int storeVersionLatest = 0;// = new ArrayList<>();
        for (String versionName : versionNames) {
            int tempVersion = Integer.parseInt(versionName);
            if (tempVersion > storeVersionLatest) {
                storeVersionLatest = tempVersion;
            }
        }
        return storeVersionLatest;
    }

    public static String getUDIDForLogging(Context context, String token) {
        String udid = new Date().getTime() + "";
        udid += "_" + token;
        return udid;
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public static void logCompletedRegistrationEvent(String registrationMethod) {
        Bundle params = new Bundle();
        params.putString(AppEventsConstants.EVENT_PARAM_REGISTRATION_METHOD, registrationMethod);
        AppEventsLogger logger = AppEventsLogger.newLogger(IndyApplication.context);
        logger.logEvent(AppEventsConstants.EVENT_NAME_COMPLETED_REGISTRATION, params);
    }

    public static void logAddedPaymentInfoEvent() {
        Bundle params = new Bundle();
        params.putInt(AppEventsConstants.EVENT_PARAM_SUCCESS, 1);
        AppEventsLogger logger = AppEventsLogger.newLogger(IndyApplication.context);
        logger.logEvent(AppEventsConstants.EVENT_NAME_ADDED_PAYMENT_INFO, params);
        Log.d("FBAddedPaymentInfo()", "Called!");
    }

    public static void logPurchaseEvent(double selectedAmount) {
        Bundle params = new Bundle();
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, "product");
        params.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, "AED");
        AppEventsLogger logger = AppEventsLogger.newLogger(IndyApplication.context);
        logger.logEvent("logPurchase", selectedAmount, params);
        Log.d("FBPurchaseEvent()", "" + selectedAmount);
    }

    public static void logContentPurchaseEvent(String contentId, double selectedAmount) {
        Bundle params = new Bundle();
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_ID, contentId);
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, "product");
        params.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, "AED");
        AppEventsLogger logger = AppEventsLogger.newLogger(IndyApplication.context);
        logger.logEvent("logPurchase", selectedAmount, params);
        Log.d("FBPurchaseEvent", "" + selectedAmount);
    }

    public static void logViewedContentEvent(String contentId, double price) {
        Bundle params = new Bundle();
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, "product");
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_ID, contentId);
        params.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, "AED");
        AppEventsLogger logger = AppEventsLogger.newLogger(IndyApplication.context);
        logger.logEvent(AppEventsConstants.EVENT_NAME_VIEWED_CONTENT, price, params);
        Log.d("FBViewedContentEvent()", "" + contentId);
    }

    public static String parseDateWithHoursMonthInFront(String date, String language) {
        String returnString = "";
        SimpleDateFormat dateFormatter;
        try {
            Date formattedDate;
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            formattedDate = df.parse(date);
            if (language.equalsIgnoreCase(ConstantUtils.lang_english)) {
                dateFormatter = new SimpleDateFormat("MMMM dd, yyyy", new Locale(language));
            } else {
                dateFormatter = new SimpleDateFormat("dd MMMM, yyyy", new Locale(language));

            }
            return dateFormatter.format(formattedDate);
        } catch (Exception ex) {
            return returnString;

        }
    }

}
