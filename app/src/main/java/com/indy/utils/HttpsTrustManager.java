package com.indy.utils;

import android.content.Context;

import com.indy.controls.MyEndPointsInterface;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

/**
 * Created by Eabdelhamid on 1/21/2016.
 */
public class HttpsTrustManager implements X509TrustManager {

    public static HttpsTrustManager httpsTrustManagerInstance;
    private static TrustManager[] trustManagers;
    public static final X509Certificate[] _AcceptedIssuers = new X509Certificate[1];
    private final Context mContext;

    public HttpsTrustManager(Context mContext) {
        this.mContext = mContext;
    }

    public static HttpsTrustManager getHttpsTrustManagerInstance(Context mContext) {
        if (httpsTrustManagerInstance != null) {
            httpsTrustManagerInstance = new HttpsTrustManager(mContext);
            return httpsTrustManagerInstance;
        } else {
            return httpsTrustManagerInstance;
        }
    }

    public static TrustManagerFactory tmf;

    public void addTrustedCertificate() {
        try {
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            InputStream caInput;
             if(MyEndPointsInterface.baseUrl.contains("uat")){
                caInput  = new BufferedInputStream(IndyApplication.context.getAssets().open("uat_swypapp.etisalat.ae.crt"));
            }else{
                caInput  = new BufferedInputStream(IndyApplication.context.getAssets().open("prod-swypapp.etisalat.ae1.crt"));
            }

            Certificate ca;
            try {
                ca = cf.generateCertificate(caInput);
                _AcceptedIssuers[0] = (X509Certificate) ca;
                System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
            } finally {
                caInput.close();
            }

// Create a KeyStore containing our trusted CAs
            String keyStoreType = KeyStore.getDefaultType();
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(null, null);
            keyStore.setCertificateEntry("ca", ca);

// Create a TrustManager that trusts the CAs in our KeyStore
            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();

            tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);

// Create an SSLContext that uses our TrustManager
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, tmf.getTrustManagers(), null);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void checkClientTrusted(
            X509Certificate[] x509Certificates, String s)
            throws java.security.cert.CertificateException {

    }

    @Override
    public void checkServerTrusted(
            X509Certificate[] x509Certificates, String s)
            throws java.security.cert.CertificateException {
    }

    public boolean isClientTrusted(X509Certificate[] chain) {
        if (chain != null && _AcceptedIssuers != null) {
            for (int i = 0; i < chain.length; i++) {
                for (int j = 0; j < _AcceptedIssuers.length; i++) {
                    if (chain[i].equals(_AcceptedIssuers[j])) {
                        return true;
                    }
                }
            }

        }
        return false;
    }

    public boolean isServerTrusted(X509Certificate[] chain) {
        return true;
    }

    @Override
    public X509Certificate[] getAcceptedIssuers() {
        return _AcceptedIssuers;
    }

    public SSLSocketFactory allowAllSSL() {
        addTrustedCertificate();
        SSLContext context = null;
        try {
            context = SSLContext.getInstance("TLS");
            context.init(null, tmf.getTrustManagers(), new SecureRandom());
//            HttpsURLConnection.setDefaultSSLSocketFactory(context
//                    .getSocketFactory());
            return context.getSocketFactory();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return HttpsURLConnection.getDefaultSSLSocketFactory();
        } catch (KeyManagementException e) {
            e.printStackTrace();
            return HttpsURLConnection.getDefaultSSLSocketFactory();
        } catch (Exception ex) {
            ex.printStackTrace();
            return HttpsURLConnection.getDefaultSSLSocketFactory();
        }

    }

}
