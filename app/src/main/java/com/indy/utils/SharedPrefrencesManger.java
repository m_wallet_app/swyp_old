package com.indy.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.indy.R;
import com.indy.helpers.DateHelpers;
import com.indy.helpers.TripleDESEncryption;
import com.indy.models.login.UserProfile;

/**
 * Created by emad on 8/17/16.
 */
public class SharedPrefrencesManger {
    private Context mContext;
    private SharedPreferences sharedPrefs;
    private SharedPreferences.Editor prefsEditor;
    private TripleDESEncryption tripleDESEncryption;

    public SharedPrefrencesManger(Context context) {
        mContext = context;
        sharedPrefs = context.getSharedPreferences(mContext.getString(R.string.APP_SHARED_PREFS), Activity.MODE_PRIVATE);
        prefsEditor = sharedPrefs.edit();
        try {
            String indy = context.getPackageName();
            indy = indy.replace(".", "");
//            tripleDESEncryption = new TripleDESEncryption(getToken());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setLanguage(String lang) {
        prefsEditor.putString(ConstantUtils.language, lang);
        prefsEditor.commit();
    }

    public String getLanguage() {
        return sharedPrefs.getString(ConstantUtils.language, "");
    }


    public void setPassword(String password) {
        prefsEditor.putString(ConstantUtils.userPassword, password);
        prefsEditor.commit();
    }

    public String getPassword() {
        return sharedPrefs.getString(ConstantUtils.userPassword, "");
    }

    public void setAppVersion(String appVersion) {
        prefsEditor.putString(ConstantUtils.appVersion, appVersion);
        prefsEditor.commit();
    }

    public String getAppVersion() {
        return sharedPrefs.getString(ConstantUtils.appVersion, "1.0");
    }

    public void setToken(String token) {
        prefsEditor.putString(ConstantUtils.token, token);
        prefsEditor.commit();
    }

    public void setAuthToken(String authToken) {

        prefsEditor.putString(ConstantUtils.headerValue, authToken);
//        prefsEditor.putString(ConstantUtils.headerValue, tripleDESEncryption.encrypt(authToken));

        prefsEditor.commit();
    }

    public String getAuthToken() {
        return sharedPrefs.getString(ConstantUtils.headerValue, "");
//        return tripleDESEncryption.decrypt(sharedPrefs.getString(ConstantUtils.headerValue, ""));
    }

    public void setRegisterationID(String registerationID) {
        prefsEditor.putString(ConstantUtils.registerationID, registerationID);
        prefsEditor.commit();
    }

    public void setMembershipPrice(String membershipPrice) {
        prefsEditor.putString(ConstantUtils.membershipPrice, membershipPrice);
        prefsEditor.commit();
    }

    public String getMembershipPrice() {
        return sharedPrefs.getString(ConstantUtils.membershipPrice, "50");
    }

    public void setNewMembershipPrice(String newMembershipPrice) {
        prefsEditor.putString(ConstantUtils.newMembershipPrice, newMembershipPrice);
        prefsEditor.commit();
    }

    public String getNewMembershipPrice() {
        return sharedPrefs.getString(ConstantUtils.newMembershipPrice, "50");
    }

    public void setSIMPrice(String simPrice) {
        prefsEditor.putString(ConstantUtils.simPrice, simPrice);
        prefsEditor.commit();
    }

    public String getSIMPrice() {
        return sharedPrefs.getString(ConstantUtils.simPrice, "0");
    }

    public void setAfterLoginPrice(String simPrice) {
        prefsEditor.putString(ConstantUtils.afterLoginPrice, simPrice);
        prefsEditor.commit();
    }

    public String getAfterLoginPrice() {
        return sharedPrefs.getString(ConstantUtils.afterLoginPrice, "0");
    }

    public void setTransactionID(String transactionID) {
        prefsEditor.putString(ConstantUtils.transactionID, transactionID);
        prefsEditor.commit();
    }

    public String getTransactionID() {
        return sharedPrefs.getString(ConstantUtils.transactionID, "token");
    }

    public String getLastStatus() {
        return sharedPrefs.getString(ConstantUtils.lastTrackingStatus, "");
    }

    public void setLastStatus(String lastStatus) {
        prefsEditor.putString(ConstantUtils.lastTrackingStatus, lastStatus);
        prefsEditor.commit();
    }

    public void setIsFiristTime(boolean isFiristTime) {
        prefsEditor.putBoolean(ConstantUtils.isFiristTime, isFiristTime);

        prefsEditor.commit();
    }

    public boolean isFiristTimeLaunch() {
        return sharedPrefs.getBoolean(ConstantUtils.isFiristTime, true);

    }


    public void setIsTermsAccepted(boolean isAccepted) {
        prefsEditor.putBoolean(ConstantUtils.TERMS_ACCEPTED, isAccepted);

        prefsEditor.commit();
    }

    public boolean isTermsAccepted() {
        return sharedPrefs.getBoolean(ConstantUtils.TERMS_ACCEPTED, false);

    }

    public void enableBalanceTransfer(boolean enable) {
        prefsEditor.putBoolean(ConstantUtils.transfer_balance_enabled, enable);
        prefsEditor.commit();
    }

    public boolean getBalanceTransfarerStatuse() {
        return sharedPrefs.getBoolean(ConstantUtils.transfer_balance_enabled, true);
    }


    public void enableRewards(boolean enable) {
        prefsEditor.putBoolean(ConstantUtils.rewards_enabled, enable);
        prefsEditor.commit();
    }

    public boolean getRewardsStatuse() {
        return sharedPrefs.getBoolean(ConstantUtils.rewards_enabled, true);
    }


    public void enableStore(boolean enable) {
        prefsEditor.putBoolean(ConstantUtils.store_enabled, enable);
        prefsEditor.commit();
    }

    public boolean getStoreStaus() {
        return sharedPrefs.getBoolean(ConstantUtils.store_enabled, true);
    }


    public void enableLiveChat(boolean enable) {
        prefsEditor.putBoolean(ConstantUtils.live_cahat_enabled, enable);
        prefsEditor.commit();
    }

    public boolean getLiveChatStaus() {
        return sharedPrefs.getBoolean(ConstantUtils.live_cahat_enabled, true);
    }

    public void enableLUsage(boolean enable) {
        prefsEditor.putBoolean(ConstantUtils.usage_enabled, enable);
        prefsEditor.commit();
    }

    public boolean getUsageStaus() {
        return sharedPrefs.getBoolean(ConstantUtils.usage_enabled, true);
    }

    public String getRegisterationID() {
        return sharedPrefs.getString(ConstantUtils.registerationID, "");
    }


    public String getToken() {
        return sharedPrefs.getString(ConstantUtils.token, null);
    }

    public void setDeviceId(String deviceId) {
        prefsEditor.putString(ConstantUtils.deviceId, deviceId);
        prefsEditor.commit();
    }

    public String getDeviceId() {
        return sharedPrefs.getString(ConstantUtils.deviceId, "deviceId");
    }

    public void setFullName(String deviceId) {
        prefsEditor.putString(ConstantUtils.fullName, deviceId);
        prefsEditor.commit();
    }

    public String getFullName() {
        return sharedPrefs.getString(ConstantUtils.fullName, "");
    }

    public void setMobileNo(String deviceId) {
        prefsEditor.putString(ConstantUtils.mobileNo, deviceId);
        prefsEditor.commit();
    }

    public String getMobileNo() {
        return sharedPrefs.getString(ConstantUtils.mobileNo, "");
    }

    public void setInvitation(String deviceId) {
        prefsEditor.putString(ConstantUtils.invitationCode, deviceId);
        prefsEditor.commit();
    }

    public String getInvitaion() {
        return sharedPrefs.getString(ConstantUtils.invitationCode, "");
    }


    public void setEmail(String deviceId) {
        prefsEditor.putString(ConstantUtils.email, deviceId);
        prefsEditor.commit();
    }

    public String getEmail() {
        return sharedPrefs.getString(ConstantUtils.email, "");
    }

    public void setNickName(String deviceId) {
        prefsEditor.putString(ConstantUtils.nickName, deviceId);
        prefsEditor.commit();
    }

    public String getNickName() {
        return sharedPrefs.getString(ConstantUtils.nickName, "");
    }

    public void setGender(String deviceId) {
        prefsEditor.putString(ConstantUtils.gender, deviceId);
        prefsEditor.commit();
    }

    public String getGender() {
        return sharedPrefs.getString(ConstantUtils.gender, "");
    }

    public void setUserProfileImage(String deviceId) {
        prefsEditor.putString(ConstantUtils.userProfileImage, deviceId);
        prefsEditor.commit();
    }

    public String getUserProfileImage() {
        return sharedPrefs.getString(ConstantUtils.userProfileImage, "");
    }

    public void setApplicationUtils(String locale) {
        prefsEditor.putString(ConstantUtils.fullName, locale);
        prefsEditor.commit();

    }

    public String getUsedActivity() {
        return sharedPrefs.getString(ConstantUtils.RewardsUsed, "");
    }

    public void setUsedActivity(String locale) {
        prefsEditor.putString(ConstantUtils.RewardsUsed, locale);
        prefsEditor.commit();

    }

    public void clearSharedPref() {
        prefsEditor.remove(ConstantUtils.fullName).commit();
        prefsEditor.remove(ConstantUtils.mobileNo).commit();
        prefsEditor.remove(ConstantUtils.invitationCode).commit();
        prefsEditor.remove(ConstantUtils.email).commit();
        prefsEditor.remove(ConstantUtils.nickName).commit();
        prefsEditor.remove(ConstantUtils.gender).commit();
        prefsEditor.remove(ConstantUtils.userProfileImage).commit();
        prefsEditor.remove(ConstantUtils.fullName).commit();
        prefsEditor.remove(ConstantUtils.RewardsUsed).commit();
        prefsEditor.remove(ConstantUtils.rewards_enabled).commit();
        prefsEditor.remove(ConstantUtils.transfer_balance_enabled).commit();
        prefsEditor.remove(ConstantUtils.store_enabled).commit();
        prefsEditor.remove(ConstantUtils.live_cahat_enabled).commit();
        prefsEditor.remove(ConstantUtils.usage_enabled).commit();
        prefsEditor.remove(ConstantUtils.headerValue).commit();
        prefsEditor.remove(ConstantUtils.esimBitmapCached).commit();

    }

    public void setUserProfileObj(UserProfile userProfileObj) {
        Gson gson = new Gson();
        String json = gson.toJson(userProfileObj);
        prefsEditor.putString(ConstantUtils.loginoutputmodel, json);
        prefsEditor.commit();
    }

    public UserProfile getUserProfileObject() {
        Gson gson = new Gson();
        String json = sharedPrefs.getString(ConstantUtils.loginoutputmodel, "");
        UserProfile userProfile = gson.fromJson(json, UserProfile.class);
        return userProfile;
    }
//    public String getApplicationLanguage() {
//        return sharedPrefs.getString(ConstantUtils.app_language, "en");
//    }


    public void setUserBalance(String balance) {
        prefsEditor.putString(ConstantUtils.userBalance, balance);
        prefsEditor.commit();
    }

    public String getUserBalance() {
        return sharedPrefs.getString(ConstantUtils.userBalance, "");
    }


    public void setLastUpdated(String lasetUpdated) {
        prefsEditor.putString(ConstantUtils.lastUpdatedValue, lasetUpdated);
        prefsEditor.commit();
    }

    public String getLastUpdatedBalance() {
        return DateHelpers.lastUpdated(sharedPrefs.getString(ConstantUtils.lastUpdatedValue, "0"));
    }

    public void setTimerValue(long newValue) {
        prefsEditor.putLong(ConstantUtils.timer_value_for_new_number, newValue);
        prefsEditor.commit();
    }

    public long getTimerValue() {
        return sharedPrefs.getLong(ConstantUtils.timer_value_for_new_number, 0l);
    }

    public void setUserId(String userId) {
        prefsEditor.putString(ConstantUtils.userId, userId);
        prefsEditor.commit();
    }

    public String getUserId() {
        return sharedPrefs.getString(ConstantUtils.userId, "");
    }

    public void setUserSessionId(String userSessionId) {
        prefsEditor.putString(ConstantUtils.userSessionId, userSessionId);
        prefsEditor.commit();
    }

    public String getUserSessionId() {
        return sharedPrefs.getString(ConstantUtils.userSessionId, "");
    }

    public String getTwitterAuthToken() {
        return sharedPrefs.getString(ConstantUtils.TWITTER_AUTH_TOKEN, "");
    }

    public String getTwitterAuthSecret() {
        return sharedPrefs.getString(ConstantUtils.TWITTER_AUTH_SECRET, "");
    }

    public void setTwitterAuthToken(String authToken) {
        prefsEditor.putString(ConstantUtils.TWITTER_AUTH_TOKEN, authToken);
        prefsEditor.commit();
    }

    public void setTwitterAuthSecret(String authSecret) {
        prefsEditor.putString(ConstantUtils.TWITTER_AUTH_SECRET, authSecret);
        prefsEditor.commit();
    }

    public void setNotificationIdForPopup(String id) {
        prefsEditor.putString(ConstantUtils.KEY_NOTIFICATION_ID_FOR_SERVICE, id);
        prefsEditor.commit();
    }

    public String getNotificationIdForPopup() {
        return sharedPrefs.getString(ConstantUtils.KEY_NOTIFICATION_ID_FOR_SERVICE, "");
    }

    public void setTwitterUsername(String id) {
        prefsEditor.putString(ConstantUtils.KEY_TWITTER_USERNAME, id);
        prefsEditor.commit();
    }

    public String getTwitterUsername() {
        return sharedPrefs.getString(ConstantUtils.KEY_TWITTER_USERNAME, "");
    }

    public void setFacebookUsername(String id) {
        prefsEditor.putString(ConstantUtils.KEY_FACEBOOK_USERNAME, id);
        prefsEditor.commit();
    }

    public String getFacebookUsername() {
        return sharedPrefs.getString(ConstantUtils.KEY_FACEBOOK_USERNAME, "");
    }

    public void setNavigationIndex(String id) {
        prefsEditor.putString(ConstantUtils.CHALLENGES_TAB_NAVIGATION_INDEX, id);
        prefsEditor.commit();
    }


    public String getNavigationBadgeIndex() {
        return sharedPrefs.getString(ConstantUtils.CHALLENGES_TAB_NAVIGATION_INDEX_BADGE, "");
    }

    public void setNavigationBadgeIndex(String id) {
        prefsEditor.putString(ConstantUtils.CHALLENGES_TAB_NAVIGATION_INDEX_BADGE, id);
        prefsEditor.commit();
    }


    public String getNavigationIndex() {
        return sharedPrefs.getString(ConstantUtils.CHALLENGES_TAB_NAVIGATION_INDEX, "");
    }

    public void setVATEnabled(boolean enable) {
        prefsEditor.putBoolean(ConstantUtils.VAT_ENABLED, enable);
        prefsEditor.commit();
    }

    public boolean isVATEnabled() {
        return sharedPrefs.getBoolean(ConstantUtils.VAT_ENABLED, true);

    }

    public void setIsAppRatingAllowed(boolean isAppRatingAllowed) {
        prefsEditor.putBoolean(ConstantUtils.IS_APP_RATING_ALLOWED, isAppRatingAllowed);

        prefsEditor.commit();
    }

    public boolean isAppRatingAllowed() {
        return sharedPrefs.getBoolean(ConstantUtils.IS_APP_RATING_ALLOWED, false);

    }

    public void setLatestStoreVersion(int latestStoreVersion) {
        prefsEditor.putInt(ConstantUtils.latest_store_version, latestStoreVersion);

        prefsEditor.commit();
    }


    public void setNotificationTypeForPopup(String id) {
        prefsEditor.putString(ConstantUtils.KEY_NOTIFICATION_TYPE_FOR_SERVICE, id);
        prefsEditor.apply();
    }

    public String getNotificationTypeForPopup() {
        return sharedPrefs.getString(ConstantUtils.KEY_NOTIFICATION_TYPE_FOR_SERVICE, "");
    }

    public void setItemId(String id) {
        prefsEditor.putString(ConstantUtils.KEY_NOTIFICATION_ITEM_ID, id);
        prefsEditor.commit();
    }

    public String getItemId() {
        return sharedPrefs.getString(ConstantUtils.KEY_NOTIFICATION_ITEM_ID, "");
    }

    public String getTempAppId() {
        return sharedPrefs.getString(ConstantUtils.KEY_APP_LOGGING_ID, "");

    }

    public void setTempAppId(String appId) {
        prefsEditor.putString(ConstantUtils.KEY_APP_LOGGING_ID, appId);
        prefsEditor.commit();
    }

    public boolean isAppRated() {
        return sharedPrefs.getBoolean(ConstantUtils.IS_APP_RATED, false);

    }

    public void setIsFromMigration(boolean isFromMigration) {
        prefsEditor.putBoolean(ConstantUtils.IS_FROM_MIGRATION, isFromMigration);

        prefsEditor.commit();
    }

    public boolean isFromMigration() {
        return sharedPrefs.getBoolean(ConstantUtils.IS_FROM_MIGRATION, false);

    }

    public void setIsAppRated(boolean isAppRated) {
        prefsEditor.putBoolean(ConstantUtils.IS_APP_RATED, isAppRated);

        prefsEditor.commit();
    }

    public void setLatestStoreVersion(float latestStoreVersion) {
        prefsEditor.putFloat(ConstantUtils.latest_store_version, latestStoreVersion);

        prefsEditor.commit();
    }

    public float getLatestStoreVersion() {
        return sharedPrefs.getInt(ConstantUtils.latest_store_version, 0);

    }

    public boolean isGamificationEnabled() {
        return sharedPrefs.getBoolean(ConstantUtils.IS_GAMIFICATION_ENABLED, true);

    }

    public void setGamificationEnabled(boolean isGamificationEnabled) {
        prefsEditor.putBoolean(ConstantUtils.IS_GAMIFICATION_ENABLED, isGamificationEnabled);

        prefsEditor.commit();
    }

    public boolean isEventsEnabled() {
        return sharedPrefs.getBoolean(ConstantUtils.IS_EVENTS_ENABLED, false);
    }

    public void setEventsEnabled(boolean isGamificationEnabled) {
        prefsEditor.putBoolean(ConstantUtils.IS_EVENTS_ENABLED, isGamificationEnabled);
        prefsEditor.commit();
    }

    public void setESIMBitmapCachedFlag(boolean isBitmapCached) {
        prefsEditor.putBoolean("isBitmapCached", isBitmapCached);
        prefsEditor.commit();
    }

    public boolean getESIMBitmapCachedFlag() {
        return sharedPrefs.getBoolean("isBitmapCached", false);
    }


    public void setLIVPopupDateTime(long LIVPopupDateTime) {
        prefsEditor.putLong("LIVPopupDateTime", LIVPopupDateTime);
        prefsEditor.commit();
    }

    public long getLIVPopupDateTime() {
        return sharedPrefs.getLong("LIVPopupDateTime", 0);
    }

}