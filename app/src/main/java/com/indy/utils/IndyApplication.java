package com.indy.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.multidex.MultiDexApplication;
import android.util.Log;
import android.webkit.WebView;

import com.adjust.sdk.Adjust;
import com.adjust.sdk.AdjustConfig;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.indy.BuildConfig;
import com.indy.R;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.twitter.sdk.android.core.Twitter;

import java.util.Locale;

import io.fabric.sdk.android.Fabric;

/**
 * Created by emad on 5/16/16.
 */
public class IndyApplication extends MultiDexApplication {
    public SharedPrefrencesManger sharedPrefrencesManger;
    public static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
//        Fabric.with(this, new Crashlytics());
        try {
            sharedPrefrencesManger = new SharedPrefrencesManger(getApplicationContext());
            String language = "";
            LocalizationUtils.mContext = getApplicationContext();
            try {
                new WebView(this).destroy();
            } catch (Exception ex) {
                if (ex.getMessage() != null && !ex.getMessage().contains("webview")) {
                    //no logging
                }
            }
            context = getApplicationContext();

            Twitter.initialize(getApplicationContext());
            language = sharedPrefrencesManger.getLanguage();
            if (language.isEmpty()) {
                language = Locale.getDefault().getLanguage();
                if (language.equalsIgnoreCase("ar"))
                    sharedPrefrencesManger.setLanguage(language);
                else
                    sharedPrefrencesManger.setLanguage("en");
            }
            LocalizationUtils.switchLocalizaion(language, null);
            getAppVersion();
            initImageLoader();
            // Set up Crashlytics, disabled for debug builds
//            Crashlytics crashlyticsKit = new Crashlytics.Builder()
//                    .core(new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build())
//                    .build();

// Initialize Fabric with the debug-disabled crashlytics.
//            Fabric.with(this, crashlyticsKit);
            Fabric.with(this, new Crashlytics());
//        FirebaseCrash.setCrashCollectionEnabled(false);
            String appToken = "v6qwin29rfgg";
            String environment = BuildConfig.DEBUG ? AdjustConfig.ENVIRONMENT_SANDBOX : AdjustConfig.ENVIRONMENT_PRODUCTION;
            AdjustConfig config = new AdjustConfig(this, appToken, environment);
            Adjust.onCreate(config);

            registerActivityLifecycleCallbacks(new AdjustLifecycleCallbacks());
            FacebookSdk.setApplicationId(getString(R.string.facebook_app_id));
            FacebookSdk.sdkInitialize(getApplicationContext(), new FacebookSdk.InitializeCallback() {
                @Override
                public void onInitialized() {
                    Log.d("Indy:Facebook", "initialized");
                }
            });
            AppEventsLogger.activateApp(this);
        }catch (Exception ex){

        }
    }

    public SharedPrefrencesManger getSharedPrefrencesManger() {
        return sharedPrefrencesManger;
    }

    public void setSharedPrefrencesManger(SharedPrefrencesManger sharedPrefrencesManger) {
        this.sharedPrefrencesManger = sharedPrefrencesManger;
    }

    public void getAppVersion() {
        PackageManager manager = getApplicationContext().getPackageManager();
        PackageInfo info = null;
        try {


            info = manager.getPackageInfo(
                    getApplicationContext().getPackageName(), 0);
            String version = info.versionName;
            sharedPrefrencesManger.setAppVersion(version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }


    public static String getAppVersionCode(Context mContext) {
        PackageManager manager = mContext.getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(
                    mContext.getPackageName(), 0);
            String version = info.versionCode + "";
            return version;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "1";
        }
    }

    private void initImageLoader() {
        com.nostra13.universalimageloader.core.ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(getApplicationContext()));
    }


    private static final class AdjustLifecycleCallbacks implements ActivityLifecycleCallbacks {

        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

        }

        @Override
        public void onActivityStarted(Activity activity) {

        }

        @Override
        public void onActivityResumed(Activity activity) {
            Adjust.onResume();
        }

        @Override
        public void onActivityPaused(Activity activity) {
            Adjust.onPause();
        }

        @Override
        public void onActivityStopped(Activity activity) {

        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

        }

        @Override
        public void onActivityDestroyed(Activity activity) {

        }
    }

}
