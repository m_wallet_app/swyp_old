package com.indy.utils;

import android.graphics.Color;

import com.google.android.gms.maps.model.LatLng;
import com.indy.BuildConfig;

/**
 * Created by emad on 8/17/16.
 */
public interface ConstantUtils {
    String arLanguage = "ARLANGUAGE";
    String enLanguage = "ENLANGUAGE";
    String language = "LANGUAGE";
    String userPassword = "USERPASSWORD";
    String paymentLocation = "payment";
    String shopLocation = "store";
    String arLng = "ar";
    String enLng = "en";
    String appVersion = "APPVERSION";
    String token = "TOKEN";
    String registerationID = "REGISTERATIONID";
    String membershipPrice = "membershipPrice";
    String newMembershipPrice = "newMembershipPrice";
    String simPrice = "simPrice";
    String afterLoginPrice = "afterLoginPrice";
    String transactionID = "TRANSACTIONID";
    String isFiristTime = "ISFIRISTTIME";
    String shops_findus = "SHOPS";
    String payment_findus = "PAYMENT";
    String wifi_hotspots = "WIFI";

    String faqs_url = "REG_FAQ";
    String live_chat_url = "chat";
    String app_language = "app_language";
    String lang_english = "en";
    String lang_arabic = "ar";
    String osVersion = "2";
    String channel = "INDYAPP";
    String deviceId = "DEVICEID";
    String userName = "USERNAME";
    String fullName = "USERNAME";
    String loginoutputmodel = "LOGINOUTPUTMODEL";
    String mobileNO = "MOBILENO";
    String email = "EMAIL";
    String nickName = "NICKNAME";
    String gender = "GENDER";
    String birthdate = "BIRTHDATE";
    String newAccountMobileNo = "NEWACCOUNTMOBILENUMBER";
    String codeNO = "CODENUMBER";
    String mobileNo = "MOBILENO";
    String userBalance = "BALANCE";
    String lastUpdated = "LASTUPDATED";

    String invitationCode = "INVITATIONCODE";
    int inValidMembership = 1;
    int validMembership = 2;
    String eidNO = "EIDNO";
    String fragmentType = "FRAGMENTTYPE";
    String changePasswordActivity = "CHANGEPASSWORDACTIVITY";
    String registerationActivity = "REGISTERATIONACTIVITY";
    String errorString = "error_string";
    String errorTitle = "error_title";
    String itemsIdList = "itemsIdList";
    String loadingString = "loadingString";
    String buttonTitle = "btn_title";
    String settingsButtonEnabled = "settings_btn_enabled";
    String password = "password";
    String userProfile = "USERPROFILE";
    String userProfileImage = "USERPROFILEIMAGE";
    String item_USED_reward_view_pager = "USEDREWARDITEM";

    String store_enabled = "STOREENABLED";
    String rewards_enabled = "REWARDSENABLED";
    String usage_enabled = "USAGEENABLED";
    String transfer_balance_enabled = "TRANSFEREBALANCEENABLED";
    String live_cahat_enabled = "LIVECHATENABLED";
    String share_significant = "share_android";
    String invite_significant = "invite_android";
    String vat_significant = "display_vat";
    String vat_significant_v2 = "display_vat_v2";
    String migration_significant = "is_enabled_migration";
    int NUMBER_OF_DIGITS_IN_CODE = 4;

    String master_error_response = "master_error";
    String Sms_Verify_OTP = "OTP";
    String item_reward_view_pager = "REWARDITEM";

//    String imgBaseUrl = "http://194.170.234.17:7003";

    String RewardsDetails = "reward_details";
    String RewardsUsed = "reward_used";
    String RechargeOrderType = "recharge";
    String isMembershipValid = "isMembership";
    String headerValue = "Etisalat-AuthToken";
    String forgotPasswordService = "FORGOTPASSWORDSERVICE";
    String cahngePasswordService = "CHANGEPASSWORDSERVICE";
    String validateOtp = "VALIDATEOTPSERVICE";
    String esimBitmapCached = "isBitmapCached";
    String numberMigrationService = "NUMBERMIGRATIONSERVICE";
    String is_migration_enabled = "is_enabled_migration";
    String oldPreferredNumber = "preferred_number";
    String showBackBtn = "SHOWBACKBTN";
    String isFromCreateNewAccount = "ISFROMCREATENEWACCOUNT";

    int unAuthorizeToken = 401;
    String lastUpdatedValue = "lastUpdated";
    String indy_status_tag = "INDY-STATUS";
    String indy_status_failure = "FAILURE";
    String timer_value_for_new_number = "NEW_NUMBER_TIMER";
    int number_of_locations = 100;
    int timeout_new_number = 30;
    String failure_string = "indy_fail_failure";
    String success_string = "indy_pass_success";
    String CREATE_ACCOUNT_SERVICE = "create_account_service";

    String KEY_INVITE_FRIEND_SERVICE = "indy.msg.content.msg.if";

    String updateLanguage = "updateLanguage";
    String logoutService = "logoutService";

    String TAGGING_LOGIN = "login_page";
    String TAGGING_HAMBURGER = "hamburger_menu";
    String TAGGING_RECHARGE = "recharge_menu";
    String TAGGING_REWARDS = "rewards_menu";
    String TAGGING_USAGE = "usage_menu";
    String TAGGING_WIFI = "wifi_page";
    String TAGGING_STORE = "store_page";


    String TERMS_ACCEPTED = "terms_accepted";


    String TAGGING_open_the_app = "open_the_app";
    String TAGGING_view_package_signup_button = "view_package_signup_button";
    String TAGGING_skip_tutorial_1 = "skip_tutorial_1";
    String TAGGING_skip_tutorial_2 = "skip_tutorial_2";
    String TAGGING_skip_tutorial_3 = "skip_tutorial_3";
    String TAGGING_view_packages_tutorial = "view_packages_tutorial";
    String TAGGING_welcome_terms_and_conditions = "welcome_terms_and_conditions";
    String TAGGING_welcome_agree_t_and_c = "welcome_agree_t_and_c";
    String TAGGING_change_language = "change_language";
    String TAGGING_buy_now = "buy_now";
    String TAGGING_login_button = "login_button";
    String TAGGING_continue_signup = "continue_signup";
    String TAGGING_check_nearest_shop = "check_nearest_shop";
    String TAGGING_Nearest_shop_takemethere = "Nearest_shop_takemethere";
    String TAGGING_Support_button = "Support_button";
    //    String TAGGING_Live_chat = "Live_chat";
//    String TAGGING_find_us = "find_us";
//    String TAGGING_faqs = "faqs";
    String TAGGING_name_email_entered = "name_email_entered";
    String TAGGING_dob_entered = "dob_entered";
    String TAGGING_onboarding_t_and_c_agreed = "onboarding_t_and_c_agreed";
    String TAGGING_confirm_age_validations = "confirm_age_validations";
    String TAGGING_confirm_age_validations_canceled = "confirm_age_validations_canceled";
    String TAGGING_onboarding_select_number = "onboarding_select_number";
    String TAGGING_onboarding_refresh_number_list = "refresh_number_list";
    String TAGGING_package_selected_sim_membership = "package_selected_sim_membership";
    String TAGGING_package_selected_simonly = "package_selected_simonly";
    String TAGGING_selected_standard_delivery = "selected_standard_delivery";
    String TAGGING_delivery_info_entered = "delivery_info_entered";
    String TAGGING_delivery_city_selected = "delivery_city_selected";
    String TAGGING_delivery_info_invalid_number = "delivery_info_invalid_number";
    String TAGGING_delivery_info_enter_address = "delivery_info_enter_address";
    String TAGGING_delivery_info_longpress_address = "delivery_info_longpress_address";
    String TAGGING_delivery_info_eid_entered = "delivery_info_eid_entered";
    String TAGGING_onboarding_invalid_eid_number = "onboarding_invalid_eid_number";
    String TAGGING_delivery_info_order_checkout = "delivery_info_order_checkout";
    String TAGGING_delivery_info_failure = "delivery_info_failure";
    String TAGGING_delivery_info_success = "delivery_info_success";
    String TAGGING_payment_success_viewpackages = "payment_success_viewpackages";
    String TAGGING_delivery_info_success_livechat = "delivery_info_success_livechat";


    String TAGGING_migration_selected = "migration_selected";
    String TAGGING_migration_number_entered = "migration_number_entered";
    String TAGGING_migration_eid_entered = "migration_eid_entered";
    String TAGGING_migration_invalid_age_eid_entered = "migration_invalid_age_eid_entered";
    String TAGGING_migration_valid_eid_entered = "migration_valid_eid_entered";
    String TAGGING_migration_invalid_eid_number = "migration_invalid_eid_entered";
    String TAGGING_migration_invalid_number_entered = "migration_invalid_number_entered";
    String TAGGING_migration_valid_number_entered = "migration_valid_number_entered";
    String TAGGING_migration_confirmed = "migration_confirmed";
    String TAGGING_migration_sim_order = "migration_sim_order";
    String TAGGING_review_details = "review_details";


    String TAGGING_create_account_selected = "create_account_selected";
    String TAGGING_create_account_enter_valid_number = "create_acount_enter_valid_number";
    String TAGGING_create_account_enter_invalid_number = "create_acount_enter_wrong_number";
    String TAGGING_create_account_validation_code_entered = "create_account_otp_entered";
    String TAGGING_create_account_validation_code_valid = "create_account_otp_valid";
    String TAGGING_create_account_validation_code_invalid = "create_account_otp_invalid";
    String TAGGING_create_account_accept_terms = "create_account_accept_terms";
    String TAGGING_create_account_password_creation = "create_account_password_creation";
    String TAGGING_create_account_enter_invite_code = "create_account_enter_invite_code";
    String TAGGING_create_account_success_page = "create_account_success_page";
    String TAGGING_create_account_swyp_login = "create_account_swyp_login";
    String TAGGGING_create_account_invalid_password = "create_account_invalid_password";
    String TAGGING_create_account_invalid_invite_code = "create_account_wrong_invite_code";

    String TAGGING_forgot_password = "forgot_password";
    String TAGGING_forgot_password_enter_number = "forgot_password_enter_number";
    String TAGGING_forgot_password_validation_code_entered = "forgot_password_otp_entered";
    String TAGGING_forgot_password_validation_code_valid = "forgot_password_otp_valid";
    String TAGGING_forgot_password_validation_code_invalid = "forgot_password_otp_invalid";
    String TAGGING_forgot_password_changed_success = "forgot_password_changed_success";


    String TAGGING_menu_opened = "menu_opened";
    String TAGGING_menu_closed = "menu_closed";
    String TAGGING_edit_profile_button = "edit_profile_button";
    String TAGGING_store_button = "store_button";
    String TAGGING_rewards_button = "rewards_button";
    String TAGGING_usage_button = "usage_button";
    String TAGGING_share_button = "share_button";
    String TAGGING_invite_friend_button = "invite_friend_button";
    String TAGGING_settings_button = "settings_button";


    String TAGGING_settings_sign_out = "settings_sign_out";
    String TAGGING_settings_about_swyp = "settings_about_swyp";
    String TAGGING_settings_about_terms_conditions = "settings_about_terms_conditions";
    String TAGGING_settings_change_language = "settings_change_language";

    String TAGGING_settings_sign_out_failure = "settings_sign_out_failure";
    String TAGGING_settings_change_language_failure = "settings_change_language_failure";


    String TAGGING_edit_profile_change_nickname = "edit_profile_change_nickname";
    String TAGGING_edit_profile_change_password = "edit_profile_change_password";
    String TAGGING_edit_profile_forgot_password = "edit_profile_forgot_password";
    String TAGGING_edit_profile_set_new_password = "edit_profile_set_new_password";
    String TAGGING_edit_profile_change_gender = "edit_profile_change_gender";
    String TAGGING_edit_profile_change_picture_delete = "edit_profile_picture_delete";
    String TAGGING_edit_profile_change_picture_save = "edit_profile_change_picture_save";
    String TAGGING_edit_profile_save = "edit_profile_save";
    String TAGGING_edit_profile_save_failure = "edit_profile_save_failure";
    String TAGGING_edit_profile_change_password_failure = "edit_profile_change_password_failure";


    String TAGGING_usage_detail = "usage_detail";
    String TAGGING_usage_shop_more = "usage_shop_more";
    String TAGGING_usage_invite_friend = "usage_invite_friend";
    String TAGGING_usage_pay_per_use = "usage_pay_per_use";
    String TAGGING_usage_transaction_history = "usage_transaction_history";
    String TAGGING_usage_wifi_hotspots = "usage_wifi_hotspots";
    String TAGGING_usage_search_wifi_hotspots = "usage_search_wifi_hotspots";
    String TAGGING_usage_click_marker_hotspots = "usage_click_marker_hotspots";
    String TAGGING_usage_logout_from_hotspots = "usage_logout_from_hotspots";
    String TAGGING_usage_membership_failed = "usage_membership_failed";
    String TAGGING_usage_cancel_logout_from_hotspots = "usage_cancel_logout_from_hotspots";


    String TAGGING_reward_detail_opened = "reward_detail_opened";
    String TAGGING_reward_filter_applied = "reward_filter_applied";
    String TAGGING_reward_map_opened = "reward_map_opened";
    String TAGGING_reward_map_pin_clicked = "reward_map_pin_clicked";
    String TAGGING_reward_reward_detail_opened_from_pin = "reward_detail_opened_from_pin";
    String TAGGING_reward_use_voucher_button_clicked = "reward_usevoucher_button_clicked";
    String TAGGING_reward_redeem_now_button_clicked = "reward_redeem_now_button_clicked";
    String TAGGING_reward_filter_cleared = "reward_filter_cleared";
    String TAGGING_reward_voucher_redeem_success = "reward_voucher_redeem_success";
    String TAGGING_reward_used_rewards_clicked = "reward_used_rewards_clicked";
    String TAGGING_reward_available_rewards_clicked = "reward_available_rewards_clicked";
    String TAGGING_reward_voucher_redeem_failure = "reward_voucher_redeem_failure";
    String TAGGING_reward_redeem_voucher = "reward_redeem_voucher";

    String TAGGING_rewards_no_membership = "rewards_no_membership";

    String TAGGING_support_live_chat = "support_live_chat";
    String TAGGING_support_faqs = "support_faqs";
    String TAGGING_support_find_us = "support_find_us";


    String TAGGING_support_find_us_shops_tab = "support_find_us_shops_tab";
    String TAGGING_support_find_us_payment_machines_tab = "support_find_us_payment_machines_tab";
    String TAGGING_support_find_us_wifi_hotspots_tab = "support_find_us_wifi_hotspots_tab";


    String TAGGING_support_wifi_hotspots = "support_wifi_hotspots";
    String TAGGING_support_wifi_hotspots_search = "support_wifi_hotspots_search";
    String TAGGING_support_wifi_hotspots_logout_from_hotspots = "support_logout_from_hotspots";
    String TAGGING_support_wifi_hotspots_confirm_logout_from_hotspots = "support_confirm_logout_hotspots";
    String TAGGING_support_wifi_hotspots_click_marker = "support_wifihotspot_click_marker";
    String TAGGING_support_wifi_hotspots_position_me = "support_wifi_hotspots_position_me";
    String TAGGING_support_payment_machines_search = "support_payment_machines_search";
    String TAGGING_support_payment_machines_click_marker = "support_payment_click_marker";
    String TAGGING_support_payment_machines_position_me = "support_payment_position_me";
    String TAGGING_support_shops_machines_search = "support_shops_machines_search";
    String TAGGING_support_shops_click_marker = "support_shops_click_marker";
    String TAGGING_support_shops_position_me = "support_shops_position_me";
    String TAGGING_support_open = "support_open";


    String TAGGING_recharge_crerdit_card = "recharge_credit_card";
    String TAGGING_recharge_card_buton = "recharge_card_button";
    String TAGGING_recharge_credit_card_select_amount = "recharge_creditcard_selectamount";
    String TAGGING_recharge_credit_card_enter_amount = "recharge_credit_card_enter_amount";
    String TAGGING_recharge_credit_card_pay = "recharge_credit_card_pay";
    String TAGGING_recharge_credit_card_payment_success = "recharge_credit_card_payment_success";
    String TAGGING_recharge_card_enter_card_number = "recharge_card_enter_card_number";
    String TAGGING_recharge_card_recharge_button = "recharge_card_recharge_button";
    String TAGGING_recharge_card_success = "recharge_card_success";
    String TAGGING_recharge_credit_card_payment_failure = "recharge_credit_card_payment_failure";
    String TAGGING_recharge_card_failure = "recharge_card_failure";


    String TAGGING_store_add_item_button = "store_add_item_button";
    String TAGGING_store_show_more = "store_show_more";
    String TAGGING_store_swyp_to_checkout = "store_swyp_to_checkout";
    String TAGGING_store_item_plus = "store_item_plus";
    String TAGGING_store_item_minus = "store_item_minus";
    String TAGGING_store_shop_more = "store_shop_more";
    String TAGGING_store_confirm_purchase_button = "store_confirm_purchase_button";
    String TAGGING_store_final_purchase_confirmation = "store_final_purchase_confirmation";
    String TAGGING_store_cancel_final_purchase = "store_cancel_final_purchase";
    String TAGGING_store_recharge_account_button = "store_recharge_account_button";
    String TAGGING_store_transfer_button = "store_transfer_button";
    String TAGGING_store_history_button = "store_history_button";
    String TAGGING_store_purchase_success = "store_purchase_success";
    String TAGGING_store_purchase_failure = "store_purchase_failure";


    String TAGGING_transfer_select_number = "transfer_select_number";
    String TAGGING_transfer_request_user_for_credit = "transfer_request_user_for_credit";
    String TAGGING_transfer_request_user_for_transfer = "transfer_request_user_for_transfer";
    String TAGGING_transfer_view_alerts = "transfer_view_alerts";
    String TAGGING_transfer_accept_received_request = "transfer_accept_received_request";
    String TAGGING_transfer_reject_received_request = "transfer_reject_received_request";
    String TAGGING_transfer_edit_received_request = "transfer_edit_received_request";
    String TAGGING_transfer_delete_sent_request = "transfer_delete_sent_request";
    String TAGGING_transfer_send_reminder = "transfer_send_reminder";
    String TAGGING_transfer_confirm_request_sending = "transfer_confirm_request_sending";
    String TAGGING_transfer_confirm_transfer_sending = "transfer_confirm_transfer_sending";
    String TAGGING_transfer_confirm_accepting_request = "transfer_confirm_accepting_request";

    String TAGGING_transfer_confirm_delete_sent_request = "transfer_confirm_delete_sent_request";
    String TAGGING_transfer_request_tab = "transfer_request_tab";
    String TAGGING_transfer_tab = "transfer_tab";
    String TAGGING_transfer_cancel_sending_balance = "transfer_cancel_sending_balance";
    String TAGGING_transfer_eligibility_failure = "transfer_eligibility_failure";
    String TAGGING_transfer_cancel_request = "transfer_cancel_request";

    String TAGGING_get_sim = "Get_SIM";
    String TAGGING_on_boarding_select_number = "On_boarding_select_number";
    String TAGGING_package_selected = "Package_selected";
    String TAGGING_id_scan_front = "ID_scan_front";
    String TAGGING_id_scan_back = "ID_scan_back";
    String TAGGING_eid_number_verify = "EID_number";
    String TAGGING_tell_us_how_you_want_to_pay_screen = "Tell_us_how_you_want_to_pay_screen";
    String TAGGING_map = "Map";
    String TAGGING_order_placed_successfully = "Order_placed_successfully";
    String TAGGING_enter_details = "Enter_details";
    String TAGGING_confirm_details = "Confirm_details";
    String TAGGING_manual_eid_entry = "Manual_eid_entry";
    String TAGGING_manual_eid_verify = "Manual_eid_verify";
    String TAGGING_manual_details = "Manual_details";
    String TAGGING_manual_confirm_details = "Manual_confirm_details";
    String TAGGING_let_us_know_enter_details  = "Let_us_know_enter_details";
    String TAGGING_let_us_know_details_successful  = "Let_us_know_details_successful";
    String TAGGING_onboarding_terms_and_conditions_agreed  = "onboarding_terms_and_conditions_agreed";


    int zoomLevel = 12;
    int zoomLevelWithoutLocation = 7;

    int zoomLevelWithoutLocationPaymentMachine = 6;
    float sizeOfCircleAnimation = 60;
    float sizeOfTotalView = 100;
    float sizeOfLeftStart = (sizeOfTotalView - sizeOfCircleAnimation) / 2;

    float ALPHA_BUTTON_ENABLED = 1f;
    float ALPHA_BUTTON_DISABLED = 0.5f;

    String TAGGING_incorrect_email_entererd = "incorrect_email_entererd";
    String TAGGING_reward_list_opened = "reward_list_opened";
    String welcome_termsandconditions = "welcome_termsandconditions";
    String REG_TERMSANDCONDITIONS = "REG_TERMSANDCONDITIONS";
    String lastTrackingStatus = "lastTrackingStatus";
    String AUTHORITY = BuildConfig.APPLICATION_ID + ".provider";
    String SIGNIFICANT_APP_UPDATE = "app_update";

    String soft = "soft";
    String hard = "hard";
    String updateType = "updateType";
    //    String KEY_EVENT_DETAIL = "KEY_EVENT_DETAIL";
    String EVENT_PHOTO_DETAIL = "EVENT_PHOTO_DETAIL";
    String orange_filter = "orange_filter";
    String purple_filter = "purple_filter";
    String item_events_pager = "item_events_pager";
    String SOURCE_IMAGE = "SOURCE_IMAGE";
    String EVENT_PHOTO = "EVENT_PHOTO";
    String EVENT_PHOTO_TITLE = "EVENT_TITLE";
    String EVENT_PHOTO_DESCRIPTION = "EVENT_DESCRIPTION";
    String DUMMY_IMAGE_URL = "https://ibb.co/b2AKPQ";
    String KEY_EVENT_PHOTOS = "KEY_EVENT_PHOTOS";
    String loadingTitle = "loadingTitle";
    String title_checkin_success = "title_checkin_success";
    String subtitle_checkin_success = "subtitle_checkin_success";
    String reward_type_checkin_success = "reward_type_checkin_success";
    String raffle_tickets_won = "raffle_tickets_won";

    String userId = "userId";
    String userSessionId = "userSessionId";

    String INDY_TALOS_ID = "B19B885D-5A48-48D7-A8C8-671A4EFBE4BC";
    String KEY_BADGE_DETAIL = "KEY_BADGE_DETAIL";
    String KEY_RAFFLE_MONTH = "KEY_RAFFLE_MONTH";
    int RAFFLES_INVITE_FRIEND = 401;
    int RAFFLES_STORE = 402;
    int RAFFLES_CHECKIN = 403;
    int RAFFLES_BADGES = 404;
    int RAFFLES_NAVIGATION = 400;
    int RAFFLES_RECHARGE_ACCOUNT = 405;
    String raffle_tickets_total = "raffle_tickets_total";
    String lottery_reward = "lottery_reward";
    String TWITTER_AUTH_TOKEN = "TWITTER_AUTH_TOKEN";
    String TWITTER_AUTH_SECRET = "TWITTER_AUTH_SECRET";
    String TOP_RATED = "TOP_RATED";
    String EVENT_KEY = "EVENT_KEY";
    String UPLOADED_ENTRY_ID = "UPLOADED_ENTRY_ID";
    String FACEBOOK = "FACEBOOK";
    String TWITTER = "TWITTER";

    String NOTIFICATION_ID_FROM_PUSH = "notificationId";
    String CHALLENGES_TAB_NAVIGATION_INDEX = "CHALLENGES_TAB_NAVIGATION_INDEX";
    String CHALLENGES_TAB_NAVIGATION_INDEX_BADGE = "CHALLENGES_TAB_NAVIGATION_INDEX_BADGE";
    String FROM_LOTTERY_DEAL = "FROM_LOTTERY_DEAL";
    String POSITIVE_BUTTON_TEXT = "POSITIVE_BUTTON_TEXT";
    String NEGATIVE_BUTTON_TEXT = "NEGATIVE_BUTTON_TEXT";
    String UPLOAD_SUCCESS_TEXT = "UPLOAD_SUCCESS_TEXT";
    //    String USER_BALANCE = "USER_BALANCE";
    String REWARD_ID = "REWARD_ID";
    String BADGE_TYPE = "badge";
    String IMAGE_TYPE = "image";
    String VIEW_MAP_VISIBLE = "VIEW_MAP_VISIBLE";
    String UPLOAD_REWARD = "UPLOAD_REWARD";
    String IS_OWNER = "IS_OWNER";
    String KEY_TWITTER_USERNAME = "KEY_TWITTER_USERNAME";
    String KEY_FACEBOOK_USERNAME = "KEY_FACEBOOK_USERNAME";

    int NOTIFICATION_REQUEST = 1515;
    String badge_share = "social_share_badge_unlock";
    String social_share = "social_share";
    String LUCKY_DEAL_ID = "LUCKY_DEAL_ID";
    String TAGGING_login_failure = "login_failure";
    String TAGGING_login_success = "login_success";
    //    String item_events_pager = "item_events_pager";
    String KEY_EVENT_DETAIL = "KEY_EVENT_DETAIL";
    String URL_TO_LOAD = "URL_TO_LOAD";
    String SHOW_BUY_NOW = "SHOW_BUY_NOW";
    int SHOW_BUY_NOW_CODE = 1010;
    String USER_BALANCE = "USER_BALANCE";
    String VAT_ENABLED = "VAT_ENABLED";


    String IS_APP_RATED = "IS_APP_RATED";
    String IS_GAMIFICATION_ENABLED = "IS_GAMIFICATION_ENABLED";
    String IS_EVENTS_ENABLED = "IS_EVENTS_ENABLED";
    String IS_APP_RATING_ALLOWED = "IS_APP_RATING_ALLOWED";
    String app_rating_versions = "app_rating_versions";
    String latest_store_version = "latest_store_version";
    String NOTIFICATIONS_ID = "NOTIFICATIONS_ID";
    String notification_type = "notificationType";
    String media_type = "mediaType";
    String media_url = "mediaUrl";
    String KEY_NOTIFICATION_ID_FOR_SERVICE = "KEY_NOTIFICATION_ID_FOR_SERVICE";
    String KEY_NOTIFICATION_TYPE_FOR_SERVICE = "KEY_NOTIFICATION_TYPE_FOR_SERVICE";
    String STORE_NAVIGATION_INDEX = "STORE_NAVIGATION_INDEX";
    String KEY_EVENT_NAVIGATION = "KEY_EVENT_NAVIGATION";
    String KEY_NOTIFICATION_ITEM_ID = "KEY_NOTIFICATION_ITEM_ID";
    String KEY_FROM_NOTIFICATION = "KEY_FROM_NOTIFICATION";
    String KEY_NOTIFICATION_ID = "notificationId";
    String KEY_ITEM_ID = "itemId";
    String KEY_APP_LOGGING_ID = "key_app_logging_id";

    String SCREEN_ID_0000 = "0000";
    String SCREEN_ID_0001 = "0001";
    String SCREEN_ID_0002 = "0002";

    String SCREEN_ID_0003 = "0003";


    String SCREEN_ID_0004 = "0004";
    String SCREEN_ID_0005 = "0005";
    String SCREEN_ID_0006 = "0006";


    String SCREEN_ID_1001 = "1001";
    String SCREEN_ID_1002 = "1002";
    String SCREEN_ID_1003 = "1003";


    String SCREEN_ID_1004 = "1004";
    String SCREEN_ID_1005 = "1005";
    String SCREEN_ID_1006 = "1006";
    String SCREEN_ID_1007 = "1007";
    String SCREEN_ID_3001 = "3001";
    String SCREEN_ID_3002 = "3002";
    String SCREEN_ID_3003 = "3003";
    String SCREEN_ID_3031 = "3031";


    String SCREEN_ID_3032 = "3032";


    String SCREEN_ID_3041 = "3041";
    String SCREEN_ID_3042 = "3042";

    String SCREEN_ID_3005 = "3005";

    String SCREEN_ID_3006 = "3006";


    String KEY_SCREEN_ID = "KEY_SCREEN_ID";
    String APP_LOG_STATUS_IN_PROGRESS = "In-progress";
    String APP_LOG_STATUS_FAILED = "Failed";
    String APP_LOG_STATUS_SUCCESSFUL = "Succesful";
    String TAGGING_rate_now = "rate_now";
    String TAGGING_cancel_rate = "cancel_rate";

    String TAGGING_CASH_ON_DELIVERY_SELECTED = "onboarding_cash_on_delivery";
    String TAGGING_onboarding_credit_card = "onboarding_credit_card";
    String TAGGING_what_is_swyp = "what_is_swyp";
    String TAGGING_SUCCESSFUL_ORDER = "order_placed_successfully";

    String TAGGING_onboarding_error_invalid_age = "onboarding_error_invalid_age";
    String TAGGING_onboarding_error_existing_user = "onboarding_error_existing_user";
    String TAGGING_onboarding_error_payment_fail = "onboarding_error_payment_fail";
    String TAGGING_onboarding_error_eid_failure = "onboarding_error_eid_failure";
    String TAGGING_onboarding_error_cod_order_fail = "onboarding_error_cod_order_fail";
    String EMIRATES_ID = "EMIRATES_ID";
    int LOG_SHARE_EVENT = 232;
    String gamification_enabled = "gamification_enabled";
    String KEY_BADGE_CATEGORY = "KEY_BADGE_CATEGORY";
    String BADGE_CATEGORY_SHARE = "BADGE_CATEGORY_SHARE";
    String BADGE_CATEGORY_CHECK_IN = "BADGE_CATEGORY_SHARE";
    String BADGE_CATEGORY_CONTEST_PARTICIPATION = "BADGE_CATEGORY_SHARE";
    String BADGE_CATEGORY_PACKAGE_PURCHASE = "BADGE_CATEGORY_SHARE";
    String KEY_PROMO_CODE_DESCRIPTION = "KEY_PROMO_CODE_DESCRIPTION";
    String KEY_PROMO_CODE_TYPE = "KEY_PROMO_CODE_TYPE";
    String IS_FROM_MIGRATION = "IS_FROM_MIGRATION";
    String FIREBASE_TOKEN_KEY = "firebase_token";
    String FIREBASE_TOKEN = "c6Z26aFoJio:APA91bGpMaZ5OIH3S-nIb3M80a_enDmxDiRdmu5zGtxgffd6s87ATPZUEoyorac12BlprSF5gLjSxbQ8swoyemGaoOTLqpDbch0J_-yrlqs_sqADJ4Mv4XPsFYCOnL3NKaR_tALP6Bze";

//  TODO: 5/14/2018 to comment in production
//  String KOFAX_LICENSE = "Klm?^5J05@nE&B!#z8l[8[(U;Ot'^bxt0dfvLIq(6bvAk,I!!g54^VW4vdcFfPbNL6f7^!r!Fhz#[9cj!U0PfNn@rlkU=dGrv,8N";
//  TODO: 5/14/2018 un comment in production
    String KOFAX_LICENSE = "v,jfFjq^4hU^x4L&n[EN!GU80!O$7P=Kl!AD`0!nvdt!!,g,I2z?Bz`Zz[tj[Id9N8Pz0mll5rkfbWUDk6dc4&lB;vtc,(5lj4rJ";
    String ACCOUNT_NUMBER = "Account Number";
    LatLng DUBAI_LAT_LONG = new LatLng(25.187992, 55.279707);
//  LatLng DUBAI_LAT_LONG = new LatLng(24.326190, 54.753607);

    public static final String KEY_DL_GET_SIM = "GetSim";
    String KEY_IS_DEEP_LINK_URI = "IS_DEEP_LINK_URI";
    String KEY_DEEP_LINK_SCREEN_NAME = "DEEP_LINK_SCREEN_NAME";
    String KEY_DEEP_LINK_URI_ITEM_ID = "DEEP_LINK_URI_ITEM_ID";
    String KEY_PROMO_CODE = "promoCode";
}
