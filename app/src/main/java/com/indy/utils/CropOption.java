package com.indy.utils;

import android.content.Intent;
import android.graphics.drawable.Drawable;

/**
 * Created by emad on 9/26/16.
 */

public class CropOption {
    public CharSequence title;
    public Drawable icon;
    public Intent appIntent;
}
