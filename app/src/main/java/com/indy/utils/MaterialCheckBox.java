package com.indy.utils;

import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

import com.indy.R;

/**
 * Created by mobile on 17/04/2017.
 */

public class MaterialCheckBox extends View {

    private Paint paintBlue;
    private Paint paintWithe;
    private Paint paintCenter;

    private int borderColor = Color.GRAY;
    private int backgroundColor = Color.BLUE;
    private int doneShapeColor = Color.WHITE;

    private int baseWidth;                    //checkbox
    private int borderWidth;
    private int width, height;

    private float[] points = new float[8];

    private int DURATION = 200;
    private boolean checked;
    private float correctProgress;

    private boolean drawRecting;
    private boolean isAnim;
    private OnCheckedChangeListener listener;

    private float padding;



    private static Paint paint;
    private int screenW, screenH;
    private float X, Y;
    private Path path;
    private float initialScreenW;
    private float initialX, plusX;
    private float TX;
    private boolean translate;
    private int flash;
    private Context context;

    float heightVal = dip2px(ConstantUtils.sizeOfCircleAnimation), widthVal = dip2px(ConstantUtils.sizeOfCircleAnimation);
    float leftTop = dip2px(ConstantUtils.sizeOfLeftStart);
    float top = dip2px(ConstantUtils.sizeOfLeftStart);
    float right = leftTop + widthVal;
    float bottom = top + heightVal;

    public MaterialCheckBox(Context context) {
        this(context, null);
    }

    public MaterialCheckBox(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MaterialCheckBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public MaterialCheckBox(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    /**
     * init
     *
     * @param context
     */
    float mDensity = -1;
    public float dip2px(float dpValue) {
        mDensity = getResources().getDisplayMetrics().density;

        return dpValue * mDensity + 0.5f;
    }
    private void init(Context context) {

        backgroundColor = getResources().getColor(R.color.color_white);
        borderColor = getResources().getColor(R.color.color_white);
        borderWidth = baseWidth = dp2px(2);

        paintBlue = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintBlue.setColor(borderColor);
        paintBlue.setStrokeWidth(borderWidth);

        paintWithe = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintWithe.setColor(doneShapeColor);
        paintWithe.setStrokeWidth(dp2px(2));

        paintCenter = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintCenter.setStyle(Paint.Style.STROKE);
        paintCenter.setColor(getResources().getColor(R.color.color_white));
        paintCenter.setStrokeWidth(borderWidth);

        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setChecked(!isChecked());
            }
        });
        drawRecting = true;
//        heightVal = getHeight();
//        widthVal  = getWidth();
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        height = width = Math.max(w, h);

        points[0] = 101 / 378f * width;
        points[1] = 0.5f * width;

        points[2] = 163 / 378f * width;
        points[3] = 251 / 378f * width;

        points[4] = 149 / 378f * width;
        points[5] = 250 / 378f * width;

        points[6] = 278 / 378f * width;
        points[7] = 122 / 378f * width;

        padding = 57 / 378f * width;

    }
    public float oldAngle = 0, newAngle = 0;

    public float getOldAngle() {
        return oldAngle;
    }

    public void setOldAngle(float oldAngle) {
        this.oldAngle = oldAngle;
    }

    public float getNewAngle() {
        return newAngle;
    }

    public void setNewAngle(float newAngle) {
        this.newAngle = newAngle;
    }

    /**
     * draw checkbox
     *
     * @param canvas
     */
    boolean firstDraw = false, secondDraw = false;
    @Override
    protected void onDraw(Canvas canvas) {
        RectF rect = new RectF(leftTop, top, right,bottom);
//        canvas.rotate(30);

        canvas.drawArc(rect,oldAngle,newAngle,false,paintCenter);
        firstDraw = true;
        float startX1 = leftTop + 23;
        float startY1 = top + 60;
        float endX1 = startX1 + 25;
        float endY1 = startY1 + 45;

        float endX2 = endX1 + 30;
        float endY2 = endY1 - 60;
//        canvas.drawRoundRect(rect, baseWidth, baseWidth, paintBlue);
        if (drawRecting) {
//            canvas.drawRect(padding + borderWidth, padding + borderWidth, width - padding - borderWidth, height - padding - borderWidth, paintCenter);
//            canvas.drawCircle(350,350,400,paintCenter);
//            canvas.rotate(20);
             canvas.drawArc(rect,oldAngle,newAngle,false,paintCenter);
            secondDraw = true;

        } else {

//            if (correctProgress > 0) {
//                if (correctProgress < 1 / 3f) {
////                    float x = points[0] + (points[2] - points[0]) * correctProgress;
////                    float y = points[1] + (points[3] - points[1]) * correctProgress;
//                    canvas.drawLine(startX1, startY1, endX1, endY1, paintBlue);
//
//                } else {
////                    float x = points[4] + (points[6] - points[4]) * correctProgress;
////                    float y = points[5] + (points[7] - points[5]) * correctProgress;
////                    canvas.drawLine(points[0], points[1], points[2], points[3], paintBlue);
////                    canvas.drawLine(points[4], points[5], x, y, paintBlue);
//                    canvas.drawLine(startX1, startY1, endX1, endY1, paintBlue);
//
//                    canvas.drawLine(endX1, endY1, endX2, endY2, paintBlue);
//                    canvas.drawLine(endX1, endY1, endX1+1, endY1+1, paintBlue);
//
//
//                }
//            }
        }
    }


    public void setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public void setDoneShapeColor(int doneShapeColor) {
        this.doneShapeColor = doneShapeColor;
        paintWithe.setColor(doneShapeColor);
    }

    public void setBorderColor(int borderColor) {
        this.borderColor = borderColor;
    }

    public void setBorderWidth(int baseWidth) {
        this.baseWidth = baseWidth;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
        if (checked) {
            showRect();
        } else {
            hideCorrect();
        }
    }

    private void hideRect() {
        if (isAnim) {
            return;
        }
        isAnim = true;
        drawRecting = true;
        ValueAnimator va = ValueAnimator.ofFloat(0, 1).setDuration(DURATION);
        va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float p = (float) animation.getAnimatedValue();
                float c = 1f - p;
                borderWidth = (int) (baseWidth + c * (width - baseWidth));
                paintBlue.setColor(evaluate(c, borderColor, backgroundColor));
                invalidate();
                if (p >= 1) {
                    isAnim = false;
                    if (listener != null) {
                        checked = false;
                        listener.onCheckedChanged(MaterialCheckBox.this, checked);
                    }
                }
            }
        });
        va.start();
    }

    private void showRect() {
        if (isAnim) {
            return;
        }
        isAnim = true;
        drawRecting = true;
        ValueAnimator va = ValueAnimator.ofFloat(0, 1).setDuration(DURATION);
        va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float p = (float) animation.getAnimatedValue();
                borderWidth = (int) (10 + p * (width - 10));
                paintBlue.setColor(evaluate(p, borderColor, backgroundColor));
                invalidate();
                if (p >= 1) {
                    isAnim = false;
                    drawRecting = false;
                    showCorrect();
                }
            }
        });
        va.start();
    }

    private void showCorrect() {
        if (isAnim) {
            return;
        }
        isAnim = true;
        correctProgress = 0;
        drawRecting = false;
        ValueAnimator va = ValueAnimator.ofFloat(0, 1).setDuration(DURATION);
        va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                correctProgress = (float) animation.getAnimatedValue();
                invalidate();
                if (correctProgress >= 1) {
                    isAnim = false;
                    if (listener != null) {
                        checked = true;
                        listener.onCheckedChanged(MaterialCheckBox.this, checked);
                    }
                }
            }
        });
        va.start();
    }

    private void hideCorrect() {
        if (isAnim) {
            return;
        }
        isAnim = true;
        correctProgress = 1;
        drawRecting = false;
        ValueAnimator va = ValueAnimator.ofFloat(0, 1).setDuration(DURATION);
        va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float p = (float) animation.getAnimatedValue();
                correctProgress = 1f - p;
                invalidate();
                if (p >= 1) {
                    isAnim = false;
                    hideRect();
                }
            }
        });
        va.start();
    }

    public void setOnCheckedChangedListener(OnCheckedChangeListener listener) {
        this.listener = listener;
    }

    interface OnCheckedChangeListener {
        void onCheckedChanged(View view, boolean isChecked);
    }

    private int evaluate(float fraction, int startValue, int endValue) {
        int startInt = startValue;
        int startA = (startInt >> 24) & 0xff;
        int startR = (startInt >> 16) & 0xff;
        int startG = (startInt >> 8) & 0xff;
        int startB = startInt & 0xff;

        int endInt = endValue;
        int endA = (endInt >> 24) & 0xff;
        int endR = (endInt >> 16) & 0xff;
        int endG = (endInt >> 8) & 0xff;
        int endB = endInt & 0xff;
        return ((startA + (int) (fraction * (endA - startA))) << 24)
                | ((startR + (int) (fraction * (endR - startR))) << 16)
                | ((startG + (int) (fraction * (endG - startG))) << 8)
                | ((startB + (int) (fraction * (endB - startB))));
    }

    public int dp2px(float value) {
        final float scale = getContext().getResources().getDisplayMetrics().densityDpi;
        return (int) (value * (scale / 160) + 0.5f);
    }
}