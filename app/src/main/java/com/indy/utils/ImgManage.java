package com.indy.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class ImgManage {

    private Activity mActivity;
    public final static int request_permission_for_camera = 10;
    int permissionCheck;
    public String mCurrentPhotoPath;
    double reqImageSize;
    public static ImgManage imageMangeInstance;
    public final int PIC_FROM_CAMERA = 0;
    public final int PIC_FROM_Gallary = 1;

    public ImgManage(Activity activity) {
        this.mActivity = activity;
        setPermissionCheck();
        imageMangeInstance = this;
    }


    private void setPermissionCheck() {
        permissionCheck = ContextCompat.checkSelfPermission(mActivity,
                Manifest.permission.CAMERA);
        permissionCheck += ContextCompat.checkSelfPermission(mActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        permissionCheck += ContextCompat.checkSelfPermission(mActivity, Manifest.permission.READ_EXTERNAL_STORAGE);
    }

    public void getImg(int way) {
        setPermissionCheck();
        if (Build.VERSION.SDK_INT >= 23) {
            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                checkPermission();
            } else {
                switch (way) {

                    case PIC_FROM_CAMERA:


                        openCamera();

                        break;
                    case PIC_FROM_Gallary:
                        openAlbum();
                        break;
                }
            }
        } else {
            switch (way) {

                case PIC_FROM_CAMERA:


                    openCamera();

                    break;
                case PIC_FROM_Gallary:
                    openAlbum();
                    break;
            }
        }
    }

    public void openCamera() {
        try {
            dispatchTakePictureIntent();
        } catch (Exception e) {
            e.printStackTrace();


        }

    }

    public void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(mActivity.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                //TODO
                Log.w("CreateImgFile", ex.getMessage());
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(this.mActivity, ConstantUtils.AUTHORITY, photoFile));
                mActivity.startActivityForResult(takePictureIntent, PIC_FROM_CAMERA);
            }
        }

    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        Log.w("imageDecoded CreateFile", mCurrentPhotoPath);
        return image;
    }

    public void openAlbum() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        mActivity.startActivityForResult(intent, PIC_FROM_Gallary);
    }

    public String analyzePhotoPathFromLocalUri(Uri imageUri) {
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = mActivity.getContentResolver().query(imageUri, filePathColumn, null, null, null);
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String picturePath = cursor.getString(columnIndex);
        cursor.close();
        return picturePath;
    }

    public Bitmap scaleBitmap(Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        } else {
           /* int width = 2000;
            float scale = (float) width / (float) bitmap.getWidth();
            Matrix matrix = new Matrix();
            matrix.postScale(scale, scale);
            return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);*/
            final int REQUIRED_SIZE = 1000;
            int width_tmp = bitmap.getWidth(), height_tmp = bitmap.getHeight();
            int scale = 1;
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
                    break;
                }
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }
            Matrix matrix = new Matrix();
            matrix.postScale(scale, scale);
            return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        }
    }


    public Bitmap decodeUri(Uri selectedImage) {
        try {

            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            try {
                BitmapFactory.decodeStream(
                        mActivity.getContentResolver().openInputStream(selectedImage), null, o);
                final int REQUIRED_SIZE = 500;
                int width_tmp = o.outWidth, height_tmp = o.outHeight;
                int scale = 1;
                while (true) {
                    if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
                        break;
                    }
                    width_tmp /= 2;
                    height_tmp /= 2;
                    scale *= 2;
                }

                BitmapFactory.Options o2 = new BitmapFactory.Options();
                o2.inSampleSize = scale;
                return BitmapFactory.decodeStream(
                        mActivity.getContentResolver().openInputStream(selectedImage), null, o2);
            } catch (FileNotFoundException e) {
                return null;
            }
        } catch (OutOfMemoryError outOfMemoryError) {
            return null;
        }
    }

    public Bitmap rotateBitmap(Bitmap bitmap, Uri uri, int PIC_FROM) {
        String filepath = null;
        if (PIC_FROM == PIC_FROM_Gallary) {
            filepath = analyzePhotoPathFromLocalUri(uri);
        } else if (PIC_FROM == PIC_FROM_CAMERA) {
            filepath = uri.getPath();
        }
        Bitmap rotatedBitmap = null;
        Matrix matrix = new Matrix();
        try {
            ExifInterface exif = new ExifInterface(filepath);
            int orientation = Integer.parseInt(exif.getAttribute(ExifInterface.TAG_ORIENTATION));
            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                matrix.postRotate(90);
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                matrix.postRotate(180);
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                matrix.postRotate(270);
            } else if (orientation == ExifInterface.ORIENTATION_FLIP_HORIZONTAL) {
                matrix.preScale(-1, 1);
            } else if (orientation == ExifInterface.ORIENTATION_FLIP_VERTICAL) {
                matrix.preScale(1, -1);
            }
            try {
                rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true); // rotating bitmap.
            } catch (OutOfMemoryError outOfMemoryError) {
                rotatedBitmap = null;
            }

        } catch (Exception e) {
            Log.d("RotateImage", e.getMessage());
            rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true); // rotating bitmap.
        }
        return rotatedBitmap;
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        if (inImage != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
            return Uri.parse(path);
        } else {
            return null;
        }
    }

    public void checkPermission() {
        ActivityCompat.requestPermissions(mActivity,
                new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, request_permission_for_camera);
    }


}
