package com.indy.utils;

import android.content.Context;
import android.content.res.Configuration;

import com.indy.helpers.AppFont;
import com.indy.views.activites.MasterActivity;

import java.util.Locale;

/**
 * Created by emad on 8/17/16.
 */
public class LocalizationUtils {
    static Context mContext;

    public LocalizationUtils(Context context) {
        this.mContext = context;
    }

    public static void switchLocalizaion(String language, MasterActivity masterActivity) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.setLocale(locale);
        AppFont._appFont = null;

        mContext.getResources().updateConfiguration(config,
                mContext.getResources().getDisplayMetrics());

    }





}
