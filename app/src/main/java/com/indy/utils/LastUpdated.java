package com.indy.utils;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.util.Log;

import java.net.URL;

/**
 * Created by emad on 11/1/16.
 */

public class LastUpdated extends IntentService {
    public static CountDownTimer countDownTimer;
    public static final String TAG = LastUpdated.class.getSimpleName();
    public static String lastUpdatedValue = "";
    Service service;
    public static TimerCountdownTask timerCountdownTask;
//    final int totalTime = 15 * 1000 * 900;
    final int totalTime = 1000;
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public LastUpdated(String name) {
        super(name);
        service = this;

//        startTimer();
    }

    public LastUpdated() {
        super(LastUpdated.class.getSimpleName());
        startTimerTask();
//        startTimer();
    }

    public void startTimerTask(){
        timerCountdownTask = new TimerCountdownTask();
//        timerCountdownTask.execute();
    }


    @Override
    protected void onHandleIntent(Intent intent) {
    }

    private void startTimer() {


        countDownTimer = new CountDownTimer(totalTime, 1000) {

            public void onTick(long millisUntilFinished) {
                if (countDownTimer != null) {


                    long time = (totalTime + 1000 - millisUntilFinished);

                    int minutes = (int) time / (60 * 1000);
                    int seconds = (int) (time / 1000) % 60;
                    String str = String.format("%d:%02d", minutes, seconds);
                    Log.d("seconds: ", "" + str);
                    changeValue(str);
                    //here you can have your logic to set text to edittext

                }
            }

            public void onFinish() {
//                countDownTimer.start();
            }


        }.start();
    }

    private void changeValue(String str) {
        if (this != null) {
            lastUpdatedValue = str;
            Log.v(TAG, str);
        }
    }

    @Override
    public boolean stopService(Intent name) {
        countDownTimer.cancel();
        timerCountdownTask.cancel(true);
        lastUpdatedValue = null;
        return true;
    }


    public class TimerCountdownTask extends AsyncTask<URL, Integer, Long> {
        int seconds = 0;

        protected Long doInBackground(URL... urls) {
            long totalSize = 0;


            for (seconds = 0; seconds < 1800; seconds++) {
                try {
//                    Thread.sleep(1000);

                } catch (Exception e) {
                    timerCountdownTask.cancel(true);
                    e.printStackTrace();
                    return 0L;
//                    try {
//                        Thread.sleep(500);
//                    } catch (InterruptedException e1) {
//                        e1.printStackTrace();
//                        Log.d(TAG,"Inner interrupt");
//                        return 0l;
//                    }

                }
                int minutes = seconds / 60;
                int secondsValue = seconds % 60;
//                lastUpdatedValue = minutes + ":" + secondsValue + "";

                Log.v(TAG, lastUpdatedValue);
            }


            return totalSize;
        }



        protected void onProgressUpdate(Integer... progress) {
//            setProgressPercent(progress[0]);
        }

        protected void onPostExecute(Long result) {
            seconds = 0;
            startTimerTask();

        }
    }


}