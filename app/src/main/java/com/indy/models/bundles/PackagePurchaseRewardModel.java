package com.indy.models.bundles;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mobile on 16/10/2017.
 */

public class PackagePurchaseRewardModel {

    @SerializedName("rewardTypeImage")
    @Expose
    private String rewardTypeImage;

    @SerializedName("rewardDescriptionEn")
    @Expose
    private String rewardDescriptionEn;

    @SerializedName("rewardDescriptionAr")
    @Expose
    private String rewardDescriptionAr;

    @SerializedName("rewardId")
    @Expose
    private String rewardId;

    public String getRewardTypeImage() {
        return rewardTypeImage;
    }

    public void setRewardTypeImage(String rewardTypeImage) {
        this.rewardTypeImage = rewardTypeImage;
    }

    public String getRewardDescriptionEn() {
        return rewardDescriptionEn;
    }

    public void setRewardDescriptionEn(String rewardDescriptionEn) {
        this.rewardDescriptionEn = rewardDescriptionEn;
    }

    public String getRewardDescriptionAr() {
        return rewardDescriptionAr;
    }

    public void setRewardDescriptionAr(String rewardDescriptionAr) {
        this.rewardDescriptionAr = rewardDescriptionAr;
    }

    public String getRewardId() {
        return rewardId;
    }

    public void setRewardId(String rewardId) {
        this.rewardId = rewardId;
    }
}
