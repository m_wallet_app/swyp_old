package com.indy.models.bundles;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by emad on 10/16/16.
 */

public class BundleList {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("inOfferId")
    @Expose
    private String[] inOfferId;
    @SerializedName("nameEn")
    @Expose
    private String nameEn;
    @SerializedName("nameAr")
    @Expose
    private String nameAr;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("iconUrl")
    @Expose
    private String iconUrl;
    @SerializedName("descriptionEn")
    @Expose
    private String descriptionEn;
    @SerializedName("descriptionAr")
    @Expose
    private String descriptionAr;

    @SerializedName("bundleName")
    @Expose
    private String bundleName;

    @SerializedName("bundleNameAr")
    @Expose
    private String bundleNameAr;

    @SerializedName("maxCount")
    @Expose
    private String maxCount;

    @SerializedName("ratePlanType")
    @Expose
    private String ratePlanType;

    @SerializedName("validtyInDays")
    @Expose
    private String validtyInDays;


    @SerializedName("packagePurchaseReward")
    @Expose
    private PackagePurchaseRewardModel packagePurchaseRewardModel;

    @SerializedName("requiresMembership")
    @Expose
    boolean requiresMembership;

    private boolean isAddSubtractEnabled = true;

    private String rewardID;

    public String getRewardID() {
        return rewardID;
    }

    public void setRewardID(String rewardID) {
        this.rewardID = rewardID;
    }

    @SerializedName("vatapplied")
    @Expose
    private boolean vatapplied;

    public String getRatePlanType() {
        return ratePlanType;
    }

    public void setRatePlanType(String ratePlanType) {
        this.ratePlanType = ratePlanType;
    }

    @SerializedName("isMembershipPack")
    @Expose
    private boolean isNewMembershipPack;


    private boolean isSelected;
    private String preferredNumber;
    private int count;

    int viewType;
    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The inOfferId
     */
    public String[] getInOfferId() {
        return inOfferId;
    }

    /**
     * @param inOfferId The inOfferId
     */
    public void setInOfferId(String[] inOfferId) {
        this.inOfferId = inOfferId;
    }

    /**
     * @return The nameEn
     */
    public String getNameEn() {
        return nameEn;
    }

    /**
     * @param nameEn The nameEn
     */
    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    /**
     * @return The nameAr
     */
    public String getNameAr() {
        return nameAr;
    }

    /**
     * @param nameAr The nameAr
     */
    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }

    /**
     * @return The price
     */
    public String getPrice() {
        return price;
    }

    /**
     * @param price The price
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * @return The iconUrl
     */
    public String getIconUrl() {
        return iconUrl;
    }

    /**
     * @param iconUrl The iconUrl
     */
    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    /**
     * @return The descriptionEn
     */
    public String getDescriptionEn() {
        return descriptionEn;
    }

    /**
     * @param descriptionEn The descriptionEn
     */
    public void setDescriptionEn(String descriptionEn) {
        this.descriptionEn = descriptionEn;
    }

    /**
     * @return The descriptionAr
     */
    public String getDescriptionAr() {
        return descriptionAr;
    }

    /**
     * @param descriptionAr The descriptionAr
     */
    public void setDescriptionAr(String descriptionAr) {
        this.descriptionAr = descriptionAr;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
    public boolean getSelected() {
        return isSelected;
    }

    public int getCount() {

        return count;
    }

    public String getBundleNameAr() {
        return bundleNameAr;
    }

    public void setBundleNameAr(String bundleNameAr) {
        this.bundleNameAr = bundleNameAr;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public String getPreferredNumber() {
        if(preferredNumber==null) {
            preferredNumber = "";
        }
        return preferredNumber;
    }

    public void setPreferredNumber(String preferredNumber) {
        this.preferredNumber = preferredNumber;
    }

    public String getBundleName() {
        if(bundleName!=null) {
            return bundleName;
        }else{
            return "";
        }
    }

    public void setBundleName(String bundleName) {
        this.bundleName = bundleName;
    }

    public int getMaxCount() {

        if(maxCount!=null) {
            return Integer.valueOf(maxCount);
        }else{
            return 0;
        }
    }

    public void setMaxCount(String maxCount) {
        this.maxCount = maxCount;
    }

    public String getValidtyInDays() {
        return validtyInDays;
    }

    public void setValidtyInDays(String validtyInDays) {
        this.validtyInDays = validtyInDays;
    }

    public PackagePurchaseRewardModel getPackagePurchaseRewardModel() {
        return packagePurchaseRewardModel;
    }

    public void setPackagePurchaseRewardModel(PackagePurchaseRewardModel packagePurchaseRewardModel) {
        this.packagePurchaseRewardModel = packagePurchaseRewardModel;
    }

    public boolean isAddSubtractEnabled() {
        return isAddSubtractEnabled;
    }

    public void setAddSubtractEnabled(boolean addSubtractEnabled) {
        isAddSubtractEnabled = addSubtractEnabled;
    }

    public boolean isVatapplied() {
        return vatapplied;
    }

    public void setVatapplied(boolean vatapplied) {
        this.vatapplied = vatapplied;
    }

    public boolean isRequiresMembership() {
        return requiresMembership;
    }

    public void setRequiresMembership(boolean requiresMembership) {
        this.requiresMembership = requiresMembership;
    }

    public boolean isNewMembershipPack() {
        return isNewMembershipPack;
    }

    public void setNewMembershipPack(boolean newMembershipPack) {
        isNewMembershipPack = newMembershipPack;
    }
}
