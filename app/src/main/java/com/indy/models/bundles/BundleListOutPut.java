package com.indy.models.bundles;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by emad on 10/16/16.
 */

public class BundleListOutPut extends MasterErrorResponse {

    @SerializedName("subscribedToNewMembershipPack")
    @Expose
    private boolean subscribedToNewMembershipPack;
    @SerializedName("bundleList")
    @Expose
    private List<BundleList> bundleList = new ArrayList<BundleList>();

    /**
     * @return The bundleList
     */
    public List<BundleList> getBundleList() {
        return bundleList;
    }

    /**
     * @param bundleList The bundleList
     */
    public void setBundleList(List<BundleList> bundleList) {
        this.bundleList = bundleList;
    }

    public boolean isNewMembershipSubscribed() {
        return subscribedToNewMembershipPack;
    }

    public void setNewMembershipSubscribed(boolean newMembershipSubscribed) {
        subscribedToNewMembershipPack = newMembershipSubscribed;
    }
}
