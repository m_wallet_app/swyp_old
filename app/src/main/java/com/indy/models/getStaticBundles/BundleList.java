package com.indy.models.getStaticBundles;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hadi.mehmood on 10/5/2016.
 */
public class BundleList
{
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("descriptionEn")
    @Expose
    private String descriptionEn;
    @SerializedName("nameEn")
    @Expose
    private String nameEn;
    @SerializedName("descriptionAr")
    @Expose
    private String descriptionAr;
    @SerializedName("iconUrl")
    @Expose
    private String iconUrl;
    @SerializedName("nameAr")
    @Expose
    private String nameAr;
    @SerializedName("priceUnitEn")
    @Expose
    private String priceUnitEn;
    @SerializedName("priceUnitAr")
    @Expose
    private String priceUnitAr;
    @SerializedName("bundleName")
    @Expose
    private String bundleName;


    @SerializedName("bundleNameAr")
    @Expose
    private String bundleNameAr;

    public String getBundleNameAr() {
        return bundleNameAr;
    }

    public void setBundleNameAr(String bundleNameAr) {
        this.bundleNameAr = bundleNameAr;
    }

    @SerializedName("inOfferId")
    @Expose
    private String[] inOfferId;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getPrice ()
    {
        return price;
    }

    public void setPrice (String price)
    {
        this.price = price;
    }

    public String getDescriptionEn ()
    {
        return descriptionEn;
    }

    public void setDescriptionEn (String descriptionEn)
    {
        this.descriptionEn = descriptionEn;
    }

    public String getNameEn ()
    {
        return nameEn;
    }

    public void setNameEn (String nameEn)
    {
        this.nameEn = nameEn;
    }

    public String getDescriptionAr ()
    {
        return descriptionAr;
    }

    public void setDescriptionAr (String descriptionAr)
    {
        this.descriptionAr = descriptionAr;
    }

    public String getIconUrl ()
    {
        return iconUrl;
    }

    public void setIconUrl (String iconUrl)
    {
        this.iconUrl = iconUrl;
    }

    public String getNameAr ()
    {
        return nameAr;
    }

    public void setNameAr (String nameAr)
    {
        this.nameAr = nameAr;
    }

    public String[] getInOfferId ()
    {
        return inOfferId;
    }

    public void setInOfferId (String[] inOfferId)
    {
        this.inOfferId = inOfferId;
    }

    public String getBundleName() {
        return bundleName;
    }

    public void setBundleName(String bundleName) {
        this.bundleName = bundleName;
    }

    public String getPriceUnitEn() {
        return priceUnitEn;
    }

    public void setPriceUnitEn(String priceUnitEn) {
        this.priceUnitEn = priceUnitEn;
    }

    public String getPriceUnitAr() {
        return priceUnitAr;
    }

    public void setPriceUnitAr(String priceUnitAr) {
        this.priceUnitAr = priceUnitAr;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", price = "+price+", descriptionEn = "+descriptionEn+", nameEn = "+nameEn+", descriptionAr = "+descriptionAr+", iconUrl = "+iconUrl+", nameAr = "+nameAr+", inOfferId = "+inOfferId+"]";
    }
}