package com.indy.models.getStaticBundles;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hadi.mehmood on 10/5/2016.
 */
public class StaticBundleOutput extends MasterErrorResponse {

    @SerializedName("bundleList")
    @Expose
    private BundleList[] bundleList;

    public List<BundleList> getBundleList() {
        List<BundleList> temp = new ArrayList<BundleList>();
        if (bundleList != null) {
            for (int i = 0; i < bundleList.length; i++) {
                temp.add(bundleList[i]);
            }
        }
        return temp;
    }

    public void setBundleList(BundleList[] bundleList) {
        this.bundleList = bundleList;
    }

    @Override
    public String toString() {
        return "ClassPojo [bundleList = " + bundleList + "]";
    }
}
