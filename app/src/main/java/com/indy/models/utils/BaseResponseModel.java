package com.indy.models.utils;


import retrofit2.Call;

/**
 * Created by Eabdelhamid on 11/10/2015.
 */
public class BaseResponseModel {
    private String serviceType;
    private Object resultObj;
    private Call<Object> call;
    int statusCode;
    String authToken;

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public void setResultObj(Object resultObj) {
        this.resultObj = resultObj;
    }

    public Object getResultObj() {
        return resultObj;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setCall(Call<Object> call) {
        this.call = call;
    }

    public Call<Object> getCall() {
        return call;
    }
}
