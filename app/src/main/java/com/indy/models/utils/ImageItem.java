package com.indy.models.utils;

import android.graphics.Bitmap;
import android.net.Uri;

/**
 * Created by Eabdelhamid on 11/17/2015.
 */
public class ImageItem {

    private int position;
    private Bitmap bitmap;
    private Uri uri;
    private String imgUrl;

    public void setPosition(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }


    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    public Uri getUri() {
        return uri;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getImgUrl() {
        return imgUrl;
    }
}
