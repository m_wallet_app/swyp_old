package com.indy.models.utils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by emad on 8/17/16.
 */
public class MasterErrorResponse {
    @SerializedName("errorCode")
    @Expose
    private String errorCode;
    @SerializedName("errorMsgEn")
    @Expose
    private String errorMsgEn;
    @SerializedName("errorMsgAr")
    @Expose
    private String errorMsgAr;

    /**
     * @return The errorCode
     */
    public String getErrorCode() {
        return errorCode != null ? errorCode : errorCode;
    }

    /**
     * @param errorCode The errorCode
     */
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * @return The errorMsgEn
     */
    public String getErrorMsgEn() {

        return errorMsgEn != null ? errorMsgEn : errorMsgEn;
    }

    /**
     * @param errorMsgEn The errorMsgEn
     */
    public void setErrorMsgEn(String errorMsgEn) {
        this.errorMsgEn = errorMsgEn;
    }

    /**
     * @return The errorMsgAr
     */
    public String getErrorMsgAr() {
        return errorMsgAr != null ? errorMsgAr : errorMsgAr;
    }

    /**
     * @param errorMsgAr The errorMsgAr
     */
    public void setErrorMsgAr(String errorMsgAr) {
        this.errorMsgAr = errorMsgAr;
    }


}
