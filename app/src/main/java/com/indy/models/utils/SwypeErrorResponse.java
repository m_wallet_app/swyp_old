package com.indy.models.utils;

import java.io.Serializable;

/**
 * Created by hadi.mehmood on 10/13/2016.
 */
public class SwypeErrorResponse extends MasterErrorResponse implements Serializable {
    String swyperErrorString;

    public String getSwyperErrorString() {
        return swyperErrorString!=null?swyperErrorString:null;
    }

    public void setSwyperErrorString(String swyperErrorString) {
        this.swyperErrorString = swyperErrorString;
    }
}
