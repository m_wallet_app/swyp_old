package com.indy.models.utils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

/**
 * This is the base request which contains all paramters common in the web services requests.
 * <p>
 * <p><b>Field descriptions</b></p>
 * <p>Msisdn: This the current phone number of the user. </p>
 * <p>Token: This is the firebase generated unique token of the application. This is used for push notifications</p>
 * <p>osVersion: This is used to identify which platform is sending the request. For example, Android or iOS.</p>
 * <p>channel: This is used to identify which of the Etisalat products is sending the request. For this application, the default value is: INDYAPPLICATION.</p>
 * <p>lang: This specifies the current language of the applicaiton as selected by user.</p>
 * <p>imsi:</p>
 * <p>deviceId: This is the same as the firebase generated device token. This is unique per application install.</p>
 * <p>authToken: This is recevied from service in response of login user.</p>
 */
public class MasterInputResponse {

    @SerializedName("appVersion")
    @Expose
    private String appVersion;
    @SerializedName("msisdn")
    @Expose
    private String msisdn;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("osVersion")
    @Expose
    private String osVersion;
    @SerializedName("channel")
    @Expose
    private String channel;
    @SerializedName("lang")
    @Expose
    private String lang;
    @SerializedName("imsi")
    @Expose
    private String imsi;
    @SerializedName("deviceId")
    @Expose
    private String deviceId;

    @SerializedName("authToken")
    @Expose
    private String authToken;

    @SerializedName("registrationId")
    @Expose
    private String registrationId;


    @SerializedName("significant")
    @Expose
    private String significant;

    /**
     * @return The appVersion
     */
    public String getAppVersion() {
        return appVersion != null ? appVersion : "";
    }

    /**
     * @param appVersion The appVersion
     */
    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    /**
     * @return The msisdn
     */
    public String getMsisdn() {
        return msisdn != null ? msisdn : "";

    }

    /**
     * @param msisdn The msisdn
     */
    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }

    /**
     * @return The token
     */
    public String getToken() {
        return token != null ? token : "";
    }

    /**
     * @param token The token
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * @return The osVersion
     */
    public String getOsVersion() {
        return osVersion != null ? osVersion : "";
    }

    /**
     * @param osVersion The osVersion
     */
    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    /**
     * @return The channel
     */
    public String getChannel() {
        return channel != null ? channel : "";
    }

    /**
     * @param channel The channel
     */
    public void setChannel(String channel) {
        this.channel = channel;
    }

    /**
     * @return The lang
     */
    public String getLang() {
        return lang != null ? lang : "";
    }

    /**
     * @param lang The lang
     */
    public void setLang(String lang) {
        this.lang = lang;
    }

    /**
     * @return The imsi
     */
    public String getImsi() {
        return imsi != null ? imsi : "";
    }

    /**
     * @param imsi The imsi
     */
    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    /**
     * @return The deviceId
     */
    public String getDeviceId() {
        return deviceId != null ? deviceId : "";
    }

    /**
     * @param deviceId The deviceId
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getAuthToken() {
        return authToken;
    }


    public String getSignificant() {
        return significant;
    }

    public void setSignificant(String significant) {
        this.significant = significant;
    }
}
