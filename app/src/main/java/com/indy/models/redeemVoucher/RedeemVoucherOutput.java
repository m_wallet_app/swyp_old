package com.indy.models.redeemVoucher;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by Amir.jehangir on 10/31/2016.
 */
public class RedeemVoucherOutput extends MasterErrorResponse {
    @SerializedName("redeemed")
    @Expose
    private Boolean redeemed;

    /**
     * @return The redeemed
     */
    public Boolean getRedeemed() {
        return redeemed;
    }

    /**
     * @param redeemed The redeemed
     */
    public void setRedeemed(Boolean redeemed) {
        this.redeemed = redeemed;
    }
}
