package com.indy.models.redeemVoucher;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterInputResponse;

/**
 * Created by Amir.jehangir on 10/31/2016.
 */
public class RedeemVoucherInput extends MasterInputResponse {

    @SerializedName("offerId")
    @Expose
    private String offerId;
    @SerializedName("voucherCode")
    @Expose
    private String voucherCode;
    @SerializedName("voucherPin")
    @Expose
    private String voucherPin;
    /**
     *
     * @return
     * The offerId
     */
    public String getOfferId() {
        return offerId;
    }

    /**
     *
     * @param offerId
     * The offerId
     */
    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    /**
     *
     * @return
     * The voucherCode
     */
    public String getVoucherCode() {
        return voucherCode;
    }

    /**
     *
     * @param voucherCode
     * The voucherCode
     */
    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    /**
     *
     * @return
     * The voucherPin
     */
    public String getVoucherPin() {
        return voucherPin;
    }

    /**
     *
     * @param voucherPin
     * The voucherPin
     */
    public void setVoucherPin(String voucherPin) {
        this.voucherPin = voucherPin;
    }
}
