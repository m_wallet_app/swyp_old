package com.indy.models.balance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by emad on 8/11/16.
 */
public class BalanceResponseModel extends MasterErrorResponse {

    @SerializedName("amount")
    @Expose
    private Double amount;
    @SerializedName("amountUnitEn")
    @Expose
    private String amountUnitEn;
    @SerializedName("amountUnitAr")
    @Expose
    private String amountUnitAr;

    /**
     * @return The amount
     */
    public double getAmount() {
        if(amount !=null)
        return amount;
        else{
            return 0;
        }
    }

    /**
     * @param amount The amount
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * @return The amountUnitEn
     */
    public String getAmountUnitEn() {
        return amountUnitEn;
    }

    /**
     * @param amountUnitEn The amountUnitEn
     */
    public void setAmountUnitEn(String amountUnitEn) {
        this.amountUnitEn = amountUnitEn;
    }

    /**
     * @return The amountUnitAr
     */
    public String getAmountUnitAr() {
        return amountUnitAr;
    }

    /**
     * @param amountUnitAr The amountUnitAr
     */
    public void setAmountUnitAr(String amountUnitAr) {
        this.amountUnitAr = amountUnitAr;
    }
}
