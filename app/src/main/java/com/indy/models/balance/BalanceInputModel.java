package com.indy.models.balance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterInputResponse;

/**
 * Created by emad on 8/17/16.
 */
public class BalanceInputModel extends MasterInputResponse {
    @SerializedName("billingPeriod")
    @Expose
    private String billingPeriod;
    /**
     * @return The billingPeriod
     */
    public String getBillingPeriod() {
        return billingPeriod;
    }

    /**
     * @param billingPeriod The billingPeriod
     */
    public void setBillingPeriod(String billingPeriod) {
        this.billingPeriod = billingPeriod;
    }

}
