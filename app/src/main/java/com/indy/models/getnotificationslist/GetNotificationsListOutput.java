package com.indy.models.getnotificationslist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

import java.util.List;



public class GetNotificationsListOutput extends MasterErrorResponse {

    @SerializedName("notificationsList")
    @Expose
    private List<NotificationModel> notificationsList = null;

    public List<NotificationModel> getNotificationsList() {
        return notificationsList;
    }

    public void setNotificationsList(List<NotificationModel> notificationsList) {
        this.notificationsList = notificationsList;
    }

}

