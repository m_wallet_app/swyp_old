package com.indy.models.updateLanguage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by mobile on 26/01/2017.
 */

public class UpdateLanguageOutput extends MasterErrorResponse {

    @SerializedName("responseCode")
    @Expose
    String responseCode;
    @SerializedName("responseMsg")
    @Expose
    String responseMsg;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }
}
