package com.indy.models.validateotp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterInputResponse;

/**
 * Created by emad on 11/17/16.
 */

public class ValidateOtpInput extends MasterInputResponse {

    @SerializedName("otpCode")
    @Expose
    private String otpCode;


    /**
     * @return The otpCode
     */
    public String getOtpCode() {
        return otpCode;
    }

    /**
     * @param otpCode The otpCode
     */
    public void setOtpCode(String otpCode) {
        this.otpCode = otpCode;
    }
}
