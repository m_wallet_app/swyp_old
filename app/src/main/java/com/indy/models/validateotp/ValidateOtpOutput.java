package com.indy.models.validateotp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by emad on 11/17/16.
 */

public class ValidateOtpOutput extends MasterErrorResponse {

    @SerializedName("valid")
    @Expose
    private Boolean isValid;

    /**
     * @return The isValid
     */
    public Boolean getIsValid() {
        return isValid;
    }

    /**
     * @param isValid The isValid
     */
    public void setIsValid(Boolean isValid) {
        this.isValid = isValid;
    }

}
