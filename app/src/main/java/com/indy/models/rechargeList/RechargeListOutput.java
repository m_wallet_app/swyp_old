package com.indy.models.rechargeList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by Amir.jehangir on 10/17/2016.
 */
public class RechargeListOutput extends MasterErrorResponse {

    @SerializedName("rechargeList")
    @Expose
    private String[] rechargeList;

    public String[] getRechargeList() {
        return rechargeList;
    }

    public void setRechargeList(String[] rechargeList) {
        this.rechargeList = rechargeList;
    }

    @Override
    public String toString() {
        return "ClassPojo [rechargeList = " + rechargeList + "]";
    }
}
