package com.indy.models.login;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by emad on 9/20/16.
 */

public class UserProfile implements Parcelable {

    @SerializedName("fullName")
    @Expose
    private String fullName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("birthDate")
    @Expose
    private String birthDate;
    @SerializedName("msisdn")
    @Expose
    private String msisdn;
    @SerializedName("contactMsisdn")
    @Expose
    private String contactMsisdn;
    @SerializedName("registrationId")
    @Expose
    private String registrationId;
    @SerializedName("profilePicUrl")
    @Expose
    private String profilePicUrl;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("nickName")
    @Expose
    private String nickName;
    @SerializedName("membershipData")
    @Expose
    private MembershipData membershipData;

    @SerializedName("invitationCode")
    @Expose
    private String invitationCode;

    protected UserProfile(Parcel in) {
        fullName = in.readString();
        email = in.readString();
        birthDate = in.readString();
        msisdn = in.readString();
        contactMsisdn = in.readString();
        registrationId = in.readString();
        profilePicUrl = in.readString();
        gender = in.readString();
        language = in.readString();
        nickName = in.readString();
        invitationCode = in.readString();
    }

    public static final Creator<UserProfile> CREATOR = new Creator<UserProfile>() {
        @Override
        public UserProfile createFromParcel(Parcel in) {
            return new UserProfile(in);
        }

        @Override
        public UserProfile[] newArray(int size) {
            return new UserProfile[size];
        }
    };

    /**
     * @return The fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName The fullName
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * @return The email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return The birthDate
     */
    public String getBirthDate() {
        return birthDate;
    }

    /**
     * @param birthDate The birthDate
     */
    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    /**
     * @return The msisdn
     */
    public String getMsisdn() {
        return msisdn;
    }

    /**
     * @param msisdn The msisdn
     */
    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    /**
     * @return The contactMsisdn
     */
    public String getContactMsisdn() {
        return contactMsisdn;
    }

    /**
     * @param contactMsisdn The contactMsisdn
     */
    public void setContactMsisdn(String contactMsisdn) {
        this.contactMsisdn = contactMsisdn;
    }

    /**
     * @return The registrationId
     */
    public String getRegistrationId() {
        return registrationId;
    }

    /**
     * @param registrationId The registrationId
     */
    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }

    /**
     * @return The profilePicUrl
     */
    public String getProfilePicUrl() {
        return profilePicUrl;
    }

    /**
     * @param profilePicUrl The profilePicUrl
     */
    public void setProfilePicUrl(String profilePicUrl) {
        this.profilePicUrl = profilePicUrl;
    }

    /**
     * @return The gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender The gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return The language
     */
    public String getLanguage() {
        return language;
    }

    /**
     * @param language The language
     */
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     * @return The nickName
     */
    public String getNickName() {
        return nickName;
    }

    /**
     * @param nickName The nickName
     */
    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    /**
     * @return The membershipData
     */
    public MembershipData getMembershipData() {
        return membershipData;
    }

    /**
     * @param membershipData The membershipData
     */
    public void setMembershipData(MembershipData membershipData) {
        this.membershipData = membershipData;
    }

    public void setInvitationCode(String invitationCode) {
        this.invitationCode = invitationCode;
    }

    public String getInvitationCode() {
        return invitationCode;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(fullName);
        dest.writeString(email);
        dest.writeString(birthDate);
        dest.writeString(msisdn);
        dest.writeString(contactMsisdn);
        dest.writeString(registrationId);
        dest.writeString(profilePicUrl);
        dest.writeString(gender);
        dest.writeString(language);
        dest.writeString(nickName);
        dest.writeString(invitationCode);
    }
}
