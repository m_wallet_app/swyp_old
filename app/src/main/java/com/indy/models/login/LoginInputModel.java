package com.indy.models.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.models.utils.MasterInputResponse;

/**
 * Created by emad on 9/20/16.
 */

public class LoginInputModel extends MasterInputResponse {

    @SerializedName("password")
    @Expose
    private String password;


    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }
}
