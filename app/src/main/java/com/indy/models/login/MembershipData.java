package com.indy.models.login;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by emad on 10/13/16.
 */

public class MembershipData implements Parcelable {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("expiryDate")
    @Expose
    private String expiryDate;
    @SerializedName("startDate")
    @Expose
    private String startDate;
    @SerializedName("subscribtionDaysLeft")
    @Expose
    private Integer subscribtionDaysLeft;

    /**
     * @return The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * @return The expiryDate
     */
    public String getExpiryDate() {
        return expiryDate;
    }

    /**
     * @param expiryDate The expiryDate
     */
    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    /**
     * @return The startDate
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * @param startDate The startDate
     */
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    /**
     * @return The subscribtionDaysLeft
     */
    public Integer getSubscribtionDaysLeft() {
        return subscribtionDaysLeft;
    }

    /**
     * @param subscribtionDaysLeft The subscribtionDaysLeft
     */
    public void setSubscribtionDaysLeft(Integer subscribtionDaysLeft) {
        this.subscribtionDaysLeft = subscribtionDaysLeft;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.status);
        dest.writeString(this.expiryDate);
        dest.writeString(this.startDate);
        dest.writeValue(this.subscribtionDaysLeft);
    }

    public MembershipData() {
    }

    protected MembershipData(Parcel in) {
        this.status = (Integer) in.readValue(Integer.class.getClassLoader());
        this.expiryDate = in.readString();
        this.startDate = in.readString();
        this.subscribtionDaysLeft = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Creator<MembershipData> CREATOR = new Creator<MembershipData>() {
        @Override
        public MembershipData createFromParcel(Parcel source) {
            return new MembershipData(source);
        }

        @Override
        public MembershipData[] newArray(int size) {
            return new MembershipData[size];
        }
    };
}
