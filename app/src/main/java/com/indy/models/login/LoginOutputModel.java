package com.indy.models.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.profilestatus.AppFlags;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by emad on 9/20/16.
 */

public class LoginOutputModel extends MasterErrorResponse {

    @SerializedName("userProfile")
    @Expose
    private UserProfile userProfile;
    @SerializedName("appFlags")
    @Expose
    private AppFlags appFlags;

    /**
     * @return The userProfile
     */
    public UserProfile getUserProfile() {
        return userProfile;
    }

    /**
     * @param userProfile The userProfile
     */
    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    /**
     * @return The appFlags
     */
    public AppFlags getAppFlags() {
        return appFlags;
    }

    /**
     * @param appFlags The appFlags
     */
    public void setAppFlags(AppFlags appFlags) {
        this.appFlags = appFlags;
    }
}
