package com.indy.models.getShipmentStatus;

import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by mobile on 26/07/2017.
 */

public class GetShipmentStatusOutput extends MasterErrorResponse {
    @SerializedName("orderId")
    private String orderId;

    @SerializedName("shipmentStatus")
    private String shipmentStatus;

    @SerializedName("shipmentStatusDescription")
    private String shipmentStatusDescription;

    @SerializedName("shipmentStatusEntryDate")
    private String shipmentStatusEntryDate;

    public String getOrdeRId() {
        return orderId;
    }

    public void setOrdeRId(String ordeRId) {
        this.orderId = ordeRId;
    }

    public String getShipmentStatus() {
        return shipmentStatus;
    }

    public void setShipmentStatus(String shipmentStatus) {
        this.shipmentStatus = shipmentStatus;
    }

    public String getShipmentStatusDescription() {
        return shipmentStatusDescription;
    }

    public void setShipmentStatusDescription(String shipmentStatusDescription) {
        this.shipmentStatusDescription = shipmentStatusDescription;
    }

    public String getShipmentStatusEntryDate() {
        return shipmentStatusEntryDate;
    }

    public void setShipmentStatusEntryDate(String shipmentStatusEntryDate) {
        this.shipmentStatusEntryDate = shipmentStatusEntryDate;
    }
}
