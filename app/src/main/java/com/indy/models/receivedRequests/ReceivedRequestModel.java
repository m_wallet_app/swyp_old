package com.indy.models.receivedRequests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hadi.mehmood on 10/14/2016.
 */
public class ReceivedRequestModel {

    @SerializedName("mobileNumber")
    @Expose
    String mobileNumber;
    @SerializedName("price")
    @Expose
    String price;
    @SerializedName("date")
    @Expose
    String date;
    @SerializedName("id")
    @Expose
    String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
