package com.indy.models.uploadimages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterInputResponse;

/**
 * Created by emad on 9/26/16.
 */

public class UploadImageInput extends MasterInputResponse {

    @SerializedName("regId")
    @Expose
    private String regId;

    public void setRegId(String regId) {
        this.regId = regId;
    }

    public String getRegId() {
        return regId;
    }
}
