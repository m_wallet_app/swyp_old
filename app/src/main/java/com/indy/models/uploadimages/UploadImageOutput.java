package com.indy.models.uploadimages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by emad on 9/26/16.
 */

public class UploadImageOutput extends MasterErrorResponse {

    @SerializedName("updated")
    @Expose
    private Boolean updated;

    @SerializedName("profilePicUrl")
    @Expose
    private String profilePicUrl;
    /**
     * @return The updated
     */
    public Boolean getUpdated() {
        return updated;
    }

    /**
     * @param updated The updated
     */
    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    public void setProfilePicUrl(String profilePicUrl) {
        this.profilePicUrl = profilePicUrl;
    }

    public String getProfilePicUrl() {
        return profilePicUrl;
    }
}
