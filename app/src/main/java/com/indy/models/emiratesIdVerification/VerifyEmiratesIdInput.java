package com.indy.models.emiratesIdVerification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.finalizeregisteration.DeliveryInfo;
import com.indy.models.utils.MasterInputResponse;

/**
 * Created by emad on 9/25/16.
 */

public class VerifyEmiratesIdInput extends MasterInputResponse{

    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("shippingMethod")
    @Expose
    private String shippingMethod;
    @SerializedName("orderType")
    @Expose
    private String orderType;
    @SerializedName("packageType")
    @Expose
    private String packageType;
    @SerializedName("deliveryInfo")
    @Expose
    private DeliveryInfo deliveryInfo;


    @SerializedName("emiratesId")
    @Expose
    private String emiratesId;

    @SerializedName("fullName")
    @Expose
    private String fullName;
    /**
     * @return The registrationId
     */


    /**
     * @return The amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * @param amount The amount
     */
    public void setAmount(String amount) {
        this.amount = amount;
    }

    /**
     * @return The shippingMethod
     */
    public String getShippingMethod() {
        return shippingMethod;
    }

    /**
     * @param shippingMethod The shippingMethod
     */
    public void setShippingMethod(String shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    /**
     * @return The orderType
     */
    public String getOrderType() {
        return orderType;
    }

    /**
     * @param orderType The orderType
     */
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    /**
     * @return The packageType
     */
    public String getPackageType() {
        return packageType;
    }

    /**
     * @param packageType The packageType
     */
    public void setPackageType(String packageType) {
        this.packageType = packageType;
    }

    /**
     * @return The deliveryInfo
     */
    public DeliveryInfo getDeliveryInfo() {
        return deliveryInfo;
    }

    /**
     * @param deliveryInfo The deliveryInfo
     */
    public void setDeliveryInfo(DeliveryInfo deliveryInfo) {
        this.deliveryInfo = deliveryInfo;
    }

    public String getEmiratesId() {
        return emiratesId;
    }

    public void setEmiratesId(String emiratesId) {
        this.emiratesId = emiratesId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
