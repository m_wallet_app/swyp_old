
package com.indy.models.esimActivation;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterInputResponse;

import java.util.HashMap;
import java.util.Map;

public class CheckESIMEligibilityRequestModel extends MasterInputResponse implements Parcelable {

    @SerializedName("additionalInfo")
    @Expose
    private Map<String, String> additionalInfo = new HashMap<>();

    public Map<String, String> getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String key, String value) {
        this.additionalInfo.put(key, value);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.additionalInfo.size());
        for (Map.Entry<String, String> entry : this.additionalInfo.entrySet()) {
            dest.writeString(entry.getKey());
            dest.writeString(entry.getValue());
        }
    }

    public CheckESIMEligibilityRequestModel() {
    }

    protected CheckESIMEligibilityRequestModel(Parcel in) {
        int additionalInfoSize = in.readInt();
        this.additionalInfo = new HashMap<String, String>(additionalInfoSize);
        for (int i = 0; i < additionalInfoSize; i++) {
            String key = in.readString();
            String value = in.readString();
            this.additionalInfo.put(key, value);
        }
    }

    public static final Creator<CheckESIMEligibilityRequestModel> CREATOR = new Creator<CheckESIMEligibilityRequestModel>() {
        @Override
        public CheckESIMEligibilityRequestModel createFromParcel(Parcel source) {
            return new CheckESIMEligibilityRequestModel(source);
        }

        @Override
        public CheckESIMEligibilityRequestModel[] newArray(int size) {
            return new CheckESIMEligibilityRequestModel[size];
        }
    };
}
