
package com.indy.models.esimActivation;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

public class ActivateESIMResponseModel extends MasterErrorResponse implements Parcelable {

    @SerializedName("activationCode")
    private String mActivationCode;
    @SerializedName("responseMsg")
    private String mResponseMsg;
    @SerializedName("simDPAddress")
    private String mSimDPAddress;

    public String getActivationCode() {
        return mActivationCode;
    }

    public void setActivationCode(String activationCode) {
        mActivationCode = activationCode;
    }

    public String getResponseMsg() {
        return mResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        mResponseMsg = responseMsg;
    }

    public String getSimDPAddress() {
        return mSimDPAddress;
    }

    public void setSimDPAddress(String simDPAddress) {
        mSimDPAddress = simDPAddress;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mActivationCode);
        dest.writeString(this.mResponseMsg);
        dest.writeString(this.mSimDPAddress);
    }

    public ActivateESIMResponseModel() {
    }

    protected ActivateESIMResponseModel(Parcel in) {
        this.mActivationCode = in.readString();
        this.mResponseMsg = in.readString();
        this.mSimDPAddress = in.readString();
    }

    public static final Creator<ActivateESIMResponseModel> CREATOR = new Creator<ActivateESIMResponseModel>() {
        @Override
        public ActivateESIMResponseModel createFromParcel(Parcel source) {
            return new ActivateESIMResponseModel(source);
        }

        @Override
        public ActivateESIMResponseModel[] newArray(int size) {
            return new ActivateESIMResponseModel[size];
        }
    };
}
