
package com.indy.models.esimActivation;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;


public class CheckESIMEligibiltyResponseModel extends MasterErrorResponse implements Parcelable {

    @SerializedName("responseMsg")
    private String mResponseMsg;

    public String getResponseMsg() {
        return mResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        mResponseMsg = responseMsg;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mResponseMsg);
    }

    public CheckESIMEligibiltyResponseModel() {
    }

    protected CheckESIMEligibiltyResponseModel(Parcel in) {
        this.mResponseMsg = in.readString();
    }

    public static final Creator<CheckESIMEligibiltyResponseModel> CREATOR = new Creator<CheckESIMEligibiltyResponseModel>() {
        @Override
        public CheckESIMEligibiltyResponseModel createFromParcel(Parcel source) {
            return new CheckESIMEligibiltyResponseModel(source);
        }

        @Override
        public CheckESIMEligibiltyResponseModel[] newArray(int size) {
            return new CheckESIMEligibiltyResponseModel[size];
        }
    };
}
