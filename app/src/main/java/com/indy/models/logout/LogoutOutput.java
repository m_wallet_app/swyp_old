package com.indy.models.logout;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by hadi.mehmood on 11/10/2016.
 */
public class LogoutOutput extends MasterErrorResponse {

    @SerializedName("loggedOut")
    @Expose
    private boolean loggedOut;

    public boolean isLoggedOut() {
        return loggedOut;
    }

    public void setLoggedOut(boolean loggedOut) {
        this.loggedOut = loggedOut;
    }
}
