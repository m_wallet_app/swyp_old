package com.indy.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.purchaseStoreBundle.OrderItems;
import com.indy.models.utils.MasterInputResponse;

/**
 * Created by hadi.mehmood on 11/12/2016.
 */
public class ChangePreferredNumberInputModel extends MasterInputResponse {

    @SerializedName("orderItem")
    @Expose
    private OrderItems orderItem;

    public OrderItems getOrderItem() {
        return orderItem;
    }

    public void setOrderItem(OrderItems orderItem) {
        this.orderItem = orderItem;
    }
}
