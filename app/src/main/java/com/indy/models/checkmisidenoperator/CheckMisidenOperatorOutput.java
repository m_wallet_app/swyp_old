package com.indy.models.checkmisidenoperator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by emad on 9/25/16.
 */

public class CheckMisidenOperatorOutput extends MasterErrorResponse {

    @SerializedName("status")
    @Expose
    private Integer status;

    /**
     * @return The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

}
