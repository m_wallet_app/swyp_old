package com.indy.models.payperuse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by emad on 9/28/16.
 */

public class PaymentPackageList {

    @SerializedName("nameEn")
    @Expose
    private String nameEn;

    @SerializedName("nameAr")
    @Expose
    private String nameAr;
    @SerializedName("consumption")
    @Expose
    private Double consumption;
    @SerializedName("consumptionUnitEn")
    @Expose
    private String consumptionUnitEn;
    @SerializedName("consumptionUnitAr")
    @Expose
    private String consumptionUnitAr;
    @SerializedName("amount")
    @Expose
    private Double amount;
    @SerializedName("amountUnitEn")
    @Expose
    private String amountUnitEn;
    @SerializedName("amountUnitAr")
    @Expose
    private String amountUnitAr;


    public String getNameEn() {
        return nameEn;
    }


    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameAr() {
        return nameAr;
    }


    public void setNameAr(String nameEn) {
        this.nameAr = nameAr;
    }


    /**
     * @return The consumption
     */
    public String getConsumption() {

        try {
            NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
            nf.setMaximumFractionDigits(2);
            nf.setMinimumFractionDigits(2);
            DecimalFormat df = (DecimalFormat) nf;

//            DecimalFormat decimalFormat = new DecimalFormat();
//            decimalFormat.applyLocalizedPattern("#.0");
            String doubleTemp = df.format(consumption);

//            Double valToReturnDouble = Double.valueOf(doubleTemp);
//            masterActivity.sharedPrefrencesManger.setLanguage(currentLanguage);
            return doubleTemp;
        } catch (Exception ex) {
            ex.printStackTrace();
            return consumption+"";
        }
    }

    /**
     * @param consumption The consumption
     */
    public void setConsumption(Double consumption) {
        this.consumption = consumption;
    }

    /**
     * @return The consumptionUnitEn
     */
    public String getConsumptionUnitEn() {
        return consumptionUnitEn;
    }

    /**
     * @param consumptionUnitEn The consumptionUnitEn
     */
    public void setConsumptionUnitEn(String consumptionUnitEn) {
        this.consumptionUnitEn = consumptionUnitEn;
    }

    /**
     * @return The consumptionUnitAr
     */
    public String getConsumptionUnitAr() {
        return consumptionUnitAr;
    }

    /**
     * @param consumptionUnitAr The consumptionUnitAr
     */
    public void setConsumptionUnitAr(String consumptionUnitAr) {
        this.consumptionUnitAr = consumptionUnitAr;
    }

    /**
     * @return The amount
     */
    public String getAmount() {
        try {
            NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
            nf.setMaximumFractionDigits(2);
            nf.setMinimumFractionDigits(2);
            DecimalFormat df = (DecimalFormat) nf;

//            DecimalFormat decimalFormat = new DecimalFormat();
//            decimalFormat.applyLocalizedPattern("#.0");
            String doubleTemp = df.format(amount);

//            Double valToReturnDouble = Double.valueOf(doubleTemp);
//            masterActivity.sharedPrefrencesManger.setLanguage(currentLanguage);
            return doubleTemp;
        } catch (Exception ex) {
            ex.printStackTrace();
            return amount+"";
        }
    }

    /**
     * @param amount The amount
     */
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    /**
     * @return The amountUnitEn
     */
    public String getAmountUnitEn() {
        return amountUnitEn;
    }

    /**
     * @param amountUnitEn The amountUnitEn
     */
    public void setAmountUnitEn(String amountUnitEn) {
        this.amountUnitEn = amountUnitEn;
    }

    /**
     * @return The amountUnitAr
     */
    public String getAmountUnitAr() {
        return amountUnitAr;
    }

    /**
     * @param amountUnitAr The amountUnitAr
     */
    public void setAmountUnitAr(String amountUnitAr) {
        this.amountUnitAr = amountUnitAr;
    }
}
