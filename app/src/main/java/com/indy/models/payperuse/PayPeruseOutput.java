package com.indy.models.payperuse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by emad on 9/28/16.
 */

public class PayPeruseOutput extends MasterErrorResponse {
    @SerializedName("paymentPackageList")
    @Expose
    private List<PaymentPackageList> paymentPackageList = new ArrayList<PaymentPackageList>();
    @SerializedName("intervalEn")
    @Expose
    private String intervalEn;

    @SerializedName("intervalAr")
    @Expose
    private String intervalAr;

    /**
     * @return The paymentPackageList
     */
    public List<PaymentPackageList> getPaymentPackageList() {
        return paymentPackageList;
    }

    /**
     * @param paymentPackageList The paymentPackageList
     */
    public void setPaymentPackageList(List<PaymentPackageList> paymentPackageList) {
        this.paymentPackageList = paymentPackageList;
    }

    /**
     * @return The interval
     */
    public String getIntervalEn() {
        return intervalEn;
    }

    public void setIntervalEn(String intervalEn) {
        this.intervalEn = intervalEn;
    }

    public String getIntervalAr() {
        return intervalAr;
    }

    public void setIntervalAr(String intervalAr) {
        this.intervalAr = intervalAr;
    }
}
