package com.indy.models.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.login.UserProfile;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by emad on 10/20/16.
 */

public class EditProfileOutput extends MasterErrorResponse {
    @SerializedName("userProfile")
    @Expose
    private UserProfile userProfile;

    /**
     * @return The userProfile
     */
    public UserProfile getUserProfile() {
        return userProfile;
    }

    /**
     * @param userProfile The userProfile
     */
    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }
}
