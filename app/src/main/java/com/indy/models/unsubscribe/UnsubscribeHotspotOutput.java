package com.indy.models.unsubscribe;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by hadi on 11/10/16.
 */

public class UnsubscribeHotspotOutput extends MasterErrorResponse {

    @SerializedName("unsubscribed")
    @Expose
    private Boolean unsubscribed;

    public Boolean getUnsubscribed() {
        return unsubscribed;
    }

    public void setUnsubscribed(Boolean unsubscribed) {
        this.unsubscribed = unsubscribed;
    }

}
