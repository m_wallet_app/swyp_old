package com.indy.models.faqs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.paymnet.AdditionalInfo;
import com.indy.models.utils.MasterErrorResponse;

import java.util.List;

/**
 * Created by emad on 8/17/16.
 */
public class FaqsResponseModel extends MasterErrorResponse {

    @SerializedName("srcUrlEn")
    @Expose
    private String srcUrlEn;
    @SerializedName("srcUrlAr")
    @Expose
    private String srcUrlAr;
    @SerializedName("appUpdateRequired")
    @Expose
    private String appUpdateRequired;

    @SerializedName("appUpdateErrorMessageAr")
    @Expose
    private String appUpdateErrorMessageAr;

    @SerializedName("appUpdateErrorMessageEn")
    @Expose
    private String appUpdateErrorMessageEn;

    @SerializedName("appUpdateDownloadUrl")
    @Expose
    private String appUpdateDownloadUrl;

    @SerializedName("displayVAT")
    @Expose
    private boolean displayVAT;

    @SerializedName("additionalInfo")
    @Expose
    private List<AdditionalInfo> additionalInfoList;

    @SerializedName("migrationEnabled")
    @Expose
    private boolean migrationEnabled;

    @SerializedName("membershipPrice")
    @Expose
    private String membershipPrice;

    public String getMembershipPrice() {
        return membershipPrice;
    }

    public void setMembershipPrice(String membershipPrice) {
        this.membershipPrice = membershipPrice;
    }

    @SerializedName("simPrice")
    @Expose
    private String simPrice;

    @SerializedName("loggedInMembershipPrice")
    @Expose
    private String loggedInMembershipPrice;

    public String getSimPrice() {
        return simPrice;
    }

    public void setSimPrice(String simPrice) {
        this.simPrice = simPrice;
    }

    public String getNewMembershipPrice() {
        return newMembershipPrice;
    }

    public void setNewMembershipPrice(String newMembershipPrice) {
        this.newMembershipPrice = newMembershipPrice;
    }

    @SerializedName("newMembershipPrice")
    @Expose
    private String newMembershipPrice;

    public boolean isMigrationEnabled() {
        return migrationEnabled;
    }

    public void setMigrationEnabled(boolean migrationEnabled) {
        this.migrationEnabled = migrationEnabled;
    }

    /**
     * @return The srcUrlEn
     */
    public String getSrcUrlEn() {
        return srcUrlEn;
    }

    /**
     * @param srcUrlEn The srcUrlEn
     */
    public void setSrcUrlEn(String srcUrlEn) {
        this.srcUrlEn = srcUrlEn;
    }

    /**
     * @return The srcUrlAr
     */
    public String getSrcUrlAr() {
        return srcUrlAr;
    }

    /**
     * @param srcUrlAr The srcUrlAr
     */
    public void setSrcUrlAr(String srcUrlAr) {
        this.srcUrlAr = srcUrlAr;
    }

    public String getAppUpdateRequired() {
        return appUpdateRequired;
    }

    public void setAppUpdateRequired(String appUpdateRequired) {
        this.appUpdateRequired = appUpdateRequired;
    }

    public String getAppUpdateErrorMessageAr() {
        return appUpdateErrorMessageAr;
    }

    public void setAppUpdateErrorMessageAr(String appUpdateErrorMessageAr) {
        this.appUpdateErrorMessageAr = appUpdateErrorMessageAr;
    }

    public String getAppUpdateErrorMessageEn() {
        return appUpdateErrorMessageEn;
    }

    public void setAppUpdateErrorMessageEn(String appUpdateErrorMessageEn) {
        this.appUpdateErrorMessageEn = appUpdateErrorMessageEn;
    }

    public String getAppUpdateDownloadUrl() {
        return appUpdateDownloadUrl;
    }

    public void setAppUpdateDownloadUrl(String appUpdateDownloadUrl) {
        this.appUpdateDownloadUrl = appUpdateDownloadUrl;
    }

    public boolean isDisplayVAT() {
        return displayVAT;
    }

    public void setDisplayVAT(boolean displayVAT) {
        this.displayVAT = displayVAT;
    }

    public List<AdditionalInfo> getAdditionalInfoList() {
        return additionalInfoList;
    }

    public void setAdditionalInfoList(List<AdditionalInfo> additionalInfoList) {
        this.additionalInfoList = additionalInfoList;
    }

    public String getLoggedInMembershipPrice() {
        return loggedInMembershipPrice;
    }

    public void setLoggedInMembershipPrice(String loggedInMembershipPrice) {
        this.loggedInMembershipPrice = loggedInMembershipPrice;
    }
}
