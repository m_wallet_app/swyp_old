package com.indy.models.getnotificationdetails;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterInputResponse;



public class GetNotificationDetailsInput extends MasterInputResponse {
    @SerializedName("notificationId")
    @Expose
    private String notificationId;




    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }
}
