package com.indy.models.getnotificationdetails;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;



public class GetNotificationDetailsOutput extends MasterErrorResponse {

    @SerializedName("notificationDetails")
    @Expose
    private NotificationDetails notificationDetails;

    public NotificationDetails getNotificationDetails() {
        return notificationDetails;
    }

    public void setNotificationDetails(NotificationDetails notificationDetails) {
        this.notificationDetails = notificationDetails;
    }

}