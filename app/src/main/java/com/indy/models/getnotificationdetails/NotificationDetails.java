package com.indy.models.getnotificationdetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;



public class NotificationDetails {

    @SerializedName("notificationId")
    @Expose
    private String notificationId;
    @SerializedName("notificationTitle")
    @Expose
    private String notificationTitle;
    @SerializedName("notificationDate")
    @Expose
    private String notificationDate;
    @SerializedName("notificationImage")
    @Expose
    private String notificationImage;
    @SerializedName("notificationText")
    @Expose
    private String notificationText;
    @SerializedName("notificationActionType")
    @Expose
    private int notificationActionType;

    @SerializedName("luckyDeal")
    @Expose
    private LuckyDeal luckyDeal;


    @SerializedName("itemId")
    @Expose
    private String itemId;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    public String getNotificationTitle() {
        return notificationTitle;
    }

    public void setNotificationTitle(String notificationTitle) {
        this.notificationTitle = notificationTitle;
    }

    public String getNotificationDate() {
        return notificationDate;
    }

    public void setNotificationDate(String notificationDate) {
        this.notificationDate = notificationDate;
    }

    public String getNotificationImage() {
        return notificationImage;
    }

    public void setNotificationImage(String notificationImage) {
        this.notificationImage = notificationImage;
    }

    public String getNotificationText() {
        return notificationText;
    }

    public void setNotificationText(String notificationText) {
        this.notificationText = notificationText;
    }

    public int getNotificationActionType() {
        return notificationActionType;
    }

    public void setNotificationActionType(int notificationActionType) {
        this.notificationActionType = notificationActionType;
    }

    public LuckyDeal getLuckyDeal() {
        return luckyDeal;
    }

    public void setLuckyDeal(LuckyDeal luckyDeal) {
        this.luckyDeal = luckyDeal;
    }
}