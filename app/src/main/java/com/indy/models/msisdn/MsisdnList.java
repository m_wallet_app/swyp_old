package com.indy.models.msisdn;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by emad on 8/16/16.
 */
public class MsisdnList {

    @SerializedName("msisdn")
    @Expose
    private String msisdn;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("description")
    @Expose
    private String description;

    /**
     * @return The msisdn
     */
    public String getMsisdn() {
        return msisdn;
    }

    /**
     * @param msisdn The msisdn
     */
    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    /**
     * @return The type
     */
    public Integer getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * @return The price
     */
    public Integer getPrice() {
        return price;
    }

    /**
     * @param price The price
     */
    public void setPrice(Integer price) {
        this.price = price;
    }

    /**
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    public MsisdnList(String number){
        this.msisdn = number;
    }
}
