package com.indy.models.msisdn;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterInputResponse;

/**
 * Created by emad on 8/17/16.
 */
public class MsisdenListInput extends MasterInputResponse {
    @SerializedName("storeId")
    @Expose
    private String storeId;
    @SerializedName("reservationId")
    @Expose
    private String reservationId;

    /**
     * @return The storeId
     */
    public String getStoreId() {
        return storeId != "" ? storeId : "indyStores";
    }

    /**
     * @param storeId The storeId
     */
    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    /**
     * @return The reservationId
     */
    public String getReservationId() {
        return reservationId != "" ? reservationId : "A9999990";
    }

    /**
     * @param reservationId The reservationId
     */
    public void setReservationId(String reservationId) {
        this.reservationId = reservationId;
    }

}
