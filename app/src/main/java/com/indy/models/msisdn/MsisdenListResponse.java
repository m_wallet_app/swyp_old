package com.indy.models.msisdn;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by emad on 8/16/16.
 */
public class MsisdenListResponse extends MasterErrorResponse{

    @SerializedName("msisdnList")
    @Expose
    private List<MsisdnList> msisdnList = new ArrayList<MsisdnList>();

    /**
     * @return The msisdnList
     */
    public List<MsisdnList> getMsisdnList() {
        return msisdnList;
    }

    /**
     * @param msisdnList The msisdnList
     */
    public void setMsisdnList(List<MsisdnList> msisdnList) {
        this.msisdnList = msisdnList;
    }
}
