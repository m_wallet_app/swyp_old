package com.indy.models.profilestatus;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by emad on 9/18/16.
 */
public class ProfileStatusOutput extends MasterErrorResponse{
    @SerializedName("profileStatus")
    @Expose
    private Integer profileStatus;
    @SerializedName("passwordStatus")
    @Expose
    private Integer passwordStatus;


    /**
     * @return The profileStatus
     */
    public Integer getProfileStatus() {
        return profileStatus;
    }

    /**
     * @param profileStatus The profileStatus
     */
    public void setProfileStatus(Integer profileStatus) {
        this.profileStatus = profileStatus;
    }

    /**
     * @return The passwordStatus
     */
    public Integer getPasswordStatus() {
        return passwordStatus;
    }

    /**
     * @param passwordStatus The passwordStatus
     */
    public void setPasswordStatus(Integer passwordStatus) {
        this.passwordStatus = passwordStatus;
    }

}
