package com.indy.models.profilestatus;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by emad on 11/19/16.
 */

public class AppFlags implements Parcelable {

    @SerializedName("ubtEnabled")
    @Expose
    private Boolean ubtEnabled;
    @SerializedName("marketplaceEnabled")
    @Expose
    private Boolean marketplaceEnabled;
    @SerializedName("storeEnabled")
    @Expose
    private Boolean storeEnabled;
    @SerializedName("liveChatEnabled")
    @Expose
    private Boolean liveChatEnabled;
    @SerializedName("usageEnabled")
    @Expose
    private Boolean usageEnabled;

    /**
     *
     * @return
     * The ubtEnabled
     */
    public Boolean getUbtEnabled() {
        return ubtEnabled;
    }

    /**
     *
     * @param ubtEnabled
     * The ubtEnabled
     */
    public void setUbtEnabled(Boolean ubtEnabled) {
        this.ubtEnabled = ubtEnabled;
    }

    /**
     *
     * @return
     * The marketplaceEnabled
     */
    public Boolean getMarketplaceEnabled() {
        return marketplaceEnabled;
    }

    /**
     *
     * @param marketplaceEnabled
     * The marketplaceEnabled
     */
    public void setMarketplaceEnabled(Boolean marketplaceEnabled) {
        this.marketplaceEnabled = marketplaceEnabled;
    }

    /**
     *
     * @return
     * The storeEnabled
     */
    public Boolean getStoreEnabled() {
        return storeEnabled;
    }

    /**
     *
     * @param storeEnabled
     * The storeEnabled
     */
    public void setStoreEnabled(Boolean storeEnabled) {
        this.storeEnabled = storeEnabled;
    }

    /**
     *
     * @return
     * The liveChatEnabled
     */
    public Boolean getLiveChatEnabled() {
        return liveChatEnabled;
    }

    /**
     *
     * @param liveChatEnabled
     * The liveChatEnabled
     */
    public void setLiveChatEnabled(Boolean liveChatEnabled) {
        this.liveChatEnabled = liveChatEnabled;
    }

    /**
     *
     * @return
     * The usageEnabled
     */
    public Boolean getUsageEnabled() {
        return usageEnabled;
    }

    /**
     *
     * @param usageEnabled
     * The usageEnabled
     */
    public void setUsageEnabled(Boolean usageEnabled) {
        this.usageEnabled = usageEnabled;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.ubtEnabled);
        dest.writeValue(this.marketplaceEnabled);
        dest.writeValue(this.storeEnabled);
        dest.writeValue(this.liveChatEnabled);
        dest.writeValue(this.usageEnabled);
    }

    public AppFlags() {
    }

    protected AppFlags(Parcel in) {
        this.ubtEnabled = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.marketplaceEnabled = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.storeEnabled = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.liveChatEnabled = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.usageEnabled = (Boolean) in.readValue(Boolean.class.getClassLoader());
    }

    public static final Creator<AppFlags> CREATOR = new Creator<AppFlags>() {
        @Override
        public AppFlags createFromParcel(Parcel source) {
            return new AppFlags(source);
        }

        @Override
        public AppFlags[] newArray(int size) {
            return new AppFlags[size];
        }
    };
}
