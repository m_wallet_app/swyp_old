package com.indy.models.profilestatus;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.login.UserProfile;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by emad on 11/14/16.
 */

public class NewProileStautsOutput extends MasterErrorResponse {

    @SerializedName("profileStatus")
    @Expose
    private Integer profileStatus;
    @SerializedName("appFlags")
    @Expose
    private AppFlags appFlags;
    @SerializedName("userProfile")
    @Expose
    private UserProfile userProfile;
    @SerializedName("passwordStatus")
    @Expose
    private Integer passwordStatus;


    /**
     *
     * @return
     * The profileStatus
     */
    public Integer getProfileStatus() {
        return profileStatus;
    }

    /**
     *
     * @param profileStatus
     * The profileStatus
     */
    public void setProfileStatus(Integer profileStatus) {
        this.profileStatus = profileStatus;
    }

    /**
     *
     * @return
     * The appFlags
     */
    public AppFlags getAppFlags() {
        return appFlags;
    }

    /**
     *
     * @param appFlags
     * The appFlags
     */
    public void setAppFlags(AppFlags appFlags) {
        this.appFlags = appFlags;
    }

    /**
     *
     * @return
     * The userProfile
     */
    public UserProfile getUserProfile() {
        return userProfile;
    }

    /**
     *
     * @param userProfile
     * The userProfile
     */
    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    /**
     *
     * @return
     * The passwordStatus
     */
    public Integer getPasswordStatus() {
        return passwordStatus;
    }

    /**
     *
     * @param passwordStatus
     * The passwordStatus
     */
    public void setPasswordStatus(Integer passwordStatus) {
        this.passwordStatus = passwordStatus;
    }

}
