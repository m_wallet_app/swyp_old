package com.indy.models.acceptTermsConditionsService;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.models.utils.MasterInputResponse;

/**
 * Created by hadi on 19/04/2018.
 */

public class AccpetTermsConditionsServiceInput extends MasterInputResponse {

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("contactNumber")
    @Expose
    private String contactNumber;

    @SerializedName("birthDate")
    @Expose
    private String birthDate;
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }
}
