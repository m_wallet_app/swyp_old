package com.indy.models.rewards;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by emad on 9/27/16.
 */

public class RewardsList implements Parcelable {

    @SerializedName("nameEn")
    @Expose
    private String nameEn;
    @SerializedName("nameAr")
    @Expose
    private String nameAr;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("offerID")
    @Expose
    private String offerID;
    @SerializedName("voucherCode")
    @Expose
    private String voucherCode;
    @SerializedName("offerDescEn")
    @Expose
    private String offerDescEn;
    @SerializedName("offerDescAr")
    @Expose
    private String offerDescAr;
    @SerializedName("offerTnCEn")
    @Expose
    private String offerTnCEn;
    @SerializedName("offerTnCAr")
    @Expose
    private String offerTnCAr;
    @SerializedName("averageSaving")
    @Expose
    private Integer averageSaving;
    @SerializedName("storeLocations")
    @Expose
    private List<StoreLocation> storeLocations = new ArrayList<StoreLocation>();

    @SerializedName("offerCategory")
    @Expose
    private String categoryId;

    private int selectedMarker;
    private int tempPosition;
    public RewardsList(){
        this.setNameEn("Test1");
        this.setAverageSaving(10);
    }
    public RewardsList(Parcel in) {
        nameEn = in.readString();
        nameAr = in.readString();
        imageUrl = in.readString();
        offerID = in.readString();
        voucherCode = in.readString();
        offerDescEn = in.readString();
        offerDescAr = in.readString();
        offerTnCEn = in.readString();
        offerTnCAr = in.readString();
        averageSaving = in.readInt();
//        in.readTypedList(storeLocations,StoreLocation.CREATOR);
        in.readList(storeLocations,StoreLocation.class.getClassLoader());

        categoryId = in.readString();
    }

    public int getTempPosition() {
        return tempPosition;
    }

    public void setTempPosition(int tempPosition) {
        this.tempPosition = tempPosition;
    }

    public int getSelectedMarker() {
        return selectedMarker;
    }

    public void setSelectedMarker(int selectedMarker) {
        this.selectedMarker = selectedMarker;
    }

    public static final Creator<RewardsList> CREATOR = new Creator<RewardsList>() {
        @Override
        public RewardsList createFromParcel(Parcel in) {
            return new RewardsList(in);
        }

        @Override
        public RewardsList[] newArray(int size) {
            return new RewardsList[size];
        }
    };

    /**
     * @return The nameEn
     */
    public String getNameEn() {
        return nameEn;
    }

    /**
     * @param nameEn The nameEn
     */
    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    /**
     * @return The nameAr
     */
    public String getNameAr() {
        return nameAr;
    }

    /**
     * @param nameAr The nameAr
     */
    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }

    /**
     * @return The price
     */
    public String getOfferID() {
        return offerID;
    }

    public void setOfferID(String offerID) {
        this.offerID = offerID;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public String getOfferDescEn() {
        return offerDescEn;
    }

    public void setOfferDescEn(String offerDescEn) {
        this.offerDescEn = offerDescEn;
    }

    public String getOfferDescAr() {
        return offerDescAr;
    }

    public void setOfferDescAr(String offerDescAr) {
        this.offerDescAr = offerDescAr;
    }

    public String getOfferTnCEn() {
        return offerTnCEn;
    }

    public void setOfferTnCEn(String offerTnCEn) {
        this.offerTnCEn = offerTnCEn;
    }

    public String getOfferTnCAr() {
        return offerTnCAr;
    }

    public void setOfferTnCAr(String offerTnCAr) {
        this.offerTnCAr = offerTnCAr;
    }

    public Integer getAverageSaving() {
        return averageSaving;
    }

    public void setAverageSaving(Integer averageSaving) {
        this.averageSaving = averageSaving;
    }


    /**
     * @return The imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * @param imageUrl The imageUrl
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     * @return The storeLocations
     */
    public List<StoreLocation> getStoreLocations() {
        return storeLocations;
    }

    /**
     * @param storeLocations The storeLocations
     */
    public void setStoreLocations(List<StoreLocation> storeLocations) {
        this.storeLocations = storeLocations;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nameEn);
        dest.writeString(nameAr);
        dest.writeString(imageUrl);
        dest.writeString(offerID);
        dest.writeString(voucherCode);
        dest.writeString(offerDescEn);
        dest.writeString(offerDescAr);
        dest.writeString(offerTnCEn);
        dest.writeString(offerTnCAr);
        dest.writeInt(averageSaving);
        dest.writeList(storeLocations);
        dest.writeString(categoryId);
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }
}
