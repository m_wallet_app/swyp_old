package com.indy.models.rewards;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by emad on 9/27/16.
 */

public class AvilableRewardsoutput extends MasterErrorResponse {

    @SerializedName("rewardList")
    @Expose
    private List<RewardsList> rewardList = new ArrayList<RewardsList>();
    @SerializedName("monthlySavings")
    @Expose
    private Integer monthlySavings;

    @SerializedName("expiryDateEn")
    @Expose
    private String expiryDateEn;

    @SerializedName("expiryDateAr")
    @Expose
    private String expiryDateAr;
    /**
     * @return The rewardList
     */
    public List<RewardsList> getRewardList() {
        return rewardList;
    }

    /**
     * @param rewardList The rewardList
     */
    public void setRewardList(List<RewardsList> rewardList) {
        this.rewardList = rewardList;
    }

    /**
     * @return The monthlySavings
     */
    public Integer getMonthlySavings() {
        return monthlySavings;
    }

    /**
     * @param monthlySavings The monthlySavings
     */
    public void setMonthlySavings(Integer monthlySavings) {
        this.monthlySavings = monthlySavings;
    }

    public String getExpiryDateEn() {
        return expiryDateEn;
    }

    public void setExpiryDateEn(String expiryDateEn) {
        this.expiryDateEn = expiryDateEn;
    }

    public String getExpiryDateAr() {
        return expiryDateAr;
    }

    public void setExpiryDateAr(String expiryDateAr) {
        this.expiryDateAr = expiryDateAr;
    }
}
