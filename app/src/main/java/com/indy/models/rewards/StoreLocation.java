package com.indy.models.rewards;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by emad on 9/27/16.
 */

public class StoreLocation implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("nameEn")
    @Expose
    private String nameEn;
    @SerializedName("nameAr")
    @Expose
    private String nameAr;
    @SerializedName("addressEn")
    @Expose
    private String addressEn;
    @SerializedName("addressAr")
    @Expose
    private String addressAr;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("geoLocation")
    @Expose
    private GeoLocation geoLocation;
    @SerializedName("descriptionEn")
    @Expose
    private String descriptionEn;
    @SerializedName("descriptionAr")
    @Expose
    private String descriptionAr;

    double distanceFromUser;
    protected StoreLocation(Parcel in) {
        id = in.readString();
        nameEn = in.readString();
        nameAr = in.readString();
        addressEn = in.readString();
        addressAr = in.readString();
        descriptionEn = in.readString();
        descriptionAr = in.readString();
        geoLocation = in.readParcelable(GeoLocation.class.getClassLoader());
        distanceFromUser = in.readDouble();
    }

    public static final Creator<StoreLocation> CREATOR = new Creator<StoreLocation>() {
        @Override
        public StoreLocation createFromParcel(Parcel in) {
            return new StoreLocation(in);
        }

        @Override
        public StoreLocation[] newArray(int size) {
            return new StoreLocation[size];
        }
    };

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The nameEn
     */
    public String getNameEn() {
        return nameEn;
    }

    /**
     * @param nameEn The nameEn
     */
    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    /**
     * @return The nameAr
     */
    public String getNameAr() {
        return nameAr;
    }

    /**
     * @param nameAr The nameAr
     */
    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }

    /**
     * @return The addressEn
     */
    public String getAddressEn() {
        return addressEn;
    }

    /**
     * @param addressEn The addressEn
     */
    public void setAddressEn(String addressEn) {
        this.addressEn = addressEn;
    }

    /**
     * @return The addressAr
     */
    public String getAddressAr() {
        return addressAr;
    }

    /**
     * @param addressAr The addressAr
     */
    public void setAddressAr(String addressAr) {
        this.addressAr = addressAr;
    }

    /**
     * @return The type
     */
    public Integer getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * @return The geoLocation
     */
    public GeoLocation getGeoLocation() {
        return geoLocation;
    }

    /**
     * @param geoLocation The geoLocation
     */
    public void setGeoLocation(GeoLocation geoLocation) {
        this.geoLocation = geoLocation;
    }

    /**
     * @return The descriptionEn
     */
    public String getDescriptionEn() {
        return descriptionEn;
    }

    /**
     * @param descriptionEn The descriptionEn
     */
    public void setDescriptionEn(String descriptionEn) {
        this.descriptionEn = descriptionEn;
    }

    /**
     * @return The descriptionAr
     */
    public String getDescriptionAr() {
        return descriptionAr;
    }

    /**
     * @param descriptionAr The descriptionAr
     */
    public void setDescriptionAr(String descriptionAr) {
        this.descriptionAr = descriptionAr;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(nameEn);
        dest.writeString(nameAr);
        dest.writeString(addressEn);
        dest.writeString(addressAr);
        dest.writeString(descriptionEn);
        dest.writeString(descriptionAr);
        dest.writeParcelable(geoLocation,flags);
        dest.writeDouble(distanceFromUser);
    }


    public double getDistanceFromUser() {
        return distanceFromUser;
    }

    public void setDistanceFromUser(double distanceFromUser) {
        this.distanceFromUser = distanceFromUser;
    }
}
