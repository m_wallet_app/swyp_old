package com.indy.models.rewards;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by emad on 9/27/16.
 */

public class UsedRewardsList implements Parcelable{

    @SerializedName("nameEn")
    @Expose
    private String nameEn;
    @SerializedName("nameAr")
    @Expose
    private String nameAr;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("offerID")
    @Expose
    private String offerID;
    @SerializedName("voucherCode")
    @Expose
    private String voucherCode;
    @SerializedName("averageSaving")
    @Expose
    private Integer averageSaving;
    @SerializedName("redeemedDate")
    @Expose
    private String redeemedDate;

    protected UsedRewardsList(Parcel in) {
        nameEn = in.readString();
        nameAr = in.readString();
        imageUrl = in.readString();
        offerID = in.readString();
        voucherCode = in.readString();
        redeemedDate = in.readString();
    }

    public static final Creator<UsedRewardsList> CREATOR = new Creator<UsedRewardsList>() {
        @Override
        public UsedRewardsList createFromParcel(Parcel in) {
            return new UsedRewardsList(in);
        }

        @Override
        public UsedRewardsList[] newArray(int size) {
            return new UsedRewardsList[size];
        }
    };

    /**
     *
     * @return
     * The nameEn
     */
    public String getNameEn() {
        return nameEn;
    }

    /**
     *
     * @param nameEn
     * The nameEn
     */
    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    /**
     *
     * @return
     * The nameAr
     */
    public String getNameAr() {
        return nameAr;
    }

    /**
     *
     * @param nameAr
     * The nameAr
     */
    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }

    /**
     *
     * @return
     * The imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     *
     * @param imageUrl
     * The imageUrl
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     *
     * @return
     * The offerID
     */
    public String getOfferID() {
        return offerID;
    }

    /**
     *
     * @param offerID
     * The offerID
     */
    public void setOfferID(String offerID) {
        this.offerID = offerID;
    }

    /**
     *
     * @return
     * The voucherCode
     */
    public String getVoucherCode() {
        return voucherCode;
    }

    /**
     *
     * @param voucherCode
     * The voucherCode
     */
    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    /**
     *
     * @return
     * The averageSaving
     */
    public Integer getAverageSaving() {
        return averageSaving;
    }

    /**
     *
     * @param averageSaving
     * The averageSaving
     */
    public void setAverageSaving(Integer averageSaving) {
        this.averageSaving = averageSaving;
    }

    /**
     *
     * @return
     * The redeemedDate
     */
    public String getRedeemedDate() {
        return redeemedDate;
    }

    /**
     *
     * @param redeemedDate
     * The redeemedDate
     */
    public void setRedeemedDate(String redeemedDate) {
        this.redeemedDate = redeemedDate;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(nameEn);
        parcel.writeString(nameAr);
        parcel.writeString(imageUrl);
        parcel.writeString(offerID);
        parcel.writeString(redeemedDate);
        parcel.writeString(voucherCode);
    }
}
