package com.indy.models.getTokenForPassword;

import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by mobile on 10/01/2017.
 */

public class GetPreLoginOutput extends MasterErrorResponse {

    @SerializedName("identifier")
    String identifier;

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }
}
