package com.indy.models.onboarding_push_notifications;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterInputResponse;

public class LogPromoCodeDetailsRequestModel extends MasterInputResponse {

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getPromoCodeType() {
        return promoCodeType;
    }

    public void setPromoCodeType(String promoCodeType) {
        this.promoCodeType = promoCodeType;
    }

    public String getPromoCodeDescription() {
        return promoCodeDescription;
    }

    public void setPromoCodeDescription(String promoCodeDescription) {
        this.promoCodeDescription = promoCodeDescription;
    }

    @SerializedName("promoCode")
    @Expose
    private String promoCode;

    @SerializedName("promoCodeType")
    @Expose
    private String promoCodeType;

    @SerializedName("promoCodeDescription")
    @Expose
    private String promoCodeDescription;

}
