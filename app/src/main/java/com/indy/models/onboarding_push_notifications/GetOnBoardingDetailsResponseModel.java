package com.indy.models.onboarding_push_notifications;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

public class GetOnBoardingDetailsResponseModel extends MasterErrorResponse implements Parcelable {


    @SerializedName("profileId")
    @Expose
    private String profileId;
    @SerializedName("profileStatus")
    @Expose
    private String profileStatus;
    @SerializedName("msisdn")
    @Expose
    private String msisdn;
    @SerializedName("fullName")
    @Expose
    private String fullName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("birthDate")
    @Expose
    private String birthDate;
    @SerializedName("contactMsisdn")
    @Expose
    private String contactMsisdn;
    @SerializedName("addressLine1")
    @Expose
    private String addressLine1;
    @SerializedName("addressLine2")
    @Expose
    private String addressLine2;
    @SerializedName("guadrianInfo")
    @Expose
    private String guadrianInfo;
    @SerializedName("shippingInfo")
    @Expose
    private String shippingInfo;
    @SerializedName("activationCode")
    @Expose
    private String activationCode;
    @SerializedName("invitationCode")
    @Expose
    private String invitationCode;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("nickName")
    @Expose
    private String nickName;
    @SerializedName("emiratesIdStatus")
    @Expose
    private String emiratesIdStatus;
    @SerializedName("deliveryInfo")
    @Expose
    private String deliveryInfo;
    @SerializedName("emiratesId")
    @Expose
    private String emiratesId;
    @SerializedName("indyPackageCode")
    @Expose
    private String indyPackageCode;
    @SerializedName("profileCreated")
    @Expose
    private String profileCreated;
    @SerializedName("migrationStatus")
    @Expose
    private String migrationStatus;
    @SerializedName("preferredLang")
    @Expose
    private String preferredLang;
    @SerializedName("promoCode")
    @Expose
    private String promoCode;
    @SerializedName("promoCodeType")
    @Expose
    private String promoCodeType;
    @SerializedName("promoCodeDescription")
    @Expose
    private String promoCodeDescription;
    @SerializedName("orderType")
    @Expose
    private String orderType;
    @SerializedName("contactNumber")
    @Expose
    private String contactNumber;

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public String getProfileStatus() {
        return profileStatus;
    }

    public void setProfileStatus(String profileStatus) {
        this.profileStatus = profileStatus;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getContactMsisdn() {
        return contactMsisdn;
    }

    public void setContactMsisdn(String contactMsisdn) {
        this.contactMsisdn = contactMsisdn;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getGuadrianInfo() {
        return guadrianInfo;
    }

    public void setGuadrianInfo(String guadrianInfo) {
        this.guadrianInfo = guadrianInfo;
    }

    public String getShippingInfo() {
        return shippingInfo;
    }

    public void setShippingInfo(String shippingInfo) {
        this.shippingInfo = shippingInfo;
    }

    public String getActivationCode() {
        return activationCode;
    }

    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }

    public String getInvitationCode() {
        return invitationCode;
    }

    public void setInvitationCode(String invitationCode) {
        this.invitationCode = invitationCode;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getEmiratesIdStatus() {
        return emiratesIdStatus;
    }

    public void setEmiratesIdStatus(String emiratesIdStatus) {
        this.emiratesIdStatus = emiratesIdStatus;
    }

    public String getDeliveryInfo() {
        return deliveryInfo;
    }

    public void setDeliveryInfo(String deliveryInfo) {
        this.deliveryInfo = deliveryInfo;
    }

    public String getEmiratesId() {
        return emiratesId;
    }

    public void setEmiratesId(String emiratesId) {
        this.emiratesId = emiratesId;
    }

    public String getIndyPackageCode() {
        return indyPackageCode;
    }

    public void setIndyPackageCode(String indyPackageCode) {
        this.indyPackageCode = indyPackageCode;
    }

    public String getProfileCreated() {
        return profileCreated;
    }

    public void setProfileCreated(String profileCreated) {
        this.profileCreated = profileCreated;
    }

    public String getMigrationStatus() {
        return migrationStatus;
    }

    public void setMigrationStatus(String migrationStatus) {
        this.migrationStatus = migrationStatus;
    }

    public String getPreferredLang() {
        return preferredLang;
    }

    public void setPreferredLang(String preferredLang) {
        this.preferredLang = preferredLang;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getPromoCodeType() {
        return promoCodeType;
    }

    public void setPromoCodeType(String promoCodeType) {
        this.promoCodeType = promoCodeType;
    }

    public String getPromoCodeDescription() {
        return promoCodeDescription;
    }

    public void setPromoCodeDescription(String promoCodeDescription) {
        this.promoCodeDescription = promoCodeDescription;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.profileId);
        dest.writeString(this.profileStatus);
        dest.writeString(this.msisdn);
        dest.writeString(this.fullName);
        dest.writeString(this.email);
        dest.writeString(this.birthDate);
        dest.writeString(this.contactMsisdn);
        dest.writeString(this.addressLine1);
        dest.writeString(this.addressLine2);
        dest.writeString(this.guadrianInfo);
        dest.writeString(this.shippingInfo);
        dest.writeString(this.activationCode);
        dest.writeString(this.invitationCode);
        dest.writeString(this.gender);
        dest.writeString(this.language);
        dest.writeString(this.nickName);
        dest.writeString(this.emiratesIdStatus);
        dest.writeString(this.deliveryInfo);
        dest.writeString(this.emiratesId);
        dest.writeString(this.indyPackageCode);
        dest.writeString(this.profileCreated);
        dest.writeString(this.migrationStatus);
        dest.writeString(this.preferredLang);
        dest.writeString(this.promoCode);
        dest.writeString(this.promoCodeType);
        dest.writeString(this.promoCodeDescription);
        dest.writeString(this.orderType);
        dest.writeString(this.contactNumber);
    }

    public GetOnBoardingDetailsResponseModel() {
    }

    protected GetOnBoardingDetailsResponseModel(Parcel in) {
        this.profileId = in.readString();
        this.profileStatus = in.readString();
        this.msisdn = in.readString();
        this.fullName = in.readString();
        this.email = in.readString();
        this.birthDate = in.readString();
        this.contactMsisdn = in.readString();
        this.addressLine1 = in.readString();
        this.addressLine2 = in.readString();
        this.guadrianInfo = in.readString();
        this.shippingInfo = in.readString();
        this.activationCode = in.readString();
        this.invitationCode = in.readString();
        this.gender = in.readString();
        this.language = in.readString();
        this.nickName = in.readString();
        this.emiratesIdStatus = in.readString();
        this.deliveryInfo = in.readString();
        this.emiratesId = in.readString();
        this.indyPackageCode = in.readString();
        this.profileCreated = in.readString();
        this.migrationStatus = in.readString();
        this.preferredLang = in.readString();
        this.promoCode = in.readString();
        this.promoCodeType = in.readString();
        this.promoCodeDescription = in.readString();
        this.orderType = in.readString();
        this.contactNumber = in.readString();
    }

    public static final Creator<GetOnBoardingDetailsResponseModel> CREATOR = new Creator<GetOnBoardingDetailsResponseModel>() {
        @Override
        public GetOnBoardingDetailsResponseModel createFromParcel(Parcel source) {
            return new GetOnBoardingDetailsResponseModel(source);
        }

        @Override
        public GetOnBoardingDetailsResponseModel[] newArray(int size) {
            return new GetOnBoardingDetailsResponseModel[size];
        }
    };
}
