package com.indy.models.onboarding_push_notifications;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

public class LandingPageResponseModel extends MasterErrorResponse {

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String regId) {
        this.registrationId = regId;
    }

    @SerializedName("registrationId")
    @Expose
    private String registrationId;

}
