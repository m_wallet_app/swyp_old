package com.indy.models.onboarding_push_notifications;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterInputResponse;

public class LogEIDDetailsRequestModel extends MasterInputResponse {

    public String getEmiratesid() {
        return emiratesid;
    }

    public void setEmiratesid(String emiratesid) {
        this.emiratesid = emiratesid;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    @SerializedName("emiratesid")
    @Expose
    private String emiratesid;

    @SerializedName("fullName")
    @Expose
    private String fullName;

    @SerializedName("birthDate")
    @Expose
    private String birthDate;

}
