package com.indy.models.onboarding_push_notifications;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterInputResponse;

public class LogContactDetailsRequestModel extends MasterInputResponse {

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("contactNumber")
    @Expose
    private String contactNumber;
    @SerializedName("address")
    @Expose
    private String address;

}
