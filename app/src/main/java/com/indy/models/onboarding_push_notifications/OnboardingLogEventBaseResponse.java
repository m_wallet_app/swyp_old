package com.indy.models.onboarding_push_notifications;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

public class OnboardingLogEventBaseResponse extends MasterErrorResponse {

    @SerializedName("responseCode")
    @Expose
    private String responseCode;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    @SerializedName("responseMsg")
    @Expose
    private String responseMsg;



}
