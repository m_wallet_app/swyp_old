package com.indy.models.search_msisdn_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterInputResponse;

public class SearchMsisdnInputModel extends MasterInputResponse {

    @SerializedName("storeId")
    @Expose
    private String storeId;
    @SerializedName("reservationId")
    @Expose
    private String reservationId;
    @SerializedName("searchString")
    @Expose
    private String searchString;

    /**
     * @return The storeId
     */
    public String getStoreId() {
        return storeId;
    }

    /**
     * @param storeId The storeId
     */
    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    /**
     * @return The reservationId
     */
    public String getReservationId() {
        return reservationId;
    }

    /**
     * @param reservationId The reservationId
     */
    public void setReservationId(String reservationId) {
        this.reservationId = reservationId;
    }

    public String getSearchString() {
        return searchString;
    }


    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }
}
