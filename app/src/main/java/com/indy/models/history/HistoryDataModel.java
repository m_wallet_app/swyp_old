package com.indy.models.history;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Amir.jehangir on 10/18/2016.
 */
public class HistoryDataModel {
    @SerializedName("headerTitle")
    @Expose
    private String headerTitle;

    @SerializedName("balance")
    @Expose
    private String balance;

    @SerializedName("monthlyTransList")
    @Expose
    private MonthlyTransList[] monthlyTransList;

    @SerializedName("allItemsInSection")
    @Expose
    private ArrayList<String> allItemsInSection;


    public HistoryDataModel() {

    }
    public HistoryDataModel(String headerTitle, ArrayList<String> allItemsInSection) {
        this.headerTitle = headerTitle;
        this.allItemsInSection = allItemsInSection;
    }



    public String getHeaderTitle() {
        return headerTitle;
    }

    public void setHeaderTitle(String headerTitle) {
        this.headerTitle = headerTitle;
    }

    public ArrayList<String> getAllItemsInSection() {
        return allItemsInSection;
    }

    public void setAllItemsInSection(ArrayList<String> allItemsInSection) {
        this.allItemsInSection = allItemsInSection;
    }



    public String getBalance ()
    {
        return balance;
    }

    public void setBalance (String balance)
    {
        this.balance = balance;
    }


    public MonthlyTransList[] getMonthlyTransList ()
    {
        return monthlyTransList;
    }

    public void setMonthlyTransList (MonthlyTransList[] monthlyTransList)
    {
        this.monthlyTransList = monthlyTransList;
    }


}
