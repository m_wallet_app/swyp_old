package com.indy.models.history;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by Amir.jehangir on 10/18/2016.
 */
public class HistoryDataOutput extends MasterErrorResponse {
    @SerializedName("balance")
    @Expose
    private String balance;

    @SerializedName("monthlyTransList")
    @Expose
    private MonthlyTransList[] monthlyTransList;

    public String getBalance ()
    {
        return balance;
    }

    public void setBalance (String balance)
    {
        this.balance = balance;
    }

    public MonthlyTransList[] getMonthlyTransList ()
    {
        return monthlyTransList;
    }

    public void setMonthlyTransList (MonthlyTransList[] monthlyTransList)
    {
        this.monthlyTransList = monthlyTransList;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [balance = "+balance+", monthlyTransList = "+monthlyTransList+"]";
    }
}
