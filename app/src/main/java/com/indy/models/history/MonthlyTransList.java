package com.indy.models.history;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Amir.jehangir on 10/18/2016.
 */
public class MonthlyTransList {
    @SerializedName("monthAr")
    @Expose
    private String monthAr;

    @SerializedName("transactionList")
    @Expose
    private TransactionList[] transactionList;

    @SerializedName("monthEn")
    @Expose
    private String monthEn;

    public String getMonthAr ()
    {
        return monthAr;
    }

    public void setMonthAr (String monthAr)
    {
        this.monthAr = monthAr;
    }

    public TransactionList[] getTransactionList ()
    {
        return transactionList;
    }

    public void setTransactionList (TransactionList[] transactionList)
    {
        this.transactionList = transactionList;
    }

    public String getMonthEn ()
    {
        return monthEn;
    }

    public void setMonthEn (String monthEn)
    {
        this.monthEn = monthEn;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [monthAr = "+monthAr+", transactionList = "+transactionList+", monthEn = "+monthEn+"]";
    }
}
