package com.indy.models.paymnet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterInputResponse;

/**
 * Created by emad on 9/19/16.
 */
public class PaymentInputModel extends MasterInputResponse{

    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("shippingMethod")
    @Expose
    private String shippingMethod;
    @SerializedName("orderType")
    @Expose
    private String orderType;
    @SerializedName("deliveryInfo")
    @Expose
    private DeliveryInfo deliveryInfo;

    @SerializedName("emiratesId")
    @Expose
    private String emiratesId;

    /**
     * @return The registrationId
     */

    public String getAmount() {
        return amount;
    }

    /**
     * @param amount The amount
     */
    public void setAmount(String amount) {
        this.amount = amount;
    }

    /**
     * @return The shippingMethod
     */
    public String getShippingMethod() {
        return shippingMethod;
    }

    /**
     * @param shippingMethod The shippingMethod
     */
    public void setShippingMethod(String shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    /**
     * @return The orderType
     */
    public String getOrderType() {
        return orderType;
    }

    /**
     * @param orderType The orderType
     */
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    /**
     * @return The deliveryInfo
     */
    public DeliveryInfo getDeliveryInfo() {
        return deliveryInfo;
    }

    /**
     * @param deliveryInfo The deliveryInfo
     */
    public void setDeliveryInfo(DeliveryInfo deliveryInfo) {
        this.deliveryInfo = deliveryInfo;
    }

    public String getEmiratesId() {
        return emiratesId;
    }

    public void setEmiratesId(String emiratesId) {
        this.emiratesId = emiratesId;
    }
}
