package com.indy.models.location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterInputResponse;

/**
 * Created by emad on 8/17/16.
 */
public class LocationInputModel extends MasterInputResponse {

    @SerializedName("geoLocation")
    @Expose
    private GeoLocation geoLocation;
    @SerializedName("size")
    @Expose
    private Integer size;

    /**
     * @return The geoLocation
     */
    public GeoLocation getGeoLocation() {
        return geoLocation;
    }

    /**
     * @param geoLocation The geoLocation
     */
    public void setGeoLocation(GeoLocation geoLocation) {
        this.geoLocation = geoLocation;
    }

    /**
     * @return The size
     */
    public Integer getSize() {
        return size;
    }

    /**
     * @param size The size
     */
    public void setSize(Integer size) {
        this.size = size;
    }


}
