package com.indy.models.location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by emad on 8/17/16.
 */
public class NearestLocationList {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("nameEn")
    @Expose
    private String nameEn;
    @SerializedName("nameAr")
    @Expose
    private String nameAr;
    @SerializedName("addressEn")
    @Expose
    private String addressEn;
    @SerializedName("addressAr")
    @Expose
    private String addressAr;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("geoLocation")
    @Expose
    private GeoLocation geoLocation;
    @SerializedName("distance")
    @Expose
    private Double distance;
    @SerializedName("descriptionEn")
    @Expose
    private String descriptionEn;
    @SerializedName("descriptionAr")
    @Expose
    private String descriptionAr;

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The nameEn
     */
    public String getNameEn() {
        return nameEn;
    }

    /**
     * @param nameEn The nameEn
     */
    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    /**
     * @return The nameAr
     */
    public String getNameAr() {
        return nameAr;
    }

    /**
     * @param nameAr The nameAr
     */
    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }

    /**
     * @return The addressEn
     */
    public String getAddressEn() {
        return addressEn;
    }

    /**
     * @param addressEn The addressEn
     */
    public void setAddressEn(String addressEn) {
        this.addressEn = addressEn;
    }

    /**
     * @return The addressAr
     */
    public String getAddressAr() {
        return addressAr;
    }

    /**
     * @param addressAr The addressAr
     */
    public void setAddressAr(String addressAr) {
        this.addressAr = addressAr;
    }

    /**
     * @return The type
     */
    public Integer getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * @return The geoLocation
     */
    public GeoLocation getGeoLocation() {
        return geoLocation;
    }

    /**
     * @param geoLocation The geoLocation
     */
    public void setGeoLocation(GeoLocation geoLocation) {
        this.geoLocation = geoLocation;
    }

    /**
     * @return The distance
     */
    public Double getDistance() {
        try {

            NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
            nf.setMaximumFractionDigits(1);
            DecimalFormat df = (DecimalFormat) nf;

//            DecimalFormat decimalFormat = new DecimalFormat();
//            decimalFormat.applyLocalizedPattern("#.0");
            String doubleTemp = df.format(distance);
            Double valToReturn = Double.parseDouble(doubleTemp);
//            masterActivity.sharedPrefrencesManger.setLanguage(currentLanguage);
            return valToReturn;

        }catch (Exception ex) {
            return distance;
        }
    }

    /**
     * @param distance The distance
     */
    public void setDistance(Double distance) {
        this.distance = distance;
    }

    /**
     * @return The descriptionEn
     */
    public String getDescriptionEn() {
        return descriptionEn;
    }

    /**
     * @param descriptionEn The descriptionEn
     */
    public void setDescriptionEn(String descriptionEn) {
        this.descriptionEn = descriptionEn;
    }

    /**
     * @return The descriptionAr
     */
    public String getDescriptionAr() {
        return descriptionAr;
    }

    /**
     * @param descriptionAr The descriptionAr
     */
    public void setDescriptionAr(String descriptionAr) {
        this.descriptionAr = descriptionAr;
    }

}
