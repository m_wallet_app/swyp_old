package com.indy.models.location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by emad on 8/14/16.
 */
public class LocationModelResponse extends MasterErrorResponse {

    @SerializedName("nearestLocationList")
    @Expose
    private List<NearestLocationList> nearestLocationList = new ArrayList<NearestLocationList>();

    /**
     * @return The nearestLocationList
     */
    public List<NearestLocationList> getNearestLocationList() {
        return nearestLocationList;
    }

    /**
     * @param nearestLocationList The nearestLocationList
     */
    public void setNearestLocationList(List<NearestLocationList> nearestLocationList) {
        this.nearestLocationList = nearestLocationList;
    }

}
