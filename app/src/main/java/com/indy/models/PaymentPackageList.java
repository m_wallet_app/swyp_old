package com.indy.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by emad on 8/15/16.
 */
public class PaymentPackageList {
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("consumption")
    @Expose
    private Integer consumption;
    @SerializedName("consumptionUnitEn")
    @Expose
    private String consumptionUnitEn;
    @SerializedName("consumptionUnitAr")
    @Expose
    private String consumptionUnitAr;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("amountUnitEn")
    @Expose
    private String amountUnitEn;
    @SerializedName("amountUnitAr")
    @Expose
    private String amountUnitAr;

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The consumption
     */
    public Integer getConsumption() {
        return consumption;
    }

    /**
     * @param consumption The consumption
     */
    public void setConsumption(Integer consumption) {
        this.consumption = consumption;
    }

    /**
     * @return The consumptionUnitEn
     */
    public String getConsumptionUnitEn() {
        return consumptionUnitEn;
    }

    /**
     * @param consumptionUnitEn The consumptionUnitEn
     */
    public void setConsumptionUnitEn(String consumptionUnitEn) {
        this.consumptionUnitEn = consumptionUnitEn;
    }

    /**
     * @return The consumptionUnitAr
     */
    public String getConsumptionUnitAr() {
        return consumptionUnitAr;
    }

    /**
     * @param consumptionUnitAr The consumptionUnitAr
     */
    public void setConsumptionUnitAr(String consumptionUnitAr) {
        this.consumptionUnitAr = consumptionUnitAr;
    }

    /**
     * @return The amount
     */
    public Integer getAmount() {
        return amount;
    }

    /**
     * @param amount The amount
     */
    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    /**
     * @return The amountUnitEn
     */
    public String getAmountUnitEn() {
        return amountUnitEn;
    }

    /**
     * @param amountUnitEn The amountUnitEn
     */
    public void setAmountUnitEn(String amountUnitEn) {
        this.amountUnitEn = amountUnitEn;
    }

    /**
     * @return The amountUnitAr
     */
    public String getAmountUnitAr() {
        return amountUnitAr;
    }

    /**
     * @param amountUnitAr The amountUnitAr
     */
    public void setAmountUnitAr(String amountUnitAr) {
        this.amountUnitAr = amountUnitAr;
    }

}
