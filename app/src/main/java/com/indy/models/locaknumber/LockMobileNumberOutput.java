package com.indy.models.locaknumber;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by emad on 9/18/16.
 */
public class LockMobileNumberOutput extends MasterErrorResponse{
    @SerializedName("status")
    @Expose
    private Integer status;

    /**
     * @return The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

}
