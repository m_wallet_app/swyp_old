package com.indy.models.locaknumber;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterInputResponse;

/**
 * Created by emad on 9/18/16.
 */
public class LockMobileNumberInput extends MasterInputResponse {

    @SerializedName("storeId")
    @Expose
    private String storeId;
    @SerializedName("reservationId")
    @Expose
    private String reservationId;
    @SerializedName("targetMsisdn")
    @Expose
    private String targetMsisdn;

    /**
     * @return The storeId
     */
    public String getStoreId() {
        return storeId;
    }

    /**
     * @param storeId The storeId
     */
    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    /**
     * @return The reservationId
     */
    public String getReservationId() {
        return reservationId;
    }

    /**
     * @param reservationId The reservationId
     */
    public void setReservationId(String reservationId) {
        this.reservationId = reservationId;
    }

    /**
     * @return The targetMsisdn
     */
    public String getTargetMsisdn() {
        return targetMsisdn;
    }

    /**
     * @param targetMsisdn The targetMsisdn
     */
    public void setTargetMsisdn(String targetMsisdn) {
        this.targetMsisdn = targetMsisdn;
    }

}

