package com.indy.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by mobile on 25/01/2018.
 */

public class LogEventInputModel implements Serializable{


    @SerializedName("authToken")
    @Expose
    private String authToken;
    @SerializedName("appVersion")
    @Expose
    private String appVersion;
    @SerializedName("msisdn")
    @Expose
    private String msisdn;
    @SerializedName("osVersion")
    @Expose
    private String osVersion;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("channel")
    @Expose
    private String channel;
    @SerializedName("lang")
    @Expose
    private String lang;
    @SerializedName("imsi")
    @Expose
    private String imsi;
    @SerializedName("deviceId")
    @Expose
    private String deviceId;
    @SerializedName("additionalInfo")
    @Expose
    private String additionalInfo;
    @SerializedName("actionTransactionId")
    @Expose
    private String actionTransactionId;
    @SerializedName("screenId")
    @Expose
    private String screenId;
    @SerializedName("signUp")
    @Expose
    private String signUp;
    @SerializedName("nearestLocation")
    @Expose
    private String nearestLocation;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("fullName")
    @Expose
    private String fullName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("day")
    @Expose
    private String day;
    @SerializedName("month")
    @Expose
    private String month;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("accept")
    @Expose
    private String accept;
    @SerializedName("confirmOrCancel")
    @Expose
    private String confirmOrCancel;
    @SerializedName("selectedNumber")
    @Expose
    private String selectedNumber;
    @SerializedName("searchNumber")
    @Expose
    private String searchNumber;
    @SerializedName("keepExistingNumber")
    @Expose
    private String keepExistingNumber;
    @SerializedName("packageSelected")
    @Expose
    private String packageSelected;
    @SerializedName("deliveryMethod")
    @Expose
    private String deliveryMethod;
    @SerializedName("contactNumber")
    @Expose
    private String contactNumber;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("area")
    @Expose
    private String area;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("checkout")
    @Expose
    private String checkout;
    @SerializedName("pay")
    @Expose
    private String pay;
    @SerializedName("tryAgain")
    @Expose
    private String tryAgain;
    @SerializedName("migrationNumber")
    @Expose
    private String migrationNumber;
    @SerializedName("nonEtisalatNewNumber")
    @Expose
    private String nonEtisalatNewNumber;
    @SerializedName("emiratesId")
    @Expose
    private String emiratesId;
    @SerializedName("eidFailureReviewDetails")
    @Expose
    private String eidFailureReviewDetails;
    @SerializedName("eidFailureLocateShop")
    @Expose
    private String eidFailureLocateShop;
    @SerializedName("eidFailureNewNumber")
    @Expose
    private String eidFailureNewNumber;
    @SerializedName("reviewFullName")
    @Expose
    private String reviewFullName;
    @SerializedName("reviewEmiratesId")
    @Expose
    private String reviewEmiratesId;
    @SerializedName("reviewVerify")
    @Expose
    private String reviewVerify;
    @SerializedName("eligibilityFailureOutstandingAmountNewNumber")
    @Expose
    private String eligibilityFailureOutstandingAmountNewNumber;
    @SerializedName("eligibilityFailureAccountRestrictionsNewNumber")
    @Expose
    private String eligibilityFailureAccountRestrictionsNewNumber;
    @SerializedName("eligibilityFailureAccountRestrictionsLiveChat")
    @Expose
    private String eligibilityFailureAccountRestrictionsLiveChat;
    @SerializedName("migratedNumber")
    @Expose
    private String migratedNumber;
    @SerializedName("isEligibleNewNumber")
    @Expose
    private String isEligibleNewNumber;
    @SerializedName("otp")
    @Expose
    private String otp;

    @SerializedName("progressStatus")
    @Expose
    private String progressStatus;

    @SerializedName("errorCode")
    @Expose
    private String errorCode;

    public final static Parcelable.Creator<LogEventInputModel> CREATOR = new Parcelable.Creator<LogEventInputModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public LogEventInputModel createFromParcel(Parcel in) {
            return new LogEventInputModel(in);
        }

        public LogEventInputModel[] newArray(int size) {
            return (new LogEventInputModel[size]);
        }

    }
            ;

    protected LogEventInputModel(Parcel in) {
        this.authToken = ((String) in.readValue((String.class.getClassLoader())));
        this.appVersion = ((String) in.readValue((String.class.getClassLoader())));
        this.msisdn = ((String) in.readValue((String.class.getClassLoader())));
        this.osVersion = ((String) in.readValue((String.class.getClassLoader())));
        this.token = ((String) in.readValue((String.class.getClassLoader())));
        this.channel = ((String) in.readValue((String.class.getClassLoader())));
        this.lang = ((String) in.readValue((String.class.getClassLoader())));
        this.imsi = ((String) in.readValue((String.class.getClassLoader())));
        this.deviceId = ((String) in.readValue((String.class.getClassLoader())));
        this.additionalInfo = ((String) in.readValue((String.class.getClassLoader())));
        this.actionTransactionId = ((String) in.readValue((String.class.getClassLoader())));
        this.screenId = ((String) in.readValue((String.class.getClassLoader())));
        this.signUp = ((String) in.readValue((String.class.getClassLoader())));
        this.nearestLocation = ((String) in.readValue((String.class.getClassLoader())));
        this.location = ((String) in.readValue((String.class.getClassLoader())));
        this.fullName = ((String) in.readValue((String.class.getClassLoader())));
        this.email = ((String) in.readValue((String.class.getClassLoader())));
        this.day = ((String) in.readValue((String.class.getClassLoader())));
        this.month = ((String) in.readValue((String.class.getClassLoader())));
        this.year = ((String) in.readValue((String.class.getClassLoader())));
        this.accept = ((String) in.readValue((String.class.getClassLoader())));
        this.confirmOrCancel = ((String) in.readValue((String.class.getClassLoader())));
        this.selectedNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.searchNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.keepExistingNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.packageSelected = ((String) in.readValue((String.class.getClassLoader())));
        this.deliveryMethod = ((String) in.readValue((String.class.getClassLoader())));
        this.contactNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.city = ((String) in.readValue((String.class.getClassLoader())));
        this.area = ((String) in.readValue((String.class.getClassLoader())));
        this.address = ((String) in.readValue((String.class.getClassLoader())));
        this.checkout = ((String) in.readValue((String.class.getClassLoader())));
        this.pay = ((String) in.readValue((String.class.getClassLoader())));
        this.tryAgain = ((String) in.readValue((String.class.getClassLoader())));
        this.migrationNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.nonEtisalatNewNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.emiratesId = ((String) in.readValue((String.class.getClassLoader())));
        this.eidFailureReviewDetails = ((String) in.readValue((String.class.getClassLoader())));
        this.eidFailureLocateShop = ((String) in.readValue((String.class.getClassLoader())));
        this.eidFailureNewNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.reviewFullName = ((String) in.readValue((String.class.getClassLoader())));
        this.reviewEmiratesId = ((String) in.readValue((String.class.getClassLoader())));
        this.reviewVerify = ((String) in.readValue((String.class.getClassLoader())));
        this.eligibilityFailureOutstandingAmountNewNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.eligibilityFailureAccountRestrictionsNewNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.eligibilityFailureAccountRestrictionsLiveChat = ((String) in.readValue((String.class.getClassLoader())));
        this.migratedNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.isEligibleNewNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.otp = ((String) in.readValue((String.class.getClassLoader())));
    }

    public LogEventInputModel() {
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public String getActionTransactionId() {
        return actionTransactionId;
    }

    public void setActionTransactionId(String actionTransactionId) {
        this.actionTransactionId = actionTransactionId;
    }

    public String getScreenId() {
        return screenId;
    }

    public void setScreenId(String screenId) {
        this.screenId = screenId;
    }

    public String getSignUp() {
        return signUp;
    }

    public void setSignUp(String signUp) {
        this.signUp = signUp;
    }

    public String getNearestLocation() {
        return nearestLocation;
    }

    public void setNearestLocation(String nearestLocation) {
        this.nearestLocation = nearestLocation;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getAccept() {
        return accept;
    }

    public void setAccept(String accept) {
        this.accept = accept;
    }

    public String getConfirmOrCancel() {
        return confirmOrCancel;
    }

    public void setConfirmOrCancel(String confirmOrCancel) {
        this.confirmOrCancel = confirmOrCancel;
    }

    public String getSelectedNumber() {
        return selectedNumber;
    }

    public void setSelectedNumber(String selectedNumber) {
        this.selectedNumber = selectedNumber;
    }

    public String getSearchNumber() {
        return searchNumber;
    }

    public void setSearchNumber(String searchNumber) {
        this.searchNumber = searchNumber;
    }

    public String getKeepExistingNumber() {
        return keepExistingNumber;
    }

    public void setKeepExistingNumber(String keepExistingNumber) {
        this.keepExistingNumber = keepExistingNumber;
    }

    public String getPackageSelected() {
        return packageSelected;
    }

    public void setPackageSelected(String packageSelected) {
        this.packageSelected = packageSelected;
    }

    public String getDeliveryMethod() {
        return deliveryMethod;
    }

    public void setDeliveryMethod(String deliveryMethod) {
        this.deliveryMethod = deliveryMethod;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCheckout() {
        return checkout;
    }

    public void setCheckout(String checkout) {
        this.checkout = checkout;
    }

    public String getPay() {
        return pay;
    }

    public void setPay(String pay) {
        this.pay = pay;
    }

    public String getTryAgain() {
        return tryAgain;
    }

    public void setTryAgain(String tryAgain) {
        this.tryAgain = tryAgain;
    }

    public String getMigrationNumber() {
        return migrationNumber;
    }

    public void setMigrationNumber(String migrationNumber) {
        this.migrationNumber = migrationNumber;
    }

    public String getNonEtisalatNewNumber() {
        return nonEtisalatNewNumber;
    }

    public void setNonEtisalatNewNumber(String nonEtisalatNewNumber) {
        this.nonEtisalatNewNumber = nonEtisalatNewNumber;
    }

    public String getEmiratesId() {
        return emiratesId;
    }

    public void setEmiratesId(String emiratesId) {
        this.emiratesId = emiratesId;
    }

    public String getEidFailureReviewDetails() {
        return eidFailureReviewDetails;
    }

    public void setEidFailureReviewDetails(String eidFailureReviewDetails) {
        this.eidFailureReviewDetails = eidFailureReviewDetails;
    }

    public String getEidFailureLocateShop() {
        return eidFailureLocateShop;
    }

    public void setEidFailureLocateShop(String eidFailureLocateShop) {
        this.eidFailureLocateShop = eidFailureLocateShop;
    }

    public String getEidFailureNewNumber() {
        return eidFailureNewNumber;
    }

    public void setEidFailureNewNumber(String eidFailureNewNumber) {
        this.eidFailureNewNumber = eidFailureNewNumber;
    }

    public String getReviewFullName() {
        return reviewFullName;
    }

    public void setReviewFullName(String reviewFullName) {
        this.reviewFullName = reviewFullName;
    }

    public String getReviewEmiratesId() {
        return reviewEmiratesId;
    }

    public void setReviewEmiratesId(String reviewEmiratesId) {
        this.reviewEmiratesId = reviewEmiratesId;
    }

    public String getReviewVerify() {
        return reviewVerify;
    }

    public void setReviewVerify(String reviewVerify) {
        this.reviewVerify = reviewVerify;
    }

    public String getEligibilityFailureOutstandingAmountNewNumber() {
        return eligibilityFailureOutstandingAmountNewNumber;
    }

    public void setEligibilityFailureOutstandingAmountNewNumber(String eligibilityFailureOutstandingAmountNewNumber) {
        this.eligibilityFailureOutstandingAmountNewNumber = eligibilityFailureOutstandingAmountNewNumber;
    }

    public String getEligibilityFailureAccountRestrictionsNewNumber() {
        return eligibilityFailureAccountRestrictionsNewNumber;
    }

    public void setEligibilityFailureAccountRestrictionsNewNumber(String eligibilityFailureAccountRestrictionsNewNumber) {
        this.eligibilityFailureAccountRestrictionsNewNumber = eligibilityFailureAccountRestrictionsNewNumber;
    }

    public String getEligibilityFailureAccountRestrictionsLiveChat() {
        return eligibilityFailureAccountRestrictionsLiveChat;
    }

    public void setEligibilityFailureAccountRestrictionsLiveChat(String eligibilityFailureAccountRestrictionsLiveChat) {
        this.eligibilityFailureAccountRestrictionsLiveChat = eligibilityFailureAccountRestrictionsLiveChat;
    }

    public String getMigratedNumber() {
        return migratedNumber;
    }

    public void setMigratedNumber(String migratedNumber) {
        this.migratedNumber = migratedNumber;
    }

    public String getIsEligibleNewNumber() {
        return isEligibleNewNumber;
    }

    public void setIsEligibleNewNumber(String isEligibleNewNumber) {
        this.isEligibleNewNumber = isEligibleNewNumber;
    }

    public String getProgressStatus() {
        return progressStatus;
    }

    public void setProgressStatus(String progressStatus) {
        this.progressStatus = progressStatus;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(authToken);
        dest.writeValue(appVersion);
        dest.writeValue(msisdn);
        dest.writeValue(osVersion);
        dest.writeValue(token);
        dest.writeValue(channel);
        dest.writeValue(lang);
        dest.writeValue(imsi);
        dest.writeValue(deviceId);
        dest.writeValue(additionalInfo);
        dest.writeValue(actionTransactionId);
        dest.writeValue(screenId);
        dest.writeValue(signUp);
        dest.writeValue(nearestLocation);
        dest.writeValue(location);
        dest.writeValue(fullName);
        dest.writeValue(email);
        dest.writeValue(day);
        dest.writeValue(month);
        dest.writeValue(year);
        dest.writeValue(accept);
        dest.writeValue(confirmOrCancel);
        dest.writeValue(selectedNumber);
        dest.writeValue(searchNumber);
        dest.writeValue(keepExistingNumber);
        dest.writeValue(packageSelected);
        dest.writeValue(deliveryMethod);
        dest.writeValue(contactNumber);
        dest.writeValue(city);
        dest.writeValue(area);
        dest.writeValue(address);
        dest.writeValue(checkout);
        dest.writeValue(pay);
        dest.writeValue(tryAgain);
        dest.writeValue(migrationNumber);
        dest.writeValue(nonEtisalatNewNumber);
        dest.writeValue(emiratesId);
        dest.writeValue(eidFailureReviewDetails);
        dest.writeValue(eidFailureLocateShop);
        dest.writeValue(eidFailureNewNumber);
        dest.writeValue(reviewFullName);
        dest.writeValue(reviewEmiratesId);
        dest.writeValue(reviewVerify);
        dest.writeValue(eligibilityFailureOutstandingAmountNewNumber);
        dest.writeValue(eligibilityFailureAccountRestrictionsNewNumber);
        dest.writeValue(eligibilityFailureAccountRestrictionsLiveChat);
        dest.writeValue(migratedNumber);
        dest.writeValue(isEligibleNewNumber);
        dest.writeValue(otp);
    }

    public int describeContents() {
        return 0;
    }

}
