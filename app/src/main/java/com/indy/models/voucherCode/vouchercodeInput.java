package com.indy.models.voucherCode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterInputResponse;

/**
 * Created by Amir.jehangir on 10/31/2016.
 */
public class vouchercodeInput extends MasterInputResponse {
    @SerializedName("offerId")
    @Expose
    private String offerId;
    /**
     *
     * @return
     * The offerId
     */
    public String getOfferId() {
        return offerId;
    }

    /**
     *
     * @param offerId
     * The offerId
     */
    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }
}
