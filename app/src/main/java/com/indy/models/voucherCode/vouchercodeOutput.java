package com.indy.models.voucherCode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by Amir.jehangir on 10/31/2016.
 */
public class vouchercodeOutput extends MasterErrorResponse {
    @SerializedName("voucherCode")
    @Expose
    private String voucherCode;

    /**
     *
     * @return
     * The voucherCode
     */
    public String getVoucherCode() {
        return voucherCode;
    }

    /**
     *
     * @param voucherCode
     * The voucherCode
     */
    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }
}
