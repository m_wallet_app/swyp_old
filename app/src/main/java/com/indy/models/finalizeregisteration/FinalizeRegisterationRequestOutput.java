package com.indy.models.finalizeregisteration;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by emad on 9/22/16.
 */

public class FinalizeRegisterationRequestOutput extends MasterErrorResponse {
    @SerializedName("additionalInfo")
    @Expose
    private List<AdditionalInfo> additionalInfo = new ArrayList<AdditionalInfo>();
    @SerializedName("transactionId")
    @Expose
    private String transactionId;

    @SerializedName("registrationId")
    @Expose
    private String registrationId;
    @SerializedName("paymentGateWayUrl")
    @Expose
    private String paymentGateWayUrl;

    /**
     * @return The additionalInfo
     */
    public List<AdditionalInfo> getAdditionalInfo() {
        return additionalInfo;
    }

    /**
     * @param additionalInfo The additionalInfo
     */
    public void setAdditionalInfo(List<AdditionalInfo> additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }

    /**
     * @return The transactionId
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * @param transactionId The transactionId
     */
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    /**
     * @return The paymentGateWayUrl
     */
    public String getPaymentGateWayUrl() {
        return paymentGateWayUrl;
    }

    /**
     * @param paymentGateWayUrl The paymentGateWayUrl
     */
    public void setPaymentGateWayUrl(String paymentGateWayUrl) {
        this.paymentGateWayUrl = paymentGateWayUrl;
    }


}
