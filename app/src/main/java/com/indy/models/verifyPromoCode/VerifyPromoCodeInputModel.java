package com.indy.models.verifyPromoCode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterInputResponse;

/**
 * Created by mobile on 19/03/2018.
 */

public class VerifyPromoCodeInputModel extends MasterInputResponse {

    @SerializedName("promoCode")
    @Expose
    private String promoCode;

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }
}
