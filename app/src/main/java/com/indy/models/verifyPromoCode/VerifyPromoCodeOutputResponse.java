package com.indy.models.verifyPromoCode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by mobile on 19/03/2018.
 */

public class VerifyPromoCodeOutputResponse extends MasterErrorResponse {
    @SerializedName("valid")
    @Expose
    private boolean isValid;
    @SerializedName("promoCode")
    @Expose
    private String promoCode;
    @SerializedName("promoCodeDescription")
    @Expose
    private String promoCodeDescrition;


    @SerializedName("promoCodeType")
    @Expose
    private int promoCodeType;

    public boolean getValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getPromoCodeDescrition() {
        return promoCodeDescrition;
    }

    public void setPromoCodeDescrition(String promoCodeDescrition) {
        this.promoCodeDescrition = promoCodeDescrition;
    }

    public int getPromoCodeType() {
        return promoCodeType;
    }

    public void setPromoCodeType(int promoCodeType) {
        this.promoCodeType = promoCodeType;
    }
}
