package com.indy.models.benefits;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

import java.util.List;

/**
 * Created by mobile on 05/01/2017.
 */

public class BenefitsOutputModel extends MasterErrorResponse {
    @SerializedName("benefits_text")
    @Expose
    public List<BenefitsText> benefitsText = null;

    @SerializedName("header")
    @Expose
    public String header;


    @SerializedName("footer")
    @Expose
    public String footer;

    public List<BenefitsText> getBenefitsText() {
        return benefitsText;
    }

    public void setBenefitsText(List<BenefitsText> benefitsText) {
        this.benefitsText = benefitsText;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getFooter() {
        return footer;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }
}
