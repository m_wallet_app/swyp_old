package com.indy.models.benefits;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;

import java.util.ArrayList;
import java.util.List;

public class NewBenefitsModel extends MasterErrorResponse implements Parcelable {
    @SerializedName("header")
    @Expose
    public String header;
    @SerializedName("isPromoActive")
    @Expose
    public Boolean isPromoActive;
    @SerializedName("benefitsFooter")
    @Expose
    public Footer footer;
    @SerializedName("benefitsSummary")
    @Expose
    public List<BenefitsSummary> benefitsSummary = new ArrayList<>();

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.header);
        dest.writeValue(this.isPromoActive);
        dest.writeParcelable(this.footer, flags);
        dest.writeList(this.benefitsSummary);
    }

    public NewBenefitsModel() {
    }

    protected NewBenefitsModel(Parcel in) {
        this.header = in.readString();
        this.isPromoActive = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.footer = in.readParcelable(Footer.class.getClassLoader());
        this.benefitsSummary = new ArrayList<BenefitsSummary>();
        in.readList(this.benefitsSummary, BenefitsSummary.class.getClassLoader());
    }

    public static final Creator<NewBenefitsModel> CREATOR = new Creator<NewBenefitsModel>() {
        @Override
        public NewBenefitsModel createFromParcel(Parcel source) {
            return new NewBenefitsModel(source);
        }

        @Override
        public NewBenefitsModel[] newArray(int size) {
            return new NewBenefitsModel[size];
        }
    };

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public Boolean getPromoActive() {
        return isPromoActive;
    }

    public void setPromoActive(Boolean promoActive) {
        isPromoActive = promoActive;
    }

    public Footer getFooter() {
        return footer;
    }

    public void setFooter(Footer footer) {
        this.footer = footer;
    }

    public List<BenefitsSummary> getBenefitsSummary() {
        return benefitsSummary;
    }

    public void setBenefitsSummary(List<BenefitsSummary> benefitsSummary) {
        this.benefitsSummary = benefitsSummary;
    }
}
