package com.indy.models.benefits;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Footer implements Parcelable {
    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("description")
    @Expose
    public String description;

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    @SerializedName("subtitle")
    @Expose
    public String subtitle;

    public Footer() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeString(this.subtitle);
    }

    protected Footer(Parcel in) {
        this.title = in.readString();
        this.description = in.readString();
        this.subtitle = in.readString();
    }

    public static final Creator<Footer> CREATOR = new Creator<Footer>() {
        @Override
        public Footer createFromParcel(Parcel source) {
            return new Footer(source);
        }

        @Override
        public Footer[] newArray(int size) {
            return new Footer[size];
        }
    };
}
