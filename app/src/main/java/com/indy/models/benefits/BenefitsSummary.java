package com.indy.models.benefits;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BenefitsSummary {
    @SerializedName("title")
    @Expose
    public String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @SerializedName("description")
    @Expose
    public String description;
}
