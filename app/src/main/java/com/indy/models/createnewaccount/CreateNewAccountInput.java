package com.indy.models.createnewaccount;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterInputResponse;

/**
 * Created by emad on 9/20/16.
 */

public class CreateNewAccountInput extends MasterInputResponse {

    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("passwordConfirmation")
    @Expose
    private String passwordConfirmation;


    public void setPassword(String password) {
        this.password = password;
    }


    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    public String getPassword() {
        return password;
    }


    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

}
