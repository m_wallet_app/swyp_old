package com.indy.models.createnewaccount;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by emad on 9/20/16.
 */

public class CreateNewAccountOutput extends MasterErrorResponse{


    @SerializedName("updated")
    @Expose
    private Boolean updated;

    /**
     *
     * @return
     * The updated
     */
    public Boolean getUpdated() {
        return updated;
    }

    /**
     *
     * @param updated
     * The updated
     */
    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }
}
