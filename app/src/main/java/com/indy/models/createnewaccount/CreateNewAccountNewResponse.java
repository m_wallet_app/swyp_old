package com.indy.models.createnewaccount;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.login.UserProfile;
import com.indy.models.profilestatus.AppFlags;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by emad on 11/14/16.
 */

public class CreateNewAccountNewResponse extends MasterErrorResponse {

    @SerializedName("updated")
    @Expose
    private Boolean updated;
    @SerializedName("userProfile")
    @Expose
    private UserProfile userProfile;
    @SerializedName("appFlags")
    @Expose
    private AppFlags appFlags;


    /**
     * @return The updated
     */
    public Boolean getUpdated() {
        return updated;
    }

    /**
     * @param updated The updated
     */
    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    /**
     * @return The userProfile
     */
    public UserProfile getUserProfile() {
        return userProfile;
    }

    /**
     * @param userProfile The userProfile
     */
    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    public void setAppFlags(AppFlags appFlags) {
        this.appFlags = appFlags;
    }

    public AppFlags getAppFlags() {
        return appFlags;
    }
}
