package com.indy.models.sendReminder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterInputResponse;

/**
 * Created by hadi.mehmood on 10/17/2016.
 */
public class SendReminderInput extends MasterInputResponse {

    @SerializedName("requestId")
    @Expose
    String requestId;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
}
