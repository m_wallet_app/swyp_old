package com.indy.models.locations;

import android.app.Activity;
import android.location.Location;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by emad on 8/16/16.
 */
public class LocationModel {
    @SerializedName("mapImage")
    @Expose
    private ImageView mapImage;
    @SerializedName("mapTitle")
    @Expose
    private TextView mapTitle;
    @SerializedName("activity")
    @Expose
    private Activity activity;
    @SerializedName("location")
    @Expose
    private Location location;

    public ImageView getMapImage() {
        return mapImage;
    }

    public TextView getMapTitle() {
        return mapTitle;
    }

    public Activity getActivity() {
        return activity;
    }

    public Location getLocation() {
        return location;
    }

    public void setMapImage(ImageView mapImage) {
        this.mapImage = mapImage;
    }

    public void setMapTitle(TextView mapTitle) {
        this.mapTitle = mapTitle;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
