package com.indy.models.inilizeregisteration;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by emad on 9/21/16.
 */

public class InilizeRegisteratonOutput extends MasterErrorResponse {
    @SerializedName("registrationId")
    @Expose
    private String registrationId;

    /**
     * @return The registrationId
     */
    public String getRegistrationId() {
        return registrationId;
    }

    /**
     * @param registrationId The registrationId
     */
    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }
}
