package com.indy.models.ForgotPassword;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterInputResponse;

/**
 * Created by emad on 9/20/16.
 */

public class ForgotPasswordInputModel extends MasterInputResponse{
    @SerializedName("contact")
    @Expose
    private String contact;

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getContact() {
        return contact;
    }
}
