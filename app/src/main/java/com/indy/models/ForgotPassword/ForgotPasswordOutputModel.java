package com.indy.models.ForgotPassword;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by emad on 9/20/16.
 */

public class ForgotPasswordOutputModel extends MasterErrorResponse {

    @SerializedName("generatedCode")
    @Expose
    private String generatedCode;

    /**
     * @return The generatedCode
     */
    public String getGeneratedCode() {
        return generatedCode;
    }

    /**
     * @param generatedCode The generatedCode
     */
    public void setGeneratedCode(String generatedCode) {
        this.generatedCode = generatedCode;
    }

}
