package com.indy.models.getPendingTransfersList.requestCreditFromFriend;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hadi.mehmood on 10/17/2016.
 */
public class TransacitonList {

    @SerializedName("amount")
    @Expose
    private String amount;

    @SerializedName("transactionId")
    @Expose
    private String transactionId;

    @SerializedName("targetMsisdn")
    @Expose
    private String targetMsisdn;

    @SerializedName("transferType")
    @Expose
    private String transferType;

    @SerializedName("transactionDate")
    @Expose
    private String transactionDate;

    @SerializedName("senderMsisdn")
    @Expose
    private String senderMsisdn;

    @SerializedName("transactionFee")
    @Expose
    private String transactionFee;

    public String getAmount ()
    {
        return amount;
    }

    public void setAmount (String amount)
    {
        this.amount = amount;
    }

    public String getRequestId ()
    {
        return transactionId;
    }

    public void setRequestId (String transactionId)
    {
        this.transactionId = transactionId;
    }

    public String getMsisdn ()
    {
        return targetMsisdn;
    }

    public void setMsisdn (String targetMsisdn)
    {
        this.targetMsisdn = targetMsisdn;
    }

    public String getTransferType()
    {
        return transferType;
    }

    public void setTransferType(String transferType)
    {
        this.transferType = transferType;
    }

    public String getDate ()
    {
        return transactionDate;
    }

    public void setDate (String transactionDate)
    {
        this.transactionDate = transactionDate;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [amount = "+amount+", transactionId = "+transactionId+", targetMsisdn = "+targetMsisdn+", transferType = "+ transferType +", transactionDate = "+transactionDate+"]";
    }

    public String getSenderMsisdn() {
        return senderMsisdn;
    }

    public void setSenderMsisdn(String senderMsisdn) {
        this.senderMsisdn = senderMsisdn;
    }
}
