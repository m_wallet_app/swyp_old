package com.indy.models.getPendingTransfersList.requestCreditFromFriend;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by hadi.mehmood on 10/17/2016.
 */
public class GetPendingTransferListOutput extends MasterErrorResponse{

    @SerializedName("transacitonList")
    @Expose
    private TransacitonList[] transacitonList;

    public TransacitonList[] getTransacitonList() {
        return transacitonList;
    }

    public void setTransacitonList(TransacitonList[] transacitonList) {
        this.transacitonList = transacitonList;
    }


}
