package com.indy.models.rechargeCreditCard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterInputResponse;

/**
 * Created by hadi.mehmood on 11/3/2016.
 */
public class CreditCardPaymentRequestInput extends MasterInputResponse{

    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("orderType")
    @Expose
    private String orderType;



    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }
}
