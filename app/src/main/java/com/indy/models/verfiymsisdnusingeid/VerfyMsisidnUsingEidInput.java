package com.indy.models.verfiymsisdnusingeid;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterInputResponse;

/**
 * Created by emad on 9/25/16.
 */

public class VerfyMsisidnUsingEidInput extends MasterInputResponse{

    @SerializedName("emiratesId")
    @Expose
    private String emiratesId;


    public void setEmiratesId(String emiratesId) {
        this.emiratesId = emiratesId;
    }

    public String getEmiratesId() {
        return emiratesId;
    }


}
