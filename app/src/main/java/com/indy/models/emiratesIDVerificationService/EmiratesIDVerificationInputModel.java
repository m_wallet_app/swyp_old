package com.indy.models.emiratesIDVerificationService;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterInputResponse;

/**
 * Created by hadi on 17/04/2018.
 */

public class EmiratesIDVerificationInputModel extends MasterInputResponse {

    @SerializedName("emiratesId")
    @Expose
    private String emiratesId;

    public String getEmiratesId() {
        return emiratesId;
    }

    public void setEmiratesId(String emiratesId) {
        this.emiratesId = emiratesId;
    }
}
