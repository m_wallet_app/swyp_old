package com.indy.models.recharge;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by Amir.jehangir on 10/16/2016.
 */
public class RechargeOutput extends MasterErrorResponse {
    @SerializedName("recharged")
    @Expose
    private String recharged;

    public String getRecharged ()
    {
        return recharged;
    }

    public void setRecharged (String recharged)
    {
        this.recharged = recharged;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [recharged = "+recharged+"]";
    }
}
