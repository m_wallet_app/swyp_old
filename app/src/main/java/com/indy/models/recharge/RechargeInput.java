package com.indy.models.recharge;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterInputResponse;

/**
 * Created by Amir.jehangir on 10/16/2016.
 */
public class RechargeInput extends MasterInputResponse {
    @SerializedName("scratchCardNo")
    @Expose
    private String scratchCardNo;


    public String getScratchCardNo() {
        return scratchCardNo;
    }

    public void setScratchCardNo(String scratchCardNo) {
        this.scratchCardNo = scratchCardNo;
    }
    @Override
    public String toString()
    {
        return "ClassPojo [scratchCardNo = "+scratchCardNo+"]";
    }

}
