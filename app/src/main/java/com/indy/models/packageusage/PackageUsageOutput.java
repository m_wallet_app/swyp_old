package com.indy.models.packageusage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by emad on 9/29/16.
 */

public class PackageUsageOutput extends MasterErrorResponse {

    @SerializedName("usageGraphList")
    @Expose
    private List<UsageGraphList> usageGraphList = new ArrayList<UsageGraphList>();

    /**
     * @return The usageGraphList
     */
    public List<UsageGraphList> getUsageGraphList() {
        return usageGraphList;
    }

    /**
     * @param usageGraphList The usageGraphList
     */
    public void setUsageGraphList(List<UsageGraphList> usageGraphList) {
        this.usageGraphList = usageGraphList;
    }

}
