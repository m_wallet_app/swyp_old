package com.indy.models.packageusage;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by emad on 9/29/16.
 */

public class GraphPointList implements Parcelable{

    @SerializedName("xAxisValue")
    @Expose
    private Double xAxisValue;
    @SerializedName("yAxisValue")
    @Expose
    private Double yAxisValue;

    protected GraphPointList(Parcel in) {
        xAxisValue= in.readDouble();
        yAxisValue= in.readDouble();

    }

    public static final Creator<GraphPointList> CREATOR = new Creator<GraphPointList>() {
        @Override
        public GraphPointList createFromParcel(Parcel in) {
            return new GraphPointList(in);
        }

        @Override
        public GraphPointList[] newArray(int size) {
            return new GraphPointList[size];
        }
    };

    /**
     * @return The xAxisValue
     */
    public Double getXAxisValue() {
        return xAxisValue;
    }

    /**
     * @param xAxisValue The xAxisValue
     */
    public void setXAxisValue(Double xAxisValue) {
        this.xAxisValue = xAxisValue;
    }

    /**
     * @return The yAxisValue
     */
    public Double getYAxisValue() {
        return yAxisValue;
    }

    /**
     * @param yAxisValue The yAxisValue
     */
    public void setYAxisValue(Double yAxisValue) {
        this.yAxisValue = yAxisValue;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(xAxisValue);
        dest.writeDouble(yAxisValue);

    }
}
