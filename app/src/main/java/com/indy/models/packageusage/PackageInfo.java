package com.indy.models.packageusage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by emad on 9/29/16.
 */

public class PackageInfo {

    @SerializedName("packageId")
    @Expose
    private String packageId;
    @SerializedName("offerInfo")
    @Expose
    private List<OfferInfo> offerInfo = new ArrayList<OfferInfo>();

    /**
     * @return The packageId
     */
    public String getPackageId() {
        return packageId;
    }

    /**
     * @param packageId The packageId
     */
    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    /**
     * @return The offerInfo
     */
    public List<OfferInfo> getOfferInfo() {
        return offerInfo;
    }

    /**
     * @param offerInfo The offerInfo
     */
    public void setOfferInfo(List<OfferInfo> offerInfo) {
        this.offerInfo = offerInfo;
    }

}
