package com.indy.models.packageusage;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by emad on 9/29/16.
 */

public class UsageGraphList implements Parcelable{

    @SerializedName("packageId")
    @Expose
    private String packageId;
    @SerializedName("xAxisNameEn")
    @Expose
    private String xAxisNameEn;
    @SerializedName("xAxisNameAr")
    @Expose
    private String xAxisNameAr;
    @SerializedName("yAxisNameEn")
    @Expose
    private String yAxisNameEn;
    @SerializedName("yAxisNameAr")
    @Expose
    private String yAxisNameAr;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("graphPointList")
    @Expose
    private List<GraphPointList> graphPointList = new ArrayList<GraphPointList>();

    @SerializedName("usedToday")
    @Expose
    private String usedToday;

    @SerializedName("usedTodayUnitEn")
    @Expose
    private String usedTodayUnitEn;

    public String getUsedTodayUnitEn() {
        return usedTodayUnitEn;
    }

    public void setUsedTodayUnitEn(String usedTodayUnitEn) {
        this.usedTodayUnitEn = usedTodayUnitEn;
    }

    public String getUsedTodayUnitAr() {
        return usedTodayUnitAr;
    }

    public void setUsedTodayUnitAr(String usedTodayUnitAr) {
        this.usedTodayUnitAr = usedTodayUnitAr;
    }

    @SerializedName("usedTodayUnitAr")
    @Expose
    private String usedTodayUnitAr;


    @SerializedName("dailyBudget")
    @Expose
    private String dailyBudget;

    @SerializedName("forecastedUsage")
    @Expose
    private String forecastedUsage;
    @SerializedName("preferredNumberOfferStatus")
    @Expose
    private String preferredNumberOfferStatus;
    @SerializedName("preferredNumber")
    @Expose
    private String preferredNumber;

    public UsageGraphList(){

    }

    /**
     * @return The packageId
     */
    public String getPackageId() {
        return packageId;
    }

    /**
     * @param packageId The packageId
     */
    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    /**
     * @return The xAxisNameEn
     */
    public String getXAxisNameEn() {
        return xAxisNameEn;
    }

    /**
     * @param xAxisNameEn The xAxisNameEn
     */
    public void setXAxisNameEn(String xAxisNameEn) {
        this.xAxisNameEn = xAxisNameEn;
    }

    /**
     * @return The xAxisNameAr
     */
    public String getXAxisNameAr() {
        return xAxisNameAr;
    }

    /**
     * @param xAxisNameAr The xAxisNameAr
     */
    public void setXAxisNameAr(String xAxisNameAr) {
        this.xAxisNameAr = xAxisNameAr;
    }

    /**
     * @return The yAxisNameEn
     */
    public String getYAxisNameEn() {
        return yAxisNameEn;
    }

    /**
     * @param yAxisNameEn The yAxisNameEn
     */
    public void setYAxisNameEn(String yAxisNameEn) {
        this.yAxisNameEn = yAxisNameEn;
    }

    /**
     * @return The yAxisNameAr
     */
    public String getYAxisNameAr() {
        return yAxisNameAr;
    }

    /**
     * @param yAxisNameAr The yAxisNameAr
     */
    public void setYAxisNameAr(String yAxisNameAr) {
        this.yAxisNameAr = yAxisNameAr;
    }

    /**
     * @return The color
     */
    public String getColor() {
        return color;
    }

    /**
     * @param color The color
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * @return The type
     */
    public Integer getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * @return The graphPointList
     */
    public List<GraphPointList> getGraphPointList() {
        return graphPointList;
    }

    /**
     * @param graphPointList The graphPointList
     */
    public void setGraphPointList(List<GraphPointList> graphPointList) {
        this.graphPointList = graphPointList;
    }

    public String getUsedToday() {
        if(usedToday!=null) {
            return usedToday;
        }else{
            return 0+"";
        }
    }

    public void setUsedToday(String usedToday) {
        this.usedToday = usedToday;
    }

    public String getDailyBudget() {
        if(dailyBudget!=null) {
            return dailyBudget;
        }else{
            return 0+"";
        }
    }

    public void setDailyBudget(String dailyBudget) {
        this.dailyBudget = dailyBudget;
    }

    public String getForecastedUsage() {
        if(forecastedUsage!=null) {
            return forecastedUsage;
        }else{
            return 0+"";
        }
    }

    public void setForecastedUsage(String forecastedUsage) {
        this.forecastedUsage = forecastedUsage;
    }

    public String getPreferredNumberOfferStatus() {
        return preferredNumberOfferStatus;
    }

    public void setPreferredNumberOfferStatus(String preferredNumberOfferStatus) {
        this.preferredNumberOfferStatus = preferredNumberOfferStatus;
    }

    public String getPreferredNumber() {
        return preferredNumber;
    }

    public void setPreferredNumber(String preferredNumber) {
        this.preferredNumber = preferredNumber;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.packageId);
        dest.writeString(this.xAxisNameEn);
        dest.writeString(this.xAxisNameAr);
        dest.writeString(this.yAxisNameEn);
        dest.writeString(this.yAxisNameAr);
        dest.writeString(this.color);
        dest.writeValue(this.type);
        dest.writeTypedList(this.graphPointList);
        dest.writeString(this.usedToday);
        dest.writeString(this.usedTodayUnitEn);
        dest.writeString(this.usedTodayUnitAr);
        dest.writeString(this.dailyBudget);
        dest.writeString(this.forecastedUsage);
        dest.writeString(this.preferredNumberOfferStatus);
        dest.writeString(this.preferredNumber);
    }

    protected UsageGraphList(Parcel in) {
        this.packageId = in.readString();
        this.xAxisNameEn = in.readString();
        this.xAxisNameAr = in.readString();
        this.yAxisNameEn = in.readString();
        this.yAxisNameAr = in.readString();
        this.color = in.readString();
        this.type = (Integer) in.readValue(Integer.class.getClassLoader());
        this.graphPointList = in.createTypedArrayList(GraphPointList.CREATOR);
        this.usedToday = in.readString();
        this.usedTodayUnitEn = in.readString();
        this.usedTodayUnitAr = in.readString();
        this.dailyBudget = in.readString();
        this.forecastedUsage = in.readString();
        this.preferredNumberOfferStatus = in.readString();
        this.preferredNumber = in.readString();
    }

    public static final Creator<UsageGraphList> CREATOR = new Creator<UsageGraphList>() {
        @Override
        public UsageGraphList createFromParcel(Parcel source) {
            return new UsageGraphList(source);
        }

        @Override
        public UsageGraphList[] newArray(int size) {
            return new UsageGraphList[size];
        }
    };
}
