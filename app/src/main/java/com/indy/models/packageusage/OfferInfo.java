package com.indy.models.packageusage;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by emad on 9/29/16.
 */

public class OfferInfo implements Parcelable {

    @SerializedName("offerId")
    @Expose
    private String offerId;
    @SerializedName("startDate")
    @Expose
    private String startDate;
    @SerializedName("endDate")
    @Expose
    private String endDate;


    public OfferInfo(){

    }

    protected OfferInfo(Parcel in) {
        offerId = in.readString();
        startDate = in.readString();
        endDate = in.readString();


    }
    public static final Creator<OfferInfo> CREATOR = new Creator<OfferInfo>() {
        @Override
        public OfferInfo createFromParcel(Parcel in) {
            return new OfferInfo(in);
        }

        @Override
        public OfferInfo[] newArray(int size) {
            return new OfferInfo[size];
        }
    };

    /**
     * @return The offerId
     */
    public String getOfferId() {
        return offerId;
    }

    /**
     * @param offerId The offerId
     */
    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    /**
     * @return The startDate
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * @param startDate The startDate
     */
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    /**
     * @return The endDate
     */
    public String getEndDate() {
        return endDate;
    }

    /**
     * @param endDate The endDate
     */
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(offerId);
        parcel.writeString(startDate);
        parcel.writeString(endDate);
    }
}
