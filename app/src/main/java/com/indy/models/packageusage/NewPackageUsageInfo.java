package com.indy.models.packageusage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.finalizeregisteration.AdditionalInfo;
import com.indy.models.utils.MasterInputResponse;

import java.util.List;

/**
 * Created by emad on 9/29/16.
 */

public class NewPackageUsageInfo extends MasterInputResponse {
    @SerializedName("packageInfos")
    @Expose
    private List<PackageInfo> packageInfo;

    @SerializedName("additionalInfo")
    @Expose
    private List<AdditionalInfo> additionalInfo;
    /**
     *
     * @return
     * The packageInfo
     */
    public List<PackageInfo> getPackageInfo() {
        return packageInfo;
    }

    /**
     *
     * @param packageInfo
     * The packageInfo
     */
    public void setPackageInfo(List<PackageInfo> packageInfo) {
        this.packageInfo = packageInfo;
    }

    public List<AdditionalInfo> getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(List<AdditionalInfo> additionalInfo) {
        this.additionalInfo = additionalInfo;
    }
}