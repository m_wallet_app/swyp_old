package com.indy.models.validateinvitioncode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by emad on 9/22/16.
 */

public class ValidateInvitaionCodeOutput extends MasterErrorResponse {

    @SerializedName("valid")
    @Expose
    private Boolean valid;

    /**
     * @return The valid
     */
    public Boolean getValid() {
        return valid;
    }

    /**
     * @param valid The valid
     */
    public void setValid(Boolean valid) {
        this.valid = valid;
    }
}
