package com.indy.models.validateinvitioncode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterInputResponse;

/**
 * Created by emad on 9/22/16.
 */

public class ValidateInvitaionCodeInput extends MasterInputResponse {
    @SerializedName("invitationCode")
    @Expose
    private String invitationCode;




    public void setInvitationCode(String invitationCode) {
        this.invitationCode = invitationCode;
    }

    public String getInvitationCode() {
        return invitationCode;
    }

}
