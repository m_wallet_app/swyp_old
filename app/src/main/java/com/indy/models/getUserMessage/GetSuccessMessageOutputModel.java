package com.indy.models.getUserMessage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by emad on 9/20/16.
 */

public class GetSuccessMessageOutputModel extends MasterErrorResponse {

    @SerializedName("message")
    @Expose
    private boolean message;

    public boolean getMessage() {
        return message;
    }

    public void setMessage(boolean message) {
        this.message = message;
    }
}
