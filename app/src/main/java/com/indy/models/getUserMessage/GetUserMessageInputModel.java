package com.indy.models.getUserMessage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterInputResponse;

/**
 * Created by emad on 9/20/16.
 */

public class GetUserMessageInputModel extends MasterInputResponse{
    @SerializedName("messageKey")
    @Expose
    private String messageKey;

    public String getMessageKey() {
        return messageKey;
    }

    public void setMessageKey(String messageKey) {
        this.messageKey = messageKey;
    }
}
