package com.indy.models.requestCreditFromFriend;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterInputResponse;

/**
 * Created by hadi.mehmood on 10/17/2016.
 */
public class RequestCreditInput extends MasterInputResponse {

    @SerializedName("targetMsisdn")
    @Expose
    private String targetMsisdn;

    @SerializedName("amount")
    @Expose
    private String amount;


    public String getTargetMsisdn() {
        return targetMsisdn;
    }

    public void setTargetMsisdn(String targetMsisdn) {
        this.targetMsisdn = targetMsisdn;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}

