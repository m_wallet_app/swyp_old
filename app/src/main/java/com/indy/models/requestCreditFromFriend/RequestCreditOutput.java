package com.indy.models.requestCreditFromFriend;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by hadi.mehmood on 10/17/2016.
 */
public class RequestCreditOutput extends MasterErrorResponse{

    @SerializedName("requested")
            @Expose
    String requested;

    @SerializedName("transactionId")
    @Expose
    String transactionId;

    public String getRequested() {
        return requested;
    }

    public void setRequested(String requested) {
        this.requested = requested;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
}
