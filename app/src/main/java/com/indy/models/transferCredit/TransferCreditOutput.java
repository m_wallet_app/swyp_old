package com.indy.models.transferCredit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by hadi.mehmood on 10/17/2016.
 */
public class TransferCreditOutput extends MasterErrorResponse {

    @SerializedName("sent")
    @Expose
    String sent;
    @SerializedName("message")
    @Expose
    String message;

    public String getSent() {
        return sent;
    }

    public void setSent(String requested) {
        this.sent = requested;
    }
}
