package com.indy.models.isEligable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by emad on 10/26/16.
 */

public class IsEligableOutputModel extends MasterErrorResponse {

    @SerializedName("requiredFees")
    @Expose
    String requiredFees;

    @SerializedName("amount")
    @Expose
    String amount;

    @SerializedName("totalAmount")
    @Expose
    String totalAmount;

    @SerializedName("eligible")
    @Expose
    boolean eligible;

    public String getRequiredFees() {
        return requiredFees;
    }

    public void setRequiredFees(String requiredFees) {
        this.requiredFees = requiredFees;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public boolean getEligible() {
        return eligible;
    }

    public void setEligible(boolean eligible) {
        this.eligible = eligible;
    }
}
