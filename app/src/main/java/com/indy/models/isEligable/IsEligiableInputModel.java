package com.indy.models.isEligable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterInputResponse;

/**
 * Created by emad on 10/26/16.
 */

public class IsEligiableInputModel extends MasterInputResponse {

    @SerializedName("targetMsisdn")
    @Expose
    private String targetMsisdn;
    @SerializedName("amount")
    @Expose
    private String amount;

    /**
     * @return The targetMsisdn
     */
    public String getTargetMsisdn() {
        return targetMsisdn;
    }

    /**
     * @param targetMsisdn The targetMsisdn
     */
    public void setTargetMsisdn(String targetMsisdn) {
        this.targetMsisdn = targetMsisdn;
    }

    /**
     * @return The amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * @param amount The amount
     */
    public void setAmount(String amount) {
        this.amount = amount;
    }

}
