package com.indy.models.createprofilestatus;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by emad on 10/6/16.
 */

public class CreateProfileStatusoutput extends MasterErrorResponse {

    @SerializedName("generatedCode")
    @Expose
    private String generatedCode;
    @SerializedName("registrationId")
    @Expose
    private String registrationId;
    /**
     * @return The generatedCode
     */
    public String getGeneratedCode() {
        return generatedCode;
    }

    /**
     * @param generatedCode The generatedCode
     */
    public void setGeneratedCode(String generatedCode) {
        this.generatedCode = generatedCode;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }

    public String getRegistrationId() {
        return registrationId;
    }
}
