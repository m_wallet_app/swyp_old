package com.indy.models.purchaseStoreBundle;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterInputResponse;

/**
 * Created by hadi.mehmood on 10/22/2016.
 */
public class PurchaseStoreInputResponse extends MasterInputResponse {

    @SerializedName("orderItems")
    @Expose
    OrderItems[] orderItems;

    @SerializedName("rewardId")
    @Expose
    String rewardId;

    @SerializedName("talosUserId")
    @Expose
    String talosUserId;

    public OrderItems[] getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(OrderItems[] orderItems) {
        this.orderItems = orderItems;
    }


    public String getRewardId() {
        return rewardId;
    }

    public void setRewardId(String rewardId) {
        this.rewardId = rewardId;
    }

    public String getTalosUserId() {
        return talosUserId;
    }

    public void setTalosUserId(String talosUserId) {
        this.talosUserId = talosUserId;
    }
}
