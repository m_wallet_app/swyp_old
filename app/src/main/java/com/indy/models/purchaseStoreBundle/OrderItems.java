package com.indy.models.purchaseStoreBundle;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by hadi.mehmood on 10/25/2016.
 */
public class OrderItems implements Serializable
{
    @SerializedName("preferedMsisdn")
    @Expose
    private String preferedMsisdn;

    @SerializedName("oldPreferedMsisdn")
    @Expose
    private String oldPreferedMsisdn;

    @SerializedName("orderAdditionalInfoList")
    @Expose
    private OrderAdditionalInfoList[] orderAdditionalInfoList;

    @SerializedName("offerId")
    @Expose
    private String offerId;

    public String getPreferedMsisdn ()
    {
        return preferedMsisdn;
    }

    public void setPreferedMsisdn (String preferedMsisdn)
    {
        this.preferedMsisdn = preferedMsisdn;
    }

    public OrderAdditionalInfoList[] getOrderAdditionalInfoList ()
    {
        return orderAdditionalInfoList;
    }

    public void setOrderAdditionalInfoList (OrderAdditionalInfoList[] orderAdditionalInfoList)
    {
        this.orderAdditionalInfoList = orderAdditionalInfoList;
    }

    public String getOfferId ()
    {
        return offerId;
    }

    public void setOfferId (String offerId)
    {
        this.offerId = offerId;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [preferedMsisdn = "+preferedMsisdn+", orderAdditionalInfoList = "+orderAdditionalInfoList+", offerId = "+offerId+"]";
    }

    public String getOldPreferedMsisdn() {
        return oldPreferedMsisdn;
    }

    public void setOldPreferedMsisdn(String oldPreferedMsisdn) {
        this.oldPreferedMsisdn = oldPreferedMsisdn;
    }
}
