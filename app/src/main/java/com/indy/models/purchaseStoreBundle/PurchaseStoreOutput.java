package com.indy.models.purchaseStoreBundle;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by hadi.mehmood on 10/22/2016.
 */
public class PurchaseStoreOutput extends MasterErrorResponse{
    @SerializedName("purchaseBundleList")
    @Expose
    private PurchaseBundleList[] purchaseBundleList;

    private String responseCode;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public PurchaseBundleList[] getPurchaseBundleList ()
    {
        return purchaseBundleList;
    }

    public void setPurchaseBundleList (PurchaseBundleList[] purchaseBundleList)
    {
        this.purchaseBundleList = purchaseBundleList;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [purchaseBundleList = "+purchaseBundleList+"]";
    }
}
