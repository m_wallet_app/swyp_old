package com.indy.models.packages;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by emad on 9/28/16.
 */

public class OfferList implements Parcelable {

    @SerializedName("inOfferId")
    @Expose
    private String inOfferId;
    @SerializedName("packageConsumption")
    @Expose
    private PackageConsumption packageConsumption;

    @SerializedName("inOfferStartDate")
    @Expose
    private String inOfferStartDate;

    @SerializedName("inOfferEndDate")
    @Expose
    private String inOfferEndDate;

    public OfferList(){

    }

    /**
     *
     * @return
     * The inOfferId
     */
    public String getInOfferId() {
        return inOfferId;
    }

    /**
     *
     * @param inOfferId
     * The inOfferId
     */
    public void setInOfferId(String inOfferId) {
        this.inOfferId = inOfferId;
    }

    /**
     *
     * @return
     * The packageConsumption
     */
    public PackageConsumption getPackageConsumption() {
        return packageConsumption;
    }

    /**
     *
     * @param packageConsumption
     * The packageConsumption
     */
    public void setPackageConsumption(PackageConsumption packageConsumption) {
        this.packageConsumption = packageConsumption;
    }

    public String getInOfferStartDate() {
        return inOfferStartDate;
    }

    public void setInOfferStartDate(String inOfferStartDate) {
        this.inOfferStartDate = inOfferStartDate;
    }

    public String getInOfferEndDate() {
        return inOfferEndDate;
    }

    public void setInOfferEndDate(String inOfferEndDate) {
        this.inOfferEndDate = inOfferEndDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.inOfferId);
        dest.writeParcelable(this.packageConsumption, flags);
        dest.writeString(this.inOfferStartDate);
        dest.writeString(this.inOfferEndDate);
    }

    protected OfferList(Parcel in) {
        this.inOfferId = in.readString();
        this.packageConsumption = in.readParcelable(PackageConsumption.class.getClassLoader());
        this.inOfferStartDate = in.readString();
        this.inOfferEndDate = in.readString();
    }

    public static final Creator<OfferList> CREATOR = new Creator<OfferList>() {
        @Override
        public OfferList createFromParcel(Parcel source) {
            return new OfferList(source);
        }

        @Override
        public OfferList[] newArray(int size) {
            return new OfferList[size];
        }
    };
}
