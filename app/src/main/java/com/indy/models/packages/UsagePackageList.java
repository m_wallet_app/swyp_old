package com.indy.models.packages;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by emad on 8/11/16.
 */
public class UsagePackageList implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("nameEn")
    @Expose
    private String nameEn;
    @SerializedName("nameAr")
    @Expose
    private String nameAr;
    @SerializedName("startDate")
    @Expose
    private String startDate;
    @SerializedName("endDate")
    @Expose
    private String endDate;
    @SerializedName("iconUrl")
    @Expose
    private String iconUrl;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("packageType")
    @Expose
    private Integer packageType;
    @SerializedName("packageDisabled")
    @Expose
    private Boolean packageDisabled;
    @SerializedName("offerList")
    @Expose
    private ArrayList<OfferList> offerList = new ArrayList<OfferList>();
    @SerializedName("descriptionEn")
    @Expose
    private String descriptionEn;
    @SerializedName("descriptionAr")
    @Expose
    private String descriptionAr;
    @SerializedName("daysLeft")
    @Expose
    private String daysLeft;

    public String getThrotleLabel() {
        return throtleLabel;
    }

    public void setThrotleLabel(String throtleLabel) {
        this.throtleLabel = throtleLabel;
    }

    public String getDisplayLabel() {
        return displayLabel;
    }

    public void setDisplayLabel(String displayLabel) {
        this.displayLabel = displayLabel;
    }

    @SerializedName("throtleLabel")
    @Expose
    private String throtleLabel;
    @SerializedName("socialLabel")
    @Expose
    private String socialLabel;
    @SerializedName("displayName")
    @Expose
    private String displayLabel;

    public String getThrotlePercentage() {
        return throtlePercentage;
    }

    public void setThrotlePercentage(String throtlePercentage) {
        this.throtlePercentage = throtlePercentage;
    }

    @SerializedName("throtlePercentage")
    @Expose
    private String throtlePercentage;
    int isAnimated = 0;

    public UsagePackageList() {
    }

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The nameEn
     */
    public String getNameEn() {
        return nameEn;
    }

    /**
     * @param nameEn The nameEn
     */
    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    /**
     * @return The nameAr
     */
    public String getNameAr() {
        return nameAr;
    }

    /**
     * @param nameAr The nameAr
     */
    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }

    /**
     * @return The startDate
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * @param startDate The startDate
     */
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    /**
     * @return The endDate
     */
    public String getEndDate() {
        return endDate;
    }

    /**
     * @param endDate The endDate
     */
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    /**
     * @return The iconUrl
     */
    public String getIconUrl() {
        return iconUrl;
    }

    /**
     * @param iconUrl The iconUrl
     */
    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    /**
     * @return The imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * @param imageUrl The imageUrl
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     * @return The packageType
     */
    public Integer getPackageType() {
        return packageType;
    }

    /**
     * @param packageType The packageType
     */
    public void setPackageType(Integer packageType) {
        this.packageType = packageType;
    }

    /**
     * @return The packageDisabled
     */
    public Boolean getPackageDisabled() {
        return packageDisabled;
    }

    /**
     * @param packageDisabled The packageDisabled
     */
    public void setPackageDisabled(Boolean packageDisabled) {
        this.packageDisabled = packageDisabled;
    }

    /**
     * @return The offerList
     */
    public ArrayList<OfferList> getOfferList() {
        return offerList;
    }

    /**
     * @param offerList The offerList
     */
    public void setOfferList(ArrayList<OfferList> offerList) {
        this.offerList = offerList;
    }

    /**
     * @return The descriptionEn
     */
    public String getDescriptionEn() {
        return descriptionEn;
    }

    /**
     * @param descriptionEn The descriptionEn
     */
    public void setDescriptionEn(String descriptionEn) {
        this.descriptionEn = descriptionEn;
    }

    /**
     * @return The descriptionAr
     */
    public String getDescriptionAr() {
        return descriptionAr;
    }

    /**
     * @param descriptionAr The descriptionAr
     */
    public void setDescriptionAr(String descriptionAr) {
        this.descriptionAr = descriptionAr;
    }


    public int getIsAnimated() {
        return isAnimated;
    }

    public void setIsAnimated(int isAnimated) {
        this.isAnimated = isAnimated;
    }

    public String getSocialLabel() {
        return socialLabel;
    }

    public void setSocialLabel(String socialLabel) {
        this.socialLabel = socialLabel;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.nameEn);
        dest.writeString(this.nameAr);
        dest.writeString(this.startDate);
        dest.writeString(this.endDate);
        dest.writeString(this.iconUrl);
        dest.writeString(this.imageUrl);
        dest.writeValue(this.packageType);
        dest.writeValue(this.packageDisabled);
        dest.writeTypedList(this.offerList);
        dest.writeString(this.descriptionEn);
        dest.writeString(this.descriptionAr);
        dest.writeString(this.daysLeft);
        dest.writeString(this.throtleLabel);
        dest.writeString(this.socialLabel);
        dest.writeString(this.displayLabel);
        dest.writeString(this.throtlePercentage);
        dest.writeInt(this.isAnimated);
    }

    protected UsagePackageList(Parcel in) {
        this.id = in.readString();
        this.nameEn = in.readString();
        this.nameAr = in.readString();
        this.startDate = in.readString();
        this.endDate = in.readString();
        this.iconUrl = in.readString();
        this.imageUrl = in.readString();
        this.packageType = (Integer) in.readValue(Integer.class.getClassLoader());
        this.packageDisabled = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.offerList = in.createTypedArrayList(OfferList.CREATOR);
        this.descriptionEn = in.readString();
        this.descriptionAr = in.readString();
        this.daysLeft = in.readString();
        this.throtleLabel = in.readString();
        this.socialLabel = in.readString();
        this.displayLabel = in.readString();
        this.throtlePercentage = in.readString();
        this.isAnimated = in.readInt();
    }

    public static final Creator<UsagePackageList> CREATOR = new Creator<UsagePackageList>() {
        @Override
        public UsagePackageList createFromParcel(Parcel source) {
            return new UsagePackageList(source);
        }

        @Override
        public UsagePackageList[] newArray(int size) {
            return new UsagePackageList[size];
        }
    };
}
