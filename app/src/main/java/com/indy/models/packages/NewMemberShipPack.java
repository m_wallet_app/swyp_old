package com.indy.models.packages;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.squad_number.models.SquadNumber;

public class NewMemberShipPack implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("squadNumber")
    @Expose
    private SquadNumber squadNumber;
    @SerializedName("usagePackageList")
    @Expose
    private List<UsagePackageList> usagePackageList = null;
    @SerializedName("nameEn")
    @Expose
    private String nameEn;
    @SerializedName("nameAr")
    @Expose
    private String nameAr;
    @SerializedName("startDate")
    @Expose
    private String startDate;
    @SerializedName("endDate")
    @Expose
    private String endDate;
    @SerializedName("packageType")
    @Expose
    private Integer packageType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public SquadNumber getSquadNumber() {
        return squadNumber;
    }

    public void setSquadNumber(SquadNumber squadNumber) {
        this.squadNumber = squadNumber;
    }

    public List<UsagePackageList> getUsagePackageList() {
        return usagePackageList;
    }

    public void setUsagePackageList(List<UsagePackageList> usagePackageList) {
        this.usagePackageList = usagePackageList;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameAr() {
        return nameAr;
    }

    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getPackageType() {
        return packageType;
    }

    public void setPackageType(Integer packageType) {
        this.packageType = packageType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeParcelable(this.squadNumber, flags);
        dest.writeTypedList(this.usagePackageList);
        dest.writeString(this.nameEn);
        dest.writeString(this.nameAr);
        dest.writeString(this.startDate);
        dest.writeString(this.endDate);
        dest.writeValue(this.packageType);
    }

    public NewMemberShipPack() {
    }

    protected NewMemberShipPack(Parcel in) {
        this.id = in.readString();
        this.squadNumber = in.readParcelable(SquadNumber.class.getClassLoader());
        this.usagePackageList = in.createTypedArrayList(UsagePackageList.CREATOR);
        this.nameEn = in.readString();
        this.nameAr = in.readString();
        this.startDate = in.readString();
        this.endDate = in.readString();
        this.packageType = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Creator<NewMemberShipPack> CREATOR = new Creator<NewMemberShipPack>() {
        @Override
        public NewMemberShipPack createFromParcel(Parcel source) {
            return new NewMemberShipPack(source);
        }

        @Override
        public NewMemberShipPack[] newArray(int size) {
            return new NewMemberShipPack[size];
        }
    };
}