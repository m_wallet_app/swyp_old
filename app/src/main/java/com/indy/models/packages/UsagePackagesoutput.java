package com.indy.models.packages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by emad on 9/28/16.
 */

public class UsagePackagesoutput extends MasterErrorResponse {

    @SerializedName("usagePackageList")
    @Expose
    private List<UsagePackageList> usagePackageList = new ArrayList<UsagePackageList>();
    @SerializedName("newMemberShipPack")
    @Expose
    private NewMemberShipPack newMemberShipPack = null;

    /**
     * @return The usagePackageList
     */
    public List<UsagePackageList> getUsagePackageList() {
        return usagePackageList;
    }

    /**
     * @param usagePackageList The usagePackageList
     */
    public void setUsagePackageList(List<UsagePackageList> usagePackageList) {
        this.usagePackageList = usagePackageList;
    }

    public NewMemberShipPack getNewMemberShipPack() {
        return newMemberShipPack;
    }

    public void setNewMemberShipPack(NewMemberShipPack newMemberShipPack) {
        this.newMemberShipPack = newMemberShipPack;
    }

}

