package com.indy.models.packages;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.utils.CommonMethods;

/**
 * Created by emad on 8/11/16.
 */
public class PackageConsumption implements Parcelable {
    @SerializedName("type")
    @Expose
    private Double type;
    @SerializedName("quota")
    @Expose
    private Double quota;
    @SerializedName("consumption")
    @Expose
    private Double consumption;
    @SerializedName("remaining")
    @Expose
    private Double remaining;
    @SerializedName("consumptionUnitEn")
    @Expose
    private String consumptionUnitEn;
    @SerializedName("consumptionUnitAr")
    @Expose
    private String consumptionUnitAr;
    @SerializedName("disabled")
    @Expose
    private Boolean disabled;
    @SerializedName("textEn")
    @Expose
    private String textEn;
    @SerializedName("textAr")
    @Expose
    private String textAr;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @SerializedName("displayName")
    @Expose
    private String displayName;

    public PackageConsumption(){

}

    /**
     * @return The type
     */
    public Double getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(Double type) {
        this.type = type;
    }

    /**
     * @return The quota
     */
    public Double getQuota() {
        return CommonMethods.getFormattedDouble(quota);
    }

    /**
     * @param quota The quota
     */
    public void setQuota(Double quota) {
        this.quota = quota;
    }

    /**
     * @return The consumption
     */
    public Double getConsumption() {
        return CommonMethods.getFormattedDouble(consumption);
    }

    /**
     * @param consumption The consumption
     */
    public void setConsumption(Double consumption) {
        this.consumption = consumption;
    }

    /**
     * @return The remaining
     */
    public Double getRemaining() {

        return CommonMethods.getFormattedDouble(remaining);
    }

    /**
     * @param remaining The remaining
     */
    public void setRemaining(Double remaining) {
        this.remaining = remaining;
    }

    /**
     * @return The consumptionUnitEn
     */
    public String getConsumptionUnitEn() {
        return consumptionUnitEn;
    }

    /**
     * @param consumptionUnitEn The consumptionUnitEn
     */
    public void setConsumptionUnitEn(String consumptionUnitEn) {
        this.consumptionUnitEn = consumptionUnitEn;
    }

    /**
     * @return The consumptionUnitAr
     */
    public String getConsumptionUnitAr() {
        return consumptionUnitAr;
    }

    /**
     * @param consumptionUnitAr The consumptionUnitAr
     */
    public void setConsumptionUnitAr(String consumptionUnitAr) {
        this.consumptionUnitAr = consumptionUnitAr;
    }

    /**
     * @return The disabled
     */
    public Boolean getDisabled() {
        return disabled;
    }

    /**
     * @param disabled The disabled
     */
    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    /**
     * @return The textEn
     */
    public String getTextEn() {
        return textEn;
    }

    /**
     * @param textEn The textEn
     */
    public void setTextEn(String textEn) {
        this.textEn = textEn;
    }

    /**
     * @return The textAr
     */
    public String getTextAr() {
        return textAr;
    }

    /**
     * @param textAr The textAr
     */
    public void setTextAr(String textAr) {
        this.textAr = textAr;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.type);
        dest.writeValue(this.quota);
        dest.writeValue(this.consumption);
        dest.writeValue(this.remaining);
        dest.writeString(this.consumptionUnitEn);
        dest.writeString(this.consumptionUnitAr);
        dest.writeValue(this.disabled);
        dest.writeString(this.textEn);
        dest.writeString(this.textAr);
        dest.writeString(this.displayName);
    }

    protected PackageConsumption(Parcel in) {
        this.type = (Double) in.readValue(Double.class.getClassLoader());
        this.quota = (Double) in.readValue(Double.class.getClassLoader());
        this.consumption = (Double) in.readValue(Double.class.getClassLoader());
        this.remaining = (Double) in.readValue(Double.class.getClassLoader());
        this.consumptionUnitEn = in.readString();
        this.consumptionUnitAr = in.readString();
        this.disabled = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.textEn = in.readString();
        this.textAr = in.readString();
        this.displayName = in.readString();
    }

    public static final Creator<PackageConsumption> CREATOR = new Creator<PackageConsumption>() {
        @Override
        public PackageConsumption createFromParcel(Parcel source) {
            return new PackageConsumption(source);
        }

        @Override
        public PackageConsumption[] newArray(int size) {
            return new PackageConsumption[size];
        }
    };
}
