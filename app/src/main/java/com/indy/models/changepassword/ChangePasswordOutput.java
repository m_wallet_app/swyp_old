package com.indy.models.changepassword;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by emad on 9/27/16.
 */

public class ChangePasswordOutput extends MasterErrorResponse {
    @SerializedName("updated")
    @Expose
    private Boolean updated;

    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    public Boolean getUpdated() {
        return updated;
    }
}
