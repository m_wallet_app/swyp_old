package com.indy.models.autorenewalOff;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterInputResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hadi.mehmood on 11/13/2016.
 */
public class AutoRenewalInputModel extends MasterInputResponse {

    @SerializedName("orderItems")
    @Expose
    private List<OrderItemRenewal> orderItems = new ArrayList<OrderItemRenewal>();

    public List<OrderItemRenewal> getOrderItems() {
        return orderItems;
    }

    /**
     *
     * @param orderItems
     * The orderItems
     */
    public void setOrderItems(List<OrderItemRenewal> orderItems) {
        this.orderItems = orderItems;
    }

}
