package com.indy.models.autorenewalOff;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by hadi.mehmood on 10/25/2016.
 */
public class OrderItemRenewal implements Serializable
{
    @SerializedName("preferedMsisdn")
    @Expose
    private String preferedMsisdn;
    @SerializedName("offerId")
    @Expose
    private String offerId;

    public String getPreferedMsisdn ()
    {
        return preferedMsisdn;
    }

    public void setPreferedMsisdn (String preferedMsisdn)
    {
        this.preferedMsisdn = preferedMsisdn;
    }


    public String getOfferId ()
    {
        return offerId;
    }

    public void setOfferId (String offerId)
    {
        this.offerId = offerId;
    }

}
