package com.indy.models.acceptDeleteTransferProcesses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterErrorResponse;

/**
 * Created by hadi.mehmood on 10/17/2016.
 */
public class AcceptDeleteTransferProcessesOutput extends MasterErrorResponse {

    @SerializedName("processed")
    @Expose
    private String processed;

    public String getProcessed() {
        return processed;
    }

    public void setProcessed(String processed) {
        this.processed = processed;
    }
}
