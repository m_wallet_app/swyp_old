package com.indy.models.acceptDeleteTransferProcesses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indy.models.utils.MasterInputResponse;

/**
 * Created by hadi.mehmood on 10/17/2016.
 */
public class AcceptDeleteTransferProcessesInput extends MasterInputResponse {

    @SerializedName("transactionId")
    @Expose
    String requestId;

    @SerializedName("status")
    @Expose
    String actionType;

    @SerializedName("targetMsisdn")
    @Expose
    String targetMsisdn;

    @SerializedName("amount")
    @Expose
    String amount;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getTargetMsisdn() {
        return targetMsisdn;
    }

    public void setTargetMsisdn(String targetMsisdn) {
        this.targetMsisdn = targetMsisdn;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
