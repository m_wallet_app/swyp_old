package com.indy.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.indy.R;
import com.indy.controls.OnLockNumberInterface;
import com.indy.models.payperuse.PaymentPackageList;
import com.indy.utils.SharedPrefrencesManger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by emad on 8/11/16.
 */
public class PayPerUseAdapter extends RecyclerView.Adapter<PayPerUseAdapter.ViewHolder> {
    private List<PaymentPackageList> paymentPackageList;
    private Context mContext;
    private OnLockNumberInterface onLockNumberInterface;
    ArrayList<String> arrayList;
    public SharedPrefrencesManger sharedPrefrencesManger;

    public PayPerUseAdapter(List<PaymentPackageList> paymentPackageList, Context context) {
        this.paymentPackageList = paymentPackageList;
        this.mContext = context;

        arrayList = new ArrayList<String>();
        sharedPrefrencesManger = new SharedPrefrencesManger(mContext);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.pay_per_use_item, viewGroup, false);
        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final PaymentPackageList paymentPackageListItem = paymentPackageList.get(position);

        if(sharedPrefrencesManger.getLanguage().equals("en")){
            holder.nameID.setText(paymentPackageListItem.getNameEn());
            holder.consumptionID.setText(paymentPackageListItem.getConsumption() + " " + paymentPackageListItem.getConsumptionUnitEn());
            holder.amountID.setText(paymentPackageListItem.getAmountUnitEn() + " " + paymentPackageListItem.getAmount());

        }else {
            holder.nameID.setText(paymentPackageListItem.getNameAr());
            holder.consumptionID.setText(paymentPackageListItem.getConsumption() + " " + paymentPackageListItem.getConsumptionUnitAr());
            holder.amountID.setText( paymentPackageListItem.getAmount() + " " + paymentPackageListItem.getAmountUnitAr());

        }


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return paymentPackageList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView logoImg;
        public TextView nameID;
        public TextView consumptionID;
        public TextView amountID;
        public View viewRvPayUse;

        public ViewHolder(View v) {
            super(v);
            //        logoImg = (ImageView) v.findViewById(R.id.logoImg);
            nameID = (TextView) v.findViewById(R.id.nameID);
            consumptionID = (TextView) v.findViewById(R.id.consumptionID);
            amountID = (TextView) v.findViewById(R.id.amountID);
           // viewRvPayUse = (View) v.findViewById(R.id.viewRvPayUse);
        }
    }

}
