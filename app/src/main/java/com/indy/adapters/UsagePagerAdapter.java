package com.indy.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.indy.utils.SharedPrefrencesManger;
import com.indy.views.fragments.usage.PackagesFragments;
import com.indy.views.fragments.usage.PayPerUseFragments;
import com.indy.views.fragments.usage.UsageFragment;
import com.indy.views.fragments.usage.WifiHotSpotFragment;

/**
 * Created by emad on 8/9/16.
 */
public class UsagePagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    SharedPrefrencesManger sharedPrefrencesManger;
    UsageFragment usageFragmentInstance;

    public UsagePagerAdapter(FragmentManager fm, int NumOfTabs, Context context, UsageFragment usageFragmentInstance) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        sharedPrefrencesManger = new SharedPrefrencesManger(context);
        this.usageFragmentInstance = usageFragmentInstance;
    }


    @Override
    public Fragment getItem(int position) {

//        switch (position) {
//            case 2:
//                WifiHotSpotFragment wifiHotspots = new WifiHotSpotFragment();
//                return wifiHotspots;
//
//            case 1:
//                PayPerUseFragments payPerUseFragments = new PayPerUseFragments();
//                return payPerUseFragments;
//
//            default:
//            case 0:
//                PackagesFragments packagesFragments = new PackagesFragments();
//                return packagesFragments;
//
//        }
        if (sharedPrefrencesManger != null) {
            if (sharedPrefrencesManger.getUsageStaus() == true)
                return onPackagesEnabled(position);
            else
                return onPackagesDisabled(position);
        } else {
            return onPackagesEnabled(position);
        }
    }

    private Fragment onPackagesEnabled(int position) {
        switch (position) {
            case 2:
                WifiHotSpotFragment wifiHotspots = new WifiHotSpotFragment();
                return wifiHotspots;

            case 1:
                PayPerUseFragments payPerUseFragments = new PayPerUseFragments();
                return payPerUseFragments;

            case 0:
                PackagesFragments packagesFragments = new PackagesFragments();
                return packagesFragments;
            default:
                return new PackagesFragments();
        }
    }

    private Fragment onPackagesDisabled(int position) {
        switch (position) {
            case 1:
                WifiHotSpotFragment wifiHotspots = new WifiHotSpotFragment();
                return wifiHotspots;

            case 0:
                PayPerUseFragments payPerUseFragments = new PayPerUseFragments();
                return payPerUseFragments;

            default:
                return new PayPerUseFragments();
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }


}

