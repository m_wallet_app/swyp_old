package com.indy.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indy.R;
import com.indy.customviews.CustomTextView;
import com.indy.models.getStaticBundles.BundleList;
import com.indy.utils.SharedPrefrencesManger;

import java.util.List;

/**
 * Created by emad on 8/11/16.
 */
public class StaticPackagesListAdapter extends RecyclerView.Adapter<StaticPackagesListAdapter.ViewHolder> {
    private List<BundleList> mUsagePackageLists;
    private Context mContext;
    private ViewHolder holder;
    public static BundleList usagePackageItem;
    public SharedPrefrencesManger mSharedPrefrencesManger;
    public StaticPackagesListAdapter(List<BundleList> usagePackageList, Context context) {
        mUsagePackageLists = usagePackageList;

        this.mContext = context;
        this.mSharedPrefrencesManger = new SharedPrefrencesManger(mContext);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.static_package_item, viewGroup, false);

        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        usagePackageItem = mUsagePackageLists.get(position);
        if(mSharedPrefrencesManger.isVATEnabled()){
            holder.tv_vatValue.setVisibility(View.VISIBLE);
        }else{
            holder.tv_vatValue.setVisibility(View.GONE);
        }
        if(usagePackageItem.getBundleName().equals("dummy element") && usagePackageItem.getNameEn().equals("dummy element")){
            holder.tv_category.setVisibility(View.INVISIBLE);
            holder.tv_name.setVisibility(View.INVISIBLE);
            holder.tv_price.setVisibility(View.INVISIBLE);
            holder.tv_subTitle.setVisibility(View.INVISIBLE);
            holder.cardView.setVisibility(View.VISIBLE);


        }
        if(mSharedPrefrencesManger!=null && mSharedPrefrencesManger.getLanguage().equals("en")) {
            holder.tv_category.setText(usagePackageItem.getDescriptionEn());
            holder.tv_subTitle.setText(usagePackageItem.getBundleName());
            holder.tv_name.setText(usagePackageItem.getNameEn());
            holder.tv_price.setText(usagePackageItem.getPriceUnitEn()+ " " + usagePackageItem.getPrice());
        }else if(mSharedPrefrencesManger!=null && mSharedPrefrencesManger.getLanguage().equals("ar")){
            holder.tv_category.setText(usagePackageItem.getDescriptionAr());
            holder.tv_subTitle.setText(usagePackageItem.getBundleNameAr());
            holder.tv_name.setText(usagePackageItem.getNameAr());
            holder.tv_price.setText(usagePackageItem.getPrice()+ " " +usagePackageItem.getPriceUnitAr());
        }

    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mUsagePackageLists.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public CustomTextView tv_name, tv_price, tv_category, tv_subTitle, tv_vatValue;
        public CardView cardView;
        public ViewHolder(View v) {
            super(v);

            tv_category = (CustomTextView) v.findViewById(R.id.tv_category);
            tv_price = (CustomTextView) v.findViewById(R.id.tv_price);
            tv_name = (CustomTextView) v.findViewById(R.id.tv_name);
            tv_subTitle = (CustomTextView) v.findViewById(R.id.tv_new_item);
            cardView = (CardView) v.findViewById(R.id.minutes_card_view);
            tv_vatValue = (CustomTextView) v.findViewById(R.id.tv_vat_tax);
            //  data packages

        }
    }

}
