package com.indy.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.indy.R;
import com.indy.utils.ConstantUtils;
import com.indy.views.activites.SwpeMainActivity;
import com.indy.views.fragments.rewards.ItemsCarousel.MainAvailableRewardsFragment;
import com.indy.views.fragments.rewards.UsedRewardsFragment;

/**
 * Created by emad on 8/9/16.
 */
public class RewardsPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    Bundle getArguments;

    public RewardsPagerAdapter(FragmentManager fm, int NumOfTabs, Bundle getArguments) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.getArguments = getArguments;
    }


    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                MainAvailableRewardsFragment mainAvailableRewardsFragment = new MainAvailableRewardsFragment();
                if (getArguments != null && getArguments.containsKey(ConstantUtils.KEY_NOTIFICATION_ITEM_ID)) {
                    Bundle bundle = new Bundle();
                    bundle.putString(ConstantUtils.KEY_NOTIFICATION_ITEM_ID, getArguments.getString(ConstantUtils.KEY_NOTIFICATION_ITEM_ID));
                    mainAvailableRewardsFragment.setArguments(bundle);
                }
                return mainAvailableRewardsFragment;
//                return new ErrorFragment();

            case 1:
                UsedRewardsFragment usedRewardsFragment = new UsedRewardsFragment();
                return usedRewardsFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }


}

