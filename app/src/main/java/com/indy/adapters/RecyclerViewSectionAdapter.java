package com.indy.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.models.history.HistoryDataModel;
import com.indy.utils.SharedPrefrencesManger;
import com.indy.views.fragments.store.recharge.ItemDecoration.SectionedRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Amir.jehangir on 10/18/2016.
 */
public class RecyclerViewSectionAdapter extends SectionedRecyclerViewAdapter<RecyclerView.ViewHolder> {


    private List<HistoryDataModel> allData;
    LinearLayout.LayoutParams normalLayoutParams;
    LinearLayout.LayoutParams lastLayoutParams;
    Context mContext;
    SharedPrefrencesManger sharedPrefrencesManger;

    public RecyclerViewSectionAdapter(List<HistoryDataModel> data, Context context) {

        this.allData = data;
        this.mContext = context;
        sharedPrefrencesManger = new SharedPrefrencesManger(mContext);
    }


    @Override
    public int getSectionCount() {
        return allData.size();
    }

    @Override
    public int getItemCount(int section) {

        return allData.get(section).getAllItemsInSection().size();

    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int section) {

        String sectionName = allData.get(section).getHeaderTitle();
        SectionViewHolder sectionViewHolder = (SectionViewHolder) holder;
        sectionViewHolder.sectionTitle.setText(sectionName);
        if (section == 0)
            sectionViewHolder.sectionSpacing.setVisibility(View.GONE);
        else
            sectionViewHolder.sectionSpacing.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int section, int relativePosition, int absolutePosition) {
        String finalAmount = "";
        ArrayList<String> itemsInSection = allData.get(section).getAllItemsInSection();

        String itemName = itemsInSection.get(relativePosition);

        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

        itemViewHolder.itemTitle.setText(itemName);

        String itemsAmount = allData.get(section).getMonthlyTransList()[section].getTransactionList()[relativePosition].getAmount();

        float amountDouble = 0.00f;

        if (itemsAmount != null && itemsAmount.length() > 0) {
         amountDouble = Float.parseFloat(itemsAmount);
        }

        if (allData.get(section).getMonthlyTransList()[section].getTransactionList()[relativePosition]
                .getType().equals("debit")) {

            itemViewHolder.itemAmoutn.setText("- " + (float)Math.round(amountDouble * 100) / 100 + "  " + mContext.getResources().getString(R.string.aed));
        } else {
            itemViewHolder.itemAmoutn.setText("+ " + (float)Math.round(amountDouble * 100) / 100 + "  " + mContext.getResources().getString(R.string.aed));

        }


        String itemsDates = allData.get(section).getMonthlyTransList()[section].getTransactionList()[relativePosition].getDate();

        itemViewHolder.itemDate.setText(itemsDates);

        String itemstime = allData.get(section).getMonthlyTransList()[section].getTransactionList()[relativePosition].getType();

//        itemViewHolder.itemtime.setText(itemstime +"min");
//        normalLayoutParams = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT
//        );
//
//        lastLayoutParams = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.MATCH_PARENT,
//                LinearLayout.LayoutParams.MATCH_PARENT
//        );
//        ((ItemViewHolder) holder).itemTitle.setLayoutParams(normalLayoutParams);

        // Try to put a image . for sample i set background color in xml layout file
        // itemViewHolder.itemImage.setBackgroundColor(Color.parseColor("#01579b"));
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, boolean header) {
        View v = null;
        if (header)

        {
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_section, parent, false);
            return new SectionViewHolder(v);
        } else {
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item, parent, false);
            return new ItemViewHolder(v);
        }

    }


    // SectionViewHolder Class for Sections
    public static class SectionViewHolder extends RecyclerView.ViewHolder {


        final TextView sectionTitle;
        final TextView sectionSpacing;

        public SectionViewHolder(View itemView) {
            super(itemView);

            sectionTitle = (TextView) itemView.findViewById(R.id.sectionTitle);
            sectionSpacing = (TextView) itemView.findViewById(R.id.tv_item_sep);

        }
    }

    // ItemViewHolder Class for Items in each Section
    public static class ItemViewHolder extends RecyclerView.ViewHolder {

        final TextView itemTitle;
        TextView itemAmoutn;
        TextView itemDate;
        TextView itemtime;

        //     final ImageView itemImage;


        public ItemViewHolder(View itemView) {
            super(itemView);
            itemTitle = (TextView) itemView.findViewById(R.id.itemname);
            itemAmoutn = (TextView) itemView.findViewById(R.id.itemAmoutn);
            itemDate = (TextView) itemView.findViewById(R.id.itemDate);
            itemtime = (TextView) itemView.findViewById(R.id.itemtime);
            //    itemImage = (ImageView) itemView.findViewById(R.id.itemImage);

//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    Toast.makeText(v.getContext(), itemTitle.getText(), Toast.LENGTH_SHORT).show();
//
//                }
//            });
        }
    }
}