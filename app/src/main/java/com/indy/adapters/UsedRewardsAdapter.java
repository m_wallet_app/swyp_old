package com.indy.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.indy.R;
import com.indy.models.rewards.UsedRewardsList;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by emad on 8/11/16.
 */
public class UsedRewardsAdapter extends RecyclerView.Adapter<UsedRewardsAdapter.ViewHolder> {
    private List<UsedRewardsList> rewardsLists;
    private Context mContext;

    public UsedRewardsAdapter(List<UsedRewardsList> mRewardsList, Context context) {
        rewardsLists = mRewardsList;
        this.mContext = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.fragment_used_rewards_item, viewGroup, false);
        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final UsedRewardsList rewardsListItem = rewardsLists.get(position);
        holder.rewardName.setText(rewardsListItem.getNameEn());
        holder.rewardPrice.setText(mContext.getString(R.string.aed)+" "+ rewardsListItem.getAverageSaving() + " " + mContext.getString(R.string.saved));
        holder.rewardsUsed.setText(mContext.getString(R.string.used_on) + " " );//+ rewardsListItem.getUsedDate());

        Picasso.with(mContext).load(rewardsListItem.getImageUrl()).into(holder.rewardImg);

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return rewardsLists.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView rewardName;
        public TextView rewardPrice;
        public TextView rewardsSaved;
        public TextView rewardsUsed;
        private ImageView rewardImg;

        public ViewHolder(View v) {
            super(v);
            rewardName = (TextView) v.findViewById(R.id.rewardName);
            rewardPrice = (TextView) v.findViewById(R.id.rewardPrice);

            rewardsUsed = (TextView) v.findViewById(R.id.usedID);
            rewardImg = (ImageView) v.findViewById(R.id.rewardImg);
        }
    }

}
