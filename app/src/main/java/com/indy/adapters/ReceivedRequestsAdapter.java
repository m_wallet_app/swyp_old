package com.indy.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.indy.R;
import com.indy.controls.OnAcceptReceivedRequestListener;
import com.indy.controls.OnDeleteReceivedRequestListener;
import com.indy.controls.OnEditAmountAlertClicked;
import com.indy.models.getPendingTransfersList.requestCreditFromFriend.TransacitonList;
import com.indy.models.rewards.StoreLocation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by hadi on 10/14/2016.
 */
public class ReceivedRequestsAdapter extends RecyclerView.Adapter<ReceivedRequestsAdapter.ViewHolder> {
    private List<TransacitonList> mReceivedRequestModel;
    private Context mContext;
    public OnAcceptReceivedRequestListener onAcceptReceivedRequestListener;
    public OnDeleteReceivedRequestListener onDeleteReceivedRequestListener;
    OnEditAmountAlertClicked onEditAmountAlertClicked;
    public static List<StoreLocation> storeLocations;
    private Typeface tf;

    public ReceivedRequestsAdapter(List<TransacitonList> mRewardsList, Context context,
                                   OnAcceptReceivedRequestListener onAcceptReceivedRequestListener,
                                   OnDeleteReceivedRequestListener onDeleteReceivedRequestListener,
                                   OnEditAmountAlertClicked onEditAmountAlertClicked) {
        mReceivedRequestModel = mRewardsList;
        this.mContext = context;
        this.onAcceptReceivedRequestListener = onAcceptReceivedRequestListener;
        this.onDeleteReceivedRequestListener = onDeleteReceivedRequestListener;
        this.onEditAmountAlertClicked = onEditAmountAlertClicked;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.item_received_requests, viewGroup, false);
        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    //parseDate

    public String parseDate(String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        try {
            Date dateValue = dateFormat.parse(date);
            dateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm");
            return dateFormat.format(dateValue);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final TransacitonList rewardsListItem = mReceivedRequestModel.get(position);
        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/OpenSans-Semibold.ttf");
        holder.tv_number.setTypeface(tf);
        holder.tv_number.setText(rewardsListItem.getMsisdn());
        holder.tv_price.setText(mContext.getString(R.string.aed) + " " + rewardsListItem.getAmount());


        holder.tv_date.setText(parseDate(rewardsListItem.getDate()));
        holder.btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onDeleteReceivedRequestListener.onDeleteReceivedRequest(position);
            }
        });
        holder.btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.et_amount.getVisibility() == View.VISIBLE) {
                    onAcceptReceivedRequestListener.onAcceptReceivedRequest(position, true, holder.et_amount.getText().toString());
                } else {
                    onAcceptReceivedRequestListener.onAcceptReceivedRequest(position, false, "0");

                }
            }
        });

        holder.tv_editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.et_amount.startAnimation(new AlphaAnimation(0.0f, 1.0f));
                holder.et_amount.setVisibility(View.VISIBLE);
                holder.tv_editText.setVisibility(View.INVISIBLE);
                onEditAmountAlertClicked.onEditRequestBeforeSending(position);
            }
        });

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mReceivedRequestModel.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_number;
        TextView tv_price;
        TextView tv_date;
        ImageView btn_accept;
        ImageView btn_delete;
        TextInputLayout til_amount;
        EditText et_amount;
        TextView tv_editText;

        public ViewHolder(View v) {
            super(v);
            tv_number = (TextView) v.findViewById(R.id.tv_mobileNo);
            tv_price = (TextView) v.findViewById(R.id.tv_amount);
            tv_date = (TextView) v.findViewById(R.id.tv_date);
            btn_accept = (ImageView) v.findViewById(R.id.btn_accept_received_request);
            btn_delete = (ImageView) v.findViewById(R.id.btn_delete_received_request);
            et_amount = (EditText) v.findViewById(R.id.et_amount);
            til_amount = (TextInputLayout) v.findViewById(R.id.amountInputLayout);
            tv_editText = (TextView) v.findViewById(R.id.tv_edit_amount);
        }
    }

    public void remove(int position) {
        mReceivedRequestModel.remove(position);
        this.notifyDataSetChanged();
    }



}
