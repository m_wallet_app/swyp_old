package com.indy.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.models.rewards.RewardsList;
import com.indy.models.rewards.StoreLocation;
import com.indy.views.activites.RewardsDetailsActivity;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by emad on 8/11/16.
 */
public class AvilableRewardsAdapter extends RecyclerView.Adapter<AvilableRewardsAdapter.ViewHolder> {
    private List<RewardsList> rewardsLists;
    private Context mContext;
    public static List<StoreLocation> storeLocations;

    public AvilableRewardsAdapter(List<RewardsList> mRewardsList, Context context) {
        rewardsLists = mRewardsList;
        this.mContext = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.fragment_rewards_item, viewGroup, false);
        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final RewardsList rewardsListItem = rewardsLists.get(position);
        holder.rewardName.setText(rewardsListItem.getNameEn());
        holder.rewardPrice.setText(rewardsListItem.getAverageSaving() + " " + mContext.getString(R.string.aed));
        holder.rewardExpirationDate.setText(mContext.getString(R.string.expires) + " ");//rewardsListItem.getExpiryDate());
//        Picasso.with(mContext).load(rewardsListItem.getImageUrl()).into(holder.rewardImg);
        Picasso.with(mContext)
                .load(rewardsListItem.getImageUrl())
                .placeholder(R.drawable.bitmap_default_image)
                .networkPolicy(NetworkPolicy.NO_STORE)
                .error(R.drawable.bitmap_default_image)
                .into(holder.rewardImg);

        holder.rewardCount.setText(position + 1 + "/" + rewardsLists.size());
        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                storeLocations = rewardsLists.get(position).getStoreLocations();
                Intent intent = new Intent(mContext, RewardsDetailsActivity.class);
                intent.putExtra("REWARDSLIST", rewardsLists.get(position));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return rewardsLists.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView rewardName;
        public TextView rewardPrice;
        public TextView rewardExpirationDate;
        public TextView tv_membership_cycle;
        private ImageView rewardImg;
        private LinearLayout card_view;
        private TextView rewardCount;

        public ViewHolder(View v) {
            super(v);
            rewardName = (TextView) v.findViewById(R.id.rewardName);
            rewardPrice = (TextView) v.findViewById(R.id.rewardPrice);
            rewardExpirationDate = (TextView) v.findViewById(R.id.expiresID);
            rewardImg = (ImageView) v.findViewById(R.id.rewardImg);
            card_view = (LinearLayout) v.findViewById(R.id.card_view);
            rewardCount = (TextView) v.findViewById(R.id.tv_count_);
            tv_membership_cycle = (TextView) v.findViewById(R.id.tv_membership_cycle);
        }
    }

}
