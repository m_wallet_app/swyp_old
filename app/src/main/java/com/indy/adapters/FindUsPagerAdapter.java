package com.indy.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.indy.views.fragments.findus.PaymentMachineFragment;
import com.indy.views.fragments.findus.ShopsFragment;
import com.indy.views.fragments.findus.WifiHotspotsFindUsFragment;

/**
 * Created by hadi on 11/10/16.
 */
public class FindUsPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public FindUsPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }


    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:

                PaymentMachineFragment payPerUseFragments = new PaymentMachineFragment();
                return payPerUseFragments;
            case 1:
                ShopsFragment shopsFragment= new ShopsFragment();
                return shopsFragment;
            default:
            case 2:
                WifiHotspotsFindUsFragment wifiHotspots = new WifiHotspotsFindUsFragment();
                return wifiHotspots;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }


}

