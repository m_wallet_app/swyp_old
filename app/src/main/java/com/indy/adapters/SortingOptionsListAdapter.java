package com.indy.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

import com.indy.R;
import com.indy.models.rewards.Category;
import com.indy.utils.ConstantUtils;
import com.indy.utils.SharedPrefrencesManger;
import com.indy.views.fragments.gamification.events.EventDetailActivity;

import java.util.List;

public class SortingOptionsListAdapter extends RecyclerView.Adapter<SortingOptionsListAdapter.ViewHolder> {
    private List<Category> rewardsLists;
    private Context mContext;
    private SharedPrefrencesManger sharedPrefrencesManger;
    public SortingOptionsListAdapter(List<Category> mRewardsList, Context context) {
        rewardsLists = mRewardsList;
        this.mContext = context;
        this.sharedPrefrencesManger = new SharedPrefrencesManger(context);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.fragment_category_item, viewGroup, false);
        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Category rewardsListItem = rewardsLists.get(position);

        if (sharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)) {
            holder.categoryName.setText(rewardsLists.get(position).getCategoryNameEn());

        }else{
            holder.categoryName.setText(rewardsLists.get(position).getCategoryNameAr());

        }

        holder.categoryName.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    holder.categoryName.setTextColor(mContext.getResources().getColor(R.color.title_usage_pink));
                    //TODO: bind category name here
//                    EventDetailActivity.selectedCategory = (rewardsListItem.getCategoryName());
                    EventDetailActivity.selectedCategory = ConstantUtils.TOP_RATED;
                    holder.iv_checkBox.setVisibility(View.VISIBLE);
                }else{
                    holder.categoryName.setTextColor(mContext.getResources().getColor(R.color.colorBlack));
                    //bind category name here
//                    EventDetailActivity.selectedCategory = "";
                    holder.iv_checkBox.setVisibility(View.GONE);
                }

            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return rewardsLists.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public CheckBox categoryName;
        ImageView iv_checkBox;

        public ViewHolder(View v) {
            super(v);
            categoryName = (CheckBox) v.findViewById(R.id.tv_categoryName);
            iv_checkBox = (ImageView) v.findViewById(R.id.iv_check);

        }
    }



}
