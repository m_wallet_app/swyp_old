package com.indy.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.indy.views.fragments.rewards.AvailableRewardsItemFragment;

import java.util.List;

/**
 * Created by hadi on 10/27/16.
 */
public class RewardsListItemPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    List<AvailableRewardsItemFragment> mList;
    public RewardsListItemPagerAdapter(FragmentManager fm, List<AvailableRewardsItemFragment> list) {
        super(fm);
        mList = list;
        this.mNumOfTabs = mList.size();
    }


    @Override
    public Fragment getItem(int position) {

        return mList.get(position);
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }


}

