package com.indy.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.controls.OnstoreClickInterface;
import com.indy.models.benefits.BenefitsSummary;
import com.indy.models.bundles.BundleList;
import com.indy.utils.SharedPrefrencesManger;
import com.indy.views.activites.SwpeMainActivity;

import java.util.List;

public class BenefitsAdapter extends RecyclerView.Adapter<BenefitsAdapter.ViewHolder> {
    private List<BenefitsSummary> mBenefitsSummaryLists;
    private Context mContext;
    private BundlesListAdapter.ViewHolder holder;
    public BenefitsSummary benefitsItem;
    private OnstoreClickInterface mOnstoreClickInterface;
    private SharedPrefrencesManger mSharedPrefrencesManger;
    private SwpeMainActivity swpeMainActivity;

    public BenefitsAdapter(List<BenefitsSummary> usagePackageList, Context context, SwpeMainActivity swpeMainActivity) {
        mBenefitsSummaryLists = usagePackageList;
        this.mContext = context;
        this.mSharedPrefrencesManger = new SharedPrefrencesManger(this.mContext);
        this.swpeMainActivity = swpeMainActivity;
    }


    @Override
    public BenefitsAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.item_benefits, viewGroup, false);

        return new BenefitsAdapter.ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final BenefitsAdapter.ViewHolder holder, int position) {
        benefitsItem = mBenefitsSummaryLists.get(position);
        holder.tv_name.setText(benefitsItem.getTitle());
        holder.tv_desc.setText(benefitsItem.getDescription());

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mBenefitsSummaryLists.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_name, tv_desc;

        public ViewHolder(View v) {
            super(v);
            tv_name = (TextView) v.findViewById(R.id.title_1);
            tv_desc = (TextView) v.findViewById(R.id.description_1);
        }
    }

    public void setData(List<BenefitsSummary> mBenefitsSummaryLists){
        this.mBenefitsSummaryLists = mBenefitsSummaryLists;
        this.notifyDataSetChanged();
    }
}
