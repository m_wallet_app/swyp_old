package com.indy.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.controls.OnstoreClickInterface;
import com.indy.models.bundles.BundleList;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.utils.SharedPrefrencesManger;
import com.indy.views.activites.SwpeMainActivity;
import com.indy.views.fragments.store.mainstore.MyStoreFragment;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by emad on 8/11/16.
 */
public class BundlesListAdapter extends RecyclerView.Adapter<BundlesListAdapter.ViewHolder> {
    private List<BundleList> mUsagePackageLists;
    private Context mContext;
    private ViewHolder holder;
    public static BundleList usagePackageItem;
    private OnstoreClickInterface mOnstoreClickInterface;
    private SharedPrefrencesManger mSharedPrefrencesManger;
    private SwpeMainActivity swpeMainActivity;
    private boolean isNewMembershipSubscribed;

    public BundlesListAdapter(List<BundleList> usagePackageList, Context context, OnstoreClickInterface onstoreClickInterface, SwpeMainActivity swpeMainActivity, boolean isNewMembershipSubscribed) {
        mUsagePackageLists = usagePackageList;
        this.mContext = context;
        this.mOnstoreClickInterface = onstoreClickInterface;
        this.mSharedPrefrencesManger = new SharedPrefrencesManger(this.mContext);
        this.swpeMainActivity = swpeMainActivity;
        this.isNewMembershipSubscribed = isNewMembershipSubscribed;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.store_data_item, viewGroup, false);

        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        usagePackageItem = mUsagePackageLists.get(position);
        holder.tv_price.setText(mContext.getString(R.string.aed) + " " + usagePackageItem.getPrice());
        //+"\n"+mContext.getString(R.string.excl_5_vat));

        if (mSharedPrefrencesManger.isVATEnabled()) {
            holder.tv_vatText.setVisibility(View.VISIBLE);
        } else {
            holder.tv_vatText.setVisibility(View.GONE);
        }

        if (mSharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)) {
            holder.tv_name.setText(usagePackageItem.getNameEn());
            holder.tv_category.setText(usagePackageItem.getBundleName());
            holder.tv_description.setText(usagePackageItem.getDescriptionEn());
        } else {
            holder.tv_category.setText(usagePackageItem.getBundleNameAr());
            holder.tv_name.setText(usagePackageItem.getNameAr());
            holder.tv_description.setText(usagePackageItem.getDescriptionAr());

        }
        if (usagePackageItem.getSelected())
            holder.tv_add_item.setVisibility(View.VISIBLE);
        else
            holder.tv_add_item.setVisibility(View.GONE);

        if (usagePackageItem.isNewMembershipPack()) {
            holder.bt_add.setText(R.string.upgrade);
            if (isNewMembershipSubscribed) {
                holder.bt_add.setEnabled(false);
                holder.store_card_view.setAlpha(0.2f);
            } else {
                holder.bt_add.setEnabled(true);
                holder.store_card_view.setAlpha(1.0f);
            }
        } else {
            holder.bt_add.setText(R.string.add);
            if (MyStoreFragment.isMembershipValid) {
                holder.bt_add.setAlpha(1.0f);
                holder.bt_add.setEnabled(true);
            } else if (usagePackageItem.isRequiresMembership()) {
                holder.bt_add.setAlpha(0.2f);
                holder.bt_add.setEnabled(false);
            } else {
                holder.bt_add.setAlpha(1.0f);
                holder.bt_add.setEnabled(true);
            }
        }
        if (usagePackageItem.getPackagePurchaseRewardModel() != null) {
            //make ll or tv visible
            //set icon
            //set values
            holder.ll_reward_raffle.setVisibility(View.VISIBLE);
            try {
                if (mSharedPrefrencesManger.getLanguage().equalsIgnoreCase(ConstantUtils.lang_english)) {
                    holder.tv_rewardText.setText(usagePackageItem.getPackagePurchaseRewardModel().getRewardDescriptionEn());
                } else {
                    holder.tv_rewardText.setText(usagePackageItem.getPackagePurchaseRewardModel().getRewardDescriptionAr());

                }

                Picasso.with(mContext)
                        .load(usagePackageItem.getPackagePurchaseRewardModel().getRewardTypeImage())
                        .into(holder.iv_rewardIcon);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            holder.ll_reward_raffle.setVisibility(View.GONE);

            //make ll or tv invisile
        }

        holder.tv_shoremore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.tv_description.getVisibility() == View.GONE) {
                    holder.tv_description.setVisibility(View.VISIBLE);
                    holder.tv_shoremore.setText(mContext.getString(R.string.show_less));
                    CommonMethods.logFirebaseEvent(swpeMainActivity.getmFirebaseAnalytics(), ConstantUtils.TAGGING_store_show_more);


                } else {
                    holder.tv_description.setVisibility(View.GONE);
                    holder.tv_shoremore.setText(mContext.getString(R.string.show_more));
                    CommonMethods.logFirebaseEvent(swpeMainActivity.getmFirebaseAnalytics(), ConstantUtils.TAGGING_store_transfer_button);

                }
            }
        });
        holder.bt_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                usagePackageItem = mUsagePackageLists.get(position);
                mOnstoreClickInterface.onItemClick(mUsagePackageLists.get(holder.getAdapterPosition()), holder.getAdapterPosition());
//                usagePackageItem.setSelected(true);
//                notifyDataSetChanged();
            }
        });

        if (usagePackageItem.getCount() > 0) {
            holder.tv_count.setText(mContext.getString(R.string.added) + " " + usagePackageItem.getCount());
        } else {
            holder.tv_count.setText("");
        }


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mUsagePackageLists.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_name, tv_price, tv_category, tv_shoremore, tv_description, tv_add_item, tv_count, tv_rewardText, tv_vatText;
        private ImageView iv_rewardIcon;
        private LinearLayout store_card_view;
        Button bt_add;
        private LinearLayout ll_reward_raffle;

        public ViewHolder(View v) {
            super(v);
            tv_category = (TextView) v.findViewById(R.id.tv_category);
            tv_price = (TextView) v.findViewById(R.id.tv_price);
            tv_name = (TextView) v.findViewById(R.id.tv_name);
            tv_shoremore = (TextView) v.findViewById(R.id.tv_shoremore);
            tv_description = (TextView) v.findViewById(R.id.tv_description);
            store_card_view = (LinearLayout) v;
            tv_add_item = (TextView) v.findViewById(R.id.tv_add_item);
            bt_add = (Button) v.findViewById(R.id.bt_add);
            tv_count = (TextView) v.findViewById(R.id.tv_count);
            tv_vatText = (TextView) v.findViewById(R.id.tv_vat_text);
            tv_rewardText = (TextView) v.findViewById(R.id.tv_raffleText);
            iv_rewardIcon = (ImageView) v.findViewById(R.id.iv_reward_image);
            ll_reward_raffle = (LinearLayout) v.findViewById(R.id.ll_reward_raffle);
            //  data packages
        }
    }

    public void updateItem(BundleList outPut, int position) {
        mUsagePackageLists.set(position, outPut);
        notifyDataSetChanged();
    }

    public String getId(int position) {
        return mUsagePackageLists.get(position).getId();
    }

}
