package com.indy.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.indy.models.packages.OfferList;
import com.indy.models.packages.UsagePackageList;
import com.indy.models.packageusage.PackageUsageOutput;
import com.indy.models.packageusage.UsageGraphList;
import com.indy.views.fragments.usage.ChartsUsages.MixPackage_DataFragment;
import com.indy.views.fragments.usage.ChartsUsages.MixPackage_SocialFragment;

import java.util.ArrayList;

/**
 * Created by hadi on 11/10/16.
 */
public class GraphPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    PackageUsageOutput packageUsageOutput;
    ArrayList<OfferList> offerList;
    UsagePackageList usagePackageList;

    public GraphPagerAdapter(FragmentManager fm, int NumOfTabs, PackageUsageOutput packageUsageOutput, ArrayList<OfferList> offerList, UsagePackageList usagePackageList) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.packageUsageOutput = packageUsageOutput;
        this.offerList = offerList;
        this.usagePackageList = usagePackageList;
    }


    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                MixPackage_DataFragment mixPackage_dataFragment = new MixPackage_DataFragment();
                Bundle bn = new Bundle();
                if (packageUsageOutput.getUsageGraphList() != null && packageUsageOutput.getUsageGraphList().size()>0 && packageUsageOutput.getUsageGraphList().get(0) != null) {
                    UsageGraphList tempList = (UsageGraphList) packageUsageOutput.getUsageGraphList().get(0);

                    bn.putParcelable("DataGraphList", tempList);
                }

                bn.putString("startDate", usagePackageList.getOfferList().get(0).getInOfferStartDate());
                bn.putString("endDate", usagePackageList.getOfferList().get(0).getInOfferEndDate());
                mixPackage_dataFragment.setArguments(bn);
                return mixPackage_dataFragment;

            case 1:
                MixPackage_SocialFragment mixPackage_socialFragment = new MixPackage_SocialFragment();
                Bundle bn_social = new Bundle();
                if (packageUsageOutput.getUsageGraphList() != null && packageUsageOutput.getUsageGraphList().size()>1&& packageUsageOutput.getUsageGraphList().get(1) != null) {
                    UsageGraphList tempList = (UsageGraphList) packageUsageOutput.getUsageGraphList().get(1);

                    bn_social.putParcelable("DataGraphList", tempList);

                }
                bn_social.putString("startDate", usagePackageList.getOfferList().get(1).getInOfferStartDate());
                bn_social.putString("endDate", usagePackageList.getOfferList().get(1).getInOfferEndDate());
                mixPackage_socialFragment.setArguments(bn_social);
                return mixPackage_socialFragment;
            default:
                return new MixPackage_DataFragment();

        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

}

