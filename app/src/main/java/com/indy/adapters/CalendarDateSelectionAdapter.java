package com.indy.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.indy.R;
import com.indy.controls.OnItemClickListener;

import java.util.List;

/**
 * Created by hadi.mehmood on 10/9/2016.
 */
public class CalendarDateSelectionAdapter extends RecyclerView.Adapter<CalendarDateSelectionAdapter.ViewHolder> {

    List<String> mItems;
    Context mContext;
    static OnItemClickListener onItemClickListener;
    public CalendarDateSelectionAdapter(Context context, List<String> items){
        this.mContext = context;
        this.mItems = items;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater mLayoutInflater = LayoutInflater.from(parent.getContext());
        final View sView = mLayoutInflater.inflate(R.layout.item_calendar_selection_adapter,parent,false);
        return new ViewHolder(sView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.title.setText(mItems.get(position));
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView title;
    public ViewHolder(View itemView) {
        super(itemView);
        title = (TextView) itemView.findViewById(R.id.tv_title);
        itemView.setOnClickListener(this);
    }

        @Override
        public void onClick(View view) {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(view, getPosition());
            }
        }
    }
    public void setOnItemClickListener(OnItemClickListener mItemClickListener) {
        this.onItemClickListener = mItemClickListener;
    }
}
