package com.indy.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by emad on 8/28/16.
 */
public class IntroFragmentAdapter extends FragmentPagerAdapter {
    //    private String[] titles;
    private ArrayList<Fragment> fragments;

    public IntroFragmentAdapter(Context context, FragmentManager fragmentManager,
                                ArrayList<Fragment> fragments, int arrayResource) {
        super(fragmentManager);
//        titles = context.getResources().getStringArray(
//                arrayResource);
        this.fragments = fragments;
    }

    public int getCount() {
        return fragments.size();
    }

    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return/* titles[position]*/null;
    }

}
