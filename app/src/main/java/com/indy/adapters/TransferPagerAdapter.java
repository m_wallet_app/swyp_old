package com.indy.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.indy.views.fragments.store.request.AlertsFragment;
import com.indy.views.fragments.store.request.RequestFragment;
import com.indy.views.fragments.store.request.TransferFragment;

/**
 * Created by hadi on 11/10/16.
 */
public class TransferPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public TransferPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }


    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                RequestFragment requestFragment= new RequestFragment();
                return requestFragment;
            case 1:
                TransferFragment requestFragment2 = new TransferFragment();
                return requestFragment2;
            case 2:
                AlertsFragment alertsFragment = new AlertsFragment();
                return alertsFragment;
            default:
                return new RequestFragment();

        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }


}

