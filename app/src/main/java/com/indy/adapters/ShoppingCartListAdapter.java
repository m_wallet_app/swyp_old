package com.indy.adapters;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.controls.OnContactsListItemClickListener;
import com.indy.controls.OnPrefferredNumberFocusChangeListener;
import com.indy.controls.OnStoreDeleteItemClickListener;
import com.indy.controls.OnstoreClickInterface;
import com.indy.models.bundles.BundleList;
import com.indy.utils.SharedPrefrencesManger;

import java.util.List;

/**
 * Created by hadi on 10/20/16.
 */
public class ShoppingCartListAdapter extends RecyclerView.Adapter<ShoppingCartListAdapter.ViewHolder> {
    private List<BundleList> mUsagePackageLists;
    private Context mContext;
    private ViewHolder holder;
    public static BundleList usagePackageItem;
    private OnstoreClickInterface mOnstoreClickInterface;
    private OnStoreDeleteItemClickListener onStoreDeleteItemClickListener;
    private OnContactsListItemClickListener onContactsListItemClickListener;
    private OnPrefferredNumberFocusChangeListener onPrefferredNumberFocusChangeListener;
    SharedPrefrencesManger sharedPrefrencesManger;

    public ShoppingCartListAdapter(List<BundleList> usagePackageList, Context context,
                                   OnstoreClickInterface onstoreClickInterface,
                                   OnStoreDeleteItemClickListener onStoreDeleteItemClickListener,
                                   OnContactsListItemClickListener onContactsClickListener,
                                   OnPrefferredNumberFocusChangeListener onFocusChangeListener) {
        mUsagePackageLists = usagePackageList;
        this.mContext = context;
        this.mOnstoreClickInterface = onstoreClickInterface;
        this.onStoreDeleteItemClickListener = onStoreDeleteItemClickListener;
        onContactsListItemClickListener = onContactsClickListener;
        onPrefferredNumberFocusChangeListener = onFocusChangeListener;
        this.sharedPrefrencesManger = new SharedPrefrencesManger(this.mContext);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.shopping_cart_item, viewGroup, false);

        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        try {
            usagePackageItem = mUsagePackageLists.get(position);
            if (usagePackageItem.getCount() > 0) {
                holder.tv_price.setText(mContext.getString(R.string.aed) + " " + usagePackageItem.getPrice());


                if (sharedPrefrencesManger != null && sharedPrefrencesManger.getLanguage().equals("en")) {
                    holder.tv_name.setText(usagePackageItem.getNameEn());
                    holder.tv_category.setText(usagePackageItem.getBundleName());
                } else {

                    holder.tv_name.setText(usagePackageItem.getNameAr());
                    holder.tv_category.setText(usagePackageItem.getBundleNameAr());
                }
                holder.tv_validity.setText(mContext.getString(R.string.valid_for) + " " + usagePackageItem.getValidtyInDays() + " " + mContext.getString(R.string.days));
                holder.bt_remove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            onStoreDeleteItemClickListener.onDeleteStoreItem(mUsagePackageLists.get(holder.getAdapterPosition()), holder.getAdapterPosition());
                        } catch (Exception ex) {

                        }
                    }
                });

            }
            if(usagePackageItem.getValidtyInDays()!=null && usagePackageItem.getValidtyInDays().length()>5){
                holder.tv_validity.setText(usagePackageItem.getValidtyInDays());
            }else {
                holder.tv_validity.setText(mContext.getString(R.string.valid_for) + " " + usagePackageItem.getValidtyInDays() + " " + mContext.getString(R.string.days));
            }
            holder.bt_remove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onStoreDeleteItemClickListener.onDeleteStoreItem(mUsagePackageLists.get(holder.getAdapterPosition()), holder.getAdapterPosition());
                }
            });

                if (usagePackageItem.getCount() >= usagePackageItem.getMaxCount()) {

                    holder.bt_add.setAlpha(0.4f);
                    holder.bt_add.setEnabled(false);
                } else {
                    holder.bt_add.setAlpha(1.0f);
                    holder.bt_add.setEnabled(true);
                    holder.bt_add.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
//                usagePackageItem = mUsagePackageLists.get(position);

                            mOnstoreClickInterface.onItemClick(mUsagePackageLists.get(holder.getAdapterPosition()), holder.getAdapterPosition());
//                usagePackageItem.setSelected(true);
//                notifyDataSetChanged();

                    }
                });
            }

            if(!usagePackageItem.isAddSubtractEnabled()){
                holder.bt_add.setVisibility(View.INVISIBLE);
                holder.bt_remove.setVisibility(View.INVISIBLE);
            }
            holder.et_number.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean hasFocus) {
                    if (!hasFocus) {
                        String numberString = holder.et_number.getText().toString().trim();
                        if (numberString.length() != 10 || !numberString.startsWith("05")) {
                            holder.til_mobile.setError(mContext.getString(R.string.invalid_delivery_number));
                        } else {
                            holder.til_mobile.setError(null);
                            onPrefferredNumberFocusChangeListener.onFocusChanged(position, numberString);
                        }
                    }
                }
            });
            if (usagePackageItem.getCount() > 0) {
                holder.tv_count.setText(mContext.getString(R.string.added) + " " + usagePackageItem.getCount());
            } else {
                holder.tv_count.setText("");
            }
//        } else {
//            holder.itemView.setVisibility(View.GONE);
//        }

                holder.et_number.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean hasFocus) {
                        if (!hasFocus) {
                            String numberString = holder.et_number.getText().toString().trim();
                            if (numberString.length() != 10 || !numberString.startsWith("05")) {
                                holder.til_mobile.setError(mContext.getString(R.string.invalid_delivery_number));
                            } else {
                                holder.til_mobile.setError(null);
                                onPrefferredNumberFocusChangeListener.onFocusChanged(position, numberString);
                            }
                        }
                    }
                });
                if (usagePackageItem.getCount() > 0) {
                    holder.tv_count.setText(mContext.getString(R.string.added) + " " + usagePackageItem.getCount());
                } else {
                    holder.tv_count.setText("");
                }
//            } else {
//                holder.itemView.setVisibility(View.GONE);
//            }

            if (usagePackageItem != null && usagePackageItem.getRatePlanType() != null && usagePackageItem.getRatePlanType().equals("PREFERED")) {
                holder.rl_contact.setVisibility(View.VISIBLE);
//            mUsagePackageLists.get(position).setEt_preferredNumber(holder.et_number);
                holder.et_number.setText(usagePackageItem.getPreferredNumber());
//            holder.iv_contacts.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    onContactsListItemClickListener.onContactsClick(mUsagePackageLists.get(holder.getAdapterPosition()), holder.getAdapterPosition());
//                }
//            });

                holder.et_number.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {

                        final int DRAWABLE_END_ENGLISH = 2;
                        final int DRAWABLE_END_ARABIC = 0;
                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            if (sharedPrefrencesManger.getLanguage().equals("en")) {
                                if (event.getRawX() >= (holder.et_number.getRight() - holder.et_number.getCompoundDrawables()[DRAWABLE_END_ENGLISH].getBounds().width())) {
                                    // your action here
//                                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
//                                startActivityForResult(intent, 1);
                                    onContactsListItemClickListener.onContactsClick(mUsagePackageLists.get(holder.getAdapterPosition()), holder.getAdapterPosition());

                                    return true;
                                }
                            } else {
                                if (event.getX() <= (holder.et_number.getCompoundDrawables()[DRAWABLE_END_ARABIC].getBounds().width())) {
                                    // your action here
//                                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
//                                startActivityForResult(intent, 1);
                                    onContactsListItemClickListener.onContactsClick(mUsagePackageLists.get(holder.getAdapterPosition()), holder.getAdapterPosition());
                                    return true;
                                }
                            }
                        }
                        return false;
                    }
                });
                holder.bt_add.setEnabled(false);
                holder.bt_add.setAlpha(0.3f);
            } else {
                holder.rl_contact.setVisibility(View.GONE);
            }
        }catch (Exception ex){

        }

    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mUsagePackageLists.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_name, tv_price, tv_category, tv_description, tv_count, tv_validity;
        private CardView store_card_view;
        ImageView bt_add, bt_remove, iv_contacts;
        RelativeLayout rl_contact;
        TextInputLayout til_mobile;
        EditText et_number;

        public ViewHolder(View v) {
            super(v);
            tv_category = (TextView) v.findViewById(R.id.tv_category);
            tv_price = (TextView) v.findViewById(R.id.tv_price);
            tv_name = (TextView) v.findViewById(R.id.tv_name);
            et_number = (EditText) v.findViewById(R.id.mobile_number);
            tv_description = (TextView) v.findViewById(R.id.tv_description);
//            store_card_view = (CardView) v.findViewById(R.id.store_card_view);
            bt_add = (ImageView) v.findViewById(R.id.add_item);
            bt_remove = (ImageView) v.findViewById(R.id.remove_item);
            tv_count = (TextView) v.findViewById(R.id.tv_count);
            iv_contacts = (ImageView) v.findViewById(R.id.iv_contacts);
            rl_contact = (RelativeLayout) v.findViewById(R.id.rl_chooseNumber);
            tv_validity = (TextView) v.findViewById(R.id.tv_validity);
            til_mobile = (TextInputLayout) v.findViewById(R.id.mobileNoInputLayout);
            //  data packages
        }
    }

    public void updateItem(BundleList outPut, int position) {
        mUsagePackageLists.set(position, outPut);
        notifyDataSetChanged();
    }


    public void setTextInputError(String error) {

    }

    public String getId(int position) {
        return mUsagePackageLists.get(position).getId();
    }

}
