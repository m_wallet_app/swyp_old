package com.indy.adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.indy.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mobile on 23/07/2017.
 */

public class GenderSpinnerAdapter extends ArrayAdapter<String> {

    List<String> mList;
    public GenderSpinnerAdapter(@NonNull Context context, @LayoutRes int resource) {
        super(context,resource);
        mList = new ArrayList<String>();
        mList.add(context.getString(R.string.male));
        mList.add(context.getString(R.string.female));
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return getDDView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        return getCustomView(position, convertView, parent);
    }


    public View getCustomView(int position, View convertView, ViewGroup parent) {
        //return super.getView(position, convertView, parent);

        LayoutInflater inflater = LayoutInflater.from(getContext());
        View row = inflater.inflate(R.layout.item_gender, parent, false);
        TextView label = (TextView) row.findViewById(R.id.tv_gender);

        label.setText(mList.get(position));
        return row;

    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Nullable
    @Override
    public String getItem(int position) {
        return mList.get(position);
    }

    public View getDDView(int position, View convertView, ViewGroup parent) {
        //return super.getView(position, convertView, parent);

        LayoutInflater inflater = LayoutInflater.from(getContext());
        View row = inflater.inflate(R.layout.item_spinner, parent, false);
        TextView label = (TextView) row.findViewById(R.id.tv_gender);
        label.setText(mList.get(position));
        return row;

    }


}
