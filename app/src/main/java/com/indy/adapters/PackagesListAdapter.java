package com.indy.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.indy.R;
import com.indy.controls.ServiceUtils;
import com.indy.helpers.DateHelpers;
import com.indy.models.packages.NewMemberShipPack;
import com.indy.models.packages.OfferList;
import com.indy.models.packages.PackageConsumption;
import com.indy.new_membership.NewMembershipChartListActivity;
import com.indy.squad_number.models.SquadNumber;
import com.indy.models.packages.UsagePackageList;
import com.indy.new_membership.NewMembershipChartActivity;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.utils.SharedPrefrencesManger;
import com.indy.views.activites.MixedDataPackageGraphPagerActivity;
import com.indy.views.activites.SwpeMainActivity;
import com.indy.views.activites.UnlimitedMinutesPackageActivity;
import com.indy.views.fragments.BenefitsFragment;
import com.indy.views.fragments.NewMembershipBenefitsFragment;
import com.indy.views.fragments.NewMembershipUpgradeBenefitsFragment;
import com.indy.views.fragments.esim.ActivateYourESIMFragment;
import com.indy.views.fragments.usage.ChartsUsages.DataPackageChartActivity;
import com.indy.views.fragments.usage.PackagesFragments;
import com.indy.views.fragments.usage.UsageFragment;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by emad on 8/11/16.
 */
public class PackagesListAdapter extends RecyclerView.Adapter<PackagesListAdapter.ViewHolder> {
    private List<UsagePackageList> mUsagePackageLists;
    private static Context mContext;
    private ViewHolder holder;
    public static UsagePackageList usagePackageItem;
    public List<UsagePackageList> mNewMemberPackageList;
    public NewMemberShipPack mNewMemberPack;

    public ArrayList<OfferList> offerList;
    public static DateHelpers dateUtils;
    private final int mixPackageType = 1, dataPackageType = 3, unlimiteddataType = 2, newMembershipType = 4, blackBoxType = -2, eSIMBoxType = -3, newMembershipBoxType = -4;
    private boolean isESIMVisible = false;
    private boolean isNewMembershipVisible = false;
    public SharedPrefrencesManger mSharedPrefrencesManger;
    SwpeMainActivity mSwpeMainActivity;

    public PackagesListAdapter(List<UsagePackageList> usagePackageList, NewMemberShipPack newMemberPackageList, Context context, SharedPrefrencesManger sharedPrefrencesManger, SwpeMainActivity swpeMainActivity, boolean isESIMVisible, boolean isNewMembershipVisible) {
        this.mUsagePackageLists = usagePackageList;
        this.mNewMemberPack = newMemberPackageList;
        if (this.mNewMemberPack != null && this.mNewMemberPack.getUsagePackageList() != null)
            this.mNewMemberPackageList = newMemberPackageList.getUsagePackageList();
        this.mContext = context;
        this.isESIMVisible = isESIMVisible;
        this.isNewMembershipVisible = isNewMembershipVisible;
        this.mSharedPrefrencesManger = sharedPrefrencesManger;
        this.mSwpeMainActivity = swpeMainActivity;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.package_item, viewGroup, false);
        dateUtils = new DateHelpers();
        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        try {
            usagePackageItem = mUsagePackageLists.get(position);
            offerList = (ArrayList<OfferList>) mUsagePackageLists.get(position).getOfferList();

            this.holder = holder;
            holder.ll_getMembership.setVisibility(View.GONE);
            holder.ll_activate_esim.setVisibility(View.GONE);
            holder.ll_new_membership.setVisibility(View.GONE);
            if (!usagePackageItem.getId().equalsIgnoreCase("-1")) {
                switch (usagePackageItem.getPackageType()) {
                    case mixPackageType:
                        usagePackageItem = mUsagePackageLists.get(position);
                        onMixedPackageTypes(position);

                        break;
                    case dataPackageType:
                    case unlimiteddataType:
                        usagePackageItem = mUsagePackageLists.get(position);
                        onDataPackageType(position);

                        break;
//                case unlimiteddataType:
//                    usagePackageItem = mUsagePackageLists.get(position);
//                    oNunLimtedMinutesType(position);
//                    break;

                    case newMembershipType:
                        if (position == getBoxPosition(newMembershipType)) {
                            onNewMembershipPackageType(position);
                            return;
                        }

                        break;
                    case blackBoxType:
//                        if ((isESIMVisible && position == 2) || (!isESIMVisible && position == 1)) {
                        if (position == getBoxPosition(blackBoxType)) {
                            holder.addPackageCardView.setVisibility(View.GONE);
                            holder.dataCardview.setVisibility(View.GONE);
                            holder.mixedCardView.setVisibility(View.GONE);
                            holder.unlimitedCardView.setVisibility(View.GONE);
                            holder.newMshipCardview.setVisibility(View.GONE);
                            holder.ll_activate_esim.setVisibility(View.GONE);
                            holder.ll_getMembership.setVisibility(View.VISIBLE);
                            holder.ll_new_membership.setVisibility(View.GONE);
                            if (this.mSharedPrefrencesManger.isVATEnabled()) {
                                Double price = Double.parseDouble(this.mSharedPrefrencesManger.getAfterLoginPrice());
                                holder.tv_overlay.setText(mContext.getResources().getString(R.string.you_dont_have_a_swyp_membership_without_next_line));

                            } else {
                                holder.tv_overlay.setText(mContext.getResources().getString(R.string.you_dont_have_a_swyp_membership_without_next_line));
                            }
                            holder.renewNow.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    mSwpeMainActivity.replaceFragmnet(new NewMembershipUpgradeBenefitsFragment(), R.id.contentFrameLayout, true);
                                }
                            });
                            return;
                        }

                        break;
                    case eSIMBoxType:
//                        if ((isNewMembershipVisible && position == 1) || (!isNewMembershipVisible && position == 0)) {
                        if (position == getBoxPosition(eSIMBoxType)) {
                            holder.addPackageCardView.setVisibility(View.GONE);
                            holder.dataCardview.setVisibility(View.GONE);
                            holder.mixedCardView.setVisibility(View.GONE);
                            holder.unlimitedCardView.setVisibility(View.GONE);
                            holder.newMshipCardview.setVisibility(View.GONE);
                            holder.ll_activate_esim.setVisibility(View.VISIBLE);
                            holder.ll_new_membership.setVisibility(View.GONE);
                            holder.bt_esim_overlay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    mSwpeMainActivity.replaceFragmnet(new ActivateYourESIMFragment(), R.id.contentFrameLayout, true);
                                }
                            });
                            return;
                        }

                        break;
                    case newMembershipBoxType:
                        if (position == getBoxPosition(newMembershipBoxType)) {
                            holder.addPackageCardView.setVisibility(View.GONE);
                            holder.dataCardview.setVisibility(View.GONE);
                            holder.mixedCardView.setVisibility(View.GONE);
                            holder.unlimitedCardView.setVisibility(View.GONE);
                            holder.newMshipCardview.setVisibility(View.GONE);
                            holder.ll_activate_esim.setVisibility(View.GONE);
                            holder.ll_new_membership.setVisibility(View.GONE);
                            holder.bt_new_mship.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    mSwpeMainActivity.replaceFragmnet(new NewMembershipUpgradeBenefitsFragment(), R.id.contentFrameLayout, true);
                                }
                            });
                            return;
                        }

                        break;
                }
            }

            if (usagePackageItem.getPackageDisabled()) {
                holder.itemView.setEnabled(false);

//                holder.itemView.setAlpha(0.4f);
            } else {
                holder.itemView.setEnabled(true);
//                holder.itemView.setAlpha(1.0f);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void onAddNewPackageItem() {
        holder.addPackageCardView.setVisibility(View.GONE);
        holder.dataCardview.setVisibility(View.GONE);
        holder.mixedCardView.setVisibility(View.GONE);
        holder.unlimitedCardView.setVisibility(View.GONE);
        holder.addPackageCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, "Add new package", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void onNewMembershipPackageType(final int position) {

//        Set the value to this item

        Log.v("current_lang_date", mSharedPrefrencesManger.getLanguage() + " ");

        holder.addPackageCardView.setVisibility(View.GONE);
        holder.dataCardview.setVisibility(View.GONE);
        holder.mixedCardView.setVisibility(View.GONE);
        holder.unlimitedCardView.setVisibility(View.GONE);
        holder.newMshipCardview.setVisibility(View.VISIBLE);

        final UsagePackageList socialUsagePackageList = mNewMemberPackageList.get(0);
        final PackageConsumption socialConsumption = socialUsagePackageList.getOfferList().get(0).getPackageConsumption();
        if (daysDifferenceLong(mNewMemberPackageList.get(0).getEndDate()) <= 365) {
            holder.new_mship_expires_id.setVisibility(View.VISIBLE);
            holder.new_mship_expires_id.setText(daysDifference(mNewMemberPackageList.get(0).getEndDate()));
        } else {
            holder.new_mship_expires_id.setVisibility(View.INVISIBLE);
        }

        //For Social Data

        if (mSharedPrefrencesManger.getLanguage().equalsIgnoreCase("en")) {
//            Log.v("ooooops_en", usagePackageItem.getDescriptionEn());
            holder.new_mship_social_package_value_ID.setText(socialUsagePackageList.getDisplayLabel());
            if (socialConsumption.getRemaining() <= 100000) {
                holder.addone_Value_desc_ID.setVisibility(View.VISIBLE);
                holder.new_mship_social_package_value_ID.setVisibility(View.VISIBLE);
//                holder.new_mship_social_package_value_ID.setText(socialConsumption.getRemaining() + "" + socialConsumption.getConsumptionUnitEn());
            } else {
                holder.addone_Value_desc_ID.setVisibility(View.INVISIBLE);
            }
        } else {
            holder.new_mship_social_package_value_ID.setText(socialUsagePackageList.getDisplayLabel());
//            Log.v("ooooops_ar", usagePackageItem.getDescriptionAr());
            if (socialConsumption.getRemaining() <= 100000) {
                holder.addone_Value_desc_ID.setVisibility(View.VISIBLE);
                holder.new_mship_social_package_value_ID.setVisibility(View.VISIBLE);
//                holder.new_mship_social_package_value_ID.setText(socialConsumption.getRemaining() + "" + socialConsumption.getConsumptionUnitAr());
            } else {
                holder.addone_Value_desc_ID.setVisibility(View.GONE);
            }
        }

        //For General Data

        if (mNewMemberPackageList.size() > 1) {
            final UsagePackageList generalUsagePackageList = mNewMemberPackageList.get(1);
            final PackageConsumption generalConsumption = generalUsagePackageList.getOfferList().get(0).getPackageConsumption();

            if (mSharedPrefrencesManger.getLanguage().equalsIgnoreCase("en")) {
//            Log.v("ooooops_en", usagePackageItem.getDescriptionEn());

                holder.new_mship_general_package_value_ID.setText(generalUsagePackageList.getDisplayLabel());
                if (generalConsumption.getRemaining() <= 100000) {
                    holder.addone_Value_desc_ID.setVisibility(View.VISIBLE);
                    holder.new_mship_general_package_value_ID.setVisibility(View.VISIBLE);
//                    holder.new_mship_general_package_value_ID.setText(generalConsumption.getRemaining() + "" + generalConsumption.getConsumptionUnitEn());
                } else {
                    holder.addone_Value_desc_ID.setVisibility(View.INVISIBLE);
                }
            } else {
//            Log.v("ooooops_ar", usagePackageItem.getDescriptionAr());
                holder.new_mship_general_package_value_ID.setText(generalUsagePackageList.getDisplayLabel());
                if (generalConsumption.getRemaining() <= 100000) {
                    holder.addone_Value_desc_ID.setVisibility(View.VISIBLE);
                    holder.new_mship_general_package_value_ID.setVisibility(View.VISIBLE);
//                    holder.new_mship_general_package_value_ID.setText(generalConsumption.getRemaining() + "" + generalConsumption.getConsumptionUnitAr());
                } else {
                    holder.addone_Value_desc_ID.setVisibility(View.GONE);
                }
            }
        }

        if (mNewMemberPackageList.size() > 2) {
            final UsagePackageList flexiUsagePackageList = mNewMemberPackageList.get(2);
            final PackageConsumption flexiConsumption = flexiUsagePackageList.getOfferList().get(0).getPackageConsumption();

            if (mSharedPrefrencesManger.getLanguage().equalsIgnoreCase("en")) {
//            Log.v("ooooops_en", usagePackageItem.getDescriptionEn());

                holder.new_mship_flexi_min_value_ID.setText(flexiUsagePackageList.getDisplayLabel());
                if (flexiConsumption.getRemaining() <= 100000) {
                    holder.addone_Value_desc_ID.setVisibility(View.VISIBLE);
                    holder.new_mship_general_package_value_ID.setVisibility(View.VISIBLE);
//                    holder.new_mship_general_package_value_ID.setText(generalConsumption.getRemaining() + "" + generalConsumption.getConsumptionUnitEn());
                } else {
                    holder.addone_Value_desc_ID.setVisibility(View.INVISIBLE);
                }
            } else {
//            Log.v("ooooops_ar", usagePackageItem.getDescriptionAr());
                holder.new_mship_general_package_value_ID.setText(flexiUsagePackageList.getDisplayLabel());
                if (flexiConsumption.getRemaining() <= 100000) {
                    holder.addone_Value_desc_ID.setVisibility(View.VISIBLE);
                    holder.new_mship_general_package_value_ID.setVisibility(View.VISIBLE);
//                    holder.new_mship_general_package_value_ID.setText(generalConsumption.getRemaining() + "" + generalConsumption.getConsumptionUnitAr());
                } else {
                    holder.addone_Value_desc_ID.setVisibility(View.GONE);
                }
            }
        }

        SquadNumber squadNumber = mNewMemberPack.getSquadNumber();

        if (squadNumber != null && squadNumber.getSquad() != null) {
            holder.new_mship_squad_number_ID.setText(squadNumber.getSquad().size() + "/" + squadNumber.getMaxSquadNumber());
        } else {
            holder.new_mship_squad_number_ID.setText("0/0");
        }
        if (usagePackageItem.getIconUrl() != null)
            Picasso.with(mContext).load(usagePackageItem.getIconUrl()).into(holder.dataImg);
        holder.newMshipCardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usagePackageItem = mUsagePackageLists.get(position);

//                Bundle bundle2 = new Bundle();
//
//                bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_usage_detail);
//                bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_usage_detail);
                CommonMethods.logFirebaseEvent(mSwpeMainActivity.getmFirebaseAnalytics(), ConstantUtils.TAGGING_usage_detail);

//                Intent intent = new Intent(mContext, LineChartActivity.class);
                if (/*!usagePackageItem.getPackageDisabled() && !(packageConsumption.getRemaining() > 100000) &&*/ !PackagesFragments.packagesDisabled) {
                    Intent intent = new Intent(mContext, NewMembershipChartListActivity.class);
                    offerList = (ArrayList<OfferList>) mUsagePackageLists.get(position).getOfferList();
                    intent.putExtra("newMemberShipPack", mNewMemberPack);
                    if (mNewMemberPack.getUsagePackageList() != null) {
                        if (mNewMemberPack.getUsagePackageList().get(0) != null)
                            intent.putExtra("socialDataItem", mNewMemberPack.getUsagePackageList().get(0));
                        if (mNewMemberPack.getUsagePackageList().size() > 1 && mNewMemberPack.getUsagePackageList().get(1) != null)
                            intent.putExtra("generalDataItem", mNewMemberPack.getUsagePackageList().get(1));
                    }
                    intent.putExtra("squad", mNewMemberPack.getSquadNumber());
                    intent.putParcelableArrayListExtra("OfferInfoList", offerList);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    mContext.startActivity(intent);
                }
            }
        });
        Animation anim = AnimationUtils.loadAnimation(mContext, R.anim.out_from_right);
        anim.setFillAfter(true);

        // By default all grid items will animate together and will look like the gridview is
        // animating as a whole. So, experiment with incremental delays as below to get a
        // wave effect.
        int offset = 0;
        offset = position / 2;
        double finalOffset = Math.floor(offset);
        anim.setStartOffset((long) finalOffset * 50);

        if (usagePackageItem.getIsAnimated() == 0) {
            holder.dataCardview.setAnimation(anim);

            anim.start();
            mUsagePackageLists.get(position).setIsAnimated(1);
        }


    }

    private void onDataPackageType(final int position) {
        Log.v("current_lang_date", mSharedPrefrencesManger.getLanguage() + " ");

        holder.addPackageCardView.setVisibility(View.GONE);
        holder.dataCardview.setVisibility(View.VISIBLE);
        holder.mixedCardView.setVisibility(View.GONE);
        holder.unlimitedCardView.setVisibility(View.GONE);
        holder.newMshipCardview.setVisibility(View.GONE);
        final PackageConsumption packageConsumption = usagePackageItem.getOfferList().get(0).getPackageConsumption();
        if (daysDifferenceLong(usagePackageItem.getEndDate()) <= 365) {
            holder.data_expires_Id.setVisibility(View.VISIBLE);
            holder.data_expires_Id.setText(daysDifference(usagePackageItem.getEndDate()));
        } else {
            holder.data_expires_Id.setVisibility(View.INVISIBLE);
        }
        if (mSharedPrefrencesManger.getLanguage().equalsIgnoreCase("en")) {
//            Log.v("ooooops_en", usagePackageItem.getDescriptionEn());
            holder.data_package_value_ID.setText(usagePackageItem.getDisplayLabel());
            holder.data_package_NameID.setText(usagePackageItem.getNameEn());
            if (packageConsumption.getRemaining() <= 100000) {
                holder.addone_Value_desc_ID.setVisibility(View.VISIBLE);
                holder.data_package_value_ID.setVisibility(View.VISIBLE);
            } else {
                holder.addone_Value_desc_ID.setVisibility(View.INVISIBLE);
            }
        } else {
//            Log.v("ooooops_ar", usagePackageItem.getDescriptionAr());
            holder.data_package_NameID.setText(usagePackageItem.getNameAr());
            holder.data_package_value_ID.setText(usagePackageItem.getDisplayLabel());
            if (packageConsumption.getRemaining() <= 100000) {
                holder.addone_Value_desc_ID.setVisibility(View.VISIBLE);
                holder.data_package_value_ID.setVisibility(View.VISIBLE);
            } else {
                holder.addone_Value_desc_ID.setVisibility(View.GONE);
            }
        }


        if (usagePackageItem.getIconUrl() != null)
            Picasso.with(mContext).load(usagePackageItem.getIconUrl()).into(holder.dataImg);
        holder.dataCardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usagePackageItem = mUsagePackageLists.get(position);

//                Bundle bundle2 = new Bundle();
//
//                bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ConstantUtils.TAGGING_usage_detail);
//                bundle2.putString(FirebaseAnalytics.Param.ITEM_ID, ConstantUtils.TAGGING_usage_detail);
                CommonMethods.logFirebaseEvent(mSwpeMainActivity.getmFirebaseAnalytics(), ConstantUtils.TAGGING_usage_detail);

//                Intent intent = new Intent(mContext, LineChartActivity.class);
                if (!usagePackageItem.getPackageDisabled() && !(packageConsumption.getRemaining() > 100000) && !PackagesFragments.packagesDisabled) {
                    Intent intent = new Intent(mContext, DataPackageChartActivity.class);
                    offerList = (ArrayList<OfferList>) mUsagePackageLists.get(position).getOfferList();

                    intent.putExtra(ServiceUtils.packageListType, usagePackageItem);
                    intent.putParcelableArrayListExtra("OfferInfoList", offerList);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    mContext.startActivity(intent);
                }
            }
        });
        Animation anim = AnimationUtils.loadAnimation(mContext, R.anim.out_from_right);
        anim.setFillAfter(true);

        // By default all grid items will animate together and will look like the gridview is
        // animating as a whole. So, experiment with incremental delays as below to get a
        // wave effect.
        int offset = 0;
        offset = position / 2;
        double finalOffset = Math.floor(offset);
        anim.setStartOffset((long) finalOffset * 50);

        if (usagePackageItem.getIsAnimated() == 0) {
            holder.dataCardview.setAnimation(anim);

            anim.start();
            mUsagePackageLists.get(position).setIsAnimated(1);
        }

    }

    private void oNunLimtedMinutesType(final int position) {
        Log.v("current_lang_unlimited", mSharedPrefrencesManger.getLanguage() + " ");

        holder.addPackageCardView.setVisibility(View.GONE);
        holder.dataCardview.setVisibility(View.GONE);
        holder.mixedCardView.setVisibility(View.GONE);
        holder.unlimitedCardView.setVisibility(View.VISIBLE);
        PackageConsumption packageConsumption = usagePackageItem.getOfferList().get(0).getPackageConsumption();

        if (mSharedPrefrencesManger.getLanguage().equalsIgnoreCase("en")) {
            holder.unlimted_package_NameID.setText(usagePackageItem.getNameEn());
            holder.unlimted_package_RemainigID.setText(packageConsumption.getRemaining() + "" + packageConsumption.getConsumptionUnitEn());
        } else {
            holder.unlimted_package_NameID.setText(usagePackageItem.getNameAr());
            holder.unlimted_package_RemainigID.setText(packageConsumption.getRemaining() + "" + packageConsumption.getConsumptionUnitAr());
        }


        holder.unlimited_expiresId.setText(daysDifference(usagePackageItem.getEndDate()));
//        holder.unlimited_val_desc.setText(packageConsumption.getTextEn());
        if (usagePackageItem.getIconUrl() != null)
            Picasso.with(mContext).load(usagePackageItem.getIconUrl()).into(holder.unlimitedDataImg);

        holder.unlimitedCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usagePackageItem = mUsagePackageLists.get(position);
                if (!usagePackageItem.getPackageDisabled() && !PackagesFragments.packagesDisabled) {

                    if (!usagePackageItem.getPackageDisabled()) {
                        Intent intent = new Intent(mContext, UnlimitedMinutesPackageActivity.class);
                        offerList = (ArrayList<OfferList>) mUsagePackageLists.get(position).getOfferList();
                        intent.putExtra(ServiceUtils.packageListType, usagePackageItem);
                        intent.putParcelableArrayListExtra("OfferInfoList", offerList);
//                        intent.putExtra("balance",PackagesFragments.)
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                        mContext.startActivity(intent);
                    }
                }

            }
        });
    }

    private void onMixedPackageTypes(final int position) {
        try {
            Log.v("current_lang_mixed", mSharedPrefrencesManger.getLanguage() + " ");
            holder.addPackageCardView.setVisibility(View.GONE);
            holder.dataCardview.setVisibility(View.GONE);
            holder.mixedCardView.setVisibility(View.VISIBLE);
            holder.unlimitedCardView.setVisibility(View.GONE);
            holder.newMshipCardview.setVisibility(View.GONE);
            holder.mixed_expiresId.setText(daysDifference(usagePackageItem.getEndDate()));
            PackageConsumption packageConsumption_1 = usagePackageItem.getOfferList().get(0).getPackageConsumption();
//        PackageConsumption packageConsumption_2 = usagePackageItem.getOfferList().get(1).getPackageConsumption();

            if (mSharedPrefrencesManger.getLanguage().equalsIgnoreCase("en")) {
                holder.mixed_package_name_ID.setText(usagePackageItem.getNameEn());
                holder.mixed_1_remainig_ID.setText(CommonMethods.getFormattedDouble(packageConsumption_1.getRemaining()) + " " + packageConsumption_1.getConsumptionUnitEn());
//        holder.mixed_1_remainig_desc_ID.setText(packageConsumption_1.getTextEn() + " ");
//            holder.mixed_2_remainig_ID.setText(packageConsumption_2.getRemaining() + " " + packageConsumption_2.getConsumptionUnitEn());
            } else {
                holder.mixed_package_name_ID.setText(usagePackageItem.getNameAr());
                holder.mixed_1_remainig_ID.setText(CommonMethods.getFormattedDouble(packageConsumption_1.getRemaining()) + " " + packageConsumption_1.getConsumptionUnitAr());
//        holder.mixed_1_remainig_desc_ID.setText(packageConsumption_1.getTextEn() + " ");
//            holder.mixed_2_remainig_ID.setText(CommonMethods.getFormattedDouble(packageConsumption_2.getRemaining()) + " " + packageConsumption_2.getConsumptionUnitAr());

            }
//        Animation anim = AnimationUtils.loadAnimation(mContext,R.anim.out_from_left);
//
//        // By default all grid items will animate together and will look like the gridview is
//        // animating as a whole. So, experiment with incremental delays as below to get a
//        // wave effect.
//
//        int offsetValue = 0;
//        if(position<2){
//            offsetValue = 3;
//        }else if(position>=2&&position<4){
//            offsetValue = 5;
//        }else if(position>=4 && position<6){
//            offsetValue = 7;
//        }
//        anim.setStartOffset(offsetValue * 50);
//
//        holder.itemView.setAnimation(anim);
//        anim.start();
//        holder.mixed_2_remainig_desc_ID.setText(packageConsumption_2.getTextEn() + " ");
            if (usagePackageItem.getIconUrl() != null)
                Picasso.with(mContext).load(usagePackageItem.getIconUrl()).into(holder.mixed_packageImg_ID);
            holder.mixedCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    CommonMethods.logFirebaseEvent(mSwpeMainActivity.getmFirebaseAnalytics(), ConstantUtils.TAGGING_usage_detail);


                    usagePackageItem = mUsagePackageLists.get(position);
                    if (!usagePackageItem.getPackageDisabled() && !PackagesFragments.packagesDisabled) {
                        offerList = (ArrayList<OfferList>) usagePackageItem.getOfferList();
//                    LinearLayout

                        ActivityOptionsCompat options = ActivityOptionsCompat.
                                makeSceneTransitionAnimation(mSwpeMainActivity, (View) UsageFragment.tv_balanceID, "package");
                        Intent intent = new Intent(mSwpeMainActivity, MixedDataPackageGraphPagerActivity.class);
                        intent.putExtra(ServiceUtils.packageListType, usagePackageItem);
                        intent.putParcelableArrayListExtra("OfferInfoList", offerList);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mSwpeMainActivity.startActivity(intent);//,options.toBundle());
                    }
                }
            });

            // Move this initialization to constructor so that its not initalized again and again.
            Animation anim = AnimationUtils.loadAnimation(mContext, R.anim.out_from_right);
            anim.setFillAfter(true);

            // By default all grid items will animate together and will look like the gridview is
            // animating as a whole. So, experiment with incremental delays as below to get a
            // wave effect.
            int offset = 0;
            offset = position / 2;
            double finalOffset = Math.floor(offset);
            anim.setStartOffset((long) finalOffset * 50);


            if (usagePackageItem.getIsAnimated() == 0) {
                holder.mixedCardView.setAnimation(anim);
                anim.start();
                mUsagePackageLists.get(position).setIsAnimated(1);
            }
        } catch (Exception ex) {

        }
    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mUsagePackageLists.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView data_package_NameID, unlimted_package_NameID, unlimited_val_desc, unlimted_package_RemainigID,
                mixed_2_remainig_desc_ID, mixed_2_remainig_ID, mixed_1_remainig_desc_ID, mixed_1_remainig_ID,
                mixed_package_name_ID, addone_Value_desc_ID;
        public TextView data_package_value_ID;
        public TextView data_expires_Id, unlimited_expiresId, mixed_expiresId;
        public TextView new_mship_social_package_value_ID, new_mship_general_package_value_ID, new_mship_squad_number_ID, new_mship_flexi_min_value_ID, new_mship_expires_id;
        public CardView dataCardview, mixedCardView, unlimitedCardView, addPackageCardView, newMshipCardview;
        public ImageView dataImg, unlimitedDataImg, mixed_packageImg_ID;
        public LinearLayout ll_getMembership, ll_activate_esim, ll_new_membership;
        public TextView tv_overlay;
        public Button renewNow, bt_esim_overlay, bt_new_mship;

        public ViewHolder(View v) {
            super(v);

            addPackageCardView = (CardView) v.findViewById(R.id.addPackageID);
            // mixed packages
            mixedCardView = (CardView) v.findViewById(R.id.mixedPackageID);
            mixed_expiresId = (TextView) v.findViewById(R.id.mixed_expiresId);
            mixed_packageImg_ID = (ImageView) v.findViewById(R.id.mixed_packageImg_ID);
            mixed_2_remainig_desc_ID = (TextView) v.findViewById(R.id.mixed_2_remainig_desc_ID);
            mixed_2_remainig_ID = (TextView) v.findViewById(R.id.mixed_2_remainig_ID);
            mixed_1_remainig_desc_ID = (TextView) v.findViewById(R.id.mixed_1_remainig_desc_ID);
            mixed_1_remainig_ID = (TextView) v.findViewById(R.id.mixed_1_remainig_ID);
            mixed_package_name_ID = (TextView) v.findViewById(R.id.mixed_package_name_ID);

            // unlimited packages
            unlimitedCardView = (CardView) v.findViewById(R.id.perferedPackageID);
            unlimted_package_NameID = (TextView) v.findViewById(R.id.unlimted_package_NameID);
            unlimited_val_desc = (TextView) v.findViewById(R.id.unlimited_val_desc);
            unlimted_package_RemainigID = (TextView) v.findViewById(R.id.unlimted_package_RemainigID);
            unlimited_expiresId = (TextView) v.findViewById(R.id.unlimited_expiresId);
            unlimited_val_desc = (TextView) v.findViewById(R.id.unlimited_val_desc);
            unlimitedDataImg = (ImageView) v.findViewById(R.id.unlimitedDataImg);
            //  data packages
            dataCardview = (CardView) v.findViewById(R.id.dataPackageID);
            data_package_value_ID = (TextView) v.findViewById(R.id.data_package_value_ID);
            data_package_NameID = (TextView) v.findViewById(R.id.data_package_NameID);
            dataImg = (ImageView) v.findViewById(R.id.data_package_img);
            data_expires_Id = (TextView) v.findViewById(R.id.data_expires_Id);
            addone_Value_desc_ID = (TextView) v.findViewById(R.id.addone_Value_desc_ID);
            //  data packages
            ll_getMembership = (LinearLayout) v.findViewById(R.id.rl_get_membership);
            ll_activate_esim = (LinearLayout) v.findViewById(R.id.ll_activate_esim);
            ll_new_membership = (LinearLayout) v.findViewById(R.id.ll_new_membership);
            tv_overlay = (TextView) v.findViewById(R.id.tv_overlay);
            renewNow = (Button) v.findViewById(R.id.bt_overlay);
            bt_esim_overlay = (Button) v.findViewById(R.id.bt_esim_overlay);

            // new membership
            newMshipCardview = (CardView) v.findViewById(R.id.newMembershipID);
            new_mship_social_package_value_ID = (TextView) v.findViewById(R.id.new_mship_social_package_value_ID);
            new_mship_general_package_value_ID = (TextView) v.findViewById(R.id.new_mship_general_package_value_ID);
            new_mship_flexi_min_value_ID = (TextView) v.findViewById(R.id.new_mship_flexi_min_value_ID);
            new_mship_squad_number_ID = (TextView) v.findViewById(R.id.new_mship_squad_number_ID);
            new_mship_expires_id = (TextView) v.findViewById(R.id.new_mship_expires_id);
            bt_new_mship = (Button) v.findViewById(R.id.bt_new_mship);


        }
    }

    //272
    public static String daysDifference(String date) {
        Date dateToPass;

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
        try {

            dateToPass = simpleDateFormat.parse(date);

        } catch (Exception ex) {
            dateToPass = null;
        }
        try {
            if (dateToPass != null) {
                long diffInMils = dateToPass.getTime() - dateUtils.getCurrentDateFor().getTime();
                long daysDiff = TimeUnit.MILLISECONDS.toDays(diffInMils);
                return daysDiff + " " + mContext.getString(R.string.days_left);
            } else {
                return mContext.getString(R.string.expires) + " " + date;
            }
        } catch (Exception ex) {
            CommonMethods.logException(ex);
            return "";
        }
    }

    public static long daysDifferenceLong(String date) {
        Date dateToPass;

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
        try {

            dateToPass = simpleDateFormat.parse(date);

        } catch (Exception ex) {
            dateToPass = null;
        }
        try {
            if (dateToPass != null) {
                long diffInMils = dateToPass.getTime() - dateUtils.getCurrentDateFor().getTime();
                long daysDiff = TimeUnit.MILLISECONDS.toDays(diffInMils);
                return daysDiff;
            } else {
                return 0;
            }
        } catch (Exception ex) {
            CommonMethods.logException(ex);
            return 0;
        }
    }

    public static String daysDifferenceForDetail(String date) {
        Date dateToPass;

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
        try {

            dateToPass = simpleDateFormat.parse(date);

        } catch (Exception ex) {
            dateToPass = null;
        }
        try {
            if (dateToPass != null) {
                long diffInMils = dateToPass.getTime() - dateUtils.getCurrentDateFor().getTime();
                long daysDiff = TimeUnit.MILLISECONDS.toDays(diffInMils);
                return daysDiff + " " + mContext.getString(R.string.days);
            } else {
                return mContext.getString(R.string.expires) + " " + date;
            }
        } catch (Exception ex) {
            CommonMethods.logException(ex);
            return "";
        }
    }

    private int getBoxPosition(int boxType) {
        int position = 0;
        for (int i = 0; i < ((mUsagePackageLists.size() > 5) ? 5 : mUsagePackageLists.size()); i++) {
            if (mUsagePackageLists.get(i).getPackageType() == boxType)
                position = i;
        }

        return position;
    }
}