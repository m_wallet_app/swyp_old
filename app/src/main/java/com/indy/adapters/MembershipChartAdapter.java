package com.indy.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indy.R;
import com.indy.controls.OnstoreClickInterface;
import com.indy.customviews.MyProgressBar;
import com.indy.helpers.DateHelpers;
import com.indy.models.bundles.BundleList;
import com.indy.models.packages.PackageConsumption;
import com.indy.models.packages.UsagePackageList;
import com.indy.models.packageusage.UsageGraphList;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.utils.SharedPrefrencesManger;
import com.indy.views.activites.SwpeMainActivity;
import com.indy.views.fragments.store.mainstore.MyStoreFragment;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by emad on 8/11/16.
 */
public class MembershipChartAdapter extends RecyclerView.Adapter<MembershipChartAdapter.ViewHolder> {
    private List<UsagePackageList> mUsagePackageLists;
    private List<UsageGraphList> usageGraphList;
    private String currentLanguage;
    private Context mContext;
    Typeface typeface;
    private ViewHolder holder;
    private SharedPrefrencesManger mSharedPrefrencesManger;

    public MembershipChartAdapter(List<UsagePackageList> usagePackageList, List<UsageGraphList> usageGraphList, Context context, String currentLanguage) {
        mUsagePackageLists = usagePackageList;
        this.usageGraphList = usageGraphList;
        this.currentLanguage = currentLanguage;
        this.mContext = context;
        this.mSharedPrefrencesManger = new SharedPrefrencesManger(this.mContext);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.item_membership_chart, viewGroup, false);

        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        UsagePackageList usagePackageListItem = mUsagePackageLists.get(position);

        if (currentLanguage.equals("en")) {
            typeface = Typeface.createFromAsset(mContext.getResources().getAssets(), "fonts/OpenSans-Regular.ttf");

            if (usagePackageListItem != null && usagePackageListItem.getDescriptionEn() != null)
                holder.data_package_name.setText(usagePackageListItem.getDescriptionEn());
            else
                holder.data_package_name.setText("");


        } else {
            typeface = Typeface.createFromAsset(mContext.getResources().getAssets(), "fonts/aktive_grotest_rg.ttf");

            if (usagePackageListItem != null && usagePackageListItem.getDescriptionAr() != null)
                holder.data_package_name.setText(usagePackageListItem.getDescriptionAr());
            else
                holder.data_package_name.setText("");

        }

        PackageConsumption packageConsumption = usagePackageListItem.getOfferList().get(0).getPackageConsumption();

        holder.my_ProgressBar.setDataLimitText(packageConsumption.getRemaining() + " " + packageConsumption.getConsumptionUnitEn() +
                " " + mContext.getString(R.string.data_left));

        Double remainingAllowance = 0.0;
        int finalProgress = 0;

        if(packageConsumption.getQuota() == 0){
            finalProgress = 0;
        }
        else{
            remainingAllowance = (packageConsumption.getConsumption() / packageConsumption.getQuota()) * 100;
            finalProgress = 100 - (int) Math.ceil(remainingAllowance);
        }


        holder.my_ProgressBar.setProgress(finalProgress);
//        holder.my_ProgressBar.setProgress(30);


        Double limitFromServiceValue = packageConsumption.getQuota();
        Double forecastedUsageValue = Double.parseDouble(usageGraphList.get(position).getForecastedUsage());

//        if (forecastedUsageValue > limitFromServiceValue) {
//            holder.tv_forecastUsage.setTextColor(mContext.getResources().getColor(R.color.red_color));
//        } else {
//            holder.tv_forecastUsage.setTextColor(mContext.getResources().getColor(R.color.colorBlack));
//        }

        holder.tv_forecastUsage.setText(daysDifference(usagePackageListItem.getOfferList().get(0).getInOfferEndDate(), mContext.getString(R.string.days)));
//        tv_forecastUsage.setText(PackagesListAdapter.daysDifferenceForDetail(
//                MixedDataPackageGraphPagerActivity.userPackgeListItem.getOfferList().get(0).getInOfferEndDate()));//, getString(R.string.days_left)));
        holder.tv_offerPackageAmount.setText(packageConsumption.getDisplayName());

        if (usagePackageListItem.getThrotleLabel() != null) {
            holder.tv_dailyUsedSubtitle.setText(mContext.getString(R.string.of_full_speed_used));
            holder.my_ProgressBar.setNonStop(true);
            holder.my_ProgressBar.setNonStopText(mContext.getResources().getString(R.string.non_stop));
            holder.my_ProgressBar.setFullSpeedText(usagePackageListItem.getSocialLabel());
            holder.my_ProgressBar.setThrottleLimitText(usagePackageListItem.getThrotleLabel());
            holder.my_ProgressBar.setThrottleProgress(usagePackageListItem.getThrotlePercentage());
        }

        else {
            holder.tv_dailyUsedSubtitle.setText(mContext.getString(R.string.total_used));
            holder.my_ProgressBar.setNonStop(false);
        }
        if (currentLanguage.equals("en")) {
            holder.tv_dataUsedToday.setText(usageGraphList.get(position).getUsedToday() + " " + usageGraphList.get(position).getUsedTodayUnitEn());
            holder.tv_dailyEstimatedUsage.setText(packageConsumption.getConsumption() + " " + packageConsumption.getConsumptionUnitEn());


//            tv_forecastUsage.setText(usageGraphListObject.getForecastedUsage() + " " + packageConsumption.getConsumptionUnitEn());
//            tv_forecastUsage.setText(tv_balance_expire.getText().toString());
//            tv_dailyEstimatedUsage.setText(usageGraphListObject.getDailyBudget() + " " + packageConsumption.getConsumptionUnitEn());
//            tv_offerName.setText(packageConsumption.getTextEn());
//            holder.tv_offerPackageAmount.setText(packageConsumption.getQuota().toString() + " " + packageConsumption.getConsumptionUnitEn());
        } else {
            holder.tv_dataUsedToday.setText(usageGraphList.get(position).getUsedToday() + " " + usageGraphList.get(position).getUsedTodayUnitAr());
            holder.tv_dailyEstimatedUsage.setText(packageConsumption.getConsumption() + " " + packageConsumption.getConsumptionUnitEn());

//            tv_forecastUsage.setText(usageGraphListObject.getForecastedUsage() + " " + packageConsumption.getConsumptionUnitAr());

//            holder.tv_dailyEstimatedUsage.setText(mContext.getString(R.string.of) + " " + packageConsumption.getQuota() + packageConsumption.getConsumptionUnitAr() + " " + mContext.getString(R.string.used_lower_case));
//            holder.tv_dailyEstimatedUsage.setText(usageGraphList.get(position).getDailyBudget() + " " + packageConsumption.getConsumptionUnitAr());
//            tv_offerName.setText(packageConsumption.getTextAr());
//            holder.tv_offerPackageAmount.setText(packageConsumption.getQuota().toString() + " " + packageConsumption.getConsumptionUnitAr());
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mUsagePackageLists.size();
    }

    public static String daysDifference(String date, String toAppendAtEnd) {
        Date dateToPass;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
        try {

            dateToPass = simpleDateFormat.parse(date);

        } catch (Exception ex) {
            dateToPass = null;
        }
        if (dateToPass != null) {
            long diffInMils = dateToPass.getTime() - DateHelpers.getCurrentDateFor().getTime();
            long daysDiff = TimeUnit.MILLISECONDS.toDays(diffInMils);
            return daysDiff + " " + toAppendAtEnd;
        } else {
            return toAppendAtEnd + " " + date;
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView titletext, data_package_name, tv_dailyUsedSubtitle, tv_dailyEstimatedUsage, tv_dataUsedToday, tv_forecastUsage, tv_daysLeftInGraph, tv_offerPackageAmount;
        private MyProgressBar my_ProgressBar;

        public ViewHolder(View v) {
            super(v);
            data_package_name = (TextView) v.findViewById(R.id.data_package_name);
            tv_dailyUsedSubtitle = (TextView) v.findViewById(R.id.tv_dailyEsimation);
            tv_dailyEstimatedUsage = (TextView) v.findViewById(R.id.tv_mbused_daily);
            tv_dataUsedToday = (TextView) v.findViewById(R.id.tv_mbused);
            tv_forecastUsage = (TextView) v.findViewById(R.id.tv_mbused_forecast);
            tv_offerPackageAmount = (TextView) v.findViewById(R.id.data_package_value);
            tv_daysLeftInGraph = (TextView) v.findViewById(R.id.tv_expire_days_left);
            my_ProgressBar = (MyProgressBar) v.findViewById(R.id.my_ProgressBar);
        }
    }

    public String getId(int position) {
        return mUsagePackageLists.get(position).getId();
    }

}
