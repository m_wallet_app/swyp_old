package com.indy.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.indy.R;
import com.indy.controls.OnDeleteSentTransferRequestListener;
import com.indy.controls.OnSendReminderRequestListener;
import com.indy.models.getPendingTransfersList.requestCreditFromFriend.TransacitonList;
import com.indy.models.rewards.StoreLocation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by hadi on 10/14/2016.
 */
public class SentRequestsAdapter extends RecyclerView.Adapter<SentRequestsAdapter.ViewHolder> {
    private List<TransacitonList> mSentRequestsList;
    private Context mContext;
    public OnDeleteSentTransferRequestListener onDeleteSentTransferRequestListener;
    public OnSendReminderRequestListener onSendReminderRequestListener;
    public static List<StoreLocation> storeLocations;

    public SentRequestsAdapter(List<TransacitonList> mRewardsList, Context context,
                               OnDeleteSentTransferRequestListener onDeleteSentTransferRequestListener,
                               OnSendReminderRequestListener onSendReminderRequestListener) {
        mSentRequestsList = mRewardsList;
        this.mContext = context;
        this.onDeleteSentTransferRequestListener = onDeleteSentTransferRequestListener;
        this.onSendReminderRequestListener = onSendReminderRequestListener;
    }

    Typeface tf;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.item_sent_requests, viewGroup, false);
        return new ViewHolder(view);
    }

    public String parseDate(String date){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        try {
            Date dateValue = dateFormat.parse(date);
            dateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm");
            return  dateFormat.format(dateValue);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final TransacitonList rewardsListItem = mSentRequestsList.get(position);
        tf = Typeface.createFromAsset(mContext.getAssets(),"fonts/OpenSans-Semibold.ttf");
        holder.tv_number.setTypeface(tf);
        holder.tv_number.setText(rewardsListItem.getMsisdn());
        holder.tv_price.setText(mContext.getString(R.string.aed)+ " " +rewardsListItem.getAmount() );
        //Mon Oct 17 12:21:50 GMT+04:00 2016
        holder.tv_date.setText( parseDate(rewardsListItem.getDate()));
        holder.btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onDeleteSentTransferRequestListener.onSelectedRequest(position);
            }
        });
        holder.tv_sendReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //call send reminder service function here
                //mRequestFragment
                onSendReminderRequestListener.onSendReminderRequestListener(position);
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mSentRequestsList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_number;
        public TextView tv_price;
        public TextView tv_date;
        public TextView tv_sendReminder;
        public ImageView btn_delete;

        public ViewHolder(View v) {
            super(v);
            tv_number = (TextView) v.findViewById(R.id.tv_mobileNo);
            tv_price= (TextView) v.findViewById(R.id.tv_amount);
            tv_date = (TextView) v.findViewById(R.id.tv_date);
            tv_sendReminder = (TextView) v.findViewById(R.id.tv_send_reminder);
            btn_delete = (ImageView) v.findViewById(R.id.btn_delete_sent_request);
        }
    }

//    public void remove(int position){
//        mSentRequestsList.remove(position);
//        this.notifyDataSetChanged();
//    }
//
//    public void add(TransacitonList sentRequestModel){
//        mSentRequestsList.add(sentRequestModel);
//        this.notifyDataSetChanged();
//    }

}
