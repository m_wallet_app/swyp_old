package com.indy.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import com.indy.R;
import com.indy.controls.OnLockNumberInterface;
import com.indy.models.msisdn.MsisdnList;

import java.util.List;

/**
 * Created by emad on 8/11/16.
 */
public class MsisdenListAdapter extends RecyclerView.Adapter<MsisdenListAdapter.ViewHolder> {
    private List<MsisdnList> mMsisdnList;
    private Context mContext;
    private OnLockNumberInterface onLockNumberInterface;
    int lastPosition;
    public MsisdenListAdapter(List<MsisdnList> msisdnLists, Context context, OnLockNumberInterface onLockNumberInterface) {
        mMsisdnList = msisdnLists;
        this.onLockNumberInterface = onLockNumberInterface;
        this.mContext = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.new_number_tem, viewGroup, false);
        return new ViewHolder(view);
    }



    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final MsisdnList msisdnListItem = mMsisdnList.get(position);
        holder.numberID.setText(msisdnListItem.getMsisdn());
        holder.numberID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLockNumberInterface.onSelectedNumber(msisdnListItem.getMsisdn());

            }
        });
//        setAnimation(holder.itemView,position);

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mMsisdnList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public Button numberID;

        public ViewHolder(View v) {
            super(v);
            numberID = (Button) v.findViewById(R.id.numberID);
        }
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > mMsisdnList.size())
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    public void addData(MsisdnList msisdnList){
        mMsisdnList.add(msisdnList);
        notifyDataSetChanged();
    }






}
