package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.ChangePreferredNumberInputModel;
import com.indy.models.purchaseStoreBundle.PurchaseStoreOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hadi.mehmood on 11/12/2016.
 */
public class ChangePreferredNumberService extends BaseServiceManger {

    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    ChangePreferredNumberInputModel changePreferredNumberInput;


    public ChangePreferredNumberService(BaseInterface baseInterface, ChangePreferredNumberInputModel changePreferredNumberInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.changePreferredNumberInput = changePreferredNumberInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<PurchaseStoreOutput> call = apiService.changePreferredNumber(changePreferredNumberInput);
        call.enqueue(new Callback<PurchaseStoreOutput>() {
            @Override
            public void onResponse(Call<PurchaseStoreOutput> call, Response<PurchaseStoreOutput> response) {
//                getResponseModel(response.body().toString());
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.changePreferredNumber);
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                  if (response.body()!=null && response.body().getErrorMsgEn() != null) {                      String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<PurchaseStoreOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.changePreferredNumber);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }

}
