package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.models.createprofilestatus.CreateProfileStatusInput;
import com.indy.models.createprofilestatus.CreateProfileStatusoutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to create user profile when user logs in to the application the first time. Also returns
 * application flags to determine which features are currently enabled/disabled and the user profile.
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>Registration id: Registration id already created in the application when sim is purchased.</p>
 * <p><b>Response</b></p>
 * <p>generated code: Returns code generated.</p>
 * <p>Registration id: Registration id of user</p>
 */
public class CreateProfileService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    CreateProfileStatusInput masterInputResponse;


    public CreateProfileService(BaseInterface baseInterface, CreateProfileStatusInput masterInputResponse) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.masterInputResponse = masterInputResponse;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<CreateProfileStatusoutput> call = apiService.createProfileStatus(masterInputResponse);
        call.enqueue(new Callback<CreateProfileStatusoutput>() {
            @Override
            public void onResponse(Call<CreateProfileStatusoutput> call, Response<CreateProfileStatusoutput> response) {
//                getResponseModel(response.body().toString());
                mBaseResponseModel.setServiceType(ConstantUtils.CREATE_ACCOUNT_SERVICE);
                mBaseResponseModel.setResultObj(response.body());
//                 if (response.body().getErrorMsgEn() != null) {                         String urlForTag = call.request().url().toString();                         urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");                         urlForTag = urlForTag.replace("/", "_");                         CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);                      }                     mBaseInterface.onSucessListener(mBaseResponseModel);


                if (response.body()!=null && response.body().getErrorMsgEn() != null) {                     String urlForTag = call.request().url().toString();
                    urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                    urlForTag = urlForTag.replace("/", "_");
                    CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);

                }
                if (response.body()!=null && response.body().getErrorMsgEn() != null) {                     String urlForTag = call.request().url().toString();
                    urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                    urlForTag = urlForTag.replace("/", "_");
                    CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                }
                mBaseInterface.onSucessListener(mBaseResponseModel);

            }

            @Override
            public void onFailure(Call<CreateProfileStatusoutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ConstantUtils.CREATE_ACCOUNT_SERVICE);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
