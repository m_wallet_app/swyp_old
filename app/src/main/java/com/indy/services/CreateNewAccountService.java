package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.models.createnewaccount.CreateNewAccountInput;
import com.indy.models.createnewaccount.CreateNewAccountNewResponse;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to create user profile when user logs in to the application the first time. Also returns
 * application flags to determine which features are currently enabled/disabled and the user profile.
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>Password: Password set by user</p>
 * <p>Confirm password: Password confirmed by user.</p>
 * <p>Registration id: Registration id already created in the application when profile creation service is called.</p>
 * <p><b>Response</b></p>
 * <p>updated: Returns true/false to indicate success/failure of service.</p>
 * <p>User Profile: Returns user profile object containing complete user information including name, nickname, email, number, gender,
 * birth date and user membership status.
 * </p>
 * <p>AppFlags: Application flags which determine which features of application are enabled/disabled.</p>
 */
public class CreateNewAccountService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private CreateNewAccountInput changePasswordInput;


    public CreateNewAccountService(BaseInterface baseInterface, CreateNewAccountInput mChangePasswordInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.changePasswordInput = mChangePasswordInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<CreateNewAccountNewResponse> call = apiService.createNewAccount(changePasswordInput);
        call.enqueue(new Callback<CreateNewAccountNewResponse>() {
            @Override
            public void onResponse(Call<CreateNewAccountNewResponse> call, Response<CreateNewAccountNewResponse> response) {
                mBaseResponseModel.setServiceType(null);
                Headers headers = response.headers();
                String authToken = headers.get(ConstantUtils.headerValue);
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setAuthToken(authToken);

                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                  if (response.body()!=null && response.body().getErrorMsgEn() != null) {                      String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);

                    }
                  if (response.body()!=null && response.body().getErrorMsgEn() != null) {                      String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);

                }

            }

            @Override
            public void onFailure(Call<CreateNewAccountNewResponse> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ConstantUtils.CREATE_ACCOUNT_SERVICE);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
