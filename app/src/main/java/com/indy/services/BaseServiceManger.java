package com.indy.services;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.indy.BuildConfig;
import com.indy.controls.MyEndPointsInterface;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.utils.HttpsTrustManager;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * This class initializes the Retrofit Rest service handler and is the parent class of all service classes in the project.
 */
public class BaseServiceManger {
    public MyEndPointsInterface apiService;
    final int TIMEOUT = 60; // seconds
    private HttpsTrustManager httpsTrustManager;

    public BaseServiceManger(Context mContext) {
        httpsTrustManager = new HttpsTrustManager(mContext);
    }

    public BaseServiceManger() {
    }

    public void consumeService() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.addInterceptor(interceptor);
        }
//        httpClient.sslSocketFactory(allowAllSSL());
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();
                Request request = null;
                if (BuildConfig.FLAVOR.equalsIgnoreCase("prod")) {
                    request = original.newBuilder()
                            .header("Content-Type", "application/json")
                            .header("Accept", "application/json")
                            .header("Etisalat-IndyHeader", "IndySwype")
                            .header("Content-Type", "application/json; charset=utf-8")
                            .method(original.method(), original.body())
                            .build();
                } else {
                    request = original.newBuilder()
                            .header("Content-Type", "application/json")
                            .header("Accept", "application/json")
                            .header("Etisalat-IndyHeader", "IndySwype")
                            .header("Content-Type", "application/json; charset=utf-8")
                            .header("custom_header", "pre_prod")
                            .method(original.method(), original.body())
                            .build();
                }
                return chain.proceed(request);
            }
        });
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        OkHttpClient client = httpClient.readTimeout(TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MyEndPointsInterface.baseUrl).addConverterFactory(GsonConverterFactory.create(gson))
                .client(client).build();

        apiService = retrofit.create(MyEndPointsInterface.class);
    }

    public MasterErrorResponse getResponseModel(String reponse) {
        MasterErrorResponse responseStr = null;
        Gson gson = new GsonBuilder().create();


        try {
            responseStr = gson.fromJson(reponse, MasterErrorResponse.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseStr;
    }

    public void onWeServiceCallCanceled() {
    }
}
