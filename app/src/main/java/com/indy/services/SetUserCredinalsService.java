package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.models.createnewaccount.CreateNewAccountInput;
import com.indy.models.createnewaccount.CreateNewAccountOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service sets the password of new user.
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>password: new password to be set. </p>
 * <p>password confirmation: password confirmation(should be same as new password for successful service execution.</p>
 * <p>registrationId: The registration id of the user.</p>
 * <p><b>Response</b></p>
 * <p>updated: Returns boolean value of true/false to indicate success/failure.</p>
 */
public class SetUserCredinalsService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private CreateNewAccountInput changePasswordInput;


    public SetUserCredinalsService(BaseInterface baseInterface, CreateNewAccountInput mChangePasswordInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.changePasswordInput = mChangePasswordInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<CreateNewAccountOutput> call = apiService.setUserCredials(changePasswordInput);
        call.enqueue(new Callback<CreateNewAccountOutput>() {
            @Override
            public void onResponse(Call<CreateNewAccountOutput> call, Response<CreateNewAccountOutput> response) {
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(null);
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                  if (response.body()!=null && response.body().getErrorMsgEn() != null) {                      String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<CreateNewAccountOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(null);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
