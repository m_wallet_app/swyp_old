package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.getTokenForPassword.GetPreLoginOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterInputResponse;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to get security encrypted json for login service.
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>Uses base request</p>
 * <p><b>Response</b></p>
 * <p>Identifier: Identifier to append to password service.
 * </p>
 */

public class GetPreLoginInfoService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    MasterInputResponse masterInputResponse;


    public GetPreLoginInfoService(BaseInterface baseInterface, MasterInputResponse masterInputResponse) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.masterInputResponse = masterInputResponse;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<GetPreLoginOutput> call = apiService.getPreLogin(masterInputResponse);
        call.enqueue(new Callback<GetPreLoginOutput>() {
            @Override
            public void onResponse(Call<GetPreLoginOutput> call, Response<GetPreLoginOutput> response) {
//                getResponseModel(response.body().toString());
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.isPreLogin);
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                  if (response.body()!=null && response.body().getErrorMsgEn() != null) {                      String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<GetPreLoginOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.isPreLogin);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
