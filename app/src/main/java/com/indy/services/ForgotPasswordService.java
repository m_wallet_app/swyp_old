package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.models.ForgotPassword.ForgotPasswordInputModel;
import com.indy.models.ForgotPassword.ForgotPasswordOutputModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * * This service is used for initiating reset password process. Once it is called, a 4 digit code is sent to user's mobile number.
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>Contact: Contact number of user to send code to. [DEPRECATED] [ Now code is sent to registered number.]
 * <p><b>Response</b></p>
 * <p>generatedCode: The generated code from server. [DEPRECATED] [ Verify OTP is used to verify code from server.]</p>
 * </p>
 */
public class ForgotPasswordService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private ForgotPasswordInputModel forgotPasswordInputModel;


    public ForgotPasswordService(BaseInterface baseInterface, ForgotPasswordInputModel mForgotPasswordInputModel) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.forgotPasswordInputModel = mForgotPasswordInputModel;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<ForgotPasswordOutputModel> call = apiService.forgotPasswordService(forgotPasswordInputModel);
        call.enqueue(new Callback<ForgotPasswordOutputModel>() {
            @Override
            public void onResponse(Call<ForgotPasswordOutputModel> call, Response<ForgotPasswordOutputModel> response) {
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ConstantUtils.forgotPasswordService);
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                  if (response.body()!=null && response.body().getErrorMsgEn() != null) {                      String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<ForgotPasswordOutputModel> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ConstantUtils.forgotPasswordService);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
