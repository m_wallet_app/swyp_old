package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetRewardForSharingInputModel;
import com.indy.views.fragments.gamification.models.serviceInputOutput.GetRewardForSharingOutputResponse;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class GetRewardForSharingService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private GetRewardForSharingInputModel editProfileInput;

    public GetRewardForSharingService(BaseInterface baseInterface, GetRewardForSharingInputModel editProfileInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.editProfileInput = editProfileInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<GetRewardForSharingOutputResponse> call = apiService.getRewardForShare(editProfileInput);
        call.enqueue(new Callback<GetRewardForSharingOutputResponse>() {
            @Override
            public void onResponse(Call<GetRewardForSharingOutputResponse> call, Response<GetRewardForSharingOutputResponse> response) {
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.getRewardForSharingService);
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (response.body() != null && response.body().getErrorMsgEn() != null) {
                        String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<GetRewardForSharingOutputResponse> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.getRewardForSharingService);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }


}
