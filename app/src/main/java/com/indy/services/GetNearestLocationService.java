package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.location.LocationInputModel;
import com.indy.models.location.LocationModelResponse;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to get locations for the requested type including Shops, Wifi Hotspots, Wifi hotspots in find us, nearest store locations and nearest kiosks.
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>Geolocation: The location of the user so distance in KM from user location can be returned.
 * <p>Size: Number of locations to return.[DEPRECATED: Ignored now from server side]</p>
 * <p>Significant: The kind of locations to get</p>
 * <p><b>Response</b></p>
 * <p>nearestLocationList: Returns list of nearest locations with name, distance, id  and geolocation.
 * </p>
 */
public class GetNearestLocationService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private LocationInputModel mLocationInputModel;

    public GetNearestLocationService(BaseInterface baseInterface, LocationInputModel locationInputModel) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.mLocationInputModel = locationInputModel;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<LocationModelResponse> call = apiService.getNearestLocationService(mLocationInputModel);
        call.enqueue(new Callback<LocationModelResponse>() {
            @Override
            public void onResponse(Call<LocationModelResponse> call, Response<LocationModelResponse> response) {
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.nearestLocationType);
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                  if (response.body()!=null && response.body().getErrorMsgEn() != null) {                      String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<LocationModelResponse> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.nearestLocationType);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
