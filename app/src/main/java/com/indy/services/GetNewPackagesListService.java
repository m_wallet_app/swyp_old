package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.packages.UsagePackagesoutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterInputResponse;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to get user's subscribed packages.
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>Uses base request</p>
 * <p><b>Response</b></p>
 * <p>usagePackageList: Returns list of user packages with name, expiry date, current remaining usage etc.
 * </p>
 */
public class GetNewPackagesListService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private MasterInputResponse masterInputResponse;

    public GetNewPackagesListService(BaseInterface baseInterface, MasterInputResponse masterInputResponse) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.masterInputResponse = masterInputResponse;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<UsagePackagesoutput> call = apiService.getNewPackesList(masterInputResponse);
        call.enqueue(new Callback<UsagePackagesoutput>() {
            @Override
            public void onResponse(Call<UsagePackagesoutput> call, Response<UsagePackagesoutput> response) {
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.packageServiceType);
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (response.body()!=null && response.body().getErrorMsgEn() != null) {                      String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<UsagePackagesoutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.packageServiceType);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
