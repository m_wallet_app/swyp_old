package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.benefits.NewBenefitsModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterInputResponse;
import com.indy.utils.CommonMethods;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to get text for benefits of buying swyp membership
 * <p>
 * <p><b>Field description</b></p>
 * <p>Uses base request</p>
 * <p><b>Response</b></p>
 * <p>benefitsList: Returns a list of benefits containing title/description pairs of text. </p>
 * <p>Header: A line of text to serve as header of the page</p>
 * <p>Footer: A line of text to serve as footer of the page</p>
 */
public class NewBenefitsService extends BaseServiceManger {

    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    MasterInputResponse masterInputResponse;


    public NewBenefitsService(BaseInterface baseInterface, MasterInputResponse masterInputResponse) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.masterInputResponse = masterInputResponse;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<NewBenefitsModel> call = apiService.getNewBenefits(masterInputResponse);
        call.enqueue(new Callback<NewBenefitsModel>() {
            @Override
            public void onResponse(Call<NewBenefitsModel> call, Response<NewBenefitsModel> response) {
//                getResponseModel(response.body().toString());
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.benefitsText);
                if (response.body()!=null && response.body().getErrorMsgEn() != null) {
                    String urlForTag = call.request().url().toString();
                    urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                    urlForTag = urlForTag.replace("/", "_");
                    CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                }
                mBaseInterface.onSucessListener(mBaseResponseModel);
            }

            @Override
            public void onFailure(Call<NewBenefitsModel> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.benefitsText);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }

}

