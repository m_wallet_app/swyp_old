package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.models.unsubscribe.UnsubscribeHotspotOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterInputResponse;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service logs out the user from wifi hotspots of swyp.
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>Uses base request</p>
 * <p><b>Response</b></p>
 * <p>un subscribed: Returns boolean value of true/false to indicate success/failure.</p>
 */
public class UnsubscribeService extends BaseServiceManger {
    MasterInputResponse masterInputResponse;
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;


    public UnsubscribeService(BaseInterface baseInterface, MasterInputResponse masterInputResponse) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.masterInputResponse = masterInputResponse;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<UnsubscribeHotspotOutput> call = apiService.unsubscribeHotspot(masterInputResponse);
        call.enqueue(new Callback<UnsubscribeHotspotOutput>() {
            @Override
            public void onResponse(Call<UnsubscribeHotspotOutput> call, Response<UnsubscribeHotspotOutput> response) {
//                getResponseModel(response.body().toString());
                mBaseResponseModel.setResultObj(response.body());
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                  if (response.body()!=null && response.body().getErrorMsgEn() != null) {                      String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<UnsubscribeHotspotOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
