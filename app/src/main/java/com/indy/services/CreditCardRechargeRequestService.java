package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.rechargeCreditCard.CreditCardPaymentRequestInput;
import com.indy.models.rechargeCreditCard.RechargeCreditCardRequestOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to create payment request for user recharging through credit card. User enters desired amount and is then taken to the credit card detaiks page.
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>Amount: Amount to recharge</p>
 * <p>Order type: Function to define which kind of payment is being requested.</p>
 * <p>Registration id: Registration id of user.</p>
 * <p><b>Response</b></p>
 * <p>TransactionID: Unique generated transaction id for this request.</p>
 * <p>PaymentGatewayURL: URL to redirect user to payment gateway with amount pre loaded in the page.
 * </p>
 */
public class CreditCardRechargeRequestService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private CreditCardPaymentRequestInput creditCardPaymentRequestInput;

    @Override
    public void consumeService() {
        super.consumeService();
        Call<RechargeCreditCardRequestOutput> call = apiService.initializeCreditCardRechargePayment(creditCardPaymentRequestInput);
        call.enqueue(new Callback<RechargeCreditCardRequestOutput>() {
            @Override
            public void onResponse(Call<RechargeCreditCardRequestOutput> call, Response<RechargeCreditCardRequestOutput> response) {
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.rechargeCreditCard);
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                  if (response.body()!=null && response.body().getErrorMsgEn() != null) {                      String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<RechargeCreditCardRequestOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(null);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }

    public CreditCardRechargeRequestService(BaseInterface baseInterface, CreditCardPaymentRequestInput creditCardPaymentRequestInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.creditCardPaymentRequestInput = creditCardPaymentRequestInput;
        consumeService();
    }
}
