package com.indy.services;

/**
 * Created by hadi on 21/05/2018.
 */

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.msisdn.MsisdenListResponse;
import com.indy.models.search_msisdn_model.SearchMsisdnInputModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchMsisdnService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    SearchMsisdnInputModel searchMsisdnInputModel;

    public SearchMsisdnService(BaseInterface baseInterface, SearchMsisdnInputModel searchMsisdnInputModel) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.searchMsisdnInputModel = searchMsisdnInputModel;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<MsisdenListResponse> call = apiService.searchNumber(searchMsisdnInputModel);
        call.enqueue(new Callback<MsisdenListResponse>() {
            @Override
            public void onResponse(Call<MsisdenListResponse> call, Response<MsisdenListResponse> response) {
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.SEARCH_MSISDN);
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (response.body() != null && response.body().getErrorMsgEn() != null) {
                        String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<MsisdenListResponse> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.SEARCH_MSISDN);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
