package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.views.fragments.gamification.models.serviceInputOutput.SuccessOutputResponse;
import com.indy.views.fragments.gamification.models.serviceInputOutput.UpdateProfileGamificationInputModel;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EditProfileGamificationService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private UpdateProfileGamificationInputModel editProfileInput;

    public EditProfileGamificationService(BaseInterface baseInterface, UpdateProfileGamificationInputModel editProfileInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.editProfileInput = editProfileInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<SuccessOutputResponse> call = apiService.updateProfileGamification(editProfileInput);
        call.enqueue(new Callback<SuccessOutputResponse>() {
            @Override
            public void onResponse(Call<SuccessOutputResponse> call, Response<SuccessOutputResponse> response) {
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.updateProfileGamification);
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (response.body() != null && response.body().getErrorMsgEn() != null) {
                        String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<SuccessOutputResponse> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.updateProfile);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }


}
