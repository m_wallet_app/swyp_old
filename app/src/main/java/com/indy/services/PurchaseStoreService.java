package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.purchaseStoreBundle.PurchaseStoreInputResponse;
import com.indy.models.purchaseStoreBundle.PurchaseStoreOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to finalize items purchase from the store.
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>Packages list: Packages list containing packages to buy.</p>
 * <p><b>Response</b></p>
 * <p>purchaseBundleList: Returns list of items purchased from store. </p>
 * <p>responseCode: Returns success/failure response in case of service success/failure cases respectively.</p>
 */
public class PurchaseStoreService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private PurchaseStoreInputResponse purchaseStoreInputResponse;

    public PurchaseStoreService(BaseInterface baseInterface, PurchaseStoreInputResponse purchaseStoreInputResponse) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.purchaseStoreInputResponse = purchaseStoreInputResponse;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<PurchaseStoreOutput> call = apiService.purchaseStoreItems(purchaseStoreInputResponse);
        call.enqueue(new Callback<PurchaseStoreOutput>() {
            @Override
            public void onResponse(Call<PurchaseStoreOutput> call, Response<PurchaseStoreOutput> response) {
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.purchaseBundle);
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                  if (response.body()!=null && response.body().getErrorMsgEn() != null) {                      String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<PurchaseStoreOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.purchaseBundle);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
