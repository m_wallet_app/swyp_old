package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.isEligable.IsEligableOutputModel;
import com.indy.models.isEligable.IsEligiableInputModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to check if user is eligible for transfer/request or not.
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>target Msisdn: The msisdn of the user whom the request is directed towards.</p>
 * <p>amount: The amount user is sending/requesting from the target msisdn.</p>
 * <p><b>Response</b></p>
 * <p>required Fees: The fees required for the amount requested.</p>
 * <p>amount: The amount to be sent</p>
 * <p>total amount: The total amount including the fees amount.</p>
 * <p>eligible: Is the user eligible for this transfer or not.</p>
 */
public class isEligableService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    IsEligiableInputModel isEligiableInputModel;

    public isEligableService(BaseInterface baseInterface, IsEligiableInputModel mIsEligiableInputModel) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.isEligiableInputModel = mIsEligiableInputModel;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<IsEligableOutputModel> call = apiService.isEligible(isEligiableInputModel);
        call.enqueue(new Callback<IsEligableOutputModel>() {
            @Override
            public void onResponse(Call<IsEligableOutputModel> call, Response<IsEligableOutputModel> response) {
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.isEligable);
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                  if (response.body()!=null && response.body().getErrorMsgEn() != null) {                      String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<IsEligableOutputModel> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.isEligable);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
