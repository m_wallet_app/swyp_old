package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.esimActivation.ActivateESIMResponseModel;
import com.indy.models.esimActivation.CheckESIMEligibiltyResponseModel;
import com.indy.models.faqs.FaqsResponseModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterInputResponse;
import com.indy.utils.CommonMethods;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivateESIMService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private MasterInputResponse masterInputResponse;


    public ActivateESIMService(BaseInterface baseInterface, MasterInputResponse masterInputResponse) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.masterInputResponse = masterInputResponse;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<ActivateESIMResponseModel> call = apiService.activateESIMService(masterInputResponse);
        call.enqueue(new Callback<ActivateESIMResponseModel>() {
            @Override
            public void onResponse(Call<ActivateESIMResponseModel> call, Response<ActivateESIMResponseModel> response) {
                mBaseResponseModel.setResultObj(response.body());
                if (response.body() != null && response.body().getErrorMsgEn() != null) {
                    String urlForTag = call.request().url().toString();
                    urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                    urlForTag = urlForTag.replace("/", "_");
                    CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                }
                mBaseResponseModel.setServiceType(ServiceUtils.get_esim_eligibility_service);
                mBaseInterface.onSucessListener(mBaseResponseModel);
            }

            @Override
            public void onFailure(Call<ActivateESIMResponseModel> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setResultObj(ServiceUtils.get_esim_eligibility_service);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }


}
