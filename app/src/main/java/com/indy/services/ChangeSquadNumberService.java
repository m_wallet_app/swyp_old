package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.squad_number.models.AddSquadNumberModel;
import com.indy.squad_number.models.ChangeSquadInput;
import com.indy.squad_number.models.SquadNumberOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangeSquadNumberService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private AddSquadNumberModel changePasswordInput;
    Call<SquadNumberOutput> call;

    public ChangeSquadNumberService(BaseInterface baseInterface, AddSquadNumberModel changePasswordInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.changePasswordInput = changePasswordInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        call = apiService.changeSquadNumberService(changePasswordInput);
        call.enqueue(new Callback<SquadNumberOutput>() {
            @Override
            public void onResponse(Call<SquadNumberOutput> call, Response<SquadNumberOutput> response) {
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.change_squad_number_service);
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (response.body() != null && response.body().getErrorMsgEn() != null) {
                        String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<SquadNumberOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.change_squad_number_service);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }

    @Override
    public void onWeServiceCallCanceled() {
        super.onWeServiceCallCanceled();
        if (call != null)
            call.cancel();
    }
}


