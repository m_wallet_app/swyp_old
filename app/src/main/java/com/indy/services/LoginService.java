package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.login.LoginInputModel;
import com.indy.models.login.LoginOutputModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to login user.
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>password: The password entered by the user is hashed by application and then sent in this parameter.</p>
 * <p><b>Response</b></p>
 * <p>User Profile: Returns user profile object containing complete user information including name, nickname, email, number, gender,
 * birth date and user membership status.
 * </p>
 * <p>AppFlags: Application flags which determine which features of application are enabled/disabled.</p>
 */
public class LoginService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private LoginInputModel loginInputModel;


    public LoginService(BaseInterface baseInterface, LoginInputModel mLoginInputModel) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.loginInputModel = mLoginInputModel;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<LoginOutputModel> call = apiService.loginService(loginInputModel);
        call.enqueue(new Callback<LoginOutputModel>() {
            @Override
            public void onResponse(Call<LoginOutputModel> call, Response<LoginOutputModel> response) {
                Headers headers = response.headers();
                String authToken = headers.get(ConstantUtils.headerValue);
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setAuthToken(authToken);
                mBaseResponseModel.setServiceType(ServiceUtils.loginService);
                if (response.body() != null && response.body().getErrorMsgEn() != null) {
                    String urlForTag = call.request().url().toString();
                    urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                    urlForTag = urlForTag.replace("/", "_");
                    CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                }
                mBaseInterface.onSucessListener(mBaseResponseModel);
            }

            @Override
            public void onFailure(Call<LoginOutputModel> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.loginService);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
