package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.msisdn.MsisdenListInput;
import com.indy.models.msisdn.MsisdenListResponse;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to get msisdn list of new numbers during on boarding.
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>Uses base request.</p>
 * <p><b>Response</b></p>
 * <p>msisdn list:Returns list of msisdn for user to select from.
 * </p>
 */
public class MsisdnService extends BaseServiceManger {
    MsisdenListInput msisdenListInput;
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;


    public MsisdnService(BaseInterface baseInterface, MsisdenListInput msisdenListInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.msisdenListInput = msisdenListInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<MsisdenListResponse> call = apiService.getMsisdnService(msisdenListInput);
        call.enqueue(new Callback<MsisdenListResponse>() {
            @Override
            public void onResponse(Call<MsisdenListResponse> call, Response<MsisdenListResponse> response) {
//                getResponseModel(response.body().toString());
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.msisdnService);
                if (response.body()!=null && response.body().getErrorMsgEn() != null) {                     String urlForTag = call.request().url().toString();
                    urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                    urlForTag = urlForTag.replace("/", "_");
                    CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                }
                mBaseInterface.onSucessListener(mBaseResponseModel);
            }

            @Override
            public void onFailure(Call<MsisdenListResponse> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.msisdnService);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
