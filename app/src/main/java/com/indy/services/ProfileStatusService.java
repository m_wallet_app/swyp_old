package com.indy.services;

import android.util.Log;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.profilestatus.NewProileStautsOutput;
import com.indy.models.profilestatus.ProfileStatusInput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to receive user status..
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>registration id: The registration id of the user.</p>
 * <p><b>Response</b></p>
 * <p>profile status: The current profile status of user. e.g. already registered, already purchased sim, waiting for sim delivery.</p>
 * <p>App flags: Returns flag to determine which features of application are currently enabled.</p>
 * <p>User profile: Returns user profile object containing complete user information including name, nickname, email, number, gender,
 * birth date and user membership status.</p>
 * <p>Registration id: Registration id of user.</p>
 * <p>Password status</p>
 */
public class ProfileStatusService extends BaseServiceManger {

    ProfileStatusInput profileStatusInput;
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;

    public ProfileStatusService(BaseInterface baseInterface, ProfileStatusInput profileStatusInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.profileStatusInput = profileStatusInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<NewProileStautsOutput> call = apiService.getProfileStatusService(profileStatusInput);
        call.enqueue(new Callback<NewProileStautsOutput>() {
            @Override
            public void onResponse(Call<NewProileStautsOutput> call, Response<NewProileStautsOutput> response) {
                Log.v("reponse is", response.body() + " " + response.message() + " " + response.errorBody());
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.profileStatusServiceType);
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        if (response.body()!=null && response.body().getErrorMsgEn() != null) {
                            String urlForTag = call.request().url().toString();
                            urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                            urlForTag = urlForTag.replace("/", "_");
                            CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<NewProileStautsOutput> call, Throwable t) {
                Log.v("failure", "failure" + " " + call.toString());
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.profileStatusServiceType);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }

}
