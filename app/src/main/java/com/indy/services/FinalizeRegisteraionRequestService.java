package com.indy.services;

import android.util.Log;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.finalizeregisteration.FinalizeRegisterationRequestInput;
import com.indy.models.finalizeregisteration.FinalizeRegisterationRequestOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used for finalizing user order. It is called once user moves forward from OrderSummary screen.
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>User order details including registrationId, delivery address, shipping type, order type, total amount.
 * <p><b>Response</b></p>
 * <p>paymentGatewayURL: The payment gateway url to load for payment.</p>
 * <p>transactionID: The transaction Id of the request.</p>
 * </p>
 */
public class FinalizeRegisteraionRequestService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private FinalizeRegisterationRequestInput finalizeRegisterationRequestInput;
    Call<FinalizeRegisterationRequestOutput> call;

    public FinalizeRegisteraionRequestService(BaseInterface baseInterface, FinalizeRegisterationRequestInput finalizeRegisterationRequestInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.finalizeRegisterationRequestInput = finalizeRegisterationRequestInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        call = apiService.finalizeRegisteration(finalizeRegisterationRequestInput);
        call.enqueue(new Callback<FinalizeRegisterationRequestOutput>() {
            @Override
            public void onResponse(Call<FinalizeRegisterationRequestOutput> call, Response<FinalizeRegisterationRequestOutput> response) {
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.finalize_registration);
                if (response.body()!=null && response.body().getErrorMsgEn() != null) {                     String urlForTag = call.request().url().toString();
                    urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                    urlForTag = urlForTag.replace("/", "_");
                    CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                }

                mBaseInterface.onSucessListener(mBaseResponseModel);
            }

            @Override
            public void onFailure(Call<FinalizeRegisterationRequestOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.finalize_registration);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }

    @Override
    public void onWeServiceCallCanceled() {
        super.onWeServiceCallCanceled();
        if (call != null)
            call.cancel();
        Log.v("oooops", call.isCanceled() + " ");
    }
}
