package com.indy.services;

import android.app.Service;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.emiratesIdVerification.VerifyEmiratesIdInput;
import com.indy.models.emiratesIdVerification.VerifyEmiratesIdOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to validate emirates id during new number purchase.
 * <p>
 * <p><b>Field description</b></p>
 * <p>User order details including registrationId, delivery address, shipping type, order type, total amount.</p>
 * <p>emirates id: The emirates id entered by the user.</p>
 * <p>Registration id: The registration id of the user.</p>
 * <p><b>Response</b></p>
 * <p>status: Returns an integer which indicates whether emirates id is valid, invalid, not matching with id in record or is valid but bill amount is due.</p>
 * <p>amount: The amount due in case there is pending bill amount to be paid.</p>
 */
public class VerifyEmiratesIdForNewOrderService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private VerifyEmiratesIdInput verifyEmiratesIdInput;


    public VerifyEmiratesIdForNewOrderService(BaseInterface baseInterface, VerifyEmiratesIdInput verifyEmiratesIdInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.verifyEmiratesIdInput = verifyEmiratesIdInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<VerifyEmiratesIdOutput> call = apiService.verifyEmiratesIfForNewOrder(verifyEmiratesIdInput);
        call.enqueue(new Callback<VerifyEmiratesIdOutput>() {
            @Override
            public void onResponse(Call<VerifyEmiratesIdOutput> call, Response<VerifyEmiratesIdOutput> response) {
                mBaseResponseModel.setResultObj(response.body());
                if (response.body()!=null && response.body().getErrorMsgEn() != null) {                     String urlForTag = call.request().url().toString();
                    urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                    urlForTag = urlForTag.replace("/", "_");
                    CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                }
                mBaseResponseModel.setServiceType(ServiceUtils.emirates_id_verify_old);
                mBaseInterface.onSucessListener(mBaseResponseModel);
            }

            @Override
            public void onFailure(Call<VerifyEmiratesIdOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.emirates_id_verify_old);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }


}
