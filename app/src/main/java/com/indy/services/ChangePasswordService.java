package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.models.changepassword.ChangePasswordInput;
import com.indy.models.changepassword.ChangePasswordOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * /**
 * This service is used to change current password of user.
 * <p>
 * <p><b>Field description</b></p>
 * <p><b>Request params</b></p>
 * <p>old password: Old password</p>
 * <p>new password: new password to be set. </p>
 * <p>password confirmation: password confirmation(should be same as new password for successful service execution.</p>
 * <p><b>Response</b></p>
 * <p>updated: Returns boolean value of true/false to indicate success/failure.</p>
 */
public class ChangePasswordService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private ChangePasswordInput changePasswordInput;
    Call<ChangePasswordOutput> call;

    public ChangePasswordService(BaseInterface baseInterface, ChangePasswordInput changePasswordInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.changePasswordInput = changePasswordInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        call = apiService.changePasswordService(changePasswordInput);
        call.enqueue(new Callback<ChangePasswordOutput>() {
            @Override
            public void onResponse(Call<ChangePasswordOutput> call, Response<ChangePasswordOutput> response) {
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ConstantUtils.cahngePasswordService);
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                  if (response.body()!=null && response.body().getErrorMsgEn() != null) {                      String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<ChangePasswordOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ConstantUtils.cahngePasswordService);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }

    @Override
    public void onWeServiceCallCanceled() {
        super.onWeServiceCallCanceled();
        if (call != null)
            call.cancel();
    }
}
