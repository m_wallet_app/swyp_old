package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.sendReminder.SendReminderInput;
import com.indy.models.transferCredit.TransferCreditOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to send reminders to users from whom we have requested credit already.
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>request id: The request id for which the reminder must be sent.</p>
 * <p><b>Response</b></p>
 * <p>Sent: returns boolean value of true/false in case of reminder sent success/failure case respectively. </p>
 */
public class SendReminderService extends BaseServiceManger {
    SendReminderInput sendReminderInput;
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;


    public SendReminderService(BaseInterface baseInterface, SendReminderInput sendReminderInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.sendReminderInput = sendReminderInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<TransferCreditOutput> call = apiService.sendReminder(sendReminderInput);
        call.enqueue(new Callback<TransferCreditOutput>() {
            @Override
            public void onResponse(Call<TransferCreditOutput> call, Response<TransferCreditOutput> response) {
//                getResponseModel(response.body().toString());
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.sendReminder);
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                  if (response.body()!=null && response.body().getErrorMsgEn() != null) {                      String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<TransferCreditOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.sendReminder);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
