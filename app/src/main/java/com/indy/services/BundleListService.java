package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.bundles.BundleListOutPut;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterInputResponse;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to get current available bundles in store for a particular user.
 * <p>
 * <p><b>Field description</b></p>
 * <p>Uses base request</p>
 * <p><b>Response</b></p>
 * <p>bundlesList: Returns list of bundles with bundle name, id, price, max available count, expiry date and description.</p>
 * <p>Returns failure or empty list in case no reward is found.</p>
 */
public class BundleListService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private MasterInputResponse masterInputResponse;


    public BundleListService(BaseInterface baseInterface, MasterInputResponse mInputRresponse) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.masterInputResponse = mInputRresponse;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<BundleListOutPut> call = apiService.getBundlesList(masterInputResponse);
        call.enqueue(new Callback<BundleListOutPut>() {
            @Override
            public void onResponse(Call<BundleListOutPut> call, Response<BundleListOutPut> response) {
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.bundleStore);
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                  if (response.body()!=null && response.body().getErrorMsgEn() != null) {                      String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<BundleListOutPut> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
