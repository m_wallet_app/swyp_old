package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.models.rechargeList.RechargeListOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterInputResponse;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to get pre defined amount values for recharge through credit card.
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>Uses base request.</p>
 * <p><b>Response</b></p>
 * <p>rechargeList: Returns list of recharge values.</p>
 */
public class RechargeListService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private MasterInputResponse rechargeListInput;

    public RechargeListService(BaseInterface baseInterface, MasterInputResponse masterInputResponse) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.rechargeListInput = masterInputResponse;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<RechargeListOutput> call = apiService.rechargeList(rechargeListInput);
        call.enqueue(new Callback<RechargeListOutput>() {
            @Override
            public void onResponse(Call<RechargeListOutput> call, Response<RechargeListOutput> response) {
                mBaseResponseModel.setResultObj(response.body());
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                  if (response.body()!=null && response.body().getErrorMsgEn() != null) {                      String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<RechargeListOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
