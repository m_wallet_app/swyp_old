package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.esimActivation.CheckESIMEligibiltyResponseModel;
import com.indy.models.faqs.FaqsResponseModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.models.utils.MasterInputResponse;
import com.indy.utils.CommonMethods;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PurchaseNewMembershipService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private MasterInputResponse masterInputResponse;


    public PurchaseNewMembershipService(BaseInterface baseInterface, MasterInputResponse masterInputResponse) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.masterInputResponse = masterInputResponse;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<MasterErrorResponse> call = apiService.purchaseNewMembership(masterInputResponse);
        call.enqueue(new Callback<MasterErrorResponse>() {
            @Override
            public void onResponse(Call<MasterErrorResponse> call, Response<MasterErrorResponse> response) {
                mBaseResponseModel.setResultObj(response.body());
                if (response.body() != null && response.body().getErrorMsgEn() != null) {
                    String urlForTag = call.request().url().toString();
                    urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                    urlForTag = urlForTag.replace("/", "_");
                    CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                }
                mBaseResponseModel.setServiceType(ServiceUtils.purchase_new_membership_service);
                mBaseInterface.onSucessListener(mBaseResponseModel);
            }

            @Override
            public void onFailure(Call<MasterErrorResponse> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.purchase_new_membership_service);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }


}
