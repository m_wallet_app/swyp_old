package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.LogEventInputModel;
import com.indy.models.getUserMessage.GetSuccessMessageOutputModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to get congratulations text for user upon creating new profile.
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>Message key: Fixef value to request user message</p>
 * <p><b>Response</b></p>
 * <p>messageText: Returns message text to display.
 * </p>
 */
public class LogApplicationFlowService extends BaseServiceManger {

    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    LogEventInputModel logEventInputModel;


    public LogApplicationFlowService(BaseInterface baseInterface, LogEventInputModel logEventInputModel) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.logEventInputModel = logEventInputModel;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<GetSuccessMessageOutputModel> call = apiService.logAppEvent(logEventInputModel);
        call.enqueue(new Callback<GetSuccessMessageOutputModel>() {
            @Override
            public void onResponse(Call<GetSuccessMessageOutputModel> call, Response<GetSuccessMessageOutputModel> response) {
//                getResponseModel(response.body().toString());
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.LOG_USER_EVENT);
                if (response.body()!=null && response.body().getErrorMsgEn() != null) {                     String urlForTag = call.request().url().toString();
                    urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                    urlForTag = urlForTag.replace("/", "_");
                    CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                }
                mBaseInterface.onSucessListener(mBaseResponseModel);
            }

            @Override
            public void onFailure(Call<GetSuccessMessageOutputModel> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.LOG_USER_EVENT);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }

}
