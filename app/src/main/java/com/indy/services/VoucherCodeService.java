package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.voucherCode.vouchercodeInput;
import com.indy.models.voucherCode.vouchercodeOutput;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to receive the voucher code for a specific reward item.
 * <p><b>Request</b></p>
 * <p>Offer id: The offer id of the reward for which the voucher code is required.
 * <p><b>Response</b></p>
 * <p>VoucherCodeOutput: The voucher code for the reward item.</p>
 */
public class VoucherCodeService extends BaseServiceManger {
    vouchercodeInput vouchercodeInput;
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;


    public VoucherCodeService(BaseInterface baseInterface, vouchercodeInput vouchercodeInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.vouchercodeInput = vouchercodeInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<vouchercodeOutput> call = apiService.VoucherCode(vouchercodeInput);
        call.enqueue(new Callback<vouchercodeOutput>() {
            @Override
            public void onResponse(Call<vouchercodeOutput> call, Response<vouchercodeOutput> response) {
//                getResponseModel(response.body().toString());
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.VoucherCode);
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                  if (response.body()!=null && response.body().getErrorMsgEn() != null) {                      String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<vouchercodeOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.VoucherCode);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
