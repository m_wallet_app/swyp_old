package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.redeemVoucher.RedeemVoucherInput;
import com.indy.models.redeemVoucher.RedeemVoucherOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to redeem a reward after merchant has entered pin.
 * * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>offerId: The offer id to redeem.</p>
 * <p><b>Response</b></p>
 * <p>redeemed: Returns boolean value of true/false for success/failure case. </p>
 */
public class RedeemVoucherService extends BaseServiceManger {
    RedeemVoucherInput redeemVoucherInput;
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;


    public RedeemVoucherService(BaseInterface baseInterface, RedeemVoucherInput redeemVoucherInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.redeemVoucherInput = redeemVoucherInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<RedeemVoucherOutput> call = apiService.RedeemVoucher(redeemVoucherInput);
        call.enqueue(new Callback<RedeemVoucherOutput>() {
            @Override
            public void onResponse(Call<RedeemVoucherOutput> call, Response<RedeemVoucherOutput> response) {
//                getResponseModel(response.body().toString());
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.RedeemVoucher);
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                  if (response.body()!=null && response.body().getErrorMsgEn() != null) {                      String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<RedeemVoucherOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.RedeemVoucher);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}