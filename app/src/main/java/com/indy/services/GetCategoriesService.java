package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.rewards.GetCategoriesListInput;
import com.indy.models.rewards.GetCategoriesOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to get rewards categories.
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>Uses base request
 * <p><b>Response</b></p>
 * <p>categoriesList: Return categories list including name and URL for map icon of the category.
 * </p>
 */
public class GetCategoriesService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private GetCategoriesListInput getCategoriesListInput;


    public GetCategoriesService(BaseInterface baseInterface, GetCategoriesListInput getCategoriesListInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.getCategoriesListInput = getCategoriesListInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<GetCategoriesOutput> call = apiService.getRewardsCategories(getCategoriesListInput);
        call.enqueue(new Callback<GetCategoriesOutput>() {
            @Override
            public void onResponse(Call<GetCategoriesOutput> call, Response<GetCategoriesOutput> response) {
                mBaseResponseModel.setServiceType(ServiceUtils.getCategories);
                mBaseResponseModel.setResultObj(response.body());

                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                  if (response.body()!=null && response.body().getErrorMsgEn() != null) {                      String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<GetCategoriesOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.getCategories);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
