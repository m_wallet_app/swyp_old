package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.profile.EditProfileInput;
import com.indy.models.profile.EditProfileOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to edit user profile.
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>User details including: Name, gender, language and email address.
 * <p>Registration id: Registration id of user.</p>
 * <p><b>Response</b></p>
 * <p>User Profile: Returns user profile object containing complete user information including name, nickname, email, number, gender,
 * birth date and user membership status.
 * </p>
 */
public class EditProfileService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private EditProfileInput editProfileInput;

    public EditProfileService(BaseInterface baseInterface, EditProfileInput editProfileInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.editProfileInput = editProfileInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<EditProfileOutput> call = apiService.updateProfile(editProfileInput);
        call.enqueue(new Callback<EditProfileOutput>() {
            @Override
            public void onResponse(Call<EditProfileOutput> call, Response<EditProfileOutput> response) {
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.updateProfile);
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                  if (response.body()!=null && response.body().getErrorMsgEn() != null) {                      String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<EditProfileOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.updateProfile);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }


}
