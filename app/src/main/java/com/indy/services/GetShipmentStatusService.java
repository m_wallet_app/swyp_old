package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.getShipmentStatus.GetShipmentStatusInput;
import com.indy.models.getShipmentStatus.GetShipmentStatusOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to get shipment status of order.
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>Registration id: Used for tracking user.
 * <p><b>Response</b></p>
 * <p>status: Return order status.
 * </p>
 */
public class GetShipmentStatusService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private GetShipmentStatusInput getShipmentStatusInput;


    public GetShipmentStatusService(BaseInterface baseInterface, GetShipmentStatusInput getShipmentStatusInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.getShipmentStatusInput = getShipmentStatusInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<GetShipmentStatusOutput> call = apiService.getShipmentStatus(getShipmentStatusInput);
        call.enqueue(new Callback<GetShipmentStatusOutput>() {
            @Override
            public void onResponse(Call<GetShipmentStatusOutput> call, Response<GetShipmentStatusOutput> response) {
                mBaseResponseModel.setServiceType(ServiceUtils.getShipmentStatus);
                mBaseResponseModel.setResultObj(response.body());

                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (response.body() != null && response.body().getErrorMsgEn() != null) {
                        String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<GetShipmentStatusOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.getShipmentStatus);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
