package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.models.packageusage.PackageUsageInput;
import com.indy.models.packageusage.PackageUsageOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to get usage graph details for a specific offer/bundle.
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>Package info: Complex object consisting of bundle id and specific offer id within that package.</p>
 * <p><b>Response</b></p>
 * <p>usageGraphList: Returns list of points for usage graph to plot. </p>
 */

public class PackageUsageService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private PackageUsageInput packageUsageInput;

    public PackageUsageService(BaseInterface baseInterface, PackageUsageInput packageUsageInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.packageUsageInput = packageUsageInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<PackageUsageOutput> call = apiService.getPackageUsage(packageUsageInput);
        call.enqueue(new Callback<PackageUsageOutput>() {
            @Override
            public void onResponse(Call<PackageUsageOutput> call, Response<PackageUsageOutput> response) {
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(null);
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                  if (response.body()!=null && response.body().getErrorMsgEn() != null) {                      String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<PackageUsageOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(null);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}

