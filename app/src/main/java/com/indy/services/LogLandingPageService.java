package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.onboarding_push_notifications.LandingPageRequestModel;
import com.indy.models.onboarding_push_notifications.LandingPageResponseModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LogLandingPageService extends BaseServiceManger {

    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    LandingPageRequestModel landingPageRequestModel;


    public LogLandingPageService(BaseInterface baseInterface, LandingPageRequestModel landingPageRequestModel) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.landingPageRequestModel = landingPageRequestModel;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<LandingPageResponseModel> call = apiService.logLandingPage(landingPageRequestModel);
        call.enqueue(new Callback<LandingPageResponseModel>() {
            @Override
            public void onResponse(Call<LandingPageResponseModel> call, Response<LandingPageResponseModel> response) {
//                getResponseModel(response.body().toString());
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.log_landing_page_service);
                if (response.body()!=null && response.body().getErrorMsgEn() != null) {                     String urlForTag = call.request().url().toString();
                    urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                    urlForTag = urlForTag.replace("/", "_");
                    CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                }
                mBaseInterface.onSucessListener(mBaseResponseModel);
            }

            @Override
            public void onFailure(Call<LandingPageResponseModel> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.log_landing_page_service);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
