package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.validateinvitioncode.ValidateInvitaionCodeInput;
import com.indy.models.validateinvitioncode.ValidateInvitaionCodeOutput;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to validate the invite code entered by user.
 * <p>
 * <p><b>Field description</b></p>
 * <p>invitation code: The invitation code entered by user.</p>
 * <p>registration id: The registration id of the user entering the invitation code.</p>
 * <p><b>Response</b></p>
 * <p>valid: Returns a true/false value to indicate service success/failure response.</p>
 */
public class ValidateInvitaionCodeService extends BaseServiceManger {
    ValidateInvitaionCodeInput mValidateInvitaionCodeInput;
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;

    public ValidateInvitaionCodeService(BaseInterface baseInterface, ValidateInvitaionCodeInput validateInvitaionCodeInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.mValidateInvitaionCodeInput = validateInvitaionCodeInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<ValidateInvitaionCodeOutput> call = apiService.validateInvitationCodeService(mValidateInvitaionCodeInput);
        call.enqueue(new Callback<ValidateInvitaionCodeOutput>() {
            @Override
            public void onResponse(Call<ValidateInvitaionCodeOutput> call, Response<ValidateInvitaionCodeOutput> response) {
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(null);
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                  if (response.body()!=null && response.body().getErrorMsgEn() != null) {                      String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<ValidateInvitaionCodeOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(null);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
