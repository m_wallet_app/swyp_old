package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.onboarding_push_notifications.LogEIDDetailsRequestModel;
import com.indy.models.onboarding_push_notifications.OnboardingLogEventBaseResponse;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.utils.CommonMethods;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LogEIDDetailsService extends BaseServiceManger {


    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    LogEIDDetailsRequestModel logEIDDetailsRequestModel;


    public LogEIDDetailsService(BaseInterface baseInterface, LogEIDDetailsRequestModel logEIDDetailsRequestModel) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.logEIDDetailsRequestModel = logEIDDetailsRequestModel;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<OnboardingLogEventBaseResponse> call = apiService.logEIDDetailsPage(logEIDDetailsRequestModel);
        call.enqueue(new Callback<OnboardingLogEventBaseResponse>() {
            @Override
            public void onResponse(Call<OnboardingLogEventBaseResponse> call, Response<OnboardingLogEventBaseResponse> response) {
//                getResponseModel(response.body().toString());
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.log_eid_details_service);
                if (response.body()!=null && response.body().getErrorMsgEn() != null) {
                    String urlForTag = call.request().url().toString();
                    urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                    urlForTag = urlForTag.replace("/", "_");
                    CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                }
                mBaseInterface.onSucessListener(mBaseResponseModel);
            }

            @Override
            public void onFailure(Call<OnboardingLogEventBaseResponse> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.log_eid_details_service);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
