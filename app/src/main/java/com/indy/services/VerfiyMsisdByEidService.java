package com.indy.services;

import android.app.Service;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.verfiymsisdnusingeid.VerfyMsisidnUsingEidInput;
import com.indy.models.verfiymsisdnusingeid.VerfyMsisidnUsingEidOutput;
import com.indy.utils.CommonMethods;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to validate emirates id during number migration.
 * <p>
 * <p><b>Field description</b></p>
 * <p>emirates id: The emirates id entered by the user.</p>
 * <p>Registration id: The registration id of the user.</p>
 * <p><b>Response</b></p>
 * <p>status: Returns an integer which indicates whether emirates id is valid, invalid, not matching with id in record or is valid but bill amount is due.</p>
 * <p>amount: The amount due in case there is pending bill amount to be paid.</p>
 */
public class VerfiyMsisdByEidService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private VerfyMsisidnUsingEidInput verfyMsisidnUsingEidInput;


    public VerfiyMsisdByEidService(BaseInterface baseInterface, VerfyMsisidnUsingEidInput verfyMsisidnUsingEidInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.verfyMsisidnUsingEidInput = verfyMsisidnUsingEidInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<VerfyMsisidnUsingEidOutput> call = apiService.verfiyMsisdnUsingEid(verfyMsisidnUsingEidInput);
        call.enqueue(new Callback<VerfyMsisidnUsingEidOutput>() {
            @Override
            public void onResponse(Call<VerfyMsisidnUsingEidOutput> call, Response<VerfyMsisidnUsingEidOutput> response) {
                mBaseResponseModel.setResultObj(response.body());
                if (response.body()!=null && response.body().getErrorMsgEn() != null) {                     String urlForTag = call.request().url().toString();
                    urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                    urlForTag = urlForTag.replace("/", "_");
                    CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                }
                mBaseResponseModel.setServiceType(ServiceUtils.CHECK_ELIGIBILITY_FOR_MIGRATION);
                mBaseInterface.onSucessListener(mBaseResponseModel);
            }

            @Override
            public void onFailure(Call<VerfyMsisidnUsingEidOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.CHECK_ELIGIBILITY_FOR_MIGRATION);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
