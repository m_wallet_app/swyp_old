package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.checkmisidenoperator.CheckMisidenOperatorOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterInputResponse;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * /**
 * This service is used to check if current operator is Etisalat customer or not.
 * <p>
 * <p><b>Field description</b></p>
 * <p>Uses base request</p>
 * <p><b>Response</b></p>
 * <p>status: Returns status of 1 or 2 to indicate Etisalat/non-Etisalat customer respectively.</p>
 */
public class CheckMsisdnOperatorService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    MasterInputResponse masterInputResponse;


    public CheckMsisdnOperatorService(BaseInterface baseInterface, MasterInputResponse masterInputResponse) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.masterInputResponse = masterInputResponse;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<CheckMisidenOperatorOutput> call = apiService.checkMisidenOperator(masterInputResponse);
        call.enqueue(new Callback<CheckMisidenOperatorOutput>() {
            @Override
            public void onResponse(Call<CheckMisidenOperatorOutput> call, Response<CheckMisidenOperatorOutput> response) {
//                getResponseModel(response.body().toString());
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.check_msisdn_service);
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                  if (response.body()!=null && response.body().getErrorMsgEn() != null) {                      String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }

                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<CheckMisidenOperatorOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.check_msisdn_service);

                mBaseInterface.onErrorListener(mBaseResponseModel);

            }
        });
    }
}
