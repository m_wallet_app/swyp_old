package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.onboarding_push_notifications.GetOnBoardingDetailsRequestModel;
import com.indy.models.onboarding_push_notifications.GetOnBoardingDetailsResponseModel;
import com.indy.models.onboarding_push_notifications.LogContactDetailsRequestModel;
import com.indy.models.onboarding_push_notifications.OnboardingLogEventBaseResponse;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetOnboardingDetailsService extends BaseServiceManger {

    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    GetOnBoardingDetailsRequestModel getOnBoardingDetailsRequestModel;


    public GetOnboardingDetailsService(BaseInterface baseInterface, GetOnBoardingDetailsRequestModel getOnBoardingDetailsRequestModel) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.getOnBoardingDetailsRequestModel = getOnBoardingDetailsRequestModel;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<GetOnBoardingDetailsResponseModel> call = apiService.getOnboardingDetailsService(getOnBoardingDetailsRequestModel);
        call.enqueue(new Callback<GetOnBoardingDetailsResponseModel>() {
            @Override
            public void onResponse(Call<GetOnBoardingDetailsResponseModel> call, Response<GetOnBoardingDetailsResponseModel> response) {
//                getResponseModel(response.body().toString());
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.log_onboarding_details_service);
                if (response.body()!=null && response.body().getErrorMsgEn() != null) {
                    String urlForTag = call.request().url().toString();
                    urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                    urlForTag = urlForTag.replace("/", "_");
                    CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                }
                mBaseInterface.onSucessListener(mBaseResponseModel);
            }

            @Override
            public void onFailure(Call<GetOnBoardingDetailsResponseModel> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.log_onboarding_details_service);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
