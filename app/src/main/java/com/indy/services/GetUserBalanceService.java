package com.indy.services;

import android.content.Context;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.balance.BalanceInputModel;
import com.indy.models.balance.BalanceResponseModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;
import com.indy.utils.SharedPrefrencesManger;

import java.io.IOException;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Service to get current user balance.
 * <p>This service returns user's current balance.</p>
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>billingPeriod: The billing period for getting the balance for. [DEPRECATED: Ignored at server side.]
 * <p><b>Response</b></p>
 * <p>amount: The balance amount of user.
 * <p>amountUnitEn: The unit of balance in en</p>
 * <p>amountUnitAr: The unit of balance in ar</p>
 * </p>
 */
public class GetUserBalanceService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    BalanceInputModel balanceInputModel;
    Context mContext;

    public GetUserBalanceService(BaseInterface baseInterface, BalanceInputModel balanceInputModel, Context context) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.balanceInputModel = balanceInputModel;
        this.mContext = context;
//        balanceInputModel = new BalanceInputModel();
//        balanceInputModel.setOsVersion("");
//        balanceInputModel.setBillingPeriod("072016");
//        balanceInputModel.setMsisdn("0566503884");
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<BalanceResponseModel> call = apiService.getUserBalance(balanceInputModel);
        call.enqueue(new Callback<BalanceResponseModel>() {
            @Override
            public void onResponse(Call<BalanceResponseModel> call, Response<BalanceResponseModel> response) {
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.balanceServiceType);
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    BalanceResponseModel balanceResponseModel = ((BalanceResponseModel) mBaseResponseModel.getResultObj());
                    if (balanceResponseModel != null) {

                        SharedPrefrencesManger sharedPrefrencesManger = new SharedPrefrencesManger(mContext);
                        sharedPrefrencesManger.setLastUpdated(new Date().getTime() + "");
                    }
                  if (response.body()!=null && response.body().getErrorMsgEn() != null) {                      String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
//                    if((mBaseResponseModel.getResultObj())

                }
            }

            @Override
            public void onFailure(Call<BalanceResponseModel> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.balanceServiceType);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
