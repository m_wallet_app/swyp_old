package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.acceptTermsConditionsService.AccpetTermsConditionsServiceInput;
import com.indy.models.emiratesIDVerificationService.EmiratesIDVerificationInputModel;
import com.indy.models.emiratesIdVerification.VerifyEmiratesIdOutput;
import com.indy.models.emiratesIdVerification.VerifyEmiratesIdOutputString;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterInputResponse;
import com.indy.utils.CommonMethods;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to validate emirates id during new number purchase.
 * <p>
 * <p><b>Field description</b></p>
 * <p>User order details including registrationId, delivery address, shipping type, order type, total amount.</p>
 * <p>emirates id: The emirates id entered by the user.</p>
 * <p>Registration id: The registration id of the user.</p>
 * <p><b>Response</b></p>
 * <p>status: Returns an integer which indicates whether emirates id is valid, invalid, not matching with id in record or is valid but bill amount is due.</p>
 * <p>amount: The amount due in case there is pending bill amount to be paid.</p>
 */
public class AcceptTermsConditionsService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private AccpetTermsConditionsServiceInput accpetTermsConditionsServiceInput;


    public AcceptTermsConditionsService(BaseInterface baseInterface, AccpetTermsConditionsServiceInput accpetTermsConditionsServiceInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.accpetTermsConditionsServiceInput = accpetTermsConditionsServiceInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<VerifyEmiratesIdOutputString> call = apiService.acceptTermsConditionsService(accpetTermsConditionsServiceInput);
        call.enqueue(new Callback<VerifyEmiratesIdOutputString>() {
            @Override
            public void onResponse(Call<VerifyEmiratesIdOutputString> call, Response<VerifyEmiratesIdOutputString> response) {
                mBaseResponseModel.setResultObj(response.body());
                if (response.body() != null && response.body().getErrorMsgEn() != null) {
                    String urlForTag = call.request().url().toString();
                    urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                    urlForTag = urlForTag.replace("/", "_");
                    CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                }
                mBaseResponseModel.setServiceType(ServiceUtils.accept_terms_conditions_service);
                mBaseInterface.onSucessListener(mBaseResponseModel);
            }

            @Override
            public void onFailure(Call<VerifyEmiratesIdOutputString> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setResultObj(ServiceUtils.accept_terms_conditions_service);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }


}
