package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.faqs.FaqsInputModel;
import com.indy.models.faqs.FaqsResponseModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * * This service is used get URL of different application features including FAQs, LiveChat, inviteFriend URL, share Swyp URL,
 * Terms and conditions URL.
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>significant: This is used to identify which feature's URL is requested.
 * <p><b>Response</b></p>
 * <p>SrcURLEn: The url to load for English language.</p>
 * <p>srcURLAr: The url to load for Arabic language.</p>
 * </p>
 */
public class FaqsService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    FaqsInputModel faqsInputModel;

    public  FaqsService(BaseInterface baseInterface, FaqsInputModel mfaqsInputModel) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.faqsInputModel = mfaqsInputModel;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<FaqsResponseModel> call = apiService.getFaqsService(faqsInputModel);
        call.enqueue(new Callback<FaqsResponseModel>() {
            @Override
            public void onResponse(Call<FaqsResponseModel> call, Response<FaqsResponseModel> response) {
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.faqsService);
                if (response.body()!=null && response.body().getErrorMsgEn() != null) {                     String urlForTag = call.request().url().toString();
                    urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                    urlForTag = urlForTag.replace("/", "_");
                    CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                }
                mBaseInterface.onSucessListener(mBaseResponseModel);
            }

            @Override
            public void onFailure(Call<FaqsResponseModel> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.faqsService);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
