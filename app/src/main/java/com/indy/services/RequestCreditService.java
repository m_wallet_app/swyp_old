package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.requestCreditFromFriend.RequestCreditInput;
import com.indy.models.requestCreditFromFriend.RequestCreditOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service requests credit from another user.
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>target msisdn: The number from which balance is requested</p>
 * <p>amount: The credit amount requested from user.</p>
 * <p><b>Response</b></p>
 * <p>requested: boolean return true/false in case of success/failure scenario. </p>
 * <p>transactionId: The transaction id for this request.</p>
 */
public class RequestCreditService extends BaseServiceManger {
    RequestCreditInput requestCreditInput;
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;


    public RequestCreditService(BaseInterface baseInterface, RequestCreditInput requestCreditInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.requestCreditInput = requestCreditInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<RequestCreditOutput> call = apiService.requestCredit(requestCreditInput);
        call.enqueue(new Callback<RequestCreditOutput>() {
            @Override
            public void onResponse(Call<RequestCreditOutput> call, Response<RequestCreditOutput> response) {
//                getResponseModel(response.body().toString());
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.requestCredit);
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                  if (response.body()!=null && response.body().getErrorMsgEn() != null) {                      String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<RequestCreditOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.requestCredit);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
