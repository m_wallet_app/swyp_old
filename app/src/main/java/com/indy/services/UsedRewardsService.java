package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.models.rewards.UsedRewardsoutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterInputResponse;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to get current used rewards for a particular user.
 * <p>
 * <p><b>Field description</b></p>
 * <p>Uses base request</p>
 * <p><b>Response</b></p>
 * <p>rewardsList: Returns a list of used rewards containing rewardName, description, location data and category of the rewards. </p>
 * <p>currentSavings: The total savings made by user for this current month.
 * <p>expiry Date En: The expiry date in english</p>
 * <p>expiry Date Ar: The expiry date in arabic</p>
 */
public class UsedRewardsService extends BaseServiceManger {
    MasterInputResponse masterInputResponse;
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;

    public UsedRewardsService(BaseInterface baseInterface, MasterInputResponse masterInputResponse) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.masterInputResponse = masterInputResponse;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<UsedRewardsoutput> call = apiService.getUsedRewards(masterInputResponse);
        call.enqueue(new Callback<UsedRewardsoutput>() {
            @Override
            public void onResponse(Call<UsedRewardsoutput> call, Response<UsedRewardsoutput> response) {
                mBaseResponseModel.setResultObj(response.body());
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                  if (response.body()!=null && response.body().getErrorMsgEn() != null) {                      String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<UsedRewardsoutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
