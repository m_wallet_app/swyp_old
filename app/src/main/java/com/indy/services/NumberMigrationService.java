package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.models.MigrateEtisaltNumber.MigrateEtisalatNumerOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterInputResponse;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to request for user number migration.
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>Uses base request.</p>
 * <p><b>Response</b></p>
 * <p>Generated code: Code sent to number which is requested for migration.
 * </p>
 */
public class NumberMigrationService extends BaseServiceManger {
    MasterInputResponse masterInputResponse;
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;

    public NumberMigrationService(BaseInterface baseInterface, MasterInputResponse masterInputResponse) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.masterInputResponse = masterInputResponse;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<MigrateEtisalatNumerOutput> call = apiService.migrationService(masterInputResponse);
        call.enqueue(new Callback<MigrateEtisalatNumerOutput>() {
            @Override
            public void onResponse(Call<MigrateEtisalatNumerOutput> call, Response<MigrateEtisalatNumerOutput> response) {
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ConstantUtils.numberMigrationService);
                if (response.body()!=null && response.body().getErrorMsgEn() != null) {                     String urlForTag = call.request().url().toString();
                    urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                    urlForTag = urlForTag.replace("/", "_");
                    CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                }
                mBaseInterface.onSucessListener(mBaseResponseModel);
            }

            //05099881533
            @Override
            public void onFailure(Call<MigrateEtisalatNumerOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ConstantUtils.numberMigrationService);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
