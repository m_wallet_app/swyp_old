package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.getPendingTransfersList.requestCreditFromFriend.GetPendingTransferListOutput;
import com.indy.models.getPendingTransfersList.requestCreditFromFriend.GetPendingTransfersListInput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to get pending requests list from server.
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>Uses base request</p>
 * <p><b>Response</b></p>
 * <p>transactionList: Returns transaction list of current pending requests with request Id, sender msisdn, amount, transfer type and date/time.
 * </p>
 */
public class GetPendingTransactionListService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private GetPendingTransfersListInput getPendingTransfersListInput;

    public GetPendingTransactionListService(BaseInterface baseInterface, GetPendingTransfersListInput getPendingTransfersListInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.getPendingTransfersListInput = getPendingTransfersListInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<GetPendingTransferListOutput> call = apiService.getPendingList(getPendingTransfersListInput);
        call.enqueue(new Callback<GetPendingTransferListOutput>() {
            @Override
            public void onResponse(Call<GetPendingTransferListOutput> call, Response<GetPendingTransferListOutput> response) {
                mBaseResponseModel.setResultObj(response.body());

                mBaseResponseModel.setServiceType(ServiceUtils.getPendingTransfersList);
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                  if (response.body()!=null && response.body().getErrorMsgEn() != null) {                      String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<GetPendingTransferListOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.getPendingTransfersList);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
