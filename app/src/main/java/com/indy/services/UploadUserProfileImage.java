package com.indy.services;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.indy.BuildConfig;
import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.helpers.FileUtils;
import com.indy.models.uploadimages.UploadImageOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterErrorResponse;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * This service uploads/updates/deletes the user profile picture.
 * <p>This is a multipart request as it needs to upload the file. </p>
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>Base request parameters: All request parameters included in the base input response.</p>
 * <p>File: The amount transferred to user.</p>
 * <p><b>Response</b></p>
 * <p>updated: Returns boolean value of true/false to indicate success/failure.</p>
 * <p>profilePictureURL: The URL of the updated profile picture URL.</p>
 */

public class UploadUserProfileImage {

    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private int TIMEOUT = 60; // seconds
    private Uri mImgPath;
    private Context context;
    public static final String MULTIPART_FORM_DATA = "multipart/form-data";
    private String mRegID, mMsisdn, mtoken, mDeviceID, mAuthToken, language, imsi, channel, osVersionParam, appVersionParam;
    private File fileForDelete;
    private MyEndPointsInterface apiService;

    public UploadUserProfileImage(BaseInterface baseInterface, Uri imgPath, Context mContext,
                                  String regID, String msisdn, String token, String deviceID,
                                  String authToken, File fileForDelete, String channel, String imsi,
                                  String langugage, String osVersion, String appVersion) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.mImgPath = imgPath;
        this.context = mContext;
        this.mRegID = regID;
        this.mMsisdn = msisdn;
        this.mAuthToken = authToken;
        this.mtoken = token;
        this.mDeviceID = deviceID;
        this.fileForDelete = fileForDelete;
        this.language = langugage;
        this.imsi = imsi;
        this.channel = channel;
//        this.osVersion = osVersion;
//        this.appVersion = appVersion;
//        consumeService();
        this.osVersionParam = osVersion;
        this.appVersionParam = appVersion;
//        this.mUploadImageInput = uploadImageInput;
        uploadFile();
    }

    private void uploadFile() {
        initParams();
        HashMap<String, RequestBody> params = new HashMap<>();

        RequestBody lang = createPartFromString(language);
        RequestBody imsi = createPartFromString(this.imsi);
        RequestBody appVersion = createPartFromString(appVersionParam);
        RequestBody osVersion = createPartFromString(osVersionParam);
        RequestBody token = createPartFromString(mtoken);
        RequestBody channel = createPartFromString(this.channel);
        RequestBody deviceId = createPartFromString(mDeviceID);
        RequestBody regId = createPartFromString(mRegID);
        RequestBody msisdn = createPartFromString(mMsisdn);
        RequestBody authToken = createPartFromString(mAuthToken);
        params.put("lang", lang);
        params.put("imsi", imsi);
        params.put("appVersion", appVersion);
        params.put("osVersion", osVersion);
        params.put("token", token);
        params.put("channel", channel);
        params.put("deviceId", deviceId);
        params.put("regId", regId);
        params.put("msisdn", msisdn);
        params.put("authToken", authToken);

        Log.v("regID", mRegID + "");

        File file;
        if (mImgPath != null) {
            file = FileUtils.getFile(context, mImgPath);
        } else {
            file = fileForDelete;
        }
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);

        MultipartBody.Part body =
                MultipartBody.Part.createFormData("profilePic", file.getName(), requestFile);
        Call<UploadImageOutput> call = apiService.uploadImg(requestFile, body, params);
        call.enqueue(new Callback<UploadImageOutput>() {
            @Override
            public void onResponse(Call<UploadImageOutput> call,
                                   Response<UploadImageOutput> response) {
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.UploadImageInput);
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (response.body() != null && response.body().getErrorMsgEn() != null) {
                        String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<UploadImageOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.UploadImageInput);

                mBaseInterface.onErrorListener(mBaseResponseModel);

            }
        });
    }


    private RequestBody createPartFromString(String descriptionString) {
        if(descriptionString!=null) {
            return RequestBody.create(
                    MediaType.parse(MULTIPART_FORM_DATA), descriptionString);
        }else{
            return RequestBody.create(
                    MediaType.parse(MULTIPART_FORM_DATA), "");
        }
    }

    public MasterErrorResponse getResponseModel(String reponse) {
        MasterErrorResponse responseStr = null;
        Gson gson = new GsonBuilder().create();
        try {
            responseStr = gson.fromJson(reponse, MasterErrorResponse.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseStr;
    }

    private void initParams() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
        httpClient.networkInterceptors().add(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();
                Request request;
                if (BuildConfig.FLAVOR.equalsIgnoreCase("prod")) {
                    request = original.newBuilder()
                            .header("Accept", "application/json")
                            .method(original.method(), original.body())
                            .build();
                }
                else{
                    request = original.newBuilder()
                            .header("Accept", "application/json")
//                            .header("custom_header", "pre_prod")
                            .method(original.method(), original.body())
                            .build();
                }
                return chain.proceed(request);
            }
        });
        httpClient.addInterceptor(interceptor);
        OkHttpClient client = httpClient.readTimeout(TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MyEndPointsInterface.baseUrl).addConverterFactory(GsonConverterFactory.create())
                .client(client).build();

        apiService = retrofit.create(MyEndPointsInterface.class);
    }
}


