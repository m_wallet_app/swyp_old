package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.inilizeregisteration.InilizeRegisterationInput;
import com.indy.models.inilizeregisteration.InilizeRegisteratonOutput;
import com.indy.models.onboarding_push_notifications.InitializeServiceRequestModel;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterInputResponse;
import com.indy.utils.CommonMethods;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to get user transactions  history list from server.
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>User details including name, email and birth date.</p>
 * <p><b>Response</b></p>
 * <p>Registration id: Registration id of user generated for first time.
 * </p>
 */
public class NewGetRegistrationIDService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private InitializeServiceRequestModel masterInputResponse;
    public NewGetRegistrationIDService(BaseInterface baseInterface, InitializeServiceRequestModel masterInputResponse) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.masterInputResponse = masterInputResponse;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<InilizeRegisteratonOutput> call = apiService.newGetRegistrationID(masterInputResponse);
        call.enqueue(new Callback<InilizeRegisteratonOutput>() {
            @Override
            public void onResponse(Call<InilizeRegisteratonOutput> call, Response<InilizeRegisteratonOutput> response) {
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.new_get_registration_id_service);
                if (response.body()!=null && response.body().getErrorMsgEn() != null) {                     String urlForTag = call.request().url().toString();
                    urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                    urlForTag = urlForTag.replace("/", "_");
                    CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                }
                mBaseInterface.onSucessListener(mBaseResponseModel);
            }

            @Override
            public void onFailure(Call<InilizeRegisteratonOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.new_get_registration_id_service);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
