package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.verifyPromoCode.VerifyPromoCodeInputModel;
import com.indy.models.verifyPromoCode.VerifyPromoCodeOutputResponse;
import com.indy.utils.CommonMethods;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to get congratulations text for user upon creating new profile.
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>Message key: Fixef value to request user message</p>
 * <p><b>Response</b></p>
 * <p>messageText: Returns message text to display.
 * </p>
 */
public class VerifyPromoCodeService extends BaseServiceManger {

    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    VerifyPromoCodeInputModel verifyPromoCodeInputModel;


    public VerifyPromoCodeService(BaseInterface baseInterface, VerifyPromoCodeInputModel verifyPromoCodeInputModel) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.verifyPromoCodeInputModel = verifyPromoCodeInputModel;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<VerifyPromoCodeOutputResponse> call = apiService.verifyPromoCode(verifyPromoCodeInputModel);
        call.enqueue(new Callback<VerifyPromoCodeOutputResponse>() {
            @Override
            public void onResponse(Call<VerifyPromoCodeOutputResponse> call, Response<VerifyPromoCodeOutputResponse> response) {
//                getResponseModel(response.body().toString());
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.VERIFY_PROMO_CODE);
                if (response.body()!=null && response.body().getErrorMsgEn() != null) {                     String urlForTag = call.request().url().toString();
                    urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                    urlForTag = urlForTag.replace("/", "_");
                    CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                }
                mBaseInterface.onSucessListener(mBaseResponseModel);
            }

            @Override
            public void onFailure(Call<VerifyPromoCodeOutputResponse> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.VERIFY_PROMO_CODE);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }

}
