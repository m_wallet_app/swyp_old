package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.updateLanguage.UpdateLanguageInput;
import com.indy.models.updateLanguage.UpdateLanguageOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service updated the language of user.
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>registration id: The registration id of the user.
 * <p><b>Response</b></p>
 * <p>response code: Returns response code to indicate success/failure.</p>
 * <p>response msg: Returns success/failure response message to indicate success/failure.</p>
 */
public class UpdateLanguageService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private UpdateLanguageInput updateLanguageInput;


    public UpdateLanguageService(BaseInterface baseInterface, UpdateLanguageInput mUpdateLanguageInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.updateLanguageInput = mUpdateLanguageInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<UpdateLanguageOutput> call = apiService.updateLanguage(updateLanguageInput);
        call.enqueue(new Callback<UpdateLanguageOutput>() {
            @Override
            public void onResponse(Call<UpdateLanguageOutput> call, Response<UpdateLanguageOutput> response) {
                mBaseResponseModel.setServiceType(ServiceUtils.UPDATE_LANGUAGE);
                mBaseResponseModel.setResultObj(response.body());

                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                  if (response.body()!=null && response.body().getErrorMsgEn() != null) {                      String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<UpdateLanguageOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.UPDATE_LANGUAGE);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
