package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.validateotp.ValidateOtpInput;
import com.indy.models.validateotp.ValidateOtpOutput;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to validate OTP entered by user in different scenarios e.g. forgot password, number migration etc.
 * <p>
 * <p><b>Field description</b></p>
 * <p>otp Code: The OTP code entered by user. </p>
 * <p><b>Response</b></p>
 * <p>valid: Returns a true/false value to indicate service success/failure response.</p>
 */
public class ValidateOtpService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private ValidateOtpInput mValidateOtpInput;


    public ValidateOtpService(BaseInterface baseInterface, ValidateOtpInput validateOtpInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.mValidateOtpInput = validateOtpInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<ValidateOtpOutput> call = apiService.validateOtp(mValidateOtpInput);
        call.enqueue(new Callback<ValidateOtpOutput>() {
            @Override
            public void onResponse(Call<ValidateOtpOutput> call, Response<ValidateOtpOutput> response) {
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ConstantUtils.validateOtp);
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                  if (response.body()!=null && response.body().getErrorMsgEn() != null) {                      String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<ValidateOtpOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ConstantUtils.validateOtp);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
