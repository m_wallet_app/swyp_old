package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.utils.BaseResponseModel;
import com.indy.squad_number.models.AddSquadNumberModel;
import com.indy.squad_number.models.SquadNumberOutput;
import com.indy.squad_number.models.ValidateSquadNumberModel;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ValidateSquadNumberService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private AddSquadNumberModel changePasswordInput;
    Call<ValidateSquadNumberModel> call;

    public ValidateSquadNumberService(BaseInterface baseInterface, AddSquadNumberModel changePasswordInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.changePasswordInput = changePasswordInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        call = apiService.validateSquadNumberService(changePasswordInput);
        call.enqueue(new Callback<ValidateSquadNumberModel>() {
            @Override
            public void onResponse(Call<ValidateSquadNumberModel> call, Response<ValidateSquadNumberModel> response) {
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.validate_squad_number_service);
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (response.body()!=null && response.body().getErrorMsgEn() != null) {                      String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<ValidateSquadNumberModel> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.validate_squad_number_service);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }

    @Override
    public void onWeServiceCallCanceled() {
        super.onWeServiceCallCanceled();
        if (call != null)
            call.cancel();
    }
}
