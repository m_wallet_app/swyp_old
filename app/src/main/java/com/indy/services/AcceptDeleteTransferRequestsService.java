package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.acceptDeleteTransferProcesses.AcceptDeleteTransferProcessesInput;
import com.indy.models.acceptDeleteTransferProcesses.AcceptDeleteTransferProcessesOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to approve/reject received transfer requests in the alerts page in the transfer module.
 * <p>
 * <p><b>Field description</b></p>
 * <p>rqeuestId: This is the request id used to uniquely identify the request.</p>
 * <p>actionType: This parameter is used to differentiate between the different functions of this service e.g. 1 is for accepting a request. </p>
 * <p>targetMsisdn: This parameter specifies the number of the user we send/request the balance to.</p>
 * <p>Amount: The amount(edited and unedited) to be sent against a specific request.</p>
 * <p>
 * <p><b>Response</b></p>
 * <p>status: success/failure</p>
 */
public class AcceptDeleteTransferRequestsService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    AcceptDeleteTransferProcessesInput acceptDeleteTransferProcessesInput;


    public AcceptDeleteTransferRequestsService(BaseInterface baseInterface, AcceptDeleteTransferProcessesInput acceptDeleteTransferProcessesInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.acceptDeleteTransferProcessesInput = acceptDeleteTransferProcessesInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<AcceptDeleteTransferProcessesOutput> call = apiService.acceptDeleteTransferRequests(acceptDeleteTransferProcessesInput);
        call.enqueue(new Callback<AcceptDeleteTransferProcessesOutput>() {
            @Override
            public void onResponse(Call<AcceptDeleteTransferProcessesOutput> call, Response<AcceptDeleteTransferProcessesOutput> response) {
//                getResponseModel(response.body().toString());
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.processRequest);
                if (response.body()!=null && response.body().getErrorMsgEn() != null) {
                    String urlForTag = call.request().url().toString();
                    urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                    urlForTag = urlForTag.replace("/", "_");
                    CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                }
                mBaseInterface.onSucessListener(mBaseResponseModel);
            }

            @Override
            public void onFailure(Call<AcceptDeleteTransferProcessesOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.processRequest);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
