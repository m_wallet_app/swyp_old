package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.locaknumber.LockMobileNumberInput;
import com.indy.models.locaknumber.LockMobileNumberOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 *
 */
public class UnLockNumberService extends BaseServiceManger {
    LockMobileNumberInput lockMobileNumberInput;
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;

    public UnLockNumberService(BaseInterface baseInterface, LockMobileNumberInput locationInputModel) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.lockMobileNumberInput = locationInputModel;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<LockMobileNumberOutput> call = apiService.UnlockNumber(lockMobileNumberInput);
        call.enqueue(new Callback<LockMobileNumberOutput>() {
            @Override
            public void onResponse(Call<LockMobileNumberOutput> call, Response<LockMobileNumberOutput> response) {
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.unLockNumberService);
                if (response.body()!=null && response.body().getErrorMsgEn() != null) {                     String urlForTag = call.request().url().toString();
                    urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                    urlForTag = urlForTag.replace("/", "_");
                    CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                }
                mBaseInterface.onSucessListener(mBaseResponseModel);
            }

            @Override
            public void onFailure(Call<LockMobileNumberOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.unLockNumberService);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
