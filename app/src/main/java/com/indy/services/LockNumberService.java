package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.locaknumber.LockMobileNumberInput;
import com.indy.models.locaknumber.LockMobileNumberOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to lock selected number during on boarding process so the same number is not purchased by some other customer.
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>store id: Fixed value to use for swyp application. </p>
 * <p>reservation id: The registration id generated in the accept terms and conditions service.</p>
 * <p>target Msisdn: THe number to lock temporarily.</p>
 * <p><b>Response</b></p>
 * <p>transactionList: Returns transaction list which includes objects having month name and month transaction list.
 * </p>
 */
public class LockNumberService extends BaseServiceManger {
    LockMobileNumberInput lockMobileNumberInput;
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;

    public LockNumberService(BaseInterface baseInterface, LockMobileNumberInput locationInputModel) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.lockMobileNumberInput = locationInputModel;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<LockMobileNumberOutput> call = apiService.lockNumber(lockMobileNumberInput);
        call.enqueue(new Callback<LockMobileNumberOutput>() {
            @Override
            public void onResponse(Call<LockMobileNumberOutput> call, Response<LockMobileNumberOutput> response) {
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.lockNumberService);
                if (response.body()!=null && response.body().getErrorMsgEn() != null) {                     String urlForTag = call.request().url().toString();
                    urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                    urlForTag = urlForTag.replace("/", "_");
                    CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                }
                mBaseInterface.onSucessListener(mBaseResponseModel);
            }

            @Override
            public void onFailure(Call<LockMobileNumberOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.lockNumberService);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
