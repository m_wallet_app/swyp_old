package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.requestCreditFromFriend.RequestCreditInput;
import com.indy.models.transferCredit.TransferCreditOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service sets the password of new user.
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>target msisdn: The number to which balance is transferred.</p>
 * <p>amount: The amount transferred to user.</p>
 * <p><b>Response</b></p>
 * <p>sent: Returns boolean value of true/false to indicate success/failure.</p>
 */
public class TransferCreditService extends BaseServiceManger {
    RequestCreditInput requestCreditInput;
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;


    public TransferCreditService(BaseInterface baseInterface, RequestCreditInput requestCreditInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.requestCreditInput = requestCreditInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<TransferCreditOutput> call = apiService.transferCredit(requestCreditInput);
        call.enqueue(new Callback<TransferCreditOutput>() {
            @Override
            public void onResponse(Call<TransferCreditOutput> call, Response<TransferCreditOutput> response) {
//                getResponseModel(response.body().toString());
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.transferCredit);
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                  if (response.body()!=null && response.body().getErrorMsgEn() != null) {                      String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<TransferCreditOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.transferCredit);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
