package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.recharge.RechargeInput;
import com.indy.models.recharge.RechargeOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to recharge by scratch card.
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>recharge card number: The card number entered by user for recharge.</p>
 * <p><b>Response</b></p>
 * <p>recharged: boolean value which returns true or false in case of success/failure respectively. </p>
 */
public class RechargeService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private RechargeInput rechargeInput;

    public RechargeService(BaseInterface baseInterface, RechargeInput rechargeInput) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.rechargeInput = rechargeInput;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<RechargeOutput> call = apiService.rechargeCard(rechargeInput);
        call.enqueue(new Callback<RechargeOutput>() {
            @Override
            public void onResponse(Call<RechargeOutput> call, Response<RechargeOutput> response) {
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.rechargeCard);

                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                  if (response.body()!=null && response.body().getErrorMsgEn() != null) {                      String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<RechargeOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.rechargeCard);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
