package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.models.rewards.AvilableRewardsoutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterInputResponse;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to get current available rewards for a particular user.
 * <p>
 * <p><b>Field description</b></p>
 * <p>Uses base request</p>
 * <p><b>Response</b></p>
 * <p>rewardsList: Returns a list of rewards containing rewardName, description, location data and category of the rewards. </p>
 * <p>Returns failure or empty list in case no reward is found.</p>
 */
public class AvilableRewardsService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    MasterInputResponse masterInputResponse;

    public AvilableRewardsService(BaseInterface baseInterface, MasterInputResponse masterInputResponse) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.masterInputResponse = masterInputResponse;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<AvilableRewardsoutput> call = apiService.getAvilableRewards(masterInputResponse);
        call.enqueue(new Callback<AvilableRewardsoutput>() {
            @Override
            public void onResponse(Call<AvilableRewardsoutput> call, Response<AvilableRewardsoutput> response) {
                mBaseResponseModel.setResultObj(response.body());
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (response.body() != null && response.body().getErrorMsgEn() != null) {
                        String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<AvilableRewardsoutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseInterface.onErrorListener(mBaseResponseModel);

            }
        });
    }
}
