package com.indy.services;

import com.indy.controls.BaseInterface;
import com.indy.controls.MyEndPointsInterface;
import com.indy.controls.ServiceUtils;
import com.indy.models.logout.LogoutOutput;
import com.indy.models.utils.BaseResponseModel;
import com.indy.models.utils.MasterInputResponse;
import com.indy.utils.CommonMethods;
import com.indy.utils.ConstantUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This service is used to logout user.
 * <p><b>Field description</b></p>
 * <p><b>Request</b></p>
 * <p>Uses base request.
 * </p>
 * <p><b>Response</b></p>
 * <p>logged out: boolean value indicating whether user is logged out or not.
 * </p>
 */
public class LogoutService extends BaseServiceManger {
    private BaseInterface mBaseInterface;
    private BaseResponseModel mBaseResponseModel;
    private MasterInputResponse masterInputResponse;


    public LogoutService(BaseInterface baseInterface, MasterInputResponse mInputRresponse) {
        this.mBaseInterface = baseInterface;
        mBaseResponseModel = new BaseResponseModel();
        this.masterInputResponse = mInputRresponse;
        consumeService();
    }

    @Override
    public void consumeService() {
        super.consumeService();
        Call<LogoutOutput> call = apiService.logoutUser(masterInputResponse);
        call.enqueue(new Callback<LogoutOutput>() {
            @Override
            public void onResponse(Call<LogoutOutput> call, Response<LogoutOutput> response) {
                mBaseResponseModel.setResultObj(response.body());
                mBaseResponseModel.setServiceType(ServiceUtils.logoutService);
                if (response.code() == ConstantUtils.unAuthorizeToken) {
                    try {
                        mBaseInterface.onUnAuthorizeToken(getResponseModel(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                  if (response.body()!=null && response.body().getErrorMsgEn() != null) {                      String urlForTag = call.request().url().toString();
                        urlForTag = urlForTag.replace(MyEndPointsInterface.baseUrl, "");
                        urlForTag = urlForTag.replace("/", "_");
                        CommonMethods.logFirebaseEvent(CommonMethods.firebaseAnalytics, "service_error_" + urlForTag);
                    }
                    mBaseInterface.onSucessListener(mBaseResponseModel);
                }
            }

            @Override
            public void onFailure(Call<LogoutOutput> call, Throwable t) {
                mBaseResponseModel.setResultObj(t);
                mBaseResponseModel.setServiceType(ServiceUtils.logoutService);
                mBaseInterface.onErrorListener(mBaseResponseModel);
            }
        });
    }
}
