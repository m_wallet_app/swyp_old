package com.indy;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;


import com.indy.views.activites.HelpActivity;
import com.indy.views.activites.MasterActivity;
import com.indy.views.activites.MySettingsActivity;
import com.indy.views.activites.UserProfileActivity;
import com.indy.views.fragments.usage.UsageFragment;
import com.indy.views.fragments.rewards.RewardsFragment;

public class MainActivity extends MasterActivity implements NavigationView.OnNavigationItemSelectedListener {
    private FrameLayout frameLayout;
    private Toolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initUI();
    }

    @Override
    public void initUI() {
        super.initUI();
        frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
        replaceFragmnet(new UsageFragment(), R.id.frameLayout, false);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        return false;
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.myProfile:
                onProifileClick();
                break;
            case R.id.settings:
                onSettingsClick();
                break;
            case R.id.support:
                onSupportClick();
                break;
            case R.id.shareIndy:
                onShareIndyClick();
                break;
            case R.id.invitefriend:
                onInviteFriendsToIndy();
                break;
            case R.id.storeID:
                onStoreClick();
                break;
            case R.id.usageID:
                onUsageClick();
                break;
            case R.id.rewrdsID:
                onRewardsClick();
                break;
        }
    }

    private void onProifileClick() {
        startActivity(new Intent(this, UserProfileActivity.class));
    }

    private void onSettingsClick() {
        startActivity(new Intent(this, MySettingsActivity.class));
    }

    private void onSupportClick() {
        startActivity(new Intent(this, HelpActivity.class));
    }

    private void onShareIndyClick() {
    }

    private void onInviteFriendsToIndy() {
    }

    private void onUsageClick() {
    }

    private void onRewardsClick() {
        replaceFragmnet(new RewardsFragment(), R.id.frameLayout, true);
    }

    private void onStoreClick() {
    }



}
